#!/bin/bash

#######################################
#
# Build Framework standard script for
#
# Tr69 component

# use -e to fail on any shell issue
# -e is the requirement from Build Framework
set -e


# default PATHs - use `man readlink` for more info
# the path to combined build
export RDK_PROJECT_ROOT_PATH=${RDK_PROJECT_ROOT_PATH-`readlink -m ..`}/
export COMBINED_ROOT=$RDK_PROJECT_ROOT_PATH
export WORK_DIR=${WORK_DIR-${COMBINED_ROOT}/work${RDK_PLATFORM_DEVICE^^}}

# path to build script (this script)
export RDK_SCRIPTS_PATH=${RDK_SCRIPTS_PATH-`readlink -m $0 | xargs dirname`}/

# path to components sources and target
export RDK_SOURCE_PATH=${RDK_SOURCE_PATH-`readlink -m .`}/
export RDK_TARGET_PATH=${RDK_TARGET_PATH-$RDK_SOURCE_PATH}/

# fsroot and toolchain (valid for all devices)
export RDK_FSROOT_PATH=${RDK_FSROOT_PATH-`readlink -m $RDK_PROJECT_ROOT_PATH/sdk/fsroot/ramdisk`}/
export RDK_TOOLCHAIN_PATH=${RDK_TOOLCHAIN_PATH-`readlink -m $RDK_PROJECT_ROOT_PATH/sdk/toolchain/staging_dir`}/


# default component name
export RDK_COMPONENT_NAME=${RDK_COMPONENT_NAME-`basename $RDK_SOURCE_PATH`}


# parse arguments
INITIAL_ARGS=$@

function usage()
{
    set +x
    echo "Usage: `basename $0` [-h|--help] [-v|--verbose] [action]"
    echo "    -h    --help                  : this help"
    echo "    -v    --verbose               : verbose output"
    echo "    -p    --platform  =PLATFORM   : specify platform for Tr69"
    echo
    echo "Supported actions:"
    echo "      configure, clean, build (DEFAULT), rebuild, install"
}

# options may be followed by one colon to indicate they have a required argument
if ! GETOPT=$(getopt -n "build.sh" -o hvp: -l help,verbose,platform: -- "$@")
then
    usage
    exit 1
fi

eval set -- "$GETOPT"

while true; do
  case "$1" in
    -h | --help ) usage; exit 0 ;;
    -v | --verbose ) set -x ;;
    -p | --platform ) CC_PLATFORM="$2" ; shift ;;
    -- ) shift; break;;
    * ) break;;
  esac
  shift
done

ARGS=$@


# component-specific vars
CC_PATH=$RDK_SOURCE_PATH
export FSROOT=${RDK_FSROOT_PATH}
export TOOLCHAIN_DIR=${RDK_TOOLCHAIN_PATH}
export BUILDS_DIR=$RDK_PROJECT_ROOT_PATH
export COMBINED_DIR=$RDK_PROJECT_ROOT_PATH
export TR69_CERTS_DEST_DIR=${RDK_FSROOT_PATH}/usr/share/tr69/certs/

if [ $RDK_PLATFORM_DEVICE = "xi3" ]; then
  source ${RDK_PROJECT_ROOT_PATH}/build_scripts/setBCMenv.sh
  source ${BUILDS_DIR}/devicesettings/hal/src/halenv.sh
fi

if [ $RDK_PLATFORM_DEVICE = "xi4" ]; then
  source ${RDK_SOURCE_PATH}/device/build_scripts/soc_env.sh
fi

export TR69_PATH=${CC_PATH}

# functional modules
function configure()
{
#    TR69_PATH=${CC_PATH}
    JOBS_NUM=$((`grep -c ^processor /proc/cpuinfo` + 1))
    buildReport=$RDK_LOG_FILE

    DEBUG=0

    if [ x${BUILD_TYPE} == x ]; then
       export BUILD_TYPE=release
    fi

    export IARM_INCLUDE="-I$IARM_PATH/core/include"
    export TR69BUS_INCLUDE="-I$IARM_PATH/core -I$IARM_MGRS/generic/tr69Bus/include"
    export TR69HOSTIF_INCLUDE=${RDK_PROJECT_ROOT_PATH}/tr69hostif/src/hostif/include
    export  OPENSOURCE_INCLUDE=$WORK_DIR/../opensource/include
    echo $BCM_TOOLCHAIN

    pd=`pwd`

    cd $TR69_PATH

    mkdir -p cfg
    aclocal -I cfg
    libtoolize --automake
    automake --foreign --add-missing
    rm -f configure
    autoconf
    configure_options=" "
    if [ "x$DEFAULT_HOST" != "x" ]; then
        configure_options="--host $DEFAULT_HOST"
    fi
    configure_options="$configure_options --enable-shared --with-pic"
    generic_options="$configure_options"

    rm -rf install
    rm -rf Makefile

    if [ "$RDK_PLATFORM_SOC" = "stm" ];then
        configure_options="$configure_options --with-libtool-sysroot=${RDK_FSROOT_PATH}"
        autoreconf --verbose --force --install
    fi

    if [ $RDK_PLATFORM_DEVICE != "xi4" ]; then
	export PLATFORM_SDK=$REFSW_TOP/uclinux-rootfs
    fi
    export APP_PATH=$BCMAPP

    export LDFLAGS="-L$OPENSOURCE_INCLUDE/../lib \
    -L${PLATFORM_SDK}/lib/uClibc \
    -L${WORK_DIR}/rootfs/lib \
    -L${ROOTFS}/lib \
    -L${ROOTFS}/usr/lib \
    -L${ROOTFS}/usr/local/lib \
    -L${PLATFORM_SDK}/usr/lib \
    -L${PLATFORM_SDK}/usr/local/lib \
    -L${PLATFORM_SDK}/lib \
    -L$APP_PATH/lib  \
    -L${PLATFORM_SDK}/lib/uClibc \
    -L${APPLIBS_DIR}/opensource/directfb/build/1.4.17/97428_linuxuser/lib  \
    -L$IARM_PATH/core/ -lIARMBus \
    -L$OPENSOURCE_LIB -ldbus-1 -ldirect -lfusion -lstdc++ \
    -L$GLIB_LIBRARY_PATH -lglib-2.0 \
    -lm -lz -ldl"

    if [ $RDK_PLATFORM_DEVICE != "xi4" ]; then
	export LDFLAGS+=" -lintl"
    fi

    export PKG_CONFIG_PATH="$OPENSOURCE_INCLUDE/../lib/pkgconfig/:$DFB_LIB/pkgconfig:$APPLIBS_TARGET_DIR/usr/local/lib/pkgconfig:${APPLIBS_DIR}/opensource/directfb/build/1.4.17/97428_linuxuser/lib/pkgconfig:"
   configure_options=""

   if [ $RDK_PLATFORM_DEVICE = "xi3" ]; then
    	export CXX=mipsel-linux-g++
	export CC=mipsel-linux-gcc
	configure_options="--host=mipsel-linux"	
    fi

    if [ $RDK_PLATFORM_DEVICE = "xi4" ]; then
        configure_options="--prefix=$target_usr --host=arm-cortex-linux-gnueabi"
    fi

    configure_options="$configure_options --with-device_root --with-tr181 --with-stunclient --with-serversslauth"
   
    echo $configure_options
 
    # Compile options.
    # To enable ACS Setting through bootstrap auth service,
    # use cFlag as 'AUTH_SERVICE'
    if [ $RDK_PLATFORM_DEVICE = "xi4" ]; then

    ./configure CPPFLAGS="-I$TARGETFS/usr/local/include\
        -I$TARGETFS/usr/local/include/openssl\
        -I$APPLIBS_TARGET_DIR/usr/local/include\
        -I${TARGETFS}/usr/include/libxml2\
        -I$TR69HOSTIF_INCLUDE\
        $IARM_INCLUDE $TR69BUS_INCLUDE -DAUTH_SERVICE" \
	$configure_options
    else
     # Enabled auth service using -DAUTH SERVICE
    ./configure CPPFLAGS="-I$WORK_DIR/rootfs/usr/local/include\
	-I$WORK_DIR/rootfs/usr/local/include/openssl\
	-I$APPLIBS_TARGET_DIR/usr/local/include\
	-I$OPENSOURCE_INCLUDE/libxml2\
	-I$TR69HOSTIF_INCLUDE\
	-I$OPENSOURCE_INCLUDE $IARM_INCLUDE -DAUTH_SERVICE" \
	$configure_options
    fi

    cd $pd
}

function build()
{
    pd=`pwd`
    cd $TR69_PATH
    make clean
    make
    cd $pd
    true #use this function to perform any pre-build configuration
}

function clean()
{
    pd=`pwd`
    cd $TR69_PATH
    make clean
    rm -rf install
    cd $pd
    true #use this function to provide instructions to clean workspace
}

function rebuild()
{
    clean
    configure
    build
}

function install()
{
    TR69_PATH=${CC_PATH}
    TR69DEST_DIR=${RDK_FSROOT_PATH}/usr/local
    EXEDST_DIR=${TR69DEST_DIR}/tr69agent/
    EXEDST_CERTS_DIR=${EXEDST_DIR}/certs/
    TR69_CONFIG_DEST_DIR=${RDK_FSROOT_PATH}/etc/tr69/
    TR69_INSTALL_DIR=$TR69_PATH
    TR69_DEVICE_PATH=device
 
    pd=`pwd`

    cd $TR69_PATH
    #rm -rf $TR69_INSTALL_DIR
    #make install
    mkdir -p $EXEDST_DIR

    rm -rf $EXEDST_DIR/*

    mkdir $EXEDST_CERTS_DIR

    if [ ! -d $TR69_CONFIG_DEST_DIR ] ; then
      mkdir -p $TR69_CONFIG_DEST_DIR
    fi	

    cp $TR69_INSTALL_DIR/conv-util $EXEDST_DIR
    cp $TR69_INSTALL_DIR/dimclient $EXEDST_DIR
    cp $TR69_INSTALL_DIR/dimclient.conf $EXEDST_DIR
    cp $TR69_INSTALL_DIR/log.config $EXEDST_DIR
    cp $TR69_INSTALL_DIR/start.sh $EXEDST_DIR
    cp $TR69_INSTALL_DIR/host-if $EXEDST_DIR
    cp $TR69_INSTALL_DIR/certs/ca.crt $EXEDST_CERTS_DIR

    mkdir -p $TR69_CERTS_DEST_DIR
    cp $TR69_INSTALL_DIR/certs/ca.crt $TR69_CERTS_DEST_DIR/comcast_acs_ca.crt
    
    if [ $RDK_PLATFORM_DEVICE = "xi3" ]; then
       cp $TR69_DEVICE_PATH/datamodel/XI3/data-model.xml $EXEDST_DIR
       cp -a $TR69_DEVICE_PATH/sharedkey/* ${TR69_CONFIG_DEST_DIR}
    fi
    if [ $RDK_PLATFORM_DEVICE = "xi4" ]; then
       cp device/datamodel/XI4/data-model.xml $EXEDST_DIR
       cp -a $TR69_DEVICE_PATH/sharedkey/* ${TR69_CONFIG_DEST_DIR}
    fi
    
    cd $pd
}


# run the logic

#these args are what left untouched after parse_args
HIT=false

for i in "$ARGS"; do
    case $i in
        configure)  HIT=true; configure ;;
        clean)      HIT=true; clean ;;
        build)      HIT=true; build ;;
        rebuild)    HIT=true; rebuild ;;
        install)    HIT=true; install ;;
        *)
            #skip unknown
        ;;
    esac
done

# if not HIT do build by default
if ! $HIT; then
  build
fi
