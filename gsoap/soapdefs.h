/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                      *
 *    support@dimark.com                                                   *
 ***************************************************************************/

//It is needed to enable the SOAP_DEBUG flag if the SOAP_DISPLAY_MSG is enabled
#ifdef SOAP_DISPLAY_MSG
#  ifndef SOAP_DEBUG
#   define SOAP_DEBUG
#  endif
#endif

#define CLIENT_LISTENER 1

/* if BINDING_TO_INTERFACE_NAME_IS_ENABLED is defined then additionally functionality of binding soap socket to interface name is enabled. */
#undef BINDING_TO_INTERFACE_NAME_IS_ENABLED
//#define BINDING_TO_INTERFACE_NAME_IS_ENABLED

/* 080715 Removed by Dimark to prevent memory leak in httpda.c */
/* when SOAP_DEBUG is defined.  Also, httpda.c follows what    */
/* SOAP_MALLOC as defined in gsoap/stdsoap2.h, rather than     */
/* be overridden by this declaration                           */

// #define SOAP_MALLOC(soap, size) calloc(1, size)

const char *getServerURL( void );

char * formatStringToXml(const char *, char *, unsigned int,  char *);

//void* dcalloc(unsigned int size);
//void dfree(void * mem);

#define SOAP_MAXKEEPALIVE (2000) /* max iterations to keep server connection alive */

#define SOAP_BUFLEN (8192)
#define SOAP_PTRBLK     (32)
#define SOAP_PTRHASH   (32)
#define SOAP_IDHASH    (19)
#define SOAP_BLKLEN    (32)
#define SOAP_TAGLEN    (256)
// must be > 4096 to read cookies 
#define SOAP_HDRLEN  (4096+1024) 

