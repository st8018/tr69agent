/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
//Created: San, june 2011
//Last modified: San, june 2011

#include "dimark_globals.h"

#ifdef WITH_STUN_CLIENT

#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
// STUN headers:
#include "STUN_packet.h"

//Attribute types:
#define STUN_AT_MAPPED_ADDRESS 			0x0001
#define STUN_AT_RESPONSE_ADDRESS 		0x0002
#define STUN_AT_CHANGE_REQUEST 			0x0003
#define STUN_AT_SOURCE_ADDRESS 			0x0004
#define STUN_AT_CHANGED_ADDRESS 		0x0005
#define STUN_AT_USERNAME 				0x0006
#define STUN_AT_PASSWORD 				0x0007
#define STUN_AT_MESSAGE_INTEGRITY 		0x0008
#define STUN_AT_ERROR_CODE 				0x0009
#define STUN_AT_UNKNOWN_ATTRIBUTES 		0x000a
#define STUN_AT_REFLECTED_FROM 			0x000b

// San 04 june 2011:
#define STUN_CONNECTION_REQUEST_BIN_ATTR 0xC001
#define STUN_BINDING_CHANGE_ATTR 		 0xC002

#define  random(N) (rand()%N)
/*
void
write_buf(const uint8_t * buf, int len, char * name)
{
	int i;
	if ((!buf) || (!len) || (!name)) return;
	printf("\n%s = [%x]\n",name,*buf);
	for (i = 0; i < len; i++) {
		printf("%X  ",*(buf++));
		if ((i%4) == 3) printf("\n");
	}
}*/


struct STUN_pack *
new_STUN_pack(u_int16_t pack_type, tid_t tid)
{
	struct STUN_pack *stun_pack = NULL;
	// Create new STUN_pack
	stun_pack = (struct STUN_pack *) malloc(sizeof(struct STUN_pack));
	if (stun_pack == NULL)	return NULL; // If error - return NULL

	stun_pack->type = pack_type;
	// Filling all properties by NULL
	stun_pack->username = stun_pack->password = NULL;
	stun_pack->response_addr =
			stun_pack->source_addr =
					stun_pack->mapped_addr =
							stun_pack->reflected_from =
									stun_pack->changed_addr = NULL;
	stun_pack->unknown_attributes =  NULL;
	stun_pack->binding_change_attr =
			stun_pack->connection_request_binding_attr = 0;
	// Filling property stun_pack->tid by value from tid
	memcpy(stun_pack->tid, tid, sizeof(tid_t));

	return stun_pack;
}

void
new_tid(tid_t tid)
{
	int i;
	// new series of pseudo-random numbers
	srand(time(NULL));
	// Filling tid array by random value
	for (i = 0; i < sizeof(tid_t); i++ ) {
		tid[i] = (unsigned char)random(256);
	}
}

struct STUN_string *
add_string_to_STUN_pack(char *str)
{
	struct STUN_string *stun_str = NULL;
	int length;
	// Create struct STUN_string
	stun_str = (struct STUN_string *) malloc(sizeof(struct STUN_string));
	if (stun_str == NULL) return NULL;

	length = strlen(str);
	// if necessary padded with trailing spaces to make its length a multiple of 4 bytes (as required by the STUN protocol).
	length = (length % 4) == 0  ? length : ((length/4)+1)*4;

	// Create STUN_string->content
	stun_str->content = (char *) calloc(length, sizeof(char));
	if (stun_str->content == NULL) {
		free(stun_str);
		stun_str = NULL;
		return NULL;   //Exit if error calloc()
	}
	strncpy(stun_str->content, str, length);
	stun_str->length = length;

	return stun_str;
}

struct STUN_address *
add_address_to_STUN_pack(u_int32_t IP_address, u_int16_t port)
{
	struct STUN_address *address = NULL;

	address = (struct STUN_address *) malloc(sizeof(struct STUN_address));
	if (!address)	return NULL;

	address->family = 0x01;
	address->port = port;
	address->IP_address = IP_address;

	return address;
}


int
add_mapped_address_to_STUN_pack(struct STUN_pack *stunPack, u_int32_t address, u_int16_t port)
{

	struct STUN_address *stunAddr = add_address_to_STUN_pack(address, port);

	if (!stunAddr) return 0;

	stunPack->mapped_addr = stunAddr;

	return 1;
}

int
add_response_address_to_STUN_pack(struct STUN_pack *stunPack, u_int32_t address, u_int16_t port)
{

	struct STUN_address *stunAddr = add_address_to_STUN_pack(address, port);

	if (!stunAddr) return 0;

	stunPack->response_addr = stunAddr;

	return 1;
}

int
add_changed_address_to_STUN_pack(struct STUN_pack *stunPack, u_int32_t address, u_int16_t port)
{

	struct STUN_address *stunAddr = add_address_to_STUN_pack(address, port);

	if (!stunAddr) return 0;

	stunPack->changed_addr = stunAddr;

	return 1;
}

int
add_source_address_to_STUN_pack(struct STUN_pack *stunPack, u_int32_t address, u_int16_t port)
{

	struct STUN_address *stunAddr = add_address_to_STUN_pack(address, port);

	if (!stunAddr)
		return 0;

	stunPack->source_addr = stunAddr;

	return 1;
}

int
add_reflected_from_to_STUN_pack(struct STUN_pack *stunPack, u_int32_t address, u_int16_t port)
{

	struct STUN_address *stunAddr = add_address_to_STUN_pack(address, port);

	if (!stunAddr)
		return 0;

	stunPack->reflected_from = stunAddr;

	return 1;
}

int
add_username_to_STUN_pack(struct STUN_pack *stun_pack, char *username)
{
	// USERNAME limited to 513 bytes.
	if (strlen(username) > 512 ) return -1;
	// Creating new struct STUN_string and exit if error
	if ( (stun_pack->username = add_string_to_STUN_pack(username)) == NULL)	return -2;
	return OK;
}

int
add_password_to_STUN_pack(struct STUN_pack *stun_pack, char *passw)
{
	// Creating new struct STUN_string and exit if error
	if ( (stun_pack->password = add_string_to_STUN_pack(passw)) == NULL) return -1;
	return OK;
}

int
add_binding_change_attr_to_STUN_pack(struct STUN_pack *stun_pack)
{
	stun_pack->binding_change_attr = STUN_BINDING_CHANGE_ATTR;
	return OK;
}

int
add_connection_request_binding_attr_to_STUN_pack(struct STUN_pack *stun_pack)
{
	stun_pack->connection_request_binding_attr = STUN_CONNECTION_REQUEST_BIN_ATTR;
	return OK;
}


struct STUN_binary_pack *
convert_STUN_pack_to_binary(struct STUN_pack *stun_pack)
{
	struct STUN_binary_pack *stun_binary_pack = NULL;
	int content_len = 0;
	unsigned char *content = NULL;
	int ret;

	if (stun_pack->mapped_addr) {
		if ((ret = stun_address_serialise(&(content), &content_len, stun_pack->mapped_addr, STUN_AT_MAPPED_ADDRESS) ) != OK) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN, "convert_STUN_pack_to_binary->address_serialise() has returned an error = %d. "
							"Attribute type = %u\n", ret, STUN_AT_MAPPED_ADDRESS);
			)
			free(content);
			content = NULL;
			return NULL;
		}
	}

	if (stun_pack->response_addr) {
		if ((ret = stun_address_serialise(&(content), &content_len, stun_pack->response_addr, STUN_AT_RESPONSE_ADDRESS) ) != OK) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN, "convert_STUN_pack_to_binary->address_serialise() has returned an error = %d. "
							"Attribute type = %u\n", ret, STUN_AT_RESPONSE_ADDRESS);
			)
			free(content);
			content = NULL;
			return NULL;
		}
	}

	if (stun_pack->changed_addr) {
		if ((ret = stun_address_serialise(&(content), &content_len, stun_pack->changed_addr, STUN_AT_CHANGED_ADDRESS) ) != OK) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN, "convert_STUN_pack_to_binary->address_serialise() has returned an error = %d. "
							"Attribute type = %u\n", ret, STUN_AT_CHANGED_ADDRESS);
			)
			free(content);
			content = NULL;
			return NULL;
		}
	}

	if (stun_pack->source_addr) {
		if ((ret = stun_address_serialise(&(content), &content_len, stun_pack->source_addr, STUN_AT_SOURCE_ADDRESS) ) != OK) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN, "convert_STUN_pack_to_binary->address_serialise() has returned an error = %d. "
							"Attribute type = %u\n", ret, STUN_AT_SOURCE_ADDRESS);
			)
			free(content);
			content = NULL;
			return NULL;
		}
	}

	if (stun_pack->reflected_from) {
		if ((ret = stun_address_serialise(&(content), &content_len, stun_pack->reflected_from, STUN_AT_REFLECTED_FROM) ) != OK) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN, "convert_STUN_pack_to_binary->address_serialise() has returned an error = %d. "
							"Attribute type = %u\n", ret, STUN_AT_REFLECTED_FROM);
			)
			free(content);
			content = NULL;
			return NULL;
		}
	}

	if (stun_pack->username) {
		if ((ret = stun_string_serialise(&(content), &content_len, stun_pack->username, STUN_AT_USERNAME) ) != OK) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN, "convert_STUN_pack_to_binary->address_serialise() has returned an error = %d. "
							"Attribute type = %u\n", ret, STUN_AT_USERNAME);
			)
			free(content);
			content = NULL;
			return NULL;
		}
	}

	if (stun_pack->password) {
		if ((ret = stun_string_serialise(&(content), &content_len, stun_pack->password, STUN_AT_PASSWORD) ) != OK) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN, "convert_STUN_pack_to_binary->address_serialise() has returned an error = %d. "
							"Attribute type = %u\n", ret, STUN_AT_PASSWORD);
			)
			free(content);
			content = NULL;
			return NULL;
		}
	}

	if (stun_pack->unknown_attributes) {
		if ((ret = stun_attribute_list_serialise(&(content), &content_len, stun_pack->unknown_attributes) ) != OK) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN, "convert_STUN_pack_to_binary->address_serialise() has returned an error = %d. "
							"Attribute type = %u\n", ret, STUN_AT_UNKNOWN_ATTRIBUTES);
			)
			free(content);
			content = NULL;
			return NULL;
		}
	}

	if (stun_pack->connection_request_binding_attr == STUN_CONNECTION_REQUEST_BIN_ATTR) {
		if ((ret = connection_request_binding_attr_serialise(&(content), &content_len) ) != OK) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN, "convert_STUN_pack_to_binary->address_serialise() has returned an error = %d. "
							"Attribute type = %u\n", ret, STUN_CONNECTION_REQUEST_BIN_ATTR);
			)
			free(content);
			content = NULL;
			return NULL;
		}


		if (stun_pack->binding_change_attr == STUN_BINDING_CHANGE_ATTR) {
			if ((ret = binding_change_attr_serialise(&(content), &content_len) ) != OK) {
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_STUN, "convert_STUN_pack_to_binary->address_serialise() has returned an error = %d. "
								"Attribute type = %u\n", ret, STUN_BINDING_CHANGE_ATTR);
				)
				free(content);
				content = NULL;
				return NULL;
			}
		}
	}


	// Create header of STUN-message
	/*   All STUN messages consist of a 20 byte header:
		0                   1                   2                   3
		0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |      STUN Message Type        |         Message Length        |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
								Transaction ID
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
																	   |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 */
	stun_binary_pack = (struct STUN_binary_pack *) malloc(sizeof(struct STUN_binary_pack));
	if (!stun_binary_pack) {
		free(content);
		return NULL;
	}

	stun_binary_pack->content = NULL;

	// Size of header STUN Message
	int head_len = 4 + sizeof(tid_t); // Type(2) + Length(2) + sizeof(tid_t) = 20 byte

	// Set length of stun_binary_pack
	stun_binary_pack->length = head_len + content_len;

	stun_binary_pack->content = (unsigned char *) malloc(head_len + content_len);
	if (!stun_binary_pack->content) {
		delete_STUN_binary_pack(stun_binary_pack);
		free(content);
		return NULL;
	}

	// add STUN Message Type
	uint16_t x1 = htons(stun_pack->type);
	memcpy(stun_binary_pack->content, &x1, 2); // sizeof(uint16_t) == 2

	// add Message Length
	uint16_t x2 = htons(content_len);
	memcpy(stun_binary_pack->content+2, &x2, 2); // sizeof(uint16_t) == 2

	// add  Transaction ID
	memcpy(stun_binary_pack->content+4, stun_pack->tid, sizeof(tid_t));

	unsigned char *mem;

	mem = stun_binary_pack->content + head_len;
	unsigned char * from = content;

	while (content_len--) {
		*(mem++) = *(from++);
	}

	free(content);

	return stun_binary_pack;
}



struct STUN_address *
convert_binary_to_stun_address(unsigned char *buf)
{
	struct STUN_address *stun_address = NULL;

	stun_address = (struct STUN_address *) malloc(sizeof(struct STUN_address));
	if (!stun_address) return NULL;

	unsigned char * temp = buf;
	// ignore firts byte(buf[0])
	stun_address->family = *(++temp);
	stun_address->port = (*(++temp) * 0x0100) + *(++temp);
	stun_address->IP_address = (u_int32_t) (*(++temp) * 0x01000000) + (*(++temp) * 0x010000) + (*(++temp) * 0x0100) + *(++temp);

	return stun_address;
}

struct STUN_pack *
convert_binary_to_STUN_pack(unsigned char *buf, int buf_len)
{
	if (buf_len < 20) {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_STUN,"convert_binary_to_STUN_pack(): too short packet.\n");
		)
		return NULL;		// too short packet
	}

	u_int16_t stun_pack_type;
	memcpy(&stun_pack_type, buf, 2);
	stun_pack_type = ntohs(stun_pack_type);

	u_int16_t stun_pack_len;
	memcpy(&stun_pack_len, buf + 2, 2);
	stun_pack_len = ntohs(stun_pack_len);

	tid_t tid;
	if (stun_pack_len != (buf_len - 20)){
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_STUN,"convert_binary_to_STUN_pack(): incorrect packet size.\n");
		)
		return NULL;	// Incorrect packet size
	}

	memcpy(tid, buf + 4, sizeof(tid));

	struct STUN_pack *stun_pack;
	stun_pack = new_STUN_pack(stun_pack_type, tid);
	stun_pack->type = stun_pack_type;

	unsigned char * attributes_start = buf + 4 + sizeof(tid);
	unsigned char * cur_attr_start = attributes_start;

	// While not buffer end:
	while (  (cur_attr_start - attributes_start) <= stun_pack_len -4 ) {

		struct STUN_address *stunAddr;

		u_int16_t attr_type;
		memcpy(&attr_type, cur_attr_start, 2);
		attr_type = ntohs(attr_type);

		u_int16_t attr_len;
		memcpy(&attr_len, cur_attr_start + 2, 2);
		attr_len = ntohs(attr_len);

		// If attribute length more that end of packet size:
		if ( (attributes_start + stun_pack_len) - (cur_attr_start + 4 + attr_len) < 0  ) {
			delete_STUN_pack(stun_pack);
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN,"convert_binary_to_STUN_pack(): attribute length more that end of packet size.\n");
			)
			return NULL;
		}

		unsigned char * cur_attr_value_start = cur_attr_start + 4;

		switch (attr_type) {
			case STUN_AT_RESPONSE_ADDRESS:
				if (!stun_pack->response_addr) {
					stunAddr = convert_binary_to_stun_address(cur_attr_value_start);
					if (!stunAddr) return NULL;
					stun_pack->response_addr = stunAddr;
				}
				break;
			case STUN_AT_SOURCE_ADDRESS:
				if (!stun_pack->source_addr) {
					stunAddr = convert_binary_to_stun_address(cur_attr_value_start);
					if (!stunAddr) return NULL;
					stun_pack->source_addr = stunAddr;
				}
				break;
			case STUN_AT_CHANGED_ADDRESS:
				if (!stun_pack->changed_addr) {
					stunAddr = convert_binary_to_stun_address(cur_attr_value_start);
					if (!stunAddr) return NULL;
					stun_pack->changed_addr = stunAddr;
				}
				break;
			case STUN_AT_REFLECTED_FROM:
				if (!stun_pack->reflected_from) {
					stunAddr = convert_binary_to_stun_address(cur_attr_value_start);
					if (!stunAddr) return NULL;
					stun_pack->reflected_from = stunAddr;
				}
				break;
			case STUN_AT_MAPPED_ADDRESS:
				if (!stun_pack->mapped_addr) {
					stunAddr = convert_binary_to_stun_address(cur_attr_value_start);
					if (!stunAddr) return NULL;
					stun_pack->mapped_addr = stunAddr;
				}
				break;
			case STUN_AT_USERNAME:
			{
				char username[512] = {'\0'};
				memcpy(username, cur_attr_value_start, attr_len);
				username[attr_len] = '\0';
				if (add_username_to_STUN_pack(stun_pack, username) != OK) return NULL;
				break;
			}
			case STUN_AT_PASSWORD:
			{
				char password[512] = {'\0'};
				memcpy(password, cur_attr_value_start, attr_len);
				password[attr_len] = '\0';
				if (add_password_to_STUN_pack(stun_pack, password) != OK) return NULL;
				break;
			}
			case STUN_AT_CHANGE_REQUEST:
			case STUN_AT_MESSAGE_INTEGRITY:
			case STUN_AT_ERROR_CODE:
			case STUN_AT_UNKNOWN_ATTRIBUTES:
			default:
				if (attr_type <= 0x7fff)
					add_unknown_attribute_to_STUN_pack(stun_pack, attr_type);
				break;
		}

		cur_attr_start += 4 + attr_len;
		stunAddr = NULL;
	} // end of while (  (cur_attr_start - attributes_start) <= stun_pack_len -4 )

	return stun_pack;
}

void
delete_STUN_pack(struct STUN_pack *stun_pack)
{
	if (!stun_pack) return;
	if (stun_pack->mapped_addr)
	{
		free(stun_pack->mapped_addr);
		stun_pack->mapped_addr = NULL;
	}
	if (stun_pack->response_addr)
	{
		free(stun_pack->response_addr);
		stun_pack->response_addr = NULL;
	}
	if (stun_pack->source_addr)
	{
		free(stun_pack->source_addr);
		stun_pack->source_addr = NULL;
	}
	if (stun_pack->changed_addr)
	{
		free(stun_pack->changed_addr);
		stun_pack->changed_addr = NULL;
	}
	if (stun_pack->reflected_from)
	{
		free(stun_pack->reflected_from);
		stun_pack->reflected_from = NULL;
	}
	if (stun_pack->username) {
		if (stun_pack->username->content)
		{
			free(stun_pack->username->content);
			stun_pack->username->content = NULL;
		}
		free(stun_pack->username);
		stun_pack->username = NULL;

	}
	if (stun_pack->password) {
		if (stun_pack->password->content)
		{
			free(stun_pack->password->content);
			stun_pack->password->content = NULL;
		}
		free(stun_pack->password);
		stun_pack->password = NULL;
	}
	if (stun_pack->unknown_attributes){
		if (stun_pack->unknown_attributes->attributes)
		{
			free(stun_pack->unknown_attributes->attributes);
			stun_pack->unknown_attributes->attributes = NULL;
		}
		free(stun_pack->unknown_attributes);
		stun_pack->unknown_attributes = NULL;
	}
	free(stun_pack);
	stun_pack = NULL;
}

void
delete_STUN_binary_pack(struct STUN_binary_pack *stun_bin_pack)
{
	if (!stun_bin_pack) return;
	if (stun_bin_pack->content)
	{
		free(stun_bin_pack->content);
		stun_bin_pack->content = NULL;
	}
	free(stun_bin_pack);
	stun_bin_pack = NULL;
}

int
add_unknown_attribute_to_STUN_pack(struct STUN_pack *stun_pack, u_int16_t attr_type)
{
	if (!stun_pack) return -1;

	if (!stun_pack->unknown_attributes) {
		stun_pack->unknown_attributes = (struct STUN_attribute_list *) malloc(sizeof(struct STUN_attribute_list));

		if (stun_pack->unknown_attributes) {
			stun_pack->unknown_attributes->unkn_attr_count = 0;
			stun_pack->unknown_attributes->attributes = NULL;
		}
		else return -1;
	}

	unsigned int count = stun_pack->unknown_attributes->unkn_attr_count;

	if (count % 2 == 1) {
		*( stun_pack->unknown_attributes->attributes + count ) = htons(attr_type);
	} else {
		u_int16_t * tmp = NULL;
		tmp = (u_int16_t *) realloc (stun_pack->unknown_attributes->attributes, sizeof(u_int16_t) * (count + 2));

		if (!tmp) return -1;

		*( tmp + count ) = htons(attr_type);
		*( tmp + count+1 ) = *( tmp + count );
		stun_pack->unknown_attributes->attributes = tmp;

	}

	stun_pack->unknown_attributes->unkn_attr_count = count + 1;

	return OK;
}

int
stun_address_serialise(unsigned char ** buf, int * buf_len, struct STUN_address *stun_addr, uint16_t stun_attr_type)
{
	/*
	 * see rfc5389:
	     0                   1                   2                   3
       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |         Type                  |            Length             |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                         Value (variable)                ....
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

                    Figure 4: Format of STUN Attributes
	 *
	 *
	 * For example:
	   The format of the MAPPED-ADDRESS attribute is:

	       0                   1                   2                   3
	       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	      |0 0 0 0 0 0 0 0|    Family     |           Port                |
	      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	      |                                                               |
	      |                 Address (32 bits or 128 bits)                 |
	      |                                                               |
	      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

	               Figure 5: Format of MAPPED-ADDRESS Attribute

	   The address family can take on the following values:

	   0x01:IPv4
	   0x02:IPv6

	   The first 8 bits of the MAPPED-ADDRESS MUST be set to 0 and MUST be
	   ignored by receivers.  These bits are present for aligning parameters
	   on natural 32-bit boundaries.
	 */
	const int value_len = 8;
	const int attr_len = 4 + value_len; // Type(2) + Length(2) + Value(8)

	uint8_t * temp_buf = NULL;
	temp_buf = (unsigned char *) malloc(attr_len);
	if (!temp_buf) return -1;

	// add attribute type
	uint16_t x1 = htons(stun_attr_type);
	memcpy(temp_buf, &x1, 2); // sizeof(uint16_t) == 2

	// add attribute length
	uint16_t x2 = htons(value_len);
	memcpy(temp_buf+2, &x2, 2); // sizeof(uint16_t) == 2

	// add first byte 0
	temp_buf[4] = 0;

	// add Family 0x01:IPv4
	temp_buf[5] = 0x01; //stun_addr->family

	// add Port
	uint16_t x3 = htons(stun_addr->port);
	memcpy(temp_buf+6, &x3, 2); // sizeof(uint16_t) == 2

	// add IP-address
	uint32_t x4 = htonl(stun_addr->IP_address);
	memcpy(temp_buf+8, &x4, 4);


	unsigned char *mem = NULL;

	if (!(*buf)) {
		*buf_len = 0;
	}
	mem = realloc(*buf, (*buf_len + attr_len) * sizeof(unsigned char));
	if (!mem)
	{
		free(temp_buf);
		temp_buf = NULL;
		return -2;
	}

	*buf = mem;

	mem = *buf + *buf_len;
	int i = attr_len;
	unsigned char * from = temp_buf;

	while (i--) {
		*(mem++) = *(from++);
	}
	free(temp_buf);
	temp_buf = NULL;

	*buf_len += attr_len;

	return OK;
}


int
stun_string_serialise(unsigned char ** buf, int * buf_len, struct STUN_string *stun_str, uint16_t stun_attr_type)
{
	const int value_len = stun_str->length;
	const int attr_len = 4 + value_len; // Type(2) + Length(2) + Value(8)

	uint8_t * temp_buf = NULL;
	temp_buf = (unsigned char *) malloc(attr_len);
	if (!temp_buf) return -1;

	// add attribute type
	uint16_t x1 = htons(stun_attr_type);
	memcpy(temp_buf, &x1, 2); // sizeof(uint16_t) == 2

	// add attribute length
	uint16_t x2 = htons(value_len);
	memcpy(temp_buf+2, &x2, 2); // sizeof(uint16_t) == 2

	// add string
	memcpy(temp_buf+4, stun_str->content, value_len);

	unsigned char *mem = NULL;

	if (!(*buf)) *buf_len = 0;
	mem = realloc(*buf, (*buf_len + attr_len) * sizeof(unsigned char));
	if (!mem)
	{
		free(temp_buf);
		temp_buf = NULL;
		return -2;
	}

	*buf = mem;

	mem = *buf + *buf_len;
	int i = attr_len;
	unsigned char * from = temp_buf;

	while (i--) {
		*(mem++) = *(from++);
	}
	free(temp_buf);
	temp_buf = NULL;

	*buf_len += attr_len;

	return OK;
}

/*
 *
 *
 */
int
stun_attribute_list_serialise(unsigned char ** buf, int * buf_len, struct STUN_attribute_list *stun_attr_list)
{
	// (n % 2 == 0) ? n : n + 1;  n*2;
	// We install the size, to the multiple 4 bytes
	const int value_len = (stun_attr_list->unkn_attr_count  +  stun_attr_list->unkn_attr_count % 2) * 2;
	const int attr_len = 4 + value_len; // Type(2) + Length(2) + Value(8)

	uint8_t * temp_buf = NULL;
	temp_buf = (unsigned char *) malloc(attr_len);
	if (!temp_buf) return -1;

	// add attribute type
	uint16_t x1 = htons(STUN_AT_UNKNOWN_ATTRIBUTES);
	memcpy(temp_buf, &x1, 2); // sizeof(uint16_t) == 2

	// add attribute length
	uint16_t x2 = htons(value_len);
	memcpy(temp_buf+2, &x2, 2); // sizeof(uint16_t) == 2

	// add attributes
	memcpy(temp_buf+4, stun_attr_list->attributes, value_len);

	unsigned char *mem = NULL;

	if (!(*buf)) *buf_len = 0;
	mem = realloc(*buf, (*buf_len + attr_len) * sizeof(unsigned char));
	if (!mem)
	{
		free(temp_buf);
		temp_buf = NULL;
		return -2;
	}

	*buf = mem;

	mem = *buf + *buf_len;
	int i = attr_len;
	unsigned char * from = temp_buf;

	while (i--) {
		*(mem++) = *(from++);
	}
	free(temp_buf);
	temp_buf = NULL;

	*buf_len += attr_len;

	return OK;
}

int
connection_request_binding_attr_serialise(unsigned char ** buf, int * buf_len)
{
	/*
	Indicates the binding on which the CPE is listening for	UDP Connection Requests.
	The content of the Value element of this attribute MUST	be the following byte string:
	0x64 0x73 0x6C 0x66
	0x6F 0x72 0x75 0x6D
	0x2E 0x6F 0x72 0x67
	0x2F 0x54 0x52 0x2D
	0x31 0x31 0x31 0x20
	This corresponds to the following text string: “dslforum.org/TR-111 ”
	A space character is the last character of this string so that its length is a multiple of four characters.
	The Length element of this attribute MUST equal: 0x0014 (20 decimal)
	 */

	const int value_len = 20;
	const int attr_len = 4 + value_len; // Type(2) + Length(2) + Value(8)

	//"dslforum.org/TR-111 ";
	unsigned char conn_req_bind_attrValue[20] = {0x64,0x73,0x6C,0x66,0x6F,0x72,0x75,0x6D,0x2E,0x6F,0x72,0x67,0x2F,0x54,0x52,0x2D,0x31,0x31,0x31,0x20};

	uint8_t * temp_buf = NULL;
	temp_buf = (unsigned char *) malloc(attr_len);
	if (!temp_buf) return -1;

	// add attribute type
	uint16_t x1 = htons(STUN_CONNECTION_REQUEST_BIN_ATTR);
	memcpy(temp_buf, &x1, 2); // sizeof(uint16_t) == 2

	// add attribute length
	uint16_t x2 = htons(value_len);
	memcpy(temp_buf+2, &x2, 2); // sizeof(uint16_t) == 2

	// add attributes
	memcpy(temp_buf+4, conn_req_bind_attrValue, value_len);

	unsigned char *mem = NULL;

	if (!(*buf)) *buf_len = 0;
	mem = realloc(*buf, (*buf_len + attr_len) * sizeof(unsigned char));
	if (!mem)
	{
		free(temp_buf);
		temp_buf = NULL;
		return -2;
	}

	*buf = mem;

	mem = *buf + *buf_len;
	int i = attr_len;
	unsigned char * from = temp_buf;

	while (i--) {
		*(mem++) = *(from++);
	}
	free(temp_buf);
	temp_buf = NULL;

	*buf_len += attr_len;

	return OK;
}

int
binding_change_attr_serialise(unsigned char ** buf, int * buf_len)
{
	/*
		Indicates that the binding has changed.
		This attribute contains no value. Its Length element MUST be equal to zero.
		This attribute MUST only be used where the CONNECTION-REQUEST-BINDING is also included.
	 */

	const int attr_len = 4; // Type(2) + Length(2)

	uint8_t * temp_buf = NULL;
	temp_buf = (unsigned char *) malloc(attr_len);
	if (!temp_buf) return -1;

	// add attribute type
	uint16_t x1 = htons(STUN_BINDING_CHANGE_ATTR);
	memcpy(temp_buf, &x1, 2); // sizeof(uint16_t) == 2

	// add attribute length
	uint16_t x2 = 0;
	memcpy(temp_buf+2, &x2, 2); // sizeof(uint16_t) == 2

	unsigned char *mem = NULL;

	if (!(*buf)) *buf_len = 0;
	mem = realloc(*buf, (*buf_len + attr_len) * sizeof(unsigned char));
	if (!mem)
	{
		free(temp_buf);
		temp_buf = NULL;
		return -2;
	}

	*buf = mem;

	mem = *buf + *buf_len;
	int i = attr_len;
	unsigned char * from = temp_buf;

	while (i--) {
		*(mem++) = *(from++);
	}
	free(temp_buf);
	temp_buf = NULL;

	*buf_len += attr_len;

	return OK;
}

#endif /* WITH_STUN_CLIENT */
