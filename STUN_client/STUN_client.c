/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
//Created: San, june 2011
//Last modified: San, Sep 2011

#include "dimark_globals.h"

#ifdef WITH_STUN_CLIENT

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <assert.h>

#include "paramaccess.h"
#include "parameterStore.h"
#include "utils.h"
#include "STUN_client.h"
#include "STUN_packet.h"
#include "udpCRhandler.h"

#define BINDING_REQUEST_RETRANSMISSION_COUNT 9

struct timeval timeouts[BINDING_REQUEST_RETRANSMISSION_COUNT] =
{
		{0L, 100000L},
		{0L, 200000L},
		{0L, 400000L},
		{0L, 800000L},
		{1L, 600000L},
		{1L, 600000L},
		{1L, 600000L},
		{1L, 600000L},
		{1L, 600000L}
};

static volatile int cancelStunThreadActivated = 0; // is true when pthread_cancel() is called for StunThread, else must be set to false

static int tid_compare(tid_t , tid_t );
static char *addr_ntoa(unsigned long );
static struct STUN_pack * send_STUN_pack_and_check_response(char *stun_host, unsigned int stun_port, struct STUN_pack * requestPack);

extern pthread_mutex_t udpSockMutex;
extern volatile int udpSocketIsShutdown;

int bindingRequest(	char 	*stun_host,
					unsigned int stun_port,
					char 	*stun_username,
					char 	*stun_password,
					char 	*my_ip,
					int  	*my_port)
{
    struct STUN_pack *requestPack = NULL, *responsePack = NULL;
    tid_t tid;

    my_ip[0] = 0;
    *my_port = 0;

    DEBUG_OUTPUT (
    		dbglog (SVR_DEBUG, DBG_STUN,
    				"bindingRequest() enter host = %s, port = %d, username = %s, listen_port = %d\n",
    				stun_host, stun_port, stun_username, ACS_UDP_NOTIFICATION_PORT);
    )

    // create STUN_packet and translate it to binary_STUN_packet
	new_tid(tid);
	requestPack = new_STUN_pack(STUN_MT_BINDING_REQ, tid);
	if (!requestPack) {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_STUN, "bindingRequest() Error: new_STUN_pack() has returned NULL\n");
		)
		return -1;
	}

    if (add_connection_request_binding_attr_to_STUN_pack(requestPack) != OK) {
    	DEBUG_OUTPUT (
    			dbglog (SVR_ERROR, DBG_STUN, "bindingRequest(): Can't add connection_request_binding_attr\n");
    	)
    	delete_STUN_pack(requestPack);
        return -2;
    }

	if (stun_username && (add_username_to_STUN_pack(requestPack, stun_username) != OK)) {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_STUN, "bindingRequest(): Can't add username\n");
		)
		delete_STUN_pack(requestPack);
		return -2;
	}

	if (stun_password && (add_password_to_STUN_pack(requestPack, stun_password) != OK)) {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_STUN, "bindingRequest(): Can't add password\n");
		)
		delete_STUN_pack(requestPack);
		return -2;
	}

	responsePack = send_STUN_pack_and_check_response(stun_host, stun_port, requestPack);
	delete_STUN_pack(requestPack);
	if (!responsePack)
	{
		return -3;
	}

	switch (responsePack->type)
	{
		case STUN_MT_BINDING_REQ:
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_STUN, "bindingRequest(): Binding Request\n");
			)
			delete_STUN_pack(responsePack);
			return OK ;
			break;

		case STUN_MT_BINDING_RES:
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_STUN, "bindingRequest(): Binding Response\n");
			)
			if (responsePack->mapped_addr)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_INFO, DBG_STUN, "Mapped Address: %s, port %d\n", addr_ntoa(responsePack->mapped_addr->IP_address),responsePack->mapped_addr->port);
				)
				strcpy(my_ip,addr_ntoa(responsePack->mapped_addr->IP_address));
				*my_port = responsePack->mapped_addr->port;

				delete_STUN_pack(responsePack);
				return OK ;
			}
			else
			{
				DEBUG_OUTPUT (
						dbglog (SVR_INFO, DBG_STUN, "bindingRequest(): responsePack->mapped_addr is NULL\n");
				)
			}
			break;

		case STUN_MT_BINDING_ERR_RES:
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_STUN, "bindingRequest(): Binding error Response\n");
			)
			break;

		default:
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_STUN, "bindingRequest(): responsePack->type is unknown\n");
			)
			break;
	}

	delete_STUN_pack(responsePack);
	return -5;
}


// Creating, sending of binding_change_packet
int bindingChange(	char 	*stun_host,
					unsigned int 	 stun_port,
					char 	*stun_username,
					char 	*stun_password)
{
	struct STUN_pack *requestPack = NULL, *responsePack = NULL;
	tid_t tid;

    DEBUG_OUTPUT (
    		dbglog (SVR_DEBUG, DBG_STUN,
    				"bindingChange() enter host = %s, port = %d, username = %s, listen_port = %d\n",
    				stun_host,stun_port, stun_username, ACS_UDP_NOTIFICATION_PORT);
    )

    new_tid(tid);
    requestPack = new_STUN_pack(STUN_MT_BINDING_REQ, tid);
    if (!requestPack) {
    	DEBUG_OUTPUT (
    			dbglog (SVR_ERROR, DBG_STUN, "bindingChange() Error: bindingChange()->new_STUN_pack() has returned NULL\n");
    	)
    	return(-6);
    }

    if (add_binding_change_attr_to_STUN_pack(requestPack) != OK) {
    	DEBUG_OUTPUT (
    			dbglog (SVR_ERROR, DBG_STUN, "bindingChange(): Can't add binding_change_attr\n");
    	)
    	delete_STUN_pack(requestPack);
        return(-2) ;
    }

    if (add_connection_request_binding_attr_to_STUN_pack(requestPack) != OK) {
    	DEBUG_OUTPUT (
    			dbglog (SVR_ERROR, DBG_STUN, "bindingChange(): Can't add connection_request_binding_attr\n");
    	)
    	delete_STUN_pack(requestPack);
        return(-2) ;
    }

    if (stun_username && (add_username_to_STUN_pack(requestPack, stun_username) != OK)) {
    	DEBUG_OUTPUT (
    			dbglog (SVR_ERROR, DBG_STUN, "bindingChange(): Can't add username\n");
    	)
    	delete_STUN_pack(requestPack);
        return(-2) ;
    }

    if (stun_password && (add_password_to_STUN_pack(requestPack, stun_password) != OK)) {
    	DEBUG_OUTPUT (
    			dbglog (SVR_ERROR, DBG_STUN, "bindingChange(): Can't add stun_password\n");
    	)
    	delete_STUN_pack(requestPack);
        return(-2) ;
    }

    responsePack = send_STUN_pack_and_check_response(stun_host, stun_port, requestPack);
	delete_STUN_pack(requestPack);
	if (!responsePack)
	{
		return -3;
	}

    switch (responsePack->type) {
        case STUN_MT_BINDING_REQ:
            DEBUG_OUTPUT (
            		dbglog (SVR_INFO, DBG_STUN, "bindingChange(): Binding Request\n");
            )
            break;

        case STUN_MT_BINDING_RES:
            DEBUG_OUTPUT (
            		dbglog (SVR_INFO, DBG_STUN, "bindingChange(): Binding Response\n");
            )
            break;

        case STUN_MT_BINDING_ERR_RES:
            DEBUG_OUTPUT (
            		dbglog (SVR_INFO, DBG_STUN, "bindingChange(): Binding error Response\n");
            )
            break;

        default:
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_STUN, "bindingChange(): responsePack->type is unknown\n");
			)
			break;
     }

    delete_STUN_pack(responsePack);
    return OK;

}

//Function compares a 2 value of tid.
static int tid_compare(tid_t tid1, tid_t tid2)
{
	if ((tid1 == NULL) && (tid2 == NULL)) return OK; // If both tids are NULL that they are equal.
	if ((tid1 == NULL) || (tid2 == NULL)) return -1; // If only one from tids is NULL than they are not equal.
	int i, tid_len = sizeof(tid_t);

	for ( i = 0; i < tid_len; i++ ) {
		if (tid1[i] != tid2[i]) return -1;
	}
	return OK;
}

static char *addr_ntoa(unsigned long addr)
{
    struct in_addr in;

    in.s_addr = ntohl(addr);
    return (inet_ntoa(in));
}





/* Convert STUN_pack request to STUN_binary_pack request,
 * Send STUN_binary_pack request,
 * Recive binary response,
 * convert binary esponse to STUN_pack response,
 * Compare tIds of STUN_pack request and STUN_pack response.
 * Return STUN_pack response.
 * */
static struct STUN_pack * send_STUN_pack_and_check_response(char *stun_host, unsigned int stun_port, struct STUN_pack * requestPack)
{
    struct sockaddr_in srvAddr;
    int sock, bytesAmount, ret, i;
    struct hostent *hostEnt = NULL;
    unsigned char buf[8192];
    struct timeval timeout;
    struct STUN_binary_pack *requestBinaryPack = NULL;
    struct STUN_pack *responsePack = NULL;
    unsigned short port = DEFAULT_STUN_SERVER_UDP_PORT;
    struct addrinfo hints, *res = NULL, *p = NULL;
    int err;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_DGRAM; /* Datagram socket */
    hints.ai_flags = 0;
    hints.ai_protocol = 0;          /* Any protocol */

    requestBinaryPack = convert_STUN_pack_to_binary(requestPack);
    if (!requestBinaryPack) {
	    DEBUG_OUTPUT (
			    dbglog (SVR_ERROR, DBG_STUN, "send_STUN_pack_and_check_response() Error: convert_STUN_pack_to_binary() has returned NULL\n");
			 )
		    return NULL;
    }

    pthread_mutex_lock(&udpSockMutex);
    if (cancelStunThreadActivated)
    {
		pthread_mutex_unlock(&udpSockMutex);
		delete_STUN_binary_pack(requestBinaryPack);
		return NULL;
    }

    sock = getUDPSocket();
    if (sock < 0)
    {
	    DEBUG_OUTPUT (
			    dbglog (SVR_ERROR, DBG_STUN, "send_STUN_pack_and_check_response()->getUDPSocket() has returned %d\n", sock);
			 )
		    pthread_mutex_unlock(&udpSockMutex);
	    delete_STUN_binary_pack(requestBinaryPack);
	    return NULL;
    }


#if 0
    /*Commented this code due to XITHREE-4291 */
    if ((hostEnt = gethostbyname(stun_host)) == NULL) {
    	DEBUG_OUTPUT (
    			dbglog (SVR_ERROR, DBG_STUN, "send_STUN_pack_and_check_response->gethostbyname() No such host: \"%s\" h_errno=%d\n", stun_host, h_errno);
    	)
    	pthread_mutex_unlock(&udpSockMutex);
		delete_STUN_binary_pack(requestBinaryPack);
		return NULL;
    }
#endif

    /* (Fix:XITHREE-4291) Implemented to handle multiple host entries using getaddrinfo instead of gethostbyname*/
    if ((err = getaddrinfo(stun_host, NULL, &hints, &res)) != 0) 
    {
	    DEBUG_OUTPUT (
			    dbglog (SVR_ERROR, DBG_STUN, "send_STUN_pack_and_check_response->getaddrinfo(): %s \"%s\" errno =%d\n", gai_strerror(err), stun_host, err);
			 )
	    pthread_mutex_unlock(&udpSockMutex);
	    delete_STUN_binary_pack(requestBinaryPack);
	    return NULL;
    }
    else
    {
	    for(p = res; p != NULL; p = p->ai_next) {
		    if(((struct sockaddr_in *)(p->ai_addr))->sin_addr.s_addr)
			    break;
		    else
			    continue;
	    }
    }

	
    if (stun_port != 0)
    	port = stun_port;
    /* Commented dur to XITHREE-4291, use next line
    //srvAddr.sin_addr = *((struct in_addr *) hostEnt->h_addr);
    */
    srvAddr.sin_addr.s_addr = ((struct sockaddr_in *)(p->ai_addr))->sin_addr.s_addr;

    srvAddr.sin_family = AF_INET;
    srvAddr.sin_port = htons(port);
    memset(&(srvAddr.sin_zero), 0, 8);

	// clean socket in_buffer
    timeout.tv_sec  = 0;
	timeout.tv_usec = 100; // 100 Microseconds
	ret = setsockopt( sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
	if (ret < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_STUN,"send_STUN_pack_and_check_response()->setsockopt() has returned %d, errno = %d (%s)\n", ret, errno, strerror(errno));
		)
		closeUDPSocket();
		pthread_mutex_unlock(&udpSockMutex);
		delete_STUN_binary_pack(requestBinaryPack);
		return NULL;
	}
	bytesAmount = recvfrom(sock, buf, sizeof(buf), 0, NULL, NULL); // clean socket in_buffer
	while ( bytesAmount > 0 )
		bytesAmount = recvfrom(sock, buf, sizeof(buf), 0, NULL, NULL);


	// sending and receiving of binary data (if no response, make several attempts):
	for ( i=0; i<BINDING_REQUEST_RETRANSMISSION_COUNT && !cancelStunThreadActivated; i++)
	{
		ret = setsockopt( sock, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *) &(timeouts[i]), sizeof(struct timeval));
		if (ret < 0)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN,"send_STUN_pack_and_check_response()->setsockopt() has returned %d, errno = %d (%s)\n", ret, errno, strerror(errno));
			)
			closeUDPSocket();
			pthread_mutex_unlock(&udpSockMutex);
			delete_STUN_binary_pack(requestBinaryPack);
			freeaddrinfo(res);
			return NULL;
		}

		/* Sent STUN query to server and expect immediate response back*/
		sendto(sock, requestBinaryPack->content, requestBinaryPack->length, 0,
				(struct sockaddr *) &srvAddr,sizeof(struct sockaddr));


		memset(buf, 0, sizeof(buf));
		bytesAmount = udpSocketIsShutdown ? 0 : recvfrom(sock, buf, sizeof(buf), 0, 0, 0);

		if(bytesAmount > 0)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_STUN,
							"send_STUN_pack_and_check_response() Response from: %s, port %d, bytesAmount = %d\n",inet_ntoa(srvAddr.sin_addr), ntohs(srvAddr.sin_port), bytesAmount);
			)
			break;
		}

		if (bytesAmount<=0 && (udpSocketIsShutdown || !udpSocketIsShutdown))
		{
			DEBUG_OUTPUT (
					dbglog (SVR_WARN, DBG_STUN,
							"send_STUN_pack_and_check_response->recvfrom(attempt %d) FAIL because shutdown was executed for socket, bytesAmount = %d, errno = %d (%s), h_errno = %d\n", i+1, bytesAmount, errno, strerror(errno), h_errno);
			)
			if(!udpSocketIsShutdown)
			    shutdownUdpCRSocket();
			closeUDPSocket();
			pthread_mutex_unlock(&udpSockMutex);
			delete_STUN_binary_pack(requestBinaryPack);
			freeaddrinfo(res);
			return NULL;
		}

		DEBUG_OUTPUT (
				dbglog (SVR_DEBUG, DBG_STUN,
							"send_STUN_pack_and_check_response->recvfrom(attempt %d) FAIL, bytesAmount = %d, errno = %d (%s), h_errno = %d\n", i+1, bytesAmount, errno, strerror(errno), h_errno);
				)
	}
	
	if(res) {
		freeaddrinfo(res);
		res = NULL;
	}

	delete_STUN_binary_pack(requestBinaryPack);

	timeout.tv_sec  = UDP_CR_RECV_TIMEOUT;
	timeout.tv_usec = 0;
	ret = setsockopt( sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
	if (ret < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_STUN,"send_STUN_pack_and_check_response()->setsockopt() has returned %d, errno = %d (%s)\n", ret, errno, strerror(errno));
		)
		closeUDPSocket();
		pthread_mutex_unlock(&udpSockMutex);
		return NULL;
	}

	pthread_mutex_unlock(&udpSockMutex);

	if (!cancelStunThreadActivated  &&  i < BINDING_REQUEST_RETRANSMISSION_COUNT) // when wasn't make >9 attempts for binding request.
	{
		/* Parse received packet as per STUN */
		responsePack = convert_binary_to_STUN_pack(buf, bytesAmount);
		if (!responsePack)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN,"send_STUN_pack_and_check_response()->convert_binary_to_STUN_pack() has returned NULL value.\n");
			)
			return NULL;
		}
		// tids compare
		if ( (ret = tid_compare(requestPack->tid, responsePack->tid)) != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN,"send_STUN_pack_and_check_response()->tid_compare(): has returned result %d. requestPack->tid != responsePack->tid.\n", ret);
			)

			delete_STUN_pack(responsePack);
			return NULL;
		}

		return responsePack;
	}

	return NULL;
}

void setCancelStunThreadActivated(int value)
{
	cancelStunThreadActivated = value;
}

#endif /* WITH_STUN_CLIENT */
