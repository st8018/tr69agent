/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
//Created: San, june 2011
//Last modified: San, june 2011

#ifndef PACKET_H
#define PACKET_H

#include "dimark_globals.h"

#ifdef WITH_STUN_CLIENT

//Message types:
#define STUN_MT_BINDING_REQ 			0x0001
#define STUN_MT_BINDING_RES 			0x0101
#define STUN_MT_BINDING_ERR_RES 		0x0111
#define STUN_MT_SHARED_SECRET_REQ 		0x0002
#define STUN_MT_SHARED_SECRET_RES 		0x0102
#define STUN_MT_SHARED_SECRET_ERR_RES 	0x0112

#define STUN_ERR_BAD_REQUEST 		400
#define STUN_ERR_UNKNOWN_ATTRIBUTE 	420

typedef unsigned char tid_t[16];

struct STUN_attribute_list {
    int unkn_attr_count;
    u_int16_t *attributes;
};

struct STUN_string {
	int length;
	char *content;
};

struct STUN_address {
	u_int32_t IP_address;
    u_int16_t port;
    u_int8_t family;
};

struct STUN_pack {
    u_int16_t type;
    tid_t tid;
    u_int16_t binding_change_attr;
    u_int16_t connection_request_binding_attr;
    struct STUN_attribute_list *unknown_attributes;
    struct STUN_address *mapped_addr;
    struct STUN_address *response_addr;
    struct STUN_address *source_addr;
    struct STUN_address *changed_addr;
    struct STUN_string *username;
    struct STUN_string *password;
    struct STUN_address *reflected_from;
};

struct STUN_binary_pack {
	unsigned char *content;
    int length;
};

// Create functions:
struct STUN_pack *new_STUN_pack(u_int16_t, tid_t);
void new_tid(tid_t);
// Delete functions:
void delete_STUN_pack(struct STUN_pack *);
void delete_STUN_binary_pack(struct STUN_binary_pack *);
// Convert functions:
struct STUN_binary_pack *convert_STUN_pack_to_binary(struct STUN_pack *);
struct STUN_pack *convert_binary_to_STUN_pack(unsigned char *, int );
int stun_address_serialise(unsigned char ** , int * , struct STUN_address *, uint16_t );
int stun_string_serialise(unsigned char ** , int * , struct STUN_string *, uint16_t );
int stun_attribute_list_serialise(unsigned char ** , int * , struct STUN_attribute_list *);
int connection_request_binding_attr_serialise(unsigned char **,  int *);
int binding_change_attr_serialise(unsigned char **, int *);
// Add attributes functions:
int add_unknown_attribute_to_STUN_pack(struct STUN_pack *, u_int16_t);
int add_username_to_STUN_pack(struct STUN_pack *, char *);
int add_password_to_STUN_pack(struct STUN_pack *, char *);
int add_binding_change_attr_to_STUN_pack(struct STUN_pack *);
int add_connection_request_binding_attr_to_STUN_pack(struct STUN_pack *);
int add_mapped_address_to_STUN_pack(struct STUN_pack *, u_int32_t , u_int16_t );
int add_response_address_to_STUN_pack(struct STUN_pack *, u_int32_t , u_int16_t );
int add_changed_address_to_STUN_pack(struct STUN_pack *, u_int32_t , u_int16_t );
int add_source_address_to_STUN_pack(struct STUN_pack *, u_int32_t , u_int16_t );
int add_reflected_from_to_STUN_pack(struct STUN_pack *, u_int32_t , u_int16_t );

#endif /* WITH_STUN_CLIENT */

#endif /* PACKET_H */
