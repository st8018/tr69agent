/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
//Created: San, june 2011
//Last modified: San, Sep 2011

#ifndef __stun_h__
#define __stun_h__

#include "dimark_globals.h"

#ifdef WITH_STUN_CLIENT

#define DEFAULT_STUN_SERVER_UDP_PORT	3478
#define DEFAULT_STUN_SERVER_UDP_ADDR	"test.dimark.com"

int bindingRequest( char *, unsigned int, char *, char *, char *,int * );
int bindingChange( char *, unsigned int, char *, char *);// Creating of binding_change_packet

void setCancelStunThreadActivated(int );

#endif /* WITH_STUN_CLIENT */

#endif /* __stun_h__ */


