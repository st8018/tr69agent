#!/bin/sh

set -x

###########################################################################
#   Original code ©Copyright (C) 2004-2013 by Dimark Software Inc.        #
#   support@dimark.com                                                    #
#   Modifications © 2013 Comcast Cable Communications, LLC                #
###########################################################################
. /etc/common.properties
#. $UTILITY_PATH/utils.sh
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:/usr/lib:/lib

getIPAddress() {
    #if=`route | grep default | head -n 1 | awk '{print $8}'`
    if="eth1"
    if [ -n "$if" ]; then
        addr=`cat /proc/net/if_inet6 | grep $if | awk '{if ($4 == 0) { for (i = 1; i < 32; i+=4) printf("%s%s",substr($1,i,4), i>28? "\n":":");}}' | head -n 1`
    fi
    if [ -z "$addr" ]; then
        addr="::"
    fi
    echo $addr
}

# To some test requirements you can use command line parameters when start start.sh script:
# boot - restart Dimclient without persistent storage cleaning. It option can not be used if Dimclient was not started before at least 1 time.
# valgrind - test of process memory usage by Valgrind. Note: Valgrind should be already instaled in your OS.
# one - start dimclient process without automatic restarting at Reboot or FactoryReset. If Reboot or FactoryReset will occur the Dimclient process will be stoped but nor restarted.
#
# Possible combinations of options are:
#  start.sh boot
#  start.sh valgrind
#  start.sh one
#  start.sh boot valgrind
#  start.sh boot one
#
# Note: These options can be used for testing of Dimclient only.

#if [[ $# > 2 ]] ; then
#	echo "$0: Too many parameters in the command line."
#	exit 1
#fi

/usr/bin/killall dimclient

sleep 5 

#Path of tr69 binaries and files                           
tr69dir=/usr/bin 
#port used for tr69 services
tr69ServicePort=7545
BOOTSTRAP_FLAG="/opt/persistent/tr69bootstrap.dat"
#CDIR="./tmp"
CDIR="/opt/tr69agent-db/tmp"

mkdir -p /opt/persistent

if [ ! -d $CDIR ]; then
 mkdir -p $CDIR
fi
# Check for NTP sync.
starttime="20130101"

arg1=$1
arg2=$2

if [ $arg1 == "boot" ] || [ $arg2 == "boot" ] ; then
   if [ ! -f $BOOTSTRAP_FLAG ]; then
      arg1=""
      arg2=""
   elif [ -f $BOOTSTRAP_FLAG ] && [ -z "$(ls $CDIR/)" ]; then
      arg1=""
      arg2=""
   fi
fi


homedir=`pwd`
echo "$0: homedir = $homedir"

if [[ $arg1 != "boot" &&  $arg2 != "boot" ]] ; then
	# Check to see if $CDIR exists
	if   [ -d $CDIR ] ; then
		echo $0: -n ""
	elif [ -e $CDIR ] ; then
   		rm -rf $CDIR
		mkdir  $CDIR
	else
		mkdir  $CDIR
	fi

	if cd $CDIR ; then
    	echo $0: cd $CDIR
	else
    	echo $0: could not cd to $CDIR
    	exit 1
	fi

	rm -f *.dat tmp.param parameters.db

	for dir in data filetrans duentries sientries options parameter ; do

	if [ -d $dir ] ; then
    	rm -f ${dir}/*
	elif [ -e $dir ] ; then
    	rm -rf $dir
    	mkdir $dir   > /dev/null 2>&1
  	else
    	mkdir $dir   > /dev/null 2>&1
	fi
	mkdir $dir   > /dev/null 2>&1

	done

	cd "$homedir"
	rm -f *.log *.log.*


#	./conv-util data-model.xml > $CDIR/tmp.param
	#cp -f dps.param $CDIR/tmp.param
        $tr69dir/conv-util /etc/data-model.xml > $CDIR/tmp.param

	CONV_UTIL_RESULT=$?
	if test $CONV_UTIL_RESULT -ne 0 ; then
    	echo "$0: 'conv-util' application has returned an error: $CONV_UTIL_RESULT. Exit from script!!!"
    	exit 1
	else
    	echo "$0: 'conv-util' application has returned a successful result: $CONV_UTIL_RESULT."
	fi

	echo "$0: Starting Dimclient with BOOTSTRAP..."
else
    echo "$0: 'boot' command line parameter is found. Starting Dimclient with BOOT, without BOOTSTRAP..."
fi

stbIp=`getIPAddress` # default to MOCA address

while [ $stbIp == "::" ]                                                                         
do                                                                                          
 stbIp=`getIPAddress`
  sleep 5;                                                                                  
  echo MoCA ipaddress is not assigned, waiting to get the STB Ipaddress $stbIp              
done                                                                                       
                                                                                            
echo Got IP address $stbIp                                                               
echo Starting tr69 Client.  

if [ -f  /opt/tr69agent-db/log.config ]; then
   logfile=/opt/tr69agent-db/log.config
else
   logfile=/etc/log.config 
fi


if [[ $1 == "valgrind" ||  $2 == "valgrind" ]] ; then
  valgrind --leak-resolution=high --show-reachable=no --leak-check=full --track-origins=yes --log-file=valgrind_result.txt ./dimclient -b8080 -t$CDIR -cdimclient.conf -llog.config
else
  if [[ $1 == "one" || $2 == "one" ]] ; then
    #./dimclient -b8080 -t$CDIR -cdimclient.conf -llog.config #-B192.168.15.28
    $tr69dir/dimclient -b8080 -t$CDIR -c/etc/dimclient.conf -l$logfile -B0.0.0.0
  else
   # until ./dimclient --base_port=8080 --temp_dir=$CDIR --config_file=dimclient.conf --log_conf_file=log.config #--binding_IP=192.168.15.28
    until $tr69dir/dimclient --base_port=$tr69ServicePort --temp_dir=$CDIR --config_file=/etc/dimclient.conf --log_conf_file=$logfile --binding_IP=[$stbIp] #--binding_IP=0.0.0.0
    do
      echo "$0: Go to the system to boot. Start to boot..."
    done
  fi
fi
