/***************************************************************************
 *    Original code © Copyright (C) 2004-2012 by Dimark Software Inc.      *
 *    support@dimark.com                                                   *
 *    Modifications © 2013 Comcast Cable Communications, LLC               *
 ***************************************************************************/

/* TCP client in the Internet domain */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define  GET_CMD_STR  "get"
#define  SET_CMD_STR  "set"
#define  ADD_CMD_STR  "add"
#define  DEL_CMD_STR  "del"

/* Definition of all ErrorCodes */
#define OK                                         	0
#define ERR_METHOD_NOT_SUPPORTED                   	9000
#define ERR_REQUEST_DENIED                         	9001
#define ERR_INTERNAL_ERROR                         	9002
#define ERR_INVALID_ARGUMENT                       	9003
#define ERR_RESOURCE_EXCEEDED                      	9004
#define ERR_INVALID_PARAMETER_NAME                 	9005
#define ERR_INVALID_PARAMETER_TYPE                 	9006
#define ERR_INVALID_PARAMETER_VALUE                	9007
#define ERR_READONLY_PARAMETER                     	9008
#define ERR_NOTIFICATION_REQ_REJECT                	9009
#define ERR_DOWNLOAD_FAILURE                       	9010
#define ERR_UPLOAD_FAILURE                         	9011
#define ERR_TRANS_AUTH_FAILURE                     	9012
#define ERR_NO_TRANS_PROTOCOL                      	9013
#define ERR_JOIN_MULTICAST_GROUP                   	9014
#define ERR_CONTACT_FILE_SERVER                    	9015
#define ERR_ACCESS_FILE                            	9016
#define ERR_COMPLETE_DOWNLOAD                      	9017
#define ERR_CORRUPTED                              	9018
#define ERR_FILE_AUTHENTICATION                    	9019
#define ERR_COMPLETE_DOWNLOAD_TIME_WINDOWS         	9020
#define ERR_CANCELATION_NOT_PERMITTED              	9021
#define ERR_INVALID_UUID_FORMAT                    	9022
#define ERR_UNKNOWN_EXEC_ENVIRONMENT               	9023
#define ERR_DISABLE_EXEC_ENVIRONMENT               	9024
#define ERR_DU_EXEC_ENVIRONMENT_MISHMATCH          	9025
#define ERR_DUPLICATE_DU                           	9026
#define ERR_SYSTEM_RESOURCES_EXCEEDED              	9027
#define ERR_UNKNOWN_DU                          	9028
#define ERR_INVALID_DU_STATE               	        9029
#define ERR_INVALID_DU_UPDATE_DOWNGRADE_NOT_PERMITTED   9030
#define ERR_INVALID_DU_UPDATE_VERSION_NOT_SPECIFIED     9031
#define ERR_INVALID_DU_UPDATE_VERSION_ALREADY_EXISTS    9032
#define ERR_NO_INFORM_DONE                         	9890
#define ERR_DIM_EVENT_WRITE                        	9800
#define ERR_DIM_EVENT_READ                         	9801
#define ERR_DIM_MARKER_OP                          	9805
#define ERR_DIM_TRANSFERLIST_WRITE                 	9810
#define ERR_DIM_TRANSFERLIST_READ                  	9811
#define ERR_INVALID_OPTION_NAME                    	9820
#define ERR_CANT_DELETE_OPTION                     	9821
#define ERR_READ_OPTION                            	9822
#define ERR_WRITE_OPTION                           	9823
#define ERR_READ_PARAMFILE                         	9830
#define ERR_WRITE_PARAMFILE                        	9831

typedef struct faultMessage
{
    int     faultCode;
    char    *faultString;
} FaultMessage;

static FaultMessage faultMessages[] =
{
    { ERR_METHOD_NOT_SUPPORTED, "Method not Supported" },
    { ERR_REQUEST_DENIED, "Request Denied" },
    { ERR_INTERNAL_ERROR, "Internal Error" },
    { ERR_INVALID_ARGUMENT, "Invalid Arguments" },
    { ERR_RESOURCE_EXCEEDED, "Resources exceeded" },
    { ERR_INVALID_PARAMETER_NAME, "Invalid Parameter Name" },
    { ERR_INVALID_PARAMETER_TYPE, "Invalid Parameter Type" },
    { ERR_INVALID_PARAMETER_VALUE, "Invalid Parameter Value" },
    { ERR_READONLY_PARAMETER, "Attempt to set a non-writable parameter" },
    { ERR_NOTIFICATION_REQ_REJECT, "Notification request rejected" },
    { ERR_DOWNLOAD_FAILURE, "Download/ScheduleDownload failure" },
    { ERR_UPLOAD_FAILURE, "Upload failure" },
    { ERR_TRANS_AUTH_FAILURE, "File transfer server authentication failure" },
    { ERR_NO_TRANS_PROTOCOL, "Unsupported protocol for file transfer" },
    { ERR_JOIN_MULTICAST_GROUP,     "File transfer failure: unable to join multicast group"},
    { ERR_CONTACT_FILE_SERVER, "File transfer failure: unable to contact file server "},
    { ERR_ACCESS_FILE, "File transfer failure: unable to access file"},
    { ERR_COMPLETE_DOWNLOAD, "File transfer failure: unable to complete download "},
    { ERR_CORRUPTED, "File transfer failure: file corrupted or otherwise unusable"},
    { ERR_FILE_AUTHENTICATION, "File transfer failure: file authentication failure"},
    { ERR_COMPLETE_DOWNLOAD_TIME_WINDOWS, "File transfer failure: unable to complete download within specified time windows"},
    { ERR_CANCELATION_NOT_PERMITTED, "Cancelation of file transfer not permitted in current transfer state"},
    { ERR_INVALID_UUID_FORMAT, "Invalid UUID format"},
    { ERR_UNKNOWN_EXEC_ENVIRONMENT, "Unknown Execution Environment"},
    { ERR_DISABLE_EXEC_ENVIRONMENT, "Disabled Execution Environment"},
    { ERR_DU_EXEC_ENVIRONMENT_MISHMATCH, "Deployment Unit to Execution Environment Mismatch"},
    { ERR_DUPLICATE_DU, "Duplicate Deployment Unit"},
    { ERR_SYSTEM_RESOURCES_EXCEEDED, "System Resources Exceeded"},
    { ERR_UNKNOWN_DU, "Unknown Deployment Unit"},
    { ERR_INVALID_DU_STATE, "Invalid Deployment Unit State"},
    { ERR_INVALID_DU_UPDATE_DOWNGRADE_NOT_PERMITTED, "Invalid Deployment Unit Update - Downgrade not permitted"},
    { ERR_INVALID_DU_UPDATE_VERSION_NOT_SPECIFIED, "Invalid Deployment Unit Update - Version not specified"},
    { ERR_INVALID_DU_UPDATE_VERSION_ALREADY_EXISTS, "Invalid Deployment Unit Update - Version already exists"},
    { ERR_NO_INFORM_DONE, "ERR_NO_INFORM_DONE"},
    { ERR_DIM_EVENT_WRITE, "Error during opening for writing the event storage file"},
    { ERR_DIM_EVENT_READ, "Error during opening for reading or reading the event storage"},
    { ERR_DIM_MARKER_OP, "Error during creating or deleting one of markers for detecting boot or bootstrap"},
    { ERR_DIM_TRANSFERLIST_WRITE, "Error during writing the file transfer information from or into the storage"},
    { ERR_DIM_TRANSFERLIST_READ,  "Error during reading the file transfer information from or into the storage"},
    { ERR_INVALID_OPTION_NAME, "ERR_INVALID_OPTION_NAME"},
    { ERR_CANT_DELETE_OPTION, "ERR_CANT_DELETE_OPTION"},
    { ERR_READ_OPTION,"ERR_READ_OPTION"},
    { ERR_WRITE_OPTION, "ERR_WRITE_OPTION"},
    { ERR_READ_PARAMFILE, "Error reading initial parameter file or data from storage or parameter metadata from storage"},
    { ERR_WRITE_PARAMFILE, "Error during writing data into storage or parameter metadata into storage "}
};

static int faultMessagesSize = sizeof( faultMessages ) / sizeof( FaultMessage );

static int sock;

static int readresult( int );
static int writeline( int, const char * );
static int readline( int, char *, int );
static int get_char( int );
static int sockconnect( const char *, const char * );
static void errorexit( const char * );
static void errormsg( const char * );
static char * getFaultString( int faultCode );

char cmd[4] = {'\0'};

void usage()
{
    printf("\nUsage: ./host-if [-H <hostname>] [-p <port>] [-adgs]<object/parameter> [-v]<value>\n");
    printf("\t-h   Help\n");
    printf("\t-a   add operation\n");
    printf("\t-d   delete operation\n");
    printf("\t-g   get operation\n");
    printf("\t-s   set operation\n");
    printf("\t-v   set value\n");
    printf("\t-H   hostname [default: 0.0.0.0]\n");
    printf("\t-p   port number [default: 8081]\n");
    printf("\nGet parameter example: \n");
    printf("./host-if [-H <hostname>] [-p <port>] -g \"Device.MoCA.Interface.1.Status\"   OR\n");
    printf("./host-if -g \"Device.MoCA.Interface.1.Status\"\n");
    printf("\nSet parameter example: \n");
    printf("./host-if [-H <hostname>] [-p <port>] -s \"Device.IP.Interface.1.Enable\" -v 0   OR\n");
    printf("./host-if -s \"Device.IP.Interface.1.Enable\" -v 0\n");
    printf("\nAdd object example: \n");
    printf("./host-if [-H <hostname>] [-p <port>] -a \"Device.Ethernet.Interface.\"   OR\n");
    printf("./host-if -a \"Device.Ethernet.Interface.\"\n");
    printf("\nDelete object example:\n");
    printf("./host-if [-H <hostname>] [-p <port>] -d \"Device.Ethernet.Interface.1.\"   OR\n");
    printf("./host-if -d \"Device.Ethernet.Interface.1.\"\n");
    printf("\n");
}

int main(int argc, char *argv[])
{
    int n;
    int ch = EOF;
    char hostname[16] = {'\0'};
    char port[6] = {'\0'};
    char parm[1024] = {'\0'};
    char val[1024] = {'\0'};

    while((ch = getopt(argc, argv, "a:d:g:s:v:p:H:")) != EOF)
    {
        switch (ch)
        {
        case 'a':
            strncpy(cmd, ADD_CMD_STR, sizeof(cmd)-1 );
            strncpy(parm, optarg, sizeof(parm)-1 );
            break;
        case 'd':
            strncpy(cmd, DEL_CMD_STR, sizeof(cmd)-1 );
            strncpy(parm, optarg, sizeof(parm)-1 );
            break;
        case 'g':
            strncpy(cmd, GET_CMD_STR, sizeof(cmd)-1 );
            strncpy(parm, optarg, sizeof(parm)-1 );
            break;
        case 's':
            strncpy(cmd, SET_CMD_STR, sizeof(cmd)-1 );
            strncpy(parm, optarg, sizeof(parm)-1 );
            break;
        case 'v':
            strncpy(val, optarg, sizeof(val)-1 );
            break;
        case 'H':
            strncpy(hostname, optarg, sizeof(hostname)-1 );
            break;
        case 'p':
            strncpy(port, optarg, sizeof(port)-1 );
            break;

        case 'h':
        case '?':
        default:
            usage();
            exit (0);
        }
    }

    if((hostname[0] == '\0') || (port[0] == '\0'))
    {
        strncpy(hostname, "0.0.0.0", sizeof(hostname)-1);
        strncpy(port, "8081", sizeof(port)-1);
    }

    if (parm[0] == '\0')
    {
        usage();
        exit (0);
    }

    if(!strcmp(cmd, SET_CMD_STR) && (val[0] == '\0'))
    {
        printf("\nError: Missing input value in SET command\n\n");
        usage();
        exit (0);
    }

    if (( sock = sockconnect( hostname, port )) < 0 )
        errorexit( "socket connect");

    printf("\n---------- HOST-IF Input ------------------\n");
    printf("\nCommand     : \"%s\"\n", cmd);
    n = writeline(sock, cmd);
    if (n < 0) {
        close(sock);
        errorexit("Error sending cmd\n");
    }
    printf("\nParameter   : \"%s\"\n", parm);
    n = writeline(sock, parm);
    if (n < 0) {
        close(sock);
        errorexit("Error sending parameter\n");
    }

    if(!strcmp(cmd, SET_CMD_STR)) {
        printf("\nInput Value : \"%s\"\n", val);
        n = writeline(sock, val);
        if (n < 0) {
            close(sock);
            errorexit("Error sending value\n");
        }
    }

    printf ("\n---------- CLIENT Response ----------------\n");
    n = readresult (sock);
    printf ("\n");
    close (sock);

    exit(0);
}

static int
readresult( int sock )
{
    char rcvBuf[256] = {'\0'};
    char i8Result[256] = {'\0'};
    int n;
    int line = 0;
    const char *fault;
    int errCode = OK;

    do
    {
        n = readline(sock,rcvBuf,255);
        if (n < 0) {
            errormsg("\nRecv Error  ");
            return n;
        }

        if(rcvBuf[0] != '\0')
        {
            strncpy(i8Result,rcvBuf,255);
        }
        line++;
    }
    while(n>0 && rcvBuf[0] != '\0');

    if (line == 2) {
        errCode =  atoi(i8Result);
        fault = getFaultString( errCode );
    }

    if(!strcmp(cmd, SET_CMD_STR) || !strcmp(cmd, DEL_CMD_STR)) {
        if(OK == errCode)
            printf("\nResponse    : OK\n");
        else if(ERR_METHOD_NOT_SUPPORTED <= errCode)
        {
            printf("\nResponse    : %s\n", fault);
        }
    }
    else if (!strcmp(cmd, GET_CMD_STR)) {
        if(line == 2) {
            if(ERR_METHOD_NOT_SUPPORTED <= errCode) {
                printf("\nResponse    : %s\n", fault);
            }
            else
                printf("\nResponse    : \n");
        }
        else
            printf("\nValue       : \"%s\"\n", i8Result);
    }
    else if (!strcmp(cmd, ADD_CMD_STR)) {
        if(OK != errCode) {
            printf("\nResponse    : OK\n");
            printf("Instance    : %s\n", i8Result);
        }
        else
            printf("\nResponse    : NOK\n");
    }

    return n;
}

static int
writeline( int sock, const char *data )
{
    int ret = 0;
    int n = 0, size = strlen(data);
    while (n < size)
    {
        ret = write( sock, ((void *)data) + n, size - n );
        if (ret < 0)
            return ret;
        n += ret;
    }
    ret = write( sock, "\n", 1 ); //ret = write( sock, "\r\n", 2 );
    return (ret < 0) ? ret : ret + n;
}

static int
readline( int fd, char *buf, int len )
{
    int i = len;
    int c;

    bzero(buf, len);
    while( --i > 0 ) {
        c = get_char(fd);
        if ((char)c == '\r' || (char)c == '\n')
            break;
        if ((int)c == EOF)
            return EOF;
        *buf++ = (char)c;
    }
    if (c != '\n')
        c = get_char(fd); /* got \r, now get \n */
    if (c == '\n')
        *buf = '\0';
    else if ((int)c == EOF)
        return EOF;

    return i;
}

static int
get_char(int fd )
{
    char c;

    if ( read(fd, &c, sizeof(c)) <= 0 )
        return EOF;
    else
        return (int)c;
}

static int
sockconnect(const char *hostname, const char *port)
{
    int sock;
    struct sockaddr_in server;
    struct timeval timeout;
    struct hostent *hp;

    sock= socket(PF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        errormsg("socket\n");
        return sock;
    }
    timeout.tv_sec = 5;
    timeout.tv_usec = 0;
    setsockopt( sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof( timeout ));

    hp = gethostbyname(hostname);            /* Server IP Address */
    if (hp==0) {
        errormsg("Unknown host\n");
        return -1;
    }

    bzero((char*)&server, sizeof( server));
    server.sin_family = AF_INET;
    memcpy((char *)&server.sin_addr, (char *)hp->h_addr, hp->h_length);
    server.sin_port = htons(atoi(port));  /* Destination Port */
    //length=sizeof(struct sockaddr_in);

    if( connect(sock, (struct sockaddr *)&server, sizeof(struct sockaddr_in)) < 0 ) {
        errormsg( "Error connect");
        return -1;
    }
    return sock;
}

void errorexit( const char *msg )
{
    errormsg(msg);
    exit(0);
}

static void
errormsg( const char *msg )
{
    perror (msg);
}

char *
getFaultString( int faultCode )
{
    register int i = 0;
    for ( i = 0; i != faultMessagesSize; i ++ )
    {
        if ( faultMessages[i].faultCode == faultCode )
            return faultMessages[i].faultString;
    }

    return NULL;
}
