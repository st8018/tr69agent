/***************************************************************************
 Copyright (C) 1998-2012 Daniel Veillard.  All Rights Reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is fur-
nished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FIT-
NESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

 Copyright (C) 2004-2014 by Dimark Software Inc.
 support@dimark.com
 ***************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <libxml/xmlreader.h>
#include "soapStub.h"
#include "src/parameter.h"

#define	MAX_PARAM_VALUE_SIZE	61430
#define MAX_INT_INDEX_SIZE		11
#define MAX_NoE_ATTR_SIZE		256

/* Kind	of Parameter.
 * This enum must be a copy from parameter.h file
 * Following lines are commented because #include "src/parameter.h" is used.
 */
//typedef	enum parameterType
//{
//	DefaultType			= /*97*//*98*/99,
//	ObjectType			= /*98*//*99*/100,								/* old */ /*new+1*/
//	MultiObjectType		= /*99*//*100*/101,
//	StringType			= SOAP_TYPE_xsd__string,						/*  6  */ /*  7  */ /* 11  */
//	DefStringType		= (DefaultType + SOAP_TYPE_xsd__string),		/* 103 */ /* 104 */ /* 110 */
//	IntegerType			= SOAP_TYPE_xsd__int,							/*  7  */ /*  8  */ /* 12  */
//	DefIntegerType		= (DefaultType + SOAP_TYPE_xsd__int),			/* 104 */ /* 105 */ /* 111 */
//	UnsignedIntType		= SOAP_TYPE_xsd__unsignedInt,					/*  9  */ /*  10 */ /* 14  */
//	DefUnsignedIntType	= (DefaultType + SOAP_TYPE_xsd__unsignedInt),	/* 106 */ /* 107 */ /* 113 */
//	BooleanType			= SOAP_TYPE_xsd__boolean,						/*  18 */ /*  19 */ /* 21  */ /* 16 */
//	DefBooleanType		= (DefaultType + SOAP_TYPE_xsd__boolean),		/* 115 */ /* 116 */ /* 120 */ /* 115*/
//	DateTimeType		= SOAP_TYPE_xsd__dateTime,						/*  11 */ /*  12 */ /* 10  */
//	DefDateTimeType		= (DefaultType + SOAP_TYPE_xsd__dateTime),		/* 108 */ /* 109 */ /* 109 */
//	Base64Type			= SOAP_TYPE_xsd__base64,						/*  12 */ /*  13 */ /* 15  */
//	DefBase64Type		= (DefaultType + SOAP_TYPE_xsd__base64),		/* 109 */ /* 110 */ /* 114 */
//	UnsignedLongType	= SOAP_TYPE_xsd__unsignedLong,					/*  -- */ /*  -- */ /* 25  */ /* 20 */
//	DefUnsignedLongType	= (DefaultType + SOAP_TYPE_xsd__unsignedLong),	/*  -- */ /*  -- */ /* 124 */ /* 119*/
//	hexBinaryType		= SOAP_TYPE_xsd__hexBinary,						/*  -- */ /*  -- */ /* 26  */ /* 21 */
//	DefHexBinaryType	= (DefaultType + SOAP_TYPE_xsd__hexBinary),		/*  -- */ /*  -- */ /* 125 */ /* 120*/
//	// MIN value from DefStringType, DefIntegerType, DefUnsignedIntType, DefBooleanType, DefDateTimeType, DefBase64Type:
//	// !!!!! Manual filling is necessary !!!!!
//	minDefType			= DefDateTimeType // Is DefDateTimeType now.
//} ParameterType;

static char	str[MAX_PARAM_VALUE_SIZE+1024] = {0};

static	int cond (char *);
static void correction (char *);
static  int processNode (xmlTextReaderPtr);
static  int streamFile (const char *);


// if type of object is equal 100 or 101+99 then return 0, else 1
// example "\n%s;100;0;0;0;0;-1;-1;-1;;"  or  "\n%.*s;101;0;0;0;0;0;0;0;;;%s;%s\n%s;99;0;0;0;0;-1;-1;-1;;"
static	int
cond (char *s)
{
	char	*tmp;
	char	*p;
	char    objectTypeStr[6];
	char    multiObjectTypeStr[6];

	if (!s)
		return 1;

	if ( (tmp = strdup(s)) == NULL)
		return 1;

	p = strtok (tmp, ";");
	p = strtok (NULL, ";");

	sprintf(objectTypeStr, "%d\0", ObjectType);
	sprintf(multiObjectTypeStr, "%d\0", MultiObjectType);

	unsigned int isNotObject = p?(strcmp (p, objectTypeStr) && strcmp (p, multiObjectTypeStr)):0;

	free(tmp);

	return isNotObject;
}

static	void
correction (char *s)
{
	char	*tmp;
	char	*p;
	int		i, contentsTheAccessList;

	if (!s)
		return;

	contentsTheAccessList = strstr(s, ";;") ? 0 : 1; // if AccessList isn't empty, then == 1.

	if ( (tmp = strdup(s)) == NULL)
		return;

	if (cond(s) && (p = strtok (s, ";")))
	{
		if (p)
			printf ("%s;", p);
		else
			printf (";");

		for (i = 0; i < 8; i++)
		{
			p = strtok (NULL, ";");
		}

		if (contentsTheAccessList)
			p = strtok (NULL, ";");

		if (p)
			printf ("%s;", p);
		else
			printf (";");

		p = strtok (tmp, ";");

		for (i = 0; i < 7; i++)
		{
			p = strtok (NULL, ";");
			printf ( "%s;", p );
		}

		if (contentsTheAccessList)
		{
			p = strtok (NULL, ";");
			printf ( "%s;", p );
		}
		else
			printf ( ";", p );

		strtok (NULL, ";");
		p = strtok (NULL, ";");

		if (p) {
			printf ( "%s", p );
		} 
	} else {
		printf("%s", str);
	}
	free(tmp);
}

static int
processNode(xmlTextReaderPtr reader)
{
    const	xmlChar *name;
    char    * tmpStr;
    char	tmp[24] = {0};
    static 	xmlChar path[256], bpath[256], param[256], bparam[256], *p;

//    static xmlChar predefinedInstances[MAX_INT_INDEX_SIZE+1];
    static xmlChar minEntry[MAX_INT_INDEX_SIZE+1];
    static xmlChar maxEntry[MAX_INT_INDEX_SIZE+1];
    static xmlChar addObjIdx[MAX_INT_INDEX_SIZE+1];
    static xmlChar delObjIdx[MAX_INT_INDEX_SIZE+1];
    static xmlChar numEntriesParameter[MAX_NoE_ATTR_SIZE+1];

    static int firstParamEntry;
    static int isOpenEntry; // if entry is open == 1, close == 0 (<entry>text</entry>)
    static int isMultiInstanceObject;

    name = xmlTextReaderConstName(reader);

    if (!strcasecmp ((char *)name, "object"))
    {
    	param[0] = '\0';
    	tmpStr = (char *)xmlTextReaderGetAttribute(reader, (xmlChar *)"base");
    	if (!tmpStr || strlen(tmpStr)>sizeof(bpath)+1 || strlen(tmpStr)==0)
    	{
    		fprintf(stderr, "ERROR: the \"base\" attribute is not found for object or is empry or overlong.\n");
    		return -1;
    	}
    	strcpy ((char *)bpath, tmpStr);

  		while ((p = (xmlChar *)strstr((char *)bpath,"{i}")))
  		{
   			strcpy ((char *)p,"0");
   			memmove (p + 1,p + 3,strlen((char *)p + 3) + 1);
  		}

  		isMultiInstanceObject = !strcmp( (char *)bpath + strlen((char *)bpath) - 3, ".0.");

  		if (isMultiInstanceObject)
  		{
  			char *tmpPath = tmpStr;
  			char *s;
  			long l;

//  			tmpStr = (char *)xmlTextReaderGetAttribute(reader, (xmlChar *)"predefinedInstances");
//  			if (!tmpStr)
//  				strcpy((char *)predefinedInstances, "0");
//  			else if (!*tmpStr || strlen(tmpStr)>MAX_INT_INDEX_SIZE)
//  			{
//  				fprintf(stderr, "ERROR: the \"predefinedInstances\" attribute is empty or overlong. Object = %s\n", tmpPath);
//  				return -1;
//  			}
// 			errno = 0;
//  			l = strtol(tmpStr, &s, 0);
//  			if (l<0 || *s != 0 || errno)
//  			{
//  				fprintf(stderr, "ERROR: the \"predefinedInstances\" attribute has a not valid value: \"%s\". Object = %s\n", tmpStr, tmpPath);
//  				return -1;
//  			}
//  			strcpy((char *)predefinedInstances, tmpStr);


  			tmpStr = (char *)xmlTextReaderGetAttribute(reader, (xmlChar *)"minEntries");
  			if (!tmpStr || strlen(tmpStr)>MAX_INT_INDEX_SIZE || strlen(tmpStr)==0)
  			{
  				fprintf(stderr, "ERROR: the \"minEntries\" attribute is not found for object or is empty or overlong. Object = %s\n", tmpPath);
  				return -1;
  			}
  			errno = 0;
  			l = strtol(tmpStr, &s, 0);
  			if (l<0 || *s != 0 || errno)
  			{
  				fprintf(stderr, "ERROR: the \"minEntries\" attribute has a not valid value: \"%s\". Object = %s\n", tmpStr, tmpPath);
  				return -1;
  			}
  			strcpy((char *)minEntry, tmpStr);


  			tmpStr = (char *)xmlTextReaderGetAttribute(reader, (xmlChar *)"maxEntries");
  			if (!tmpStr || strlen(tmpStr)>MAX_INT_INDEX_SIZE || strlen(tmpStr)==0)
  			{
  				fprintf(stderr, "ERROR: the \"maxEntries\" attribute is not found for object or is empty or overlong. Object = %s\n", tmpPath);
  				return -1;
  			}
  			if (strcasecmp(tmpStr, "unbounded"))
  			{
  				errno = 0;
  				l = strtol(tmpStr, &s, 0);
  				if (l<0 || *s != 0 || errno)
  				{
  					fprintf(stderr, "ERROR: the \"maxEntries\" attribute has a not valid value: \"%s\". Object = %s\n", tmpStr, tmpPath);
  					return -1;
  				}
  			}
  			strcpy((char *)maxEntry, tmpStr);


  			tmpStr = (char *)xmlTextReaderGetAttribute(reader, (xmlChar *)"addObjIdx");
  			if (!tmpStr || strlen(tmpStr)>MAX_INT_INDEX_SIZE || strlen(tmpStr)==0)
  			{
  				fprintf(stderr, "ERROR: the \"addObjIdx\" attribute is not found for object or is empty or overlong. Object = %s\n", tmpPath);
  				return -1;
  			}
  			errno = 0;
  			strtol(tmpStr, &s, 0);
  			if (*s != 0 || errno)
  			{
  				fprintf(stderr, "ERROR: the \"addObjIdx\" attribute has a not valid value: \"%s\". Object = %s\n", tmpStr, tmpPath);
  				return -1;
  			}
  			strcpy((char *)addObjIdx, tmpStr);


  			tmpStr = (char *)xmlTextReaderGetAttribute(reader, (xmlChar *)"delObjIdx");
  			if (!tmpStr || strlen(tmpStr)>MAX_INT_INDEX_SIZE || strlen(tmpStr)==0)
  			{
  				fprintf(stderr, "ERROR: the \"delObjIdx\" attribute is not found for object or is empty or overlong. Object = %s\n", tmpPath);
  				return -1;
  			}
  			errno = 0;
  			strtol(tmpStr, &s, 0);
  			if (*s != 0 || errno)
  			{
  				fprintf(stderr, "ERROR: the \"delObjIdx\" attribute has a not valid value: \"%s\". Object = %s\n", tmpStr, tmpPath);
  				return -1;
  			}
  			strcpy((char *)delObjIdx, tmpStr);


  			tmpStr = (char *)xmlTextReaderGetAttribute(reader, (xmlChar *)"numEntriesParameter");
  			if (!tmpStr || strlen(tmpStr)==0)
  			{
  				*numEntriesParameter = '\0';
  			}
  			else if (strlen(tmpStr)>MAX_NoE_ATTR_SIZE)
  			{
  				fprintf(stderr, "ERROR: the \"numEntriesParameter\" attribute is overlong. Object = %s\n", tmpPath);
  				return -1;
  			}
  			else
  			{
  				strcpy((char *)numEntriesParameter, tmpStr);
  			}
  		}


    	if (strcasecmp ((char *)path, (char *)bpath))
    	{
    		strcpy ((char *)path, (char *)bpath);

    		if (isMultiInstanceObject)
    		{
    			correction (str);
    			firstParamEntry = 1;
    			sprintf (str, "\n%.*s;%u;0;0;0;0;0;%s;%s;;;%s;%s;%s\n%s;%u;0;0;0;0;-1;-1;-1;;;1;1",
    					(int)strlen((char *)path) - 2, path, MultiObjectType, /*predefinedInstances,*/ addObjIdx, delObjIdx, minEntry, maxEntry, numEntriesParameter,
    					path, DefaultType);
    		}
    		else
    		{
    			correction (str);
    			firstParamEntry = 1;
    			sprintf (str, "\n%s;%u;0;0;0;0;-1;-1;-1;;;1;1", path, ObjectType);
    		}
    	}
    	return 0;
    }

    if (!strcasecmp((char *)name, "parameter"))
    {
    	long l;
    	char *s;

    	tmpStr = (char *)xmlTextReaderGetAttribute((xmlTextReaderPtr)reader, (xmlChar *)"base");
    	if (!tmpStr || strlen(tmpStr)==0)
    	{
    		fprintf(stderr, "ERROR: the \"base\" attribute is not found for parameter or is empty.\n");
    		return -1;
    	}
       	strcpy((char *)bparam, tmpStr);

       	if (strcmp((char *)param, (char *)bparam))
       	{
       		firstParamEntry = 1;
			correction (str);
       		strcpy((char *)param, (char *)bparam);

       		char *tmps1, *tmps2, *tmps3, *tmps4, *tmps5, *tmps6, *tmps7;

//       		tmps1 = xmlTextReaderGetAttribute(reader, (xmlChar *)"instance");
//  			if (!tmps1 || strlen(tmps1)>MAX_INT_INDEX_SIZE || strlen(tmps1)==0)
//  			{
//  				fprintf(stderr, "ERROR: the \"instance\" attribute is not found for object or is empty or overlong. Parameter = %s\n", tmpStr);
//  				return -1;
//  			}
//  			errno = 0;
//  			strtol(tmps1, &s, 0);
//  			if (*s != 0 || errno)
//  			{
//  				fprintf(stderr, "ERROR: the \"instance\" attribute has a not valid value: \"%s\". Parameter = %s\n", tmps1, tmpStr);
//  				return -1;
//  			}

       		tmps2 = xmlTextReaderGetAttribute(reader, (xmlChar *)"notification");
  			if (!tmps2 || strlen(tmps2)>MAX_INT_INDEX_SIZE || strlen(tmps2)==0)
  			{
  				fprintf(stderr, "ERROR: the \"notification\" attribute is not found for object or is empty or overlong. Parameter = %s\n", tmpStr);
  				return -1;
  			}
  			errno = 0;
  			l = strtol(tmps2, &s, 0);
  			if (l<0 || *s != 0 || errno)
  			{
  				fprintf(stderr, "ERROR: the \"notification\" attribute has a not valid value: \"%s\". Parameter = %s\n", tmps2, tmpStr);
  				return -1;
  			}

       		tmps3 = xmlTextReaderGetAttribute(reader, (xmlChar *)"maxNotification");
  			if (!tmps3 || strlen(tmps3)>MAX_INT_INDEX_SIZE || strlen(tmps3)==0)
  			{
  				fprintf(stderr, "ERROR: the \"maxNotification\" attribute is not found for object or is empty or overlong. Parameter = %s\n", tmpStr);
  				return -1;
  			}
  			errno = 0;
  			l = strtol(tmps3, &s, 0);
  			if (l<0 || *s != 0 || errno)
  			{
  				fprintf(stderr, "ERROR: the \"maxNotification\" attribute has a not valid value: \"%s\". Parameter = %s\n", tmps3, tmpStr);
  				return -1;
  			}

       		tmps4 = xmlTextReaderGetAttribute(reader, (xmlChar *)"rebootIdx");
  			if (!tmps4 || strlen(tmps4)>MAX_INT_INDEX_SIZE || strlen(tmps4)==0)
  			{
  				fprintf(stderr, "ERROR: the \"rebootIdx\" attribute is not found for object or is empty or overlong. Parameter = %s\n", tmpStr);
  				return -1;
  			}
  			errno = 0;
  			if (strcasecmp(tmps4, "Reboot"))
  			{
  				strtol(tmps4, &s, 0);
  				if (*s != 0 || errno)
  				{
  					fprintf(stderr, "ERROR: the \"rebootIdx\" attribute has a not valid value: \"%s\". Parameter = %s\n", tmps4, tmpStr);
  					return -1;
  				}
  			}

       		tmps5 = xmlTextReaderGetAttribute(reader, (xmlChar *)"initIdx");
  			if (!tmps5 || strlen(tmps5)>MAX_INT_INDEX_SIZE || strlen(tmps5)==0)
  			{
  				fprintf(stderr, "ERROR: the \"initIdx\" attribute is not found for object or is empty or overlong. Parameter = %s\n", tmpStr);
  				return -1;
  			}
  			errno = 0;
  			strtol(tmps5, &s, 0);
  			if (*s != 0 || errno)
  			{
  				fprintf(stderr, "ERROR: the \"initIdx\" attribute has a not valid value: \"%s\". Parameter = %s\n", tmps5, tmpStr);
  				return -1;
  			}

       		tmps6 = xmlTextReaderGetAttribute(reader, (xmlChar *)"getIdx");
  			if (!tmps6 || strlen(tmps6)>MAX_INT_INDEX_SIZE || strlen(tmps6)==0)
  			{
  				fprintf(stderr, "ERROR: the \"getIdx\" attribute is not found for object or is empty or overlong. Parameter = %s\n", tmpStr);
  				return -1;
  			}
  			errno = 0;
  			strtol(tmps6, &s, 0);
  			if (*s != 0 || errno)
  			{
  				fprintf(stderr, "ERROR: the \"getIdx\" attribute has a not valid value: \"%s\". Parameter = %s\n", tmps6, tmpStr);
  				return -1;
  			}

       		tmps7 = xmlTextReaderGetAttribute(reader, (xmlChar *)"setIdx");
  			if (!tmps7 || strlen(tmps7)>MAX_INT_INDEX_SIZE || strlen(tmps7)==0)
  			{
  				fprintf(stderr, "ERROR: the \"setIdx\" attribute is not found for object or is empty or overlong. Parameter = %s\n", tmpStr);
  				return -1;
  			}
  			errno = 0;
  			strtol(tmps7, &s, 0);
  			if (*s != 0 || errno)
  			{
  				fprintf(stderr, "ERROR: the \"setIdx\" attribute has a not valid value: \"%s\". Parameter = %s\n", tmps7, tmpStr);
  				return -1;
  			}

       		sprintf (	str, "\n%s%s;%s;%s;%s;%s;%s;%s;%s;", path, param,
											/*tmps1*/"0", tmps2, tmps3, tmps4, tmps5, tmps6, tmps7 );
       	}
       	return 0;
    }

    if (!strcasecmp((char *)name, "accessList"))
    {
    	isOpenEntry = 1;
    	firstParamEntry = 1;
    	return 0;
    }

    if (!strcasecmp((char *)name, "entry"))
    {
    	if (isOpenEntry)
    	{
    		tmpStr = (char *) xmlTextReaderReadString((xmlTextReaderPtr)reader);
        	if (!tmpStr || strlen(tmpStr)==0)
        	{
        		fprintf(stderr, "ERROR: the xmlTextReaderReadString() has returned NULL.\n");
        		return -1;
        	}

    		if (!firstParamEntry)
    			strcat (str, "|");

    		firstParamEntry = 0;

    		strcat (str, tmpStr);
    	}

    	isOpenEntry = 1 - isOpenEntry;
    	return 0;
    }

    if (!strcasecmp((char *)name, "string"))
    {
    	sprintf (tmp, ";%u;", isMultiInstanceObject ? DefStringType : StringType);
    	strcat (str, tmp);
    	return 0;
    }
    else if (!strcasecmp((char *)name, "int"))
    {
    	sprintf (tmp, ";%u;", isMultiInstanceObject ? DefIntegerType : IntegerType);
    	strcat (str, tmp);
    	return 0;
    }
    else if (!strcasecmp((char *)name, "unsignedInt"))
    {
    	sprintf (tmp, ";%u;", isMultiInstanceObject ? DefUnsignedIntType : UnsignedIntType );
    	strcat (str, tmp);
    	return 0;
    }
    else if (!strcasecmp((char *)name, "boolean"))
    {
    	sprintf (tmp, ";%u;", isMultiInstanceObject ? DefBooleanType : BooleanType);
    	strcat (str, tmp);
    	return 0;
    }
    else if (!strcasecmp((char *)name, "dateTime"))
    {
    	sprintf (tmp, ";%u;", isMultiInstanceObject ? DefDateTimeType : DateTimeType);
    	strcat (str, tmp);
    	return 0;
    }
    else if (!strcasecmp((char *)name, "base64"))
    {
    	sprintf (tmp, ";%u;", isMultiInstanceObject ? DefBase64Type : Base64Type);
    	strcat (str, tmp);
    	return 0;
    }
    else if (!strcasecmp((char *)name, "unsignedLong"))
    {
    	sprintf (tmp, ";%u;", isMultiInstanceObject ? DefUnsignedLongType : UnsignedLongType );
    	strcat (str, tmp);
    	return 0;
    }
    else if (!strcasecmp((char *)name, "hexBinary"))
    {
    	sprintf (tmp, ";%u;", isMultiInstanceObject ? DefHexBinaryType : hexBinaryType );
    	strcat (str, tmp);
    	return 0;
    }

    if( !strcasecmp((char *)name, "default") ) {
		tmpStr = (char *) xmlTextReaderGetAttribute(reader, (xmlChar *)"value");
    	if (!tmpStr)
    	{
    		fprintf(stderr, "ERROR: the \"value\" attribute is not found for \"default\" tag.\n");
    		return -1;
    	}
    	strcat (str, tmpStr);
    }

    return 0;
}

static int
streamFile(const char *filename)
{
    xmlTextReaderPtr reader;
    int ret = 0;
    int readRes = 0;

    reader = xmlReaderForFile(filename, NULL, 0);
    if (reader != NULL)
    {
    	readRes = xmlTextReaderRead(reader);
        while (readRes == 1)
        {
            if ( (ret = processNode(reader)) != 0)
            	break;
            readRes = xmlTextReaderRead(reader);
        }

        correction (str);
        printf ("\n");

        xmlFreeTextReader(reader);
        if (ret != 0 || readRes == -1)
        {
            fprintf(stderr, "ERROR: File '%s' is failed to parse!\n", filename);
            return 1;
        }
    }
    else
    {
        fprintf(stderr, "Unable to open %s\n", filename);
        return 1;
    }
    return 0;
}

int main(int argc, char **argv)
{
	int ret = 0;
    if (argc != 2)
        return 1; //return error

    /*
     * this initialize the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used.
     */
    LIBXML_TEST_VERSION

    ret = streamFile(argv[1]);

    /*
     * Cleanup function for the XML library.
     */
    xmlCleanupParser();
    /*
     * this is to debug memory for regression tests
     */
    xmlMemoryDump();
	return ret; //return error or 0
}
