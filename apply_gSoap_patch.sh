#!/bin/sh
###########################################################################
#   Copyright (C) 2004-2012 by Dimark Software Inc.                       #
#   support@dimark.com                                                    #
###########################################################################

homedir=`pwd`
echo homedir = $homedir

# apply patch
patch $homedir/gsoap/stdsoap2.c       < $homedir/patch/stdsoap2.c.diff
patch $homedir/gsoap/stdsoap2.h       < $homedir/patch/stdsoap2.h.diff
patch $homedir/gsoap/struct_timeval.c < $homedir/patch/struct_timeval.c.diff
patch $homedir/gsoap/struct_timeval.h < $homedir/patch/struct_timeval.h.diff
patch $homedir/plugin/httpda.c        < $homedir/patch/httpda.c.diff
patch $homedir/plugin/httpda.h        < $homedir/patch/httpda.h.diff
patch $homedir/plugin/md5evp.c        < $homedir/patch/md5evp.c.diff
patch $homedir/plugin/md5evp.h        < $homedir/patch/md5evp.h.diff
patch $homedir/plugin/smdevp.c        < $homedir/patch/smdevp.c.diff
patch $homedir/plugin/smdevp.h        < $homedir/patch/smdevp.h.diff
patch $homedir/plugin/threads.h       < $homedir/patch/threads.h.diff

