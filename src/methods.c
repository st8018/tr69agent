/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"
#include "serverdata.h"
#include "eventcode.h"
#include "filetransfer.h"
#include "du_transfer.h"
#include "si_transfer.h"
#include "option.h"
#include "vouchers.h"
#include "parameter.h"

extern SUPORTEDRPC suportedRPC;

#define MANDATORY_METHODS_COUNT 9
/* Array of implemented RPC Methods/Functions on the client side.
 * Methods are sorted in corresponding for CWMP version*/
static xsd__string meths[] =
{
		//since CWMP-1-0:
//9 methods are mandatory:
		"GetRPCMethods",
		"SetParameterValues",
		"GetParameterValues",
		"GetParameterNames",
		"SetParameterAttributes",
		"GetParameterAttributes",
		"AddObject",
		"DeleteObject",
		"Reboot",
#ifdef HAVE_VOUCHERS_OPTIONS
		"SetVouchers",
		"GetOptions",
#endif /* HAVE_VOUCHERS_OPTIONS */
#ifdef HAVE_FILE_DOWNLOAD
		"Download",
#endif /* HAVE_FILE_DOWNLOAD */
#ifdef HAVE_FILE_UPLOAD
		"Upload",
#endif /* HAVE_FILE_UPLOAD */
#ifdef HAVE_FACTORY_RESET
		"FactoryReset",
#endif /* HAVE_FACTORY_RESET */
#ifdef HAVE_GET_QUEUED_TRANSFERS
		"GetQueuedTransfers",
#endif /* HAVE_GET_QUEUED_TRANSFERS */
#ifdef HAVE_SCHEDULE_INFORM
		"ScheduleInform",
#endif /* HAVE_SCHEDULE_INFORM */

		//since CWMP-1-1:
#ifdef HAVE_GET_ALL_QUEUED_TRANSFERS
		"GetAllQueuedTransfers",
#endif /* HAVE_GET_ALL_QUEUED_TRANSFERS */

		//since CWMP-1-2:
#ifdef	HAVE_DEPLOYMENT_UNIT
		"ChangeDUState",
#endif	/* HAVE_DEPLOYMENT_UNIT */
#ifdef HAVE_FILE_SCHEDULE_DOWNLOAD
		"ScheduleDownload",
#endif /* HAVE_FILE_SCHEDULE_DOWNLOAD */
#ifdef HAVE_CANCEL_TRANSFER
		"CancelTransfer",
#endif	/* HAVE_CANCEL_TRANSFER */
};

/* CPE methods */

int cwmp__GetRPCMethods(struct soap *soap, struct cwmp__GetRPCMethodsResponse *response)
{
	int ret = SOAP_OK;
	long startTime = getTime ();
	int size, i, count;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter GetRPCMethods st: %ld\n", startTime);
	)
	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	size = sizeof(meths) / sizeof(xsd__string);
	xsd__string *methsTmp = emallocTemp(sizeof(meths), 0);

	for(i=0; i<MANDATORY_METHODS_COUNT; i++)
	{
		methsTmp[i] = meths[i];
	}

	count = MANDATORY_METHODS_COUNT;


	for(i=MANDATORY_METHODS_COUNT; i<size; i++)
	{
		if (!strcmp(meths[i], "GetAllQueuedTransfers"))
		{
			if (!suportedRPC.rpc.isGetAllQueuedTransfers)
				continue;
		}
		else if (!strcmp(meths[i], "ChangeDUState"))
		{
			if (!suportedRPC.rpc.isChangeDUState)
				continue;
		}
		else if (!strcmp(meths[i], "ScheduleDownload"))
		{
			if (!suportedRPC.rpc.isScheduleDownload)
				continue;
		}
		else if (!strcmp(meths[i], "CancelTransfer"))
		{
			if (!suportedRPC.rpc.isCancelTransfer)
				continue;
		}

		methsTmp[count++] = meths[i];
	}

	response->MethodList.__ptrstring = methsTmp;
	response->MethodList.__size = count;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit GetRPCMethods ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	return SOAP_OK;
}

int cwmp__SetParameterValues(struct soap *soap, struct ArrayOfParameterValueStruct *ParameterList, xsd__string ParameterKey, struct cwmp__SetParameterValuesResponse *response)
{
	int ret;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter SetParameterValues  st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = setParameters(ParameterList, &response->Status);
	if (ret == OK)
	{
		setParameter(PARAMETER_KEY, ParameterKey);
	}
	else
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT) &&
				(ret != ERR_RESOURCE_EXCEEDED) &&
				(ret != ERR_INVALID_PARAMETER_NAME) &&
				(ret != ERR_INVALID_PARAMETER_TYPE) &&
				(ret != ERR_INVALID_PARAMETER_VALUE) &&
				(ret != ERR_READONLY_PARAMETER))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}


	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit SetParameterValues ret: %d  lz: %ld\n", ret, getTime () - startTime);
	)
	return ret;
}

int cwmp__GetParameterValues(struct soap *soap, struct ArrayOfString *ParameterNames, struct cwmp__GetParameterValuesResponse *response)
{
	int ret = 0;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_SOAP, "Enter GetParameterValues st: %ld\n", startTime);
	)
	sessionCall();
	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = getParameters(ParameterNames, &response->ParameterList);

	if (ret != OK)
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT) &&
				(ret != ERR_RESOURCE_EXCEEDED) &&
				(ret != ERR_INVALID_PARAMETER_NAME))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_SOAP, "Exit GetParameterValues ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	return ret;
}

int cwmp__GetParameterNames(struct soap *soap,
							xsd__string ParameterPath,
							xsd__boolean NextLevel,
							struct cwmp__GetParameterNamesResponse *response)
{
	int ret = 0;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter GetParameterNames st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = getParameterNames(ParameterPath, NextLevel, &response->ParameterList);

	if (ret != OK)
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT) &&
				(ret != ERR_INVALID_PARAMETER_NAME))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit GetParameterNames ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	return ret;
}

int cwmp__SetParameterAttributes(struct soap *soap,
								 struct ArrayOfSetParameterAttributesStruct *ParameterList,
								 struct cwmp__SetParameterAttributesResponse *emptyRes)
{
	int ret = 0;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter SetParameterAttributes  st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}


	ret = setParametersAttributes(ParameterList);
	if (ret != OK)
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT) &&
				(ret != ERR_RESOURCE_EXCEEDED) &&
				(ret != ERR_INVALID_PARAMETER_NAME) &&
				(ret != ERR_NOTIFICATION_REQ_REJECT))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}


	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit SetParameterAttributes ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	return ret;
}

int cwmp__GetParameterAttributes(struct soap *soap,
								 struct ArrayOfString *ParameterNames,
								 struct cwmp__GetParameterAttributesResponse *response)
{
	int ret = 0;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter GetParameterAttributes st: %ld\n", startTime);
	)
	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = getParametersAttributes(ParameterNames, &response->ParameterList);
	if (ret != OK)
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT) &&
				(ret != ERR_RESOURCE_EXCEEDED) &&
				(ret != ERR_INVALID_PARAMETER_NAME))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit GetParameterAttributes ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	return ret;
}

int cwmp__AddObject(struct soap *soap, xsd__string ObjectName, xsd__string ParameterKey, struct cwmp__AddObjectResponse *ReturnValue)
{
	int ret = 0;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter AddObject st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = addObject(ObjectName, ReturnValue);
	if (ret == OK)
	{
		setParameter(PARAMETER_KEY, ParameterKey);
	}
	else
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT) &&
				(ret != ERR_RESOURCE_EXCEEDED) &&
				(ret != ERR_INVALID_PARAMETER_NAME))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit AddObject ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	return ret;
}

int cwmp__DeleteObject(struct soap *soap, xsd__string ObjectName, xsd__string ParameterKey, struct cwmp__DeleteObjectResponse *ReturnValue)
{
	int ret = 0;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter DeleteObject st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = deleteObject(ObjectName, ReturnValue);
	if (ret == OK)
	{
		setParameter(PARAMETER_KEY, ParameterKey);
	}
	else
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT) &&
				(ret != ERR_INVALID_PARAMETER_NAME))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit DeleteObject ret: %d lz: %ld\n", ret, getTime () - startTime);
	)
	ReturnValue->Status = 0;
	return ret;
}

int cwmp__Reboot(struct soap *soap, xsd__string CommandKey, struct cwmp__RebootResponse *emptyRes)
{
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter Reboot st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	if (CommandKey != NULL)
	{
		setSdLastCommandKey(CommandKey);
	}

	addEventCodeMultiple(EV_M_BOOT, getSdLastCommandKey());

	addRebootIdx(REBOOT_CALLBACKS_INDEX_SUM);

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "cwmp__Reboot() -> addRebootIdx(0x%.4X)\n", REBOOT_CALLBACKS_INDEX_SUM);
	)

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit Reboot lz: %ld\n", getTime () - startTime);
	)

	return SOAP_OK;
}

int cwmp__Download(struct soap *soap,
				   xsd__string CommandKey,
				   xsd__string FileType,
				   xsd__string URL,
				   xsd__string Username,
				   xsd__string Password,
				   unsigned int FileSize,
				   xsd__string TargetFileName,
				   unsigned int DelaySeconds,
				   xsd__string SuccessURL,
				   xsd__string FailureURL,
				   struct cwmp__DownloadResponse *response)
{
#ifdef HAVE_FILE_DOWNLOAD
	int ret;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter Download  st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = execDownload(soap,
			CommandKey,
			FileType,
			URL,
			Username,
			Password,
			FileSize,
			TargetFileName,
			DelaySeconds,
			SuccessURL,
			FailureURL,
			ACS,
			NULL,
			NULL,
			response);

	if (ret != OK)
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT) &&
				(ret != ERR_RESOURCE_EXCEEDED) &&
				(ret != ERR_DOWNLOAD_FAILURE) &&
				(ret != ERR_TRANS_AUTH_FAILURE) &&
				(ret != ERR_NO_TRANS_PROTOCOL))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit Download ret: %d  lz: %ld\n", ret, getTime () - startTime);
	)

	return ret;
#else
	return ERR_METHOD_NOT_SUPPORTED;
#endif /* HAVE_FILE_DOWNLOAD */
}

int cwmp__ScheduleDownload(struct soap *soap,
						   xsd__string CommandKey,
						   xsd__string FileType,
						   xsd__string URL,
						   xsd__string Username,
						   xsd__string Password,
						   unsigned int FileSize,
						   xsd__string TargetFileName,
						   struct ArrayOfTimeWindowsStruct *TimeWindowList,
						   struct cwmp__ScheduleDownloadResponse *emptyRes)
{
#ifdef HAVE_FILE_SCHEDULE_DOWNLOAD
	if (!suportedRPC.rpc.isScheduleDownload)
	{
		DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_SOAP, "Method ScheduleDownload is not supported\n");
		)
		return ERR_METHOD_NOT_SUPPORTED;
	}

	int ret;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter ScheduleDownload  st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = execScheduleDownload(soap,
			CommandKey,
			FileType,
			URL,
			Username,
			Password,
			FileSize,
			TargetFileName,
			TimeWindowList,
			ACS,
			NULL,
			NULL,
			emptyRes);

	if (ret != OK)
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT) &&
				(ret != ERR_RESOURCE_EXCEEDED) &&
				(ret != ERR_DOWNLOAD_FAILURE) &&
				(ret != ERR_NO_TRANS_PROTOCOL))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit ScheduleDownload ret: %d  lz: %ld\n", ret, getTime () - startTime);
	)

	return ret;
#else
	DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_SOAP, "Method ScheduleDownload is not supported\n");
	)
	return ERR_METHOD_NOT_SUPPORTED;
#endif /* HAVE_FILE_SCHEDULE_DOWNLOAD */
}

int cwmp__Upload(struct soap *soap,
				 xsd__string CommandKey,
				 xsd__string FileType,
				 xsd__string URL,
				 xsd__string Username,
				 xsd__string Password,
				 xsd__unsignedInt DelaySeconds,
				 struct cwmp__UploadResponse *response)
{
#ifdef HAVE_FILE_UPLOAD
	int ret = OK;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter Upload st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = execUpload(soap, CommandKey, FileType, URL, Username, Password, DelaySeconds, ACS, NULL, NULL, response);

	if (ret != OK)
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT) &&
				(ret != ERR_RESOURCE_EXCEEDED) &&
				(ret != ERR_UPLOAD_FAILURE) &&
				(ret != ERR_TRANS_AUTH_FAILURE) &&
				(ret != ERR_NO_TRANS_PROTOCOL))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit Upload ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	return ret;
#else
	return ERR_METHOD_NOT_SUPPORTED;
#endif /* HAVE_FILE_UPLOAD */
}

int cwmp__CancelTransfer(struct soap *soap, xsd__string CommandKey, struct cwmp__CancelTransferResponse *emptyRes)
{
#ifdef HAVE_CANCEL_TRANSFER
	if (!suportedRPC.rpc.isCancelTransfer)
	{
		DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_SOAP, "Method ScheduleDownload is not supported\n");
		)
		return ERR_METHOD_NOT_SUPPORTED;
	}

	int ret = OK;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "cwmp__CancelTransfer is started\n");
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = cancelTransferHandler(soap, CommandKey);

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit from cwmp__CancelTransfer ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	if (ret!=OK && ret!=ERR_METHOD_NOT_SUPPORTED && ret!=ERR_REQUEST_DENIED && ret!=ERR_RESOURCE_EXCEEDED && ret!=ERR_CANCELATION_NOT_PERMITTED)
	{
		ret = ERR_REQUEST_DENIED;
	}

	return ret;
#else
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Method Cancel Transfer is not supported\n");
	)
	return ERR_METHOD_NOT_SUPPORTED;
#endif /* HAVE_CANCEL_TRANSFER */
}

int cwmp__ChangeDUState(struct soap *soap, xsd__string CommandKey, struct ArrayOfOperationStruct *Operations, struct cwmp__ChangeDUStateResponse *emptyRes)
{
#ifdef HAVE_DEPLOYMENT_UNIT
	if (!suportedRPC.rpc.isChangeDUState)
	{
		DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_SOAP, "Method Change DU State is not supported\n");
		)
		return ERR_METHOD_NOT_SUPPORTED;
	}

	int ret = 0;
	char * str_operations_begin = NULL;
	char * str_operations_end = NULL;
	char str_operations[4096] = {"\0"};
	int len_operations = 0;
	char * str_tmp = NULL;

	char URL[DU_URL_LEN+1] = {"\0"};
	char UUID[DU_UUID_LEN+1] = {"\0"};
	char Username[DU_USERNAME_LEN+1] = {"\0"};
	char Password[DU_PASSWORD_LEN+1] = {"\0"};
	char Version[DU_VERSION_LEN+1] = {"\0"};
	char ExecutionEnvRef[DU_EXECUTION_ENV_REF_LEN+1] = {"\0"};

	char str_InstallOpStruct[2048] = {"\0"};
	char str_UpdateOpStruct[2048] = {"\0"};
	char str_UninstallOpStruct[512] = {"\0"};

	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "ChangeDUState st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	if (CommandKey != NULL)
	{
		setSdLastCommandKey(CommandKey);
	}

	memset(str_operations, 0, sizeof(str_operations));
	str_operations_begin = strstr(soap->buf, "<Operations");
	str_operations_end = strstr(soap->buf, "</Operations>");

	/*get len section Operations*/
	len_operations = strlen(str_operations_begin) - strlen(str_operations_end);

	if(len_operations > sizeof(str_operations))
	{
		DEBUG_OUTPUT(
			dbglog (SVR_INFO, DBG_SOAP, "cwmp__ChangeDUState buffer size is insufficient\n");
		)

		return ERR_RESOURCE_EXCEEDED;
	}

	strncpy(str_operations, str_operations_begin, len_operations);

	/*InstallOpStruct processing*/
	str_tmp = str_operations;
	while(strstr(str_tmp, "<InstallOpStruct>"))
	{
		char * str_InstallOpStruct_begin = NULL;
		char * str_InstallOpStruct_end = NULL;
		int len_InstallOpStruct = 0;

		char * str_begin = NULL;
		char * str_end = NULL;

		memset(URL, 0, sizeof(URL));
		memset(UUID, 0, sizeof(UUID));
		memset(Username, 0, sizeof(Username));
		memset(Password, 0, sizeof(Password));
		memset(ExecutionEnvRef, 0, sizeof(ExecutionEnvRef));
		memset(str_InstallOpStruct, 0, sizeof(str_InstallOpStruct));

		str_InstallOpStruct_begin = strstr(str_tmp, "<InstallOpStruct>");
		str_InstallOpStruct_end = strstr(str_tmp, "</InstallOpStruct>");

		if(str_InstallOpStruct_begin == NULL || str_InstallOpStruct_end == NULL)
			break;

		/*get len section InstallOpStruct*/
		len_InstallOpStruct = strlen(str_InstallOpStruct_begin)- strlen(str_InstallOpStruct_end);
		/* get InstallOpStruct section*/
		strncpy(str_InstallOpStruct, str_InstallOpStruct_begin, len_InstallOpStruct);

		/*get URL*/
		str_begin = strstr(str_InstallOpStruct, "<URL>");
		str_end = strstr(str_InstallOpStruct, "</URL>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(URL, str_begin + strlen("<URL>"), (strlen(str_begin) - strlen(str_end) -  strlen("<URL>")));

		/*get UUID*/
		str_begin = strstr(str_InstallOpStruct, "<UUID>");
		str_end = strstr(str_InstallOpStruct, "</UUID>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(UUID, str_begin + strlen("<UUID>"), (strlen(str_begin) - strlen(str_end) - strlen("<UUID>")));

		/*get Username*/
		str_begin = strstr(str_InstallOpStruct, "<Username>");
		str_end = strstr(str_InstallOpStruct, "</Username>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(Username, str_begin + strlen("<Username>"), (strlen(str_begin) - strlen(str_end) - strlen("<Username>")));

		/*get Password*/
		str_begin = strstr(str_InstallOpStruct, "<Password>");
		str_end = strstr(str_InstallOpStruct, "</Password>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(Password, str_begin + strlen("<Password>"), (strlen(str_begin) - strlen(str_end) - strlen("<Password>")));

		/*get ExecutionEnvRef*/
		str_begin = strstr(str_InstallOpStruct, "<ExecutionEnvRef>");
		str_end = strstr(str_InstallOpStruct, "</ExecutionEnvRef>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(ExecutionEnvRef, str_begin + strlen("<ExecutionEnvRef>"), (strlen(str_begin) - strlen(str_end) - strlen("<ExecutionEnvRef>")));

		/* implementation function execute Install Deployment Unit */
		ret = execInstallDeploymentUnit(soap, CommandKey, URL, UUID, Username, Password, ExecutionEnvRef, ACS);

		if(ret != OK)
			return ret;

		/*GOTO  next InstallOpStruct*/
		str_tmp = str_InstallOpStruct_end + strlen("</InstallOpStruct>");
	}
	/*UpdateOpStruct processing*/
	str_tmp = str_operations;
	while(strstr(str_tmp, "<UpdateOpStruct>"))
	{
		char * str_UpdateOpStruct_begin = NULL;
		char * str_UpdateOpStruct_end = NULL;
		int len_UpdateOpStruct = 0;

		char * str_begin = NULL;
		char * str_end = NULL;

		memset(UUID, 0, sizeof(UUID));
		memset(Version, 0, sizeof(Version));
		memset(URL, 0, sizeof(URL));
		memset(Username, 0, sizeof(Username));
		memset(Password, 0, sizeof(Password));
		memset(str_UpdateOpStruct, 0, sizeof(str_UpdateOpStruct));

		str_UpdateOpStruct_begin = strstr(str_tmp, "<UpdateOpStruct>");
		str_UpdateOpStruct_end = strstr(str_tmp, "</UpdateOpStruct>");

		if(str_UpdateOpStruct_begin == NULL || str_UpdateOpStruct_end == NULL)
			break;

		/*get len section UpdateOpStruct*/
		len_UpdateOpStruct = strlen(str_UpdateOpStruct_begin) - strlen(str_UpdateOpStruct_end);
		/* get UpdateOpStruct section*/
		strncpy(str_UpdateOpStruct, str_UpdateOpStruct_begin, len_UpdateOpStruct);

		/*get UUID*/
		str_begin = strstr(str_UpdateOpStruct, "<UUID>");
		str_end = strstr(str_UpdateOpStruct, "</UUID>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(UUID, str_begin + strlen("<UUID>"), (strlen(str_begin) - strlen(str_end) - strlen("<UUID>")));

		/*get Version*/
		str_begin = strstr(str_UpdateOpStruct, "<Version>");
		str_end = strstr(str_UpdateOpStruct, "</Version>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(Version, str_begin + strlen("<Version>"), (strlen(str_begin) - strlen(str_end) - strlen("<Version>")));

		/*get URL*/
		str_begin = strstr(str_UpdateOpStruct, "<URL>");
		str_end = strstr(str_UpdateOpStruct, "</URL>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(URL, str_begin + strlen("<URL>"), (strlen(str_begin) - strlen(str_end) -  strlen("<URL>")));

		/*get Username*/
		str_begin = strstr(str_UpdateOpStruct, "<Username>");
		str_end = strstr(str_UpdateOpStruct, "</Username>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(Username, str_begin + strlen("<Username>"), (strlen(str_begin) - strlen(str_end) - strlen("<Username>")));

		/*get Password*/
		str_begin = strstr(str_UpdateOpStruct, "<Password>");
		str_end = strstr(str_UpdateOpStruct, "</Password>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(Password, str_begin + strlen("<Password>"), (strlen(str_begin) - strlen(str_end) - strlen("<Password>")));

		/* implementation function execute Update Deployment Unit */
		ret = execUpdateDeploymentUnit(soap, CommandKey, UUID, Version, URL, Username, Password, ACS);

		if(ret != OK)
			return ret;

		/*GOTO  next UpdateOpStruct*/
		str_tmp = str_UpdateOpStruct_end + strlen("</UpdateOpStruct>");
	}

	/*UninstallOpStruct processing*/
	str_tmp = str_operations;
	while(strstr(str_tmp, "<UninstallOpStruct>"))
	{
		char * str_UninstallOpStruct_begin = NULL;
		char * str_UninstallOpStruct_end = NULL;
		int len_UninstallOpStruct = 0;

		char * str_begin = NULL;
		char * str_end = NULL;

		memset(UUID, 0, sizeof(UUID));
		memset(Version, 0, sizeof(Version));
		memset(ExecutionEnvRef, 0, sizeof(ExecutionEnvRef));
		memset(str_UninstallOpStruct, 0, sizeof(str_UninstallOpStruct));

		str_UninstallOpStruct_begin = strstr(str_tmp, "<UninstallOpStruct>");
		str_UninstallOpStruct_end = strstr(str_tmp, "</UninstallOpStruct>");

		if(str_UninstallOpStruct_begin == NULL || str_UninstallOpStruct_end == NULL)
			break;

		/*get len section UninstallOpStruct*/
		len_UninstallOpStruct = strlen(str_UninstallOpStruct_begin)- strlen(str_UninstallOpStruct_end);
		/* get UninstallOpStruct section*/
		strncpy(str_UninstallOpStruct, str_UninstallOpStruct_begin, len_UninstallOpStruct);

		/*get UUID*/
		str_begin = strstr(str_UninstallOpStruct, "<UUID>");
		str_end = strstr(str_UninstallOpStruct, "</UUID>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(UUID, str_begin + strlen("<UUID>"), (strlen(str_begin) - strlen(str_end) - strlen("<UUID>")));

		/*get Version*/
		str_begin = strstr(str_UninstallOpStruct, "<Version>");
		str_end = strstr(str_UninstallOpStruct, "</Version>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(Version, str_begin + strlen("<Version>"), (strlen(str_begin) - strlen(str_end) - strlen("<Version>")));

		/*get ExecutionEnvRef*/
		str_begin = strstr(str_UninstallOpStruct, "<ExecutionEnvRef>");
		str_end = strstr(str_UninstallOpStruct, "</ExecutionEnvRef>");
		if(str_begin != NULL || str_end != NULL)
			strncpy(ExecutionEnvRef, str_begin + strlen("<ExecutionEnvRef>"), (strlen(str_begin) - strlen(str_end) - strlen("<ExecutionEnvRef>")));

		/* implementation function execute Update Deployment Unit */
		ret = execUninstallDeploymentUnit(soap, CommandKey, UUID, Version, ExecutionEnvRef, ACS);

		if(ret != OK)
			return ret;

		/*GOTO  next UninstallOpStruct*/
		str_tmp = str_UninstallOpStruct_end + strlen("</UninstallOpStruct>");
	}

	return ret;
#else
	DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_SOAP, "Method Change DU State is not supported\n");
	)
	return ERR_METHOD_NOT_SUPPORTED;
#endif /* HAVE_DEPLOYMENT_UNIT */
}

/* FactoryReset
 * reset all Parameters to the initial value
 * remove all Eventcodes
 * remove all Options
 * remove all outstanding down-/up-loads
 * and request a reboot */
int cwmp__FactoryReset(struct soap *soap, void *emptyReq, struct cwmp__FactoryResetResponse *emptyRes)
{
#ifdef HAVE_FACTORY_RESET
	int ret = SOAP_OK;
	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	/* set make factory reset */
	addRebootIdx(FACTORYRESET_CALLBACKS_INDEX_SUM);

	return ret;
#else
	return ERR_METHOD_NOT_SUPPORTED;
#endif /* HAVE_FACTORY_RESET */
}

int cwmp__GetQueuedTransfers(struct soap *soap, void *empty, struct cwmp__GetQueuedTransfersResponse *response)
{
#ifdef HAVE_GET_QUEUED_TRANSFERS
	int ret = OK;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter GetQueuedTransfers st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = execGetQueuedTransfers(&response->TransferList);

	if (ret != OK)
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit GetQueuedTransfers ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	return SOAP_OK;
#else
	return ERR_METHOD_NOT_SUPPORTED;
#endif /* HAVE_GET_QUEUED_TRANSFERS */
}

int cwmp__GetAllQueuedTransfers(struct soap *soap, void *empty, struct cwmp__GetAllQueuedTransfersResponse *response)
{
#ifdef HAVE_GET_ALL_QUEUED_TRANSFERS
	if (!suportedRPC.rpc.isGetAllQueuedTransfers)
	{
		DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_SOAP, "Method GetAllQueuedTransfers is not supported\n");
		)
		return ERR_METHOD_NOT_SUPPORTED;
	}

	int ret = OK;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter GetAllQueuedTransfers st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = execGetAllQueuedTransfers(&response->TransferList);

	if (ret != OK)
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit GetAllQueuedTransfers ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	return SOAP_OK;
#else
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Method GetAllQueuedTransfers is not supported\n");
	)
	return ERR_METHOD_NOT_SUPPORTED;
#endif /* HAVE_GET_ALL_QUEUED_TRANSFERS */
}

int cwmp__ScheduleInform(struct soap *soap,
						 xsd__unsignedInt DelaySeconds,
						 xsd__string CommandKey,
						 struct cwmp__ScheduleInformResponse *emptyRes)
{
#ifdef HAVE_SCHEDULE_INFORM
	int ret = 0;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter Schedule Inform st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = execScheduleInform(soap, DelaySeconds, CommandKey);

	if (ret != OK)
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit Schedule Inform ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	return ret;
#else
	return ERR_METHOD_NOT_SUPPORTED;
#endif /* HAVE_SCHEDULE_INFORM */
}

int cwmp__SetVouchers(struct soap *soap, struct ArrayOfVouchers *VoucherList, struct cwmp__SetVouchersResponse *emptyRes)
{
#ifdef HAVE_VOUCHERS_OPTIONS
	int ret = OK;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter SetVouchers st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = storeVouchers(VoucherList);
	if (ret != OK)
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT) &&
				(ret != ERR_RESOURCE_EXCEEDED))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit SetVouchers ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	return SOAP_OK;
#else
	return ERR_METHOD_NOT_SUPPORTED;
#endif /* HAVE_VOUCHERS_OPTIONS */
}

int cwmp__GetOptions(struct soap *soap, xsd__string OptionName, struct cwmp__GetOptionsResponse *response)
{
#ifdef HAVE_VOUCHERS_OPTIONS
	int ret = OK;
	long startTime = getTime ();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Enter GetOptions st: %ld\n", startTime);
	)

	sessionCall();

	if (soap->header)
	{
		soap->header = analyseHeader(soap->header);
	}

	ret = getOptions(OptionName, &response->OptionList);

	if (ret != OK)
	{
		if((ret != ERR_REQUEST_DENIED) &&
				(ret != ERR_INTERNAL_ERROR) &&
				(ret != ERR_INVALID_ARGUMENT))
		{
			ret = ERR_INTERNAL_ERROR;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SOAP, "Exit GetOptions ret: %d lz: %ld\n", ret, getTime () - startTime);
	)

	return ret;
#else
	return ERR_METHOD_NOT_SUPPORTED;
#endif /* HAVE_VOUCHERS_OPTIONS */
}

/* ACS methods */

int cwmp__Inform(struct soap *soap,
				 struct DeviceId *DeviceId,
				 struct ArrayOfEventStruct *Event,
				 int MaxEnvelopes,
				 xsd__dateTime CurrentTime,
				 int RetryCount,
				 struct ArrayOfParameterValueStruct *ParameterList,
				 struct cwmp__InformResponse *response)
{
	return SOAP_OK;
}

int cwmp__TransferComplete(struct soap *soap,
						   xsd__string CommandKey,
						   struct cwmp__Fault *FaultStruct,
						   xsd__dateTime StartTime,
						   xsd__dateTime CompleteTime,
						   struct cwmp__TransferCompleteResponse *emptyRes)
{
	return SOAP_OK;
}

int cwmp__AutonomousTransferComplete(struct soap *soap,
									 xsd__string AnnounceURL,
									 xsd__string TransferURL,
									 xsd__boolean IsDownload,
									 xsd__string FileType,
									 xsd__unsignedInt FileSize,
									 xsd__string TargetFileName,
									 struct cwmp__Fault *FaultStruct,
									 xsd__dateTime StartTime,
									 xsd__dateTime CompleteTime,
									 struct cwmp__AutonomousTransferCompleteResponse *emptyRes)
{
	return SOAP_OK;
}

int cwmp__RequestDownload(struct soap *soap,
						  xsd__string FileType,
						  struct ArrayOfArgs *FileTypeArg,
						  struct cwmp__RequestDownloadResponse *emptyRes)
{
	return SOAP_OK;
}

int cwmp__Kicked(struct soap *soap,
				 xsd__string Command,
				 xsd__string Referer,
				 xsd__string Arg,
				 xsd__string Next,
				 struct cwmp__KickedResponse *response)
{
	return SOAP_OK;
}

int cwmp__DUStateChangeComplete(struct soap *soap,
								struct ArrayOfOpResultStruct * Results,
								xsd__string CommandKey,
								struct cwmp__DUStateChangeCompleteResponse *emptyRes)
{
	return SOAP_OK;
}

int cwmp__AutonomousDUStateChangeComplete(struct soap *soap,
										  struct ArrayOfAutonOpResultStruct * Results,
										  struct cwmp__AutonomousDUStateChangeCompleteResponse * emptyRes)
{
	return SOAP_OK;
};
