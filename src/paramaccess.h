/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef paramaccess_H
#define paramaccess_H

#include "parameter.h"

#define STRING_GET 	*(char **)
#define INT_GET		*(int *)
#define UINT_GET	*(unsigned int *)
#define BOOL_GET	*(int *)
#define TIME_GET	*(struct timeval *)

#define INT_SET		**(int **)
#define BOOL_SET	**(int **)
#define UINT_SET	**(unsigned int **)
#define TIME_SET	**(struct timeval **)
#define STRING_SET	*(char**)
#define BASE64_SET	*(char**)

#define MARKED_NODE_FOR_ADDITIONAL_READ 4 // Used to create the instances of Multi-Instance Objects on Start-up

#define MANAGEMENT_SERVER_URL_SETIDX 120 //for MANAGEMENT_SERVER_URL we set the setIdx forcibly - for some functionality work.
#define ALIASBASEDADDRESSING_INITIDX 130 //for ALIASBASEDADDRESSING we set the initIdx forcibly - for some functionality work.
#define INSTANCEMODE_SETIDX          131 //for INSTANCEMODE we set the setIdx forcibly - for some functionality work.
#define AUTOCREATEINSTANCES_SETIDX   132 //for AUTOCREATEINSTANCES we set the setIdx forcibly - for some functionality work.


/* Because we have to store different kind of values
 * use a union
 */
typedef union
{
	char *in_cval;	/*! Characters, Bytes and Base64 into param access */
	char *out_cval;	/*! Characters, Bytes and Base64 from param access */
	int in_int;					/*! Integers into param access */
	int out_int;				/*! Integers from param access */
	unsigned int in_uint;		/*! Unsigned Integers into param access */
	unsigned int out_uint;		/*! Unsigned Integers from param access */
	struct timeval in_timet;	/*! Date times Integers into param access */
	struct timeval out_timet;	/*! Date times from param access */
	unsigned long in_ulong;		/*! Unsigned long into param access */
	unsigned long out_ulong;	/*! Unsigned long from param access */
	xsd__hexBinary in_hexBin; 	/*! hexBinary into param access */
	xsd__hexBinary out_hexBin;	/*! hexBinary from param access */
} ParameterValue;

int addObjectAccess (int idx, const char *, const char *);
int delObjectAccess (int idx, const char *, const char *);
int initAccess( int, const char*, ParameterType, ParameterValue * );
int deleteAccess( int, const char*, ParameterType, ParameterValue * );
int getAccess( int, const char *, ParameterType, ParameterValue * );
int setAccess( int, const char*, ParameterType, ParameterValue * );
int rebootAccess();
int getUdpCRU ( const char *, ParameterType, ParameterValue * );
int udpHostInitialization();
int is_IP_local( char * , int );

int getParameterValueCopy(ParameterValue*, ParameterValue*, ParameterType , unsigned int );

int dimarkPreparationToFactoryReset();
int dimarkPreparationToReboot();

#endif /* paramaccess_H */
