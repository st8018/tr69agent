/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef stun_dimark_H
#define stun_dimark_H

#include "dimark_globals.h"

#ifdef WITH_STUN_CLIENT

#define 	DEFAULT_STUN_MINIMUM_KEEP_ALIVE		100

void rx_udp_connection_request ( int, int, char *, char *);
unsigned int getMinStunPeriodicInterval();
int getMaxStunPeriodicInterval();
int is_Stun_enabled (char *);

#endif /* WITH_STUN_CLIENT */

#endif /* stun_dimark_H */
