/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"

#ifdef WITH_STUN_CLIENT

#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <net/ethernet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <openssl/hmac.h>

#include "STUN_dimark.h"

unsigned int
getMinStunPeriodicInterval()
{
	unsigned int *periodicInterval = NULL;

	if (getParameter (STUNMINIMUMKEEPALIVEPERIOD, &periodicInterval) != OK)
		return( DEFAULT_STUN_MINIMUM_KEEP_ALIVE );

	return(*periodicInterval);
}

int
getMaxStunPeriodicInterval()
{
	int *periodicInterval = NULL;

	if (getParameter (STUNMAXIMUMKEEPALIVEPERIOD, &periodicInterval) != OK)
		return (-1);

	return(*periodicInterval);
}

int
is_Stun_enabled( char *param_name )
{
	int stunEnabled;
	unsigned int *pStunEnabled;

	if ( getParameter(param_name, &pStunEnabled) != OK)
	{
		return (-1);
	}
	stunEnabled = (pStunEnabled && *pStunEnabled != 0) ? 1 : 0;

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_STUN, "is_Stun_enabled(): STUNEnabled = %d\n", stunEnabled);
	)

	return stunEnabled;
}

#endif /* WITH_STUN_CLIENT */
