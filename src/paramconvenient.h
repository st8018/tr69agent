/***************************************************************************
 *    Original code © Copyright (C) 2004-2012 by Dimark Software Inc.      *
 *    support@dimark.com                                                   *
 *    Modifications © 2013 Comcast Cable Communications, LLC               *
 ***************************************************************************/

#ifndef paramconvenient_H
#define paramconvenient_H

#include "utils.h"

/**	get	some useful Parameter values */
unsigned int getPeriodicInterval( void );
int getPeriodicTime( struct timeval* );
const char *getServerURL( void );
void set_server_url_when_redirect(const char * );
const char *getUsername( void );
const char *getPassword( void );
int getConnectionURL( void * );
int getConnectionUsername( void * );
int getConnectionPassword( void * );
struct DeviceId* getDeviceId( void );

int initConfStruct(const char * );
char *getServerURLFromConf( void );
char *getUsernameFromConf( void );
char *getPasswordFromConf( void );
char *getSharedKey( void );

#ifdef WITH_STUN_CLIENT
const char *getServerAddr( void );
#endif /* WITH_STUN_CLIENT */

#endif /* paramconvenient_H */
