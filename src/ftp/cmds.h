/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef cmds_H
#define cmds_H

int doLogin(const char *, const char *, const char *);
int dimget(char *, char *, long *);
int dimput(char *, char *, long *);
void disconnect(void);
void setpassive(void);

#endif
