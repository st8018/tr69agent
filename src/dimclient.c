/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                      *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"
#include "dimclient.h"
#include "serverdata.h"
#include "utils.h"
#include "filetransfer.h"
#include "du_transfer.h"
#include "si_transfer.h"
#include "paramaccess.h"
#include "callback.h"
#include "eventcode.h"
#include "option.h"
#include "voipParameter.h"
#include "ftcallback.h"
#include "deployment_unit.h"
#include "hosthandler.h"
#include "vouchers.h"
#include "list.h"
#include "diagParameter.h"
#include "dim.nsmap"
#include "httpda.h"
#include "paramconvenient.h"
#include "kickedhandler.h"
#include "transfershandler.h"
#include "CRhandler.h"
#include "timehandler.h"
#include "stunhandler.h"
#include "notificationhandler.h"
#include "udpCRhandler.h"
#include "DBhandler.h"
#include "diagnosticsHandler.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include <openssl/md5.h>
#include "/usr/include/time.h"
#include <time.h>
#include <stdlib.h>
#ifdef LONG_COMMAND_LINE_OPTION_ENABLED
#include <unistd.h>
#include <getopt.h>
#endif

#define		FINISH_TRANSACTION		9999
#define 	BROKEN_TRANSACTION  	9998
#define		CONTINUE_TRANSACTION	9997
#define		HOLD_REQUEST_DETECTED	9996

#ifndef DIMARK_GSOAP_VERSION
#define DIMARK_GSOAP_VERSION "dSoap version wasn't defined"
#endif

#ifndef VERSION_OF_BUILD
#define VERSION_OF_BUILD "DimClient version wasn't defined"
#endif

#ifndef SVN_REV
#define SVN_REV "wasn't defined"
#endif

typedef int (*fparsehdr)(struct soap*, const char*, const char*);

char prevSentHTTPpacketBody[SOAP_BUFLEN+1];
volatile int  prevSoapErrorValue = 0;
char tmp_path[128] = { "./tmp" }; //default path to temp dir. You can set other parh in the start.sh file, line: CDIR="./tmp"
char conf_path[256] = "dimclient.conf";
char logconfig_path[256] = { '\0' };//"log.config";
char host[257] = { '\0' };
char udp_host[257] = { '\0' };
char proxy_host[256] = { '\0' };
int	 proxy_port = 0;
char proxy_userid[128] = { '\0' };
char proxy_passwd[128] = { '\0' };
char proxy_version[4] = { '\0' };

char volatile ConnectionReguestURLPath[CR_URL_PATH_LEN+1] = { '\0' };
char info_for_soap_header[100] = {'\0'};
char last_parsed_param_name[MAX_PARAM_PATH_LENGTH] = {'\0'};

// Intel IPv4 or IPv6 working mode flag
#ifdef WITH_IPV6
volatile int acsUrlIsIpv6 = 0;
#endif

#ifdef BINDING_TO_INTERFACE_NAME_IS_ENABLED
// Intel Bind to device, TCP socket option
char bind_interface_name[IFNAMSIZ] = "";
#endif

char DEFAULT_PARAMETER_FILE_[128] = {'\0'};
char PERSISTENT_PARAMETER_DIR_[128] = {'\0'};
char PERSISTENT_DATA_DIR_[128] = {'\0'};
char PERSISTENT_FILE_[128] = {'\0'};
char EVENT_STORAGE_FILE_[256] = {'\0'};
char PERSISTENT_OPTION_DIR_[128] = {'\0'};
char VOUCHER_FILE_[256] = {'\0'};
char PERSISTENT_TRANSFERLIST_DIR_[128] = {'\0'};
char PERSISTENT_DU_ENTRYLIST_DIR_[128] = {'\0'};
char PERSISTENT_SI_ENTRYLIST_DIR_[128] = {'\0'};
char DEFAULT_DOWNLOAD_FILE_[256] = {'\0'};
char DB_file_path[256];

char *DEFAULT_PARAMETER_FILE = "tmp.param";
char *PERSISTENT_PARAMETER_DIR = "parameter";
char *PERSISTENT_DATA_DIR = "data";
char *PERSISTENT_FILE = "bootstrap.dat";
char *EVENT_STORAGE_FILE = "eventcode.dat";
char *PERSISTENT_OPTION_DIR = "options";
char *VOUCHER_FILE = "Voucher_%d";
char *PERSISTENT_TRANSFERLIST_DIR = "filetrans";
char *PERSISTENT_DU_ENTRYLIST_DIR = "duentries";
char *PERSISTENT_SI_ENTRYLIST_DIR = "sientries";
char *DEFAULT_DOWNLOAD_FILE = "download.dwn";

//char *PERSISTENT_BOOTSTRAP_FILE = "/opt/persistent/bootstrap.dat";

#define DB_FILE_NAME	"parameters.db"

volatile int procId = 8080;

extern struct ConfigManagement dimclientConfStruct;

pthread_cond_t condGo = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutexGo = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t paramLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t informLock = PTHREAD_MUTEX_INITIALIZER;
pthread_attr_t  threadAttr;

#ifdef HAVE_FILE
#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
/* Define a download/upload callback function pointers
 * it is handed to the download/upload handlers
 * and called before/after a download/upload of the files */
DownloadCB downloadCBB = NULL;
ScheduleDownloadCB scheduleDownloadCBB = NULL;
UploadCB uploadCBB = NULL;
DownloadCB downloadCBA = NULL;
ScheduleDownloadCB scheduleDownloadCBA = NULL;
UploadCB uploadCBA = NULL;
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */
#ifdef HAVE_DEPLOYMENT_UNIT
DeploymentUnitCB DU_CB_AfterDownload = NULL;
#endif /* HAVE_DEPLOYMENT_UNIT */
#endif /* HAVE_FILE */

#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)

#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */

struct http_da_info info;
long local_sendInform_count = 0;
static char authrealm[] = CONNECTION_REALM;

static int dimarkMain(struct soap *, unsigned short);
static int dimark_fdisconnect(struct soap *);
static void soapSetup(struct soap *);
static int sendEmptyBody(struct soap *);
static int sendInform(struct soap *, struct ArrayOfParameterValueStruct *, struct ArrayOfEventStruct *, struct DeviceId *);
static void sigpipe_handler(int);
static void sigsegv_handler(int);
static void sigint_handler(int);
static int dimarkSoap_serve(struct soap *, int *);
#ifdef	WITH_COOKIES
static int cookieHandler (struct soap *);
#endif /* WITH_COOKIES */
static int initParameters(bool);
static void freeMemory(struct soap *);
static int sendFault(struct soap *);
static int clearGetRPCMethods(struct soap *);
static int setSuportedRPCInfoFromCWMPVersion(SUPORTEDRPC *, char * );

static struct soap soap;

static unsigned int prevAuthenticationSoapError = 0; // 401 or 407. To determine Authentication or Proxy-Authentication should be used.

#ifdef HAVE_HOST
static struct soap hostSoap;
#endif
#ifdef HAVE_CONNECTION_REQUEST
struct soap CRSoap;
#endif
#ifdef HAVE_KICKED
static struct soap kickedSoap;
#endif
#ifdef HAVE_GET_ALL_QUEUED_TRANSFERS
static struct soap transfersSoap;
#endif

pthread_t hostTid;
#ifdef HAVE_CONNECTION_REQUEST
pthread_t udpCRhandlerTid;
#endif
pthread_t timeTid;
#if defined(HAVE_SCHEDULE_INFORM)
pthread_t scheduleInformTimerTid;
#endif /* HAVE_SCHEDULE_INFORM */
#ifdef HAVE_FILE
#if defined(HAVE_DEPLOYMENT_UNIT) && defined(HAVE_FILE_DOWNLOAD)
pthread_t deploymentUnitTimerTid;
#endif /* HAVE_DEPLOYMENT_UNIT && HAVE_FILE_DOWNLOAD */
#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
pthread_t transferTimerTid;
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */
#endif
#ifdef HAVE_KICKED
pthread_t kickedTid;
#endif
#ifdef HAVE_GET_ALL_QUEUED_TRANSFERS
pthread_t transfersTid;
#endif
#ifdef HAVE_UDP_ECHO
pthread_t udpEchoTid;
#endif
#ifdef HAVE_DIAGNOSTICS
pthread_t diagTid;
#endif
#ifdef WITH_STUN_CLIENT
pthread_t stunTid;
#endif
#ifdef HAVE_NOTIFICATION
pthread_t passive_notification_id; /* thread id for passive notification */
pthread_t active_notification_id; /* thread id for active notification */
#endif

// This CWMP version will be used in the first Inform RPC after dimclient start.
// If ACS supports the earlier CWMP version that Dimclient will change cwmp_version_ns value.
// Change CWMP_DEFAULT_VERSION value if you want to compile the client with other default CWMP version.
char cwmp_version_ns[] = CWMP_DEFAULT_VERSION;

struct Namespace Dim_namespaces[] =
{
	{"SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/", "http://www.w3.org/*/soap-envelope", NULL},
	{"SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/", "http://www.w3.org/*/soap-encoding", NULL},
	{"xsi", "http://www.w3.org/2001/XMLSchema-instance", "http://www.w3.org/*/XMLSchema-instance", NULL},
	{"xsd", "http://www.w3.org/2001/XMLSchema", "http://www.w3.org/*/XMLSchema", NULL},
	{"cwmp", cwmp_version_ns, "urn:dslforum-org:cwmp-1-*", NULL},
	{NULL, NULL, NULL, NULL}
};

SUPORTEDRPC suportedRPC;

/**	
 * Main	Function
 * this Function is normally called	by the HostSystem of the Modem
 */
int main(int argc, char **argv)
{
	int ret = OK;
	unsigned short delayCnt;
	unsigned int periodicInformInterval, periodicIntervalDelay;
	struct timeval periodicIntervalTime;
	int delta;
	struct timespec handlerWait;
	struct timespec retryDelay, retryRemain;
	struct timespec nextPeriodicTime;
	time_t now;
	int ch;
	setvbuf(stdout, NULL, _IOLBF, 0);
    /* Initialization of signal handlers: */
#ifndef EXTERNAL_INITIALIZATION_OF_SIGSEGV_HANDLER
	signal(SIGSEGV, sigsegv_handler);
#else
	EXTERNAL_INITIALIZATION_OF_SIGSEGV_HANDLER;
#endif

#ifndef EXTERNAL_INITIALIZATION_OF_SIGINT_HANDLER
	struct sigaction new_action;
	new_action.sa_handler = sigint_handler;
	sigemptyset (&new_action.sa_mask);
    sigaddset (&new_action.sa_mask, SIGSEGV);
	new_action.sa_flags = 0;
	sigaction (SIGINT, &new_action, NULL);
#else
	EXTERNAL_INITIALIZATION_OF_SIGINT_HANDLER;
#endif
	/* END OF Initialization of signal handlers: */

#ifdef LONG_COMMAND_LINE_OPTION_ENABLED
	int longIndex = 0;
	const struct option longOpts[] = {
	    { "binding_IP", required_argument, NULL, 'B' },
	    { "base_port", required_argument, NULL, 'b' },
	    { "temp_dir", required_argument, NULL, 't' },
	    { "proxy_name", required_argument, NULL, 'P' },
	    { "proxy_port", required_argument, NULL, 'p' },
	    { "proxy_userid", required_argument, NULL, 'u' },
	    { "proxy_passwd", required_argument, NULL, 'a' },
	    { "proxy_version", required_argument, NULL, 'v' },
	    { "config_file", required_argument, NULL, 'c' },
	    { "log_conf_file", required_argument, NULL, 'l' },
	    { "help", no_argument, NULL, 'h' },
	    { NULL, no_argument, NULL, 0 }
	};
	while ((ch = getopt_long( argc, argv, "B:b:t:P:p:u:a:v:c:l:h", longOpts, &longIndex )) != EOF)
#else
	while ((ch = getopt(argc, argv, "B:b:t:P:p:u:a:v:c:l:h")) != EOF)
#endif
	//'h' without following ':' because it has not argument.
	{
		switch (ch)
		{
			case 'B':
				strncpy(host, optarg, sizeof(host)-1);
				strcpy(udp_host, host);
				break;
			case 'b':
				procId = atoi(optarg);
				break;
			case 't':
				strncpy(tmp_path, optarg, sizeof(tmp_path)-1 );
				break;
			case 'P':
				strncpy(proxy_host, optarg, sizeof(proxy_host)-1);
				break;
			case 'p':
				proxy_port = atoi(optarg);
				break;
			case 'u':
				strncpy(proxy_userid, optarg, sizeof(proxy_userid)-1);
				break;
			case 'a':
				strncpy(proxy_passwd, optarg, sizeof(proxy_passwd)-1);
				break;
			case 'v':
				strncpy(proxy_version, optarg, sizeof(proxy_version)-1);
				break;
			case 'c':
				strncpy(conf_path, optarg, sizeof(conf_path)-1);
				break;
			case 'l':
				strncpy(logconfig_path, optarg, sizeof(logconfig_path)-1);
				break;
			case '?':
				printf("!!! PLEASE NOTE THAT DIMCLIENT COMMAND LINE OPTIONS ARE CHANGED !!!\n");
				//break is not needed, to print help info.
			case 'h':
			default:
#ifdef LONG_COMMAND_LINE_OPTION_ENABLED
				printf("\n-h, --help          - print this Help\n");
				printf("-B, --binding_IP    - IP address for binding in the Connection Request and other handlers\n");
				printf("-b, --base_port     - Base binding port. Used as offset to calculate the binding ports in the Connection Request and other handlers\n");
				printf("-t, --temp_dir      - Temporary folder for runtime DB (must be writable)\n");
				printf("-P, --proxy_name    - Proxy Server host name or IP address\n");
				printf("-p, --proxy_port    - Proxy Server port (default = 8080)\n");
				printf("-u, --proxy_userid  - Proxy Authorization user name\n");
				printf("-a, --proxy_passwd  - Proxy Authorization password\n");
				printf("-v, --proxy_version - HTTP version of proxy \"1.0\" or \"1.1\"\n");
				printf("-c, --config_file   - Configuration file path (ManagementServer.URL, ManagementServer.Username, ManagementServer.Password, etc.)\n");
				printf("-l, --log_conf_file - Log configuration file path (default \"log.config\")\n");
				exit(0);
#else
				printf("\n-h - print this Help\n");
				printf("-B - IP address for binding in the Connection Request and other handlers\n");
				printf("-b - Base binding port. Used as offset to calculate the binding ports in the Connection Request and other handlers\n");
				printf("-t - Temporary folder for runtime DB (must be writable)\n");
				printf("-P - Proxy Server host name or IP address\n");
				printf("-p - Proxy Server port (default = 8080)\n");
				printf("-u - Proxy Authorization user name\n");
				printf("-a - Proxy Authorization password\n");
				printf("-v - HTTP version of proxy \"1.0\" or \"1.1\"\n");
				printf("-c - Configuration file path (ManagementServer.URL, ManagementServer.Username, ManagementServer.Password, etc.)\n");
				printf("-l - Log configuration file path (default \"log.config\")\n");
				exit(0);
#endif
		}
	}

	printf("------------------------------------------------------------\n");
	printf("Version of Build: %s\nDate of build: %s %s\nVersion of gSoap: %s\n", VERSION_OF_BUILD, __DATE__, __TIME__, DIMARK_GSOAP_VERSION);
	printf("SVN_REVISION: [%s]\n", SVN_REV);
	printf("CWMP version: [%s]\n", strstr(cwmp_version_ns, "cwmp"));
	printf("Base binding port: %d\n", procId);
	printf("Temporary folder: %s\n", tmp_path);
	printf("Proxy Server hostname/IP address: %s\n", (*proxy_host) ? proxy_host : "n/a" );
	if (proxy_port)
		printf("Proxy port: %d\n", proxy_port);
	else
		printf("Proxy port: n/a\n");
	printf("Proxy Authorization username: %s\n", (*proxy_userid) ? proxy_userid : "n/a" );
	printf("Proxy Authorization password: %s\n", (*proxy_passwd) ? proxy_passwd : "n/a" );
	printf("HTTP version of proxy: %s\n", (*proxy_version) ? proxy_version : "n/a" );
	printf("Configuration file path: %s\n", (*conf_path) ? conf_path : "n/a" );
	printf("Log configuration file path: %s\n", (*logconfig_path) ? logconfig_path : "n/a" );

	printf("\nListener config:\n");
	char * host_or_unknown = host[0]? host : "Unknown._Will_be_retrieved_from_the_data-model";
	int  isHostInIPv6Format = 0;
	if (host[0] && host[0] != '[')
		isHostInIPv6Format = isIPv6Adress(host);
#ifdef HAVE_HOST
	if (isHostInIPv6Format)
		printf("Host Notification bind address and port: [%s]:%d\n", host_or_unknown, HOST_NOTIFICATION_PORT + procId);
	else
		printf("Host Notification bind address and port: %s:%d\n", host_or_unknown, HOST_NOTIFICATION_PORT + procId);
#endif
#ifdef HAVE_CONNECTION_REQUEST
	if (isHostInIPv6Format)
		printf("Connection Request bind address and port: [%s]:%d\n", host_or_unknown, ACS_NOTIFICATION_PORT + procId);
	else
		printf("Connection Request bind address and port: %s:%d\n", host_or_unknown, ACS_NOTIFICATION_PORT + procId);
#endif
#ifdef HAVE_KICKED
	if (isHostInIPv6Format)
		printf("Kicked Notification bind address and port: [%s]:%d\n", host_or_unknown, KICKED_NOTIFICATION_PORT + procId);
	else
		printf("Kicked Notification bind address and port: %s:%d\n", host_or_unknown, KICKED_NOTIFICATION_PORT + procId);
#endif
#ifdef HAVE_GET_ALL_QUEUED_TRANSFERS
	if (isHostInIPv6Format)
		printf("Transfer Notification bind address and port: [%s]:%d\n", host_or_unknown, TRANSFERS_NOTIFICATION_PORT + procId);
	else
		printf("Transfer Notification bind address and port: %s:%d\n", host_or_unknown, TRANSFERS_NOTIFICATION_PORT + procId);
#endif
#ifdef HAVE_UDP_ECHO
	printf("UDP Echo bind port: %d\n", UDP_ECHO_DEFAULT_PORT + procId);
#endif
#ifdef HAVE_CONNECTION_REQUEST
	printf("UDP Connection Request bind port: %d\n", ACS_UDP_NOTIFICATION_PORT);
#endif

	printf("------------------------------------------------------------\n");
	printf("Compilation flags:\n");
#ifdef _DEBUG
	printf ("_DEBUG\n");
#endif /* _DEBUG */

#ifdef WITH_GATEWAY_ROOT
	printf ("WITH_GATEWAY_ROOT\n");
#endif /* WITH_GATEWAY_ROOT */

#ifdef WITH_DEVICE_ROOT
	printf ("WITH_DEVICE_ROOT\n");
#endif /* WITH_DEVICE_ROOT */

#ifdef DEVICE_WITHOUT_GATEWAY
	printf ("DEVICE_WITHOUT_GATEWAY\n");
#endif /* DEVICE_WITHOUT_GATEWAY */

#ifdef TR_181_DEVICE
	printf ("TR_181_DEVICE\n");
#endif /* TR_181_DEVICE */

#ifdef WITH_STUN_CLIENT
	printf ("WITH_STUN_CLIENT\n");
#endif /* WITH_STUN_CLIENT */

#ifdef ACS_REGMAN
	printf ("ACS_REGMAN\n");
#endif /* ACS_REGMAN */

#ifdef WITH_OPENSSL
	printf ("WITH_OPENSSL\n");
#endif /* WITH_OPENSSL */

#ifdef WITH_CLIENT_SSLAUTH
	printf ("WITH_CLIENT_SSLAUTH\n");
#endif /* WITH_CLIENT_SSLAUTH */

#ifdef WITH_SERVER_SSLAUTH
	printf ("WITH_SERVER_SSLAUTH\n");
#endif /* WITH_SERVER_SSLAUTH */

#ifdef WITH_USE_SQLITE_DB
	printf ("WITH_USE_SQLITE_DB\n");
#endif /* WITH_USE_SQLITE_DB */

#ifdef WITH_IPV6
	printf ("WITH_IPV6\n");
#endif /* WITH_IPV6 */

#ifdef WITH_COOKIES
	printf ("WITH_COOKIES\n");
#endif /* WITH_COOKIES */

#ifdef WITH_SOAPDEFS_H
	printf ("WITH_SOAPDEFS_H\n");
#endif /* WITH_SOAPDEFS_H */

#ifdef WITH_SSL_RESUMPTION
	printf ("WITH_SSL_RESUMPTION\n");
#endif /* WITH_SSL_RESUMPTION */

#ifdef SOAP_DISPLAY_MSG
	printf ("SOAP_DISPLAY_MSG\n");
#endif /* SOAP_DISPLAY_MSG */

	printf("------------------------------------------------------------\n");

#ifdef _DEBUG
	loadConfFile (logconfig_path);
#endif /* _DEBUG */

	printf("------------------------------------------------------------\n");


	/* check if exist tmp_path */
	checkDirectories(tmp_path);

	strcpy(DEFAULT_PARAMETER_FILE_, tmp_path);
	strcat(DEFAULT_PARAMETER_FILE_, "/");
	strncat(DEFAULT_PARAMETER_FILE_, DEFAULT_PARAMETER_FILE, sizeof(DEFAULT_PARAMETER_FILE_) - strlen(DEFAULT_PARAMETER_FILE_) - 1);
	DEFAULT_PARAMETER_FILE = DEFAULT_PARAMETER_FILE_; // San: It is memory leak?

	strcpy(PERSISTENT_PARAMETER_DIR_, tmp_path);
	strcat(PERSISTENT_PARAMETER_DIR_, "/");
	strncat(PERSISTENT_PARAMETER_DIR_, PERSISTENT_PARAMETER_DIR, sizeof(PERSISTENT_PARAMETER_DIR_) - strlen(PERSISTENT_PARAMETER_DIR_) - 1);
	strcat(PERSISTENT_PARAMETER_DIR_, "/");
	PERSISTENT_PARAMETER_DIR = PERSISTENT_PARAMETER_DIR_;
	/* check if exist PERSISTENT_PARAMETER_DIR */
	checkDirectories(PERSISTENT_PARAMETER_DIR);

	strcpy(PERSISTENT_DATA_DIR_, tmp_path);
	strcat(PERSISTENT_DATA_DIR_, "/");
	strncat(PERSISTENT_DATA_DIR_, PERSISTENT_DATA_DIR, sizeof(PERSISTENT_DATA_DIR_) - strlen(PERSISTENT_DATA_DIR_) - 1);
	strcat(PERSISTENT_DATA_DIR_, "/");
	PERSISTENT_DATA_DIR = PERSISTENT_DATA_DIR_;
	/* check if exist PERSISTENT_DATA_DIR */
	checkDirectories(PERSISTENT_DATA_DIR);

	strcpy(PERSISTENT_FILE_, tmp_path);
	strcat(PERSISTENT_FILE_, "/");
	strncat(PERSISTENT_FILE_, PERSISTENT_FILE, sizeof(PERSISTENT_FILE_) - strlen(PERSISTENT_FILE_) - 1);
	PERSISTENT_FILE = PERSISTENT_FILE_;

	strcpy(EVENT_STORAGE_FILE_, tmp_path);
	strcat(EVENT_STORAGE_FILE_, "/");
	strncat(EVENT_STORAGE_FILE_, EVENT_STORAGE_FILE, sizeof(EVENT_STORAGE_FILE_) - strlen(EVENT_STORAGE_FILE_) - 1);
	EVENT_STORAGE_FILE = EVENT_STORAGE_FILE_;

	strcpy(PERSISTENT_OPTION_DIR_, tmp_path);
	strcat(PERSISTENT_OPTION_DIR_, "/");
	strncat(PERSISTENT_OPTION_DIR_, PERSISTENT_OPTION_DIR, sizeof(PERSISTENT_OPTION_DIR_) - strlen(PERSISTENT_OPTION_DIR_) - 1);
	strcat(PERSISTENT_OPTION_DIR_, "/");
	PERSISTENT_OPTION_DIR = PERSISTENT_OPTION_DIR_;
	/* check if exist PERSISTENT_OPTION_DIR */
	checkDirectories(PERSISTENT_OPTION_DIR);

	strcpy(VOUCHER_FILE_, tmp_path);
	strcat(VOUCHER_FILE_, "/");
	strncat(VOUCHER_FILE_, VOUCHER_FILE, sizeof(VOUCHER_FILE_) - strlen(VOUCHER_FILE_) - 1);
	VOUCHER_FILE = VOUCHER_FILE_;

	strcpy(PERSISTENT_TRANSFERLIST_DIR_, tmp_path);
	strcat(PERSISTENT_TRANSFERLIST_DIR_, "/");
	strncat(PERSISTENT_TRANSFERLIST_DIR_, PERSISTENT_TRANSFERLIST_DIR, sizeof(PERSISTENT_TRANSFERLIST_DIR_) - strlen(PERSISTENT_TRANSFERLIST_DIR_) - 1);
	strcat(PERSISTENT_TRANSFERLIST_DIR_, "/");
	PERSISTENT_TRANSFERLIST_DIR = PERSISTENT_TRANSFERLIST_DIR_;
	/* check if exist PERSISTENT_TRANSFERLIST_DIR */
	checkDirectories(PERSISTENT_TRANSFERLIST_DIR);

	strcpy(PERSISTENT_DU_ENTRYLIST_DIR_, tmp_path);
	strcat(PERSISTENT_DU_ENTRYLIST_DIR_, "/");
	strncat(PERSISTENT_DU_ENTRYLIST_DIR_, PERSISTENT_DU_ENTRYLIST_DIR, sizeof(PERSISTENT_DU_ENTRYLIST_DIR_) - strlen(PERSISTENT_DU_ENTRYLIST_DIR_) - 1);
	strcat(PERSISTENT_DU_ENTRYLIST_DIR_, "/");
	PERSISTENT_DU_ENTRYLIST_DIR = PERSISTENT_DU_ENTRYLIST_DIR_;
	/* check if exist PERSISTENT_DU_ENTRYLIST_DIR */
	checkDirectories(PERSISTENT_DU_ENTRYLIST_DIR);

	strcpy(PERSISTENT_SI_ENTRYLIST_DIR_, tmp_path);
	strcat(PERSISTENT_SI_ENTRYLIST_DIR_, "/");
	strncat(PERSISTENT_SI_ENTRYLIST_DIR_, PERSISTENT_SI_ENTRYLIST_DIR, sizeof(PERSISTENT_SI_ENTRYLIST_DIR_) - strlen(PERSISTENT_SI_ENTRYLIST_DIR_) - 1);
	strcat(PERSISTENT_SI_ENTRYLIST_DIR_, "/");
	PERSISTENT_SI_ENTRYLIST_DIR = PERSISTENT_SI_ENTRYLIST_DIR_;
	/* check if exist PERSISTENT_SI_ENTRYLIST_DIR */
	checkDirectories(PERSISTENT_SI_ENTRYLIST_DIR);

	strncpy(DEFAULT_DOWNLOAD_FILE_, DEFAULT_DOWNLOAD_FILE, sizeof(DEFAULT_DOWNLOAD_FILE_) - 1 );
	DEFAULT_DOWNLOAD_FILE = DEFAULT_DOWNLOAD_FILE_;

	sprintf(DB_file_path, "%s/%s\0", tmp_path, DB_FILE_NAME);

	/* Set of info_for_soap_header to send it in the SOAP message Headers */
	sprintf(info_for_soap_header, "%s / %s [%s]\0", DIMARK_GSOAP_VERSION, VERSION_OF_BUILD, SVN_REV);

	/* Initialize the callback lists */
	initCallbackList();

	/* Set an EventCode, This is normally done by the HostSystem */
	loadEventCode();
	initServerData();

#ifdef WITH_USE_SQLITE_DB
	if (isBootstrap())
	{
		remove(DB_file_path);
		executeDBCommand(BEGIN);
		if ( (ret = createDBAndTables()) != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_MAIN, "main()->createDBAndTables() has returned ret = %d. Exit.\n", ret );
			)
			executeDBCommand(END);
			closeDBConnection(0);
			efreeAllTypedMem(0);
			exit(0);
		}
	}
	else
		executeDBCommand(BEGIN);
#endif /* WITH_USE_SQLITE_DB */

	executeCallback(&initParametersBeforeCbList);

	/* Load and create the Parameters */
	if((ret = initParameters(isBootstrap())) != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "main()->initParameters(): has returned error %d. Load and Initialization of parameter tree failed.\n", ret);
		)
#ifdef WITH_USE_SQLITE_DB
		executeDBCommand(END);
		closeDBConnection(0);
#endif /* WITH_USE_SQLITE_DB */
		efreeAllTypedMem(0);
		exit(0);
	}

	/* Initialize parameters from the config */
	if((ret = initConfStruct(conf_path)) != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "main(): Initialization Configure File error. initConfStruct(\"%s\") has returned %i\n", conf_path, ret);
		)
#ifdef WITH_USE_SQLITE_DB
		executeDBCommand(END);
		closeDBConnection(0);
#endif /* WITH_USE_SQLITE_DB */
		efreeAllTypedMem(0);
		exit(0);
	}

	// Intel - IPv6 or IPv4 working mode flag
#ifdef WITH_IPV6
	if (dimclientConfStruct.url[0]) // if dimclientConfStruct.url is nor empty
		acsUrlIsIpv6 = isIPv6Adress(dimclientConfStruct.url);
	else
		acsUrlIsIpv6 = isIPv6Adress(getServerURL());
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_MAIN, "main(): ManagementServerURL is in %s format\n", acsUrlIsIpv6 ? "IPv6" : "IPv4" );
	)
#endif

#ifdef AUTH_SERVICE
	/* Initialize Bootstrap ACS setting from Auth service */
	if((ret = init_BootStrapAuthService_ACSSettingConfig()) != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "main(): Initialization Configure File error. readBootStrapAuthServiceConfig() has returned %i\n", ret);
		)
#ifdef WITH_USE_SQLITE_DB
		executeDBCommand(END);
		closeDBConnection(0);
#endif /* WITH_USE_SQLITE_DB */
		efreeAllTypedMem(0);
		exit(0);
	}
#endif /* AUTH_SERVICE */


#ifdef BINDING_TO_INTERFACE_NAME_IS_ENABLED
	// Intel Binding TCP socket option
	if (dimclientConfStruct.netDeviceName[0])
	{
		strncpy(bind_interface_name, dimclientConfStruct.netDeviceName, sizeof(bind_interface_name));
		bind_interface_name[sizeof(bind_interface_name)-1] = '\0';
	}
	else
		memset(bind_interface_name, 0, sizeof(bind_interface_name));
#endif	/* BINDING_TO_INTERFACE_NAME_IS_ENABLED */

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	readAliasBasedAddressingInfoFromDB();
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */

	readAllMarkedParamsToCreateInstancesOnStartUp();

	/* Execute startup callback list this is only called once at this time */
	executeCallback(&initParametersDoneCbList);

	{
		// call this function for filling  information about 'host' and CR URL
		char * tmpStr1;
		getConnectionURL((void *) &tmpStr1);
	}

#ifdef WITH_USE_SQLITE_DB
	executeDBCommand(END);
	ret = closeDBConnection(0);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_MAIN, "main()->closeDBConnection() has returned ret = %d\n", ret );
	)
#endif /* WITH_USE_SQLITE_DB */

	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);

	/* Load the options if available */
#ifdef HAVE_VOUCHERS_OPTIONS
	loadOptions();
	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
#endif /* HAVE_VOUCHERS_OPTIONS */

#ifdef HAVE_FILE
#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
	downloadCBB = &fileDownloadCallbackBefore;
	downloadCBA = &fileDownloadCallbackAfter;
	scheduleDownloadCBB = &fileScheduleDownloadCallbackBefore;
	scheduleDownloadCBA = &fileScheduleDownloadCallbackAfter;
	uploadCBB = &fileUploadCallbackBefore;
	uploadCBA = &fileUploadCallbackAfter;
	/* Load transferList */
	ret = initFiletransfer(downloadCBB, scheduleDownloadCBB, uploadCBB, downloadCBA, scheduleDownloadCBA, uploadCBA);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "mail()->initFiletransfer() has returned error: %d\n", ret );
		)
	}
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */

#ifdef HAVE_DEPLOYMENT_UNIT
	DU_CB_AfterDownload = &processDeploymentUnit;
	/* Load deployment unit transfer list */
	initDeploymentUnitTransfer(DU_CB_AfterDownload);
#endif /* HAVE_DEPLOYMENT_UNIT */

	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
#endif /* HAVE_FILE */

#ifdef HAVE_SCHEDULE_INFORM
	/* Load scheduleInformList */
	initScheduleInform();
	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
#endif /* HAVE_SCHEDULE_INFORM */

#ifdef VOIP_DEVICE
	/* Initialize Voice over IP data */
	initVoIP();
#endif /* VOIP_DEVICE */

	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
	//printProcMemStatus(1);
	soapSetup(&soap);

#if defined(WITH_OPENSSL) || defined(WITH_GNUTLS)

#if defined(WITH_CLIENT_SSLAUTH) && !defined(WITH_SERVER_SSLAUTH)
	int flag;
	char * keyFile = (isFileExists(CLIENT_AUTHENTICATION_KEYFILE_NAME) ? CLIENT_AUTHENTICATION_KEYFILE_NAME : NULL);
	char * keyFilePassword = ( keyFile ? CLIENT_AUTHENTICATION_KEYFILE_PASSWORD : NULL );
	if (!keyFile)
	{
		flag = SOAP_SSL_NO_AUTHENTICATION;
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_MAIN, "KeyFile for client authentication isn't found. soap_ssl_client_context() will be called with SOAP_SSL_NO_AUTHENTICATION.\n");
		)
	}
	else
		flag = SOAP_SSL_REQUIRE_CLIENT_AUTHENTICATION | SOAP_SSLv3_TLSv1;

	if((ret = soap_ssl_client_context (&soap,
								flag,
								keyFile,			 	/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
								keyFilePassword, 			/* password to read the key file (not used with GNUTLS) */
								NULL, 					/* cacert file to store trusted certificates (needed to verify server) */
								NULL, 					/* capath to directory with trusted certificates */
								NULL 					/* if randfile!=NULL: use a file with random data to seed randomness */
								)))
	{
		soap_print_fault(&soap, stderr);
		efreeAllTypedMem(0);
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "main()->soap_ssl_client_context() has returned ret = %d. Exit.\n", ret );
		)
#ifdef HAVE_COLOR_SCREEN_LOG
		printf ("\033[%um", C_Reset);
#endif /* HAVE_COLOR_SCREEN_LOG */
		exit(0);
	}
#endif /* defined(WITH_CLIENT_SSLAUTH) && !defined(WITH_SERVER_SSLAUTH) */

#if defined(WITH_SERVER_SSLAUTH) && !defined(WITH_CLIENT_SSLAUTH)
	int flag;
	
	//char * cacertFile = (isFileExists(SERVER_AUTHENTICATION_CACERT_FILE_NAME) ? SERVER_AUTHENTICATION_CACERT_FILE_NAME : NULL);
	char * cacertFile = (isFileExists(dimclientConfStruct.SslCertPath_Acs) ? SERVER_AUTHENTICATION_CACERT_FILE_NAME : NULL);
	if (!cacertFile)
	{
		flag = SOAP_SSL_NO_AUTHENTICATION;
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_MAIN, "cacert file for server authentication isn't found. soap_ssl_client_context() will be called with SOAP_SSL_NO_AUTHENTICATION.\n");
		)
	}
	else
		flag = SOAP_SSL_REQUIRE_SERVER_AUTHENTICATION | SOAP_SSLv3_TLSv1;

	if((ret = soap_ssl_client_context (&soap,
								flag,
								NULL,					 	/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
								NULL, 						/* password to read the key file (not used with GNUTLS) */
								cacertFile,					/* cacert file to store trusted certificates (needed to verify server) */
								NULL, 						/* capath to directory with trusted certificates */
								NULL 						/* if randfile!=NULL: use a file with random data to seed randomness */
								)))
	{
		soap_print_fault(&soap, stderr);
		efreeAllTypedMem(0);
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "main()->soap_ssl_client_context() has returned ret = %d. Exit.\n", ret );
		)
#ifdef HAVE_COLOR_SCREEN_LOG
		printf ("\033[%um", C_Reset);
#endif /* HAVE_COLOR_SCREEN_LOG */
		exit(0);
	}
#endif /* defined(WITH_SERVER_SSLAUTH) && !defined(WITH_CLIENT_SSLAUTH) */

#if !defined(WITH_CLIENT_SSLAUTH) && !defined(WITH_SERVER_SSLAUTH)
	if((ret = soap_ssl_client_context (&soap,
								SOAP_SSL_NO_AUTHENTICATION,
								NULL,					/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
								NULL, 					/* password to read the key file (not used with GNUTLS) */
								NULL, 					/* cacert file to store trusted certificates (needed to verify server) */
								NULL, 					/* capath to directory with trusted certificates */
								NULL 					/* if randfile!=NULL: use a file with random data to seed randomness */
								)))
	{
		soap_print_fault(&soap, stderr);
		efreeAllTypedMem(0);
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "main()->soap_ssl_client_context() has returned ret = %d. Exit.\n", ret );
		)
#ifdef HAVE_COLOR_SCREEN_LOG
		printf ("\033[%um", C_Reset);
#endif /* HAVE_COLOR_SCREEN_LOG */
		exit(0);
	}
#endif /* !defined(WITH_CLIENT_SSLAUTH) && !defined(WITH_SERVER_SSLAUTH) */

#if defined(WITH_CLIENT_SSLAUTH) && defined(WITH_SERVER_SSLAUTH)
	int flag = SOAP_SSLv3_TLSv1;
	char * keyFile = (isFileExists(CLIENT_AUTHENTICATION_KEYFILE_NAME) ? CLIENT_AUTHENTICATION_KEYFILE_NAME : NULL);
	char * keyFilePassword = ( keyFile ? CLIENT_AUTHENTICATION_KEYFILE_PASSWORD : NULL );
	char * cacertFile = (isFileExists(SERVER_AUTHENTICATION_CACERT_FILE_NAME) ? SERVER_AUTHENTICATION_CACERT_FILE_NAME : NULL);
	if (!keyFile && !cacertFile)
	{
		flag = SOAP_SSL_NO_AUTHENTICATION;
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_MAIN, "KeyFile for client authentication and cacert file for server authentication aren't found. "
						"soap_ssl_client_context() will be called with flag SOAP_SSL_NO_AUTHENTICATION.\n");
		)
	}
	else
	{
		if (keyFile)
		{
			flag = flag | SOAP_SSL_REQUIRE_CLIENT_AUTHENTICATION;
		}
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_WARN, DBG_MAIN, "KeyFile for client authentication isn't found. "
							"soap_ssl_client_context() will be called WITHOUT flag SOAP_SSL_REQUIRE_CLIENT_AUTHENTICATION.\n");
			)
		}

		if (cacertFile)
		{
			flag = flag | SOAP_SSL_REQUIRE_SERVER_AUTHENTICATION;
		}
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_WARN, DBG_MAIN, "cacert file for server authentication isn't found. "
							"soap_ssl_client_context() will be called WITHOUT flag SOAP_SSL_REQUIRE_SERVER_AUTHENTICATION.\n");
			)
		}
	}

	if((ret = soap_ssl_client_context (&soap,
								flag, //SOAP_SSL_REQUIRE_CLIENT_AUTHENTICATION | SOAP_SSL_REQUIRE_SERVER_AUTHENTICATION | SOAP_SSLv3_TLSv1,
								keyFile,			 	/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
								keyFilePassword, 		/* password to read the key file (not used with GNUTLS) */
								cacertFile, 			/* cacert file to store trusted certificates (needed to verify server) */
								NULL, 					/* capath to directory with trusted certificates */
								NULL 					/* if randfile!=NULL: use a file with random data to seed randomness */
								)))
	{
		soap_print_fault(&soap, stderr);
		efreeAllTypedMem(0);
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "main()->soap_ssl_client_context() has returned ret = %d. Exit.\n", ret );
		)
#ifdef HAVE_COLOR_SCREEN_LOG
		printf ("\033[%um", C_Reset);
#endif /* HAVE_COLOR_SCREEN_LOG */
		exit(0);
	}
#endif /* defined(WITH_CLIENT_SSLAUTH) && defined(WITH_SERVER_SSLAUTH) */

#endif /* defined(WITH_OPENSSL) || defined(WITH_GNUTLS) */

	pthread_attr_init(&threadAttr);
	pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_DETACHED);

#ifdef HAVE_HOST
	soap_init(&hostSoap);
	hostSoap.proxy_host = proxy_host[0] ? proxy_host : NULL;
	hostSoap.proxy_port = proxy_port;
	hostSoap.proxy_userid = proxy_userid[0] ? proxy_userid : NULL;
	hostSoap.proxy_passwd = proxy_passwd[0] ? proxy_passwd : NULL;
	hostSoap.proxy_http_version = proxy_version[0] ? proxy_version : NULL;
	pthread_create(&hostTid, &threadAttr, hostHandler, (void *) &hostSoap);
	/* Wait for x seconds the hostHandler could bind the port */
	handlerWait.tv_sec = time(NULL) + HOST_HANDLER_WAIT_SECS;
	handlerWait.tv_nsec = 0;
	pthread_mutex_lock(&hostHandlerMutexLock);
	if (pthread_cond_timedwait(&hostHandlerStarted, &hostHandlerMutexLock, &handlerWait) == ETIMEDOUT)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "Host handler binds failed\n" );
		)
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_MAIN, "Host handler binds successfully\n" );
		)
	}
	pthread_mutex_unlock(&hostHandlerMutexLock);
#endif /* HAVE_HOST */

#ifdef HAVE_CONNECTION_REQUEST
	runCRhandler(proxy_host, proxy_port, proxy_userid, proxy_passwd, proxy_version);

	// Create udpCRhandler thread
	pthread_create (&udpCRhandlerTid, &threadAttr, udpCRhandler, NULL);
#endif /* HAVE_CONNECTION_REQUEST */

#ifdef HAVE_KICKED
	soap_init(&kickedSoap);
	kickedSoap.proxy_host = proxy_host[0] ? proxy_host : NULL;
	kickedSoap.proxy_port = proxy_port;
	kickedSoap.proxy_userid = proxy_userid[0] ? proxy_userid : NULL;
	kickedSoap.proxy_passwd = proxy_passwd[0] ? proxy_passwd : NULL;
	kickedSoap.proxy_http_version = proxy_version[0] ? proxy_version : NULL;
	pthread_create (&kickedTid, &threadAttr, kickedHandler, (void *) &kickedSoap);
	/* Wait for x seconds the kickedHandler could bind the port */
	handlerWait.tv_sec = time(NULL) + KICKED_HANDLER_WAIT_SECS;
	handlerWait.tv_nsec = 0;
	pthread_mutex_lock(&kickedHandlerMutexLock);
	if(pthread_cond_timedwait( &kickedHandlerStarted, &kickedHandlerMutexLock, &handlerWait ) == ETIMEDOUT)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "Kick handler binds failed\n" );
		)
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_MAIN, "Kick handler binds successfully\n" );
		)
	}
	pthread_mutex_unlock(&kickedHandlerMutexLock);
#endif /* HAVE_KICKED */

#ifdef HAVE_GET_ALL_QUEUED_TRANSFERS
	soap_init(&transfersSoap);
	transfersSoap.proxy_host = proxy_host[0] ? proxy_host : NULL;
	transfersSoap.proxy_port = proxy_port;
	transfersSoap.proxy_userid = proxy_userid[0] ? proxy_userid : NULL;
	transfersSoap.proxy_passwd = proxy_passwd[0] ? proxy_passwd : NULL;
	transfersSoap.proxy_http_version = proxy_version[0] ? proxy_version : NULL;
	pthread_create (&transfersTid, &threadAttr, transfersHandler, (void *) &transfersSoap);
	/* Wait for x seconds the transfersHandler could bind the port */
	handlerWait.tv_sec = time(NULL) + TRANSFERS_HANDLER_WAIT_SECS;
	handlerWait.tv_nsec = 0;
	pthread_mutex_lock(&transfersHandlerMutexLock);
	if(pthread_cond_timedwait( &transfersHandlerStarted, &transfersHandlerMutexLock, &handlerWait ) == ETIMEDOUT)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "Transfer handler binds failed\n" );
		)
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_MAIN, "Transfer handler binds successfully\n" );
		)
	}
	pthread_mutex_unlock(&transfersHandlerMutexLock);
#endif /* HAVE_GET_ALL_QUEUED_TRANSFERS */

	pthread_create (&timeTid, &threadAttr, timeHandler, NULL);
#if defined(HAVE_SCHEDULE_INFORM)
	pthread_create (&scheduleInformTimerTid, &threadAttr, scheduleInformTimeHandler, NULL);
#endif /* HAVE_SCHEDULE_INFORM */

#ifdef HAVE_FILE
#if defined(HAVE_DEPLOYMENT_UNIT) && defined(HAVE_FILE_DOWNLOAD)
	pthread_create (&deploymentUnitTimerTid, &threadAttr, deploymentUnitTimeHandler, NULL);
#endif /* HAVE_DEPLOYMENT_UNIT && HAVE_FILE_DOWNLOAD */
#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
	pthread_create (&transferTimerTid, &threadAttr, transferTimerHandler, NULL);
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */
#endif /* HAVE_FILE */


#ifdef HAVE_DIAGNOSTICS
	pthread_create (&diagTid, &threadAttr, diagnosticsThread, NULL);
#endif /* HAVE_DIAGNOSTICS */

#ifdef WITH_STUN_CLIENT
	runStunHandler();
#endif /* WITH_STUN_CLIENT */

#ifdef HAVE_UDP_ECHO
	pthread_create (&udpEchoTid, &threadAttr, udpEchoHandler, NULL);
#endif /* HAVE_UDP_ECHO */

#ifdef HAVE_NOTIFICATION
	/* Initialize active and passive notification handler */
	pthread_create(&passive_notification_id, &threadAttr, passiveNotificationHandler, NULL); /*thread for passive notification*/
	pthread_create(&active_notification_id, &threadAttr, activeNotificationHandler, NULL); /*thread for active notification*/
#endif /* HAVE_NOTIFICATION */

	/* free all temporary allocated memory during setup */
	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);

	srand(time(NULL));

	cleanRebootIdx();

	//printProcMemStatus(2);
	for (;;)
	{
		/* Initial Call to Server */
		delayCnt = 0;
		ret = OK;

		ret = check_log_files_size(dimclientConfStruct.HTTPLogFilePathName, dimclientConfStruct.maxHTTPLogSize, dimclientConfStruct.maxHTTPLogBackupCount);
		if(ret == OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_MAIN, "check_log_files_size() of file '%s' is successful.\n", dimclientConfStruct.HTTPLogFilePathName);
			)
		}
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_MAIN, "Backup of file '%s' is NOT successful, ret = %d\n", dimclientConfStruct.HTTPLogFilePathName, ret);
			)
		}

		ret = check_log_files_size(dimclientConfStruct.TESTLogFilePathName, dimclientConfStruct.maxTestLogSize, dimclientConfStruct.maxTestLogBackupCount);
		if (ret == OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_MAIN, "check_log_files_size() of file '%s' is successful.\n", dimclientConfStruct.TESTLogFilePathName);
			)
		}
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_MAIN, "Backup of file '%s' is NOT successful, ret = %d\n", dimclientConfStruct.TESTLogFilePathName, ret);
			)
		}

		setSuportedRPCInfoFromCWMPVersion(&suportedRPC, strstr(cwmp_version_ns, "cwmp"));

		while(true)
		{
			//printProcMemStatus(3);
			pthread_mutex_lock(&informLock);
			pthread_mutex_lock(&paramLock);
			getServerURL();
#ifdef WITH_USE_SQLITE_DB
			executeDBCommand(BEGIN);
#endif
			ret = dimarkMain (&soap, delayCnt);
#ifdef WITH_USE_SQLITE_DB
			executeDBCommand(END);
#endif
#if defined( WITH_DEVICE_ROOT ) && !defined( DEVICE_WITHOUT_GATEWAY )
			if( ret != SOAP_OK )
			{
				DHCP_Discover_dimarkMain();
			}
#endif /* defined( WITH_DEVICE_ROOT ) && !defined( DEVICE_WITHOUT_GATEWAY ) */

			//printProcMemStatus(4);

			if (ret != OK)
			{
				/* the mutexes are unlocked in the cond_timed_wait function */
				pthread_mutex_lock(&mutexGo);

				pthread_mutex_unlock(&paramLock);
				pthread_mutex_unlock(&informLock);


				incSdRetryCount ();
				//retryDelay.tv_sec = expWait (delayCnt++);
				int delaySec = expWait (delayCnt++);
				retryDelay.tv_sec = time(NULL) + delaySec;
				retryDelay.tv_nsec = 0;

				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_MAIN, "dimarkMain() has returned ret = %d.\n", ret);
						dbglog (SVR_INFO, DBG_MAIN,  "Retry Policy: Retry count = %d, next attempt will be in %d sec.\n", delayCnt, delaySec);
				)

				if(pthread_cond_timedwait(&condGo, &mutexGo, &retryDelay) == ETIMEDOUT)
				{
					DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_MAIN, "DelayCnt: %d. ***** Inform activation by timeout according to \"Session Retry Policy\" *****\n", delayCnt);
					)
				}
				else
				{
					DEBUG_OUTPUT (
							dbglog (SVR_DEBUG, DBG_MAIN, "DelayCnt: %d.  ***** Inform activation by signal *****\n", delayCnt);
					)
				}
				pthread_mutex_unlock(&mutexGo);
			}
			else
			{
#ifdef HAVE_NOTIFICATION
				/* save last inform time */
				setLastInformTime(time(NULL));
#endif
				if(getRebootIdx())
				{
					// execute rebootAccess() under locked mutexs to be sure that other thread doesn't change DB structure and didn't begin a transaction
					rebootAccess();
				}

				/* the mutexes are unlocked in the cond_timed_wait function */
				pthread_mutex_lock(&mutexGo);

				pthread_mutex_unlock(&paramLock);
				pthread_mutex_unlock(&informLock);
				break;
			}
		} /* while */

		/* mutexGo is unlocked and we wait for condGo
		 * after cond wait mutexGo is locked */

		periodicInformInterval = getPeriodicInterval();
		if (periodicInformInterval) //if PeriodicInformEnable is True then periodicIntervalDelay will be > 0
			getPeriodicTime(&periodicIntervalTime);
		else
			periodicIntervalTime.tv_sec = periodicIntervalTime.tv_usec = 0;

		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
		now = time(NULL);

		/* Handle PeriodicIntervalTime
		 * at 	= actual time in seconds
		 * pit 	= periodicInformTime in seconds
		 * pi 	= periodicInformInterval
		 * npi  = time in seconds to next periodic inform call
		 *
		 * npi = at <= pit ? (pit - at)%pi : pi - (at - pit)%pi
		 */
		if( periodicIntervalTime.tv_sec > 0 )
		{
			periodicIntervalDelay = (now <= periodicIntervalTime.tv_sec) ?
					(periodicIntervalTime.tv_sec - now) % periodicInformInterval :
					periodicInformInterval - ((now - periodicIntervalTime.tv_sec) % periodicInformInterval);
			if (periodicIntervalDelay == 0)
				periodicIntervalDelay = periodicInformInterval;
		}
		else
			periodicIntervalDelay = periodicInformInterval;

		/* periodicIntervalDelay > 0 periodicInform is enabled */
		if( periodicIntervalDelay > 0 )
		{
			/* save time for pereodic inform */
			nextPeriodicTime.tv_sec = now + periodicIntervalDelay;
			nextPeriodicTime.tv_nsec = 0;

			if(pthread_cond_timedwait(&condGo, &mutexGo, &nextPeriodicTime) == ETIMEDOUT)
			{
				pthread_mutex_lock(&informLock);
				addEventCodeSingle (EV_PERIODIC);
				pthread_mutex_unlock(&informLock);

				DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_MAIN, " ***** Inform activation by timeout *****\n");
				)
			}
			else
			{
				DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_MAIN, " ***** Inform activation by signal *****\n");
				)
			}
			pthread_mutex_unlock(&mutexGo);
		}
		else
		{
			int ret;
			/* PeriodicInform is disabled, just wait for signals from ACS */
			if ( (ret = pthread_cond_wait ( &condGo, &mutexGo )) == 0 )
			{
				DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_MAIN, " ***** Inform activation by signal *****\n");
				)
			}
			else
			{
				DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_MAIN, "main()->pthread_cond_wait() error: %d\n", ret);
				)
			}
			pthread_mutex_unlock(&mutexGo);
		} /* else */
	} /* for */
	return 0;
}


/* Mainloop for one transaction with the ACS */
static int dimarkMain(struct soap *soap, unsigned short delayCnt)
{
	int ret;
	int last_hold_value = 0;

	prevAuthenticationSoapError = 0; //clean this value.

	struct ArrayOfParameterValueStruct *informs = (struct ArrayOfParameterValueStruct *) emallocTemp(sizeof(struct ArrayOfParameterValueStruct), 6);
	struct ArrayOfEventStruct *eventList = (struct ArrayOfEventStruct *) emallocTemp(sizeof(struct ArrayOfEventStruct), 7);
	struct DeviceId *dev = getDeviceId();

	if (!informs || !eventList || !dev)
	{
		DEBUG_OUTPUT(
				dbglog (SVR_ERROR, DBG_MAIN, "dimarkMain(): 'informs', 'eventList' and 'dev' variables cann't be NULL.\n");
		)
		return -1;
	}

	informs->__size = 0;
	eventList->__size = 0;

	ret = getInformParameters(informs);
	if(ret != OK)
	{
		DEBUG_OUTPUT(
				dbglog (SVR_ERROR, DBG_MAIN, "dimarkMain(): Get Inform Parameters completed with error: %d\n", ret);
		)
		return ret;
	}

	ret = getEventCodeList(eventList);
	if(ret != OK)
	{
		DEBUG_OUTPUT(
				dbglog (SVR_ERROR, DBG_MAIN, "dimarkMain(): Get Event Code List completed with error: %d\n", ret);
		)
	}

	setAsyncInform(false); // The session is started, clear 'asyncInform' flag.

	/* Reset the soap->error variable because it is not cleared by GSOAP */
	soap->error = SOAP_OK;
	soap_errno = SOAP_OK;

	/* Call the preSessionCallbacks */
	executeCallback(&preSessionCbList);
	DEBUG_OUTPUT(
			dbglog (SVR_INFO, DBG_MAIN,	"\n\t\t\t\t\t============================="
										"\n\t\t\t\t\tStarting of connection to ACS"
										"\n\t\t\t\t\t=============================\n");
	)
	DEBUG_OUTPUT(
			dbglog (SVR_DEBUG, DBG_MAIN,	"Dimclient will use the CWMP version: \"%s\"\n", strstr(cwmp_version_ns, "cwmp"));
	)
	sessionStart();

	ret = sendInform(soap, informs, eventList, dev);
	if(ret != SOAP_OK)
	{
		/* if we have fault from ACS on Inform */
		if(soap->error == SOAP_SVR_FAULT)
			getFaultFromACS(soap);

		sessionEnd();
		DEBUG_OUTPUT(
				dbglog (SVR_ERROR, DBG_MAIN,"sendInform() has returned ret = %d, soap->error = %d"
						"\n\t\t\t\t\t======================"
						"\n\t\t\t\t\tFail to connect to ACS"
						"\n\t\t\t\t\t======================\n", ret, soap->error);
		)
		freeMemory(soap);
		//if(acsConnStatus != )
		
		notifyEvent_hostIf(IARM_BUS_TR69Agent_ACS_CONN_EVENT, ACS_FAILED_TO_CONNECT);	
		return ERR_NO_INFORM_DONE;
	}

//  TODO: Check: is this code needed?
	if (!soap->keep_alive)
	{
		soap_closesock(soap);
		soap->keep_alive = 1;
	}

	// Analyze the cwmp version in the responded packet (from ACS):
	char * tmps = ( soap->local_namespaces && !strcmp(soap->local_namespaces[4].id, "cwmp") )? soap->local_namespaces[4].out : NULL;
	int dif;
	if (tmps && (dif = strcmp(cwmp_version_ns, tmps)))
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_MAIN, "dimarkMain(): ACS has returned other CWMP version: \"%s\"\n", strstr(tmps, "cwmp"));
		)

		if ((dif > 0) && (OK == setSuportedRPCInfoFromCWMPVersion(&suportedRPC, strstr(tmps, "cwmp"))) )
		{
			strcpy(cwmp_version_ns, tmps);
			DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_MAIN, "dimarkMain(): Dimclient will use the changed CWMP version: \"%s\"\n", strstr(cwmp_version_ns, "cwmp"));
			)
		}
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_WARN, DBG_MAIN, "dimarkMain(): Dimclient can not use other CWMP version \"%s\", and will use CWMP version : \"%s\"\n",
							strstr(tmps, "cwmp"), strstr(cwmp_version_ns, "cwmp"));
			)
		}
	}

	DEBUG_OUTPUT(
			dbglog (SVR_INFO, DBG_MAIN,	"\n\t\t\t\t\t==========================="
										"\n\t\t\t\t\tConnection to ACS succeeded"
										"\n\t\t\t\t\t===========================\n");
	)
	notifyEvent_hostIf(IARM_BUS_TR69Agent_ACS_CONN_EVENT, ACS_CONNECTED);	
//	printf(" Add Boot event and file to check.\n");

	/*Check for bootstrap file, if successfully connected to ACS, then create and check*/
	createAndCheckBootStarpEventFile(dimclientConfStruct.BootstrapFilePathName);

#ifdef	WITH_COOKIES
	cookieHandler (soap);
#endif /* WITH_COOKIES */

	last_hold_value = getSdHoldRequests();

	usleep(DELAY_BEFORE_NEXT_RPC); //delay for successful socket closing (if needed) before sending of next RPC

	/* Clear the delayed file transfers and send TransferComplete Message
	 * clear the delayed schedules and send Inform Message
	 * Send Kicked Message
	 * Send RequestDownload Message
	 * Send GetRPCMethods Message
	 * before we send an EmptyBodyMessage
	 * but only if the HoldRequest Flag is not set to 1
	 */
	if (getSdHoldRequests() == 0)
	{
		ret = OK;
		do // execute 1 iteration:
		{
#ifdef HAVE_RPC_METHODS
			if ( (ret = clearGetRPCMethods(soap)) != OK)
				break;
#endif /* HAVE_RPC_METHODS */
#ifdef HAVE_FILE
#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
			if ( (ret = clearDelayedFiletransfers(soap)) != OK)
				break;
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */
#endif /* HAVE_FILE */
#ifdef HAVE_DEPLOYMENT_UNIT
			if ( (ret = clearDeploymentUnitTransfers(soap)) != OK)
				break;
#endif /* HAVE_DEPLOYMENT_UNIT */
#ifdef HAVE_SCHEDULE_INFORM
			if ( (ret = clearDelayedScheduleInform(soap)) != OK)
				break;
#endif /* HAVE_SCHEDULE_INFORM */
#ifdef HAVE_KICKED
			if ( (ret = clearKicked(soap)) != OK)
				break;
#endif /* HAVE_KICKED */
#ifdef HAVE_REQUEST_DOWNLOAD
			if ( (ret = clearRequestDownload(soap)) != OK)
				break;
#endif /* HAVE_REQUEST_DOWNLOAD */
		} while(false);

		if (ret)
		{
			// return if error
			clearFault();
			freeMemory(soap);
			return ret;
		}

		clearFault();
		soap_destroy(soap);
		soap_dealloc(soap, NULL);
	}

	if (sendEmptyBody(soap) != SOAP_OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "dimarkMain(): Inform completed error: %d\n", soap->error);
		)
		sessionEnd();
		ret = soap->error;
	}
	else
	{
		prevSentHTTPpacketBody[0] = '\0';

		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_MAIN, "dimarkMain(): Inform completed successfully\n");
		)

		/* Loops until all Requests from the ACS are completed */
		while(true)
		{
			ret = dimarkSoap_serve(soap, &last_hold_value);

			if(ret == BROKEN_TRANSACTION)
			{
				sendEmptyBody(soap);
				continue;
			}

			DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_MAIN, "dimarkMain(): (1)dimarkSoap_serve ret: %d %d\n", ret, soap->error);
			)

			if(ret == SOAP_FAULT)
			{
				createFault(soap, soap->error);
				sendFault(soap);
				clearFault();
				continue;
			}
			if(ret == CONTINUE_TRANSACTION)
			{
				clearFault();
				continue;
			}
			if(ret == FINISH_TRANSACTION)
			{
				ret = OK;
				break;
			}
			if(ret == HOLD_REQUEST_DETECTED && last_hold_value)
			{
				if(sendEmptyBody(soap) != SOAP_OK)
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_MAIN, "sendEmptyBody() error: %d\n", soap->error);
					)
					ret = soap->error;
					break;
				}
				else
				{
					last_hold_value = 0;
					continue;
				}
			}

			if(ret < 0)
			{
				ret = soap->error;
				break;
			}
		}

		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
		/* execute registered Callbacks */
		executeCallback(&postSessionCbList);

		sessionEnd();

		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_MAIN, "About DimClient: %s\n", info_for_soap_header);
		)

		printf(sessionInfo());

		executeCallback(&cleanUpCbList);
		DEBUG_OUTPUT (
				dbglog (SVR_DEBUG, DBG_MAIN, "dimarkSoap_serve() end error: %d tag:%s\n", ((struct soap *) soap)->error, ((struct soap *) soap)->tag);
		)
	}

	clearFault();
	freeMemory(soap);
	return ret;
}

/* Created by San.
 * This function allows to the Dimclient to not close the TCP connection when 401(407) error is received from ACS.
 * Without this function gSOAP closes TCP connection when 401(407) error is received - in any case (either "Connection: close" or no).
 * */
static int dimark_fdisconnect(struct soap* soap)
{
	if (soap->error == 401 || soap->error == 407)
		return soap->error;
	return SOAP_OK;
}

/* Initialize Soap Environment
 */
static void soapSetup(struct soap *soap)
{
	//signal(SIGPIPE, sigpipe_handler); Is not needed because MSG_NOSIGNAL is used.
	soap_init(soap);

	soap_set_namespaces(soap, Dim_namespaces);

	soap->socket_flags = MSG_NOSIGNAL;
	soap_set_omode(soap, SOAP_IO_KEEPALIVE | SOAP_XML_TREE | SOAP_IO_STORE);
	soap_set_imode(soap, SOAP_IO_KEEPALIVE);

	soap->recv_timeout = 300;
	soap->send_timeout = 60;
	soap->connect_timeout = 3;	//this property was tested on Linux Suse.
								//As result:
								//When I set soap->connect_timeout < 3 sec and connected to the wrong URL,
								//the connection lasted soap->connect_timeout sec.
								//But, when I set soap->connect_timeout > 3 sec, the connection lasted 3 sec always.

	/* We send no Header in our Requests */
	soap->header = NULL;
	soap->actor = NULL;

	/* [v4.0.1c2] */
	/* Proxy */
	soap->proxy_host = proxy_host[0] ? proxy_host : NULL;
	soap->proxy_port = proxy_port;
	soap->proxy_userid = proxy_userid[0] ? proxy_userid : NULL;
	soap->proxy_passwd = proxy_passwd[0] ? proxy_passwd : NULL;
	soap->proxy_http_version = proxy_version[0] ? proxy_version : NULL;

	/* Register Digest Authentication Plugin */
	soap_register_plugin(soap, http_da);

	soap->fdisconnect = dimark_fdisconnect;

	/* for setsockopt BINDTODEVICE */
#ifdef BINDING_TO_INTERFACE_NAME_IS_ENABLED
	// Intel - Bind TCP socket options
	if (bind_interface_name[0])
	{
		strncpy(soap->bind_interface, bind_interface_name, sizeof(soap->bind_interface));
		soap->bind_interface[sizeof(soap->bind_interface)-1] = '\0';
	}
	else
		memset(soap->bind_interface, 0, sizeof(soap->bind_interface));
#if defined (WITH_IPV6)
	// Intel - Bind TCP socket options in case of WITH_IPV6 and IPv6 or IPv4 working mode
	if (acsUrlIsIpv6)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_MAIN, "ACS URL contains IPv6 address - set soap->bind_interface_addr to %s \n", host);
		)
		strncpy(soap->bind_interface_addr, host, sizeof(soap->bind_interface_addr));
		soap->bind_interface_addr[sizeof(soap->bind_interface_addr)-1] = '\0';
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_MAIN, "ACS URL contains IPv4 address %s \n", host);
		)
		soap->bind_interface_addr[0] = '\0';
	}
#endif /* (WITH_IPV6)*/
#endif /* BINDING_TO_INTERFACE_NAME_IS_ENABLED */

}


/* Send a SOAP Message with an empty body part
 * This signals the ACS, after the inform message, to start the transactions from the ACS.
 * */
static int sendEmptyBody(struct soap *soap)
{
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_MAIN, "Send empty body\n");
	)

	soap->encodingStyle = NULL;

	// Restore Authentication data:
	authorizeClient(soap, &info);

	soap->keep_alive = 1;

	/*TR-069 requires: An empty HTTP POST MUST NOT contain a SOAPAction header.*/
	return (soap_connect(soap, getServerURL(), NULL) || soap_end_send(soap));
}


/* Send a Inform Command to the ACS  and wait for the response */
static int sendInform(struct soap *soap, struct ArrayOfParameterValueStruct *informs, struct ArrayOfEventStruct *eventList, struct DeviceId *dev)
{
	int ret = OK;
	int maxEnvelopes;
	int idx;
	time_t timet;
	struct tm *timeofday;
	struct timeval curr_time;
	unsigned int SessionTimeout = DEFAULT_SESSION_TIMEOUT_IN_HEADER;

	if (!soap || !informs )
		return -1;

	local_sendInform_count++;
	time(&timet);
	timeofday = localtime(&timet);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_MAIN, "sendInform() enter count = %d  |  Time-stamp: %s", local_sendInform_count, asctime(timeofday));
	)

	maxEnvelopes = 0;
#ifndef ACS_REGMAN
	for (idx = 0; idx != informs->__size; idx++)
	{
		if (informs->__ptrParameterValueStruct[idx] != NULL)
		{
			switch (informs->__ptrParameterValueStruct[idx]->__typeOfValue)
			{
				case StringType:
				case Base64Type:
				case DefStringType:
				case DefBase64Type:
				case DefHexBinaryType:
				case hexBinaryType:
					DEBUG_OUTPUT(
							dbglog (SVR_DEBUG, DBG_MAIN, "InfP: %d %s = %s\n", idx, informs->__ptrParameterValueStruct[idx]->Name,
									(char*)informs->__ptrParameterValueStruct[idx]->Value);
					)
					break;
				case IntegerType:
				case DefIntegerType:
				case BooleanType:
				case DefBooleanType:
					DEBUG_OUTPUT(
							dbglog (SVR_DEBUG, DBG_MAIN, "InfP: %d %s = %d\n", idx, informs->__ptrParameterValueStruct[idx]->Name,
									*((int*)informs->__ptrParameterValueStruct[idx]->Value));
					)
					break;
				case UnsignedIntType:
				case DefUnsignedIntType:
					DEBUG_OUTPUT(
							dbglog (SVR_DEBUG, DBG_MAIN, "InfP: %d %s = %u\n", idx, informs->__ptrParameterValueStruct[idx]->Name,
									*((unsigned int*)informs->__ptrParameterValueStruct[idx]->Value));
					)
					break;
				case DateTimeType:
				case DefDateTimeType:
					DEBUG_OUTPUT(
							dbglog (SVR_DEBUG, DBG_MAIN, "InfP: %d %s = %s\n", idx, informs->__ptrParameterValueStruct[idx]->Name,
									dateTime2s((struct timeval*)informs->__ptrParameterValueStruct[idx]->Value));
					)
					break;
				case UnsignedLongType:
				case DefUnsignedLongType:
					DEBUG_OUTPUT(
							dbglog (SVR_DEBUG, DBG_MAIN, "InfP: %d %s = %lu\n",	idx, informs->__ptrParameterValueStruct[idx]->Name,
									*((unsigned long*)informs->__ptrParameterValueStruct[idx]->Value));
					)
					break;
				default:
					break;
			}

		}
	}
#endif /* #ifndef ACS_REGMAN */
	soap->keep_alive = 1;

	gettimeofday(&curr_time, NULL);

	setAttrToHTTPandSOAPHeaders(soap);

	// It header->SessionTimeout is not NULL then value will be sent in the Envelope Header.
	// If NULL then this field will not be added to the Envelope Header.
	if (soap->header)
		soap->header->cwmp__SessionTimeout = &SessionTimeout;
		
	struct cwmp__InformResponse informResponse;
	unsigned int redirectCount = MAX_REDIRECT_COUNT + 1; //+1 to sent first packet
	while(redirectCount)
	{
		ret = soap_call_cwmp__Inform (soap, getServerURL(), "", dev, eventList, 1, curr_time, getSdRetryCount (), informs, &informResponse);
		prevSoapErrorValue = soap->error;
		if (ret != SOAP_OK)
		{
			switch (soap->error)
			{
				case 301:
				case 302:
				case 307:
					set_server_url_when_redirect(soap->endpoint);
					redirectCount--;
					continue;
					break;
				case 401:
				case 407:
					DEBUG_OUTPUT (
							dbglog (SVR_DEBUG, DBG_MAIN, "sendInform()->soap_call_cwmp__Inform(): HTTP Authentication required. soap->error=%d\n", soap->error);
					)
					setAttrToHTTPandSOAPHeaders(soap);
					ret = soap_call_cwmp__Inform (soap, getServerURL(), "", dev, eventList, 1, curr_time, getSdRetryCount (), informs, &informResponse);
					break;
				default:
					break;
			}
			break;
		}
		else
			break;
	}
	if (redirectCount==0 || ret != SOAP_OK)
	{
		char buf[1024] = {'\0'};
		char *p = buf;
		soap_sprint_fault(soap, buf, sizeof(buf));
		while ( (p = strchr(p, '\n')) )
			*p++ = ' ';
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "sendInform(): Exit. count=%d ret=%d soap->errnum=%d\n"
						"                                        FAULT: '%s'\n",
						local_sendInform_count, ret, (soap ? soap->errnum : 0), buf);
		)
		return ret;
	}

	if (soap->header )
	{
		if ( soap->header->cwmp__HoldRequests != 0 )
			setSdHoldRequests( (int)soap->header->cwmp__HoldRequests );
	}
	soap_destroy(soap);
	soap_dealloc(soap, NULL);

	clearSdRetryCount ();
	setSdMaxEnvelopes (informResponse.MaxEnvelopes);
	resetParameterStatus ();
	freeEventList(true);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_MAIN, "sendInform(): maxEnvelopes = %d\n", informResponse.MaxEnvelopes);
	)

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_MAIN, "sendInform(): Exit. count = %d, ret = SOAP_OK\n", local_sendInform_count);
	)
	return SOAP_OK;
}


static void sigpipe_handler(int x)
{
	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_MAIN, "Pipe broke %d\n", x);
	)
}

static void sigsegv_handler(int x)
{
	DEBUG_OUTPUT (
			dbglog (SVR_WARN, DBG_MAIN, "Segmentation fault was detected. sigsegv_handler() is executing.\n");
	)
#ifdef CUSTOMER_FINISHING_FUNCTION
	customerFunctionAtFinish();
#endif
	killAllThreads();
	unlock_typedMemList_mutex();
	cleanCallbackLists();
#ifdef HAVE_SCHEDULE_INFORM
	clearScheduleInformList();
#endif
#ifdef HAVE_FILE
#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
	freeTransferEntryMemory();
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */
#ifdef HAVE_DEPLOYMENT_UNIT
	freeDUEntryMemory();
#endif /* HAVE_DEPLOYMENT_UNIT */
#endif /* HAVE_FILE */
#ifdef VOIP_DEVICE
	// freeing memory after using VoIP Parameters and devices
	freeVoIPAllocatedMemory();
#endif /* VOIP_DEVICE */
	freeEventList(false);
	clearFault();
#ifdef WITH_USE_SQLITE_DB
	int ret = closeDBConnection(1);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_MAIN, "main()->closeDBConnection() 3 has returned ret = %d\n", ret );
	)
#endif /* WITH_USE_SQLITE_DB */
	doneAllSoapInstances();
	efreeAllTypedMem(0);
	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_MAIN, "Segmentation fault was detected. Signal %d. Exit.\n", x);
	)
	exit(0);
}

static void sigint_handler(int x)
{
	DEBUG_OUTPUT (
			dbglog (SVR_WARN, DBG_MAIN, "CTRL+C was pressed. sigint_handler() is executing.\n");
	)
#ifdef CUSTOMER_FINISHING_FUNCTION
	customerFunctionAtFinish();
#endif
	killAllThreads();
	unlock_typedMemList_mutex();
	cleanCallbackLists();
#ifdef HAVE_SCHEDULE_INFORM
	clearScheduleInformList();
#endif
#ifdef HAVE_FILE
#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
	freeTransferEntryMemory();
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */
#ifdef HAVE_DEPLOYMENT_UNIT
	freeDUEntryMemory();
#endif /* HAVE_DEPLOYMENT_UNIT */
#endif /* HAVE_FILE */
#ifdef VOIP_DEVICE
	// freeing memory after using VoIP Parameters and devices
	freeVoIPAllocatedMemory();
#endif /* VOIP_DEVICE */
	freeEventList(false);
	clearFault();
#ifdef WITH_USE_SQLITE_DB
	int ret = closeDBConnection(1);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_MAIN, "main()->closeDBConnection() 4 has returned ret = %d\n", ret );
	)
#endif /* WITH_USE_SQLITE_DB */
	doneAllSoapInstances();
	efreeAllTypedMem(0);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_MAIN, "CTRL+C was pressed. Exit.\n");
	)
	exit(0);
}

/** Mainloop to handle ACS SOAP requests.
 * As long as the connection is alive, the requests is executed and the response is sent back.
 * If the connection is closed by the ACS,
 *	by sending an empty request
 *	or a "Connection: close" in the HTTP header, a BROKEN_TRANSACTION is returned.
 *
 "Connection: close"				= close
 "Transfer-Encoding: chunked" 	= chunk
 "Soaplength: 0"					= noMsg
 "Soaplength: >0"				= msg

 close		noMsg		msg		chunk		noChunk
 -----------------------------------------------------------------------------
 FINISH_TRANSACTION		 t|f	      t								    t
 BROKEN_TRANSACTION		 t								   t
 */
static int dimarkSoap_serve(struct soap *soap, int *hold_value)
{
	int ret;
	static int error401_AlreadyWasHandledInPreviousPacket = false;
	static unsigned int redirectCount = MAX_REDIRECT_COUNT + 1; //+1 to sent first packet

	do
	{
		if (!soap)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_DEBUG, "dimarkSoap_serve(): pointer soap is NULL\n");
			)
			return SOAP_FAULT;
		}

		long startTime = getTime ();

		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_SOAP, "dimarkSoap_serve startTime: %ld\n", startTime);
		)

		// free unused memory
		soap_destroy(soap);
		soap_dealloc(soap, NULL);

		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);

		/* Commented below lines to fix the wrong ACS header
		   for XITHREE-1819. 
		Workaround-1:Start
		soap->error = OK; */
		/* [v4.0.0c10] */
		/* soap->length = 0xFFFF; 	
		Workaround-1:End
		*/

		soap->error = SOAP_OK;
		ret = soap_begin_recv(soap);
		prevSoapErrorValue = soap->error;
		DEBUG_OUTPUT (
				dbglog (SVR_DEBUG, DBG_SOAP, "dimarkSoap_serve()->soap_begin_recv() has returned result = %d\n", ret);
		)
		if (ret)
		{
			if (soap->error < SOAP_STOP)
			{
				if (redirectCount && (soap->error==301 || soap->error==302 || soap->error==307))
				{
					redirectCount--;
					set_server_url_when_redirect(soap->endpoint);
					error401_AlreadyWasHandledInPreviousPacket = false;
					soap_connect(soap, getServerURL(), DO_NOT_SEND_SOAPACTION);
					soap_send(soap, prevSentHTTPpacketBody);
					soap_end_send(soap);
					return CONTINUE_TRANSACTION;
				}

				if (!error401_AlreadyWasHandledInPreviousPacket && (soap->error == 401 || soap->error == 407) )
				{
					authorizeClient(soap, &info);
					soap->keep_alive = 1;
					soap_connect(soap, getServerURL(), DO_NOT_SEND_SOAPACTION);
					soap_send(soap, prevSentHTTPpacketBody);
					soap_end_send(soap);
					error401_AlreadyWasHandledInPreviousPacket = true;
					return CONTINUE_TRANSACTION;
				}

				if ((*hold_value == 1) && (soap->error == 204))
				{
					/*Don't close soap_closesock, if in previous soap-packet was holdrequest=true and we must to send "HTTP POST empty". */
					soap->keep_alive = 1;
					return HOLD_REQUEST_DETECTED;
				}

				if ( soap->error/100 != 2 ) // not in 200..299 range
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_SOAP, "dimarkSoap_serve()->soap_begin_recv() has returned error: %d\n", soap->error);
					)
					soap_closesock(soap);
					soap->keep_alive = 1;
					return -1;
				}
			}
		}

		redirectCount = MAX_REDIRECT_COUNT + 1; //+1 to sent first packet
		error401_AlreadyWasHandledInPreviousPacket = false;

		/* To identify an empty HTTP Header     take the Content-Length which is 0
		 * Does not work, because the server does not set the Content-Length when handling a normal Request
		 *
		 * if "Content-length: 0" is found
		 * and no "Transfer-Encoding: chunked" is found
		 * return a FINISH_TRANSACTION
		 * keep_alive dosn't matter */
		 /* [v4.0.0c10] */
		/* Commented to satisfy AL-Motive ACS wrong message: 204 with Content-Type.
		 if ((soap->length == 0 || soap->length == 0xFFFF) && !((soap->mode & SOAP_IO) == SOAP_IO_CHUNK)) */
		if (soap->status == 204 || ((soap->length == 0) && !((soap->mode & SOAP_IO) == SOAP_IO_CHUNK)))
		{
			soap->keep_alive = 0;
			soap_closesock(soap);
			return FINISH_TRANSACTION;
		}

		// use following code instead previous code, which is commented (fixing bug 18632)
		/* if "Connection: close" in http header, keep_alive is set to 0 by gsoap */
		if (soap->keep_alive == 0)
		{
			soap_closesock(soap);
			soap->keep_alive = 1;
		}

		if (soap_envelope_begin_in(soap) || soap_recv_header(soap) || soap_body_begin_in(soap) /*|| soap_serve_request(soap)*/)
		{
			DEBUG_OUTPUT (
					dbglog(SVR_ERROR, DBG_SOAP, "dimarkSoap_serve()->envelope_begin_in(): soap->error = %d, soap->tag = '%s'\n", soap->error, soap->tag);
			)

			/* check for an empty body message and finish the transaction */
			/* Workaround: Check for 500 and 400 error and finish the transcation. */
			if ((soap->error == 500 || soap->error == 400 || soap->error == SOAP_NO_METHOD) && soap->tag[0] == '\0')
			{
				soap_end_recv(soap);
				soap_closesock(soap);
				return FINISH_TRANSACTION;
			}
			else
			{
				authorizeClient(soap, &info);
				return SOAP_FAULT;
			}
		}
		else
		{
			authorizeClient(soap, &info);

			// Check the next element: whether is equal SPV
			unsigned int isParsedSPV = 0;
			soap_peek_element(soap);
			if (myStrCaseStr(soap->tag, "SetParameterValues"))
				isParsedSPV = 1;

			memset(last_parsed_param_name, 0, sizeof (last_parsed_param_name));
			if ((soap->error = soap_serve_request(soap)) != SOAP_OK)
			{
				// If INVALID PARAMETER VALUE is detected on gSOAP layer the soap->error == SOAP_TYPE
				if (isParsedSPV && soap->error == SOAP_TYPE && !strcasecmp(soap->tag, "Value"))
				{
					DEBUG_OUTPUT (
							dbglog(SVR_ERROR, DBG_SOAP, "dimarkSoap_serve()->soap_serve_request(): INVALID PARAMETER VALUE is detected on gSOAP layer during SPV parsing.\n"
									"                                        "
									"soap->error = %d, soap->tag = '%s', last_parsed_param_name = %s\n", soap->error, soap->tag, last_parsed_param_name);
					)
					soap->error = ERR_INVALID_ARGUMENT;
					addFault ("SetParameterValuesFault",  last_parsed_param_name, ERR_INVALID_PARAMETER_VALUE, NULL);
				}
				else
				{
					DEBUG_OUTPUT (
							dbglog(SVR_ERROR, DBG_SOAP, "dimarkSoap_serve()->soap_serve_request(): soap->error = %d, soap->tag = '%s'\n", soap->error, soap->tag);
					)
				}
				return SOAP_FAULT;
			}
		}
		*hold_value = getSdHoldRequests();

		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_SOAP, "Exit dimarkSoap_serve. Runtime: %ld\n", getTime () - startTime);
		)
	}
	while (soap->keep_alive);
	return SOAP_OK;
}

#ifdef	WITH_COOKIES
/* Handles cookies
 * Modify the server cookies otherwise the server can't identify us */
static int cookieHandler (struct soap *soap)
{
//	/* clear the cookie path extension, other the server can't recognize the client */
//	struct soap_cookie *cookie;
//	if (soap->cookies != NULL)
//	{
//		/* Set a cookie for the charCode conversion */
//		//cookie = (struct soap_cookie *) soap_set_cookie (soap, "CharCode", "293a1567c77f25780de94981d4b8b907ba280ee2baa0c4", NULL, "/");
//		//cookie->expire = 0L;
//
//		/* clear the cookie path */
//		cookie = soap->cookies;
//		while( cookie != NULL )
//		{
//			cookie->path = NULL;
//			cookie = cookie->next;
//		}
//	}
	return OK;
}
#endif /* WITH_COOKIES */


/* Parameter Initialization
 */
static int initParameters(bool bootstrap)
{
	int ret;
	ret = loadParameters(bootstrap);

	// On factory reset, the value of ParameterKey MUST	be set to empty.
	if (bootstrap)
		setParameter(PARAMETER_KEY, "");
	return ret;
}


/* Frees temporary allocated memory from gSOAP and Client
 */
static void freeMemory(struct soap *soap)
{
#ifdef WITH_COOKIES
			// Fix of raising of cookies number in the new session.
			// free all previous cookies.
	soap_free_cookies(soap);
#endif

	soap_destroy(soap);  // delete deserialized objects
	soap_end(soap); // Remove temporary data and deserialized data

	/* free the Digest data */
	http_da_release(soap, &info);

	/* free the memory allocated with emallocTemp() */
	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);

	/* free session memory */
	efreeTypedMemByType(MEM_TYPE_SESSION, 4);
}

static int sendFault(struct soap *soap)
{
	int ret;

	//soap_set_fault(soap);
	soap->keep_alive = 1;
	soap_serializeheader(soap);
	soap_serializefault(soap);
	if ( (ret = soap_begin_count(soap)) != SOAP_OK )
		return ret;

	if (soap->mode & SOAP_IO_LENGTH)
	{
		if ( (ret = soap_envelope_begin_out(soap)) != SOAP_OK )
			return ret;
		if ( (ret = soap_putheader(soap)) != SOAP_OK )
			return ret;
		if ( (ret = soap_body_begin_out(soap)) != SOAP_OK )
			return ret;
		if ( (ret = soap_putfault(soap)) != SOAP_OK )
			return ret;
		if ( (ret = soap_body_end_out(soap)) != SOAP_OK )
			return ret;
		if ( (ret = soap_envelope_end_out(soap)) != SOAP_OK )
			return ret;
	}

	if ( (ret = soap_end_count(soap)) != SOAP_OK )
		return ret;

	if (soap_connect(soap, getServerURL(), DO_NOT_SEND_SOAPACTION) || soap_envelope_begin_out(soap) || soap_putheader(soap) || soap_body_begin_out(soap)
			|| soap_putfault(soap) || soap_body_end_out(soap) || soap_envelope_end_out(soap))
	{
		return soap_closesock(soap);
	}

	if ( (ret = soap_end_send(soap)) != SOAP_OK )
		return ret;

	return OK;
}

#ifdef HAVE_RPC_METHODS
static int clearGetRPCMethods(struct soap *server)
{
	static bool first_rpc = true;
	int i, ret;
	void *_ = NULL;

	if (first_rpc)
	{
#ifdef HAVE_KICKED
		isKicked = false;
#endif /* HAVE_KICKED */

#ifdef HAVE_REQUEST_DOWNLOAD
		isRequestDownload = false;
#endif /* HAVE_REQUEST_DOWNLOAD */

#ifdef	 HAVE_AUTONOMOUS_TRANSFER_COMPLETE
		isAutonomousTransferComplete = false;
#endif /* HAVE_AUTONOMOUS_TRANSFER_COMPLETE */

#ifdef	 HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE
		isAutonomousDUStateChangeComplete = false;
#endif /* HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE */

		struct cwmp__GetRPCMethodsResponse response;
		unsigned int redirectCount = MAX_REDIRECT_COUNT + 1; //+1 to sent first packet
		setAttrToHTTPandSOAPHeaders(server);
		while(redirectCount)
		{
			ret = soap_call_cwmp__GetRPCMethods(server, getServerURL(), "", &response);
			prevSoapErrorValue = server->error;
			if (ret != SOAP_OK)
			{
				switch (server->error)
				{
					case 301:
					case 302:
					case 307:
						set_server_url_when_redirect(server->endpoint);
						redirectCount--;
						continue;
						break;
					case 401:
					case 407:
						DEBUG_OUTPUT (
								dbglog (SVR_DEBUG, DBG_REQUEST, "clearGetRPCMethods()->soap_call_cwmp__GetRPCMethods(): HTTP Authentication required. soap->error=%d\n", server->error);
						)
						setAttrToHTTPandSOAPHeaders(server);
						ret = soap_call_cwmp__GetRPCMethods(server, getServerURL(), "", &response);
						break;
					default:
						break;
				}
				break;
			}
			else
				break;
		}
		if (redirectCount==0 || ret != SOAP_OK)
		{
			DEBUG_OUTPUT (
					dbglog(SVR_ERROR, DBG_MAIN, "soap_call_cwmp__GetRPCMethods() has returned %d, soap->error=%d\n", ret, server->error);
			)
			return ret;
		}

		for (i = 0; i < response.MethodList.__size; i++)
		{
			if ( !((char *)response.MethodList.__ptrstring[i]) )
				continue;
#ifdef	 HAVE_AUTONOMOUS_TRANSFER_COMPLETE
			if (!strcmp("AutonomousTransferComplete", response.MethodList.__ptrstring[i]))
				isAutonomousTransferComplete = true;
#endif /* HAVE_AUTONOMOUS_TRANSFER_COMPLETE */

#ifdef	 HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE
			if(!strcmp("AutonomousDUStateChangeComplete", response.MethodList.__ptrstring[i]))
				isAutonomousDUStateChangeComplete = true;
#endif /* HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE */

#ifdef HAVE_REQUEST_DOWNLOAD
			if (!strcmp("RequestDownload", response.MethodList.__ptrstring[i]))
				isRequestDownload = true;
#endif /* HAVE_REQUEST_DOWNLOAD */

#ifdef HAVE_KICKED
			if (!strcmp("Kicked", response.MethodList.__ptrstring[i]))
				isKicked = true;
#endif /* HAVE_KICKED */
		}
	}
	first_rpc = false;

	return OK;
}
#endif /* HAVE_RPC_METHODS */


/* Authorize a client against an ACS which needs the Authorization Header
 * every time the clients sends a request */
int authorizeClient(struct soap *soap, struct http_da_info *info)
{
	struct http_da_info infoTmp = {NULL, NULL, NULL, NULL, NULL, NULL, NULL};
	int soapError = soap->error;

	if (!info)
		return -1;

	struct http_da_data *data = (struct http_da_data*)soap_lookup_plugin(soap, http_da_id);
	if (!data)
	{
		DEBUG_OUTPUT (
				dbglog(SVR_DEBUG, DBG_MAIN, "authorizeClient()->soap_lookup_plugin(): has returned no plugins.\n");
		)
	}

	DEBUG_OUTPUT (
			dbglog(SVR_DEBUG, DBG_MAIN, "authorizeClient() start: soapError = %d, previousSoapError = %d\n", soapError, prevAuthenticationSoapError);
	)

	if (soapError == 401 || soapError == 407)
	{
		prevAuthenticationSoapError = soapError;
		if (soapError == 401)
		{
			if (data && data->nonce && *data->nonce)
			{
				// Digest authenticate is required.
				DEBUG_OUTPUT (
						dbglog(SVR_DEBUG, DBG_MAIN, "authorizeClient(): Digest authenticate is required. soapError = 401, http_da_save() + http_da_restore() will be executed.\n");
				)
				http_da_save(soap, &infoTmp, (soap->authrealm ? soap->authrealm : authrealm), getUsername(), getPassword());
				if (info->authrealm && *info->authrealm)
					http_da_release(soap, info);
				memcpy(info, &infoTmp, sizeof(struct http_da_info));
				http_da_restore(soap, info);
			}
			else
			{
				// Basic authenticate is required.
				DEBUG_OUTPUT (
						dbglog(SVR_DEBUG, DBG_MAIN, "authorizeClient(): Basic authenticate is required. soapError = 401.\n");
				)
				soap->userid = getUsername();
				soap->passwd = getPassword();
			}
		}
		else
		{
			if (data && data->nonce && *data->nonce)
			{
				// Digest authentication is required by Proxy.
				DEBUG_OUTPUT (
						dbglog(SVR_DEBUG, DBG_MAIN, "authorizeClient(): Digest authentication is required by Proxy. soapError = 407, http_da_proxy_save() + http_da_proxy_restore() will be executed.\n");
				)
				http_da_proxy_save(soap, &infoTmp, (soap->authrealm ? soap->authrealm : authrealm),
						(proxy_userid[0] ? proxy_userid : getUsername()), (proxy_passwd[0] ? proxy_passwd : getPassword()));
				http_da_proxy_release(soap, info);
				memcpy(info, &infoTmp, sizeof(struct http_da_info));
				http_da_proxy_restore(soap, info);
			}
			else
			{
				// Basic authentication is required.
				DEBUG_OUTPUT (
						dbglog(SVR_DEBUG, DBG_MAIN, "authorizeClient(): Basic authentication is required by Proxy. soapError = 401.\n");
				)
				soap->proxy_userid = (proxy_userid[0] ? proxy_userid : getUsername());
				soap->proxy_passwd = (proxy_passwd[0] ? proxy_passwd : getPassword());
			}
		}
	}
	else if (soapError == 0)
	{
		if (prevAuthenticationSoapError == 401)
		{
			if (data && data->nonce && *data->nonce)
			{
				// Digest authentication.
				DEBUG_OUTPUT (
						dbglog(SVR_DEBUG, DBG_MAIN, "authorizeClient(): Digest authentication. http_da_restore() will be executed.\n");
				)
				http_da_restore(soap, info);
			}
			else
			{
				// Basic authentication.
				DEBUG_OUTPUT (
						dbglog(SVR_DEBUG, DBG_MAIN, "authorizeClient(): Basic authentication.\n");
				)
				soap->userid = getUsername();
				soap->passwd = getPassword();
			}
		}
		else if (prevAuthenticationSoapError == 407)
		{
			if (data && data->nonce && *data->nonce)
			{
				// Digest authentication.
				DEBUG_OUTPUT (
						dbglog(SVR_DEBUG, DBG_MAIN, "authorizeClient(): Digest Proxy authentication. http_da_proxy_restore() will be executed.\n");
				)
				http_da_proxy_restore(soap, info);
			}
			else
			{
				// Basic authentication.
				DEBUG_OUTPUT (
						dbglog(SVR_DEBUG, DBG_MAIN, "authorizeClient(): Basic Proxy authentication.\n");
				)
				soap->proxy_userid = (proxy_userid[0] ? proxy_userid : getUsername());
				soap->proxy_passwd = (proxy_passwd[0] ? proxy_passwd : getPassword());
			}
		}
	}

	return SOAP_OK;
}

/* De/Serializers for special type xsd__boolean_
 * This type is used for the SOAP header structure.
 * The value can be read, but will never be written. */
enum xsd__boolean *soap_in_xsd__HoldRequestBoolean(struct soap *soap, const char *tag, enum xsd__boolean *a, const char *type)
{
	return soap_in_xsd__boolean(soap, tag, a, type);
}

int soap_out_xsd__HoldRequestBoolean(struct soap *soap, const char *tag, int id, const enum xsd__boolean *a, const char *type)
{
	soap->mustUnderstand = 0;
	return SOAP_OK;
}

void soap_default_xsd__HoldRequestBoolean(struct soap *soap, enum xsd__boolean *a)
{
	soap_default_xsd__boolean(soap, a);
}

void killAllThreads()
{
#ifdef HAVE_HOST
	if (hostTid > 0 /* &&  pthread_self() != hostTid*/)
		pthread_cancel(hostTid);
#endif

#ifdef WITH_STUN_CLIENT
	terminateStunHandler();
#endif

#ifdef HAVE_CONNECTION_REQUEST
	if (udpCRhandlerTid > 0)
		pthread_cancel(udpCRhandlerTid);
	terminateCRhandler();
#endif
	if (timeTid > 0)
		pthread_cancel(timeTid);
#if defined(HAVE_SCHEDULE_INFORM)
	if (scheduleInformTimerTid > 0)
		pthread_cancel(scheduleInformTimerTid);
#endif /* HAVE_SCHEDULE_INFORM */
#ifdef HAVE_FILE
#if defined(HAVE_DEPLOYMENT_UNIT) && defined(HAVE_FILE_DOWNLOAD)
	if (deploymentUnitTimerTid > 0)
		pthread_cancel(deploymentUnitTimerTid);
#endif /* HAVE_DEPLOYMENT_UNIT && HAVE_FILE_DOWNLOAD */
#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
	if (transferTimerTid > 0)
		pthread_cancel(transferTimerTid);
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */
#endif
#ifdef HAVE_KICKED
	if (kickedTid > 0)
		pthread_cancel(kickedTid);
#endif
#ifdef HAVE_GET_ALL_QUEUED_TRANSFERS
	if (transfersTid > 0)
		pthread_cancel(transfersTid);
#endif
#ifdef HAVE_UDP_ECHO
	if (udpEchoTid > 0)
		terminateUDPEchoHandler(udpEchoTid);
#endif
#ifdef HAVE_DIAGNOSTICS
	if (diagTid > 0)
		pthread_cancel(diagTid);
#endif
#ifdef HAVE_NOTIFICATION
	if (passive_notification_id > 0)
		pthread_cancel(passive_notification_id);
	if (active_notification_id > 0)
		pthread_cancel(active_notification_id);
#endif
#if defined(HAVE_CONNECTION_REQUEST) || defined(WITH_STUN_CLIENT)
	shutdownUdpCRSocket();
	closeUDPSocket();
#endif
	pthread_attr_destroy(&threadAttr);
}

void doneAllSoapInstances()
{
	soap_destroy(&soap);
	soap_end(&soap);
	soap_done(&soap);
#ifdef HAVE_HOST
	soap_destroy(&hostSoap);
	soap_end(&hostSoap);
	soap_done(&hostSoap);
#endif
#ifdef HAVE_CONNECTION_REQUEST
	soap_destroy(&CRSoap);
	soap_end(&CRSoap);
	soap_done(&CRSoap);
#endif
#ifdef HAVE_KICKED
	soap_destroy(&kickedSoap);
	soap_end(&kickedSoap);
	soap_done(&kickedSoap);
#endif
#ifdef HAVE_GET_ALL_QUEUED_TRANSFERS
	soap_destroy(&transfersSoap);
	soap_end(&transfersSoap);
	soap_done(&transfersSoap);
#endif
}

void setCWMPVersionToDefault()
{
	if (strcmp(cwmp_version_ns, CWMP_DEFAULT_VERSION))
	{
		strcpy(cwmp_version_ns, CWMP_DEFAULT_VERSION);
		setSuportedRPCInfoFromCWMPVersion(&suportedRPC, strstr(cwmp_version_ns, "cwmp"));
	}
}

static int setSuportedRPCInfoFromCWMPVersion(SUPORTEDRPC *suportedRPCtmp, char * cwmpNameSpace)
{
	if (!suportedRPCtmp || !cwmpNameSpace)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_MAIN, "setSuportedRPCInfoFromCWMPVersion(): suportedRPCtmp & cwmpNameSpace can not be NULL. Return -1.\n");
		)
		return -1;
	}

	if (!strcmp(cwmpNameSpace, "cwmp-1-0"))
	{
		suportedRPCtmp->cwmpMask = 0;
	}
	else if (!strcmp(cwmpNameSpace, "cwmp-1-1"))
	{
		suportedRPCtmp->cwmpMask = 0;
		suportedRPCtmp->rpc.isGetAllQueuedTransfers = suportedRPCtmp->rpc.isAutonomousTransferComplete = 1;
	}
	else if (!strcmp(cwmpNameSpace, "cwmp-1-2") || !strcmp(cwmpNameSpace, "cwmp-1-3"))
	{
		/*suportedRPCtmp->cwmpMask = 0;
		suportedRPCtmp->rpc.isGetAllQueuedTransfers = suportedRPCtmp->rpc.isAutonomousTransferComplete =
				suportedRPCtmp->rpc.isScheduleDownload = suportedRPCtmp->rpc.isCancelTransfer =
				suportedRPCtmp->rpc.isChangeDUState = suportedRPCtmp->rpc.isDUStateChangeComplete =
				suportedRPCtmp->rpc.isAutonomousDUStateChangeComplete = 1;
		 */
		suportedRPCtmp->cwmpMask = UINT_MAX;
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_MAIN, "setSuportedRPCInfoFromCWMPVersion(): Unknown (unsupported) cwmp version. Return -1.\n");
		)
		return -1;

	}

	return OK;
}
