/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef DU_TRANSFER_H
#define DU_TRANSFER_H

#ifdef HAVE_DEPLOYMENT_UNIT

//#include "utils.h"

#define DU_URL_LEN					1024
#define DU_UUID_LEN					36
#define DU_DUID_LEN					64
#define DU_USERNAME_LEN				256
#define DU_PASSWORD_LEN				256
#define DU_REF_LEN					256
#define DU_EXECUTION_ENV_REF_LEN	256
#define DU_VERSION_LEN				32
#define DU_FAULT_STR_LEN			256
#define DU_CURRENT_STATE_LEN		16
#define DU_DEST_FILE_PATH_LEN		256
#define DU_ALIAS_LEN				64
#define DU_NAME_LEN					64
#define DU_DESCRIPTION_LEN			256
#define DU_VENDOR_LEN				128

#define DU_MAX_NAME_SIZE			32


typedef struct _strValueDU
{
	char *value;
} StrValueDU;

/* Type of status deployment units */
typedef enum status_DU
{
	INSTALLING,
	INSTALLED,
	UPDATING,
	UPDATED,
	UNINSTALLING,
	UNINSTALLED,
	FAILED
} Status_DU;

/* Type of file transfers */
typedef enum _DU_Type
{
	DU_INSTALL,
	DU_UPDATE,
	DU_UNINSTALL
} DU_Type;

typedef enum _DU_TransferState
{
	DU_Transfer_NotStarted = 1,
	DU_Transfer_InProgress = 2,
	DU_Transfer_Completed = 3,
	DU_Informed_to_ACS = 4,
	DU_Completed = 5
} DU_TransferState;

/* A Deployment Unit Entry. */
typedef struct deploymentUnitEntry
{
	/* Type of transfer */
	DU_Type type;
	Status_DU du_status;
	Transfers_type initiator;
	/* All Data we got from the ACS */
	char commandKey[CMD_KEY_STR_LEN + 1];
	char URL[DU_URL_LEN + 1];
	char UUID[DU_UUID_LEN + 1];
	char Username[DU_USERNAME_LEN + 1];
	char Password[DU_PASSWORD_LEN + 1];
	char ExecutionEnvRef[DU_EXECUTION_ENV_REF_LEN + 1];
	char Version[DU_VERSION_LEN + 1];
	struct timeval startTime;
	struct timeval completeTime;
	unsigned int fileSize;
	char targetFileName[DU_DEST_FILE_PATH_LEN + 1];
	/* Deployment Unit Transfer State */
	DU_TransferState transfer_status;
	/* Fault Handling */
	unsigned int faultCode;
	char faultString[DU_FAULT_STR_LEN + 1];

	/* For data-model */
	char DUID[DU_DUID_LEN + 1];
	char Alias[DU_ALIAS_LEN + 1];
	char Name[DU_NAME_LEN + 1];
	char Description[DU_DESCRIPTION_LEN + 1];
	char Vendor[DU_VENDOR_LEN + 1];
	char VendorLogList[MAX_PARAM_PATH_LENGTH];
	char VendorConfigList[MAX_PARAM_PATH_LENGTH];
	char ExecutionUnitList[MAX_PARAM_PATH_LENGTH];
	char DeploymentUnitRef[MAX_PARAM_PATH_LENGTH];
	char ExecutionUnitRefList[MAX_PARAM_PATH_LENGTH];

	/* Instance Number in data-model */
	int InstanceNumber;

	/*Indicates whether or not this DeploymentUnit has resolved all of its dependencies*/
	xsd__boolean Resolved;

	/* Result for DUStateChangeComplete Method */
	OpResultStruct result;

#ifdef	 HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE
	/* Result for AutonomousDUStatateChangeComplete Method */
	AutonOpResultStruct autonomous_result;
#endif /* HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE */
} DeploymentUnitEntry;

char * getDeploymentUnitStatus(Status_DU dus);
char * getDeploymentUnitType(DU_Type dut);

/* Callback for processing deployment unit */
typedef int (*DeploymentUnitCB)(DeploymentUnitEntry * due);

int initDeploymentUnitTransfer(DeploymentUnitCB);
int resetAllDeploymentUnitEntry(void);
int isDeploymentUnitTransfer(void);
int handleDeploymentUnitTransfers();
int handleDeploymentUnitTransfersEvents(int *);
int clearDeploymentUnitTransfers(struct soap *);
int freeDUEntryMemory();

int execInstallDeploymentUnit(struct soap * soap,
							  char * CommandKey,
							  char * URL,
							  char * UUID,
							  char * Username,
							  char * Password,
							  char * ExecutionEnvRef,
							  Transfers_type initiator);
int execUpdateDeploymentUnit(struct soap * soap,
							 char * CommandKey,
							 char * UUID,
							 char * Version,
							 char * URL,
							 char * Username,
							 char * Password,
							 Transfers_type initiator);
int execUninstallDeploymentUnit(struct soap * soap,
								char * CommandKey,
								char * UUID,
								char *  Version,
								char *  ExecutionEnvRef,
								Transfers_type initiator);

#ifdef	 HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE
extern bool isAutonomousDUStateChangeComplete;
/* create Autonomous Deploymant Unit entry */
int createAutonomousDeploymentUnitEntry(DU_Type type,
										Status_DU du_status,
										char * URL,
										char * UUID,
										char * Username,
										char * Password,
										char * ExecutionEnvRef,
										char * Version,
										struct timeval startTime,
										struct timeval completeTime,
										DU_TransferState transfer_status);

#endif /* HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE */

#endif /* HAVE_DEPLOYMENT_UNIT */
#endif /* DU_TRANSFER_H */
