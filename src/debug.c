/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>
#include <unistd.h>

#include "dimark_globals.h"
#include "debug.h"

#ifdef _DEBUG

// count of the element of array must be equal size of dbg_sys_enum (debug.h)
struct dbg_stuct sev_m[] =
{
	{"SOAP", 1, "INFO"},
	{"MAIN", 1, "INFO"},
	{"PARAMETER", 1, "INFO"},
	{"TRANSFER", 1, "INFO"},
	{"DU_TRANSFER", 1, "INFO"},
	{"STUN", 1, "INFO"},
	{"ACCESS", 1, "INFO"},
	{"MEMORY", 1, "INFO"},
	{"EVENTCODE", 1, "INFO"},
	{"SCHEDULE", 1, "INFO"},
	{"ACS", 1, "INFO"},
	{"VOUCHERS", 1, "INFO"},
	{"OPTIONS", 1, "INFO"},
	{"DIAGNOSTIC", 1, "INFO"},
	{"VOIP", 1, "INFO"},
	{"KICK", 1, "INFO"},
	{"CALLBACK", 1, "INFO"},
	{"HOST", 1, "INFO"},
	{"REQUEST", 1, "INFO"},
	{"DEBUG", 1, "INFO"},
	{"AUTH", 1, "INFO"},
	{"DB", 1, "INFO"},
	{"ABAM", 1, "INFO"}
};

int loadConfFile (char *filename)
{
	int ret = OK;
	char buf[MAX_PATH_NAME_SIZE + 1];
	FILE *file;
	int i;
	char *s;

	unsigned int sev_m_size = sizeof(sev_m) / sizeof(struct dbg_stuct);

	if (!isFileExists(filename))
	{
		printf("Loading of logconfig file was not executed, because logconfig file was not found or name is Empty.\n");
		return OK;
	}

	printf("Loading of logconfig file:\n");

	file = fopen (filename, "r");
	if(file == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DEBUG, "loadReadConfFile: file not found %s\n", filename);
		)

		return ERR_INTERNAL_ERROR;
	}

	while(fgets (buf, MAX_PATH_NAME_SIZE, file) != NULL)
	{
		if(buf[0] == '#' || buf[0] == ' ' || buf[0] == '\n' || buf[0] == '\0')
			continue;
		buf[strlen (buf) - 1] = '\0'; /* remove trailing EOL */

		s = strtok (buf, ";");
		for (i = 0; i < sev_m_size; i++)
		{
			if ( !strcmp(sev_m[i].sys_str, s) )
			{
				strcpy(sev_m[i].level_str, strtok (NULL, "\n"));

				if ( !strcmp(sev_m[i].level_str, "DEBUG") )
				{
					sev_m[i].level_int = SVR_DEBUG;
				}
				else
				{
					if ( !strcmp(sev_m[i].level_str, "INFO") )
					{
						sev_m[i].level_int = SVR_INFO;
					}
					else
					{
						if ( !strcmp(sev_m[i].level_str, "WARN") )
						{
							sev_m[i].level_int = SVR_WARN;
						}
						else
						{
							sev_m[i].level_int = SVR_ERROR;
						}
					}
				}

				break;
			}
		}
	}

	for (i = 0; i < sev_m_size; i++)
		printf("%s->%d->%s\n",	sev_m[i].sys_str, sev_m[i].level_int, sev_m[i].level_str);

	fclose (file);
	return ret;
}

#ifndef dbglog
void dbglog( Dbg_level_enum severity, Dbg_sys_enum modul, const char *format, ... )
{
	va_list ap;
	time_t time_of_day;
	char buffer[ 40 ];
	int  colorWasChanged;

	va_start( ap, format);

	if (sev_m[modul].level_int <= severity)
	{
		time_of_day = time( NULL );
		strftime (buffer, sizeof(buffer), "%c", localtime( &time_of_day ));

#ifdef HAVE_COLOR_SCREEN_LOG
		switch (severity)
		{
			/*case SVR_DEBUG:
				//printf ("");
			break;
			 */
			case SVR_INFO:
				printf ("\033[%um", C_Green);
				colorWasChanged = 1;
			break;

			case SVR_WARN:
				printf ("\033[%um", C_Brown);
				colorWasChanged = 1;
			break;

			case SVR_ERROR:
				printf ("\033[%um", C_Red);
				colorWasChanged = 1;
			break;

			default:
				colorWasChanged = 0;
			break;
		}
#endif /* HAVE_COLOR_SCREEN_LOG */

		printf ("%s [", buffer);
		switch (severity)
		{
			case SVR_DEBUG:
				printf ("%-5s", "DEBUG");
			break;

			case SVR_INFO:
				printf ("%-5s", "INFO");
			break;

			case SVR_WARN:
				printf ("%-5s", "WARN");
			break;

			case SVR_ERROR:
				printf ("%-5s", "ERROR");
			break;

			default:
			break;
		}

		printf ("] %s - ", sev_m[modul].sys_str);
		vprintf (format, ap);
	}

	va_end( ap );
#ifdef HAVE_COLOR_SCREEN_LOG
	if (colorWasChanged == 1)
		printf ("\033[%um", C_Reset);
#endif /* HAVE_COLOR_SCREEN_LOG */

	//system("ps -Cdimclient o rss,vsz,comm");
}
#endif /* dbglog */

#endif /* _DEBUG */
