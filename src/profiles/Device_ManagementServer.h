#ifndef DEVICE_MANAGEMENTSERVER_H_
#define DEVICE_MANAGEMENTSERVER_H_

/*****************************************************************************
 * TR069-SPECIFIC INCLUDE FILES
 *****************************************************************************/
#include "paramaccess.h"
#include "parameter.h"

#ifdef __cplusplus
extern "C"
{
#endif

    int get_Device_ManagementServer_ValidateManagementServerCertificate(const char *, ParameterType , ParameterValue *);

    int get_Device_ManagementServer_ValidateDownloadServerCertificate(const char *, ParameterType , ParameterValue *);

    int get_Device_ManagementServer_RootCertificateNumberOfEntries(const char *, ParameterType , ParameterValue *);

    int set_Device_ManagementServer_ValidateManagementServerCertificate(const char *, ParameterType , ParameterValue *);

    int get_Device_ManagementServer_X_TWC_COM_RootCertificate_Enabled(const char *, ParameterType , ParameterValue *);

    int get_Device_ManagementServer_X_TWC_COM_RootCertificate_Certificate(const char *, ParameterType , ParameterValue *);

    int get_Device_ManagementServer_X_TWC_COM_RootCertificate_LastModif(const char *, ParameterType , ParameterValue *);

    int get_Device_ManagementServer_X_TWC_COM_RootCertificate_SerialNumber(const char *, ParameterType , ParameterValue *);

    int get_Device_ManagementServer_X_TWC_COM_RootCertificate_Issuer(const char *, ParameterType , ParameterValue *);

    int get_Device_ManagementServer_X_TWC_COM_RootCertificate_NotBefore(const char *, ParameterType , ParameterValue *);

    int get_Device_ManagementServer_X_TWC_COM_RootCertificate_NotAfter(const char *, ParameterType , ParameterValue *);

    int get_Device_ManagementServer_X_TWC_COM_RootCertificate_Subject(const char *, ParameterType , ParameterValue *);

    int get_Device_ManagementServer_X_TWC_COM_RootCertificate_SubjectAlt(const char *, ParameterType , ParameterValue *);

    int get_Device_ManagementServer_X_TWC_COM_RootCertificate_SignatureAlgorithm(const char *, ParameterType , ParameterValue *);

    int set_Device_ManagementServer_X_TWC_COM_RootCertificate_Enabled(const char *, ParameterType , ParameterValue *);

    int set_Device_ManagementServer_X_TWC_COM_RootCertificate_Certificate(const char *, ParameterType , ParameterValue *);

#ifdef __cplusplus
}
#endif

#endif /* DEVICE_MANAGEMENTSERVER_H_ */
