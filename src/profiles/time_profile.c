/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * time_profile.c
 *
 *  Created on: May 12, 2011
 *      Author: gonchar
 */

#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/time.h>

#include "dimark_globals.h"
#include "../paramaccess.h"
#include "parameterStore.h"
#include "utils.h"
#include "parameter.h"
#include "ethParameter.h"
#include "diagParameter.h"
#include "time_profile.h"

#define _PATH_ETC_LOCALTIME "/etc/localtime"

#define	 Time_Status_Disabled "Disabled"
#define	 Time_Status_Unsynchronized "Unsynchronized"
#define	 Time_Status_Synchronized "Synchronized"
#define	 Time_Status_Error_FailedToSynchronize "Error_FailedToSynchronize"
#define	 Time_Status_Error "Error (OPTIONAL)"

// Time Profile. Getters:

/**
 * Parameter Name: InternetGatewayDevice.Time.Enable
 * Type ID: 18
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18000
 * Set Idx: 18000
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_Enable(const char *name, ParameterType type, ParameterValue *value)
{
#if defined(TR_181_DEVICE) || !defined(WITH_DEVICE_ROOT)
	int ret;
	//Retrieving value
	ret = retrieveParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "get_InternetGatewayDevice_Time_Enable: retrieveParamValue() error\n");
		)
		return ret;
	}
#endif
	return OK;
}


/**
 * Parameter Name: InternetGatewayDevice.Time.Status
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18001
 * Set Idx: -1
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_Status(const char *name, ParameterType type, ParameterValue *value)
{
#if defined(TR_181_DEVICE) || !defined(WITH_DEVICE_ROOT)
	int ret;
	//Retrieving value
	ret = retrieveParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "get_InternetGatewayDevice_Time_Status: retrieveParamValue() error\n");
		)
		return ret;
	}
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.NTPServer1
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18002
 * Set Idx: 18001
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_NTPServer1(const char *name, ParameterType type, ParameterValue *value)
{
	int ret;
	//Retrieving value
	ret = retrieveParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "get_InternetGatewayDevice_Time_NTPServer1: retrieveParamValue() error\n");
		)
		return ret;
	}

	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.NTPServer2
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18003
 * Set Idx: 18002
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_NTPServer2(const char *name, ParameterType type, ParameterValue *value)
{
	int ret;
	//Retrieving value
	ret = retrieveParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "get_InternetGatewayDevice_Time_NTPServer2: retrieveParamValue() error\n");
		)
		return ret;
	}

	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.NTPServer3
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18004
 * Set Idx: 18003
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_NTPServer3(const char *name, ParameterType type, ParameterValue *value)
{
	int ret;
	//Retrieving value
	ret = retrieveParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "get_InternetGatewayDevice_Time_NTPServer3: retrieveParamValue() error\n");
		)
		return ret;
	}

	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.NTPServer4
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18005
 * Set Idx: 18004
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_NTPServer4(const char *name, ParameterType type, ParameterValue *value)
{
	int ret;
	//Retrieving value
	ret = retrieveParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "get_InternetGatewayDevice_Time_NTPServer4: retrieveParamValue() error\n");
		)
		return ret;
	}

	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.NTPServer5
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18006
 * Set Idx: 18005
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_NTPServer5(const char *name, ParameterType type, ParameterValue *value)
{
	int ret;
	//Retrieving value
	ret = retrieveParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "get_InternetGatewayDevice_Time_NTPServer5: retrieveParamValue() error\n");
		)
		return ret;
	}

	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.CurrentLocalTime
 * Type ID: 11
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18007
 * Set Idx: -1
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_CurrentLocalTime(const char *name, ParameterType type, ParameterValue *value)
{
	struct timeval now;

	gettimeofday(&now, NULL);

	value->in_timet.tv_sec = now.tv_sec;
	value->in_timet.tv_usec = now.tv_usec;
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.LocalTimeZone
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18008
 * Set Idx: 18006
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_LocalTimeZone(const char *name, ParameterType type, ParameterValue *value)
{
	static char strTimezone[8] = {'\0'};

	time_t t;
	struct tm *tmp;

	t = time(NULL);
	tmp = localtime(&t);

	strftime(strTimezone, sizeof(strTimezone),  "%z", tmp);
	value->out_cval = strTimezone;

	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.LocalTimeZoneName
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18009
 * Set Idx: 18007
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_LocalTimeZoneName(const char *name, ParameterType type, ParameterValue *value)
{
#if !defined(WITH_DEVICE_ROOT) && !defined(TR_181_DEVICE)
	int ret;
	char line[100] = {"\0"};
	int len = 0;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "IntelGet: %s\n", name);
	)
	FILE * fp = popen("tail -n 1 /etc/localtime", "r");
	if(!fp)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "get_InternetGatewayDevice_Time_LocalTimeZoneName() error1\n");
		)
		return -1;
	}
	if (!fgets(line, sizeof(line), fp))
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "get_InternetGatewayDevice_Time_LocalTimeZoneName() error2\n");
		)
		pclose(fp);
		return -1;
	}
	pclose(fp);

	len = strlen(line);
	if ( line[len-1] == '\n' )
		line[len-1] = '\0';
	value->out_cval = (char *)emallocTemp(len, 1);
	strncpy(value->out_cval, line, len);
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.DaylightSavingsUsed
 * Type ID: 18
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18010
 * Set Idx: 18008
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_DaylightSavingsUsed(const char *name, ParameterType type, ParameterValue *value)
{
#if !defined(WITH_DEVICE_ROOT) && !defined(TR_181_DEVICE)
	int ret;
	//Retrieving value
	ret = retrieveParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "get_InternetGatewayDevice_Time_DaylightSavingsUsed: retrieveParamValue() error\n");
		)
		return ret;
	}
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.DaylightSavingsStart
 * Type ID: 11
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 0
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18011
 * Set Idx: 18009
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_DaylightSavingsStart(const char *name, ParameterType type, ParameterValue *value)
{
#if !defined(WITH_DEVICE_ROOT) && !defined(TR_181_DEVICE)
	int ret;
	//Retrieving value
	ret = retrieveParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "get_InternetGatewayDevice_Time_DaylightSavingsStart: retrieveParamValue() error\n");
		)
		return ret;
	}
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.DaylightSavingsEnd
 * Type ID: 11
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 0
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18012
 * Set Idx: 18010
 * Access List:
 * Default value:
 */
int
get_InternetGatewayDevice_Time_DaylightSavingsEnd(const char *name, ParameterType type, ParameterValue *value)
{
#if !defined(WITH_DEVICE_ROOT) && !defined(TR_181_DEVICE)
	int ret;
	//Retrieving value
	ret = retrieveParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "get_InternetGatewayDevice_Time_DaylightSavingsEnd: retrieveParamValue() error\n");
		)
		return ret;
	}
#endif
	return OK;
}







/****************************************************************************************************************************************************/
// Time Profile. Setters:
/****************************************************************************************************************************************************/
/**
 * Parameter Name: InternetGatewayDevice.Time.Enable
 * Type ID: 18
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18000
 * Set Idx: 18000
 * Access List:
 * Default value:
 */
int
set_InternetGatewayDevice_Time_Enable(const char *name, ParameterType type, ParameterValue *value)
{
#if defined(TR_181_DEVICE) || !defined(WITH_DEVICE_ROOT)
	int ret;

	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_Time_Enable: updateParamValue() error\n");
		)
		return ret;
	}
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.NTPServer1
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18002
 * Set Idx: 18001
 * Access List:
 * Default value:
 */
int
set_InternetGatewayDevice_Time_NTPServer1(const char *name, ParameterType type, ParameterValue *value)
{
	int ret;

	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_Time_NTPServer1: updateParamValue() error\n");
		)
		return ret;
	}

	return OK;
}


/**
 * Parameter Name: InternetGatewayDevice.Time.NTPServer2
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18003
 * Set Idx: 18002
 * Access List:
 * Default value:
 */
int
set_InternetGatewayDevice_Time_NTPServer2(const char *name, ParameterType type, ParameterValue *value)
{
	int ret;

	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_Time_NTPServer2: updateParamValue() error\n");
		)
		return ret;
	}

	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.NTPServer3
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18004
 * Set Idx: 18003
 * Access List:
 * Default value:
 */
int
set_InternetGatewayDevice_Time_NTPServer3(const char *name, ParameterType type, ParameterValue *value)
{
	int ret;

	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_Time_NTPServer3: updateParamValue() error\n");
		)
		return ret;
	}

	return OK;
}


/**
 * Parameter Name: InternetGatewayDevice.Time.NTPServer4
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18005
 * Set Idx: 18004
 * Access List:
 * Default value:
 */
int
set_InternetGatewayDevice_Time_NTPServer4(const char *name, ParameterType type, ParameterValue *value)
{
	int ret;

	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_Time_NTPServer4: updateParamValue() error\n");
		)
		return ret;
	}

	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.NTPServer5
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18006
 * Set Idx: 18005
 * Access List:
 * Default value:
 */
int
set_InternetGatewayDevice_Time_NTPServer5(const char *name, ParameterType type, ParameterValue *value)
{
	int ret;

	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_Time_NTPServer5: updateParamValue() error\n");
		)
		return ret;
	}

	long a = timezone;

	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.LocalTimeZone
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18008
 * Set Idx: 18006
 * Access List:
 * Default value:
 */
int
set_InternetGatewayDevice_Time_LocalTimeZone(const char *name, ParameterType type, ParameterValue *value)
{

	int ret;
	int hoursTimezone = 0;
	int minutesTimezone = 0;
	int newTimeZoneMinutes = 0;
	struct timeval tv;
	struct timezone tz;

	sscanf(value->in_cval, "%d:%d", &hoursTimezone, &minutesTimezone);

	newTimeZoneMinutes = -(hoursTimezone * 60 + minutesTimezone); /* convert Minutes east of GMT to Minutes west of GMT.  */

	gettimeofday(&tv, &tz);

	tz.tz_minuteswest = newTimeZoneMinutes;

	ret = settimeofday(&tv, &tz);
	if (ret != 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_Time_LocalTimeZone: settimeofday() error\n");
		)
		return ERR_INTERNAL_ERROR;
	}

	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_Time_LocalTimeZone: updateParamValue() error\n");
		)
		return ret;
	}

	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.LocalTimeZoneName
 * Type ID: 6
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18009
 * Set Idx: 18007
 * Access List:
 * Default value:
 */
int
set_InternetGatewayDevice_Time_LocalTimeZoneName(const char *name, ParameterType type, ParameterValue *value)
{
#if !defined(WITH_DEVICE_ROOT) && !defined(TR_181_DEVICE)
	int ret;

	// TODO: For different OS may be needed different methods for setup LocalTimeZoneName.

	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_Time_LocalTimeZoneName: updateParamValue() error\n");
		)
		return ret;
	}
#endif
	return OK;


}

/**
 * Parameter Name: InternetGatewayDevice.Time.DaylightSavingsUsed
 * Type ID: 18
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 2
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18010
 * Set Idx: 18008
 * Access List:
 * Default value:
 */
int
set_InternetGatewayDevice_Time_DaylightSavingsUsed(const char *name, ParameterType type, ParameterValue *value)
{
#if !defined(WITH_DEVICE_ROOT) && !defined(TR_181_DEVICE)
	int ret;

	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_Time_DaylightSavingsUsed: updateParamValue() error\n");
		)
		return ret;
	}
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.DaylightSavingsStart
 * Type ID: 11
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 0
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18011
 * Set Idx: 18009
 * Access List:
 * Default value:
 */
int
set_InternetGatewayDevice_Time_DaylightSavingsStart(const char *name, ParameterType type, ParameterValue *value)
{
#if !defined(WITH_DEVICE_ROOT) && !defined(TR_181_DEVICE)
	int ret;

	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_Time_DaylightSavingsStart: updateParamValue() error\n");
		)
		return ret;
	}
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.Time.DaylightSavingsEnd
 * Type ID: 11
 * Instance: 0
 * Notification: 0
 * Max Notitfication: 0
 * Reboot: 1
 * Init Idx: 1
 * Get Idx: 18012
 * Set Idx: 18010
 * Access List:
 * Default value:
 */
int
set_InternetGatewayDevice_Time_DaylightSavingsEnd(const char *name, ParameterType type, ParameterValue *value)
{
#if !defined(WITH_DEVICE_ROOT) && !defined(TR_181_DEVICE)
	int ret;
	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_Time_DaylightSavingsEnd: updateParamValue() error\n");
		)
		return ret;
	}
#endif
	return OK;
}

