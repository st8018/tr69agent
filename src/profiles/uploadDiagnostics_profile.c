/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * uploadDiagnostics_profile.c
 *
 *  Created on: Oct 31, 2011
 *      Author: San
 */

#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <net/if.h>
#include <arpa/inet.h>

#include "dimark_globals.h"
#include "../paramaccess.h"
#include "parameterStore.h"
#include "utils.h"
#include "parameter.h"
#include "ethParameter.h"
#include "diagParameter.h"
#include "diagParameter.h"
#include "uploadDiagnostics_profile.h"
#include "diagnosticsHandler.h"

// uploadDiagnostics Profile. Getters:
// None



/****************************************************************************************************************************************************/
// uploadDiagnostics Profile. Setters:
/****************************************************************************************************************************************************/
/*
 * Parameter Name: InternetGatewayDevice.UploadDiagnosticsDiagnostics.DiagnosticsState
*/
int
set_InternetGatewayDevice_UploadDiagnostics_DiagnosticsState(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_UPLOAD_DIAGNOSTICS
	int ret;

	ret = setUploadDiagnostics(name, type,value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_UploadDiagnostics_DiagnosticsState: setUploadDiagnostics() error = %d\n", ret);
		)
		return ret;
	}
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.UploadDiagnostics.Interface
 */
int
set_InternetGatewayDevice_UploadDiagnostics_Interface(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_UPLOAD_DIAGNOSTICS
	int ret;
	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_UploadDiagnostics_Interface: updateParamValue() error2\n");
		)
		return ret;
	}
	//If the test is in progress - test being terminated
	ret = diagnosticTaskCancel(uploadDiagnostic, UploadDiagnostics_DiagnosticsState);
	return ret;
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.UploadDiagnostics.UploadURL
 */
int
set_InternetGatewayDevice_UploadDiagnostics_UploadURL(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_UPLOAD_DIAGNOSTICS
	int ret;
	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_UploadDiagnostics_UploadURL: updateParamValue() error2\n");
		)
		return ret;
	}
	//If the test is in progress - test being terminated
	ret = diagnosticTaskCancel(uploadDiagnostic, UploadDiagnostics_DiagnosticsState);
	return ret;
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.UploadDiagnostics.DSCP
 */
int
set_InternetGatewayDevice_UploadDiagnostics_DSCP(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_UPLOAD_DIAGNOSTICS
	int ret;
	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_UploadDiagnostics_DSCP: updateParamValue() error2\n");
		)
		return ret;
	}
	//If the test is in progress - test being terminated
	ret = diagnosticTaskCancel(uploadDiagnostic, UploadDiagnostics_DiagnosticsState);
	return ret;
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.UploadDiagnostics.EthernetPriority
 */
int
set_InternetGatewayDevice_UploadDiagnostics_EthernetPriority(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_UPLOAD_DIAGNOSTICS
	int ret;
	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_UploadDiagnostics_EthernetPriority: updateParamValue() error2\n");
		)
		return ret;
	}
	//If the test is in progress - test being terminated
	ret = diagnosticTaskCancel(uploadDiagnostic, UploadDiagnostics_DiagnosticsState);
	return ret;
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.UploadDiagnostics.TestFileLength
 */
int
set_InternetGatewayDevice_UploadDiagnostics_TestFileLength(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_UPLOAD_DIAGNOSTICS
	int ret;
	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_UploadDiagnostics_TestFileLength: updateParamValue() error2\n");
		)
		return ret;
	}
	//If the test is in progress - test being terminated
	ret = diagnosticTaskCancel(uploadDiagnostic, UploadDiagnostics_DiagnosticsState);
	return ret;
#endif
	return OK;
}
