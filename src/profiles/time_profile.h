/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * time_profile.h
 *
 *  Created on: May 12, 2011
 *      Author: gonchar
 */

#ifndef time_profile_H_
#define time_profile_H_

#include "parameter.h"
#include "paramaccess.h"


// Time Profile. Getters:
int get_InternetGatewayDevice_Time_Enable(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_Time_Status(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_Time_NTPServer1(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_Time_NTPServer2(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_Time_NTPServer3(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_Time_NTPServer4(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_Time_NTPServer5(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_Time_CurrentLocalTime(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_Time_LocalTimeZone(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_Time_LocalTimeZoneName(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_Time_DaylightSavingsUsed(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_Time_DaylightSavingsStart(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_Time_DaylightSavingsEnd(const char *, ParameterType, ParameterValue *);


// Time Profile. Setters:
int set_InternetGatewayDevice_Time_Enable(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_Time_NTPServer1(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_Time_NTPServer2(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_Time_NTPServer3(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_Time_NTPServer4(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_Time_NTPServer5(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_Time_LocalTimeZone(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_Time_LocalTimeZoneName(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_Time_DaylightSavingsUsed(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_Time_DaylightSavingsStart(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_Time_DaylightSavingsEnd(const char *, ParameterType, ParameterValue *);

#endif /* time_profile_H_ */
