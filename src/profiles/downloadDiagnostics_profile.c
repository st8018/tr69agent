/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * downloadDiagnostics_profile.c
 *
 *  Created on: Oct 31, 2011
 *      Author: San
 */

#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <net/if.h>
#include <arpa/inet.h>

#include "dimark_globals.h"
#include "../paramaccess.h"
#include "parameterStore.h"
#include "utils.h"
#include "parameter.h"
#include "ethParameter.h"
#include "diagParameter.h"
#include "diagParameter.h"
#include "downloadDiagnostics_profile.h"
#include "diagnosticsHandler.h"

// DownloadDiagnostics Profile. Getters:
// None



/****************************************************************************************************************************************************/
// DownloadDiagnostics Profile. Setters:
/****************************************************************************************************************************************************/
/*
 * Parameter Name: InternetGatewayDevice.DownloadDiagnostics.DiagnosticsState
*/
int
set_InternetGatewayDevice_DownloadDiagnostics_DiagnosticsState(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_DOWNLOAD_DIAGNOSTICS
	int ret;

	ret = setDownloadDiagnostics(name, type,value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_DownloadDiagnostics_DiagnosticsState: setDownloadDiagnostics() error = %d\n", ret);
		)
		return ret;
	}
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.DownloadDiagnostics.Interface
 */
int
set_InternetGatewayDevice_DownloadDiagnostics_Interface(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_DOWNLOAD_DIAGNOSTICS
	int ret;
	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_DownloadDiagnostics_Interface: updateParamValue() error2\n");
		)
		return ret;
	}
	//If the test is in progress - test being terminated
	ret = diagnosticTaskCancel(downloadDiagnostic, DownloadDiagnostics_DiagnosticsState);
	return ret;
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.DownloadDiagnostics.DownloadURL
 */
int
set_InternetGatewayDevice_DownloadDiagnostics_DownloadURL(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_DOWNLOAD_DIAGNOSTICS
	int ret;
	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_DownloadDiagnostics_DownloadURL: updateParamValue() error\n");
		)
		return ret;
	}
	//If the test is in progress - test being terminated
	ret = diagnosticTaskCancel(downloadDiagnostic, DownloadDiagnostics_DiagnosticsState);
	return ret;
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.DownloadDiagnostics.EthernetPriority
 */
int
set_InternetGatewayDevice_DownloadDiagnostics_EthernetPriority(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_DOWNLOAD_DIAGNOSTICS
	int ret;
	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_DownloadDiagnostics_EthernetPriority: updateParamValue() error2\n");
		)
		return ret;
	}
	//If the test is in progress - test being terminated
	ret = diagnosticTaskCancel(downloadDiagnostic, DownloadDiagnostics_DiagnosticsState);
	return ret;
#endif
	return OK;
}

/**
 * Parameter Name: InternetGatewayDevice.DownloadDiagnostics.DSCP
 */
int
set_InternetGatewayDevice_DownloadDiagnostics_DSCP(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_DOWNLOAD_DIAGNOSTICS
	int ret;
	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_InternetGatewayDevice_DownloadDiagnostics_DSCP: updateParamValue() error2\n");
		)
		return ret;
	}
	//If the test is in progress - test being terminated
	ret = diagnosticTaskCancel(downloadDiagnostic, DownloadDiagnostics_DiagnosticsState);
	return ret;
#endif
	return OK;
}
