#ifndef X_TWC_COM_TEST_INTERFACE_H_
#define X_TWC_COM_TEST_INTERFACE_H_

#include "paramaccess.h"
#include "parameter.h"

#ifdef __cplusplus
extern "C"
{
#endif

int get_Device_X_TWC_COM_TEST_UpgradeStatus(const char *name, ParameterType type, ParameterValue *value);

int set_Device_X_TWC_COM_TEST_DownloadImage(const char *name, ParameterType type, ParameterValue *value);

int set_Device_X_TWC_COM_TEST_InstallImage(const char *name, ParameterType type, ParameterValue *value);

#ifdef __cplusplus
}
#endif

#endif /* X_TWC_COM_TEST_INTERFACE_H_ */
