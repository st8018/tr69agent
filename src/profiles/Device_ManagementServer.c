#include "dimark_globals.h"
#include "Device_ManagementServer.h"
#include <openssl/ssl.h>
#include <openssl/x509.h>

#define FALSE 0
#define TRUE 1

#define _BEGIN_CERT_ "-----BEGIN CERTIFICATE-----\n"
#define _END_CERT_ "-----END CERTIFICATE-----"
#define _LINE_LENGTH_  64

#define _PREFIX_ "Device.ManagementServer.X_TWC_COM_RootCertificate"

char* STR_FALSE = "FALSE";
char* STR_TRUE = "TRUE";

static char cert_Data[1024*2] = {0,};
static bool isCert = false;
static bool isCert_enable = false;

int get_Device_ManagementServer_ValidateManagementServerCertificate(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    if (isCert) {
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
        BOOL_GET value = TRUE;
    } else {
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
        BOOL_GET value = FALSE;
    }
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return OK;
}

int get_Device_ManagementServer_ValidateDownloadServerCertificate(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    BOOL_GET value = FALSE;
    return OK;
}

int get_Device_ManagementServer_RootCertificateNumberOfEntries(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    //Max number of root certificates is 4 according to TWC specification.
    UINT_GET value = 4;
    return OK;
}

int set_Device_ManagementServer_ValidateManagementServerCertificate(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    char ret[8];
    bool b;
    if (isCert) {
        b = TRUE;
    } else {
        b = FALSE;
    }
    if (b != BOOL_GET value) {
        if (BOOL_GET value == TRUE) {
            isCert=true;
        } else {
            isCert=false;
        }
        setAsyncInform(true);
    }
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return OK;
}

static X509* getX509(int idx)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    char certdata[1024*2];
    char name[64];
    int i, line_cnt, buf_length, ret;
    char *buf, *cur, *cur_certdata;
    BIO* cbio;
    X509* cert;
    memset(certdata, 0x00, 1024*2);
    sprintf(name, "%s.%d.Certificate", _PREFIX_, idx);
    memcpy(certdata, cert_Data, 1024*2);
    //persistence_get(PERSISTENCE_ROOTCA, name, certdata, certdata);
    if (strlen(certdata) == 0) {
        return NULL;
    }
    line_cnt = strlen(certdata)/ _LINE_LENGTH_;
    buf_length = strlen(_BEGIN_CERT_) + (_LINE_LENGTH_ + 1) * (line_cnt + 1) + strlen(_END_CERT_) + 1;
    buf = malloc(buf_length);
    cur = buf;
    cur_certdata = certdata;
    ret = sprintf(cur, _BEGIN_CERT_);
    cur += ret;
    for (i = 0; i < line_cnt; i++)
    {
        ret = snprintf(cur, _LINE_LENGTH_ + 1, "%s", cur_certdata);
        cur += ret > _LINE_LENGTH_ ? _LINE_LENGTH_ : ret;
        cur_certdata += _LINE_LENGTH_;
        *cur = '\n';
        cur++;
    }
    cur += sprintf(cur, "%s\n", cur_certdata);
    sprintf(cur, _END_CERT_);

    cbio = BIO_new_mem_buf((void*)buf, -1);
    cert = PEM_read_bio_X509(cbio, NULL, 0, NULL);
    BIO_free(cbio);
    free(buf);
    return cert;
}

int get_Device_ManagementServer_X_TWC_COM_RootCertificate_Enabled(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    if (isCert_enable) {
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
        BOOL_GET value = TRUE;
    } else {
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
        BOOL_GET value = FALSE;
    }
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return OK;
}

int get_Device_ManagementServer_X_TWC_COM_RootCertificate_Certificate(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    char ret[1024*2];
    memset(ret, 0x00, 1024*2);
    memcpy(ret, cert_Data, 1024*2);
    value->out_cval = (char *)emallocTemp(strlen(ret) + 1, 1);
    if (value->out_cval != NULL) {
        strcpy(value->out_cval, ret);
        return OK;
    } else {
        g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
        return ERR_RESOURCE_EXCEEDED;
    }
}

int get_Device_ManagementServer_X_TWC_COM_RootCertificate_LastModif(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return ERR_METHOD_NOT_SUPPORTED;
}

int get_Device_ManagementServer_X_TWC_COM_RootCertificate_SerialNumber(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return ERR_METHOD_NOT_SUPPORTED;
}

int get_Device_ManagementServer_X_TWC_COM_RootCertificate_Issuer(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    int idx = getIdx(name, _PREFIX_);
    X509* cert = getX509(idx);
    char buf[1024];
    if (cert == NULL)
    { 
        g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
        char* ret = "Empty";
        value->out_cval = (char *)emallocTemp(strlen(ret) + 1, 1);
        if (value->out_cval != NULL) {
            g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
            strcpy(value->out_cval, ret);
        } else {
            g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
            return ERR_RESOURCE_EXCEEDED;
        }
    }
    else
    {
        g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
        X509_NAME_oneline(X509_get_issuer_name(cert), buf, sizeof(buf));
        value->out_cval = (char *)emallocTemp(strlen(buf) + 1, 1);
        if (value->out_cval != NULL) {
            g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
            strcpy(value->out_cval, buf);
        } else {
            g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
            return ERR_RESOURCE_EXCEEDED;
        }
    }
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return OK;
}

int get_Device_ManagementServer_X_TWC_COM_RootCertificate_NotBefore(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return ERR_METHOD_NOT_SUPPORTED;
}

int get_Device_ManagementServer_X_TWC_COM_RootCertificate_NotAfter(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return ERR_METHOD_NOT_SUPPORTED;
}

int get_Device_ManagementServer_X_TWC_COM_RootCertificate_Subject(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);

    int idx = getIdx(name, _PREFIX_);
    X509* cert = getX509(idx);
    char buf[1024];
    if (cert == NULL)
    {
        char* ret = "Empty";
        value->out_cval = (char *)emallocTemp(strlen(ret) + 1, 1);
        if (value->out_cval != NULL) {
            strcpy(value->out_cval, ret);
        } else {
            g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
            return ERR_RESOURCE_EXCEEDED;
        }
    }
    else
    {
        X509_NAME_oneline(X509_get_subject_name(cert), buf, sizeof(buf));
        value->out_cval = (char *)emallocTemp(strlen(buf) + 1, 1);
        if (value->out_cval != NULL) {
            g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
            strcpy(value->out_cval, buf);
        } else {
            g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
            return ERR_RESOURCE_EXCEEDED;
        }
    }
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return OK;
}

int get_Device_ManagementServer_X_TWC_COM_RootCertificate_SubjectAlt(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return ERR_METHOD_NOT_SUPPORTED;
}

int get_Device_ManagementServer_X_TWC_COM_RootCertificate_SignatureAlgorithm(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return ERR_METHOD_NOT_SUPPORTED;
}

int set_Device_ManagementServer_X_TWC_COM_RootCertificate_Enabled(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    if (BOOL_GET value == TRUE) {
        g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
        isCert_enable = true;
    } else {
        g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
        isCert_enable = false;
    }
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return OK;
}

int set_Device_ManagementServer_X_TWC_COM_RootCertificate_Certificate(
        const char *name, ParameterType type, ParameterValue *value)
{
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    memset(cert_Data, 0x00, 1024*2);
    memcpy(cert_Data, value->in_cval, 1024*2);
    g_printf("st8018 %s[%d]\n", __FUNCTION__, __LINE__);
    return OK;
}




