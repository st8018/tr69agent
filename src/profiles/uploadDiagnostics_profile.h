/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * uploadDiagnostics_profile.h
 *
 *  Created on: Oct 31, 2011
 *      Author: San
 */

#ifndef uploadDiagnostics_profile_H_
#define uploadDiagnostics_profile_H_

#include "diagParameter.h"
#include "paramaccess.h"
#include "parameter.h"

// uploadDiagnostics Profile. Setters:
int set_InternetGatewayDevice_UploadDiagnostics_DiagnosticsState(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_UploadDiagnostics_Interface(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_UploadDiagnostics_UploadURL(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_UploadDiagnostics_DSCP(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_UploadDiagnostics_EthernetPriority(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_UploadDiagnostics_TestFileLength(const char *, ParameterType, ParameterValue *);
#endif /* uploadDiagnostics_profile_H_ */
