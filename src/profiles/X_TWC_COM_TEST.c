#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <net/if.h>
#include <arpa/inet.h>

#include "dimark_globals.h"
#include "../paramaccess.h"
#include "parameterStore.h"
#include "utils.h"
#include "parameter.h"
#include "X_TWC_COM_TEST.h"

#include <string.h>
#include "libIBus.h"
#include "swupMgr.h"

int get_Device_X_TWC_COM_TEST_UpgradeStatus(const char *name, ParameterType type, ParameterValue *value)
{
    IARM_Bus_SWUPMgr_GetDownloadState_Param_t param;

    /* populate param here */
    IARM_Bus_Call(IARM_BUS_SWUPMGR_NAME,
            IARM_BUS_SWUPMGR_API_GetDownloadState,
            (void *)&param,
            sizeof(param));

    DEBUG_OUTPUT (
            dbglog (SVR_INFO, DBG_MAIN, "upgradeState:%d\n",
                param.upgradeState);
            )

    value->out_cval = (char *)emallocTemp(12, 1);

    if (value->out_cval != NULL) {
        switch(param.upgradeState){
            case IARM_BUS_SWUPMGR_SWUPGRADE_STATE_IDLE:
                strcpy(value->out_cval, "Idle");
                break;
            case IARM_BUS_SWUPMGR_SWUPGRADE_STATE_CONFIGURING:
                strcpy(value->out_cval, "Configuring");
                break;
            case IARM_BUS_SWUPMGR_SWUPGRADE_STATE_PREPARING:
                strcpy(value->out_cval, "Preparing");
                break;
            case IARM_BUS_SWUPMGR_SWUPGRADE_STATE_DOWNLOADING:
                strcpy(value->out_cval, "Downloading");
                break;
            case IARM_BUS_SWUPMGR_SWUPGRADE_STATE_DOWNLOADED:
                strcpy(value->out_cval, "Downloaded");
                break;
            case IARM_BUS_SWUPMGR_SWUPGRADE_STATE_WRITING:
                strcpy(value->out_cval, "Writing");
                break;
            case IARM_BUS_SWUPMGR_SWUPGRADE_STATE_COMPLETED:
                strcpy(value->out_cval, "Completed");
                break;
            case IARM_BUS_SWUPMGR_SWUPGRADE_STATE_REBOOTING:
                strcpy(value->out_cval, "Rebooting");
                break;
            case IARM_BUS_SWUPMGR_SWUPGRADE_STATE_FAILED:
                strcpy(value->out_cval, "Failed");
                break;
        }
        return OK;
    } else {
        DEBUG_OUTPUT (
                dbglog (SVR_ERROR, DBG_MAIN, "%s[%d]\n",
                    __FUNCTION__, __LINE__);
                )
            return ERR_RESOURCE_EXCEEDED;
    }
}

int set_Device_X_TWC_COM_TEST_DownloadImage(const char *name, ParameterType type, ParameterValue *value)
{
    IARM_Bus_SWUPMgr_InvokeSWUpgradeService_Param_t param;

    strcpy(param.commandKey, "testCommandKey");
    strcpy(param.url, value->in_cval);
    strcpy(param.userName, "");
    strcpy(param.password, "");
    param.fileSize = 91*1024*1024;
    strcpy(param.targetFileName, "");
    param.delaySeconds = 60;
    strcpy(param.successURL, "");
    strcpy(param.failureURL, "");

    DEBUG_OUTPUT (
       dbglog (SVR_INFO, DBG_MAIN, "Calling IARM_BUS_SWUPMGR_API_InvokeSWUpgradeService\n");
   )

    /* populate param here */
    IARM_Bus_Call(IARM_BUS_SWUPMGR_NAME,
            IARM_BUS_SWUPMGR_API_InvokeSWUpgradeService,
            (void *)&param,
            sizeof(param));

    return OK;
}

int set_Device_X_TWC_COM_TEST_InstallImage(const char *name, ParameterType type, ParameterValue *value)
{
    int ret=-1;
    if (BOOL_GET value == 1) {

        IARM_Bus_SWUPMgr_InstallFirmwareUpdate_Param_t param;

        param.afterReboot = false;

        /* populate param here */
        IARM_Bus_Call(IARM_BUS_SWUPMGR_NAME,
                IARM_BUS_SWUPMGR_API_InstallFirmwareUpdate,
                (void *)&param,
                sizeof(param));

        DEBUG_OUTPUT (
                dbglog(SVR_INFO, DBG_TRANSFER,"result:%d\n", param.result);
                )

            if (param.result == 0) {
                DEBUG_OUTPUT (
                        dbglog(SVR_ERROR, DBG_TRANSFER, "Installation of a SW upgrade image is failed due to error=%s\n", param.errorMessage);
                        )
                    return -1;
            }
    }

    return ret;
}
