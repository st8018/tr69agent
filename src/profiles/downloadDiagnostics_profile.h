/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * downloadDiagnostics_profile.h
 *
 *  Created on: Oct 31, 2011
 *      Author: San
 */

#ifndef DownloadDiagnostics_profile_H_
#define DownloadDiagnostics_profile_H_

#include "diagParameter.h"
#include "paramaccess.h"
#include "parameter.h"

// DownloadDiagnostics Profile. Setters:
int set_InternetGatewayDevice_DownloadDiagnostics_DiagnosticsState(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_DownloadDiagnostics_Interface(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_DownloadDiagnostics_DownloadURL(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_DownloadDiagnostics_EthernetPriority(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_DownloadDiagnostics_DSCP(const char *, ParameterType, ParameterValue *);
#endif /* DownloadDiagnostics_profile_H_ */
