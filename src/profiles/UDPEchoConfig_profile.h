/***************************************************************************
 *    Copyright (C) 2004-2012 Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * UDPEchoConfig_profile.h
 *
 *  Created on: Jun 12, 2012
 *      Author: San
 */

#ifndef UDPEchoConfig_profile_H_
#define UDPEchoConfig_profile_H_

#include "dimark_globals.h"
#include "diagParameter.h"
#include "paramaccess.h"
#include "parameter.h"

// UDPEchoConfig Profile. Setters:
int set_UDPEchoConfig_Enable(const char *, ParameterType, ParameterValue *);
int set_UDPEchoConfig_Interface(const char *, ParameterType, ParameterValue *);
int set_UDPEchoConfig_UDPPort(const char *, ParameterType, ParameterValue *);

#endif /* UDPEchoConfig_profile_H_ */
