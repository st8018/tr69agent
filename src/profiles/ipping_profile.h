/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * ipping_profile.h
 *
 *  Created on: May 6, 2011
 *      Author: San
 */

#ifndef ipping_profile_H_
#define ipping_profile_H_

#include "paramaccess.h"
#include "parameter.h"

#ifdef HAVE_IP_PING_DIAGNOSTICS
#define		IPPingDiagnosticState_None							"None"
#define		IPPingDiagnosticState_Requested						"Requested"
#define		IPPingDiagnosticState_Complete						"Complete"
#define		IPPingDiagnosticState_CannotResolveHostNameError	"Error_CannotResolveHostName"
#define		IPPingDiagnosticState_InternalError					"Error_Internal"
#define		IPPingDiagnosticState_OtherError					"Error_Other"
#endif

// IPPing Profile. Getters:
int get_InternetGatewayDevice_IPPingDiagnostics_DiagnosticsState(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_IPPingDiagnostics_Interface(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_IPPingDiagnostics_Host(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_IPPingDiagnostics_NumberOfRepetitions(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_IPPingDiagnostics_Timeout(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_IPPingDiagnostics_DataBlockSize(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_IPPingDiagnostics_DSCP(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_IPPingDiagnostics_SuccessCount(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_IPPingDiagnostics_FailureCount(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_IPPingDiagnostics_AverageResponseTime(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_IPPingDiagnostics_MinimumResponseTime(const char *, ParameterType, ParameterValue *);
int get_InternetGatewayDevice_IPPingDiagnostics_MaximumResponseTime(const char *, ParameterType, ParameterValue *);

// IPPing Profile. Setters:
int set_InternetGatewayDevice_IPPingDiagnostics_DiagnosticsState(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_IPPingDiagnostics_Interface(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_IPPingDiagnostics_Host(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_IPPingDiagnostics_NumberOfRepetitions(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_IPPingDiagnostics_Timeout(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_IPPingDiagnostics_DataBlockSize(const char *, ParameterType, ParameterValue *);
int set_InternetGatewayDevice_IPPingDiagnostics_DSCP(const char *, ParameterType, ParameterValue *);
#endif /* ipping_profile_H_ */
