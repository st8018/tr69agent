/***************************************************************************
 *    Copyright (C) 2004-2012 Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * UDPEchoConfig_profile.c
 *
 *  Created on: Jun 12, 2012
 *      Author: San
 */

//#include <sys/sysinfo.h>
//#include <sys/ioctl.h>
//#include <sys/socket.h>
//#include <sys/types.h>
//#include <net/if.h>
//#include <arpa/inet.h>

#include "dimark_globals.h"
#include "paramaccess.h"
#include "parameterStore.h"
#include "parameter.h"
#include "diagParameter.h"
#include "diagParameter.h"
#include "UDPEchoConfig_profile.h"
#include "diagnosticsHandler.h"

// UDPEchoConfig Profile. Getters:
// None



/****************************************************************************************************************************************************/
// UDPEchoConfig Profile. Setters:
/****************************************************************************************************************************************************/
int
set_UDPEchoConfig_Enable(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_UDP_ECHO
	int ret;
	if ( (ret = updateParamValue(name, type, value)) != OK )
		return ret;
	if (value)
	{
		updateUDPEchoConfigEnable(value->in_int);
	}
#endif /* HAVE_UDP_ECHO */
	return OK;
}


int
set_UDPEchoConfig_Interface(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_UDP_ECHO
	int ret;
	if (value)
	{
		ret = updateUDPEchoConfigInterface(value->out_cval);
		if (ret != OK)
			return ERR_INVALID_PARAMETER_VALUE;
	}
	if ( (ret = updateParamValue(name, type, value)) != OK )
		return ret;
#endif /* HAVE_UDP_ECHO */
	return OK;
}

int
set_UDPEchoConfig_UDPPort(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_UDP_ECHO
	int ret;
	if ( (ret = updateParamValue(name, type, value)) != OK )
		return ret;
	if (value)
	{
		updateUDPEchoConfigPort(value->out_uint);
	}
#endif /* HAVE_UDP_ECHO */
	return OK;
}


