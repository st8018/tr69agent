/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"
#include "paramaccess.h"
#include "utils.h"
#include "option.h"
#include "optionStore.h"

int
storeVouchers( struct ArrayOfVouchers *vouchers )
{
#ifdef HAVE_VOUCHERS_OPTIONS

	const char *filename;
	FILE *vFile;
	int idx;
	int vouchersSize;
	int ret = OK;

	if (!vouchers)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_VOUCHERS, "storeVouchers(): function parameter 'vouchers' can't be a NULL\n");
		)
		return ERR_INTERNAL_ERROR;
	}

	vouchersSize = vouchers->__size;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_VOUCHERS, "Array of Vouchers: %d\n", vouchersSize );
	)

	for ( idx = 0; idx < vouchersSize; idx++ )
	{
		filename = getVoucherFilename(idx);
		vFile = fopen( filename, "w+");
		fwrite( vouchers[idx].__ptrVoucher, strlen(vouchers[idx].__ptrVoucher), 1, vFile );
		fclose( vFile );
		ret = readVoucherFile( filename );
		if ( ret != OK )
			return ret;
	}
	return OK;
#else
	return ERR_METHOD_NOT_SUPPORTED;
#endif /* HAVE_VOUCHERS_OPTIONS */
}
