/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"

#ifdef HAVE_FILE

/* This file is an example for a file transfer callback function.
 * the fileDownloadCallback() is opening the transfered file and reads
 * byte by byte the data and calculates the checksum */
#include <time.h>

#include "ftcallback.h"
#include "parameter.h"


#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)

extern pthread_mutex_t paramLock;

static char * getVendorConfigFileVersionFromFile(const char *);
static char * getVendorConfigFileDescriptionFromFile(const char *);

/* Callback function is called before a file is downloaded */
int fileDownloadCallbackBefore(TransferEntry * te)
{
	int ret = OK;

	DEBUG_OUTPUT (
			dbglog( SVR_DEBUG, DBG_TRANSFER, "Download Callback Before: %s  Filetype: %s\n", te->targetFileName, te->fileType);
	)

	return ret;
}

/* Callback function is called after a file is downloaded */
int fileDownloadCallbackAfter(TransferEntry * te)
{
	int ret = OK;
	int i;
	struct ArrayOfParameterInfoStruct ParameterList = {NULL, 0};
	struct ParameterInfoStruct *tmpParameterInfoStruct;
	char paramName[MAX_PARAM_PATH_LENGTH];
	char aliasValue[MAX_ALIAS_VALUE_SIZE];
	char * paramNameValue;
	unsigned int instanceNumber = 0;
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	VendorConfigFile vendorConfigFile = {"\0", "\0", "\0", {0,0}, "\0" };
#else
	VendorConfigFile vendorConfigFile = {"\0", "\0", {0,0}, "\0" };
#endif
	char vendorConfigFileDate[42];


	DEBUG_OUTPUT (
			dbglog( SVR_DEBUG, DBG_TRANSFER, "Download/ScheduleDownload Callback After started: filename=[%s],  Filetype=[%s], Checksum=%d\n", te->targetFileName, te->fileType, 0 );
	)
#if !defined(WITH_DEVICE_ROOT) || defined(TR_181_DEVICE)
	if (strcmp(te->fileType, VENDOR_CONFIGURATION_FILE) == 0)
	{
		ret = getParameterNames(VENDOR_CONFIG_FILE, true, &ParameterList);
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog( SVR_ERROR, DBG_TRANSFER, "Download/ScheduleDownload Callback After started filename=[%s]: getParameterNames(%s) ret = %d\n",
							te->targetFileName, VENDOR_CONFIG_FILE, ret);
			)
			return ret;
		}

		strncpy(vendorConfigFile.Name, te->targetFileName, 64);
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
		strcpy(vendorConfigFile.Alias, "cpe-");
		strncat(vendorConfigFile.Alias, vendorConfigFile.Name, 60);
#endif
		strncpy(vendorConfigFile.Version, getVendorConfigFileVersionFromFile(te->targetFileName), 16);
		gettimeofday(&vendorConfigFile.Date, NULL);
		sprintf(vendorConfigFileDate, "%ld|%ld", vendorConfigFile.Date.tv_sec, vendorConfigFile.Date.tv_usec);
		strncpy(vendorConfigFile.Description, getVendorConfigFileDescriptionFromFile(te->targetFileName), 256 );

		for (i=0; i<ParameterList.__size; i++)
		{
			tmpParameterInfoStruct = (struct ParameterInfoStruct *) *(ParameterList.__ptrParameterInfoStruct + i);

			memset(paramName, 0 , sizeof(paramName));
			strcpy(paramName, tmpParameterInfoStruct->Name);
			strcat(paramName, "Name");
			ret = getParameter(paramName, &paramNameValue);
			if (ret != OK)
			{
				DEBUG_OUTPUT (
						dbglog( SVR_ERROR, DBG_TRANSFER, "Download/ScheduleDownload Callback After started filename=[%s]: getParameter() ret = %d\n", te->targetFileName, ret);
				)
				return ret;
			}
			if (strCmp(paramNameValue, te->targetFileName) > 0)
			{
				break;
			}
		}

		if ( i < ParameterList.__size ) // if was found VendorConfigFile with Name = filename
		{
			// Update object

			//The CPE MUST NOT change the Alias parameter value.

			// InternetGatewayDevice.DeviceInfo.VendorConfigFile.*.Version
			sprintf(paramName, "%sVersion\0",tmpParameterInfoStruct->Name);
			setUniversalParamValueInternal(NULL, paramName, 1, vendorConfigFile.Version );

			// InternetGatewayDevice.DeviceInfo.VendorConfigFile.*.Date
			sprintf(paramName, "%sDate\0",tmpParameterInfoStruct->Name);
			setUniversalParamValueInternal(NULL, paramName, 1, &(vendorConfigFile.Date) );

			// InternetGatewayDevice.DeviceInfo.VendorConfigFile.*.Description
			sprintf(paramName, "%sDescription\0",tmpParameterInfoStruct->Name);
			setUniversalParamValueInternal(NULL, paramName, 1, vendorConfigFile.Description );
		}
		else
		{
			// Add new object
			pthread_mutex_lock(&paramLock);
#ifdef WITH_USE_SQLITE_DB
			executeDBCommand(BEGIN);
#endif
			ret = addObjectIntern(VENDOR_CONFIG_FILE, &instanceNumber, RPC_COMMAND_IS_CALLED_INTERNAL, CALL_USER_DEFINED_CALLBACK);
			if (ret != OK)
			{
				DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_TRANSFER, "Download/ScheduleDownload Callback After started: addObjectIntern() "
							"has returned error: ret = %d\n", ret);
				)
#ifdef WITH_USE_SQLITE_DB
				executeDBCommand(END);
#endif
				pthread_mutex_unlock(&paramLock);
				return ret;
			}

			// InternetGatewayDevice.DeviceInfo.VendorConfigFile.*.Name
			sprintf(paramName, "%s%u.Name\0",VENDOR_CONFIG_FILE, instanceNumber);
			setUniversalParamValueInternal(NULL, paramName, 1, vendorConfigFile.Name );


#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
			// check is Alias value correct in the pathName.
			if (!isAliasValueValid(vendorConfigFile.Alias))
				sprintf(vendorConfigFile.Alias, "cpe-VCF_%u\0", instanceNumber);
			// InternetGatewayDevice.DeviceInfo.VendorConfigFile.*.Alias
			sprintf(paramName, "%s%u.Alias\0",VENDOR_CONFIG_FILE, instanceNumber);
			setUniversalParamValueInternal(NULL, paramName, 0, vendorConfigFile.Alias );
#endif

			// InternetGatewayDevice.DeviceInfo.VendorConfigFile.*.Version
			sprintf(paramName, "%s%u.Version\0",VENDOR_CONFIG_FILE, instanceNumber);
			setUniversalParamValueInternal(NULL, paramName, 1, vendorConfigFile.Version );

			// InternetGatewayDevice.DeviceInfo.VendorConfigFile.*.Date
			sprintf(paramName, "%s%u.Date\0",VENDOR_CONFIG_FILE, instanceNumber);
			setUniversalParamValueInternal(NULL, paramName, 1, &(vendorConfigFile.Date) );

			// InternetGatewayDevice.DeviceInfo.VendorConfigFile.*.Description
			sprintf(paramName, "%s%u.Description\0",VENDOR_CONFIG_FILE, instanceNumber);
			setUniversalParamValueInternal(NULL, paramName, 1, vendorConfigFile.Description );

#ifdef WITH_USE_SQLITE_DB
			executeDBCommand(END);
#endif
			pthread_mutex_unlock(&paramLock);
		}

	}
#endif
	DEBUG_OUTPUT (
			dbglog( SVR_DEBUG, DBG_PARAMETER, "Download/ScheduleDownload Callback After finished: filename=[%s],  Filetype=[%s]\n", te->targetFileName, te->fileType );
	)

	return ret;
}


/* Callback function is called before a file is downloaded */
int fileScheduleDownloadCallbackBefore(TransferEntry *te, int isFirstTimeWindow)
{
	int ret = OK;

	DEBUG_OUTPUT (
			dbglog( SVR_DEBUG, DBG_TRANSFER, "ScheduleDownload Callback Before: %s  Filetype: %s\n", te->targetFileName, te->fileType);
	)
	if ( (isFirstTimeWindow && strcmp(te->TimeWindow1->WindowMode, "4 Confirmation Needed") == 0 )
		|| (!isFirstTimeWindow && strcmp(te->TimeWindow2->WindowMode, "4 Confirmation Needed") == 0 ) )
	{
		if ( !isConfirmationTrue(te) )
			return ERR_SCHEDULE_DOWNLOAD_ISNOT_CONFIRMED;
	}

	return ret;
}

/* Callback function is called after a file is ScheduleDownloaded */
int fileScheduleDownloadCallbackAfter(TransferEntry *te, int isFirstTimeWindow)
{
	return fileDownloadCallbackAfter(te);
}



/* Callback function is called before a file is uploaded */
int fileUploadCallbackBefore(TransferEntry *te)
{
	int ret = OK;

	DEBUG_OUTPUT (
			dbglog( SVR_DEBUG, DBG_PARAMETER, "Upload Callback Before: targetFileName = %s  Checksum = %d\n", te->targetFileName, 0 );
	)

	return ret;
}

/* Callback function is called after a file is uploaded */
int fileUploadCallbackAfter(TransferEntry *te)
{
	int ret = OK;

	DEBUG_OUTPUT (
			dbglog( SVR_DEBUG, DBG_PARAMETER, "Upload Callback After: targetFileName = %s  Checksum = %d\n", te->targetFileName, 0 );
	)

	return ret;
}

/*
 * The function returns Vendor Config File Version from Vendor Config File
 * */
static char * getVendorConfigFileVersionFromFile(const char * fileName)
{
	if (!fileName)
		return NULL;

	//TODO: read Version from Vendor Config File

	return "\0";
}

static char * getVendorConfigFileDescriptionFromFile(const char * fileName)
{
	char str[257];
	if (!fileName)
		return NULL;
/*
	strcpy(str, "Description of vendor config file " );
	strncat(str, fileName, sizeof(str) - strlen(str) - 1);
*/	return "Description of vendor config file\0";
}


/* if Confirmation is true then function returns 1
 * else returns 0
 * */
int isConfirmationTrue(TransferEntry *te)
{
	/*
	"The CPE MAY support “4 Confirmation Needed”. This means that
	the CPE MUST ask for and receive confirmation before performing
	and applying the download. It is outside the scope of this
	specification how the CPE asks for and receives this confirmation.
	If confirmation is not received, this time window MUST NOT be used."
 	 */

	// TODO: ask the confirmation

	return false; // default
}
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */

#endif /* HAVE_FILE */
