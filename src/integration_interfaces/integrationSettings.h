/***************************************************************************
 *    Original code © Copyright (C) 2004-2013 by Dimark Software Inc.      *
 *    support@dimark.com                                                   *
 *    Modifications © 2013 Comcast Cable Communications, LLC               *
 ***************************************************************************/

#ifndef integrationSettings_H
#define integrationSettings_H

//#include <stdio.h>
/* Here you can include the header files needed you*/

#define FIRMWARE_IMAGENAME "Device.DeviceInfo.X_COMCAST-COM_FirmwareFilename"
#ifndef ENABLE_SYSTEMD
#define REBOOT_SCR_PATH "sh /rebootNow.sh"
#else
#define REBOOT_SCR_PATH "systemctl reboot"
#endif

/*! Events published from TR69 host interface */
typedef enum _TR69Agent_EventId_t {
    IARM_BUS_TR69Agent_ACS_CONN_EVENT,   	    /*!< Add Event  */
    IARM_BUS_TR69Agent_EVENT_MAX,           /*!< Maximum event id*/
} IARM_Bus_tr69Agent_EventId_t;

typedef enum _eACS_CONN_STATUS{
   ACS_CONNECTED,
   ACS_FAILED_TO_CONNECT
} eACS_CONN_STATUS;

/*! Event Data */
typedef struct _TR69Agent_EventData_t {
    int value;              /*!<  Value (/DOWN/REPEAT) */
}IARM_Bus_TR69Agent_EventData_t;


/******************** dimark_globals.h settings ********************/
/* Here you can define Dimark's macros or add your own */

/* This CWMP version will be used in the first Inform RPC after reboot by default.
 Change CWMP_DEFAULT_VERSION value if you want to compile the client with other default CWMP version.
 Use "urn:dslforum-org:cwmp-1-3" or "urn:dslforum-org:cwmp-1-2" or "urn:dslforum-org:cwmp-1-1" or "urn:dslforum-org:cwmp-1-0"
 !!! NOTE: If you add new cwmp version (cwmp-1-4, etc.) you MUST add the handling of it in the function setSuportedRPCInfoFromCWMPVersion() !!!
*/
//#define CWMP_DEFAULT_VERSION "urn:dslforum-org:cwmp-1-3"

//#define	BACKUP_FILES_DEFAULT_COUNT		3
//#define	DEBUG_LOG_FILE_NAME	"/var/tmp/HTTP.log"
//#define	TEST_LOG_FILE_NAME	"/var/tmp/TEST.log"

//#define	DEFAULT_SESSION_TIMEOUT_IN_HEADER	30


/******************** ban of object instance adding/deleting settings ********************/
//#define ADD_OBJ_CAN_BE_UNALLOWED // if defined the object can not be added if parent.addObjIdx<0.
//#define DEL_OBJ_CAN_BE_UNALLOWED // if defined the object can not be deleted if parent.delObjIdx<0.



/******************** Debug output settings ********************/
//#ifdef INTEL_LOGGER
//	#include "gw_ctx.h"
//	#include "logger.h"
//#endif

/* For example (if following is uncommented): the 'Dbg_level_enum' is declared and 'dbglog' macros is defined: */

//typedef enum dbg_level_enum
//{
//	SVR_DEBUG = 0,	//You can set some other DEBUG level number
//	SVR_INFO = 1,	//You can set some other INFO  level number
//	SVR_WARN = 2,	//You can set some other WARN  level number
//	SVR_ERROR = 3	//You can set some other ERROR level number
//#define DBG_LEVEL_ENUM_IS_DECLARED //If you declared 'Dbg_level_enum' then you MUST define DBG_LEVEL_ENUM_IS_DECLARED
//} Dbg_level_enum;

//#define dbglog(_severity, _modul, _format, args...) {\
//	printf("sev=%u, mod=%u\t", _severity, _modul);\
//	printf(_format, ##args);\
//}


/******************** IPPing diagnostic settings *******************/
//#define IPPING_COMMAND "ping" //Linux ping command (or path + command), specified by OS
//#define DOES_PING_SUPPORT_DSCP_ATTR 1 //set 0 or 1: does ping command support DSCP attribute?


/******************** traceRoute diagnostic settings ********************/
//#define TRACE_ROUTE_COMMAND "/usr/sbin/traceroute" //traceroute command (or path + command), specified by OS
//#define DOES_TRACE_ROUTE_SUPPORT_DSCP_ATTR 1 //set 0 or 1: does traceroute command support DSCP attribute?
//#define TRACE_ROUTE_PROTOCOL UDP //select ICMP or UDP or TCP


/******************** "uuid" header file settings ********************/
//#define UUID_H_FILE_PATH <uuid/uuid.h>


/******************** Linux installed commands settings ********************/
//#define MOVE_COMMAND_IS_NOT_INSTALLED // If 'Move' is not installed in the Embedded Linux, will be used 'Copy' and 'Remove' instead


/******************** bool type settings ********************/
/* You can define 'true' & 'false' values. Else the enum bool will be declared in the utils.h file */
//#define true	1
//#define false	0


/******************** dimclient.[ch] settings ********************/
/* Here you can define your initialization code to handle the signals. See examples. */
//#define EXTERNAL_INITIALIZATION_OF_SIGSEGV_HANDLER { signal(SIGSEGV, sigsegv_handler); }
//#define EXTERNAL_INITIALIZATION_OF_SIGINT_HANDLER { struct sigaction new_action;\
//	new_action.sa_handler = sigint_handler;\
//	sigemptyset (&new_action.sa_mask);\
//    sigaddset (&new_action.sa_mask, SIGSEGV);\
//	new_action.sa_flags = 0;\
//	sigaction (SIGINT, &new_action, NULL); }


/******************** callbacks settings ********************/
/* Here you can define your callback addition code. See examples.
 * The definition EXTERNAL_CALLBACK_ADDITION is used in the callback.c file. */
#define EXTERNAL_CALLBACK_ADDITION {addCallback(&initParametersBeforeCbOnceTest, &initParametersBeforeCbList); addCallback(&initParametersAfterDBOnceTest, &initParametersDoneCbList);}
#ifdef EXTERNAL_CALLBACK_ADDITION
int initParametersBeforeCbOnceTest(void); // This callback function is implemented in the integrationSettings.c file
int initParametersAfterDBOnceTest(void);
#endif

/* If CUSTOMER_FINISHING_FUNCTION is defined then customerFunctionAtFinish() function
 * will be called at any time when the Dimclient process ends. See examples. */
//#define CUSTOMER_FINISHING_FUNCTION
#ifdef CUSTOMER_FINISHING_FUNCTION
void customerFunctionAtFinish(void);
#endif


/******************** Connection Request settings ********************/
//#define	CR_URL_PATH_STORAGE_FILE_NAME	"/tmp/CR_URL_path.txt" //this file saves the last valid value of Path, that will be used after reboot.
char * customerCreationOfCR_URL_path();

/***** Connection Request DoS attack detection settings *****/
/* The CPE SHOULD restrict the number of Connection Requests for a particular CWMP Endpoint
 * that it accepts during a given period of time in order to further reduce the possibility
 * of a denial of service attack. If the CPE chooses to reject a Connection Request for this reason,
 * the CPE MUST respond to that Connection Request with an HTTP 503 status code (Service Unavailable).
 * In this case, the CPE SHOULD NOT include the HTTP Retry-After header in the response.
 * */
/* Defines for DoS attack recognition length of the time slot/interval in seconds */
//#define CR_TIME_INTERVAL				60 //sec

/* the number of allowed requests in one time slot/interval */
//#define CR_MAX_REQUESTS_PER_TIME 		12

/* Size of CR_time_deltas[] to save the statistics of last requests times. MUST be >= CR_MAX_REQUESTS_PER_TIME !!! */
//#define CR_REQUEST_STATISTICS_COUNT		CR_MAX_REQUESTS_PER_TIME

/* Optional setting (may be not defined):
 * the min time between requests, msec
 * Any response will not be sent if time between current
 * and previous HTTP requests < CR_MIN_TIME_BETWEEN_REQUESTS */
//#define CR_MIN_TIME_BETWEEN_REQUESTS	200 //in milliseconds

/* Optional setting (may be not defined):
 * This value means following:
 * if the time between last sent Inform with event "6 CONNECTION REQUEST" and
 * current time > MAX_INTERVAL_WITHOUT_CR_INFORM_DURING_DOS_ATTACK then
 * Inform with event "6 CONNECTION REQUEST" will be send
 * regardless of whether the attack is detected.*/
//#define MAX_INTERVAL_WITHOUT_CR_INFORM_DURING_DOS_ATTACK  30000 //in milliseconds

// !!! PLEASE NOTE: IF THESE DEFINES ARE NOT DEFINED IN THIS FILE THEN THEY WILL BE DEFINED TO DEFAULT VALUE IN THE FILE DoSAttackChecking.c !!!


/******************** Reboot/Factory Reset callback index settings ********************/
/* Here you can define a sum of callback function indexes for Reboot/Factory Reset behavior of Dimclient
 * If these defines are not defined here then they will be defined to default value in the parameter.h file
 * */
//#define REBOOT_CALLBACKS_INDEX_SUM  (RebootWithoutFactoryReset /*+ <some your index> +...*/ )
//#define FACTORYRESET_CALLBACKS_INDEX_SUM  (FactoryReset /*+ <some your index> +...*/ )


#endif /* integrationSettings_H */
