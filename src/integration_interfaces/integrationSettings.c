/***************************************************************************
 *    Original code © Copyright (C) 2004-2013 by Dimark Software Inc.      *
 *    support@dimark.com                                                   *
 *    Modifications © 2013 Comcast Cable Communications, LLC               *
 ***************************************************************************/

#include <integrationSettings.h>
#include <stdio.h>
#include <libxml/xmlreader.h>
#include <curl/curl.h>
#include <jansson.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>

/* Here you can include the header files needed you*/
#include "dimark_globals.h"
#include "paramaccess.h"
#include "debug.h"
#include "parameter.h"
#include "libIBus.h"
#include "libIARM.h"
#include "hostIf_tr69ReqHandler.h"
static bool eventRecFlag = false;
static bool bootstrapEventFlag = false;
static bool acsConnStatus = false;

#define IARM_TR69_CLIENT        "tr69Client"
#define IARM_MEMTYPE_PROCESSLOCAL 2

#define DEV_MOCA_ASSOC          "Device.MoCA.Interface.1.AssociatedDevice."
#define DEV_MOCA_NUM_ENTRIES    "Device.MoCA.Interface.1.AssociatedDeviceNumberOfEntries"
#define DEV_ETH_INTERFACE       "Device.Ethernet.Interface."
#define DEV_ETH_NUM_ENTRIES    "Device.Ethernet.InterfaceNumberOfEntries"
#define DEV_IP_INTERFACE        "Device.IP.Interface."
#define DEV_IP_NUM_ENTRIES    	"Device.IP.InterfaceNumberOfEntries"
#define DEV_IP_ACTIVEPORT 		"Device.IP.ActivePort."
#define DEV_IP_ACTIVEPORT_NUM_ENTRIES		"Device.IP.ActivePortNumberOfEntries"
#define DEVINFO_PROCESSOR      "Device.DeviceInfo.Processor."
#define DEVINFO_PROCESSOR_NUM_ENTRIES      "Device.DeviceInfo.ProcessorNumberOfEntries"
#define DEVINFO_PROCESS        "Device.DeviceInfo.ProcessStatus.Process."
#define DEVINFO_PROCESS_NUM_ENTRIES		"Device.DeviceInfo.ProcessStatus.ProcessNumberOfEntries"
#define DEV_MOCA_MESH          "Device.MoCA.Interface.1.X_RDKCENTRAL-COM_MeshTable."
#define DEV_MOCA_MESH_ENTRIES    "Device.MoCA.Interface.1.X_RDKCENTRAL-COM_MeshTableNumberOfEntries"

#define SUPPORTED_DATA_MODEL_NUMBER_OF_ENTRIES 2
#define URL_LEN 260
#define URN_LEN 260

#define URL_TR_181 "http://www.broadband-forum.org/cwmp/tr-181-2-2-0.html"
#define URN_TR_181 "http://www.broadband-forum.org/cwmp/tr-181-2-2-0.html#D.Device."

#define URL_TR_135 "http://www.broadband-forum.org/cwmp/tr-135-1-2-0.html"
#define URN_TR_135 "http://www.broadband-forum.org/cwmp/tr-135-1-2-0.html#D.STBService.{i}."

#define XML_FILE_NAME "/usr/local/tr69agent/data-model.xml"


typedef struct Device_DeviceInfo_SupportedDataModel
{
    char cURL[URL_LEN];
    char cURN[URN_LEN];
    char *pcFeatures;
} DeviceSupportedDataModel;

typedef enum ESupportedDataModelMembers
{
    eURL = 0,
    eURN,
    eFeatures
} ESupportedDataModelMembers;

typedef enum EProfileTypes
{
    eDeviceType = 0,
    eSTBServicesType
} EProfileTypes;

#ifdef AUTH_SERVICE
typedef enum _EBUILD_TYPE {
    BUILD_TYPE_INVALID = -1,
    BUILD_TYPE_DEV	= 1,
    BUILD_TYPE_VBN,
    BUILD_TYPE_PROD
} EBuildType;
#endif

static DeviceSupportedDataModel stDataModel = {{'\0'},{'\0'},NULL};

IARM_Result_t IARM_TR69_Client_Connect(char *);
IARM_Result_t IARM_TR69_Client_DisConnect();
IARM_Result_t get_tr69hostIf_Param(const char *, ParameterType , void **);
int get_ParamValues_tr69hostIf (const char *, ParameterType , ParameterValue *);
int set_ParamValues_tr69hostIf (const char *, ParameterType , ParameterValue *);
int initialize_DeviceStr_Params();
bool get_boolean(const char *);
void put_boolean(char *, bool);
void put_int(char *, int);
pthread_mutex_t notifyParamLock = PTHREAD_MUTEX_INITIALIZER;
static void  _tr69Event_handler(const char *, IARM_Bus_tr69HostIfMgr_EventId_t, void *, size_t);
extern struct ConfigManagement dimclientConfStruct;

extern int checkAndDeleteTblObjectLastInstances(const char *, const char *, const char *, unsigned int);

void callRebootScript();
int checkAndAddTblObjectInstances(const char *, const char *, const char *);

int get_Device_DeviceInfo_SupportedDataModelNumberOfEntries(const char *, ParameterType, ParameterValue *);
int get_Device_DeviceInfo_SupportedDataModel_URL(const char *, ParameterType , ParameterValue *);
int get_Device_DeviceInfo_SupportedDataModel_URN(const char *, ParameterType , ParameterValue *);
int get_Device_DeviceInfo_SupportedDataModel_Features(const char *, ParameterType , ParameterValue *);
void deleteTbleInstancesAndResetNextInstNumber();
#ifdef AUTH_SERVICE
int createAndCheckBootStarpEventFile(char *bootstrapFile);
bool readAuthSer_ACSConfig(const char *, char **, char *);
int init_BootStrapAuthService_ACSSettingConfig();
EBuildType read_BuildType();
#endif /* #ifdef AUTH_SERVICE */

/******************** callbacks settings ********************/
/* Example of a callback function only called once */
int initParametersBeforeCbOnceTest(void)
{
    /* Here you can add some your actions, function calls, etc. */
    DEBUG_OUTPUT (
        dbglog (SVR_INFO, DBG_CALLBACK, "initParametersBeforeCbOnceTest(): PreSession Callback only once\n");
    )

    IARM_Result_t retCode = IARM_TR69_Client_Connect(IARM_TR69_CLIENT);
    if(IARM_RESULT_SUCCESS != retCode)
    {
        DEBUG_OUTPUT (
            dbglog (SVR_ERROR, DBG_ACCESS, "main(): Failed to Connect TR69 Client to IARM Bus, retCode = %d\n", retCode);
        )
        exit(0);
    }

    return 0;
}


/* callback function after database initialize */
int initParametersAfterDBOnceTest()
{
    DEBUG_OUTPUT (
        dbglog (SVR_INFO, DBG_CALLBACK, "%s(): PreSession Callback only once\n", __FUNCTION__);
    )

    //--------------------------------
    // Delete all the table instances
    //---------------------------------
    deleteTbleInstancesAndResetNextInstNumber();

    while( true)
    {
        if(OK == initialize_DeviceStr_Params())
        {
            break;
        }
        sleep (5);
    }
    IARM_Bus_Call(IARM_BUS_TR69HOSTIFMGR_NAME, IARM_BUS_TR69HOSTIFMGR_API_RegisterForEvents, NULL, 0);
    eventRecFlag = true;
}


/* Customer function. It will be called at any time when the Dimclient process ends. */
void customerFunctionAtFinish(void)
{
    /* Here you can add some your actions, function calls, etc. */
    IARM_TR69_Client_DisConnect();
    return;
}

/* Customer function. In this function you can create Connection Request URL path and return this value.
 * For example: if CR host is http://192.15.1.5 and you will create the path = "abcdefjh"
 * then CR URL will be == http://192.15.1.5/abcdefjh
 * Max len CR URL path is defined in the CR_URL_PATH_LEN == 8
 * If this function returns NULL the CR URL path will be created automatically by dimclient.
 *
 * PLEASE NOTE: A STANDARD REQUIRES THE UNIQUE VALUE OF Connection Request URL path FOR EACH DEVICE !!!
 *  */
char * customerCreationOfCR_URL_path()
{
//For example:
//	static char CR_URL_path[8+1] = {"MyCRpath\0"};
//	return CR_URL_path;

//or
//	return "MyCRpth";

    return NULL;
}


/* IARM INIT and Connect Call*/
IARM_Result_t IARM_TR69_Client_Connect(char *tr69ClientName)
{
    IARM_Result_t ret = IARM_RESULT_IPCCORE_FAIL;

    ret = IARM_Bus_Init(tr69ClientName);

    if(ret != IARM_RESULT_SUCCESS)
    {
        DEBUG_OUTPUT (
            dbglog (SVR_ERROR, DBG_ACCESS, "Error initializing IARM Bus for '%s'\r\n", tr69ClientName);
        )
        return ret;
    }

//    printf ("[%s] Initializing IARM Bus.\n", __FUNCTION__);

    ret = IARM_Bus_Connect();
    if(ret != IARM_RESULT_SUCCESS)
    {
        DEBUG_OUTPUT (
            dbglog(SVR_ERROR, DBG_ACCESS,"Error connecting to IARM Bus for '%s'\r\n", tr69ClientName);	)
        return ret;
    }

//  printf("[%s:%s:%d] Successfully connected '%s' to IARM Bus\n", __FILE__, __FUNCTION__, __LINE__, tr69ClientName);


    IARM_Bus_RegisterEventHandler(IARM_BUS_TR69HOSTIFMGR_NAME, IARM_BUS_TR69HOSTIFMGR_EVENT_ADD, _tr69Event_handler);
    IARM_Bus_RegisterEventHandler(IARM_BUS_TR69HOSTIFMGR_NAME, IARM_BUS_TR69HOSTIFMGR_EVENT_REMOVE, _tr69Event_handler);

    /* Register call for notify events */
    IARM_Bus_RegisterEvent(IARM_BUS_TR69Agent_EVENT_MAX);

//    IARM_Bus_RegisterEventHandler(IARM_BUS_TR69HOSTIFMGR_NAME, IARM_BUS_TR69HOSTIFMGR_EVENT_VALUECHANGED, _tr69Event_handler);

    return ret;
}

/* IARM Disconnect and Term Call */
IARM_Result_t IARM_TR69_Client_DisConnect()
{
    IARM_Result_t ret = IARM_RESULT_IPCCORE_FAIL;

    if(IARM_RESULT_SUCCESS != (ret = IARM_Bus_Disconnect()))
    {
        DEBUG_OUTPUT (
            dbglog(SVR_ERROR, DBG_ACCESS,"[%s:%s] Failed to IARM_Bus_Disconnect(), return value: %d \n", __FILE__, __FUNCTION__, ret);
        )
        return ret;
    }

    if(IARM_RESULT_SUCCESS != (ret = IARM_Bus_Term()))
    {
        DEBUG_OUTPUT (
            dbglog(SVR_ERROR, DBG_ACCESS,"[%s:%s] Failed to IARM_Bus_Term(), return value: %d \n", __FILE__, __FUNCTION__, ret);
        )
        return ret;
    }
    return ret;
}

//---------------------------------------------------------
// Initialize the DeviceStructure parameters after database
// --------------------------------------------------------
int initialize_DeviceStr_Params()
{
    int i = 0;
    int ret = OK;
    ParameterValue pValue;

    const char *devStrParamList[] =
    {   "Device.DeviceInfo.Manufacturer",
        "Device.DeviceInfo.ManufacturerOUI",
        "Device.DeviceInfo.ModelName",
        "Device.DeviceInfo.SerialNumber",
        "Device.DeviceInfo.HardwareVersion",
        "Device.DeviceInfo.SoftwareVersion"
        //"Device.DeviceInfo.AdditionalSoftwareVersion",
        //"Device.DeviceInfo.X_COMCAST-COM_STB_MAC",
        //FIRMWARE_IMAGENAME,
        //"Device.DeviceInfo.ProvisioningCode"
    };

//   printf("[%s:%s:%d] Entering..\n", __FILE__, __FUNCTION__, __LINE__);
    int sizeParam = sizeof(devStrParamList)/sizeof(char *);

    for ( i = 0; i < sizeParam; i++)
    {
        ret = get_ParamValues_tr69hostIf (devStrParamList[i], StringType, &pValue);

        if(NULL == pValue.out_cval) return -1;
        if (OK == ret && pValue.out_cval != NULL)
        {
            ret = updateParamValue(devStrParamList[i], StringType, &pValue);
            if(ret != OK)
            {
                DEBUG_OUTPUT (
                    dbglog(SVR_ERROR, DBG_ACCESS,"Failed to update database for devStrParamList[%d] : %s value[%s], returns updateParamValue() %d\n", i, devStrParamList[i], pValue.out_cval, ret);	)
            }
            if(pValue.out_cval)
            {
//            printf("[%s:%s:%d] freeing value %s\n", __FILE__, __FUNCTION__, __LINE__,pValue.out_cval);
                free(pValue.out_cval);
            }
        }
        else
        {
            DEBUG_OUTPUT (
                dbglog(SVR_ERROR, DBG_ACCESS,"Failed to get devStrParamList[%d] : %s value[%s] with return status as : %d\n", i, devStrParamList[i], pValue.out_cval, ret);	)
        }
    }
    return ret;
    // printf("[%s:%s:%d] Exiting..\n", __FILE__, __FUNCTION__, __LINE__);
}



//----------------------------------------------------------------------------
// Api to delete All the Instances of MultiObject and resetNextInstNumber to 0
// ---------------------------------------------------------------------------
void deleteTbleInstancesAndResetNextInstNumber()
{
    int ret = -1;
    ret = deleteAllInstancesOfMultiObjectAndResetNextInstNumber(NULL, DEV_ETH_INTERFACE, 0, 1);
    DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_PARAMETER, "%s(): Deleted [\'%s\'] table.\n", __FUNCTION__, DEV_ETH_INTERFACE);)
    ret =+ deleteAllInstancesOfMultiObjectAndResetNextInstNumber(NULL, DEV_IP_INTERFACE, 0, 1);
    DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_PARAMETER, "%s(): Deleted [\'%s\'] table.\n", __FUNCTION__, DEV_IP_INTERFACE);)
    ret =+ deleteAllInstancesOfMultiObjectAndResetNextInstNumber(NULL, DEV_IP_ACTIVEPORT, 0, 1);
    DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_PARAMETER, "%s(): Deleted [\'%s\'] table.\n", __FUNCTION__, DEV_IP_ACTIVEPORT);)
    ret =+ deleteAllInstancesOfMultiObjectAndResetNextInstNumber(NULL, DEVINFO_PROCESSOR, 0, 1);
    DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_PARAMETER, "%s(): Deleted [\'%s\'] table.\n", __FUNCTION__, DEVINFO_PROCESSOR);)
    ret =+ deleteAllInstancesOfMultiObjectAndResetNextInstNumber(NULL, DEVINFO_PROCESS, 0, 1);
    DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_PARAMETER, "%s(): Deleted [\'%s\'] table.\n", __FUNCTION__, DEVINFO_PROCESS);)
    ret =+ deleteAllInstancesOfMultiObjectAndResetNextInstNumber(NULL, DEV_MOCA_ASSOC, 0, 1);
    DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_PARAMETER, "%s(): Deleted [\'%s\'] table.\n", __FUNCTION__, DEV_MOCA_ASSOC );)
    ret =+ deleteAllInstancesOfMultiObjectAndResetNextInstNumber(NULL, DEV_MOCA_MESH, 0, 1);
    DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_PARAMETER, "%s(): Deleted [\'%s\'] table.\n", __FUNCTION__, DEV_MOCA_MESH );)
}

//---------------------------------------------------------
// generic Api for get HostIf parameters through IARM_TR69Bus
// --------------------------------------------------------
int get_ParamValues_tr69hostIf (const char *name, ParameterType type, ParameterValue *value)
{
    IARM_Result_t ret = IARM_RESULT_IPCCORE_FAIL;
//	g_printf("[%s:%s:%d] Entering..\n", __FILE__, __FUNCTION__, __LINE__);

    HOSTIF_MsgData_t param = {0};

    if(NULL == name)    {
        DEBUG_OUTPUT (dbglog(SVR_ERROR, DBG_ACCESS,"Failed due to Parameter name is NULL\n");)
        return ERR_INVALID_PARAMETER_NAME;
    }

    if(name)
    {
        /* Initialize hostIf get structure */
        strncpy(param.paramName, name, strlen(name)+1);
        param.reqType = HOSTIF_GET;
        //printf("[%s:%s:%d] Param: %s DataType: %d\n",  __FILE__, __FUNCTION__, __LINE__, name, type);
        switch (type) {
        case StringType:
        case DefStringType:
        case DateTimeType:
        case hexBinaryType:
        case DefHexBinaryType:
            param.paramtype = hostIf_StringType;
            break;
        case IntegerType:
        case UnsignedIntType:
        case DefUnsignedIntType:
        case UnsignedLongType:
        case DefUnsignedLongType:
            param.paramtype = hostIf_IntegerType;
        case BooleanType:
            param.paramtype = hostIf_BooleanType;

        default:
            break;
        }

        /* IARM get Call*/
#if 0
        ret = IARM_Bus_Call(IARM_BUS_TR69HOSTIFMGR_NAME,
                            IARM_BUS_TR69HOSTIFMGR_API_GetParams,
                            (void *)&param,
                            sizeof(HOSTIF_MsgData_t));

        if(ret != IARM_RESULT_SUCCESS) {
            DEBUG_OUTPUT( dbglog(SVR_ERROR, DBG_ACCESS,"[%s:%s:%d] Failed in IARM_Bus_Call(), with return value: %d\n", __FILE__, __FUNCTION__, __LINE__, ret); )
            return ERR_REQUEST_DENIED;
        }
        else
        {
            DEBUG_OUTPUT(
                dbglog(SVR_DEBUG, DBG_ACCESS,"[%s:%s:%d] The value of param: %s paramLen : %d\n", __FILE__, __FUNCTION__, __LINE__, name, param.paramLen); )
            //printf("[%s:%s:%d] The param \'%s\' value : %s paramLen : %d\n", __FILE__, __FUNCTION__, __LINE__, name, (char *)param.paramValue, param.paramLen);
        }
#else
	    g_printf("st8018 [%s:%s:%d] get hardcoding value!!\n", __FILE__, __FUNCTION__, __LINE__);
        //param.paramName
        if (strstr(param.paramName, "ManufacturerOUI") > 0) { 
            //"Device.DeviceInfo.Manufacturer",
            memcpy(param.paramValue, "6CCA08\0", 6);
            param.paramLen = 6;
        } else if (strstr(param.paramName, "Manufacturer") > 0 ) {
            //"Device.DeviceInfo.ManufacturerOUI",
            memcpy(param.paramValue, "Motorola\0", 8);
            param.paramLen = 8;
        } else if (strstr(param.paramName, "ModelName") > 0) { 
            //"Device.DeviceInfo.ModelName",
            memcpy(param.paramValue, "IP805\0", 5);
            param.paramLen = 5;
        } else if (strstr(param.paramName, "SerialNumber") > 0) { 
            //"Device.DeviceInfo.SerialNumber",
            struct ifreq s;
            int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);

            char tmp[12];
            strcpy(s.ifr_name, "eth1");
            if (0 == ioctl(fd, SIOCGIFHWADDR, &s)) {
                sprintf(tmp, "%02x%02x%02x%02x%02x%02x",
                        (unsigned char) s.ifr_addr.sa_data[0],
                        (unsigned char) s.ifr_addr.sa_data[1],
                        (unsigned char) s.ifr_addr.sa_data[2],
                        (unsigned char) s.ifr_addr.sa_data[3],
                        (unsigned char) s.ifr_addr.sa_data[4],
                        (unsigned char) s.ifr_addr.sa_data[5]);
            }

            memcpy(param.paramValue, tmp, 12);
            param.paramLen = 12;
        } else if (strstr(param.paramName, "HardwareVersion") > 0) { 
            //"Device.DeviceInfo.HardwareVersion",
            memcpy(param.paramValue, "V1.0\0", 4);
            param.paramLen = 4;
        } else if (strstr(param.paramName, "SoftwareVersion") > 0) { 
            //"Device.DeviceInfo.SoftwareVersion",
            //memcpy(param.paramValue, "v1.0.0\0", 6);
            char key[1024] = "";
            FILE *filePtr = NULL;
            filePtr = fopen ("/version.txt", "r");
            if (filePtr != NULL) {
                while (1)
                {
                    fscanf (filePtr, "%s", key);
                    char *keyValue = strstr(key, "VERSION=");
                    if (keyValue) {
                        memcpy(param.paramValue, key + 8, 7);
                        break;
                    }
                    if (feof(filePtr)) {
                        break;
                    }
                }
                fclose (filePtr);
            }
            else {
                memcpy(param.paramValue, "0.0.0.0\0", 7);
            }
            param.paramLen = 7;
        }
#endif

        int str_len = 0;

        switch(type)
        {
        case StringType:
        case DefStringType:
            /* get the value as string */
            str_len = strlen(param.paramValue);
            if(str_len)
            {
                value->out_cval = (char *)malloc(str_len+1);
                if (value != NULL)
                {
                    strncpy(value->out_cval, (char*)param.paramValue, str_len+1);
                }
                else
                {
                    DEBUG_OUTPUT (dbglog(SVR_ERROR, DBG_ACCESS,"%s: Failed to allocate memory for ParamValue\n", __FUNCTION__);)
                }
            }
            else {
                DEBUG_OUTPUT (dbglog(SVR_ERROR, DBG_ACCESS,"%s(): Failed: No value returned for Param: %s\n", __FUNCTION__, name);)
                value->out_cval = NULL;
            }
            break;
        case DateTimeType:
            if(strlen(param.paramValue))
            {
                s2dateTime(param.paramValue,&(value->in_timet));
            } else
                s2dateTime(UNKNOWN_TIME, &(value->in_timet));
            break;
        case IntegerType:
        case UnsignedIntType:
        case DefUnsignedIntType:
        case UnsignedLongType:
        case DefUnsignedLongType:
            value->out_int = get_int(param.paramValue);
            //	printf("[%s:%s:%d] ParamPath: [%s]; PramaValue: [%d] \n", __FILE__, __FUNCTION__, __LINE__, name,value->out_int, param.paramLen, str_len);
            break;
        case BooleanType:
            value->out_int = get_boolean(param.paramValue);
            //  printf("[%s:%s:%d] ParamPath: [%s]; PramaValue: [%d] \n", __FILE__, __FUNCTION__, __LINE__, name,value->out_int, param.paramLen, str_len);
            break;
        case hexBinaryType:
        case DefHexBinaryType:
            str_len = strlen(param.paramValue);
            if(str_len == 0)
            {
                value->out_hexBin = (char*)malloc(1);
                *(value->out_hexBin) = '\0';
            }
            else
            {
                value->in_hexBin = (char*)malloc(str_len+1);
                strncpy(value->out_hexBin, param.paramValue, str_len);
                *(value->out_hexBin + str_len) = '\0';
            }
            break;
        default:
            break;
        }
    }
    return OK;

}

bool get_boolean(const char *ptr)
{
    bool *ret = (bool *)ptr;
    return *ret;
}

int get_int(const char* ptr)
{
    int *ret = (int *)ptr;
    return *ret;
}

void put_boolean(char *ptr, bool val)
{
    bool *tmp = (bool *)ptr;
    *tmp = val;
}

void put_int(char *ptr, int val)
{
    int *tmp = (int *)ptr;
    *tmp = val;
}

//---------------------------------------------------------
// generic Api for set hostIf params through IARM_TR69Bus
// --------------------------------------------------------
int set_ParamValues_tr69hostIf (const char *name, ParameterType type, ParameterValue *value)
{
    IARM_Result_t ret = IARM_RESULT_IPCCORE_FAIL;
//    g_printf("[%s:%s:%d] Entering..\n", __FILE__, __FUNCTION__, __LINE__);
    HOSTIF_MsgData_t param = {0};

    if(NULL == name || value == NULL) {
        DEBUG_OUTPUT (dbglog(SVR_ERROR, DBG_ACCESS,"Failed due to Parameter name/value is NULL\n");)
        return ERR_REQUEST_DENIED;
    }

    /* Initialize hostIf Set structure */
    strncpy(param.paramName, name, strlen(name)+1);
    param.reqType = HOSTIF_SET;

    switch (type) {
    case StringType:
    case DefStringType:
    case DateTimeType:
        param.paramtype = hostIf_StringType;
        strncpy(param.paramValue, value->in_cval, strlen(value->in_cval));
    	DEBUG_OUTPUT(dbglog(SVR_DEBUG, DBG_ACCESS,"[%s:%s:%d] Param Name : %s ; value : %s\n", __FILE__, __FUNCTION__, __LINE__, name, (char *)value->in_cval); )
        break;
    case IntegerType:
    case UnsignedIntType:
        put_int(param.paramValue, value->in_uint);
        param.paramtype = hostIf_IntegerType;
    case BooleanType:
        put_boolean(param.paramValue,value->in_int);
        param.paramtype = hostIf_BooleanType;
        break;
    default:
        break;
    }

    ret = IARM_Bus_Call(IARM_BUS_TR69HOSTIFMGR_NAME,
                        IARM_BUS_TR69HOSTIFMGR_API_SetParams,
                        (void *)&param,
                        sizeof(param));
    if(ret != IARM_RESULT_SUCCESS) {
        DEBUG_OUTPUT(
            dbglog(SVR_ERROR, DBG_ACCESS,"[%s:%s:%d] Failed in IARM_Bus_Call(), with return value: %d\n", __FILE__, __FUNCTION__, __LINE__, ret); )
        return ERR_REQUEST_DENIED;
    }
    else
    {
        DEBUG_OUTPUT(dbglog(SVR_DEBUG, DBG_ACCESS,"[%s:%s:%d] Set Successful for value : %s\n", __FILE__, __FUNCTION__, __LINE__, (char *)param.paramValue); )
    }
//    g_printf("[%s:%s:%d] Entering..\n", __FILE__, __FUNCTION__, __LINE__);
    return OK;
}

static void  _tr69Event_handler(const char *owner, IARM_Bus_tr69HostIfMgr_EventId_t eventId, void *data, size_t len)
{
    IARM_Bus_tr69HostIfMgr_EventData_t *tr69EventData = (IARM_Bus_tr69HostIfMgr_EventData_t *)data;
    unsigned int instance;
    char paramPath[TR69HOSTIFMGR_MAX_PARAM_LEN] = {'\0'};
    int ret = OK;
    bool notify = false;
    bool notifyTmp = false;
    char value[TR69HOSTIFMGR_MAX_PARAM_LEN] = {'\0'};

//    printf("[%s:%s] EventId: %d paramPath: %s eventRecFlag: %d\n", __FILE__, __FUNCTION__, eventId, tr69EventData->paramName, eventRecFlag);

    if(false == eventRecFlag)
    {
        DEBUG_OUTPUT(dbglog(SVR_ERROR, DBG_ACCESS,"[%s:%s] Not recieve the event since the eventRecFlag : %d paramPath: %s\n", __FILE__, __FUNCTION__, eventRecFlag, paramPath); )
        return;
    }

    if (0 == strcmp(owner, IARM_BUS_TR69HOSTIFMGR_NAME))
    {
        strncpy(paramPath, tr69EventData->paramName, strlen(tr69EventData->paramName));
        switch (eventId)
        {
        case IARM_BUS_TR69HOSTIFMGR_EVENT_ADD:
            /* The HostSystem want's to add new instance for an tabular object
             * add NL <objectpath> NL
             * The instance number of the new created object is returned, or
             * if an error occurred a 0 is returned. */

            DEBUG_OUTPUT (	dbglog (SVR_INFO, DBG_HOST, "Received Add Event for Param Path = %s\n", tr69EventData->paramName);)
            //printf ("[%s:%s:%d] Received Add Event: paramPath = %s\n", __FILE__, __FUNCTION__, __LINE__, tr69EventData->paramName);
            if(0 == strncasecmp(tr69EventData->paramName, DEV_MOCA_ASSOC, strlen(DEV_MOCA_ASSOC)))
            {
                checkAndAddTblObjectInstances(DEV_MOCA_NUM_ENTRIES, DEV_MOCA_ASSOC, tr69EventData->paramName);
            }
            else if (0 == strncasecmp(tr69EventData->paramName, DEV_ETH_INTERFACE, strlen(DEV_ETH_INTERFACE)))
            {
                checkAndAddTblObjectInstances(DEV_ETH_NUM_ENTRIES, DEV_ETH_INTERFACE, tr69EventData->paramName);
            }
            else if (0 == strncasecmp(tr69EventData->paramName, DEV_IP_INTERFACE, strlen(DEV_IP_INTERFACE)))
            {
                checkAndAddTblObjectInstances(DEV_IP_NUM_ENTRIES, DEV_IP_INTERFACE, tr69EventData->paramName);
            }
            else if (0 == strncasecmp(tr69EventData->paramName, DEV_IP_ACTIVEPORT, strlen(DEV_IP_ACTIVEPORT)))
            {
                checkAndAddTblObjectInstances(DEV_IP_ACTIVEPORT_NUM_ENTRIES, DEV_IP_ACTIVEPORT, tr69EventData->paramName);
            }
            else if (0 == strncasecmp(tr69EventData->paramName, DEVINFO_PROCESS, strlen(DEVINFO_PROCESS)))
            {
                checkAndAddTblObjectInstances(DEVINFO_PROCESS_NUM_ENTRIES, DEVINFO_PROCESS, tr69EventData->paramName);
            }
            else if (0 == strncasecmp(tr69EventData->paramName, DEVINFO_PROCESSOR, strlen(DEVINFO_PROCESSOR)))
            {
                checkAndAddTblObjectInstances(DEVINFO_PROCESSOR_NUM_ENTRIES, DEVINFO_PROCESSOR, tr69EventData->paramName);
            }
            else if(0 == strncasecmp(tr69EventData->paramName, DEV_MOCA_MESH, strlen(DEV_MOCA_MESH)))
            {
                checkAndAddTblObjectInstances(DEV_MOCA_MESH_ENTRIES, DEV_MOCA_MESH, tr69EventData->paramName);
            }
            break;
        case IARM_BUS_TR69HOSTIFMGR_EVENT_REMOVE:
            DEBUG_OUTPUT (
                dbglog (SVR_DEBUG, DBG_HOST, "Received Remove event for param Path = %s\n", tr69EventData->paramName);
            )
            if(0 == strncasecmp(tr69EventData->paramName, DEV_MOCA_ASSOC, strlen(DEV_MOCA_ASSOC)))
            {
                checkAndDeleteTblObjectLastInstances(DEV_MOCA_NUM_ENTRIES, DEV_MOCA_ASSOC, tr69EventData->paramName, 1);
            }
            else if (0 == strncasecmp(tr69EventData->paramName, DEV_ETH_INTERFACE, strlen(DEV_ETH_INTERFACE)))
            {
                checkAndDeleteTblObjectLastInstances(DEV_ETH_NUM_ENTRIES, DEV_ETH_INTERFACE, tr69EventData->paramName, 1);
            }
            else if (0 == strncasecmp(tr69EventData->paramName, DEV_IP_INTERFACE, strlen(DEV_IP_INTERFACE)))
            {
                checkAndDeleteTblObjectLastInstances(DEV_IP_NUM_ENTRIES, DEV_IP_INTERFACE, tr69EventData->paramName, 1);
            }
            else if (0 == strncasecmp(tr69EventData->paramName, DEV_IP_ACTIVEPORT, strlen(DEV_IP_ACTIVEPORT)))
            {
                checkAndDeleteTblObjectLastInstances(DEV_IP_ACTIVEPORT_NUM_ENTRIES, DEV_IP_ACTIVEPORT, tr69EventData->paramName, 1);
            }
            else if (0 == strncasecmp(tr69EventData->paramName, DEVINFO_PROCESS, strlen(DEVINFO_PROCESS)))
            {
                checkAndDeleteTblObjectLastInstances(DEVINFO_PROCESS_NUM_ENTRIES, DEVINFO_PROCESS, tr69EventData->paramName, 1);
            }
            else if (0 == strncasecmp(tr69EventData->paramName, DEVINFO_PROCESSOR, strlen(DEVINFO_PROCESSOR)))
            {
                checkAndDeleteTblObjectLastInstances(DEVINFO_PROCESSOR_NUM_ENTRIES, DEVINFO_PROCESSOR, tr69EventData->paramName, 1);
            }
            else if(0 == strncasecmp(tr69EventData->paramName, DEV_MOCA_MESH, strlen(DEV_MOCA_MESH)))
            {
                checkAndDeleteTblObjectLastInstances(DEV_MOCA_MESH_ENTRIES, DEV_MOCA_MESH, tr69EventData->paramName, 1);
            }

            break;
        case IARM_BUS_TR69HOSTIFMGR_EVENT_VALUECHANGED:
            strncpy(value, tr69EventData->paramValue, strlen(tr69EventData->paramValue));
            DEBUG_OUTPUT (
                dbglog (SVR_INFO, DBG_HOST, "set: paramPath = %s, buf = %s\n", paramPath, value);
            )
            pthread_mutex_lock(&notifyParamLock);
            ret = setParameter2Host(paramPath, &notifyTmp, value);
            pthread_mutex_unlock(&notifyParamLock);
//					sendInt(ret);
            notify |= notifyTmp;
            DEBUG_OUTPUT ( dbglog (SVR_INFO, DBG_HOST, "set notify: %d\n", notify);
                         )

            //If parameter is UDPCONNECTIONREQUESTADDRESS then rebind the UDC CR handler/thread and STUN_client thread.
            if (!strcmp(paramPath, UDPCONNECTIONREQUESTADDRESS))
                rebindUdpCRSocket(value);
            break;
        default:
            break;
        }
    }
}


/* This function is responsible to call the RebootNow.sh script for XI3 devices
*  This will be called once the reboot rpc method is called from ACS.  */
void callRebootScript()
{
    DEBUG_OUTPUT ( dbglog (SVR_ERROR, DBG_ACCESS, "[%s] Executing Reboot command \'%s\'. \n",__FUNCTION__, REBOOT_SCR_PATH); )

    int ret = system(REBOOT_SCR_PATH);

    //printf("[%s:%s:%d]\n", __FILE__, __FUNCTION__, __LINE__);
    if (WEXITSTATUS(ret) != 0 )
    {
        DEBUG_OUTPUT ( dbglog (SVR_ERROR, DBG_ACCESS, "[%s] Failed in executing Reboot script from \'%s\'. \n",__FUNCTION__, REBOOT_SCR_PATH); )
    }
    //printf("[%s:%s:%d]\n", __FILE__, __FUNCTION__, __LINE__);
}

/* This function is responsible to call the RebootNow.sh script for XI3 devices
*  This will be called once the reboot rpc method is called from ACS.  */
bool callImageFlashScript()
{
    char ui8FlashScrPath[100] = {'\0'};
    bool ret = false;
    //printf("[%s:%s:%d]\n", __FILE__, __FUNCTION__, __LINE__);
    sprintf(ui8FlashScrPath, "%s %s/%s", "sh", dimclientConfStruct.ScriptPath, "tr69FWDnld.sh");
    //printf("[%s:%s:%d] ui8FlashScrPath : %s\n", __FILE__, __FUNCTION__, __LINE__, ui8FlashScrPath);
    ret = system(ui8FlashScrPath);
    if (WEXITSTATUS(ret) != 0 )
    {
        DEBUG_OUTPUT ( dbglog (SVR_ERROR, DBG_ACCESS, "[%s] Failed in executing Image Flashing script. \n",__FUNCTION__); )
    }
    else
    {
        DEBUG_OUTPUT ( dbglog (SVR_ERROR, DBG_ACCESS, "[%s] Successfully executing Flashing. \n",__FUNCTION__); )
        ret = true;
    }
    //printf("[%s:%s:%d]\n", __FILE__, __FUNCTION__, __LINE__);
    return ret;
}


/** Description: Creates all the instances of nested object under a given parent object
 *               xxx.{i}.yyy{i}.
 *  Input:   objTablePath - Parent Object path
 *           subObjTablePath - Nested object path
 *           subObjGetNumOfEntries - Function pointer to get number of entries
             for nested object for a given parent object instance
 *
 * \Return:  None
 *
 */
int checkAndAddTblObjectInstances(const char *objEntriesPath, const char *tableObjectPath, const char *paramPath)
{
    int ret = OK;
    int subObjDBCnt = 0, subObjCnt = 0;
    unsigned int instance;
    ParameterValue pValue;
    int count = 0;

    /*Get the number of entries for Associated Device.*/
    ret = get_ParamValues_tr69hostIf (objEntriesPath, IntegerType, &pValue);

    if (OK == ret)
    {
        subObjCnt = pValue.out_int;
    }

    /* get the all instances for Associated MoCA device */
    ret = countInstances( tableObjectPath, &count );
    if ( ret == OK ) {
        subObjDBCnt = count;
    }
    else
    {
        subObjDBCnt = 0;
    }

    //printf("[%s:%s()] %s Count: %d, subObjDBCnt: %d, subObjCnt :%d\n", __FILE__, __FUNCTION__,tableObjectPath, count, subObjDBCnt, subObjCnt);
    if (subObjCnt > subObjDBCnt)
    {
        pthread_mutex_lock(&notifyParamLock);
        ret = addObjectIntern(paramPath, &instance, RPC_COMMAND_IS_CALLED_INTERNAL, CALL_USER_DEFINED_CALLBACK);
        pthread_mutex_unlock(&notifyParamLock);

        if(ret != OK)
        {
            DEBUG_OUTPUT (dbglog (SVR_ERROR, DBG_HOST, "Failed to add instaces = %d for paramPath = %s\n", instance,paramPath); )
        }
        else
        {
            DEBUG_OUTPUT (dbglog (SVR_INFO, DBG_HOST, "Successfully added paramPath = %s with instaces = %d\n", paramPath, instance); )
        }
    }
    return ret;
}



/*
Description: Helper Function to parse the .xml file to list the features supported by XI-3 Box.
Input:  Profile type. Enum value
        0 for Device. profile
        1 for STBServices. profile
Output: OK on success.
        ERR_INTERNAL_ERROR on failure.
*/
int parseDataModel(EProfileTypes eType )
{
    xmlTextReaderPtr stReader = NULL;
    int iRet;
    const xmlChar *pxcTagName = NULL;
    char *pcAttName = NULL;
    char cPrevAttName[256] = {'\0'};

    stReader = xmlReaderForFile(XML_FILE_NAME,NULL,0);
    if(NULL == stReader)
    {

        DEBUG_OUTPUT
        (
            dbglog (SVR_ERROR, DBG_PARAMETER, "[%s:%s:%d] Opening .xml\n",__FILE__,__FUNCTION__,__LINE__);
        )

        return ERR_INTERNAL_ERROR;
    }


    DEBUG_OUTPUT
    (
        dbglog (SVR_INFO, DBG_PARAMETER, "[%s:%s:%d] Data Model File Name:%s \n",__FILE__,__FUNCTION__,__LINE__,XML_FILE_NAME);
    )

    stDataModel.pcFeatures = (char *)calloc(1,1);
    if(stDataModel.pcFeatures != NULL)
    {
        iRet = xmlTextReaderRead(stReader);
        while(iRet == 1)
        {
            pxcTagName = xmlTextReaderConstName(stReader);

            /*Reading the base tag's Attribute name from "data-model.xml" file to list the Features supported by the XI-3 Box */
            if(!strcasecmp ((char *)pxcTagName, "object"))
            {
                pcAttName = (char *)xmlTextReaderGetAttribute(stReader, (xmlChar *)"base");

                if(0 != strcmp(pcAttName,cPrevAttName))
                {
                    if(eDeviceType == eType && 0 == strncmp("Device.",pcAttName,7))
                    {
                        stDataModel.pcFeatures = (char *)realloc(stDataModel.pcFeatures,strlen(stDataModel.pcFeatures)+strlen(pcAttName)+2);
                        if(NULL != stDataModel.pcFeatures)
                        {
                            strcat(stDataModel.pcFeatures,pcAttName);
                            strcpy(cPrevAttName,pcAttName);
                            strcat(stDataModel.pcFeatures,",");
                        }
                        else
                        {
                            DEBUG_OUTPUT
                            (
                                dbglog (SVR_ERROR, DBG_PARAMETER, "[%s:%s:%d] malloc failed\n",__FILE__,__FUNCTION__,__LINE__);
                            )

                            //Free xml file pointer and free pointer allocated to memory.
                            free(stDataModel.pcFeatures);
                            xmlFree(pcAttName);
                            xmlFreeTextReader(stReader);

                            return ERR_INTERNAL_ERROR;
                        }
                    }

                    if(eSTBServicesType == eType && 0 == strncmp("Device.Services.",pcAttName,16))
                    {
                        stDataModel.pcFeatures = (char *)realloc(stDataModel.pcFeatures,strlen(stDataModel.pcFeatures)+strlen(pcAttName)+2);
                        if(NULL != stDataModel.pcFeatures)
                        {
                            strcat(stDataModel.pcFeatures,pcAttName);
                            strcpy(cPrevAttName,pcAttName);
                            strcat(stDataModel.pcFeatures,",");
                        }
                        else
                        {
                            DEBUG_OUTPUT
                            (
                                dbglog (SVR_ERROR, DBG_PARAMETER, "[%s:%s:%d] malloc failed\n",__FILE__,__FUNCTION__,__LINE__);
                            )

                            //Free xml file pointer and free pointer allocated to memory.
                            free(stDataModel.pcFeatures);
                            xmlFree(pcAttName);
                            xmlFreeTextReader(stReader);

                            return ERR_INTERNAL_ERROR;
                        }
                    }
                }
                xmlFree(pcAttName);
            }
            iRet = xmlTextReaderRead(stReader);
        }
        xmlFreeTextReader(stReader);
    }
    else
    {
        DEBUG_OUTPUT
        (
            dbglog (SVR_ERROR, DBG_PARAMETER, "[%s:%s:%d] malloc failed\n",__FILE__,__FUNCTION__,__LINE__);
        )
        //Free xml file Pointer.
        xmlFreeTextReader(stReader);

        return ERR_INTERNAL_ERROR;
    }

    return OK;
}


int get_Supported_Data_Model_Fields(const char *name,ESupportedDataModelMembers eDataModelMem)
{
    int iDataModelInstance = 0;

    iDataModelInstance = getIdx(name,"Device.DeviceInfo.SupportedDataModel");

    DEBUG_OUTPUT
    (
        dbglog (SVR_INFO, DBG_PARAMETER, "[%s:%s:%d] DataModelInstance No:%d \n",__FILE__,__FUNCTION__,__LINE__,iDataModelInstance);
    )

    if(iDataModelInstance <= SUPPORTED_DATA_MODEL_NUMBER_OF_ENTRIES)
    {
        if(1 == iDataModelInstance)
        {
            switch(eDataModelMem)
            {
            case eURL:
                strcpy(stDataModel.cURL,URL_TR_181);
                break;
            case eURN:
                strcpy(stDataModel.cURN,URN_TR_181);
                break;
            case eFeatures:
                if(OK != parseDataModel(eDeviceType))
                {
                    return ERR_INTERNAL_ERROR;
                }
                break;
            default:
                break;
            }
            DEBUG_OUTPUT
            (
                dbglog (SVR_INFO, DBG_PARAMETER, "[%s:%s:%d] TR-181 \n",__FILE__,__FUNCTION__,__LINE__);
            )
        }
        else
        {
            switch(eDataModelMem)
            {
            case eURL:
                strcpy(stDataModel.cURL,URL_TR_135);
                break;
            case eURN:
                strcpy(stDataModel.cURN,URN_TR_135);
                break;
            case eFeatures:
                if(OK != parseDataModel(eSTBServicesType))
                {
                    return ERR_INTERNAL_ERROR;
                }
                break;
            default:
                break;
            }

            DEBUG_OUTPUT
            (
                dbglog (SVR_INFO, DBG_PARAMETER, "[%s:%s:%d] TR-135 \n",__FILE__,__FUNCTION__,__LINE__);
            )
        }
    }
    else
    {
        memset(&stDataModel, 0,sizeof(stDataModel));
        DEBUG_OUTPUT
        (
            dbglog (SVR_ERROR, DBG_PARAMETER, "[%s:%s:%d] Entry Not Found In SupportedDataModel Profile Table For Instance: %d \n",__FILE__,__FUNCTION__,__LINE__,iDataModelInstance);
        )
    }

    return OK;
}


/****************************************************************************************************************************************************/
// Device.Deviceinfo.SupportedDataModel Profile. Getters:
/****************************************************************************************************************************************************/
int get_Device_DeviceInfo_SupportedDataModelNumberOfEntries(const char *name, ParameterType type, ParameterValue *value)
{
    UINT_GET value = (unsigned int)SUPPORTED_DATA_MODEL_NUMBER_OF_ENTRIES;

    return OK;
}
int get_Device_DeviceInfo_SupportedDataModel_URL(const char *name, ParameterType type, ParameterValue *value)
{
    if(OK != get_Supported_Data_Model_Fields(name,eURL))
    {
        return ERR_INTERNAL_ERROR;
    }

    value->out_cval = (char *)emallocTemp(strlen(stDataModel.cURL)+1, 1);
    if ( value->out_cval != NULL)
    {
        strcpy(value->out_cval, stDataModel.cURL);
    }
    else
    {
        DEBUG_OUTPUT
        (
            dbglog (SVR_ERROR, DBG_PARAMETER, "[%s:%s:%d] Malloc failed\n",__FILE__,__FUNCTION__,__LINE__);
        )
        return ERR_INTERNAL_ERROR;
    }

    DEBUG_OUTPUT
    (
        dbglog (SVR_INFO, DBG_PARAMETER, "[%s:%s:%d] URL: %s \n",__FILE__,__FUNCTION__,__LINE__,value->out_cval);
    )

    return OK;
}

int get_Device_DeviceInfo_SupportedDataModel_URN(const char *name, ParameterType type, ParameterValue *value)
{
    if(OK != get_Supported_Data_Model_Fields(name,eURN))
    {
        return ERR_INTERNAL_ERROR;
    }

    value->out_cval = (char *)emallocTemp(strlen(stDataModel.cURN)+1, 1);
    if ( value->out_cval != NULL)
    {
        strcpy(value->out_cval, stDataModel.cURN);
    }
    else
    {
        DEBUG_OUTPUT
        (
            dbglog (SVR_ERROR, DBG_ACCESS, "[%s:%s:%d] Malloc failed\n",__FILE__,__FUNCTION__,__LINE__);
        )
        return ERR_INTERNAL_ERROR;
    }

    DEBUG_OUTPUT
    (
        dbglog (SVR_INFO, DBG_PARAMETER, "[%s:%s:%d] URN: %s \n",__FILE__,__FUNCTION__,__LINE__,value->out_cval);
    )

    return OK;
}


int get_Device_DeviceInfo_SupportedDataModel_Features(const char *name, ParameterType type, ParameterValue *value)
{
    if(OK != get_Supported_Data_Model_Fields(name,eFeatures))
    {
        return ERR_INTERNAL_ERROR;
    }

    if(NULL != stDataModel.pcFeatures)
    {
        value->out_cval = (char *)emallocTemp(strlen(stDataModel.pcFeatures)+1, 1);
        if ( value->out_cval != NULL)
        {
            strcpy(value->out_cval, stDataModel.pcFeatures);
        }
        else
        {
            DEBUG_OUTPUT
            (
                dbglog (SVR_ERROR, DBG_ACCESS, "[%s:%s:%d] Malloc failed\n",__FILE__,__FUNCTION__,__LINE__);
            )
            return ERR_INTERNAL_ERROR;
        }

        DEBUG_OUTPUT
        (
            dbglog (SVR_INFO, DBG_PARAMETER, "[%s:%s:%d] Features: %s \n",__FILE__,__FUNCTION__,__LINE__,value->out_cval);
        )

        free(stDataModel.pcFeatures);
    }

    return OK;
}

int createAndCheckBootStarpEventFile(char *bootstrapFile)
{
    int ret = -1;


    if( bootstrapEventFlag == false)
    {
        if(access( bootstrapFile, F_OK ) != -1 )
        {
            dbglog (SVR_INFO, DBG_MAIN, "[%s:%s:%d] Bootstrap file exists: %s with flag %d \n",__FILE__,__FUNCTION__,__LINE__, bootstrapFile, bootstrapEventFlag);
            bootstrapEventFlag = true;
        }
        else
        {
            dbglog (SVR_INFO, DBG_MAIN,"[%s:%s:%d] Bootstrap file doesn't exists: %s (%d) \n",__FILE__,__FUNCTION__,__LINE__, bootstrapFile, bootstrapEventFlag);
            ret = creat( bootstrapFile, 0666 );
            if ( ret >= 0 ) {
                ret = 0;
                bootstrapEventFlag = true;
            }
            else {
                ret = -1;
            }
            return ret;
        }
    }
}

int deleteBootStarpEventFile( char *bootstrapFile )
{
    int ret = OK;

    ret = remove( bootstrapFile );
    if ( ret != OK )
        ret = ERR_DIM_MARKER_OP;
    DEBUG_OUTPUT (
        dbglog (SVR_INFO, DBG_EVENTCODE, "%s: %d\n", __FUNCTION__,ret );
    )
    return ret;
}


void notifyEvent_hostIf(IARM_Bus_tr69Agent_EventId_t event_type, int event_data)
{

    IARM_Bus_TR69Agent_EventData_t eventData;

    eventData.value = event_data;

    IARM_Bus_BroadcastEvent(IARM_TR69_CLIENT, (IARM_EventId_t) event_type, (void *)&eventData, sizeof(eventData));
}



#ifdef AUTH_SERVICE

int init_BootStrapAuthService_ACSSettingConfig()
{
    char *response = NULL;
    EBuildType build_type = 0;
    char *urlParamName = NULL;
    char partnerId[50] = {'\0'};
    int ret = 0;
    const char *devId = NULL, *pId = NULL;
    char *deviceIdinfo = NULL;

    char *bsPropUrl = "http://127.0.0.1:50050/authService/getBootstrapProperty";
    char *partnerIdUrl = "http://127.0.0.1:50050/authService/getDeviceId";


    if(readAuthSer_ACSConfig("", &deviceIdinfo, partnerIdUrl))
    {
        json_t *root = NULL;
        json_error_t error;

        if(!deviceIdinfo) {
            DEBUG_OUTPUT ( dbglog (SVR_ERROR, DBG_MAIN, "[%s:%s] Failed in getDeviceId(), return as \'%s\' from \'%s\'\n",__FILE__,__FUNCTION__, deviceIdinfo, partnerIdUrl); )
        }
        else
        {
            DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_MAIN, "[%s:%s] getDeviceId() returns as \'%s\'. \n",__FILE__,__FUNCTION__, deviceIdinfo); )
            root = json_loads(deviceIdinfo,0,&error);
            if(root)
            {
                json_unpack(root, "{s:s, s:s}", "deviceId", &devId, "partnerId", &pId);
                DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_MAIN, "[%s:%s] deviceId : %s partnerId : %s\n", __FILE__, __FUNCTION__, devId, partnerId); )
                json_decref(root);
            }

            free(deviceIdinfo);
            deviceIdinfo = NULL;
        }
    }

    /*Read build type from /etc/device.properties */
    if(BUILD_TYPE_INVALID != (build_type = read_BuildType()))
    {
        urlParamName = ((build_type == BUILD_TYPE_PROD) || (build_type == BUILD_TYPE_VBN)) ? "tr069:acs:url:prod" : "tr069:acs:url:dev";

        if(readAuthSer_ACSConfig(urlParamName, &response, bsPropUrl))
        {
            if(!response)
            {
                DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_MAIN, "[%s:%s] Failed to get ACS Url, returns as \'%s\' from \'%s\'\n", __FILE__, __FUNCTION__, response, bsPropUrl); )
            }
            else
            {
                memset(&dimclientConfStruct.url, 0, sizeof(dimclientConfStruct.url));
                DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_MAIN, "[%s:%s] Url : %s\n", __FILE__, __FUNCTION__, response); )
                //memcpy(dimclientConfStruct.url, response, strlen(response) +1);
                memcpy(dimclientConfStruct.url, "https://acs-cpe-devel.eng.rr.com:7547/edge/tr69", strlen("https://acs-cpe-devel.eng.rr.com:7547/edge/tr69")+1 );

                if(response) {
                    free(response);
                    response = NULL;
                }

                if (strlen(dimclientConfStruct.url) > 0 && isBootstrap())
                {
                    ret = setUniversalParamValueInternal(NULL, MANAGEMENT_SERVER_URL, 1, dimclientConfStruct.url);
                    if (ret != OK)
                    {
                        DEBUG_OUTPUT (
                            dbglog (SVR_ERROR, DBG_DIAGNOSTIC,
                                    "%s()->setUniversalParamValueInternal(%s) has returned error = %i\n", __FUNCTION__, MANAGEMENT_SERVER_URL,ret);
                        )
                    }
                }
            }
        }
    }

    if(readAuthSer_ACSConfig("tr069:authschme:one-way-ssl:server-side:acs:certdata", &response, bsPropUrl))
    {
        if(!response) {
            DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_MAIN,"[%s:%s] Failed to get the Cert file path, retrun as \'%s\' from \'%s\' \n", __FILE__, __FUNCTION__, response, bsPropUrl);)
        }
        else
        {
            memset(&dimclientConfStruct.SharedKeyPathName, 0, sizeof(dimclientConfStruct.SharedKeyPathName));
            DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_MAIN,"[%s:%s] Cert file : %s\n", __FILE__, __FUNCTION__, response);)
            memcpy(dimclientConfStruct.SslCertPath_Acs, response, strlen(response) +1);
            free(response);
            response = NULL;
        }
    }

    if(readAuthSer_ACSConfig("tr069:authschme:one-way-ssl:server-side:cpe:key", &response, bsPropUrl))
    {
        if(!response) {
            DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_MAIN, "[%s:%s] Failed to get Shared key file path, returns as \'%s\' from \'%s\'\n", __FILE__, __FUNCTION__, response, bsPropUrl);)
        }
        else
        {
            memset(&dimclientConfStruct.SslCertPath_Acs, 0, sizeof(dimclientConfStruct.SslCertPath_Acs));
            DEBUG_OUTPUT ( dbglog (SVR_DEBUG, DBG_MAIN, "[%s:%s] Shared key file : %s\n", __FILE__, __FUNCTION__, response);)
            memcpy(dimclientConfStruct.SharedKeyPathName, response, strlen(response) +1);
            free(response);
            response = NULL;
        }
    }
    return OK;
}

size_t static write_callback_func(void *buffer,  size_t size, size_t nmemb, void *userp)
{
    char **response_ptr =  (char**)userp;
    *response_ptr = strndup((const char *) buffer, (size_t)(size *nmemb));
}


bool readAuthSer_ACSConfig(const char *postthis, char **resp, char *url)
{
    CURL *curl = NULL;
    CURLcode res;
    char *response = NULL;

    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postthis);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long)strlen(postthis));
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback_func);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
        curl_easy_perform(curl);
        curl_easy_cleanup(curl);
    }
    else
    {
        return false;
    }
    *resp = response;
    return true;
}

EBuildType read_BuildType()
{
    char line[MAX_CONF_FILE_SRT_LEN];
    int ret;
    FILE* fp;
    char * point1;
    char paramName[50] = {'\0'};
    int len = 0;
    int res;
    int bType = BUILD_TYPE_INVALID;
    char buildType[257] = {'\0'};

    fp = fopen(dimclientConfStruct.devicePropertiesFile,"r");
    if (!fp)
    {
        DEBUG_OUTPUT (
            dbglog (SVR_ERROR, DBG_ACCESS, "%s: fopen() error. Check if path set correctly by dimclient.conf file.\n", __FUNCTION__);
        )
        return ERR_INTERNAL_ERROR;
    }

    while (fgets(line, MAX_CONF_FILE_SRT_LEN, fp))
    {
        memset(paramName, 0, sizeof(paramName) );
        point1 = strchr(line,0x3D); // symbol '='
        if (!point1) {
            continue;
        }
        len = strlen(line) - strlen(point1);
        if (len < 1) {
            continue;
        }
        strncpy(paramName,line,len);

        if (0 == strcmp(paramName,"BUILD_TYPE"))
        {
            res = sscanf(line, "BUILD_TYPE=%s", buildType);

            if (strlen(buildType))
            {
                if (0 == strcasecmp(buildType, "vbn"))
                {
                    bType = BUILD_TYPE_VBN;
                }
                else if (0 == strcasecmp(buildType, "prod"))
                {
                    bType = BUILD_TYPE_PROD;
                }
                else
                    bType = BUILD_TYPE_DEV;
            }
            break;
        }
    }
    fclose(fp);
    return bType;
}

#endif /* #define AUTH_SERVICE */
