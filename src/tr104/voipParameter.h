/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef voipParameter_H
#define voipParameter_H

#include "dimark_globals.h"

/* if VOIP_DEVICE is defined then VoIP device (tr104) is enabled.
 * But functionality VoIP isn't used at present. So, it isn't recommended to define the VOIP_DEVICE definition.
 * VOIP_DEVICE enabling requires the implementation of the func freeVoIPAllocatedMemory() -
 * - freeing memory after using VoIP Parameters and devices.
 * */
#ifdef VOIP_DEVICE

int initVoIP( void );
int freeVoIPAllocatedMemory(void);
int initVdMaxLine( const char *, ParameterType, ParameterValue * );
int initVdMaxProfile( const char *, ParameterType, ParameterValue * );
int setVdLineEnable( const char *, ParameterType, ParameterValue * );
int getVdLineEnable( const char *, const ParameterType , ParameterValue *);
int setVdLineDirectoryNumber( const char *, ParameterType, ParameterValue * );
int getVdLineDirectoryNumber( const char *, const ParameterType , ParameterValue *);
int getVdLinePacketsSent( const char *, ParameterType, ParameterValue * );
#endif /* VOIP_DEVICE */

#endif /* voipParameter_H */
