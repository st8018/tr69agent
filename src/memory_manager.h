/***************************************************************************
 *    Copyright (C) 2004-2012 Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef memory_manager_H
#define memory_manager_H

#include <sys/types.h>

#define MEM_TYPE_SESSION			1 // lives temporarily during session is active until will not be freed.
#define MEM_TYPE_TEMP				2 // lives temporarily until will not be freed.
#define MEM_TYPE_PARAM_ENTRY		3 // memory for ParameterEntry list. MUST Live all time. Free at program exit.
#define MEM_TYPE_SHORT				4 // For local operation. Lives short time until will not be freed. You must free this mem after usage.

#define MEM_TYPE_EVENT_LIST			30 // Memory for EventList. Lives until will not be freed. You must free this mem in freeEventList()
#define MEM_TYPE_DIAGNOSTIC_LIST	31 // Memory for DiagnosticsList. Lives until will not be freed. You must free this mem at exit from program.
#define MEM_TYPE_CALLBACK_LISTS		32 // Memory for preSessionCbList. Lives until will not be freed. You must free this mem in cleanCallbackLists()
#define MEM_TYPE_DU_LIST			33 // Memory for deploymentUnitList. Lives until will not be freed. You must free this mem at exit from program.
#define MEM_TYPE_FILETRANSFER_LIST	34 // Memory for transferList. Lives until will not be freed. You must free this mem at exit from program.
#define MEM_TYPE_SI_LIST			35 // Memory for scheduleInformList. Lives until will not be freed. You must free this mem at exit from program in clearScheduleInformList().
#define MEM_TYPE_OPSION_LIST		36 // Memory for Opsion List. Lives until will not be freed. You must free this mem at exit from program.
#define MEM_TYPE_PARAM_FAULT_LIST	37 // Memory for parameterFaultList. Lives until will not be freed. You must free this mem in the clearFault().

#define MEM_TYPE_GSOAP_MEM			38 // Memory for gSOAP

static pid_t getTid();

static void *emalloc( unsigned int );
static void efree(void ** );

void *emallocTypedMem( unsigned int , unsigned int , int );
void efreeAllTypedMem( int );
void efreeTypedMemByType(unsigned int , int );
void efreeAllTypedMemForCurrentThread( int );
void efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY( int );
void efreeTypedMemByTypeForCurrentThread(unsigned int , int );
void efreeTypedMemByPointer( void ** , int );

void *emallocTemp( unsigned int, int );
void *emallocSession( unsigned int, int );

int getAllocatedChunksCount();
long getTypedMemAllocCount();
long getTypedMemAllocBytes();
long getMaxTypedMemAllocBytes();

long getTypedMemInfoByType(unsigned int , long * );
long getTypedMemInfoByTypeForThread(unsigned int , long * );
long getTypedMemInfoForThread( long * );

void printMemoryInfo();
void unlock_typedMemList_mutex();

void* dcalloc(unsigned int size);
void dfree(void * mem);

#endif /* memory_manager_H */
