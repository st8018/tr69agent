/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"

#ifdef HAVE_SCHEDULE_INFORM

#include "list.h"
#include "eventcode.h"
#include "si_transfer.h"
#include "host/si_entryStore.h"

pthread_mutex_t siList_usage_mutex = PTHREAD_MUTEX_INITIALIZER;

typedef enum scheduleInformState
{
	ScheduleInform_NotStarted = 1,
	ScheduleInform_InProgress = 2,
	ScheduleInform_Completed = 3
} ScheduleInformState;

typedef struct ScheduleInformEntry
{
	/* All Data we got from the ACS */
	char commandKey[CMD_KEY_STR_LEN + 1];
	/* StartTime is calculated from now() + delayedSeconds */
	time_t startTime;
	/*Index of files */
	int indexOfFile;
	ScheduleInformState status;
} ScheduleInformEntry;

List scheduleInformList;
/* Index of files, which contents an information about schedule inform entry. */
int indexOfFile = 0;

int isDelayedScheduleInform(void);
int writeScheduleInformEntry(ScheduleInformEntry *);
int deleteScheduleInformEntry(ScheduleInformEntry *);
int readScheduleInformEntry(void);
int readScheduleInformEntryList(char *name, char *data);

/********* Function for work with scheduleInformList *******/
static void initScheduleInformList();
static void addScheduleInform(ScheduleInformEntry *, int );
static ListEntry * iterateScheduleInformList(ListEntry * entry);
static void addEntryToScheduleInformList(ScheduleInformEntry * se);
static ListEntry * getFirstEntryFromScheduleInformList();
static ListEntry * iterateRemoveFromScheduleInformList(ListEntry * entry);

/* Checks the existence of a delayed schedule inform.
 * if there is an entry in the schedule inform, then return true
 * else false. */
int isDelayedScheduleInform(void)
{
	return (getFirstEntryFromScheduleInformList() != NULL);
}

int writeScheduleInformEntry(ScheduleInformEntry * se)
{
	int ret = OK;
	char name[SI_MAX_NAME_SIZE];
	char data[SI_MAX_INFO_SIZE];

	sprintf(name, "%ld_%d", se->startTime, se->indexOfFile);

	DEBUG_OUTPUT(
			dbglog (SVR_INFO, DBG_SCHEDULE, "Save ScheduleInformEntry Status: %d\n", se->status);
	)

	ret = sprintf(data, "%s|%ld|%d|%d\n", se->commandKey, se->startTime, se->indexOfFile, se->status);

	if (ret < 0)
	{
		return ERR_INTERNAL_ERROR;
	}

	ret = storeScheduleInformEntryInfo(name, data);		/* create new file */

	return ret;
}

int deleteScheduleInformEntry(ScheduleInformEntry * se)
{
	int ret = OK;
	char name[SI_MAX_NAME_SIZE];

	sprintf(name, "%ld_%d", se->startTime, se->indexOfFile);

	ret = deleteScheduleInformEntryInfo(name);

	return ret;
}

int readScheduleInformEntryList(char *name, char *data)
{
	int ret = OK;
	ScheduleInformEntry *se = NULL;
	char *bufptr;
	bufptr = data;
	se = (ScheduleInformEntry *) emallocTypedMem( sizeof(ScheduleInformEntry), MEM_TYPE_SI_LIST, 0 );

	if (se == NULL)
	{
		return ERR_INTERNAL_ERROR;
	}

	strnCopy(se->commandKey, strsep(&bufptr, "|"), CMD_KEY_STR_LEN);
	se->startTime = a2l(strsep(&bufptr, "|"), NULL);
	se->indexOfFile = a2i(strsep(&bufptr, "|"), NULL);
	se->status = a2i(strsep(&bufptr, "|"), NULL);

	addScheduleInform(se, 0);
	return ret;
}



/* Iterates through the list of delayed schedule informs.
 * When find one, it executes the informs the ACS.
 */
unsigned int handleDelayedScheduleInform()
{
	unsigned int cnt = 0;
	ListEntry *entry = NULL;
	ScheduleInformEntry *se = NULL;

	while ((entry = iterateScheduleInformList(entry)))
	{
		se = (ScheduleInformEntry *) entry->data;
		if(!se)
			continue;
		time_t actTime = time(NULL);
		if (se->status == ScheduleInform_NotStarted && se->startTime <= actTime)
		{
			cnt++;
			se->status = ScheduleInform_InProgress;

			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_SCHEDULE, "handleDelayedScheduleInform\n");
			)

			se->status = ScheduleInform_Completed;
			continue;
		}
	}
	return cnt;
}

int handleDelayedScheduleInformEvents()
{
	int ret = SOAP_OK;
	ListEntry *entry = NULL;
	ScheduleInformEntry *se = NULL;

	entry = iterateScheduleInformList(entry);
	while (entry)
	{
		se = (ScheduleInformEntry *) entry->data;
		if(!se)
		{
			entry = iterateScheduleInformList(entry);
			continue;
		}
		if (se->status == ScheduleInform_Completed)
		{
			/* It's used in the InformMessage with the SCHEDULED event */
			addEventCodeMultiple(EV_M_SCHEDULED, se->commandKey);
		}
		entry = iterateScheduleInformList(entry);
	}
	return ret;
}

/* For all schedule informs with status == ScheduleInform_Completed a Inform Message is sent to the ACS
 * and the entry is cleared from the schedule list. In case of an error the status is not changed. */
int clearDelayedScheduleInform(struct soap *server)
{
	int ret = SOAP_OK;
	ListEntry *entry = NULL;
	ScheduleInformEntry *se = NULL;

	entry = iterateScheduleInformList(entry);
	while (entry)
	{
		se = (ScheduleInformEntry *) entry->data;
		if(!se)
		{
			entry = iterateScheduleInformList(entry);
			continue;
		}
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_SCHEDULE, "ClearDelay Schedule Inform Status: %d\n", se->status);
		)

		if (se->status == ScheduleInform_Completed)
		{
			deleteScheduleInformEntry(se);

			efreeTypedMemByPointer((void**)&se, 0);
			entry = iterateRemoveFromScheduleInformList(entry);
		}
		else
		{
			entry = iterateScheduleInformList(entry);
		}
	}
	return ret;
}

/* Initialize the scheduleInformList and load the scheduleInformList from file if there is one available */
int initScheduleInform(void)
{
	initScheduleInformList();
	return readScheduleInformEntry();
}

int readScheduleInformEntry(void)
{
	int ret = OK;
	ret = readScheduleInformEntryInfos((newScheduleInformEntryInfo *) &readScheduleInformEntryList);

	return ret;
}

static void addScheduleInform(ScheduleInformEntry * se, int isNeededPersistentStoring)
{
	addEntryToScheduleInformList(se);
	if (isNeededPersistentStoring)
		writeScheduleInformEntry(se);
}

int execScheduleInform(struct soap *soap, xsd__unsignedInt DelaySeconds, xsd__string CommandKey)
{
	int returnCode = OK;
	ScheduleInformEntry *se;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SCHEDULE, "Delayed Schedule Inform\n");
	)

	se = (ScheduleInformEntry *) emallocTypedMem(sizeof(ScheduleInformEntry), MEM_TYPE_SI_LIST, 0);

	if (se == NULL)
	{
		return ERR_RESOURCE_EXCEEDED;
	}

	strnCopy(se->commandKey, CommandKey, (sizeof(se->commandKey) - 1));
	se->startTime = time(NULL) + DelaySeconds;
	se->status = ScheduleInform_NotStarted;
	indexOfFile = indexOfFile % 10000;
	se->indexOfFile = indexOfFile++;
	addScheduleInform(se, 1);

	return returnCode;
}

void clearScheduleInformList()
{
	scheduleInformList.firstEntry = scheduleInformList.lastEntry = NULL;
	efreeTypedMemByType(MEM_TYPE_SI_LIST, 0);
}

/********* Function for work with scheduleInformList *******/
/* Initialization Schedule Inform List */
static void initScheduleInformList()
{
	pthread_mutex_lock(&siList_usage_mutex);
	initList(&scheduleInformList);
	pthread_mutex_unlock(&siList_usage_mutex);
}

/* Iterate  Schedule Inform List */
static ListEntry * iterateScheduleInformList(ListEntry * entry)
{
	pthread_mutex_lock(&siList_usage_mutex);
	entry = iterateList(&scheduleInformList, entry);
	pthread_mutex_unlock(&siList_usage_mutex);
	return entry;
}

/* Add new Entry to Schedule Inform List */
static void addEntryToScheduleInformList(ScheduleInformEntry * se)
{
	pthread_mutex_lock(&siList_usage_mutex);
	addEntry(&scheduleInformList, se, MEM_TYPE_SI_LIST);
	pthread_mutex_unlock(&siList_usage_mutex);
}

/* Get first entry from Schedule Inform */
static ListEntry * getFirstEntryFromScheduleInformList()
{
	ListEntry * entry = NULL;
	pthread_mutex_lock(&siList_usage_mutex);
	entry = getFirstEntry(&scheduleInformList);
	pthread_mutex_unlock(&siList_usage_mutex);
	return entry;
}

/* Remove entry from Schedule Inform */
static ListEntry * iterateRemoveFromScheduleInformList(ListEntry * entry)
{
	pthread_mutex_lock(&siList_usage_mutex);
	entry = iterateRemove (&scheduleInformList, entry);
	pthread_mutex_unlock(&siList_usage_mutex);
	return entry;
}

#endif /* HAVE_SCHEDULE_INFORM */
