/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"
#include "callback.h"
#include "utils.h"
#include "list.h"
#include "paramaccess.h"

List initParametersBeforeCbList;
List initParametersDoneCbList;
List preSessionCbList;
List postSessionCbList;
List cleanUpCbList;

List postSPVCbList = {NULL, NULL};

static int preSessionCbOnce(void);
static int preSessionCbCont(void);

/* Initialization list of callback */
void initCallbackList(void)
{
	initList(&initParametersBeforeCbList);
	initList(&initParametersDoneCbList);
	initList(&preSessionCbList);
	initList(&postSessionCbList);
	initList(&cleanUpCbList);

#ifndef EXTERNAL_CALLBACK_ADDITION
	/* Example of preSession Callbacks, these can also be used for post session
	 * or cleanup
	 * See the return codes for one shot or continuous callbacks*/
	addCallback(&preSessionCbOnce, &preSessionCbList);
	addCallback(&preSessionCbCont, &preSessionCbList);
#else
	EXTERNAL_CALLBACK_ADDITION;
#endif
}

void addCallback(Callback cb, List* list)
{
	addEntry(list, cb, MEM_TYPE_CALLBACK_LISTS);
}

void addSPVCallback(SPVCallback spvCB, const char * name, ParameterType type, ParameterValue * value)
{
	pSPVCallbackData spvCallbackData;
	spvCallbackData = emallocTemp(sizeof(SPVCallbackData), 0);
	spvCallbackData->paramName = strnDupByMemType( spvCallbackData->paramName, name, strlen(name), MEM_TYPE_TEMP);

	if (getParameterValueCopy(value, &spvCallbackData->paramValue, type, MEM_TYPE_TEMP) != OK)
	{
		memset((void *) &spvCallbackData->paramValue, 0, sizeof(ParameterValue));
	}

	spvCallbackData->cbFunction = spvCB;

	addEntry(&postSPVCbList, (void*)spvCallbackData, MEM_TYPE_TEMP);
}

/* Executes the callback from the given list
 * the callbacks are called from the head of the list to the end
 * If a callback returns CALLBACK_STOP = 0 the callback is removed from the list
 * if it returns CALLBACK_REPEAT = 1 the callback stays in the list and is called
 * again executeCallback is called.
 * 
 */
int executeCallback(List* list)
{
	int ret = OK;
	ListEntry *entry = NULL;
	Callback cb = NULL;

	entry = iterateList(list, NULL);
	while (entry != NULL)
	{
		if (!entry->data)
		{
			entry = iterateRemove(list, entry);
			continue;
		}
		cb = (Callback) entry->data;
		ret = cb();
		if (ret == CALLBACK_STOP)
		{
			entry = iterateRemove(list, entry);
		}
		else
		{
			entry = iterateList(list, entry);
		}
	}
	return OK;
}

int executeSPVCallback(List* list)
{
	int ret = OK;
	ListEntry *entry = NULL;
	SPVCallback spvcb = NULL;
	pSPVCallbackData spvCallbackData = NULL;

	entry = iterateList(list, NULL);
	while (entry != NULL)
	{
		if ((spvCallbackData = (pSPVCallbackData)entry->data) != NULL)
		{
			spvcb = (SPVCallback) spvCallbackData->cbFunction;
			ret = spvcb( spvCallbackData->paramName, &spvCallbackData->paramValue);
		}
		entry = iterateRemove(list, entry);
	}
	return OK;
}

void cleanCallbackLists()
{
	initParametersDoneCbList.firstEntry = initParametersDoneCbList.lastEntry = NULL;
	preSessionCbList.firstEntry = preSessionCbList.lastEntry = NULL;
	postSessionCbList.firstEntry = postSessionCbList.lastEntry = NULL;
	cleanUpCbList.firstEntry = cleanUpCbList.lastEntry = NULL;
	efreeTypedMemByType(MEM_TYPE_CALLBACK_LISTS, 0);
}

void cleanCallbackList(List* list)
{
	void * point = NULL;
	while (( point = freeFirstEntryOfList(list)) !=  NULL )
	{
		;
	}
}

/* Example of a callback function only called once */
static int preSessionCbOnce(void)
{
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_CALLBACK, "PreSession Callback only once\n");
	)

	return CALLBACK_STOP;
}

/* Example of a callback function called every time the callback list is
 * processed
 */
static int preSessionCbCont(void)
{
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_CALLBACK, "PreSession Callback continuous\n");
	)

	return CALLBACK_REPEAT;
}
