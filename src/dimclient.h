/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef dimclient_H
#define dimclient_H

#include "httpda.h"

#define DELAY_BEFORE_NEXT_RPC  10000 //delay for successful socket closing (if needed) before sending of next RPC

int authorizeClient(struct soap *, struct http_da_info *);
enum xsd__boolean *soap_in_xsd__HoldRequestBoolean(struct soap *, const char *, enum xsd__boolean *, const char *);
int soap_out_xsd__HoldRequestBoolean(struct soap *, const char *, int, const enum xsd__boolean *, const char *);
void soap_default_xsd__HoldRequestBoolean(struct soap *, enum xsd__boolean *);
void killAllThreads();
void doneAllSoapInstances();
void setCWMPVersionToDefault();

#endif /* dimclient_H */
