/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef eventcode_H
#define eventcode_H

#include "utils.h" 

typedef int *( newEvent)(char *);

int loadEventCode(void);
int getEventCodeList(struct ArrayOfEventStruct *);
void freeEventList(bool );
int addEventCodeMultiple(const char *, const char *);
int addEventCodeSingle(const char *);
int findEventCode(const char *);

#endif /* eventcode_H */
