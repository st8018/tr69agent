/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef vouchers_H
#define vouchers_H

int storeVouchers( struct ArrayOfVouchers * );

#endif /* vouchers_H */
