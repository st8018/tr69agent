/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

/** Example implementation for parameter storage.
 * 
 * All parameter data is stored in the file system.
 * 
 */

#include <dirent.h>
#include "parameterStore.h"
#include "dimark_globals.h"
#include "debug.h"
#include "unistd.h"
#include "storage.h"
#include "DBhandler.h"

extern	char*	DEFAULT_PARAMETER_FILE;
extern	char*	PERSISTENT_DATA_DIR;
extern	char*	PERSISTENT_PARAMETER_DIR;

static int loadParamFile (char*, char *, newParam *);

#ifndef WITH_USE_SQLITE_DB
static int reloadParameters( newParam *);
static int removeAllParameterFromDir( const char * );
#endif /* WITH_USE_SQLITE_DB */

/** Load the initial parameter file ( normally tmp.param )
 * and call callbackF for every line read.
 */
int
loadInitialParameterFile( newParam *callbackF )
{
	int ret;
	ret = loadParamFile(DEFAULT_PARAMETER_FILE, NULL, callbackF);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "loadInitialParameterFile()->loadParamFile(): has returned error %d\n", ret);
		)
		return ret;
	}
	ret = reloadParameters(callbackF);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "loadInitialParameterFile()->reloadParameters(): has returned error %d\n", ret);
		)
		return ret;
	}
	return OK;
}


static char bufTmp[MAX_PARAM_VALUE_SIZE + MAX_PATH_NAME_SIZE + MAX_ACCESS_LIST_SIZE + 1];

static int
loadParamFile (char *filename, char *name, newParam* callbackF)
{
	int ret;
	FILE *file;
	if (!filename)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "loadParamFile(): function parameter 'filename' cannot be NULL.\n");
		)
		return ERR_INTERNAL_ERROR;
	}
	if (!callbackF)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "loadParamFile(): function parameter 'callbackF' cannot be NULL.\n");
		)
		return ERR_INTERNAL_ERROR;
	}
	file = fopen (filename, "r");
	if (file == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "loadParamFile: file not found %s\n", filename);
		)

		return ERR_INTERNAL_ERROR;
	}

	memset(bufTmp, 0, sizeof bufTmp);

	ret = OK;
	while (ret == OK && fgets(bufTmp, sizeof(bufTmp), file) != NULL)
	{
		if ( bufTmp[0] == '#' || bufTmp[0] == ' ' || bufTmp[0] == '\n' || bufTmp[0] == '\0' )
			continue;
		bufTmp[strlen (bufTmp) - 1] = '\0';	/* remove trailing EOL  */
		ret = callbackF (name, bufTmp);
	}
	fclose (file);
	return ret;
}


#ifndef WITH_USE_SQLITE_DB

/** Update one parameter, which is in the initial parameter file
 * but has be updated by the ACS or the hostsystem.
 * Only data which can be changed is stored.
 */
int
updateParameter( const char *name, const char *data)
{
	int ret = OK;
	int fd;
	char buf[MAX_PATH_NAME_SIZE];

	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "updateParameter(): function parameter 'name' cannot be NULL.\n");
		)
		return ERR_INTERNAL_ERROR;
	}
	strncpy (buf, PERSISTENT_PARAMETER_DIR, sizeof(buf)-1);
	strncat (buf, name, sizeof(buf) - strlen(buf) - 1 );
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "updateParameter: %s\n", buf);
	)
	if ((fd = open (buf, O_RDWR | O_CREAT | O_TRUNC, FILE_MASK)) < 0)
		return ERR_RESOURCE_EXCEEDED;
	ret = write( fd, data, strlen(data));
	close (fd);
	return ret;
}

/** Check the existence of a parameter data and/or metadata file
 * Return
 * 	0 = no file exists
 *  1 = data file exists
 *  2 = meta file exists
 *  3 = both files exists
 */
unsigned int
checkParameter( const char *name )
{
	char buf[MAX_PATH_NAME_SIZE];
	int ret = NO_PARAMETER_FILE_EXISTS;

	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "checkParameter(): function parameter 'name' cannot be NULL.\n");
		)
		return ERR_INTERNAL_ERROR;
	}
	strncpy (buf, PERSISTENT_DATA_DIR, sizeof(buf)-1 );
	strncat (buf, name, sizeof(buf) - strlen(buf) - 1 );
	if ( access( buf, F_OK ) == OK )
		ret |= PARAMETER_DATA_FILE_EXISTS;
	memset( buf, 0, sizeof(buf));
	strncpy (buf, PERSISTENT_PARAMETER_DIR, sizeof(buf)-1);
	strncat (buf, name, sizeof(buf) - strlen(buf) - 1 );
	if ( access( buf, F_OK ) == OK )
		ret |= PARAMETER_META_FILE_EXISTS;
	return ret;
}


static char getbuf[MAX_PARAM_VALUE_SIZE + 1];

/** Store the metadata of parameter in name 
 * The metadata is given in a character string
 */
int
storeParameter( const char *name, const char *data)
{
	int ret = OK;
	int fd;
	char buf[MAX_PATH_NAME_SIZE + 1];
	char *p1 = getbuf, *p2 = getbuf;
	int n = 0, n1 = 1, n2 = 1;

	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "storeParameter(): function parameter 'name' cannot be NULL.\n");
		)
		return ERR_INTERNAL_ERROR;
	}
	strncpy (buf, PERSISTENT_PARAMETER_DIR, MAX_PATH_NAME_SIZE);
	strncat (buf, name, MAX_PATH_NAME_SIZE - strlen(buf) );

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "storeParameter: %s\n", buf);
	)

	if ((fd = open (buf, O_RDONLY | O_CREAT, FILE_MASK)) < 0)
		return ERR_RESOURCE_EXCEEDED;

	memset(getbuf, 0, sizeof getbuf);
	if ((n = read(fd, getbuf, sizeof(getbuf))) < 0)
		return ERR_RESOURCE_EXCEEDED;
	close (fd);

	if ((fd = open (buf, O_WRONLY | O_TRUNC, FILE_MASK)) < 0)
		return ERR_RESOURCE_EXCEEDED;
	if( n == 0 )
	{
		ret = write( fd, data, strlen(data));
	}
	else
	{
		while( *p2++ != '\n' )
			n1++;
		if( p2 == &getbuf[n] )	*p2 = 0;
		if( *p1 == *data )
		{
			ret = write( fd, data, strlen(data));
			if( *p2 )	{
				p1 = p2;
				while( *p1++ != '\n' )
					n2++;
				ret = write( fd, p2, n2 );
			}
		}	else
		{
			ret = write( fd, p1, n1 );
			ret = write( fd, data, strlen(data));
		}
	}
	if ( ret > 0 )
		ret = OK;
	else
		ret = ERR_RESOURCE_EXCEEDED;
	close (fd);

	return ret;
}

/** remove a named parameter from the persistent storage
 * remove the data and the metadata file
 * 
 */
int
removeParameter( const char *name )
{
	char buf[MAX_PATH_NAME_SIZE + 1];
	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "removeParameter(): function parameter 'name' cannot be NULL.\n");
		)
		return ERR_INTERNAL_ERROR;
	}
	// remove the parameter
	strncpy (buf, PERSISTENT_PARAMETER_DIR, MAX_PATH_NAME_SIZE);
	strncat (buf, name, MAX_PATH_NAME_SIZE - strlen(buf));
	remove (buf);
	// remove the data
	memset(buf, 0, MAX_PATH_NAME_SIZE);
	strncpy (buf, PERSISTENT_DATA_DIR, MAX_PATH_NAME_SIZE);
	strncat (buf, name, MAX_PATH_NAME_SIZE - strlen(buf));
	remove (buf);
	/* ignore all errors, because we can't handle them
	 */
	return OK;
}

/** Remove all data and metadata files.
 * Used by factory reset.
 */
int
removeAllParameters( void )
{
	removeAllParameterFromDir(PERSISTENT_PARAMETER_DIR);
	removeAllParameterFromDir(PERSISTENT_DATA_DIR);
	return OK;
}

/** Store the data value of a parameter
 */
int
storeParamValue( const char *name, ParameterType type , ParameterValue *value )
{
	int ret = OK;
	int fd;
	unsigned int len;
	char buf[MAX_PATH_NAME_SIZE];

	// Ignore calls without data
	if ( value == NULL )
		return ret;
	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "storeParamValue(): function parameter 'name' cannot be NULL.\n");
		)
		return ERR_WRITE_PARAMFILE;
	}
	strncpy (buf, PERSISTENT_DATA_DIR, sizeof(buf)-1 );
	strncat (buf, name, sizeof(buf) - strlen(buf) - 1 );

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "storeParamValue(): %s\n", buf);
	)
	if ((fd = open (buf, O_RDWR | O_CREAT, FILE_MASK)) < 0) {

		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "storeParamValue(): %s open\n", buf);
		)

		return ERR_WRITE_PARAMFILE;
	}
	switch (type)
	{
	case DefIntegerType:
	case IntegerType:
	case DefBooleanType:
	case BooleanType:
		ret = write(fd, &value->in_int, sizeof(int));
		break;
	case DefUnsignedIntType:
	case UnsignedIntType:
		ret = write(fd, &value->in_uint, sizeof(unsigned int));
		break;
	case DefStringType:
	case StringType:
	case DefBase64Type:
	case Base64Type:
		if ( value->in_cval != NULL )
			len = strlen(value->in_cval);
		else
			len = 0;
		if ( len > 0 )
			len++;  // add one for EOS
		ret = write(fd, &len, sizeof(unsigned int));
		if ( len > 0 && ret > 0 )
			ret = write(fd, value->in_cval, len);
		break;
	case DefDateTimeType:
	case DateTimeType:
	{
		char val[42];
		memset(val, 0, sizeof(val));
		sprintf(val, "%ld|%ld", value->in_timet.tv_sec, value->in_timet.tv_usec);
		/* write how str */
		len = strlen(val);
		if ( len > 0 )
			len++;  // add one for EOS
		ret = write(fd, &len, sizeof(unsigned int));
		if ( len > 0 && ret > 0 )
			ret = write(fd, val, len);
		//ret = write(fd, &value->in_timet, sizeof(struct timeval));
		break;
	}
	case DefUnsignedLongType:
	case UnsignedLongType:
		ret = write(fd, &value->in_ulong, sizeof(unsigned long));
		break;
	case DefHexBinaryType:
	case hexBinaryType:
		if ( value->in_hexBin != NULL )
			len = strlen(value->in_hexBin);
		else
			len = 0;
		if ( len > 0 )
			len++;  // add one for EOS
		ret = write(fd, &len, sizeof(unsigned int));
		if ( len > 0 && ret > 0 )
			ret = write(fd, value->in_hexBin, len);
		break;
	default:
		break;
	}		
	if ( ret >= 0 )
		ret = OK;
	else {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "WriteParameterFile: %s write: %d\n", buf, ret);
		)

		ret = ERR_WRITE_PARAMFILE;
	}
	close (fd);

	return ret;
}

int
updateParamValue( const char *name, ParameterType type , ParameterValue *value )
{
	return storeParamValue( name, type , value );
}

/** Get the data value of the parameter given in name
 */
int
retrieveParamValue( const char *name, ParameterType type, ParameterValue *value )
{
	int ret = OK;
	int fd;
	unsigned int len;
	char buf[MAX_PATH_NAME_SIZE];
	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "retrieveParamValue(): function parameter 'name' cannot be NULL.\n");
		)
		return ERR_READ_PARAMFILE;
	}
	strncpy (buf, PERSISTENT_DATA_DIR, sizeof(buf)-1 );
	strncat (buf, name, sizeof(buf) - strlen(buf) - 1 );
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "retrieveParamValue(): %s\n", buf);
	)

	if ((fd = open (buf, O_RDONLY, FILE_MASK)) < 0) {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "retrieveParamValue(): %s not found\n", buf);
		)

		return ERR_READ_PARAMFILE;
	}
	switch (type) {
	case DefIntegerType:
	case IntegerType:
	case DefBooleanType:
	case BooleanType:
		if(value != NULL)
			ret = read (fd, &value->out_int, sizeof(int));
		break;
	case DefUnsignedIntType:
	case UnsignedIntType:
		if(value != NULL)
			ret = read (fd, &value->out_uint, sizeof(unsigned int));
		break;
	case DefStringType:
	case StringType:
	case DefBase64Type:
	case Base64Type:
		if(value != NULL)
		{
			len = 0;
			ret = read (fd, &len, sizeof(unsigned int));
			if ( len == 0 ) {
				// malloc an empty string instead of NULL
				value->out_cval = (char *)emallocTemp(1, 1);
			} else {
				value->out_cval = (char *)emallocTemp(len, 2);
				if ( value->out_cval != NULL)
					ret = read (fd, value->out_cval, len);
			}
		}
		break;
	case DefDateTimeType:
	case DateTimeType:
		if(value != NULL)
		{
			char val[42];
			memset(val, 0, sizeof(val));
			len = 0;
			ret = read (fd, &len, sizeof(unsigned int));
			if(len == 0)
			{
				value->out_timet.tv_sec = 0;
				value->out_timet.tv_usec = 0;
			}
			else
			{
				ret = read (fd, val, len);
				sscanf(val, "%ld|%ld" ,&value->out_timet.tv_sec, &value->out_timet.tv_usec);
			}
			//ret = read (fd, &value->out_timet, sizeof(struct timeval));
		}
		break;
	case DefUnsignedLongType:
	case UnsignedLongType:
		if(value != NULL)
			ret = read (fd, &value->out_ulong, sizeof(unsigned long));
		break;
	case DefHexBinaryType:
	case hexBinaryType:
		if(value != NULL)
		{
			len = 0;
			ret = read (fd, &len, sizeof(unsigned int));
			if (len == 0) {
				// malloc an empty string instead of NULL
				value->out_hexBin = (char *)emallocTemp(1, 1);
			} else {
				value->out_hexBin = (char *)emallocTemp(len, 2);
				if ( value->out_hexBin != NULL)
					ret = read (fd, value->out_hexBin, len);
			}
		}
		break;
	default:
		break;
	}
	if ( ret > 0 )
		ret = OK;
	else {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "ReadParameterFile: %s write error: %d\n", buf, ret);
		)

		ret = ERR_READ_PARAMFILE;
	}
	close (fd);
	return ret;
}

static int
removeAllParameterFromDir( const char *dirname )
{
	char buf[MAX_PATH_NAME_SIZE];
	char *bufPtr;
	struct dirent *entry;
	DIR *dir;
	if (!dirname)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "removeAllParameterFromDir(): function parameter 'dirname' cannot be NULL.\n");
		)
		return ERR_INTERNAL_ERROR;
	}
	dir = opendir (dirname);
	if (dir == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "removeAllParameterFromDir(): directory not found %s\n", dirname);
		)

		return ERR_INTERNAL_ERROR;
	}

	strcpy( buf, dirname );
	bufPtr = (buf + strlen(dirname));

	while ((entry = readdir (dir)) != NULL)
	{
		if (strcmp (entry->d_name, ".") == 0)
			continue;
		if (strcmp (entry->d_name, "..") == 0)
			continue;
		*bufPtr = '\0';
		strcat ( buf, entry->d_name);
		remove( buf ); 
	}
	closedir (dir);
	return OK;
}

#ifdef USE_OLD_DIR_SCANNING
static int
reloadParameters( newParam *callbackF )
{
	char buf[MAX_PATH_NAME_SIZE];
	struct dirent *entry;
	int ret = OK;
	DIR *dir;
	int nfiles;
	bool readLoop = true;

	nfiles = 0;
	while( readLoop ) {
		readLoop = false;
		dir = opendir (PERSISTENT_PARAMETER_DIR);
		if (dir == NULL)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_PARAMETER, "reloadParameters: directory not found %s\n", PERSISTENT_PARAMETER_DIR);
			)

			return ERR_INTERNAL_ERROR;
		}
		while ((entry = readdir (dir)) != NULL)
		{
			if (strcmp (entry->d_name, ".") == 0)
				continue;
			if (strcmp (entry->d_name, "..") == 0)
				continue;

			strncpy (buf, PERSISTENT_PARAMETER_DIR, sizeof(buf)-1 );
			strncat (buf, entry->d_name, sizeof(buf) - strlen(buf) - 1 );

			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_PARAMETER, "reloadParameters: %s\n", buf);
			)

			ret = loadParamFile (buf, entry->d_name, callbackF);
			// If one parent is not found we don't leave the readLoop
			if ( ret == INFO_PARENT_NOT_FOUND )
				readLoop = true;
		}
		closedir (dir);
	}
	return OK;
}

#else /* USE_OLD_DIR_SCANNING */


#ifdef _GNU_SOURCE
#define SORT_FUNC versionsort
#else
#define SORT_FUNC alphasort
#endif

/* Sort list of files. Firstly: alphasorted objects, then alphasorted parameters
 */
static int fileSort(const struct dirent **e1, const struct dirent **e2)
{
	char c1, c2;

	c1 = (*e1)->d_name[strlen((*e1)->d_name)-1];
	c2 = (*e2)->d_name[strlen((*e2)->d_name)-1];
	if ((c1 == '.' && c2 == '.') || (c1 != '.' && c2 != '.'))
		return SORT_FUNC (e1, e2);
	else
	{
		if (c1 == '.')
			return -1;
		else
			return 1;
	}
}

/* to not select names "."  and  "..".
 * And check whether the pathName contains a DOT inside. If not - it is wrong pathname - ignore it.
 * */
static int notSelectDOTnames(const struct dirent * d)
{
	int len;
	if (!d)
		return 0;

	if (d->d_name[0] == '.' || !strcmp(d->d_name, ".."))
		return 0;

	if ( !strchr(d->d_name,'.') )
		return 0;

	return 1;
}

static int
reloadParameters( newParam *callbackF )
{
	char buf[MAX_PATH_NAME_SIZE];
	int ret;
    struct dirent **namelist;
    int n, m;

    n = scandir(PERSISTENT_PARAMETER_DIR, &namelist, notSelectDOTnames, fileSort);
    if (n < 0)
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "reloadParameters()->scandir(): has returned error %d. (selectObjectsOnly)\n", n);
		)
    else
    {
    	ret = OK;
        for(m=0; m<n; m++)
        {
        	if (ret == OK)
        	{
        		strncpy (buf, PERSISTENT_PARAMETER_DIR, sizeof(buf)-1 );
        		strncat (buf, namelist[m]->d_name, sizeof(buf) - strlen(buf) - 1 );

        		DEBUG_OUTPUT (
        				dbglog (SVR_INFO, DBG_PARAMETER, "reloadParameters(): \"%s\"\n", buf);
        		)

        		ret += loadParamFile (buf, namelist[m]->d_name, callbackF);
        		if ( ret )
        		{
        			DEBUG_OUTPUT (
        					dbglog (SVR_ERROR, DBG_PARAMETER, "reloadParameters(): buf = \"%s\", ret = %d\n", buf, ret);
        			)
        		}
        	}
        	free(namelist[m]);
        }
        free((void*)namelist);
    }
	return ret;
}
#endif /*else USE_OLD_DIR_SCANNING*/

#endif /* !WITH_USE_SQLITE_DB */
