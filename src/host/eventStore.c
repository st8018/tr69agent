/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

/**
 * All event data is stored in the file system. Also the boot recognition is
 * implemented as a file in the file system.
 */

#include "eventStore.h"
#include "dimark_globals.h"
#include "debug.h"
#include "unistd.h"
#include "storage.h"

extern	char*	PERSISTENT_FILE;
extern	char*	EVENT_STORAGE_FILE;

pthread_mutex_t eventStore_lock = PTHREAD_MUTEX_INITIALIZER;

static int createEmptyFile( const char * );

/** Deletes all entries in the event data storage
 *  ignore a negative return code from remove call, this can happen if
 *  the event store does not exist.
 */
int
clearEventStorage( void )
{
	int ret = OK;
	pthread_mutex_lock(&eventStore_lock);
	ret = remove(EVENT_STORAGE_FILE);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_EVENTCODE, "ClearEventStorage: %d ignored\n", ret );
	)
	pthread_mutex_unlock(&eventStore_lock);
	return ret;
}

/** Insert the event data in the char * at the end of the storage
 * \param data  the complete event as NULL terminated string
 */
int
insertEvent( const char *data )
{
	int ret = OK;
	FILE *pf;

	//	CLI24
	// open the eventstorage or create a new one,  if not existing, in append mode
	pthread_mutex_lock(&eventStore_lock);
	pf = fopen( EVENT_STORAGE_FILE, "a+" );
	if ( pf == NULL )
	{
		pthread_mutex_unlock(&eventStore_lock);
		return ERR_DIM_EVENT_WRITE;
	}
	ret = fprintf( pf, "%s\n", data );
	ret = fclose( pf );
	if ( ret != OK )
		ret = ERR_DIM_EVENT_WRITE;
	DEBUG_OUTPUT ( 
			dbglog (SVR_INFO, DBG_EVENTCODE, "Save Event: %s : %d\n", data, ret );
	)
	pthread_mutex_unlock(&eventStore_lock);
	return ret;	
}

/** Get all events from the storage and calls newEvent() to process it in dimclient
 * A trailing EOL is replaced by a EOS char.
 */
int
readEvents( newEvent *func )
{
	int ret = OK;
	char buf[101]; // should be large enough to hold the eventCode and the CommandKey

	FILE *pf;

	pthread_mutex_lock(&eventStore_lock);

	pf = fopen( EVENT_STORAGE_FILE, "r" );
	if ( pf == NULL )
	{
		pthread_mutex_unlock(&eventStore_lock);
		return ERR_DIM_EVENT_READ;
	}
	while (	fgets( buf, 100, pf ) != NULL ) {
		buf[ strlen( buf ) -1 ]	= '\0';	// remove trailing EOL 
		DEBUG_OUTPUT ( 
				dbglog (SVR_INFO, DBG_EVENTCODE, "Read Event: %s\n", buf );
		)
		func(buf);
	}
	ret = fclose( pf );

	pthread_mutex_unlock(&eventStore_lock);

	if ( ret != OK )
		ret = ERR_DIM_EVENT_READ;

	return ret;
}

/** Creates a file in the RAM file system.
 */
int
createBootstrapMarker( void )
{
	int ret = OK;

	ret = createEmptyFile( PERSISTENT_FILE );
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_EVENTCODE, "CreateBootStrapMarker: %d\n", ret );
	)
	return ret;
}

/*
 * delete Bootstrap Marker.
 * return OK or ERR_DIM_MARKER_OP
 */
int
deleteBootstrapMarker( void )
{
	int ret = OK;

	ret = remove( PERSISTENT_FILE );
	if ( ret != OK )
		ret = ERR_DIM_MARKER_OP;
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_EVENTCODE, "DeleteBootStrapMarker: %d\n", ret );
	)
	return ret;
}

/** Returns true if bootstrapMarker is found
 *  else false
 */
int
isBootstrapMarker( void )
{
	int ret = OK;

	ret = access( PERSISTENT_FILE, F_OK );
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_EVENTCODE, "IsBootStrapMarker: %d\n", (ret == OK) );
	)
	return (ret == 0);
}

static int
createEmptyFile( const char *path )
{
	int ret;

	ret = creat( path, 0666 );
	if ( ret >= 0 )
		ret = OK; 
	else
		ret = ERR_DIM_MARKER_OP;
	return ret;
}
