/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef diagParameter_H
#define diagParameter_H

#include "dimark_globals.h"
#include "parameter.h"
#include "paramaccess.h"

#ifdef HAVE_DIAGNOSTICS

#ifdef TR_181_DEVICE
#define		DownloadDiagnostics_DiagnosticsState			"Device.IP.Diagnostics.DownloadDiagnostics.DiagnosticsState"
#define		DownloadDiagnostics_Interface					"Device.IP.Diagnostics.DownloadDiagnostics.Interface"
#define		DownloadDiagnostics_DownloadURL					"Device.IP.Diagnostics.DownloadDiagnostics.DownloadURL"
#define		DownloadDiagnostics_DownloadTransports			"Device.IP.Diagnostics.DownloadDiagnostics.DownloadTransports"
#define		DownloadDiagnostics_DSCP						"Device.IP.Diagnostics.DownloadDiagnostics.DSCP"
#define		DownloadDiagnostics_EthernetPriority			"Device.IP.Diagnostics.DownloadDiagnostics.EthernetPriority"
#define		DownloadDiagnostics_ROMTime						"Device.IP.Diagnostics.DownloadDiagnostics.ROMTime"
#define		DownloadDiagnostics_BOMTime						"Device.IP.Diagnostics.DownloadDiagnostics.BOMTime"
#define		DownloadDiagnostics_EOMTime						"Device.IP.Diagnostics.DownloadDiagnostics.EOMTime"
#define		DownloadDiagnostics_TestBytesReceived			"Device.IP.Diagnostics.DownloadDiagnostics.TestBytesReceived"
#define		DownloadDiagnostics_TotalBytesReceived			"Device.IP.Diagnostics.DownloadDiagnostics.TotalBytesReceived"
#define		DownloadDiagnostics_TCPOpenRequestTime			"Device.IP.Diagnostics.DownloadDiagnostics.TCPOpenRequestTime"
#define		DownloadDiagnostics_TCPOpenResponseTime			"Device.IP.Diagnostics.DownloadDiagnostics.TCPOpenResponseTime"

#define		UploadDiagnostics_DiagnosticsState				"Device.IP.Diagnostics.UploadDiagnostics.DiagnosticsState"
#define		UploadDiagnostics_Interface						"Device.IP.Diagnostics.UploadDiagnostics.Interface"
#define		UploadDiagnostics_UploadURL						"Device.IP.Diagnostics.UploadDiagnostics.UploadURL"
#define		UploadDiagnostics_UploadTransports				"Device.IP.Diagnostics.UploadDiagnostics.UploadTransports"
#define		UploadDiagnostics_DSCP							"Device.IP.Diagnostics.UploadDiagnostics.DSCP"
#define		UploadDiagnostics_EthernetPriority				"Device.IP.Diagnostics.UploadDiagnostics.EthernetPriority"
#define		UploadDiagnostics_TestFileLength				"Device.IP.Diagnostics.UploadDiagnostics.TestFileLength"
#define		UploadDiagnostics_ROMTime						"Device.IP.Diagnostics.UploadDiagnostics.ROMTime"
#define		UploadDiagnostics_BOMTime						"Device.IP.Diagnostics.UploadDiagnostics.BOMTime"
#define		UploadDiagnostics_EOMTime						"Device.IP.Diagnostics.UploadDiagnostics.EOMTime"
#define		UploadDiagnostics_TotalBytesSent				"Device.IP.Diagnostics.UploadDiagnostics.TotalBytesSent"
#define		UploadDiagnostics_TCPOpenRequestTime			"Device.IP.Diagnostics.UploadDiagnostics.TCPOpenRequestTime"
#define		UploadDiagnostics_TCPOpenResponseTime			"Device.IP.Diagnostics.UploadDiagnostics.TCPOpenResponseTime"

#define		UDPEchoConfig_Enable							"Device.IP.Diagnostics.UDPEchoConfig.Enable"
#define		UDPEchoConfig_Interface							"Device.IP.Diagnostics.UDPEchoConfig.Interface"
#define		UDPEchoConfig_SourceIPAddress					"Device.IP.Diagnostics.UDPEchoConfig.SourceIPAddress"
#define		UDPEchoConfig_UDPPort							"Device.IP.Diagnostics.UDPEchoConfig.UDPPort"
#define		UDPEchoConfig_EchoPlusEnabled					"Device.IP.Diagnostics.UDPEchoConfig.EchoPlusEnabled"
#define		UDPEchoConfig_EchoPlusSupported					"Device.IP.Diagnostics.UDPEchoConfig.EchoPlusSupported"
#define		UDPEchoConfig_PacketsReceived					"Device.IP.Diagnostics.UDPEchoConfig.PacketsReceived"
#define		UDPEchoConfig_PacketsResponded					"Device.IP.Diagnostics.UDPEchoConfig.PacketsResponded"
#define		UDPEchoConfig_BytesReceived						"Device.IP.Diagnostics.UDPEchoConfig.BytesReceived"
#define		UDPEchoConfig_BytesResponded					"Device.IP.Diagnostics.UDPEchoConfig.BytesResponded"
#define		UDPEchoConfig_TimeFirstPacketReceived			"Device.IP.Diagnostics.UDPEchoConfig.TimeFirstPacketReceived"
#define		UDPEchoConfig_TimeLastPacketReceived			"Device.IP.Diagnostics.UDPEchoConfig.TimeLastPacketReceived"

#define		IPPingDiagnostics_DiagnosticsState				"Device.IP.Diagnostics.IPPing.DiagnosticsState"
#define		IPPingDiagnostics_Interface						"Device.IP.Diagnostics.IPPing.Interface"
#define		IPPingDiagnostics_Host							"Device.IP.Diagnostics.IPPing.Host"
#define		IPPingDiagnostics_NumberOfRepetitions			"Device.IP.Diagnostics.IPPing.NumberOfRepetitions"
#define		IPPingDiagnostics_Timeout						"Device.IP.Diagnostics.IPPing.Timeout"
#define		IPPingDiagnostics_DataBlockSize					"Device.IP.Diagnostics.IPPing.DataBlockSize"
#define		IPPingDiagnostics_DSCP							"Device.IP.Diagnostics.IPPing.DSCP"
#define		IPPingDiagnostics_SuccessCount					"Device.IP.Diagnostics.IPPing.SuccessCount"
#define		IPPingDiagnostics_FailureCount					"Device.IP.Diagnostics.IPPing.FailureCount"
#define		IPPingDiagnostics_AverageResponseTime			"Device.IP.Diagnostics.IPPing.AverageResponseTime"
#define		IPPingDiagnostics_MinimumResponseTime			"Device.IP.Diagnostics.IPPing.MinimumResponseTime"
#define		IPPingDiagnostics_MaximumResponseTime			"Device.IP.Diagnostics.IPPing.MaximumResponseTime"

#define		TraceRouteDiagnostics_DiagnosticsState			"Device.IP.Diagnostics.TraceRoute.DiagnosticsState"
#define		TraceRouteDiagnostics_Interface					"Device.IP.Diagnostics.TraceRoute.Interface"
#define		TraceRouteDiagnostics_Host						"Device.IP.Diagnostics.TraceRoute.Host"
#define		TraceRouteDiagnostics_NumberOfTries				"Device.IP.Diagnostics.TraceRoute.NumberOfTries"
#define		TraceRouteDiagnostics_Timeout					"Device.IP.Diagnostics.TraceRoute.Timeout"
#define		TraceRouteDiagnostics_DataBlockSize				"Device.IP.Diagnostics.TraceRoute.DataBlockSize"
#define		TraceRouteDiagnostics_DSCP						"Device.IP.Diagnostics.TraceRoute.DSCP"
#define		TraceRouteDiagnostics_MaxHopCount				"Device.IP.Diagnostics.TraceRoute.MaxHopCount"
#define		TraceRouteDiagnostics_ResponseTime				"Device.IP.Diagnostics.TraceRoute.ResponseTime"
#define		TraceRouteDiagnostics_RouteHopsNumberOfEntries	"Device.IP.Diagnostics.TraceRoute.RouteHopsNumberOfEntries"
#define		TraceRouteDiagnostics_RouteHopsObject			"Device.IP.Diagnostics.TraceRoute.RouteHops."
#define		TraceRouteDiagnostics_RouteHopsObject_HopHost	"Host"
#define		TraceRouteDiagnostics_RouteHopsObject_HopHostAddress	"HostAddress"
#define		TraceRouteDiagnostics_RouteHopsObject_HopErrorCode	"ErrorCode"
#define		TraceRouteDiagnostics_RouteHopsObject_HopRTTimes	"RTTimes"

#elif defined(WITH_DEVICE_ROOT)
#define		DownloadDiagnostics_DiagnosticsState			"Device.DownloadDiagnostics.DiagnosticsState"
#define		DownloadDiagnostics_Interface					"Device.DownloadDiagnostics.Interface"
#define		DownloadDiagnostics_DownloadURL					"Device.DownloadDiagnostics.DownloadURL"
#define		DownloadDiagnostics_DSCP						"Device.DownloadDiagnostics.DSCP"
#define		DownloadDiagnostics_EthernetPriority			"Device.DownloadDiagnostics.EthernetPriority"
#define		DownloadDiagnostics_ROMTime						"Device.DownloadDiagnostics.ROMTime"
#define		DownloadDiagnostics_BOMTime						"Device.DownloadDiagnostics.BOMTime"
#define		DownloadDiagnostics_EOMTime						"Device.DownloadDiagnostics.EOMTime"
#define		DownloadDiagnostics_TestBytesReceived			"Device.DownloadDiagnostics.TestBytesReceived"
#define		DownloadDiagnostics_TotalBytesReceived			"Device.DownloadDiagnostics.TotalBytesReceived"
#define		DownloadDiagnostics_TCPOpenRequestTime			"Device.DownloadDiagnostics.TCPOpenRequestTime"
#define		DownloadDiagnostics_TCPOpenResponseTime			"Device.DownloadDiagnostics.TCPOpenResponseTime"

#define		UploadDiagnostics_DiagnosticsState				"Device.UploadDiagnostics.DiagnosticsState"
#define		UploadDiagnostics_Interface						"Device.UploadDiagnostics.Interface"
#define		UploadDiagnostics_UploadURL						"Device.UploadDiagnostics.UploadURL"
#define		UploadDiagnostics_DSCP							"Device.UploadDiagnostics.DSCP"
#define		UploadDiagnostics_EthernetPriority				"Device.UploadDiagnostics.EthernetPriority"
#define		UploadDiagnostics_TestFileLength				"Device.UploadDiagnostics.TestFileLength"
#define		UploadDiagnostics_ROMTime						"Device.UploadDiagnostics.ROMTime"
#define		UploadDiagnostics_BOMTime						"Device.UploadDiagnostics.BOMTime"
#define		UploadDiagnostics_EOMTime						"Device.UploadDiagnostics.EOMTime"
#define		UploadDiagnostics_TotalBytesSent				"Device.UploadDiagnostics.TotalBytesSent"
#define		UploadDiagnostics_TCPOpenRequestTime			"Device.UploadDiagnostics.TCPOpenRequestTime"
#define		UploadDiagnostics_TCPOpenResponseTime			"Device.UploadDiagnostics.TCPOpenResponseTime"

#define		UDPEchoConfig_Enable							"Device.UDPEchoConfig.Enable"
#define		UDPEchoConfig_Interface							"Device.UDPEchoConfig.Interface"
#define		UDPEchoConfig_SourceIPAddress					"Device.UDPEchoConfig.SourceIPAddress"
#define		UDPEchoConfig_UDPPort							"Device.UDPEchoConfig.UDPPort"
#define		UDPEchoConfig_EchoPlusEnabled					"Device.UDPEchoConfig.EchoPlusEnabled"
#define		UDPEchoConfig_EchoPlusSupported					"Device.UDPEchoConfig.EchoPlusSupported"
#define		UDPEchoConfig_PacketsReceived					"Device.UDPEchoConfig.PacketsReceived"
#define		UDPEchoConfig_PacketsResponded					"Device.UDPEchoConfig.PacketsResponded"
#define		UDPEchoConfig_BytesReceived						"Device.UDPEchoConfig.BytesReceived"
#define		UDPEchoConfig_BytesResponded					"Device.UDPEchoConfig.BytesResponded"
#define		UDPEchoConfig_TimeFirstPacketReceived			"Device.UDPEchoConfig.TimeFirstPacketReceived"
#define		UDPEchoConfig_TimeLastPacketReceived			"Device.UDPEchoConfig.TimeLastPacketReceived"

#define		IPPingDiagnostics_DiagnosticsState				"Device.LAN.IPPingDiagnostics.DiagnosticsState"
//#define		IPPingDiagnostics_Interface						"Device.LAN.IPPingDiagnostics.Interface"
#define		IPPingDiagnostics_Host							"Device.LAN.IPPingDiagnostics.Host"
#define		IPPingDiagnostics_NumberOfRepetitions			"Device.LAN.IPPingDiagnostics.NumberOfRepetitions"
#define		IPPingDiagnostics_Timeout						"Device.LAN.IPPingDiagnostics.Timeout"
#define		IPPingDiagnostics_DataBlockSize					"Device.LAN.IPPingDiagnostics.DataBlockSize"
#define		IPPingDiagnostics_DSCP							"Device.LAN.IPPingDiagnostics.DSCP"
#define		IPPingDiagnostics_SuccessCount					"Device.LAN.IPPingDiagnostics.SuccessCount"
#define		IPPingDiagnostics_FailureCount					"Device.LAN.IPPingDiagnostics.FailureCount"
#define		IPPingDiagnostics_AverageResponseTime			"Device.LAN.IPPingDiagnostics.AverageResponseTime"
#define		IPPingDiagnostics_MinimumResponseTime			"Device.LAN.IPPingDiagnostics.MinimumResponseTime"
#define		IPPingDiagnostics_MaximumResponseTime			"Device.LAN.IPPingDiagnostics.MaximumResponseTime"

#define		TraceRouteDiagnostics_DiagnosticsState			"Device.LAN.TraceRouteDiagnostics.DiagnosticsState"
//#define		TraceRouteDiagnostics_Interface					"Device.LAN.TraceRouteDiagnostics.Interface"
#define		TraceRouteDiagnostics_Host						"Device.LAN.TraceRouteDiagnostics.Host"
#define		TraceRouteDiagnostics_Timeout					"Device.LAN.TraceRouteDiagnostics.Timeout"
#define		TraceRouteDiagnostics_DataBlockSize				"Device.LAN.TraceRouteDiagnostics.DataBlockSize"
#define		TraceRouteDiagnostics_MaxHopCount				"Device.LAN.TraceRouteDiagnostics.MaxHopCount"
#define		TraceRouteDiagnostics_DSCP						"Device.LAN.TraceRouteDiagnostics.DSCP"
#define		TraceRouteDiagnostics_ResponseTime				"Device.LAN.TraceRouteDiagnostics.ResponseTime"
#define		TraceRouteDiagnostics_RouteHopsNumberOfEntries	"Device.LAN.TraceRouteDiagnostics.NumberOfRouteHops"
#define		TraceRouteDiagnostics_RouteHopsObject			"Device.LAN.TraceRouteDiagnostics.RouteHops."
#define		TraceRouteDiagnostics_RouteHopsObject_HopHost	"HopHost"

#else

#define		DownloadDiagnostics_DiagnosticsState			"InternetGatewayDevice.DownloadDiagnostics.DiagnosticsState"
#define		DownloadDiagnostics_Interface					"InternetGatewayDevice.DownloadDiagnostics.Interface"
#define		DownloadDiagnostics_DownloadURL					"InternetGatewayDevice.DownloadDiagnostics.DownloadURL"
#define		DownloadDiagnostics_DSCP						"InternetGatewayDevice.DownloadDiagnostics.DSCP"
#define		DownloadDiagnostics_EthernetPriority			"InternetGatewayDevice.DownloadDiagnostics.EthernetPriority"
#define		DownloadDiagnostics_ROMTime						"InternetGatewayDevice.DownloadDiagnostics.ROMTime"
#define		DownloadDiagnostics_BOMTime						"InternetGatewayDevice.DownloadDiagnostics.BOMTime"
#define		DownloadDiagnostics_EOMTime						"InternetGatewayDevice.DownloadDiagnostics.EOMTime"
#define		DownloadDiagnostics_TestBytesReceived			"InternetGatewayDevice.DownloadDiagnostics.TestBytesReceived"
#define		DownloadDiagnostics_TotalBytesReceived			"InternetGatewayDevice.DownloadDiagnostics.TotalBytesReceived"
#define		DownloadDiagnostics_TCPOpenRequestTime			"InternetGatewayDevice.DownloadDiagnostics.TCPOpenRequestTime"
#define		DownloadDiagnostics_TCPOpenResponseTime			"InternetGatewayDevice.DownloadDiagnostics.TCPOpenResponseTime"

#define		UploadDiagnostics_DiagnosticsState				"InternetGatewayDevice.UploadDiagnostics.DiagnosticsState"
#define		UploadDiagnostics_Interface						"InternetGatewayDevice.UploadDiagnostics.Interface"
#define		UploadDiagnostics_UploadURL						"InternetGatewayDevice.UploadDiagnostics.UploadURL"
#define		UploadDiagnostics_DSCP							"InternetGatewayDevice.UploadDiagnostics.DSCP"
#define		UploadDiagnostics_EthernetPriority				"InternetGatewayDevice.UploadDiagnostics.EthernetPriority"
#define		UploadDiagnostics_TestFileLength				"InternetGatewayDevice.UploadDiagnostics.TestFileLength"
#define		UploadDiagnostics_ROMTime						"InternetGatewayDevice.UploadDiagnostics.ROMTime"
#define		UploadDiagnostics_BOMTime						"InternetGatewayDevice.UploadDiagnostics.BOMTime"
#define		UploadDiagnostics_EOMTime						"InternetGatewayDevice.UploadDiagnostics.EOMTime"
#define		UploadDiagnostics_TotalBytesSent				"InternetGatewayDevice.UploadDiagnostics.TotalBytesSent"
#define		UploadDiagnostics_TCPOpenRequestTime			"InternetGatewayDevice.UploadDiagnostics.TCPOpenRequestTime"
#define		UploadDiagnostics_TCPOpenResponseTime			"InternetGatewayDevice.UploadDiagnostics.TCPOpenResponseTime"

#define		UDPEchoConfig_Enable							"InternetGatewayDevice.UDPEchoConfig.Enable"
#define		UDPEchoConfig_Interface							"InternetGatewayDevice.UDPEchoConfig.Interface"
#define		UDPEchoConfig_SourceIPAddress					"InternetGatewayDevice.UDPEchoConfig.SourceIPAddress"
#define		UDPEchoConfig_UDPPort							"InternetGatewayDevice.UDPEchoConfig.UDPPort"
#define		UDPEchoConfig_EchoPlusEnabled					"InternetGatewayDevice.UDPEchoConfig.EchoPlusEnabled"
#define		UDPEchoConfig_EchoPlusSupported					"InternetGatewayDevice.UDPEchoConfig.EchoPlusSupported"
#define		UDPEchoConfig_PacketsReceived					"InternetGatewayDevice.UDPEchoConfig.PacketsReceived"
#define		UDPEchoConfig_PacketsResponded					"InternetGatewayDevice.UDPEchoConfig.PacketsResponded"
#define		UDPEchoConfig_BytesReceived						"InternetGatewayDevice.UDPEchoConfig.BytesReceived"
#define		UDPEchoConfig_BytesResponded					"InternetGatewayDevice.UDPEchoConfig.BytesResponded"
#define		UDPEchoConfig_TimeFirstPacketReceived			"InternetGatewayDevice.UDPEchoConfig.TimeFirstPacketReceived"
#define		UDPEchoConfig_TimeLastPacketReceived			"InternetGatewayDevice.UDPEchoConfig.TimeLastPacketReceived"

#define		IPPingDiagnostics_DiagnosticsState				"InternetGatewayDevice.IPPingDiagnostics.DiagnosticsState"
#define		IPPingDiagnostics_Interface						"InternetGatewayDevice.IPPingDiagnostics.Interface"
#define		IPPingDiagnostics_Host							"InternetGatewayDevice.IPPingDiagnostics.Host"
#define		IPPingDiagnostics_NumberOfRepetitions			"InternetGatewayDevice.IPPingDiagnostics.NumberOfRepetitions"
#define		IPPingDiagnostics_Timeout						"InternetGatewayDevice.IPPingDiagnostics.Timeout"
#define		IPPingDiagnostics_DataBlockSize					"InternetGatewayDevice.IPPingDiagnostics.DataBlockSize"
#define		IPPingDiagnostics_DSCP							"InternetGatewayDevice.IPPingDiagnostics.DSCP"
#define		IPPingDiagnostics_SuccessCount					"InternetGatewayDevice.IPPingDiagnostics.SuccessCount"
#define		IPPingDiagnostics_FailureCount					"InternetGatewayDevice.IPPingDiagnostics.FailureCount"
#define		IPPingDiagnostics_AverageResponseTime			"InternetGatewayDevice.IPPingDiagnostics.AverageResponseTime"
#define		IPPingDiagnostics_MinimumResponseTime			"InternetGatewayDevice.IPPingDiagnostics.MinimumResponseTime"
#define		IPPingDiagnostics_MaximumResponseTime			"InternetGatewayDevice.IPPingDiagnostics.MaximumResponseTime"

#define		TraceRouteDiagnostics_DiagnosticsState			"InternetGatewayDevice.TraceRouteDiagnostics.DiagnosticsState"
#define		TraceRouteDiagnostics_Interface					"InternetGatewayDevice.TraceRouteDiagnostics.Interface"
#define		TraceRouteDiagnostics_Host						"InternetGatewayDevice.TraceRouteDiagnostics.Host"
#define		TraceRouteDiagnostics_NumberOfTries				"InternetGatewayDevice.TraceRouteDiagnostics.NumberOfTries"
#define		TraceRouteDiagnostics_Timeout					"InternetGatewayDevice.TraceRouteDiagnostics.Timeout"
#define		TraceRouteDiagnostics_DataBlockSize				"InternetGatewayDevice.TraceRouteDiagnostics.DataBlockSize"
#define		TraceRouteDiagnostics_DSCP						"InternetGatewayDevice.TraceRouteDiagnostics.DSCP"
#define		TraceRouteDiagnostics_MaxHopCount				"InternetGatewayDevice.TraceRouteDiagnostics.MaxHopCount"
#define		TraceRouteDiagnostics_ResponseTime				"InternetGatewayDevice.TraceRouteDiagnostics.ResponseTime"
#define		TraceRouteDiagnostics_RouteHopsNumberOfEntries	"InternetGatewayDevice.TraceRouteDiagnostics.RouteHopsNumberOfEntries"
#define		TraceRouteDiagnostics_RouteHopsObject			"InternetGatewayDevice.TraceRouteDiagnostics.RouteHops."
#define		TraceRouteDiagnostics_RouteHopsObject_HopHost	"HopHost"
#define		TraceRouteDiagnostics_RouteHopsObject_HopHostAddress	"HopHostAddress"
#define		TraceRouteDiagnostics_RouteHopsObject_HopErrorCode	"HopErrorCode"
#define		TraceRouteDiagnostics_RouteHopsObject_HopRTTimes	"HopRTTimes"

#endif

#define			DiagnosticsState_None								"None"
#define			DiagnosticsState_Requested							"Requested"
#define			DiagnosticsState_Completed							"Completed"
#define			DiagnosticsState_Error_InitConnectionFailed			"Error_InitConnectionFailed"
#define			DiagnosticsState_Error_NoResponse					"Error_NoResponse"
#define			DiagnosticsState_Error_TransferFailed				"Error_TransferFailed"
#define			DiagnosticsState_Error_PasswordRequestFailed		"Error_PasswordRequestFailed"
#define			DiagnosticsState_Error_LoginFailed					"Error_LoginFailed"
#define			DiagnosticsState_Error_NoTransferMode				"Error_NoTransferMode"
#define			DiagnosticsState_Error_NoPASV						"Error_NoPASV"
#define			DiagnosticsState_Error_IncorrectSize				"Error_IncorrectSize"
#define			DiagnosticsState_Error_Timeout						"Error_Timeout"
#define			DiagnosticsState_Error_NoCWD						"Error_NoCWD"
#define			DiagnosticsState_Error_NoSTOR						"Error_NoSTOR"
#define			DiagnosticsState_Error_NoTransferComplete			"Error_NoTransferComplete"


int setDownloadDiagnostics( const char *, ParameterType, ParameterValue * );
int setUploadDiagnostics( const char *, ParameterType, ParameterValue * );
int setIPPingDiagnostics( const char *, ParameterType, ParameterValue * );
int setTraceRouteDiagnostics( const char *, ParameterType, ParameterValue * );
int setWANDSLDiagnostics( const char *, ParameterType, ParameterValue * );
int setATMF5Diagnostics( const char *, ParameterType, ParameterValue * );
int set_WANATMF5LoopbackDiagnostics_NumberOfRepetitions( const char *, ParameterType, ParameterValue * );
int set_WANATMF5LoopbackDiagnostics_Timeout( const char *, ParameterType, ParameterValue * );

#endif /*HAVE_DIAGNOSTICS*/

#ifdef HAVE_UDP_ECHO

#define DEFAULT_UDPECHO_PORT	64200
#define ERROR_INVALID_INTERFACE	1

void *udpEchoHandler(void *);
void terminateUDPEchoHandler(pthread_t );
int updateUDPEchoConfigEnable(int );
int updateUDPEchoConfigInterface(char *);
int updateUDPEchoConfigPort(unsigned int );
void shutdownUDPEchoSocket();
#endif /* HAVE_UDP_ECHO */

#endif /* diagParameter_H */
