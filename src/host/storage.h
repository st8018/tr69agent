/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef FILESTORE_H_
#define FILESTORE_H_

#define FILE_MASK	0666

/** FILETRANSFER_STORE_STATUS controls, if the status change in a transferEntry Object
 is written to file or not.
 If defined the status is written, undefined it is not.
 The difference is, after a reboot the download is done again or not, because the status in the
 stored data is not updated.
*/
#define FILETRANSFER_STORE_STATUS

#define FILETRANSFER_MAX_INFO_SIZE 2048

#endif /*FILESTORE_H_*/
