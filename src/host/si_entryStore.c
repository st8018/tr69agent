/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

/**
 * All schedule informs entry information is stored in the file system.
 */

#include <dirent.h>
#include "si_entryStore.h"
#include "dimark_globals.h"
#include "debug.h"
#include "unistd.h"

extern	char*	PERSISTENT_SI_ENTRYLIST_DIR;

static int loadScheduleInformEntryFile (char *, char *, newScheduleInformEntryInfo * );

/** Read all informations of pending schedule informs entryes from the storage
 *  for every file info the callback function is called.
 */
int readScheduleInformEntryInfos(newScheduleInformEntryInfo *func)
{
	char buf[MAX_PATH_NAME_SIZE];
	char *bufPtr;
	struct dirent *entry;
	int ret = OK;
	DIR *dir;
	int nfiles;

	nfiles = 0;
	dir = opendir (PERSISTENT_SI_ENTRYLIST_DIR);
	if (dir == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_SCHEDULE, "readScheduleInformEntryInfos: directory not found %s\n", PERSISTENT_SI_ENTRYLIST_DIR);
		)

		return ERR_INTERNAL_ERROR;
	}

	strncpy (buf, PERSISTENT_SI_ENTRYLIST_DIR, sizeof(buf)-1);
	bufPtr = (buf + strlen(PERSISTENT_SI_ENTRYLIST_DIR));

	while ((entry = readdir (dir)) != NULL)
	{
		// skip . and .. and all files starting with .
		if (entry->d_name[0] == '.')
			continue;
		*bufPtr = '\0';
		strncat (buf, entry->d_name, sizeof(buf) - strlen(buf) - 1 );
		ret = loadScheduleInformEntryFile (buf, entry->d_name, func);
	}
	closedir(dir);
	return ret;
}

/** Write the informations for one schedule inform entry into the storage
 *
 * \param name 	unique name of the information
 * \param data	transfer informations
 */
int storeScheduleInformEntryInfo(const char *name, const char *data)
{
	int ret = OK;
	char path[MAX_PATH_NAME_SIZE + 1];
	int fd;

	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_SCHEDULE, "storeScheduleInformEntryInfo(): function parameter 'name' can not be NULL.\n");
		)
		return ERR_INTERNAL_ERROR;
	}

	// build pathname for storage file
	strncpy( path, PERSISTENT_SI_ENTRYLIST_DIR, MAX_PATH_NAME_SIZE );
	strncat( path, name, MAX_PATH_NAME_SIZE - strlen(path));
	fd = open( path, O_RDWR|O_CREAT|O_TRUNC, 0777 );
	if ( fd < 0 )
		return ERR_INTERNAL_ERROR;
	if (data)
	{
		ret = write( fd, data, strlen(data));
		if ( ret < 0 )
		{
			close( fd );
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_SCHEDULE, "storeScheduleInformEntryInfo: write() returns error %d\n", ret );
			)
			return ERR_INTERNAL_ERROR;
		}
	}
	ret = close( fd );
	if ( ret != OK )
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_SCHEDULE, "storeScheduleInformEntryInfo: close() returns error %d\n", ret );
		)
		return ERR_INTERNAL_ERROR;
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_SCHEDULE, "storeScheduleInformEntryInfo: %s : %d\n", data, ret );
	)

	return ret;
}

/** Delete the information of a specific schedule inform entry
 *
 * \param name	unique name of the information
 */
int deleteScheduleInformEntryInfo(const char *name)
{
	int ret = OK;
	char path[MAX_PATH_NAME_SIZE+1];

	// build pathname for storage file
	strncpy( path, PERSISTENT_SI_ENTRYLIST_DIR, MAX_PATH_NAME_SIZE );
	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_SCHEDULE, "deleteScheduleInformEntryInfo(): function parameter 'name' can not be NULL.\n");
		)
		return ERR_INTERNAL_ERROR;
	}
	strncat( path, name, MAX_PATH_NAME_SIZE - strlen(path) );
	ret = remove(path);
	if ( ret != OK )
		ret = ERR_INTERNAL_ERROR;
	return ret;
}


/** Delete all informations about pending schedule inform entryes
 */
int clearAllScheduleInformEntryInfo( void )
{
	int ret = OK;
	struct dirent *entry;
	DIR *dir;
	int nfiles;
	char buf[MAX_PATH_NAME_SIZE];

	nfiles = 0;
	dir = opendir (PERSISTENT_SI_ENTRYLIST_DIR);
	if (dir == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_SCHEDULE, "clearAllScheduleInformEntryInfo(): directory not found %s\n", PERSISTENT_SI_ENTRYLIST_DIR);
		)
		return ERR_INTERNAL_ERROR;
	}
	while ((entry = readdir (dir)) != NULL)
	{
		if (strcmp (entry->d_name, ".") == 0)
			continue;
		if (strcmp (entry->d_name, "..") == 0)
			continue;
		sprintf (buf, "%s%s", PERSISTENT_SI_ENTRYLIST_DIR, entry->d_name);
		ret = remove (buf);
		if ( ret < 0 ) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_SCHEDULE, "clearAllScheduleInformEntryInfo(): file not found %s\n", buf);
			)
			closedir(dir);
			return ERR_INTERNAL_ERROR;
		}
	}
	closedir(dir);
	return ret;
}

static int loadScheduleInformEntryFile(char *filename, char *name, newScheduleInformEntryInfo* callbackF )
{
	int ret = OK;
	char buf[SI_MAX_INFO_SIZE + 1];
	FILE *file;

	if (!filename)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_SCHEDULE, "loadScheduleInformEntryFile(): function parameter 'filename' can not be NULL.\n");
		)
		return ERR_INTERNAL_ERROR;
	}

	file = fopen (filename, "r");
	if (file == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_SCHEDULE, "loadScheduleInformEntryFile(): file not found %s\n", filename);
		)

		return ERR_INTERNAL_ERROR;
	}

	while (fgets (buf, SI_MAX_INFO_SIZE, file) != NULL)
	{
		if ( buf[0] == '#' || buf[0] == ' ' || buf[0] == '\n' || buf[0] == '\0' )
			continue;
		buf[strlen (buf) - 1] = '\0';	/* remove trailing EOL  */
		ret += callbackF(name, buf);
	}
	fclose(file);
	return ret;
}
