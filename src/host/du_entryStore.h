/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * du_entryStore.h
 *
 *  Created on: 25.01.2012
 *      Author: apnt
 */

#ifndef DU_ENTRYSTORE_H_
#define DU_ENTRYSTORE_H_

#define DU_TRANSFER_MAX_INFO_SIZE	4096

#include "utils.h"

typedef int (newDeploymentUnitEntryInfo)(char *, char *);

/** Read all informations of pending deployment unit entry from the storage
 *  for every file info the callback function is called.
 */
int readDeploymentUnitEntryInfos(newDeploymentUnitEntryInfo *);

/** Write the informations for one deployment unit entry into the storage
 *
 * \param name 	unique name of the information
 * \param data	du informations
 */
int storeDeploymentUnitEntryInfo( const char *, const char * );

/** Delete the information of a specific deployment unit entry
 *
 * \param name	unique name of the information
 */
int deleteDeploymentUnitEntryInfo( const char * );


/** Delete all informations about pending deployment unit entryes
 */
int clearAllDeploymentUnitEntryInfo( void );

#endif /* DU_ENTRYSTORE_H_ */
