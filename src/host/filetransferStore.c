/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

/**
 * All file transfer information is stored in the file system.
 */

#include <dirent.h>

#include "filetransferStore.h"
#include "dimark_globals.h"
#include "debug.h"
#include "unistd.h"
#include "storage.h"

extern	char*	PERSISTENT_TRANSFERLIST_DIR;
extern	char*	DEFAULT_DOWNLOAD_FILE;

static int loadFtFile (char *, char *, newFtInfo * );

/** Read all informations of pending file transfers from the storage
 *  for every file info the callback function is called.
 */
int
readFtInfos( newFtInfo *func )
{
	char buf[MAX_PATH_NAME_SIZE];
	char *bufPtr;
	struct dirent *entry;
	int ret = OK;
	DIR *dir;
	int nfiles;

	nfiles = 0;
	dir = opendir (PERSISTENT_TRANSFERLIST_DIR);
	if (dir == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "reloadTransferlist: directory not found %s\n", PERSISTENT_TRANSFERLIST_DIR);
		)

		return ERR_DIM_TRANSFERLIST_READ;
	}

	strncpy (buf, PERSISTENT_TRANSFERLIST_DIR, sizeof(buf)-1);
	bufPtr = (buf + strlen(PERSISTENT_TRANSFERLIST_DIR));

	while ((entry = readdir (dir)) != NULL)
	{
		// skip . and .. and all files starting with .
		if (entry->d_name[0] == '.') 
			continue;
		*bufPtr = '\0';
		strncat (buf, entry->d_name, sizeof(buf) - strlen(buf) - 1 );
		ret = loadFtFile (buf, entry->d_name, func);
	}
	closedir(dir);
	return ret;
}

/** Write the informations for one file transfer into the storage
 *  
 * \param name 	unique name of the information
 * \param data	transfer informations
 */
int
storeFtInfo( const char *name, const char *data )
{
	int ret = OK;
	char path[MAX_PATH_NAME_SIZE + 1];
	int fd;

	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "storeFtInfo(): function parameter 'name' cannot be NULL.\n");
		)
		return ERR_DIM_TRANSFERLIST_WRITE;
	}
	// build pathname for storage file
	strncpy( path, PERSISTENT_TRANSFERLIST_DIR, MAX_PATH_NAME_SIZE );
	strncat( path, name, MAX_PATH_NAME_SIZE - strlen(path));
	fd = open( path, O_RDWR|O_CREAT|O_TRUNC, 0777 );
	if ( fd < 0 )
		return ERR_DIM_TRANSFERLIST_WRITE;
	if (data)
	{
		ret = write( fd, data, strlen(data));
		if ( ret < 0 )
		{
			close( fd );
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_TRANSFER, "Save FTInfo: write() returns error %d\n", ret );
			)
			return ERR_DIM_TRANSFERLIST_WRITE;
		}
	}
	ret = close( fd );
	if ( ret != OK )
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "Save FTInfo: close() returns error %d\n", ret );
		)
		return ERR_DIM_TRANSFERLIST_WRITE;
	}
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "Save FTInfo: %s : %d\n", data, ret );
	)
	return ret;	
}

/** Delete the information of a specific file transfer
 * 
 * \param name	unique name of the information
 */
int
deleteFtInfo( const char *name )
{
	int ret = OK;
	char path[MAX_PATH_NAME_SIZE+1];

	// build pathname for storage file
	strncpy( path, PERSISTENT_TRANSFERLIST_DIR, MAX_PATH_NAME_SIZE );
	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "deleteFtInfo(): function parameter 'name' cannot be NULL.\n");
		)
		return ERR_DIM_TRANSFERLIST_WRITE;
	}
	strncat( path, name, MAX_PATH_NAME_SIZE - strlen(path) );
	ret = remove(path);
	if ( ret != OK )
		ret = ERR_DIM_TRANSFERLIST_WRITE;	
	return ret;
}


/** Delete all informations about pending file transfers
 */
int
clearAllFtInfo( void )
{

	int ret = OK;
	struct dirent *entry;
	DIR *dir;
	int nfiles;
	char buf[MAX_PATH_NAME_SIZE];

	nfiles = 0;
	dir = opendir (PERSISTENT_TRANSFERLIST_DIR);
	if (dir == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "deleteTransferlist: directory not found %s\n", PERSISTENT_TRANSFERLIST_DIR);
		)
		return ERR_DIM_TRANSFERLIST_WRITE;
	}
	while ((entry = readdir (dir)) != NULL)
	{
		if (strcmp (entry->d_name, ".") == 0)
			continue;
		if (strcmp (entry->d_name, "..") == 0)
			continue;
		sprintf (buf, "%s%s", PERSISTENT_TRANSFERLIST_DIR, entry->d_name);
		ret = remove (buf);
		if ( ret < 0 ) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_TRANSFER, "deleteTransferlist: file not found %s\n", buf);
			)
			closedir(dir);
			return ERR_DIM_TRANSFERLIST_WRITE;
		}
	}
	closedir(dir);
	return ret;	
}

/** Returns a default filename as destination for a download.
 * This is used if the ACS does not deliver a destination filename in the download request
 */
char*
getDefaultDownloadFilename( char * url)
{
	char * tmp;
	if (!url)
		return DEFAULT_DOWNLOAD_FILE;

	tmp = strrchr(url, '/');
	if (!tmp)
		return DEFAULT_DOWNLOAD_FILE;
	return (tmp+1);
}

static int
loadFtFile(char *filename, char *name, newFtInfo* callbackF )
{
	int ret = OK;
	char buf[FILETRANSFER_MAX_INFO_SIZE + 1];
	FILE *file;

	if (!filename)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "loadFtFile(): function parameter 'filename' cannot be NULL.\n");
		)
		return ERR_DIM_TRANSFERLIST_WRITE;
	}

	file = fopen (filename, "r");
	if (file == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "loadInitialParameters: file not found %s\n", filename);
		)

		return ERR_INTERNAL_ERROR;
	}

	while (fgets (buf, FILETRANSFER_MAX_INFO_SIZE, file) != NULL)
	{
		if ( buf[0] == '#' || buf[0] == ' ' || buf[0] == '\n' || buf[0] == '\0' )
			continue;
		buf[strlen (buf) - 1] = '\0';	/* remove trailing EOL  */
		ret += callbackF(name, buf);
	}
	fclose(file);
	return ret;
} 
