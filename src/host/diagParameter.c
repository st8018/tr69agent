/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                      *
 *    support@dimark.com                                                   *
 ***************************************************************************/

/*
 * Copyright (c) 1985, 1989 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <arpa/ftp.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <stdlib.h>


#include "dimark_globals.h"
#include "diagParameter.h"
#include "parameterStore.h"
#include "parameter.h"
#include "utils.h"
#include "filetransfer.h"
#include "parameter.h"
#include "utils.h"
#include "httpda.h"
#include "list.h"
#include "serverdata.h"
#include "ftp_ft.h"
#include "ftp_var.h"
#include "eventcode.h"
#include "diagnosticsHandler.h"
#include "ipping_profile.h"
#include "traceroute_profile.h"

extern pthread_mutex_t informLock;
extern pthread_mutex_t paramLock;

#ifdef HAVE_DIAGNOSTICS

#define		ISUNKNOWNHOST 20000
#define		MAXHOPCOUNTEXCEEDED 20001

static int setTimeParamToCurrentTime( const char * );

#ifdef HAVE_FILE

#define		HTTP_DOWNLOAD_DIAGNOSTICS_FILE	"HTTP_DOWNLOAD_DIAGNOSTICS_FILE"
#define		FTP_DOWNLOAD_DIAGNOSTICS_FILE	"FTP_DOWNLOAD_DIAGNOSTICS_FILE"

struct	Types {
	const char *t_name;
	const char *t_mode;
	int t_type;
	const char *t_arg;
};
static struct Types types[] = {
		{ "ascii",	"A",	TYPE_A,	NULL },
		{ "binary",	"I",	TYPE_I,	NULL },
		{ "image",	"I",	TYPE_I,	NULL },
		{ "ebcdic",	"E",	TYPE_E,	NULL },
		{ "tenex",	"L",	TYPE_L,	bytename },
		{ NULL, NULL, 0, NULL }
};

extern char proxy_host[];
extern int  proxy_port;
extern char proxy_userid[];
extern char proxy_passwd[];
extern char proxy_version[];

static void _do_settype( const char * );
static int _doLogin( const char *, const char *, const char * );
static int _ftp_login( char *, const char *, const char * );

/*
 * Set transfer type.
 */
static void
_do_settype(const char *thetype)
{
	struct Types *p;
	int comret;

	for (p = types; p->t_name; p++)
		if (strcmp(thetype, p->t_name) == 0)
			break;
	if (p->t_name == 0) {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "_do_settype: %s - unknown mode\n", thetype);
		)

		code = -1;
		return;
	}
	if ((p->t_arg != NULL) && (*(p->t_arg) != '\0'))
		comret = command("TYPE %s %s", p->t_mode, p->t_arg);
	else
		comret = command("TYPE %s", p->t_mode);
	if (comret == COMPLETE) {
		(void) strncpy(typename, p->t_name, sizeof(typename)-1 );
		curtype = type = p->t_type;
	}
}

/** Connect to a host with username and password
 */
static	int
_doLogin( const char *host, const char *user, const char *pass )
{
	int n;
	char *nHost;
	unsigned short port;
	ParameterValue value;

	port = ftp_port;

	nHost = hookup(host, port);
	if ( nHost ) {
		// set username
		if ( !*user )
			user = "anonymous";
		n = command("USER %s", user);
		if (n != CONTINUE) {
			value.in_cval = DiagnosticsState_Error_PasswordRequestFailed;
			updateParamValue(DownloadDiagnostics_DiagnosticsState, StringType, &value);
			return (0);
		}
		if (n == CONTINUE) {
			// set password
			if ( !*pass )
				pass = "guest";
			n = command("PASS %s", pass);
		}
		if (n != COMPLETE) {
			value.in_cval = DiagnosticsState_Error_LoginFailed;
			updateParamValue(DownloadDiagnostics_DiagnosticsState, StringType, &value);
			return (0);
		}
		_do_settype( "binary" );
		return (1);
	} else {
		value.in_cval = DiagnosticsState_Error_InitConnectionFailed;
		updateParamValue(DownloadDiagnostics_DiagnosticsState, StringType, &value);
		return (0);
	}
}

static	int
_ftp_login(char *hostname, const char *username, const char *password )
{
	ftp_port = htons(21);
	passivemode = 0;

	cpend = 0;	//* no pending replies
	proxy = 0;	//* proxy not active
	sendport = -1;	//* not using ports
	/*
	 * Connect to the remote server
	 */
	connected = _doLogin( hostname, username, password);
	return ( connected == 1 ? OK : ERR_TRANS_AUTH_FAILURE );
}

#endif /* HAVE_FILE */

#ifdef HAVE_DOWNLOAD_DIAGNOSTICS

static int DiagDoHttpDownload (char *, char *, char *, int, char *, struct soap *);
static int _dimget( char *, char *, long * );
static int _ftp_get( char *, char *, long * );
static int DiagDoFtpDownload (char *, char *, char *, int, char *);

/*
 * Try HTTP download a file URL
 */
static int
DiagDoHttpDownload (char *URL, char *username, char *password, int bufferSize, char *targetFileName, struct soap *tmpSoap)
{
	int ret = OK;
	int readSize = 0;
	ParameterValue value;

	char *inpPtr;
	unsigned int cnt = 0; //counter of bytes
	int fd = 0;
	bool isError = false;

	// using of soap structure buf for input
	tmpSoap->proxy_host = proxy_host[0] ? proxy_host : NULL;
	tmpSoap->proxy_port = proxy_port;
	tmpSoap->proxy_userid = proxy_userid[0] ? proxy_userid : NULL;
	tmpSoap->proxy_passwd = proxy_passwd[0] ? proxy_passwd : NULL;
	tmpSoap->proxy_http_version = proxy_version[0] ? proxy_version : NULL;

	soap_begin (tmpSoap);
	tmpSoap->recv_timeout = 30;	// Timeout after 30 seconds stall on recv
	tmpSoap->send_timeout = 60;	// Timeout after 1 minute stall on send
	tmpSoap->keep_alive = 1;
	tmpSoap->status = SOAP_GET;
	tmpSoap->authrealm = "";

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "DiagHttpDownloadGet: %s Host: %s Port: %d\n", URL, tmpSoap->host, tmpSoap->port);
	)

	setTimeParamToCurrentTime( DownloadDiagnostics_TCPOpenRequestTime );

#if defined(WITH_OPENSSL) || defined(WITH_GNUTLS)
#if defined(WITH_CLIENT_SSLAUTH) && !defined(WITH_SERVER_SSLAUTH)
	if(soap_ssl_client_context (tmpSoap,
								SOAP_SSL_REQUIRE_CLIENT_AUTHENTICATION | SOAP_SSLv3_TLSv1,
								"client.pem",			 	/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
								"password", 				/* password to read the key file (not used with GNUTLS) */
								NULL, 						/* cacert file to store trusted certificates (needed to verify server) */
								NULL, 						/* capath to directory with trusted certificates */
								NULL 						/* if randfile!=NULL: use a file with random data to seed randomness */
								))
	{
		soap_print_fault(tmpSoap, stderr);
	}
#endif /* defined(WITH_CLIENT_SSLAUTH) && !defined(WITH_SERVER_SSLAUTH) */

#if defined(WITH_SERVER_SSLAUTH) && !defined(WITH_CLIENT_SSLAUTH)
	if(soap_ssl_client_context (tmpSoap,
								SOAP_SSL_REQUIRE_SERVER_AUTHENTICATION | SOAP_SSLv3_TLSv1,
								NULL,					 	/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
								NULL, 						/* password to read the key file (not used with GNUTLS) */
								"ca.crt",					/* cacert file to store trusted certificates (needed to verify server) */
								NULL, 						/* capath to directory with trusted certificates */
								NULL 						/* if randfile!=NULL: use a file with random data to seed randomness */
								))
	{
		soap_print_fault(tmpSoap, stderr);
	}
#endif /* defined(WITH_SERVER_SSLAUTH) && !defined(WITH_CLIENT_SSLAUTH) */

#if !defined(WITH_CLIENT_SSLAUTH) && !defined(WITH_SERVER_SSLAUTH)
	if(soap_ssl_client_context (tmpSoap,
								SOAP_SSL_NO_AUTHENTICATION,
								NULL,						/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
								NULL, 						/* password to read the key file (not used with GNUTLS) */
								NULL, 						/* cacert file to store trusted certificates (needed to verify server) */
								NULL, 						/* capath to directory with trusted certificates */
								NULL 						/* if randfile!=NULL: use a file with random data to seed randomness */
								))
	{
		soap_print_fault(tmpSoap, stderr);
	}
#endif /* !defined(WITH_CLIENT_SSLAUTH) && !defined(WITH_SERVER_SSLAUTH) */

#if defined(WITH_CLIENT_SSLAUTH) && defined(WITH_SERVER_SSLAUTH)
	if(soap_ssl_client_context (tmpSoap,
								SOAP_SSL_REQUIRE_CLIENT_AUTHENTICATION | SOAP_SSL_REQUIRE_SERVER_AUTHENTICATION | SOAP_SSLv3_TLSv1,
								"client.pem",			 	/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
								"password", 				/* password to read the key file (not used with GNUTLS) */
								"ca.crt", 					/* cacert file to store trusted certificates (needed to verify server) */
								NULL, 						/* capath to directory with trusted certificates */
								NULL 						/* if randfile!=NULL: use a file with random data to seed randomness */
								))
	{
		soap_print_fault(tmpSoap, stderr);
	}
#endif /* defined(WITH_CLIENT_SSLAUTH) && defined(WITH_SERVER_SSLAUTH) */
#endif /* defined(WITH_OPENSSL) || defined(WITH_GNUTLS) */

	if (make_connect(tmpSoap, URL)
			|| tmpSoap->fpost (tmpSoap, URL, tmpSoap->host, tmpSoap->port, tmpSoap->path, NULL, 0))
	{
		ret = tmpSoap->error;
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Request send %d %d\n", ret, tmpSoap->errmode);
		)
		value.in_cval = DiagnosticsState_Error_InitConnectionFailed;
		updateParamValue(DownloadDiagnostics_DiagnosticsState, StringType, &value);

		return DIAG_ERROR;
	}

	setTimeParamToCurrentTime( DownloadDiagnostics_TCPOpenResponseTime );
	setTimeParamToCurrentTime( DownloadDiagnostics_ROMTime );

	// Parse the HTML response header and remove it from the work data
	ret = tmpSoap->fparse (tmpSoap);

	if ( ret != OK )
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Request %d\n", ret);
		)
		value.in_cval = DiagnosticsState_Error_NoResponse;
		updateParamValue(DownloadDiagnostics_DiagnosticsState, StringType, &value);
		return DIAG_ERROR;
	}

	setTimeParamToCurrentTime( DownloadDiagnostics_BOMTime );
	unsigned int uintTotalBytesReceived = tmpSoap->buflen;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Request to endpoint: %s, ret=%d\n", URL, ret);
	)

	inpPtr = tmpSoap->buf;

	// open the output file, must be given with the complete path
	if ((fd = open (targetFileName, O_RDWR | O_CREAT | O_TRUNC, 0777)) < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Can't open File: %s\n", targetFileName);
		)
		value.in_cval = DiagnosticsState_Error_TransferFailed;
		updateParamValue(DownloadDiagnostics_DiagnosticsState, StringType, &value);
		return DIAG_ERROR;
	}

	// the first part of the data is already in the buffer,
	// Therefore calculate the offset and write it to the output file
	// bufidx is the index to the first data after the HTML header
	cnt = tmpSoap->buflen - tmpSoap->bufidx;
	inpPtr = tmpSoap->buf;
	inpPtr += tmpSoap->bufidx;
	if (write (fd, inpPtr, cnt) < 0)
		isError = true;

	// Get the rest of the data
	while (isError == false
			&& (readSize =
					tmpSoap->frecv (tmpSoap, inpPtr, SOAP_BUFLEN)) > 0)
	{
		if (write (fd, inpPtr, readSize) < 0)
		{
			isError = true;
			break;
		}
		cnt += readSize;
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "BuffSize: %d  read: %d  soap->err: %d\n", bufferSize, cnt, tmpSoap->errnum);
	)
	if (close (fd) < 0)
	{
		value.in_cval = DiagnosticsState_Error_TransferFailed;
		updateParamValue(DownloadDiagnostics_DiagnosticsState, StringType, &value);
		return DIAG_ERROR;
	}

	setTimeParamToCurrentTime( DownloadDiagnostics_EOMTime );

	setUniversalParamValueInternal(NULL, DownloadDiagnostics_TestBytesReceived, 1, cnt);
	setUniversalParamValueInternal(NULL, DownloadDiagnostics_TotalBytesReceived, 1, cnt + uintTotalBytesReceived);

	// Check if we got all the data we expected
	if (cnt == 0)
	{
		value.in_cval = DiagnosticsState_Error_TransferFailed;
		updateParamValue(DownloadDiagnostics_DiagnosticsState, StringType, &value);
		return DIAG_ERROR;
	}
	else
	{
		value.in_cval = DiagnosticsState_Completed;
		updateParamValue(DownloadDiagnostics_DiagnosticsState, StringType, &value);
		return OK;
	}
}

static int
_dimget( char *remoteFile, char *localFile, long *size )
{
	return recvrequest("RETR", localFile, remoteFile, "w", size );
}

static int
_ftp_get( char *remoteFile, char *localFile, long *size )
{
	*size = 0;
	return _dimget( remoteFile, localFile, size );
}


/*
 * Try HTTP download a file URL
 */
static int
DiagDoFtpDownload (char *URL, char *username, char *password, int bufferSize, char *targetFileName)
{
	// extract remote host and remote filename from URL
	// Only ftp://hostname[:port]/dir/dir/remotename  is allowed
	// the remote name must be relative to the ftp directory
	//
	char *tmp;
	char *host;
	char *remoteFileName;
	long size;
	int	ret = OK;

	// skip scheme "ftp://"
	host = URL + 6; // strstr( URL, "://" );
	tmp = host;
	while ( *tmp && *tmp != ':' && *tmp != '/' ) {
		tmp++;
	}
	*tmp = '\0';
	tmp++;
	remoteFileName = tmp;

	if ((ret = _ftp_login( host, username, password )) != OK )
		return ret;

	setTimeParamToCurrentTime( DownloadDiagnostics_TCPOpenRequestTime );
	setTimeParamToCurrentTime( DownloadDiagnostics_TCPOpenResponseTime );
	setTimeParamToCurrentTime( DownloadDiagnostics_ROMTime );
	setTimeParamToCurrentTime( DownloadDiagnostics_BOMTime );

	ret = _ftp_get( remoteFileName, targetFileName, (long *)&size );
	if ( ret != OK )
	{
		setUniversalParamValueInternal(NULL, DownloadDiagnostics_DiagnosticsState, 1, DiagnosticsState_Error_NoTransferMode);
		return DIAG_ERROR;
	}
	else
	{
		setTimeParamToCurrentTime( DownloadDiagnostics_EOMTime );
		setUniversalParamValueInternal(NULL, DownloadDiagnostics_TestBytesReceived, 1, (unsigned int)size);
		setUniversalParamValueInternal(NULL, DownloadDiagnostics_TotalBytesReceived, 1, (unsigned int)size);
		setUniversalParamValueInternal(NULL, DownloadDiagnostics_DiagnosticsState, 1, DiagnosticsState_Completed);
	}
	if ( size < 0 || ( bufferSize != 0 && size != bufferSize))
		return DIAG_ERROR;

	ftp_disconnect();

	return OK;
}

#endif /* HAVE_DOWNLOAD_DIAGNOSTICS */

#ifdef HAVE_UPLOAD_DIAGNOSTICS

#define ERR_NO_TRANSFER_MODE 		9870
#define ERR_INIT_CONNECTION_FAILED 	9871
#define ERR_NO_TRANSFER_COMPLETE 	9872
#define ERR_NO_RESPONSE			 	9873
#define ERR_LOGIN_FAILED		 	9874



static int data = -1;
static sigjmp_buf sendabort;
static int abrtflag = 0;

extern FILE *dataconn( const char * );
extern char info_for_soap_header[100];

static int DiagSendFile( struct soap *, long );
static int DiagUploadFile( struct soap *, const UploadFile *, long );
static int DiagDoHttpUpload ( char *, char *, char *, long, struct soap * );
static void abortsend( int );
static int _sendrequest( const char *, char *, char *, long * );
static int _dimput( char *, char *, long * );
static int _ftp_put( char *, char *, long * );
static int DiagDoFtpUpload (char *, char *, char *, long );
static int cbUploadDiagnostics (struct soap *);

static int 
DiagSendFile( struct soap *tmpSoap, long fileSize )
{
	int ret = OK;
	int	i = 0;
	int j = 0;
	int buf_len = sizeof(tmpSoap->tmpbuf);

	srand(time(NULL));

	while(i < fileSize)
	{
		tmpSoap->tmpbuf[j] = 'a' + (rand() % 26);
		i++; j++;
		if (j == buf_len)
		{
			if ( (ret = soap_send_raw( tmpSoap, tmpSoap->tmpbuf, j )) != SOAP_OK )
			{
				return ret;
			}
			j = 0;
		}
	}
	if ( j > 2)
	{
		tmpSoap->tmpbuf[j-2] = '\n';
		tmpSoap->tmpbuf[j-1] = '\0';
	}
	return	soap_send_raw( tmpSoap, tmpSoap->tmpbuf, j );
}

/*
 * Try upload a file srcFile
 */
static int 
DiagUploadFile( struct soap *tmpSoap, const UploadFile *srcFile, long fileSize )
{
	char fileSizeStr[20];
	ParameterValue value;

	if (tmpSoap->path[strlen(tmpSoap->path)-1] == '/')
		sprintf(tmpSoap->tmpbuf, "\r\nPUT %s HTTP/1.1\r\n", tmpSoap->path);
	else
		sprintf(tmpSoap->tmpbuf, "\r\nPUT %s/ HTTP/1.1\r\n", tmpSoap->path);
	soap_send(tmpSoap, tmpSoap->tmpbuf);
	sprintf(tmpSoap->tmpbuf, "%s:%d", tmpSoap->host, tmpSoap->port);
	tmpSoap->fposthdr(tmpSoap, "Host", tmpSoap->tmpbuf);
	if (tmpSoap->userid && tmpSoap->passwd && strlen(tmpSoap->userid) + strlen(tmpSoap->passwd) < 761)
	{
		sprintf(tmpSoap->tmpbuf + 262, "%s:%s", tmpSoap->userid, tmpSoap->passwd);
		strcpy(tmpSoap->tmpbuf, "Basic ");
		soap_s2base64(tmpSoap, (const unsigned char*) (tmpSoap->tmpbuf + 262), tmpSoap->tmpbuf + 6, strlen(tmpSoap->tmpbuf + 262));
		tmpSoap->fposthdr(tmpSoap, "Authorization", tmpSoap->tmpbuf);
	}
	tmpSoap->fposthdr(tmpSoap, "User-Agent", info_for_soap_header);
	tmpSoap->fposthdr(tmpSoap, "Connection", tmpSoap->keep_alive ? "keep_alive" : "close");
	tmpSoap->fposthdr(tmpSoap, "Content-Type", srcFile->filetype);

	// Calc contentlength
	sprintf(fileSizeStr, "%ld", fileSize);
	tmpSoap->fposthdr(tmpSoap, "Content-Length", fileSizeStr);
	soap_send(tmpSoap, "\r\n");

	setTimeParamToCurrentTime( UploadDiagnostics_BOMTime );

	pthread_testcancel();
	if ( DiagSendFile( tmpSoap, fileSize ) != OK)
	{
		return ERR_NO_TRANSFER_COMPLETE;
	}

	if (soap_send(tmpSoap, "\r\n") != SOAP_OK)
		return ERR_NO_TRANSFER_COMPLETE;

	return OK;
}

static int
DiagDoHttpUpload ( char *URL, char *username, char *password, long fileSize, struct soap * tmpSoap )
{
	int ret = 0;
	const UploadFile *srcFile;
	unsigned int size;

	srcFile = type2file ("2 Vendor Log File", &size);

	tmpSoap->proxy_host = proxy_host[0] ? proxy_host : NULL;
	tmpSoap->proxy_port = proxy_port;
	tmpSoap->proxy_userid = proxy_userid[0] ? proxy_userid : NULL;
	tmpSoap->proxy_passwd = proxy_passwd[0] ? proxy_passwd : NULL;
	tmpSoap->proxy_http_version = proxy_version[0] ? proxy_version : NULL;
	tmpSoap->keep_alive = 1;
	tmpSoap->recv_timeout = 30;	// Timeout after 30 seconds stall on recv
	tmpSoap->send_timeout = 60;	// Timeout after 1 minute stall on send

	soap_set_endpoint(tmpSoap, URL);

	setTimeParamToCurrentTime( UploadDiagnostics_TCPOpenRequestTime );

	tmpSoap->socket = tmpSoap->fopen(tmpSoap, URL, tmpSoap->host, tmpSoap->port);
	if (tmpSoap->error)
		return ERR_INIT_CONNECTION_FAILED;

	setTimeParamToCurrentTime( UploadDiagnostics_TCPOpenResponseTime );
	setTimeParamToCurrentTime( UploadDiagnostics_ROMTime );

	pthread_testcancel();
	ret = DiagUploadFile( tmpSoap, srcFile, fileSize );

	if( ret != OK )
		return ret;

	pthread_testcancel();
	// Parse the HTML response header and remove it from the work data
	ret = tmpSoap->fparse (tmpSoap);
	if (ret != OK)
		return ERR_NO_RESPONSE;

	setTimeParamToCurrentTime( UploadDiagnostics_EOMTime );
	setUniversalParamValueInternal(NULL, UploadDiagnostics_TotalBytesSent, 1, (unsigned int)fileSize );

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Request 2 %d endpoint: %s\n", ret, tmpSoap->endpoint);
	)
	return OK;
}


static void
abortsend(int ignore)
{
	(void)ignore;

	mflag = 0;
	abrtflag = 0;
	// printf("\nsend aborted\nwaiting for remote to finish abort\n");
	(void) fflush(stdout);
	siglongjmp(sendabort, 1);
}

/** Send a file from local filename to remote
 * no restart support
 *
 * @return errorcode 	0 = OK , > 1 = Error
 */
static int
_sendrequest(const char *cmd, char *local, char *remote, long *size )
{
	register int d;
	FILE *volatile dout = 0;
	int (*volatile closefunc)(FILE *);
	void (*volatile oldintr)(int);
	void (*volatile oldintp)(int);
	volatile long bytes = 0;
	char buf[BUFSIZ];
	const char *volatile lmode;

	if (curtype != type)
		changetype(type, 0);
	closefunc = NULL;
	oldintr = NULL;
	oldintp = NULL;
	lmode = "w";
	if (sigsetjmp(sendabort, 1)) {
		while (cpend) {
			(void) getreply(0);
		}
		if (data >= 0) {
			(void) close(data);
			data = -1;
		}
		if (oldintr)
			(void) signal(SIGINT,oldintr);
		if (oldintp)
			(void) signal(SIGPIPE,oldintp);
		code = -1;
		return 1;
	}
	oldintr = signal(SIGINT, abortsend);
	closefunc = fclose;

	if (initconn()) {
		(void) signal(SIGINT, oldintr);
		if (oldintp)
			(void) signal(SIGPIPE, oldintp);
		code = -1;

		return 1;
	}
	if (sigsetjmp(sendabort, 1))
		goto abort;

	if (remote) {
		if (command("%s %s", cmd, remote) != PRELIM) {
			(void) signal(SIGINT, oldintr);
			if (oldintp)
				(void) signal(SIGPIPE, oldintp);

			return 1;
		}
	} else
		if (command("%s", cmd) != PRELIM) {
			(void) signal(SIGINT, oldintr);
			if (oldintp)
				(void) signal(SIGPIPE, oldintp);

			return 1;
		}
	dout = dataconn(lmode);
	if (dout == NULL)
		goto abort;
	oldintp = signal(SIGPIPE, SIG_IGN);
	switch (curtype) {

		case TYPE_I:
		case TYPE_L:
			errno = d = 0;
			long	i = *size;
			buf[0] = 'a';

			while( i-- )
				if ((d = write(fileno(dout), buf, 1)) <= 0)
					break;

			if (d < 0) {
				//			if (errno != EPIPE)
				//				;
				bytes = -1;
			}
			break;
	}

	(void) fclose(dout);
	/* closes data as well, so discard it */
	data = -1;
	(void) getreply(0);
	(void) signal(SIGINT, oldintr);
	if (oldintp)
		(void) signal(SIGPIPE, oldintp);
	return 0;
	abort:
	(void) signal(SIGINT, oldintr);
	if (oldintp)
		(void) signal(SIGPIPE, oldintp);
	if (!cpend) {
		code = -1;
		return 1;
	}
	if (dout) {
		(void) fclose(dout);
	}
	if (data >= 0) {
		/* if it just got closed with dout, again won't hurt */
		(void) close(data);
		data = -1;
	}
	(void) getreply(0);HAVE_DIAGNOSTICS
	code = -1;

	return 1;
}

/** Put a file to the remote host.
 *
 * \param remoteFile	remotePathname relative to ftp home or absolute
 * \param localFile		localPathname relative or better absolute
 * \return errorcode
 */
static	int
_dimput( char *remoteFile, char *localFile, long *size )
{
	return _sendrequest("STOR", localFile, remoteFile, size );
}

/** Put a file to the remote host.
 *
 * \param remoteFile	remotePathname relative to ftp home or absolute
 * \param localFile		localPathname relative or better absolute
 * \return errorcode
 */
static	int
_ftp_put( char *remoteFile, char *localFile, long *size )
{
	return _dimput( remoteFile, localFile, size );
}

static int
DiagDoFtpUpload (char *URL, char *username, char *password, long fileSize )
{
	// extract remote host and remote filename from URL
	// Only ftp://hostname[:port]/dir/dir/remotename  is allowed
	// the remote name must be relative to the ftp directory
	// If no remote name is found in the URL the srcFilename is used.
	// Attention: the complete srcFilename including path is used.

	int ret = OK;
	char *tmp;
	char *host;
	char *remoteFileName;
	unsigned int size;
	const UploadFile *srcFile;
	ParameterValue value;

	srcFile = type2file ("2 Vendor Log File", &size);

	// skip scheme "ftp://"
	host = URL + 6; // strstr( URL, "://" );
	tmp = host;
	while ( *tmp && *tmp != ':' && *tmp != '/' ) {
		tmp++;
	}
	*tmp = '\0';
	tmp++;
	if ( strlen( tmp ) == 0 )
		remoteFileName = srcFile->filename;
	else
		remoteFileName = tmp;
	if ((ret =  _ftp_login( host, username, password )) != OK )
		return ERR_LOGIN_FAILED;

	setTimeParamToCurrentTime( UploadDiagnostics_TCPOpenRequestTime );
	setTimeParamToCurrentTime( UploadDiagnostics_TCPOpenResponseTime );
	setTimeParamToCurrentTime( UploadDiagnostics_ROMTime );
	setTimeParamToCurrentTime( UploadDiagnostics_BOMTime );

	ret = _ftp_put( remoteFileName, srcFile->filepath, &fileSize );
	if ( ret != OK ) {
		ftp_disconnect();
		return ERR_NO_TRANSFER_COMPLETE;
	}

	setTimeParamToCurrentTime( UploadDiagnostics_EOMTime );
	setUniversalParamValueInternal(NULL, UploadDiagnostics_TotalBytesSent, 1, (unsigned int)fileSize );
	ftp_disconnect();
	return OK;
}

/** Function to handle a upload test
 */
static int cbUploadDiagnostics (struct soap * tmpSoap)
{
	char			*pUploadDiagnostics_UploadURL_str = NULL;
	unsigned int	*pUploadDiagnostics_TestFileLength_uint = NULL;

	char			UploadDiagnostics_UploadURL_str[URL_STR_LEN + 1] = {"\0"};
	unsigned int	UploadDiagnostics_TestFileLength_uint = 0;

	int ret = OK;

	getParameter( UploadDiagnostics_UploadURL, &pUploadDiagnostics_UploadURL_str );
	strncpy(UploadDiagnostics_UploadURL_str, pUploadDiagnostics_UploadURL_str, URL_STR_LEN);
	//getParameter( UploadDiagnostics_DSCP, &UploadDiagnostics_DSCP_uint );
	//getParameter( UploadDiagnostics_EthernetPriority, &UploadDiagnostics_EthernetPriority_uint );
	getParameter( UploadDiagnostics_TestFileLength, &pUploadDiagnostics_TestFileLength_uint );
	UploadDiagnostics_TestFileLength_uint = pUploadDiagnostics_TestFileLength_uint ? *pUploadDiagnostics_TestFileLength_uint : 0;

	if ( strnStartsWith( UploadDiagnostics_UploadURL_str, "http", 4 ) == 1)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Begin of HTTP Upload Diagnostic\n");
		)

		ret = DiagDoHttpUpload( UploadDiagnostics_UploadURL_str, "", "", UploadDiagnostics_TestFileLength_uint, tmpSoap );

		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DIAGNOSTIC, "End of HTTP Upload Diagnostic. Returned value = %d\n", ret);
		)
	}
	else
		if ( strnStartsWith( UploadDiagnostics_UploadURL_str, "ftp", 3 ) == 1)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Begin of FTP Upload Diagnostic\n");
			)

			ret = DiagDoFtpUpload( UploadDiagnostics_UploadURL_str, "", "", UploadDiagnostics_TestFileLength_uint );

			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DIAGNOSTIC, "End of FTP Upload Diagnostic. Returned value = %d\n", ret);
			)
		}
		else
			ret = ERR_NO_TRANSFER_MODE;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Exit from UploadDiagnostic: ret = %d, %s, %d\n", ret, UploadDiagnostics_UploadURL_str, UploadDiagnostics_TestFileLength_uint);
	)
	return ret;
}


static void uploadDiagnosticThreadExitCallback(void * arg)
{
	struct soap * tmpSoap = (struct soap *)arg;
	soap_closesock (tmpSoap);
	soap_destroy (tmpSoap);
	soap_end (tmpSoap);
	soap_done (tmpSoap);

	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "uploadDiagnosticThreadExitCallback() has been done. The allocated memory is freed.\n");
	)
}

static void * uploadDiagnosticStartFunk(void * param)
{
	int ret = OK;
	ParameterValue temp_value;

	struct soap soap;
	soap_init(&soap); //located memory for this soap struct will be freed in the uploadDiagnosticThreadExitCallback()

	pthread_cleanup_push(uploadDiagnosticThreadExitCallback, (void *)&soap);

	/*Next line can be used for immediate termination of diagnostic thread.
	  Default state = PTHREAD_CANCEL_DEFERRED.
	*/
	//pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &ret);

	ret = cbUploadDiagnostics(&soap);

	switch (ret)
	{
		case OK:
			temp_value.in_cval = DiagnosticsState_Completed;
			break;
		case ERR_NO_RESPONSE:
			temp_value.in_cval = DiagnosticsState_Error_NoResponse;
			break;
		case ERR_NO_TRANSFER_MODE:
			temp_value.in_cval = DiagnosticsState_Error_NoTransferMode;
			break;
		case ERR_INIT_CONNECTION_FAILED:
			temp_value.in_cval = DiagnosticsState_Error_InitConnectionFailed;
			break;
		case ERR_LOGIN_FAILED:
			temp_value.in_cval = DiagnosticsState_Error_LoginFailed;
			break;
		case ERR_NO_TRANSFER_COMPLETE:
		case ERR_UPLOAD_FAILURE:
		default:
			temp_value.in_cval = DiagnosticsState_Error_NoTransferComplete;
			break;
	}


	ret = updateParamValue(UploadDiagnostics_DiagnosticsState, StringType, &temp_value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "IpPingDiagnosticStartFunk(): updateParamValue() error\n");
		)
	}

	*((int *) param) = ret;

	pthread_exit(param);
	pthread_cleanup_pop(1);
	return NULL;
}

DiagnosticTypes getUploadDiagnosticInfo (DiagnosticStartFunk* pDiagMainFunc)
{
	if (pDiagMainFunc)
		*pDiagMainFunc = uploadDiagnosticStartFunk;
	return uploadDiagnostic;
}

#endif /* HAVE_UPLOAD_DIAGNOSTICS */

/** This function is called by setParameters()
	if data == Requested then add DownloadDiagnostics to DiagnosticsList.
	Diagnostic will be called after all requests are done.
*/
int setUploadDiagnostics( const char *name, ParameterType type, ParameterValue *data )
{
#ifdef HAVE_UPLOAD_DIAGNOSTICS
	int ret;
	if ( strncmp( *(char **)data, DiagnosticsState_Requested, strlen( *(char**)data ) ) == 0 )
	{
		// Store the value in the permanent storage
		ret = updateParamValue( name, type, data);
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_ACCESS, "setUploadDiagnostics(): updateParamValue(%s) error = %d\n", name, ret);
			)
			return ret;
		}
		addDiagnosticToDiagnosticsList(uploadDiagnostic, getUploadDiagnosticInfo);
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "setUploadDiagnostics(): Invalid parameter value, DiagnosticsState can be set to \"Requested\" only.\n");
		)
		return ERR_INVALID_PARAMETER_VALUE;
	}
#endif /* HAVE_UPLOAD_DIAGNOSTICS */
	return OK;
}


#ifdef HAVE_UDP_ECHO

#define MAX_UDPECHO_PACKET_SIZE		2048
#define UDPECHOPLUS_MIN_SIZE		20

extern	int procId;

static volatile unsigned int UDPEchoConfig_PacketsReceived_count = 0;
static volatile unsigned int UDPEchoConfig_BytesReceived_count = 0;
static volatile unsigned int UDPEchoConfig_PacketsResponded_count = 0;
static volatile unsigned int UDPEchoConfig_BytesResponded_count = 0;

static  volatile int 	UDPEchoConfig_isEnable;
static  struct sockaddr_in	UDPEcho_address =
{
	.sin_port = 0,
	.sin_addr.s_addr = INADDR_ANY,
	.sin_family = AF_INET
};
static  volatile int sockfd = 0;
static  volatile int sockStoppingIsActivated = 0;
static  volatile int isItFirstPacketAfterStartListener = 1;
static  volatile unsigned int TestRespSN = 0;
static  volatile unsigned int TestRespReplyFailureCount = 0;
static  struct timeval startTime;

static	int startServer();
static	void waitForConnections();

void terminateUDPEchoHandler(pthread_t udpEchoTid)
{
	if (udpEchoTid > 0)
	{
		pthread_cancel(udpEchoTid);
	}
	if (sockfd > 0)
	{
		shutdownUDPEchoSocket();
		close(sockfd);
		sockfd = 0;
	}
	/* NOTE: Considering that terminateUDPEchoHandler() is called from killAllThreads() only at the end of process
	 * and efreeAllTypedMem(0) will be called in any way the call of following function isn't needed.
	 * But if you want to call terminateUDPEchoHandler() from other place you must uncomment following string. */
	//efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
}

void *
udpEchoHandler(void *param)
{
	int	ret = OK;
	unsigned int *pUDPEchoConfig_UDPPort_uint = NULL;
	unsigned int *pUDPEchoConfig_isEnable_int = NULL;
	char *pUDPEchoConfig_Interface_string = NULL;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	sleep(10);
	pthread_mutex_lock(&informLock); // wait while inform occurs
	pthread_mutex_unlock(&informLock);

	if (getParameter( UDPEchoConfig_Enable, &pUDPEchoConfig_isEnable_int ) == OK)
		UDPEchoConfig_isEnable = pUDPEchoConfig_isEnable_int ? *pUDPEchoConfig_isEnable_int : 0;
	else
		UDPEchoConfig_isEnable = 0;

	memset(&UDPEcho_address, 0, sizeof(UDPEcho_address));
	UDPEcho_address.sin_family = AF_INET;

	if (getParameter( UDPEchoConfig_UDPPort, &pUDPEchoConfig_UDPPort_uint ) == OK)
		UDPEcho_address.sin_port = htons(pUDPEchoConfig_UDPPort_uint && *pUDPEchoConfig_UDPPort_uint ? *pUDPEchoConfig_UDPPort_uint : UDP_ECHO_DEFAULT_PORT + (procId));
	else
		UDPEcho_address.sin_port = htons(UDP_ECHO_DEFAULT_PORT + (procId));

	if (getParameter( UDPEchoConfig_Interface, &pUDPEchoConfig_Interface_string ) != OK)
	{
		UDPEcho_address.sin_addr.s_addr = INADDR_ANY; // use INADDR_ANY
	}
	else
	{
		if ( !pUDPEchoConfig_Interface_string || strlen(pUDPEchoConfig_Interface_string)==0 )
			UDPEcho_address.sin_addr.s_addr = INADDR_ANY; // use INADDR_ANY, if interface is empty string.
		else
		{
			struct sockaddr_in tmpAddr;
			char *ip;
			if ( (ip = getIPAddrByInterfaceName(pUDPEchoConfig_Interface_string, &tmpAddr)) == NULL )
				UDPEcho_address.sin_addr.s_addr = INADDR_ANY;
			else
				// copy new addr to static address struct.
				memcpy(&UDPEcho_address.sin_addr.s_addr, &tmpAddr.sin_addr.s_addr, sizeof(tmpAddr.sin_addr.s_addr));
		}
	}

	setUniversalParamValueInternal(NULL, UDPEchoConfig_PacketsReceived, 1, 0u );
	setUniversalParamValueInternal(NULL, UDPEchoConfig_PacketsResponded, 1, 0u );
	setUniversalParamValueInternal(NULL, UDPEchoConfig_BytesReceived, 1, 0u );
	setUniversalParamValueInternal(NULL, UDPEchoConfig_BytesResponded, 1, 0u );

	struct timeval t;
	t.tv_sec = t.tv_usec = 0;
	setUniversalParamValueInternal(NULL, UDPEchoConfig_TimeFirstPacketReceived, 1, &t );
	setUniversalParamValueInternal(NULL, UDPEchoConfig_TimeLastPacketReceived, 1, &t );


	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);

	while(1)
	{
		if (UDPEchoConfig_isEnable == 0)
		{
			usleep(100000); // sleep 0.1 sec and continue if DPEchoConfig isn't Enable
			continue;
		}

		pthread_mutex_lock(&informLock); //wait while inform is not completed
		pthread_mutex_unlock(&informLock);
		if ((sockfd = startServer()) == -1)
		{
			sleep(10); // sleep 10 sec and continue if error occurs when server tried to be started
			//TODO: for some integration project it may be needed to change actions in this case.
			continue;
		}
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DIAGNOSTIC, "UDP Echo Address: %s, port: %d. Binding is successful.\n", inet_ntoa(UDPEcho_address.sin_addr), ntohs(UDPEcho_address.sin_port));
		)

		// wait for incoming connections
		waitForConnections();
		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
	}

	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	pthread_exit(NULL);
	return NULL;
}

// 	returns the server socket, or -1 if we couldn't bind the socket
static int startServer()
{
	int sockfd_local;
	struct sockaddr_in address;

	errno = 0;
	// open the server socket
	if ((sockfd_local = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "startServer()-> socket() error: ret = -1, errno = %d (%s)\n", errno, strerror(errno));
		)
		return -1;
	}
	sockStoppingIsActivated = 0;

	// setup bind stuffs
	memcpy(&address, &UDPEcho_address, sizeof(address));

	int one = 1;
	errno = 0;
	if (setsockopt(sockfd_local, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one)) != 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC,"startServer()->setsockopt(SO_REUSEADD) has returned -1, errno = %d (%s)\n", errno, strerror(errno));
		)
		close(sockfd_local);
		return -1;
	}

	errno = 0;
	// bind the socket
	if (bind(sockfd_local, (struct sockaddr *)&address, sizeof(address)) != 0 )
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "startServer()-> bind() has returned -1, errno = %d (%s)\n", errno, strerror(errno));
		)
		close(sockfd_local);
		return -1;
	}

	gettimeofday(&startTime, NULL);

	// return the socket
	return sockfd_local;
}

// 	waits in an infinite loop for sockect connections, then passes them to handleConnection
static	void
waitForConnections()
{
	int ret;
	char buffer[MAX_UDPECHO_PACKET_SIZE+1];
	int size = 0;

	unsigned int *tmp_uint_pointer = NULL;
	char tmpUIntStr[11];
	unsigned int *pUDPEchoConfig_EchoPlusEnabled_int = NULL;
	unsigned int isEchoPlusEnabled;
	char *pStrUDPEchoConfig_SourceIPAddress = NULL;
	int doNotHandlePacket;

	struct sockaddr_in remoteAddress;
	int remoteAddressSize;

	struct timeval requestTime, responseTime;
	long int usec;

	typedef struct
	{
		unsigned int TestGenSN;
		unsigned int TestRespSN;
		unsigned int TestRespRecvTimeStamp;
		unsigned int TestRespReplyTimeStamp;
		unsigned int TestRespReplyFailureCount;
		char Data[MAX_UDPECHO_PACKET_SIZE + 1 - 20];
	} Pack;

	Pack	*pack;
	pack = (Pack*) &buffer;

	if (isItFirstPacketAfterStartListener)
	{
		TestRespSN = 0;
		TestRespReplyFailureCount = 0;
	}

	for (;;)
	{
		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
		// read the client message
		do {
			memset(buffer, 0, MAX_UDPECHO_PACKET_SIZE+1);
			remoteAddressSize = sizeof(remoteAddress);
			memset(&remoteAddress, 0, remoteAddressSize);
			size = recvfrom(sockfd, buffer, MAX_UDPECHO_PACKET_SIZE, 0, (struct sockaddr *)&remoteAddress, (socklen_t *)&remoteAddressSize);
			gettimeofday(&requestTime, NULL);
			if (size == 0 && sockStoppingIsActivated)
			{
				if (sockfd)
				{
					close(sockfd);
					sockfd = 0;
				}
				return;
			}
			ret = getParameter( UDPEchoConfig_SourceIPAddress, &pStrUDPEchoConfig_SourceIPAddress );
			if (ret != OK)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "waitForConnections()->getParameter(%s): has returned %d.\n", UDPEchoConfig_SourceIPAddress, ret);
				)
				TestRespReplyFailureCount++;
				efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
				continue;
			}
			doNotHandlePacket = ( !UDPEchoConfig_isEnable || !pStrUDPEchoConfig_SourceIPAddress ||
					( inet_addr(pStrUDPEchoConfig_SourceIPAddress) != remoteAddress.sin_addr.s_addr )
				  );
			if (doNotHandlePacket)
				TestRespReplyFailureCount++;

			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DIAGNOSTIC, "UDPEcho: packet is received from IP = [%s], size = %d\n", inet_ntoa(remoteAddress.sin_addr), size);
			)
			efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
		} while (size <= 0 || doNotHandlePacket);

		if (strlen(buffer) == size) // else the '\0' symbols are in the packet
		{
			// print the client message and what we're doing
			DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_DIAGNOSTIC, "UDPEcho client said: \"%s\"\n", buffer);
				dbglog (SVR_DEBUG, DBG_DIAGNOSTIC, "Echoing...\n");
			)
		}

		if (size > UDPECHOPLUS_MIN_SIZE) // if packet may be UDPEcho Plus packet
		{
			if (getParameter( UDPEchoConfig_EchoPlusEnabled, &pUDPEchoConfig_EchoPlusEnabled_int ) == OK)
				isEchoPlusEnabled = pUDPEchoConfig_EchoPlusEnabled_int ? *pUDPEchoConfig_EchoPlusEnabled_int : 0;
			else
				isEchoPlusEnabled = 0;

			if (isEchoPlusEnabled)
			{
				//pack->TestGenSN is left unmodified in the response.
				TestRespSN++;
				pack->TestRespSN = TestRespSN;
				usec = (requestTime.tv_sec - startTime.tv_sec)*1000000L + (requestTime.tv_usec - startTime.tv_usec);
				pack->TestRespRecvTimeStamp = (unsigned int) (usec & 0xFFFFFFFF);
				gettimeofday(&responseTime, NULL);
				usec = (responseTime.tv_sec - startTime.tv_sec)*1000000L + (responseTime.tv_usec - startTime.tv_usec);
				pack->TestRespReplyTimeStamp = (unsigned int) (usec & 0xFFFFFFFF);
				pack->TestRespReplyFailureCount = TestRespReplyFailureCount;
			}
		}
		// echo the client message
		sendto(sockfd, buffer, size, 0, (struct sockaddr *)&remoteAddress, (socklen_t)remoteAddressSize);

		if (isItFirstPacketAfterStartListener)
		{
			isItFirstPacketAfterStartListener = 0;
			setTimeParamToCurrentTime( UDPEchoConfig_TimeFirstPacketReceived );
		}

		setTimeParamToCurrentTime( UDPEchoConfig_TimeLastPacketReceived );

		UDPEchoConfig_PacketsReceived_count++;
		setUniversalParamValueInternal(NULL, UDPEchoConfig_PacketsReceived, 1, UDPEchoConfig_PacketsReceived_count );

		UDPEchoConfig_BytesReceived_count += (size + 8); // 8 is size of UDPHeader
		setUniversalParamValueInternal(NULL, UDPEchoConfig_BytesReceived, 1, UDPEchoConfig_BytesReceived_count );

		UDPEchoConfig_PacketsResponded_count++;
		setUniversalParamValueInternal(NULL, UDPEchoConfig_PacketsResponded, 1, UDPEchoConfig_PacketsResponded_count );

		UDPEchoConfig_BytesResponded_count += (size + 8); // 8 is size of UDPHeader
		setUniversalParamValueInternal(NULL, UDPEchoConfig_BytesResponded, 1, UDPEchoConfig_BytesResponded_count );
	}
}

void shutdownUDPEchoSocket()
{
	if (sockfd)
	{
		sockStoppingIsActivated = 1;
		errno = 0;
		shutdown(sockfd,SHUT_RDWR);
	}
}

/* From standard:
 * MUST be enabled to receive UDP echo. When enabled from a disabled state all related timestamps,
 * statistics and UDP Echo Plus counters are cleared.
 */
int updateUDPEchoConfigEnable(int isEnable)
{
	isEnable = (isEnable > 0) ? 1 : 0;
	if ( UDPEchoConfig_isEnable == isEnable )
		return OK; // don't change anything if the addresses are equal.

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "UDPEchoConfig.Enable is changed. New value = %d\n", isEnable);
	)

	if (!isEnable)
		shutdownUDPEchoSocket(); // for disconnect the socket
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_DEBUG, DBG_DIAGNOSTIC, "updateUDPEchoConfigEnable(): cleaning of UDPEchoConfig parameter values is started.\n", UDPEchoConfig_isEnable);
		)

		isItFirstPacketAfterStartListener = 1;
		TestRespSN = 0;
		TestRespReplyFailureCount = 0;

		// cleaning:
		UDPEchoConfig_PacketsReceived_count = 0;
		UDPEchoConfig_BytesReceived_count = 0;
		UDPEchoConfig_PacketsResponded_count = 0;
		UDPEchoConfig_BytesResponded_count = 0;


		setUniversalParamValueInternal(NULL, UDPEchoConfig_PacketsReceived, 1, 0u );
		setUniversalParamValueInternal(NULL, UDPEchoConfig_PacketsResponded, 1, 0u );
		setUniversalParamValueInternal(NULL, UDPEchoConfig_BytesReceived, 1, 0u );
		setUniversalParamValueInternal(NULL, UDPEchoConfig_BytesResponded, 1, 0u );
		struct timeval t;
		t.tv_sec = t.tv_usec = 0;
		setUniversalParamValueInternal(NULL, UDPEchoConfig_TimeFirstPacketReceived, 1, &t );
		setUniversalParamValueInternal(NULL, UDPEchoConfig_TimeLastPacketReceived, 1, &t );
	}
	UDPEchoConfig_isEnable = isEnable;
	return OK;
}


/* From standard:
 * The value MUST be the path name of IP-layer interface over which the CPE MUST listen and receive UDP echo requests on.
 * The value of this parameter MUST be either a valid interface or an empty string. An attempt to set this parameter to a different value MUST be rejected as an invalid parameter value.
 * If an empty string is specified, the CPE MUST listen and receive UDP echo requests on all interfaces.
 */
int updateUDPEchoConfigInterface(char *newInterface)
{
	struct sockaddr_in tmpAddr;

	if (!newInterface)
		return ERROR_INVALID_INTERFACE;

	if ( strlen(newInterface) == 0 )
	{
		UDPEcho_address.sin_addr.s_addr = INADDR_ANY; // use INADDR_ANY, if interface is empty string.
	}
	else
	{
		if ( getIPAddrByInterfaceName(newInterface, &tmpAddr) == NULL )
			return ERROR_INVALID_INTERFACE;

		if ( memcmp(&UDPEcho_address.sin_addr.s_addr, &tmpAddr.sin_addr.s_addr, sizeof(tmpAddr.sin_addr.s_addr)) == 0 )
			return OK; // don't change anything if the addresses are equal.

		// copy new addr to static address struct.
		memcpy(&UDPEcho_address.sin_addr.s_addr, &tmpAddr.sin_addr.s_addr, sizeof(tmpAddr.sin_addr.s_addr));
	}
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "UDPEchoConfig.Interface is changed to \"%s\", new address is \"%s\"\n", newInterface, inet_ntoa(UDPEcho_address.sin_addr));
	)

	shutdownUDPEchoSocket(); // for reinstall the new socket

	return OK;
}

/* From standard:
 * The UDP port on which the UDP server MUST listen and respond to UDP echo requests.
 */
int updateUDPEchoConfigPort(unsigned int newPort)
{
	if (!newPort)
		newPort = UDP_ECHO_DEFAULT_PORT + procId;

	if ( UDPEcho_address.sin_port == htons(newPort) )
		return OK; // don't change anything if the addresses are equal.

	UDPEcho_address.sin_port = htons(newPort);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "UDPEchoConfig.UDPPort is changed. New port = %d\n", newPort);
	)

	shutdownUDPEchoSocket(); // for reinstall the new socket

	return OK;
}
#endif /* HAVE_UDP_ECHO */


#ifdef HAVE_DOWNLOAD_DIAGNOSTICS

static int cbDownloadDiagnostics (struct soap *);

/** Function to handle a download test
 */
static int cbDownloadDiagnostics (struct soap *tmpSoap)
{
	char	*pDownloadDiagnostics_DownloadURL_str = NULL;
	char	DownloadDiagnostics_DownloadURL_str[URL_STR_LEN+1] = {"\0"};
	int 	ret = OK;

	getParameter( DownloadDiagnostics_DownloadURL, &pDownloadDiagnostics_DownloadURL_str );
	strncpy(DownloadDiagnostics_DownloadURL_str, pDownloadDiagnostics_DownloadURL_str, URL_STR_LEN);
	//getParameter( DownloadDiagnostics_DSCP, &DownloadDiagnostics_DSCP_uint );
	//getParameter( DownloadDiagnostics_EthernetPriority, &DownloadDiagnostics_EthernetPriority_uint );

	if (strnStartsWith(DownloadDiagnostics_DownloadURL_str, "http", 4) == 1
			|| strnStartsWith(DownloadDiagnostics_DownloadURL_str, "https", 5) == 1)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Begin of HTTP Download Diagnostic\n");
		)

		ret = DiagDoHttpDownload( DownloadDiagnostics_DownloadURL_str, "", "", 0, HTTP_DOWNLOAD_DIAGNOSTICS_FILE, tmpSoap );
		remove( HTTP_DOWNLOAD_DIAGNOSTICS_FILE );

		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DIAGNOSTIC, "End of HTTP Download Diagnostic. Returned value = %d\n", ret);
		)
	}
	else
		if ( strnStartsWith( DownloadDiagnostics_DownloadURL_str, "ftp", 3 ) == 1)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Begin of FTP Download Diagnostic\n");
			)

			ret = DiagDoFtpDownload( DownloadDiagnostics_DownloadURL_str, "", "", 0, FTP_DOWNLOAD_DIAGNOSTICS_FILE );
			remove( FTP_DOWNLOAD_DIAGNOSTICS_FILE );

			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DIAGNOSTIC, "End of FTP Download Diagnostic. Returned value = %d\n", ret);
			)
		}
		else
		{
			setUniversalParamValueInternal(NULL, DownloadDiagnostics_DiagnosticsState, 1, DiagnosticsState_Error_InitConnectionFailed);
		}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Exit from DownloadDiagnostic: %s\n", DownloadDiagnostics_DownloadURL_str);
	)

	return ret;
}


static void downloadDiagnosticThreadExitCallback(void * arg)
{
	struct soap * tmpSoap = (struct soap *)arg;
	soap_closesock (tmpSoap);
	soap_destroy (tmpSoap);
	soap_end (tmpSoap);
	soap_done (tmpSoap);

	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "downloadDiagnosticThreadExitCallback() has been done. The allocated memory is freed.\n");
	)
}

static void * downloadDiagnosticStartFunk(void * param)
{
	int ret = OK;

	struct soap soap;
	soap_init(&soap); //located memory for this soap struct will be freed in the downloadDiagnosticThreadExitCallback()

	pthread_cleanup_push(downloadDiagnosticThreadExitCallback, (void *)&soap);

	/*Next line can be used for immediate termination of diagnostic thread.
	  Default state = PTHREAD_CANCEL_DEFERRED.
	*/
	//pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &ret);

	ret = cbDownloadDiagnostics(&soap);

	*((int *) param) = ret;
	pthread_exit(param);
	pthread_cleanup_pop(1);
	return NULL;
}

DiagnosticTypes getDownloadDiagnosticInfo (DiagnosticStartFunk* pDiagMainFunc)
{
	if (pDiagMainFunc)
		*pDiagMainFunc = downloadDiagnosticStartFunk;
	return downloadDiagnostic;
}

#endif

/** This function is called by setParameters()
	if data == Requested then add DownloadDiagnostics to DiagnosticsList.
	Diagnostic will be called after all requests are done.
*/
int setDownloadDiagnostics( const char *name, ParameterType type, ParameterValue *data )
{
#ifdef HAVE_DOWNLOAD_DIAGNOSTICS
	int ret;
	if ( strncmp( *(char **)data, DiagnosticsState_Requested, strlen( *(char**)data ) ) == 0 )
	{
		// Store the value in the permanent storage
		ret = updateParamValue( name, type, data);
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_ACCESS, "setDownloadDiagnostics(): updateParamValue(%s) error = %d\n",name, ret);
			)
			return ret;
		}
		addDiagnosticToDiagnosticsList(downloadDiagnostic, getDownloadDiagnosticInfo);
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "setDownloadDiagnostics(): Invalid parameter value, DiagnosticsState can be set to \"Requested\" only.\n");
		)
		return ERR_INVALID_PARAMETER_VALUE;
	}
#endif
	return OK;
}

#ifdef HAVE_IP_PING_DIAGNOSTICS

#ifndef IPPING_COMMAND
  #define IPPING_COMMAND "ping" //default ping command. Can be set in the integrationSettings.h
#endif

#ifndef DOES_PING_SUPPORT_DSCP_ATTR
  #define DOES_PING_SUPPORT_DSCP_ATTR 1 //set 0 or 1: does ping command support DSCP attribute?
#endif

static int cbIPPing (void);

/** Function to handle a IP Ping test
 */
static int cbIPPing (void)
{
	int ret;

	unsigned int numberOfRepetitions = 0, timeout = 3000, dataBlockSize = 58, _DSCP = 0, SuccessCount, FailureCount, AverageResponseTime,
			MinimumResponseTime,MaximumResponseTime;
	static float fl_AverageResponseTime,fl_MinimumResponseTime,fl_MaximumResponseTime;
	char command[128] = {'\0'};
	int isUnknownHost = 1;
	char interfacePath[257] = {'\0'};
	char interfaceName[257] = {'\0'};

	char * tmp_str = NULL;
	unsigned int * tmp_uint = NULL;
	char	line[200];	/* input line buffer */

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "cbIPPing(): Start IPPing Diagnostic\n");
	)

	memset(&interfacePath, 0, sizeof(interfacePath));
#ifdef IPPingDiagnostics_Interface
	//Retrieving values:
	ret = getParameter( IPPingDiagnostics_Interface, &tmp_str);
	if (ret != OK || !tmp_str)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_DIAGNOSTIC, "cbIPPing(): getParameter(%s) error, ret = %d, tmp_str=%s\n",IPPingDiagnostics_Interface, ret, tmp_str);
		)
	}
	else
		strcpy(interfacePath, tmp_str);

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "cbIPPing(): interfacePath = %s\n", interfacePath);
	)
#endif

	memset(&interfaceName, 0, sizeof(interfaceName));
	if (strlen(interfacePath) > 0)
	{
#ifdef TR_181_DEVICE
		strcat(interfacePath,".Name");
#else
		if (NULL != strstr(interfacePath,".WANDevice."))
		{
			strcat(interfacePath,".Name");

		}
		else if (NULL != strstr(interfacePath,".LANDevice."))
		{
			strcat(interfacePath,".IPInterfaceIPAddress");
		}
#endif
		ret = getParameter(interfacePath , &tmp_str);
		if (ret != OK || !tmp_str)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_WARN, DBG_DIAGNOSTIC, "cbIPPing(): getParameter(%s) error, ret = %d, tmp_str=%s\n",interfacePath, ret, tmp_str);
			)
		}
		else
			strcpy(interfaceName, tmp_str);
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "cbIPPing(): interfaceName = %s\n", interfaceName);
	)

	ret = getParameter( IPPingDiagnostics_Host, &tmp_str );
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbIPPing(): getParameter(%s) error\n",IPPingDiagnostics_Host);
		)
		return ERR_INTERNAL_ERROR;
	}
	char hostName[strlen(tmp_str)+1];
	strcpy(hostName, tmp_str);

	ret = getParameter( IPPingDiagnostics_NumberOfRepetitions, &tmp_uint );
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbIPPing(): getParameter(%s) error\n",IPPingDiagnostics_NumberOfRepetitions);
		)
		return ERR_INTERNAL_ERROR;
	}
	numberOfRepetitions = *tmp_uint;

	ret = getParameter( IPPingDiagnostics_Timeout, &tmp_uint );
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbIPPing(): getParameter(%s) error\n",IPPingDiagnostics_Timeout);
		)
		return ERR_INTERNAL_ERROR;
	}
	timeout = *tmp_uint;

	ret = getParameter( IPPingDiagnostics_DataBlockSize, &tmp_uint );
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbIPPing(): getParameter(%s) error\n",IPPingDiagnostics_DataBlockSize);
		)
		return ERR_INTERNAL_ERROR;
	}
	dataBlockSize = *tmp_uint;

	if (DOES_PING_SUPPORT_DSCP_ATTR)
	{
		ret = getParameter( IPPingDiagnostics_DSCP, &tmp_uint );
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbIPPing(): getParameter(%s) error\n",IPPingDiagnostics_DSCP);
			)
			return ERR_INTERNAL_ERROR;
		}
		_DSCP = *tmp_uint;
	}


	if (strlen(interfaceName) == 0)
	{
		if (DOES_PING_SUPPORT_DSCP_ATTR)
			sprintf(command, "%s -q -c %u -W %u -s %u -Q %u %s", IPPING_COMMAND, numberOfRepetitions, timeout/1000, dataBlockSize, _DSCP, hostName);
		else
			sprintf(command, "%s -q -c %u -W %u -s %u %s", IPPING_COMMAND, numberOfRepetitions, timeout/1000, dataBlockSize, hostName);
	}
	else
	{
		if (DOES_PING_SUPPORT_DSCP_ATTR)
			sprintf(command, "%s -q -I %s -c %u -W %u -s %u -Q %u %s", IPPING_COMMAND, interfaceName, numberOfRepetitions, timeout/1000, dataBlockSize, _DSCP, hostName);
		else
			sprintf(command, "%s -q -I %s -c %u -W %u -s %u %s", IPPING_COMMAND, interfaceName, numberOfRepetitions, timeout/1000, dataBlockSize, hostName);
	}
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Executing ping command: %s\n", command);
	)

	pthread_testcancel();
	FILE* fp = popen(command,"r");

	if(!fp)
	{
		DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbIPPing(): popen() error\n");
		)
		return ERR_INTERNAL_ERROR;
	}
	SuccessCount = 0;
	FailureCount = 0;
	MinimumResponseTime = 0;
	AverageResponseTime = 0;
	MaximumResponseTime = 0;
	int transmitted=0;
	while( fgets(line, sizeof(line), fp))
	{
		if (NULL != strstr(line,"packets transmitted"))
		{
			isUnknownHost = 0;
			sscanf(line,"%u packets transmitted, %u received, %*", &transmitted, &SuccessCount);
			FailureCount = transmitted - SuccessCount;
		}
		if ((SuccessCount>0) && (NULL != strstr(line,"rtt min/avg/max/mdev =")))
		{
			fl_MinimumResponseTime=0;
			fl_AverageResponseTime=0;
			fl_MaximumResponseTime=0;
			sscanf(line,"rtt min/avg/max/mdev = %f/%f/%f/%*", &fl_MinimumResponseTime, &fl_AverageResponseTime, &fl_MaximumResponseTime);
			MinimumResponseTime = uiRoundf(fl_MinimumResponseTime);
			AverageResponseTime = uiRoundf(fl_AverageResponseTime);
			MaximumResponseTime = uiRoundf(fl_MaximumResponseTime);
		}
	}
	pclose(fp);
	pthread_testcancel();

	if (isUnknownHost == 1)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbIPPing(): Cannot resolve host name\n");
		)
		return ISUNKNOWNHOST;
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Ping SuccessCount=%u, FailureCount=%u\n",SuccessCount,FailureCount);
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "MinimumResponseTime=%u ms, AverageResponseTime=%u ms, MaximumResponseTime=%u ms\n", MinimumResponseTime, AverageResponseTime, MaximumResponseTime);
	)

	ret = setUniversalParamValueInternal(NULL, IPPingDiagnostics_SuccessCount, 1, SuccessCount);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbIPPing(): setUniversalParamValueInternal(%s) error, ret=%d\n", IPPingDiagnostics_SuccessCount, ret);
		)
		return ERR_INTERNAL_ERROR;
	}

	ret = setUniversalParamValueInternal(NULL, IPPingDiagnostics_FailureCount, 1, FailureCount);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbIPPing(): setUniversalParamValueInternal(%s), ret=%d\n", IPPingDiagnostics_FailureCount, ret);
		)
		return ERR_INTERNAL_ERROR;
	}

	ret = setUniversalParamValueInternal(NULL, IPPingDiagnostics_MinimumResponseTime, 1, MinimumResponseTime);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbIPPing(): setUniversalParamValueInternal(%s), ret=%d\n", IPPingDiagnostics_MinimumResponseTime, ret);
		)
		return ERR_INTERNAL_ERROR;
	}

	ret = setUniversalParamValueInternal(NULL, IPPingDiagnostics_AverageResponseTime, 1, AverageResponseTime);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbIPPing(): setUniversalParamValueInternal(%s), ret=%d\n", IPPingDiagnostics_AverageResponseTime, ret);
		)
		return ERR_INTERNAL_ERROR;
	}

	ret = setUniversalParamValueInternal(NULL, IPPingDiagnostics_MaximumResponseTime, 1, MaximumResponseTime);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbIPPing(): setUniversalParamValueInternal(%s), ret=%d\n", IPPingDiagnostics_MaximumResponseTime, ret);
		)
		return ERR_INTERNAL_ERROR;
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Exit from cbIPPing()\n");
	)
	return OK;
}


static void IpPingDiagnosticThreadExitCallback(void * arg)
{
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "IpPingDiagnosticThreadExitCallback() has been done. The allocated memory is freed.\n");
	)
}

static void * IpPingDiagnosticStartFunk(void * param)
{
	int ret = OK;
	ParameterValue temp_value;

	pthread_cleanup_push(IpPingDiagnosticThreadExitCallback, NULL);

	/*Next line can be used for immediate termination of diagnostic thread.
	  Default state = PTHREAD_CANCEL_DEFERRED.
	*/
	//pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &ret);

	ret = cbIPPing();

	switch (ret)
	{
		case OK:
			temp_value.in_cval = IPPingDiagnosticState_Complete;
			break;
		case ERR_INTERNAL_ERROR:
			temp_value.in_cval = IPPingDiagnosticState_InternalError;
			break;
		case ISUNKNOWNHOST:
			temp_value.in_cval = IPPingDiagnosticState_CannotResolveHostNameError;
			break;
		default:
			temp_value.in_cval = IPPingDiagnosticState_OtherError;
			break;
	}

	ret = updateParamValue(IPPingDiagnostics_DiagnosticsState, StringType, &temp_value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "IpPingDiagnosticStartFunk(): updateParamValue() error\n");
		)
	}

	*((int *) param) = ret;
	pthread_exit(param);
	pthread_cleanup_pop(1);
	return NULL;
}

DiagnosticTypes getIpPingDiagnosticInfo (DiagnosticStartFunk* pDiagMainFunc)
{
	if (pDiagMainFunc)
		*pDiagMainFunc = IpPingDiagnosticStartFunk;
	return ipPingDiagnostic;
}
#endif

/** This function is called by setParameters()
	if data == Requested then add ipPingDiagnostic to DiagnosticsList.
	Diagnostic will be called after all requests are done.
*/
int setIPPingDiagnostics( const char *name, ParameterType type, ParameterValue *data )
{
#ifdef HAVE_IP_PING_DIAGNOSTICS
	int ret;
	if ( strncmp( *(char **)data, IPPingDiagnosticState_Requested, strlen( *(char**)data ) ) == 0 )
	{
		// Store the value in the permanent storage
		ret = updateParamValue( name, type, data);
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_ACCESS, "setIPPingDiagnostics(): updateParamValue() error = %d\n",name, ret);
			)
			return ret;
		}
		addDiagnosticToDiagnosticsList(ipPingDiagnostic, getIpPingDiagnosticInfo);
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "setIPPingDiagnostics(): Invalid parameter value, DiagnosticsState can be set to \"Requested\" only.\n");
		)
		return ERR_INVALID_PARAMETER_VALUE;
	}
#endif
	return OK;
}

#ifdef HAVE_TRACE_ROUTE_DIAGNOSTICS

#ifndef TRACE_ROUTE_COMMAND
  #define TRACE_ROUTE_COMMAND "/usr/sbin/traceroute" //default traceroute command. Can be set in the integrationSettings.h
#endif

#ifndef DOES_TRACE_ROUTE_SUPPORT_DSCP_ATTR
  #define DOES_TRACE_ROUTE_SUPPORT_DSCP_ATTR 1 //set 0 or 1: does ping command support DSCP attribute?
#endif

typedef enum tTraceRouteProtocol
{
	ICMP,
	UDP,
	TCP
} TraceRouteProtocol;

static int cbTraceRoute (void);

/** Function to handle a traceroute test
 */
static int cbTraceRoute(void)
{
	int i;
	int ret;
	struct ArrayOfParameterInfoStruct ParameterList = {NULL, 0};

	unsigned int numberOfTries, timeout_msec, timeout_sec, dataBlockSize, _DSCP, maxHopCount, routeHopsNumber;
	long int responseTime;
	double double_responseTime;
	char command[128] = {'\0'};
	int isUnknownHost;
	char line[400];
	char * protocol = NULL;
	char * tmpStr = NULL;

	char interfacePath[257] = {'\0'};
	char interfaceName[257] = {'\0'};

	struct timeval tv1,tv2,dtv;
	struct timezone tz;

	float  floatTimeOut;

	char paramName[256] = {'\0'};

	int traceRouteIsStarted = 0;
	char * linePtr = line;
	size_t size = sizeof(line);
	ssize_t len;

#ifdef TRACE_ROUTE_PROTOCOL
	TraceRouteProtocol traceRouteProtocol = TRACE_ROUTE_PROTOCOL; //You can set a TRACE_ROUTE_PROTOCOL in the integrationSettings.h.
#else
	TraceRouteProtocol traceRouteProtocol = UDP;
#endif

	// default values
	numberOfTries = 3;
	dataBlockSize = 38;
	timeout_msec = 5000;
	_DSCP = 0;
	maxHopCount = 30;

	char * tmp_str = NULL;
	unsigned int * tmp_uint = NULL;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "cbTraceRoute(): Start TraceRoute Diagnostic\n");
	)

	//Retrieving values:
	memset(&interfacePath, 0, sizeof(interfacePath));
#ifdef TraceRouteDiagnostics_Interface
	ret = getParameter( TraceRouteDiagnostics_Interface, &tmp_str);
	if (ret != OK || !tmp_str)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_DIAGNOSTIC, "cbTraceRoute(): getParameter(%s) error, ret = %d, tmp_str=%s\n",TraceRouteDiagnostics_Interface, ret, tmp_str);
		)
	}
	else
		strcpy(interfacePath, tmp_str);

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "cbTraceRoute(): interfacePath = %s\n", interfacePath);
	)
#endif

	memset(&interfaceName, 0, sizeof(interfaceName));
	if (strlen(interfacePath) > 0)
	{
#ifdef TR_181_DEVICE
		strcat(interfacePath,".Name");
#else
		if (NULL != strstr(interfacePath,".WANDevice."))
		{
			strcat(interfacePath,".Name");

		}
		else if (NULL != strstr(interfacePath,".LANDevice."))
		{
			strcat(interfacePath,".IPInterfaceIPAddress");
		}
#endif
		ret = getParameter(interfacePath , &tmp_str);
		if (ret != OK || !tmp_str)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_WARN, DBG_DIAGNOSTIC, "cbTraceRoute(): getParameter(%s) error, ret = %d, tmp_str=%s\n",interfacePath, ret, tmp_str);
			)
		}
		else
			strcpy(interfaceName, tmp_str);
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "cbTraceRoute(): interfaceName = %s\n", interfaceName);
	)

	ret = getParameter( TraceRouteDiagnostics_Host, &tmp_str );
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbTraceRoute(): getParameter(%s) error, ret = %d\n",TraceRouteDiagnostics_Host, ret);
		)
		return ERR_INTERNAL_ERROR;
	}
	char hostName[strlen(tmp_str)+1];
	strcpy(hostName, tmp_str);

	numberOfTries = 3; // default by standard
#ifdef TraceRouteDiagnostics_NumberOfTries
	ret = getParameter( TraceRouteDiagnostics_NumberOfTries, &tmp_uint );
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_DIAGNOSTIC, "cbTraceRoute(): getParameter(%s) error, ret = %d\n",TraceRouteDiagnostics_NumberOfTries, ret);
		)
	}
	else
		numberOfTries = *tmp_uint;
#endif

	ret = getParameter( TraceRouteDiagnostics_Timeout, &tmp_uint );
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbTraceRoute(): getParameter(%s) error, ret = %d\n",TraceRouteDiagnostics_Timeout, ret);
		)
		return ERR_INTERNAL_ERROR;
	}
	timeout_msec = *tmp_uint;

	ret = getParameter( TraceRouteDiagnostics_DataBlockSize, &tmp_uint );
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbTraceRoute(): getParameter(%s) error, ret = %d\n",TraceRouteDiagnostics_DataBlockSize, ret);
		)
		return ERR_INTERNAL_ERROR;
	}
	dataBlockSize = *tmp_uint;

	if (DOES_TRACE_ROUTE_SUPPORT_DSCP_ATTR)
	{
		ret = getParameter( TraceRouteDiagnostics_DSCP, &tmp_uint );
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbTraceRoute(): getParameter(%s) error, ret = %d\n",TraceRouteDiagnostics_DSCP, ret);
			)
			return ERR_INTERNAL_ERROR;
		}
		_DSCP = *tmp_uint;
	}

	ret = getParameter( TraceRouteDiagnostics_MaxHopCount, &tmp_uint );
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbTraceRoute(): getParameter(%s) error, ret = %d\n",TraceRouteDiagnostics_MaxHopCount, ret);
		)
		return ERR_INTERNAL_ERROR;
	}
	maxHopCount = *tmp_uint;

	deleteAllInstancesOfMultiObject(NULL, TraceRouteDiagnostics_RouteHopsObject, CALL_USER_DEFINED_CALLBACK, 1);

	// Preparation of 'traceroute' command with parameters
	switch (traceRouteProtocol)
	{
		case ICMP:
			protocol = "-I";
			break;
		case TCP:
			protocol = "-T";
			break;
		default:
			protocol = "";
			break;
	}

	floatTimeOut = timeout_msec / 1000.0;
	timeout_sec = (floatTimeOut < 1.0) ? 1 : iRoundf(floatTimeOut);

	if ( numberOfTries > 10 ) // The command traceroute in Linux Suse does not support a "retry value" > 10
	{
		numberOfTries = 10;
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DIAGNOSTIC, "cbTraceRoute(): The command 'traceroute' in Linux Suse does not support a 'retry value' > 10. Set numberOfTries to 10.\n");
		)
	}


	if (strlen(interfaceName) == 0)
	{
		if (DOES_TRACE_ROUTE_SUPPORT_DSCP_ATTR)
			sprintf(command, "%s %s -m %u -q %u -t %u -w %u %s %u", TRACE_ROUTE_COMMAND, protocol, maxHopCount, numberOfTries, _DSCP, timeout_sec,  hostName, dataBlockSize);
		else
			sprintf(command, "%s %s -m %u -q %u -w %u %s %u", TRACE_ROUTE_COMMAND, protocol, maxHopCount, numberOfTries, timeout_sec,  hostName, dataBlockSize);
	}
	else
	{
		if (DOES_TRACE_ROUTE_SUPPORT_DSCP_ATTR)
			sprintf(command, "%s %s -i %s -m %u -q %u -t %u -w %u %s %u", TRACE_ROUTE_COMMAND, protocol, interfaceName, maxHopCount, numberOfTries, _DSCP, timeout_sec,  hostName, dataBlockSize);
		else
			sprintf(command, "%s %s -i %s -m %u -q %u -w %u %s %u", TRACE_ROUTE_COMMAND, protocol, interfaceName, maxHopCount, numberOfTries, timeout_sec,  hostName, dataBlockSize);
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Executing traceroute command:  \"%s\"\n", command);
	)
	routeHopsNumber = 0;

	// Start time
	gettimeofday(&tv1, &tz);

	FILE* fp = popen(command,"r");

	if(!fp)
	{
		DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbTraceRoute(): popen() error\n");
		)
		return ERR_INTERNAL_ERROR;
	}

	traceRouteIsStarted = 0;
	isUnknownHost = 1;

	/* getline() function is a GNU extension, but it is the recommended way to read lines from a stream.
	 * The alternative standard functions are unreliable.
	 * No GNU function fgets: while( !(tmpStr = fgets(line, sizeof(line), fp)) );
	 */
	//skip header lines:
	while( !traceRouteIsStarted  &&  0 < (len = getline(&linePtr, &size, fp)) )
	{
		isUnknownHost = 0; // if at least one getlin() calling has returned line, then it isn't Unknown Host

		if (strstr(linePtr,"traceroute to "))
			traceRouteIsStarted = 1;

		if (linePtr &&  linePtr != line )
		{
			free(linePtr);
			linePtr = line;
			size = sizeof(line);
		}
	}

	if (isUnknownHost == 1)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbTraceRoute(): Cannot resolve host name\n");
		)
		pclose(fp);
		return ISUNKNOWNHOST;
	}

	if (traceRouteIsStarted)
	{
		while (0 < (len = getline(&linePtr, &size, fp)) )
		{
			char hopHost[257] = {'\0'};
			char hopHostAddress[257] = {'\0'};
			unsigned int hopErrorCode, wasFoundHopErrorCode;
			char hopRTTimes[120] = {'\0'}; // real size == 16.
			char template[100] = {'\0'};
			char * pointToTimes = NULL;
			char tmpParamName[MAX_PARAM_PATH_LENGTH] = {'\0'};
			unsigned int instanceNumber;

			pthread_testcancel();
			pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
			pthread_mutex_lock(&paramLock);
#ifdef WITH_USE_SQLITE_DB
			executeDBCommand(BEGIN);
#endif
			// Add new object
			ret = addObjectIntern(TraceRouteDiagnostics_RouteHopsObject, &instanceNumber, RPC_COMMAND_IS_CALLED_INTERNAL, CALL_USER_DEFINED_CALLBACK);
			if (ret != OK)
			{
#ifdef WITH_USE_SQLITE_DB
				executeDBCommand(END);
#endif
				pthread_mutex_unlock(&paramLock);

				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_ACCESS, "cbTraceRoute()->addObjectIntern(%s) has returned error = %d\n",
								TraceRouteDiagnostics_RouteHopsObject, ret);
				)
				pclose(fp);
				return ERR_INTERNAL_ERROR;
			}

			pthread_mutex_unlock(&paramLock);
			pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
			pthread_testcancel();

			sscanf(linePtr, " %u  %s (%s)", &routeHopsNumber, &hopHost, &hopHostAddress);
			pointToTimes = strstr(linePtr, hopHostAddress) + strlen(hopHostAddress);

			char *pchar = strchr(hopHostAddress, ')');
			if (pchar)
				*pchar = '\0';

#ifdef TraceRouteDiagnostics_RouteHopsObject_HopHost
			memset(&tmpParamName, 0, sizeof(tmpParamName));
			sprintf (tmpParamName, "%s%d.%s", TraceRouteDiagnostics_RouteHopsObject,
					instanceNumber, TraceRouteDiagnostics_RouteHopsObject_HopHost);
			ret = setUniversalParamValueInternal(NULL, tmpParamName, 1, hopHost);
#endif

#ifdef TraceRouteDiagnostics_RouteHopsObject_HopHostAddress
			memset(&tmpParamName, 0, sizeof(tmpParamName));
			sprintf (tmpParamName, "%s%d.%s", TraceRouteDiagnostics_RouteHopsObject,
					instanceNumber, TraceRouteDiagnostics_RouteHopsObject_HopHostAddress);
			ret = setUniversalParamValueInternal(NULL, tmpParamName, 1, hopHostAddress);
#endif

#ifdef TraceRouteDiagnostics_RouteHopsObject_HopErrorCode
			hopErrorCode = 0; // default ? If don't use ICMP.
			//getting a hop error code from the ICMP error message
			/* After the time some additional annotation can be printed:
			 * !H, !N, or !P (host, network or protocol unreachable),
			 * !S (source route failed), !F (fragmentation needed),
			 * !X (communication administratively prohibited), !V (host precedence violation),
			 * !C (precedence cutoff in effect), or !<num> (ICMP unreachable code <num>).
			 * If almost all the probes result in some kind of unreachable, traceroute will give up and exit.
			 */
			wasFoundHopErrorCode = 1;
			if ( strstr(linePtr,"N!") || strstr(linePtr,"!N") )
			{
				hopErrorCode = 0; // Net Unreachable
			}
			else if ( strstr(linePtr,"H!") || strstr(linePtr,"!H") )
			{
				hopErrorCode = 1; // Host Unreachable
			}
			else if ( strstr(linePtr,"P!") || strstr(linePtr,"!P") )
			{
				hopErrorCode = 2; // Protocol Unreachable
			}
			else if ( strstr(linePtr,"F!") || strstr(linePtr,"!F") )
			{
				hopErrorCode = 4; // Fragmentation Needed and Don't Fragment was Set
			}
			else if ( strstr(linePtr,"S!") || strstr(linePtr,"!S") )
			{
				hopErrorCode = 5; // Source Route Failed
			}
			else if ( strstr(linePtr,"X!") || strstr(linePtr,"!X") )
			{
				hopErrorCode = 13; // Communication Administratively Prohibited
			}
			else if ( strstr(linePtr,"V!") || strstr(linePtr,"!V") )
			{
				hopErrorCode = 14; // host precedence violation
			}
			else if ( strstr(linePtr,"C!") || strstr(linePtr,"!C") )
			{
				hopErrorCode = 15; // precedence cutoff in effect
			}
			else
				wasFoundHopErrorCode = 0;

			memset(&tmpParamName, 0, sizeof(tmpParamName));
			sprintf (tmpParamName, "%s%d.%s", TraceRouteDiagnostics_RouteHopsObject,
					instanceNumber, TraceRouteDiagnostics_RouteHopsObject_HopErrorCode);
			ret = setUniversalParamValueInternal(NULL, tmpParamName, 1, hopErrorCode);

#endif

#ifdef TraceRouteDiagnostics_RouteHopsObject_HopRTTimes
			memset(&template, 0, sizeof(template));
			for (i = 0; i < numberOfTries; i++)
			{
				if (wasFoundHopErrorCode)
					strcat(template, "%f ms%*s");
				else
					strcat(template, "%f ms");
			}

			float arrayHopTimes[10] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
			sscanf(pointToTimes, template, &arrayHopTimes[0], &arrayHopTimes[1], &arrayHopTimes[2], &arrayHopTimes[3], &arrayHopTimes[4], &arrayHopTimes[5], &arrayHopTimes[6], &arrayHopTimes[7], &arrayHopTimes[8], &arrayHopTimes[9]);

			char tempHopTime[11] = {'\0'};
			for (i = 0; i < numberOfTries; i++)
			{
				sprintf(tempHopTime, "%d", iRoundf(arrayHopTimes[i]));
				strcat(hopRTTimes, tempHopTime);
				strcat(hopRTTimes, ",");
			}

			// Comma-separated list (maximum length 16) of unsigned integers.
			// Contains one or more round trip times in milliseconds (one for each repetition) for this hop.
			if (hopRTTimes[16] == ',')
				hopRTTimes[16] = '\0';
			if (strlen(hopRTTimes) >= 16)
			{
				int i = 16;
				while(hopRTTimes[--i] != ',') ;
				hopRTTimes[i] = '\0';
			}

			memset(&tmpParamName, 0, sizeof(tmpParamName));
			sprintf (tmpParamName, "%s%d.%s", TraceRouteDiagnostics_RouteHopsObject,
					instanceNumber, TraceRouteDiagnostics_RouteHopsObject_HopRTTimes);
			ret = setUniversalParamValueInternal(NULL, tmpParamName, 1, hopRTTimes);
#endif

#ifdef WITH_USE_SQLITE_DB
			executeDBCommand(END);
#endif

			if (linePtr  &&  linePtr != line )
			{
				free(linePtr);
				linePtr = line;
				size = sizeof(line);
			}
		} /* end of while*/
	} /* end of  if (traceRouteIsStarted) */
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbTraceRoute():  command 'traceroute' has not been executed\n");
		)
		pclose(fp);
		return ERR_INTERNAL_ERROR;
	}
	pclose(fp);

	gettimeofday(&tv2, &tz);
	dtv.tv_sec= tv2.tv_sec -tv1.tv_sec;
	dtv.tv_usec=tv2.tv_usec-tv1.tv_usec;
	if(dtv.tv_usec < 0)
	{
		dtv.tv_sec--;
		dtv.tv_usec+=1000000;
	}

	double_responseTime = dtv.tv_sec * 1000  +  dtv.tv_usec * 0.001; // Converting to msec
	responseTime = lRoundd(double_responseTime);

	ret = setUniversalParamValueInternal(NULL, TraceRouteDiagnostics_ResponseTime, 1, (unsigned int)responseTime);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "cbTraceRoute(): setUniversalParamValueInternal(%s) error, ret=%d\n",
						TraceRouteDiagnostics_ResponseTime, ret);
		)
		return ERR_INTERNAL_ERROR;
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Exit from cbTraceRoute()\n");
	)
	return OK;
}


static void traceRouteDiagnosticThreadExitCallback(void * arg)
{
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "traceRouteDiagnosticThreadExitCallback() has been done. The allocated memory is freed.\n");
	)
}

static void * traceRouteDiagnosticStartFunk(void * param)
{
	int ret = OK;
	ParameterValue temp_value;

	pthread_cleanup_push(traceRouteDiagnosticThreadExitCallback, NULL);

	/*Next line can be used for immediate termination of diagnostic thread.
	  Default state = PTHREAD_CANCEL_DEFERRED.
	*/
	//pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &ret);

	ret = cbTraceRoute();

	switch (ret)
	{
		case OK:
			temp_value.in_cval = TraceRouteDiagnosticsState_Complete;
			break;
		case ISUNKNOWNHOST:
			temp_value.in_cval = TraceRouteDiagnosticsState_Error_CannotResolveHostName;
			break;
		case MAXHOPCOUNTEXCEEDED:
			temp_value.in_cval = TraceRouteDiagnosticsState_Error_MaxHopCountExceeded;
			break;
		default:
			temp_value.in_cval = TraceRouteDiagnosticsState_InternalError;
			break;
	}

	ret = updateParamValue(TraceRouteDiagnostics_DiagnosticsState, StringType, &temp_value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "traceRouteDiagnosticStartFunk(): updateParamValue() error\n");
		)
	}

	*((int *) param) = ret;
	pthread_exit(param);
	pthread_cleanup_pop(1);
	return NULL;
}

DiagnosticTypes getTraceRouteDiagnosticInfo (DiagnosticStartFunk* pDiagMainFunc)
{
	if (pDiagMainFunc)
		*pDiagMainFunc = traceRouteDiagnosticStartFunk;
	return traceRouteDiagnostic;
}
#endif

/** This function is called by setParameters()
	if data == Requested then install the diagnostic start function
	the function is called after all requests are done.
 */
int setTraceRouteDiagnostics( const char *name, ParameterType type, ParameterValue *data )
{
#ifdef HAVE_TRACE_ROUTE_DIAGNOSTICS
	int ret;
	if ( strncmp( *(char **)data, TraceRouteDiagnosticsState_Requested, strlen( *(char**)data ) ) == 0 )
	{
		// Store the value in the permanent storage
		ret = updateParamValue( name, type, data);
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_ACCESS, "setTraceRouteDiagnostics(): updateParamValue(%s) error = %d\n",name, ret);
			)
			return ret;
		}
		addDiagnosticToDiagnosticsList(traceRouteDiagnostic, getTraceRouteDiagnosticInfo);
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "setTraceRouteDiagnostics(): Invalid parameter value, DiagnosticsState can be set to \"Requested\" only.\n");
		)
		return ERR_INVALID_PARAMETER_VALUE;
	}
#endif
	return OK;
}

#ifdef HAVE_WANDSL_DIAGNOSTICS

static int cbWANDSL( void );

static int cbWANDSL( void )
{
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Start of cbWANDSL\n" );
	)
	return OK;
}


static void WANDSLDiagnosticThreadExitCallback(void * arg)
{
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "WANDSLDiagnosticThreadExitCallback() has been done. The allocated memory is freed.\n");
	)
}

static void * WANDSLDiagnosticStartFunk(void * param)
{
	int ret = OK;

	pthread_cleanup_push(WANDSLDiagnosticThreadExitCallback, NULL);

	/*Next line can be used for immediate termination of diagnostic thread.
	  Default state = PTHREAD_CANCEL_DEFERRED.
	*/
	//pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &ret);

	ret = cbWANDSL();

	*((int *) param) = ret;
	pthread_exit(param);
	pthread_cleanup_pop(1);
	return NULL;
}

DiagnosticTypes getWANDSLDiagnosticInfo (DiagnosticStartFunk* pDiagMainFunc)
{
	if (pDiagMainFunc)
		*pDiagMainFunc = WANDSLDiagnosticStartFunk;
	return WANDSLDiagnostic;
}

#endif /* HAVE_WANDSL_DIAGNOSTICS */

/** This function is called by setParameters()
	if data == Requested then add DownloadDiagnostics to DiagnosticsList.
	Diagnostic will be called after all requests are done.
*/
int setWANDSLDiagnostics( const char *name, ParameterType type, ParameterValue *data )
{
#ifdef HAVE_WANDSL_DIAGNOSTICS
	int ret;
	if ( strncmp( *(char **)data, DiagnosticsState_Requested, strlen( *(char**)data ) ) == 0 )
	{
		// Store the value in the permanent storage
		ret = updateParamValue( name, type, data);
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_ACCESS, "setWANDSLDiagnostics(): updateParamValue(%s) error = %d\n", name, ret);
			)
			return ret;
		}
		addDiagnosticToDiagnosticsList(WANDSLDiagnostic, getWANDSLDiagnosticInfo);
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "setWANDSLDiagnostics(): Invalid parameter value, DiagnosticsState can be set to \"Requested\" only.\n");
		)
		return ERR_INVALID_PARAMETER_VALUE;
	}
#endif /* HAVE_WANDSL_DIAGNOSTICS */
	return OK;
}

#ifdef HAVE_ATMF5_DIAGNOSTICS

int cbATMF5( void );

int cbATMF5( void )
{
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "Start of cbATMF5\n");
	)
	return OK;
}


static void ATMF5DiagnosticThreadExitCallback(void * arg)
{
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "ATMF5DiagnosticThreadExitCallback() has been done. The allocated memory is freed.\n");
	)
}

static void * ATMF5DiagnosticStartFunk(void * param)
{
	int ret = OK;

	pthread_cleanup_push(ATMF5DiagnosticThreadExitCallback, NULL);

	/*Next line can be used for immediate termination of diagnostic thread.
	  Default state = PTHREAD_CANCEL_DEFERRED.
	*/
	//pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &ret);

	ret = cbATMF5();

	*((int *) param) = ret;
	pthread_exit(param);
	pthread_cleanup_pop(1);
	return NULL;
}

DiagnosticTypes getATMF5DiagnosticInfo (DiagnosticStartFunk* pDiagMainFunc)
{
	if (pDiagMainFunc)
		*pDiagMainFunc = ATMF5DiagnosticStartFunk;
	return ATMF5Diagnostic;
}

#endif /* HAVE_ATMF5_DIAGNOSTICS */

int setATMF5Diagnostics( const char *name, ParameterType type, ParameterValue *data )
{
#ifdef HAVE_ATMF5_DIAGNOSTICS
	int ret;
	if ( strncmp( *(char **)data, DiagnosticsState_Requested, strlen( *(char**)data ) ) == 0 )
	{
		// Store the value in the permanent storage
		ret = updateParamValue( name, type, data);
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_ACCESS, "setATMF5Diagnostics(): updateParamValue(%s) error = %d\n", name, ret);
			)
			return ret;
		}
		addDiagnosticToDiagnosticsList(ATMF5Diagnostic, getATMF5DiagnosticInfo);
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "setATMF5Diagnostics(): Invalid parameter value, DiagnosticsState can be set to \"Requested\" only.\n");
		)
		return ERR_INVALID_PARAMETER_VALUE;
	}
#endif /* HAVE_ATMF5_DIAGNOSTICS */

	return OK;
}


int
set_WANATMF5LoopbackDiagnostics_NumberOfRepetitions(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_ATMF5_DIAGNOSTICS
	int ret;
	char diagState[120] = {"\0"};
	char * tmp;

	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_WANATMF5LoopbackDiagnostics_NumberOfRepetitions(): updateParamValue() error2\n");
		)
		return ret;
	}

	tmp = strrchr(name, '.');
	strncpy(diagState, name, tmp - name);
	diagState[tmp - name] = '\0';
	strcat(diagState, ".DiagnosticsState\0");

	//If the test is in progress - test being terminated
	ret = diagnosticTaskCancel(ATMF5Diagnostic, diagState);
	return ret;
#endif /* HAVE_ATMF5_DIAGNOSTICS */
	return OK;
}

int
set_WANATMF5LoopbackDiagnostics_Timeout(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_ATMF5_DIAGNOSTICS
	int ret;
	char diagState[120] = {"\0"};
	char * tmp;

	//Storing value
	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "set_WANATMF5LoopbackDiagnostics_Timeout(): updateParamValue() error2\n");
		)
		return ret;
	}

	tmp = strrchr(name, '.');
	strncpy(diagState, name, tmp - name);
	diagState[tmp - name] = '\0';
	strcat(diagState, ".DiagnosticsState\0");

	//If the test is in progress - test being terminated
	ret = diagnosticTaskCancel(ATMF5Diagnostic, diagState);
	return ret;
#endif /* HAVE_ATMF5_DIAGNOSTICS */
	return OK;
}

/* calls setUniversalParamValueInternal
 *  Write current time to parameterPath
 */
static int
setTimeParamToCurrentTime( const char *parameterPath )
{
	struct	timeval	t;
	gettimeofday(&t, NULL);
	return  setUniversalParamValueInternal(NULL, parameterPath, 0, &t);
}

#endif /* HAVE_DIAGNOSTICS */

