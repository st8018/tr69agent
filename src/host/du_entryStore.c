/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * du_entryStore.c
 *
 *  Created on: 25.01.2012
 *      Author: apnt
 */

/**
 * All deployment unit entry information is stored in the file system.
 */

#include <dirent.h>
#include "du_entryStore.h"
#include "dimark_globals.h"
#include "debug.h"
#include "unistd.h"

extern	char*	PERSISTENT_DU_ENTRYLIST_DIR;

static int loadDeploymentUnitEntryFile (char *, char *, newDeploymentUnitEntryInfo * );

/** Read all informations of pending deployment units entryes from the storage
 *  for every file info the callback function is called.
 */
int readDeploymentUnitEntryInfos(newDeploymentUnitEntryInfo *func)
{
	char buf[MAX_PATH_NAME_SIZE];
	char *bufPtr;
	struct dirent *entry;
	int ret = OK;
	DIR *dir;
	int nfiles;

	nfiles = 0;
	dir = opendir (PERSISTENT_DU_ENTRYLIST_DIR);
	if (dir == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DU_TRANSFER, "reloadDeploymentUnitEntryList: directory not found %s\n", PERSISTENT_DU_ENTRYLIST_DIR);
		)

		return ERR_DIM_TRANSFERLIST_READ;
	}

	strncpy (buf, PERSISTENT_DU_ENTRYLIST_DIR, sizeof(buf)-1);
	bufPtr = (buf + strlen(PERSISTENT_DU_ENTRYLIST_DIR));

	while ((entry = readdir (dir)) != NULL)
	{
		// skip . and .. and all files starting with .
		if (entry->d_name[0] == '.')
			continue;
		*bufPtr = '\0';
		strncat (buf, entry->d_name, sizeof(buf) - strlen(buf) - 1 );
		ret = loadDeploymentUnitEntryFile (buf, entry->d_name, func);
	}
	closedir(dir);
	return ret;
}

/** Write the informations for one deployment unit entry into the storage
 *
 * \param name 	unique name of the information
 * \param data	transfer informations
 */
int storeDeploymentUnitEntryInfo(const char *name, const char *data)
{
	int ret = OK;
	char path[MAX_PATH_NAME_SIZE + 1];
	int fd;

	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DU_TRANSFER, "storeDeploymentUnitEntryInfo(): function parameter 'name' can not be NULL.\n");
		)
		return ERR_DIM_TRANSFERLIST_WRITE;
	}

	// build pathname for storage file
	strncpy( path, PERSISTENT_DU_ENTRYLIST_DIR, MAX_PATH_NAME_SIZE );
	strncat( path, name, MAX_PATH_NAME_SIZE - strlen(path));
	fd = open( path, O_RDWR|O_CREAT|O_TRUNC, 0777 );
	if ( fd < 0 )
		return ERR_DIM_TRANSFERLIST_WRITE;
	if (data)
	{
		ret = write( fd, data, strlen(data));
		if ( ret < 0 )
		{
			close( fd );
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_DU_TRANSFER, "Save DeploymentUnitInfo: write() returns error %d\n", ret );
			)
			return ERR_DIM_TRANSFERLIST_WRITE;
		}
	}
	ret = close( fd );
	if ( ret != OK )
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DU_TRANSFER, "Save DeploymentUnitInfo: close() returns error %d\n", ret );
		)
		return ERR_DIM_TRANSFERLIST_WRITE;
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DU_TRANSFER, "Save DeploymentUnitInfo: %s : %d\n", data, ret );
	)

	return ret;
}

/** Delete the information of a specific deployment unit entry
 *
 * \param name	unique name of the information
 */
int deleteDeploymentUnitEntryInfo(const char *name)
{
	int ret = OK;
	char path[MAX_PATH_NAME_SIZE+1];

	// build pathname for storage file
	strncpy( path, PERSISTENT_DU_ENTRYLIST_DIR, MAX_PATH_NAME_SIZE );
	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DU_TRANSFER, "deleteDeploymentUnitEntryInfo(): function parameter 'name' can not be NULL.\n");
		)
		return ERR_DIM_TRANSFERLIST_WRITE;
	}
	strncat( path, name, MAX_PATH_NAME_SIZE - strlen(path) );
	ret = remove(path);
	if ( ret != OK )
		ret = ERR_DIM_TRANSFERLIST_WRITE;
	return ret;
}


/** Delete all informations about pending deployment unit entryes
 */
int clearAllDeploymentUnitEntryInfo( void )
{
	int ret = OK;
	struct dirent *entry;
	DIR *dir;
	int nfiles;
	char buf[MAX_PATH_NAME_SIZE];

	nfiles = 0;
	dir = opendir (PERSISTENT_DU_ENTRYLIST_DIR);
	if (dir == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DU_TRANSFER, "deleteDeploymentUnitEntrylist: directory not found %s\n", PERSISTENT_DU_ENTRYLIST_DIR);
		)
		return ERR_DIM_TRANSFERLIST_WRITE;
	}
	while ((entry = readdir (dir)) != NULL)
	{
		if (strcmp (entry->d_name, ".") == 0)
			continue;
		if (strcmp (entry->d_name, "..") == 0)
			continue;
		sprintf (buf, "%s%s", PERSISTENT_DU_ENTRYLIST_DIR, entry->d_name);
		ret = remove (buf);
		if ( ret < 0 ) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_DU_TRANSFER, "deleteDeploymentUnitlist: file not found %s\n", buf);
			)
			closedir(dir);
			return ERR_DIM_TRANSFERLIST_WRITE;
		}
	}
	closedir(dir);
	return ret;
}

static int loadDeploymentUnitEntryFile(char *filename, char *name, newDeploymentUnitEntryInfo* callbackF )
{
	int ret = OK;
	char buf[DU_TRANSFER_MAX_INFO_SIZE + 1];
	FILE *file;

	if (!filename)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DU_TRANSFER, "loadDeploymentUnitEntryFile(): function parameter 'filename' can not be NULL.\n");
		)
		return ERR_DIM_TRANSFERLIST_WRITE;
	}

	file = fopen (filename, "r");
	if (file == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DU_TRANSFER, "loadInitialParameters: file not found %s\n", filename);
		)

		return ERR_INTERNAL_ERROR;
	}

	while (fgets (buf, DU_TRANSFER_MAX_INFO_SIZE, file) != NULL)
	{
		if ( buf[0] == '#' || buf[0] == ' ' || buf[0] == '\n' || buf[0] == '\0' )
			continue;
		buf[strlen (buf) - 1] = '\0';	/* remove trailing EOL  */
		ret += callbackF(name, buf);
	}
	fclose(file);
	return ret;
}
