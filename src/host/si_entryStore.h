/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef SI_ENTRYSTORE_H_
#define SI_ENTRYSTORE_H_

#define SI_MAX_INFO_SIZE	128

#include "utils.h"

typedef int (newScheduleInformEntryInfo)(char *, char *);

/** Read all informations of pending schedule inform entry from the storage
 *  for every file info the callback function is called.
 */
int readScheduleInformEntryInfos(newScheduleInformEntryInfo *);

/** Write the informations for one schedule inform entry into the storage
 *
 * \param name 	unique name of the information
 * \param data	se informations
 */
int storeScheduleInformEntryInfo( const char *, const char * );

/** Delete the information of a specific schedule inform entry
 *
 * \param name	unique name of the information
 */
int deleteScheduleInformEntryInfo( const char * );


/** Delete all informations about pending schedule inform entryes
 */
int clearAllScheduleInformEntryInfo( void );

#endif /* SI_ENTRYSTORE_H_ */


