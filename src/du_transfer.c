/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"

#ifdef HAVE_FILE

#ifdef HAVE_DEPLOYMENT_UNIT
#include <stdio.h>
#include <sys/types.h>

#ifdef UUID_H_FILE_PATH
#include  UUID_H_FILE_PATH
#else
#include <uuid/uuid.h>
#endif

#include "du_transfer.h"
#include "list.h"
#include "host/du_entryStore.h"
#include "host/filetransferStore.h"
#include "deployment_unit.h"

pthread_mutex_t duList_usage_mutex = PTHREAD_MUTEX_INITIALIZER;

static StrValueDU strDeploymentUnitStatus[] = {{"Installing"},
							  	  	  	  	   {"Installed"},
							  	  	  	  	   {"Updating"},
								  	  	  	   {"Updated"},
								  	  	  	   {"Uninstalling"},
								  	  	  	   {"Uninstalled"},
								  	  	  	   {"Failed"}};

static StrValueDU strDeploymentUnitType[] = {{"Install"},
											 {"Update"},
											 {"Uninstall"}};

extern DeploymentUnitCB DU_CB_AfterDownload;

extern SUPORTEDRPC suportedRPC;

extern int  prevSoapErrorValue;

List deploymentUnitList;
bool isAutonomousDUStateChangeComplete = true;

void setDeploymentUnitEntryFault(DeploymentUnitEntry *);
void generateUUID(DeploymentUnitEntry *);
static void addDeploymentUnitEntry(DeploymentUnitEntry *);
static void printDeploymentUnitEntryList(void);
static int deleteDeploymentUnitEntry(DeploymentUnitEntry *);
static int writeDeploymentUnitEntry(DeploymentUnitEntry *, int);
static int readDeploymentUnitEntry(void);
static int readDeploymentUnitEntryList(char *, char *);
static char *getDUTransferStateName(DU_TransferState);
/********* Function for work with deploymentUnitList *******/
static void initDeploymentUnitList();
static ListEntry * iterateDeploymentUnitList(ListEntry * entry);
static void addEntryToDeploymentUnitList(DeploymentUnitEntry * due);
static ListEntry * getFirstEntryFromDeploymentUnitList();
static ListEntry * iterateRemoveFromDeploymentUnitList(ListEntry * entry);


/* set deployment unit entry fault */
void setDeploymentUnitEntryFault(DeploymentUnitEntry * due)
{
	if(due == NULL)
		return;

	/* get complete time */
	gettimeofday(&due->completeTime, NULL);

	strnCopy(due->faultString, getFaultString(due->faultCode), (sizeof (due->faultString) - 1));

	if(due->initiator == ACS)
	{
		due->result.fault.FaultString = due->faultString;
		due->result.fault.FaultCode = due->faultCode;
		due->result.StartTime.tv_sec = due->startTime.tv_sec;
		due->result.StartTime.tv_usec = due->startTime.tv_usec;
		due->result.CompleteTime.tv_sec = due->completeTime.tv_sec;
		due->result.CompleteTime.tv_usec = due->completeTime.tv_usec;
	}

	if(due->initiator == NOTACS)
	{
#ifdef	HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE
		due->autonomous_result.fault.FaultString = due->faultString;
		due->autonomous_result.fault.FaultCode = due->faultCode;
		due->autonomous_result.StartTime.tv_sec = due->startTime.tv_sec;
		due->autonomous_result.StartTime.tv_usec = due->startTime.tv_usec;
		due->autonomous_result.CompleteTime.tv_sec = due->completeTime.tv_sec;
		due->autonomous_result.CompleteTime.tv_usec = due->completeTime.tv_usec;
#endif /* HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE */
	}

	return;
}

/* generate new UUID */
void generateUUID(DeploymentUnitEntry * due)
{
	uuid_t out;
	uuid_generate(out);
	uuid_unparse (out, due->UUID);
};

/* check UUID return 0 - uuid is good,  -1 - uuid not good */
int checkUUID(DeploymentUnitEntry * due)
{
	uuid_t out;
	return uuid_parse(due->UUID, out);
};

char * getDeploymentUnitStatus(Status_DU dus)
{
	return strDeploymentUnitStatus[dus].value;
};

char * getDeploymentUnitType(DU_Type dut)
{
	return strDeploymentUnitType[dut].value;
};

/* Initialize the deployment unit list and load the du list from file
 * if there is one available */
int initDeploymentUnitTransfer(DeploymentUnitCB du_CBAD)
{
	DU_CB_AfterDownload = du_CBAD;
	initDeploymentUnitList();
	return readDeploymentUnitEntry();
}

/* resets all deployment unit entries and deletes the bootstrap file */
int resetAllDeploymentUnitEntry(void)
{
	freeDUEntryMemory();
	return clearAllDeploymentUnitEntryInfo();
}

/* Prints the transfer list */
static void printDeploymentUnitEntryList(void)
{
	ListEntry *entry = NULL;
	DeploymentUnitEntry *due = NULL;
	struct timeval now;
	gettimeofday(&now, NULL);

	while((entry = iterateDeploymentUnitList(entry)))
	{
		due = (DeploymentUnitEntry *) entry->data;
		if(!due)
			continue;
		if((due->transfer_status == DU_Transfer_NotStarted) && (due->startTime.tv_sec >= now.tv_sec))
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DU_TRANSFER, "URL: %s Status: \"NotStarted\"(%d) Start in %d sec\n", due->URL, due->transfer_status, due->startTime.tv_sec - now.tv_sec);
			)
			continue;
		}
		if((due->transfer_status == DU_Transfer_InProgress) && (due->startTime.tv_sec <= now.tv_sec))
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DU_TRANSFER, "URL: %s Status: \"InProgress\"(%d) Started %d sec ago\n", due->URL, due->transfer_status, now.tv_sec - due->startTime.tv_sec);
			)
			continue;
		}
		if((due->transfer_status == DU_Transfer_Completed) && (due->completeTime.tv_sec <= now.tv_sec))
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DU_TRANSFER, "URL: %s Status: \"Completed\"(%d) Completed %d sec ago\n", due->URL, due->transfer_status, now.tv_sec - due->completeTime.tv_sec);
			)
			continue;
		}
	}
}

/* Helper function to add an DeploymentUnitEntry to the DeploymentUnitList */
static void addDeploymentUnitEntry(DeploymentUnitEntry * due)
{
	addEntryToDeploymentUnitList(due);
	writeDeploymentUnitEntry(due, false);
}

static int writeDeploymentUnitEntry(DeploymentUnitEntry * due, int rewrite)
{
	int ret = OK;
	char name[DU_UUID_LEN];
	char data[DU_TRANSFER_MAX_INFO_SIZE];

	sprintf(name, "%s", due->UUID);

	DEBUG_OUTPUT(
			dbglog (SVR_INFO, DBG_DU_TRANSFER, "Save: %s Status: %d\n", due->URL, due->transfer_status);
	)

	ret = sprintf(data, "%d|%d|%d|%s|%s|%s|%s|%s|%s|%s|%ld|%ld|%ld|%ld|%d|%s|%d|%d|%s|%s|%s|%d|%d\n",
			due->type,
			due->du_status,
			due->initiator,
			due->commandKey,
			due->URL,
			due->UUID,
			due->Username,
			due->Password,
			due->ExecutionEnvRef,
			due->Version,
			due->startTime.tv_sec,
			due->startTime.tv_usec,
			due->completeTime.tv_sec,
			due->completeTime.tv_usec,
			due->fileSize,
			due->targetFileName,
			due->transfer_status,
			due->faultCode,
			due->faultString,
			due->DeploymentUnitRef,
			due->ExecutionUnitRefList,
			due->InstanceNumber,
			due->Resolved);

	if (ret < 0)
	{
		return ERR_DIM_TRANSFERLIST_WRITE;
	}

	if(!rewrite)
		ret = storeDeploymentUnitEntryInfo(name, data);		/* create new file */
	else
	{
		ret = deleteDeploymentUnitEntryInfo(name);			/* remove exists file */
		ret = storeDeploymentUnitEntryInfo(name, data);		/* create new file */
	}

	return ret;
}

static int deleteDeploymentUnitEntry(DeploymentUnitEntry * due)
{
	int ret = OK;
	char name[DU_UUID_LEN];

	sprintf(name, "%s", due->UUID);

	ret = deleteDeploymentUnitEntryInfo(name);

	return ret;
}

static int readDeploymentUnitEntry(void)
{
	int ret = OK;
	ret = readDeploymentUnitEntryInfos((newDeploymentUnitEntryInfo *) &readDeploymentUnitEntryList);

	return ret;
}

static int readDeploymentUnitEntryList(char *name, char *data)
{
	int ret = OK;
	DeploymentUnitEntry *due = NULL;
	char *bufptr;
	bufptr = data;
	due = (DeploymentUnitEntry *) emallocTypedMem(sizeof(DeploymentUnitEntry), MEM_TYPE_DU_LIST, 0);

	if (due == NULL)
	{
		return ERR_RESOURCE_EXCEEDED;
	}

	due->type = a2i(strsep(&bufptr, "|"), NULL);
	due->du_status = a2i(strsep(&bufptr, "|"), NULL);
	due->initiator = a2i(strsep(&bufptr, "|"), NULL);
	strnCopy(due->commandKey, strsep(&bufptr, "|"), CMD_KEY_STR_LEN);
	strnCopy(due->URL, strsep(&bufptr, "|"), DU_URL_LEN);
	strnCopy(due->UUID, strsep(&bufptr, "|"), DU_UUID_LEN);
	strnCopy(due->Username, strsep(&bufptr, "|"), DU_USERNAME_LEN);
	strnCopy(due->Password, strsep(&bufptr, "|"), DU_PASSWORD_LEN);
	strnCopy(due->ExecutionEnvRef, strsep(&bufptr, "|"), DU_EXECUTION_ENV_REF_LEN);
	strnCopy(due->Version, strsep(&bufptr, "|"), DU_VERSION_LEN);
	due->startTime.tv_sec = a2l(strsep(&bufptr, "|"), NULL);
	due->startTime.tv_usec = a2l(strsep(&bufptr, "|"), NULL);
	due->completeTime.tv_sec = a2l(strsep(&bufptr, "|"), NULL);
	due->completeTime.tv_usec = a2l(strsep(&bufptr, "|"), NULL);
	due->fileSize = a2ui(strsep(&bufptr, "|"), NULL);
	strnCopy(due->targetFileName, strsep(&bufptr, "|"), DU_DEST_FILE_PATH_LEN);
	due->transfer_status = a2i(strsep(&bufptr, "|"), NULL);
	due->faultCode = a2ui(strsep(&bufptr, "|"), NULL);
	strnCopy(due->faultString, strsep(&bufptr, "|"), DU_FAULT_STR_LEN);
	strnCopy(due->DeploymentUnitRef, strsep(&bufptr, "|"), MAX_PARAM_PATH_LENGTH);
	strnCopy(due->ExecutionUnitRefList, strsep(&bufptr, "|"), MAX_PARAM_PATH_LENGTH);
	due->InstanceNumber = a2i(strsep(&bufptr, "|"), NULL);
	due->Resolved = a2i(strsep(&bufptr, "|"), NULL);


	addDeploymentUnitEntry(due);
	return ret;
}

int execInstallDeploymentUnit(struct soap *soap, char * CommandKey, char * URL, char * UUID, char * Username,	char * Password, char * ExecutionEnvRef, Transfers_type initiator)
{
	int returnCode = OK;
	DeploymentUnitEntry *due;
	char *targetFileName;

	DEBUG_OUTPUT ( dbglog (SVR_INFO, DBG_DU_TRANSFER, "Install deployment unit\n");
				   dbglog (SVR_INFO, DBG_DU_TRANSFER, "URL                 : %s\n", URL);
				   dbglog (SVR_INFO, DBG_DU_TRANSFER, "UUID                : %s\n", UUID);
				   dbglog (SVR_INFO, DBG_DU_TRANSFER, "Username            : %s\n", Username);
				   dbglog (SVR_INFO, DBG_DU_TRANSFER, "Password            : %s\n", Password);
	)

	targetFileName = getDefaultDownloadFilename(URL);
	due = (DeploymentUnitEntry *) emallocTypedMem(sizeof(DeploymentUnitEntry), MEM_TYPE_DU_LIST, 0);

	if(due == NULL)
		return ERR_RESOURCE_EXCEEDED;

	/* init deployment unit entry and add deployment unit entry */
	due->type = DU_INSTALL;
	due->du_status = INSTALLING;
	strnCopy(due->commandKey, CommandKey, (sizeof(due->commandKey)-1));
	strnCopy(due->URL, URL, (sizeof(due->URL)-1));
	strnCopy(due->UUID, UUID, (sizeof(due->UUID)-1));
	strnCopy(due->Username, Username, (sizeof(due->Username)-1));
	strnCopy(due->Password, Password, (sizeof(due->Password)-1));
	strnCopy(due->ExecutionEnvRef, ExecutionEnvRef, (sizeof(due->ExecutionEnvRef)-1));
	strnCopy(due->targetFileName, targetFileName, (sizeof(due->targetFileName) - 1));
	due->Resolved = false;
	due->InstanceNumber = 0;
	gettimeofday(&due->startTime, NULL);
	due->completeTime.tv_sec = 0;
	due->completeTime.tv_usec = 0;
	due->transfer_status = DU_Transfer_NotStarted;
	due->faultCode = NO_ERROR;
	due->initiator = initiator;
	/* generate UUID if need */
	if(!strcmp(due->UUID, ""))
		generateUUID(due); /* yes need generate new UUID */

	addDeploymentUnitEntry(due);
	return returnCode;
};

int execUpdateDeploymentUnit(struct soap *soap, char * CommandKey, char * UUID, char * Version, char * URL, char * Username, char * Password, Transfers_type initiator)
{
	int returnCode = OK;
	DeploymentUnitEntry *due = NULL;
	ListEntry *entry = NULL;
	char *targetFileName;


	DEBUG_OUTPUT ( dbglog (SVR_INFO, DBG_DU_TRANSFER, "Update deployment unit\n");
				   dbglog (SVR_INFO, DBG_DU_TRANSFER, "URL                 : %s\n", URL);
				   dbglog (SVR_INFO, DBG_DU_TRANSFER, "UUID                : %s\n", UUID);
				   dbglog (SVR_INFO, DBG_DU_TRANSFER, "Version             : %s\n", Version);
				   dbglog (SVR_INFO, DBG_DU_TRANSFER, "Username            : %s\n", Username);
				   dbglog (SVR_INFO, DBG_DU_TRANSFER, "Password            : %s\n", Password);
	)

	targetFileName = getDefaultDownloadFilename(URL);

	/* find deployment unit entry  */
	while((entry = iterateDeploymentUnitList(entry)))
	{
		due = (DeploymentUnitEntry *) entry->data;
		if(!due)
			continue;
		if(strcmp(due->UUID, UUID))
		{
			due = NULL;
			continue;
		}
		else
			break;
	}

	if(due == NULL)
		return ERR_RESOURCE_EXCEEDED;

	due->type = DU_UPDATE;
	due->du_status = UPDATING;
	strnCopy(due->commandKey, CommandKey, (sizeof(due->commandKey)-1));
	strnCopy(due->URL, URL, (sizeof(due->URL)-1));
	strnCopy(due->Version, Version, (sizeof(due->Version)-1));
	strnCopy(due->Username, Username, (sizeof(due->Username)-1));
	strnCopy(due->Password, Password, (sizeof(due->Password)-1));
	strnCopy(due->targetFileName, targetFileName, (sizeof(due->targetFileName) - 1));
	due->Resolved = false;
	gettimeofday(&due->startTime, NULL);
	due->completeTime.tv_sec = 0;
	due->completeTime.tv_usec = 0;
	due->transfer_status = DU_Transfer_NotStarted;
	due->faultCode = NO_ERROR;
	due->initiator = initiator;

	return returnCode;
};

int execUninstallDeploymentUnit(struct soap *soap, char * CommandKey, char * UUID, char *  Version, char *  ExecutionEnvRef, Transfers_type initiator)
{
	int returnCode = OK;
	DeploymentUnitEntry *due = NULL;
	ListEntry *entry = NULL;

	DEBUG_OUTPUT ( dbglog (SVR_INFO, DBG_DU_TRANSFER, "Uninstall deployment unit\n");
				   dbglog (SVR_INFO, DBG_DU_TRANSFER, "UUID                : %s\n", UUID);
				   dbglog (SVR_INFO, DBG_DU_TRANSFER, "Version             : %s\n", Version);
	)

	/* find deployment unit entry and update deployment unit entry */
	while((entry = iterateDeploymentUnitList(entry)))
	{
		due = (DeploymentUnitEntry *) entry->data;
		if(!due)
			continue;
		if(strcmp(due->UUID, UUID))
		{
			due = NULL;
			continue;
		}
		else
			break;
	}

	if(due == NULL)
		return ERR_RESOURCE_EXCEEDED;

	due->type = DU_UNINSTALL;
	due->du_status = UNINSTALLING;
	strnCopy(due->commandKey, CommandKey, (sizeof(due->commandKey) - 1));
	strnCopy(due->Version, Version, (sizeof(due->Version)-1));
	strnCopy(due->ExecutionEnvRef, ExecutionEnvRef, (sizeof(due->ExecutionEnvRef) -1));
	due->Resolved = false;
	gettimeofday(&due->startTime, NULL);
	due->completeTime.tv_sec = 0;
	due->completeTime.tv_usec = 0;
	due->transfer_status = DU_Transfer_NotStarted;
	due->faultCode = NO_ERROR;
	due->initiator = initiator;

	return returnCode;
};

/* Checks the existence of a delayed filet ransfer.
 * if there is an entry in the transfer list, then return true
 * else false. */
int isDeploymentUnitTransfer(void)
{
	return (getFirstEntryFromDeploymentUnitList() != NULL);
}


/* Iterates through the list of delayed deployment unit transfers.
 * When find one, it executes the download and informs the ACS about the result.
 * For informing the ACS, DUStateCnaheComplete() WebService is called immediately after the transfer.
 *
  * call clearDelayedFiletransfers() after this function */

/* param server	Pointer to the soap data
 * return int	Number of downloaded files */
int handleDeploymentUnitTransfers()
{
	int cnt = 0;
	int ret = OK;
	ListEntry *entry = NULL;
	DeploymentUnitEntry *due = NULL;

	printDeploymentUnitEntryList();

	while ((entry = iterateDeploymentUnitList(entry)))
	{
		struct timeval actTime;
		gettimeofday(&actTime, NULL);
		due = (DeploymentUnitEntry *) entry->data;
		if(!due)
			continue;

		/* all conditions full filled ? */
		if (((due->type == DU_INSTALL ) || (due->type == DU_UPDATE) || (due->type == DU_UNINSTALL)) && due->transfer_status == DU_Transfer_NotStarted && due->startTime.tv_sec <= actTime.tv_sec)
		{
			due->startTime = actTime;
			due->transfer_status = DU_Transfer_InProgress;

			/* check UUID */
			if(checkUUID(due) == 0)
			{
				if((due->type == DU_INSTALL ) || (due->type == DU_UPDATE))
				{
					ret = doDownload (due->URL, due->Username, due->Password, due->fileSize, due->targetFileName);

					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_DU_TRANSFER, "handleDeploymentUnitTransfers URL: %s  ret: %d\n", due->URL, ret);
					)

					if (ret != SOAP_OK)
					{
						due->faultCode = ret;
						/* set fault */
						setDeploymentUnitEntryFault(due);
						due->du_status = FAILED;

						cnt++;
						due->transfer_status = DU_Transfer_Completed;
						writeDeploymentUnitEntry(due, true);
						continue;
					}
				}

				/* process Deployment Unit */
				due->faultString[0] = '\0';
				if (DU_CB_AfterDownload != NULL)
				{
					ret = DU_CB_AfterDownload (due);
					if (ret != OK)
					{
						DEBUG_OUTPUT (
								dbglog (SVR_ERROR, DBG_DU_TRANSFER, "handleDeploymentUnitTransfers(): Deployment Unit Callback After has returned error = %d\n", ret);
						)

						cnt++;
						due->transfer_status = DU_Transfer_Completed;
						writeDeploymentUnitEntry(due, true);
						continue;
					}
				}
				else
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_DU_TRANSFER, "handleDeploymentUnitTransfers(): Deployment Unit Callback After Nof found \n");
					)
				}
			}
			else
			{
				due->faultCode = ERR_INVALID_UUID_FORMAT;
				/* set fault */
				setDeploymentUnitEntryFault(due);
				due->du_status = FAILED;

				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_DU_TRANSFER, "handleDeploymentUnitTransfers(): Invalid UUID format\n");
				)

				cnt++;
				due->transfer_status = DU_Transfer_Completed;
				writeDeploymentUnitEntry(due, true);
				continue;
			}

			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DU_TRANSFER, "URL: %s Status: \"Completed\"(%d)\n", due->URL, due->transfer_status);
			)

			cnt++;
			due->transfer_status = DU_Transfer_Completed;
			writeDeploymentUnitEntry(due, true);
			continue;
		}
	}
	return cnt;
}

int handleDeploymentUnitTransfersEvents(int * isNeedInform)
{
	int ret = SOAP_OK;
	ListEntry *entry = NULL;
	DeploymentUnitEntry *due = NULL;

	if (isNeedInform)
	{
		*isNeedInform = 0;
	}

	entry = iterateDeploymentUnitList(entry);
	while(entry)
	{
		due = (DeploymentUnitEntry *) entry->data;
		if(!due)
		{
			entry = iterateDeploymentUnitList(entry);
			continue;
		}

		if((due->transfer_status == DU_Transfer_Completed) && ( (due->type == DU_INSTALL) || (due->type == DU_UPDATE) || (due->type == DU_UNINSTALL) ))
		{
			/* It's used in the InformMessage with the DU STATE CHANGE COMPLETE event */
			if(due->initiator == ACS)
			{
				addEventCodeMultiple(EV_M_CHANGE_DU_STATE, due->commandKey);
				addEventCodeSingle(EV_DU_STATE_CHANGE_COMPLETE);
			}
			else
			{
#ifdef HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE
				if(isAutonomousDUStateChangeComplete)
				{
					addEventCodeSingle(EV_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE);
				}
#endif /* HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE */
			}
			due->transfer_status = DU_Informed_to_ACS;
			if (isNeedInform)
			{
				*isNeedInform = 1;
			}

			writeDeploymentUnitEntry(due, true);
		}
		entry = iterateDeploymentUnitList(entry);
	}
	return ret;
}

/* For all transfers with status == 1 a TranferComplete Message is sent to the ACS
 * and the entry is cleared from the file transfer list.
 * In case of an error the status is not changed. */
int clearDeploymentUnitTransfers(struct soap *server)
{
	int ret = SOAP_OK;
	ListEntry *entry = NULL;
	DeploymentUnitEntry *due = NULL;

	entry = iterateDeploymentUnitList(entry);
	while (entry)
	{
		due = (DeploymentUnitEntry *)entry->data;
		if(!due)
		{
			entry = iterateDeploymentUnitList(entry);
			continue;
		}
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DU_TRANSFER, "clearDeploymentUnitTransfers Type: %d URL: %s  Status: %s(%d)\n", due->type, due->URL, getDUTransferStateName(due->transfer_status), due->transfer_status);
		)

		if (due->transfer_status == DU_Informed_to_ACS)
		{
			/* Inform Server */
			if ((due->type == DU_INSTALL ) || (due->type == DU_UPDATE) || (due->type == DU_UNINSTALL))
			{
				if (due->initiator == ACS && suportedRPC.rpc.isDUStateChangeComplete)
				{
					struct cwmp__DUStateChangeCompleteResponse empty;
					struct ArrayOfOpResultStruct *Results = (struct ArrayOfOpResultStruct *)emallocTemp(sizeof(struct ArrayOfEventStruct), 50);
					OpResultStruct **res = (OpResultStruct **)emallocTemp(sizeof(OpResultStruct[1]),50);
					Results->__ptrOpResultStruct = res;
					Results->__size = 1;
					res[0] = &due->result;

					unsigned int redirectCount = MAX_REDIRECT_COUNT + 1; //+1 to sent first packet
					setAttrToHTTPandSOAPHeaders(server);
					while(redirectCount)
					{
						ret = soap_call_cwmp__DUStateChangeComplete(server, getServerURL(), "", Results, due->commandKey, &empty);
						prevSoapErrorValue = server->error;
						if (ret != SOAP_OK)
						{
							switch (server->error)
							{
								case 301:
								case 302:
								case 307:
									set_server_url_when_redirect(server->endpoint);
									redirectCount--;
									continue;
									break;
								case 401:
								case 407:
									DEBUG_OUTPUT (
												dbglog (SVR_DEBUG, DBG_DU_TRANSFER, "clearDeploymentUnitTransfers()->soap_call_cwmp__DUStateChangeComplete(): HTTP Authentication required. "
																	"soap->error=%d\n", server->error);
												)
									setAttrToHTTPandSOAPHeaders(server);
									ret = soap_call_cwmp__DUStateChangeComplete(server, getServerURL(), "", Results, due->commandKey, &empty);
									break;
								default:
									break;
							}
							break;
						}
						else
							break;
					}
					DEBUG_OUTPUT(
							dbglog ((ret?SVR_WARN:SVR_DEBUG), DBG_DU_TRANSFER, "clearDeploymentUnitTransfers()->soap_call_cwmp__DUStateChangeComplete(): ret=%d\n", ret);
					)
					if (redirectCount==0)
						return ret;
				}
				else
				{
#ifdef HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE
					if (suportedRPC.rpc.isAutonomousDUStateChangeComplete && isAutonomousDUStateChangeComplete)
					{
						struct cwmp__AutonomousDUStateChangeCompleteResponse empty;
						struct ArrayOfAutonOpResultStruct * Results = (struct ArrayOfAutonOpResultStruct *)emallocTemp(sizeof(struct ArrayOfAutonOpResultStruct), 50);
						AutonOpResultStruct **res = (AutonOpResultStruct **)emallocTemp(sizeof(AutonOpResultStruct[1]), 50);
						Results->__ptrAutonOpResultStruct = res;
						Results->__size = 1;
						res[0] = &due->autonomous_result;

						unsigned int redirectCount = MAX_REDIRECT_COUNT + 1; //+1 to sent first packet
						setAttrToHTTPandSOAPHeaders(server);
						while(redirectCount)
						{
							ret = soap_call_cwmp__AutonomousDUStateChangeComplete(server, getServerURL(), "", Results, &empty);
							prevSoapErrorValue = server->error;
							if (ret != SOAP_OK)
							{
								switch (server->error)
								{
									case 301:
									case 302:
									case 307:
										set_server_url_when_redirect(server->endpoint);
										redirectCount--;
										continue;
										break;
									case 401:
									case 407:
										DEBUG_OUTPUT (
												dbglog (SVR_DEBUG, DBG_DU_TRANSFER, "clearDeploymentUnitTransfers()->soap_call_cwmp__AutonomousDUStateChangeComplete(): HTTP Authentication required. "
														"soap->error=%d\n", server->error);
										)
										setAttrToHTTPandSOAPHeaders(server);
										ret = soap_call_cwmp__AutonomousDUStateChangeComplete(server, getServerURL(), "", Results, &empty);
										break;
									default:
										break;
								}
								break;
							}
							else
								break;
						}
						DEBUG_OUTPUT(
								dbglog ((ret?SVR_WARN:SVR_DEBUG), DBG_DU_TRANSFER, "clearDeploymentUnitTransfers()->soap_call_cwmp__AutonomousDUStateChangeComplete(): ret=%d\n", ret);
						)
						if (redirectCount==0)
							return ret;
					}
#endif /* HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE */
				}
			}
			/* remove this when REGMEN bug fixed */
			if (ret == SOAP_OK || ret == 32 )
			{
				due->transfer_status = DU_Completed;
				writeDeploymentUnitEntry(due, true);

				if(due->type == DU_UNINSTALL)
				{
					deleteDeploymentUnitEntry(due);
					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_DU_TRANSFER, "Remove Type: %d URL: %s Status: %s(%d)\n", due->type, due->URL, getDUTransferStateName(due->transfer_status), due->transfer_status);
					)
					efreeTypedMemByPointer((void**)&due, 0);
					entry = iterateRemoveFromDeploymentUnitList (entry);
				}
			}
			else
			{
				if(server->error == SOAP_SVR_FAULT)
					getFaultFromACS(server);
				entry = iterateDeploymentUnitList(entry);
			}
		}
		else
		{
			entry = iterateDeploymentUnitList(entry);
		}
	}

	return ret;
}


/* Returns the string with name of DU_TransferState
 * */
static char *getDUTransferStateName(DU_TransferState state)
{
	switch (state)
	{
		case DU_Transfer_NotStarted:
			return "DU_Transfer_NotStarted";
		case DU_Transfer_InProgress:
			return "DU_Transfer_InProgress";
		case DU_Transfer_Completed:
			return "DU_Transfer_Completed";
		case DU_Informed_to_ACS:
			return "DU_Informed_to_ACS";
		case DU_Completed:
			return "DU_Completed";
		default:
			return "";
	}
	return "";
}

#ifdef	 HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE
/* create Autonomous Deploymant Unit entry */
int createAutonomousDeploymentUnitEntry(DU_Type type,
										Status_DU du_status,
										char * URL,
										char * UUID,
										char * Username,
										char * Password,
										char * ExecutionEnvRef,
										char * Version,
										struct timeval startTime,
										struct timeval completeTime,
										DU_TransferState transfer_status)
{
	int returnCode = OK;
	DeploymentUnitEntry *due;
	char *targetFileName;
	char CommandKey[CMD_KEY_STR_LEN + 1] = {"\0"};


	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DU_TRANSFER, "Create autonomous deployment unit\n");
	)

	due = (DeploymentUnitEntry *) emallocTypedMem(sizeof(DeploymentUnitEntry), MEM_TYPE_DU_LIST, 0);

	if(due == NULL)
		return ERR_RESOURCE_EXCEEDED;

	due->type = type;

	if((URL == NULL) || (UUID == NULL) || (Username == NULL) || (Password == NULL) || (ExecutionEnvRef == NULL) || (Version == NULL))
	{
		efreeTypedMemByPointer((void**)&due, 0);
		return ERR_RESOURCE_EXCEEDED;
	}

	targetFileName = getDefaultDownloadFilename(URL);

	due->type = type;
	if(due->type == DU_INSTALL)
		due->du_status = INSTALLED;
	if(due->type == DU_UPDATE)
		due->du_status = UPDATED;
	if(due->type == DU_UNINSTALL)
		due->du_status = UNINSTALLED;

	strnCopy(due->commandKey, CommandKey, (sizeof(due->commandKey)-1));
	strnCopy(due->URL, URL, (sizeof(due->URL)-1));
	strnCopy(due->UUID, UUID, (sizeof(due->UUID)-1));
	strnCopy(due->Username, Username, (sizeof(due->Username)-1));
	strnCopy(due->Password, Password, (sizeof(due->Password)-1));
	strnCopy(due->Version, Version, (sizeof(due->Version)-1));
	strnCopy(due->ExecutionEnvRef, ExecutionEnvRef, (sizeof(due->ExecutionEnvRef)-1));
	strnCopy(due->targetFileName, targetFileName, (sizeof(due->targetFileName) - 1));
	due->Resolved = false;
	due->InstanceNumber = 0;
	gettimeofday(&due->startTime, NULL);
	due->completeTime.tv_sec = 0;
	due->completeTime.tv_usec = 0;
	due->transfer_status = DU_Informed_to_ACS;
	due->faultCode = NO_ERROR;
	due->initiator = NOTACS;

	addDeploymentUnitEntry(due);
	return returnCode;
}
#endif /* HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE */


/********* Function for work with deploymentUnitList *******/

/* Initialization Deployment Unit List */
static void initDeploymentUnitList()
{
	pthread_mutex_lock(&duList_usage_mutex);
	initList(&deploymentUnitList);
	pthread_mutex_unlock(&duList_usage_mutex);
}

/* Iterate  Deployment Unit List */
static ListEntry * iterateDeploymentUnitList(ListEntry * entry)
{
	pthread_mutex_lock(&duList_usage_mutex);
	entry = iterateList(&deploymentUnitList, entry);
	pthread_mutex_unlock(&duList_usage_mutex);
	return entry;
}

/* Add new Entry to Deployment Unit List */
static void addEntryToDeploymentUnitList(DeploymentUnitEntry * due)
{
	pthread_mutex_lock(&duList_usage_mutex);
	addEntry(&deploymentUnitList, due, MEM_TYPE_DU_LIST);
	pthread_mutex_unlock(&duList_usage_mutex);
}

/* Get first entry from Deployment Unit */
static ListEntry * getFirstEntryFromDeploymentUnitList()
{
	ListEntry * entry = NULL;
	pthread_mutex_lock(&duList_usage_mutex);
	entry = getFirstEntry(&deploymentUnitList);
	pthread_mutex_unlock(&duList_usage_mutex);
	return entry;
}

/* Remove entry from Deployment Unit */
static ListEntry * iterateRemoveFromDeploymentUnitList(ListEntry * entry)
{
	pthread_mutex_lock(&duList_usage_mutex);
	entry = iterateRemove (&deploymentUnitList, entry);
	pthread_mutex_unlock(&duList_usage_mutex);
	return entry;
}

int freeDUEntryMemory()
{
	deploymentUnitList.firstEntry = deploymentUnitList.lastEntry = NULL;
	efreeTypedMemByType(MEM_TYPE_DU_LIST, 0);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "freeDUEntryMemory() is completed.\n");
	)
	return OK;
}

#endif /* HAVE_DEPLOYMENT_UNIT */

#endif /* HAVE_FILE */

