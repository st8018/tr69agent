/***************************************************************************
 *    Original code ©Copyright (C) 2004-2012 by Dimark Software Inc.       *
 *    support@dimark.com                                                   *
 *    Modifications © 2013 Comcast Cable Communications, LLC               *
 ***************************************************************************/

#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <net/if.h>
#include <arpa/inet.h>

#include "dimark_globals.h"
#include "paramaccess.h"
#include "parameterStore.h"
#include "utils.h"
#include "parameter.h"
#include "ethParameter.h"
#include "diagParameter.h"
#include "voipParameter.h"
#include "diagParameter.h"
#include "list.h"
#include "callback.h"
#include "dimclient.h"
#include "aliasBasedAddressingUtils.h"
// Profiles:
#include "profiles/ipping_profile.h"
#include "profiles/time_profile.h"
#include "profiles/traceroute_profile.h"
#include "profiles/downloadDiagnostics_profile.h"
#include "profiles/uploadDiagnostics_profile.h"
#include "profiles/UDPEchoConfig_profile.h"

//Device.ManagementServer Getters
#include "profiles/Device_ManagementServer.h"
#include "profiles/X_TWC_COM_TEST.h"

#ifdef TR_181_DEVICE
#define ManagementServer_ManageableDevice_Object "Device.ManagementServer.ManageableDevice"
#elif defined(WITH_DEVICE_ROOT)

#else
#define ManagementServer_ManageableDevice_Object "InternetGatewayDevice.ManagementServer.ManageableDevice"
#endif


#define LAYER_3_FORWARD_SIZE 3

static int DeleteDebug (const char *, ParameterType, ParameterValue *);
static int getInt (const char *, ParameterType, ParameterValue *);
static int getString (const char *, ParameterType, ParameterValue *);
static int initInt (const char *, ParameterType, ParameterValue *);
static int initString (const char *, ParameterType, ParameterValue *);
static int setString (const char *, ParameterType, ParameterValue *);
static int setInt (const char *, ParameterType, ParameterValue *);
static int getUptime (const char *, ParameterType, ParameterValue *);
static int getLocalNetworkData (const char *);
static int getMacAddr (const char *, ParameterType, ParameterValue *);
static int createAndStoreCR_URL_path();
static int getCRU (const char *, ParameterType, ParameterValue *);
static int getOUI (const char *, ParameterType, ParameterValue *);
static int getManageableDeviceCount( const char *, ParameterType, ParameterValue *);
static int setLayer3ForwardingEnable( const char *, ParameterType, ParameterValue * );
static int getLayer3ForwardingEnable( const char *, ParameterType, ParameterValue * );
static int setLayer3ForwardingType( const char *, ParameterType, ParameterValue * );
static int getLayer3ForwardingType( const char *, ParameterType, ParameterValue * );
static int getLayer3ForwardingStatus (const char *, ParameterType, ParameterValue *);

static int setManagementServerURL(const char *, ParameterType , ParameterValue *);
static int setPeriodicInformInterval(const char *, ParameterType , ParameterValue *);
static int setManagementServerUsername(const char *, ParameterType , ParameterValue *);
static int setManagementServerPassword(const char *, ParameterType , ParameterValue *);
static int factoryReset();
static int rebootWithoutFactoryReset();

static int initAliasBasedAddressingParam(const char *, ParameterType , ParameterValue *);
static int setInstanceModeParam(const char *, ParameterType , ParameterValue *);
static int setAutoCreateInstancesParam(const char *, ParameterType , ParameterValue *);

extern int get_ParamValues_tr69hostIf (const char *, ParameterType, ParameterValue *);
extern int set_ParamValues_tr69hostIf (const char *, ParameterType, ParameterValue *);

extern int get_Device_DeviceInfo_SupportedDataModelNumberOfEntries(const char *, ParameterType, ParameterValue *);
extern int get_Device_DeviceInfo_SupportedDataModel_URL(const char *, ParameterType , ParameterValue *);
extern int get_Device_DeviceInfo_SupportedDataModel_URN(const char *, ParameterType , ParameterValue *);
extern int get_Device_DeviceInfo_SupportedDataModel_Features(const char *, ParameterType , ParameterValue *);

int addObjectCallbackDefault(const char *pathNameOfNewInstance, const char *pathNameOfNewInstanceAliasBased)
{
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "addObjectCallbackDefault(): pathNameOfNewInstance=\"%s\",\n\t\t\t\t\t\t"
				    "pathNameOfNewInstanceAliasBased =\"%s\"\n", pathNameOfNewInstance, pathNameOfNewInstanceAliasBased);
	)
	return OK;
}

int delObjectCallbackDefault(const char *pathNameOfNewInstance, const char *pathNameOfNewInstanceAliasBased)
{
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "addObjectCallbackDefault(): pathNameOfNewInstance=\"%s\",\n\t\t\t\t\t\t"
				    "pathNameOfNewInstanceAliasBased =\"%s\"\n", pathNameOfNewInstance, pathNameOfNewInstanceAliasBased);
	)
	return OK;
}


/* Functions are examples of using post SPV call back functionality */
//static int testSetter9999(const char *name, ParameterType type, ParameterValue *value);
//static int testCallBackFunctionFor9999();

static char macAddr[18] = { '\0' };
static char ip[257] = { '\0' };
static char cru[512] = { '\0' };
extern int procId;
extern char host[257];
extern char udp_host[257];
extern char ConnectionReguestURLPath[CR_URL_PATH_LEN+1];

extern List postSPVCbList;

static char oui[6] = { '\0' };

/******************************************************************************************/
//initialization callback functions for parameters
Func initArray[] = {
		{1, 	&storeParamValue},
		{2, 	&initString},
		{3, 	&initInt},
		{MARKED_NODE_FOR_ADDITIONAL_READ, &storeParamValue},
		{ALIASBASEDADDRESSING_INITIDX, 	&initAliasBasedAddressingParam},
#ifdef VOIP_DEVICE
		{10000,	&initVdMaxProfile},
		{10001,	&initVdMaxLine},
#endif /* VOIP_DEVICE */
};

static int initArraySize = sizeof (initArray) / sizeof (Func);

/******************************************************************************************/
//delete callback functions for parameters (not for objects)
Func deleteArray[] = {
		{1, 	&DeleteDebug},
};

static int deleteArraySize = sizeof (deleteArray) / sizeof (Func);

/******************************************************************************************/
Func getArray[] = {
		{1,		&retrieveParamValue},
		{2, 	&getInt},
		{3,		&getString},
		{112, 	&getOUI},
		{116, 	&getMacAddr},
		{117, 	&getCRU},
		{118, 	&getUdpCRU},
		{124, 	&getUptime},
		{200, 	&getManageableDeviceCount},

        //Device.ManagementServer Getters
        {235,   &get_Device_ManagementServer_ValidateManagementServerCertificate},
        {236,   &get_Device_ManagementServer_ValidateDownloadServerCertificate},
        {237,   &get_Device_ManagementServer_RootCertificateNumberOfEntries},

        //Device.ManagementServer.X_TWC_COM_RootCertificate Getters
        {241,   &get_Device_ManagementServer_X_TWC_COM_RootCertificate_Enabled},
        {242,   &get_Device_ManagementServer_X_TWC_COM_RootCertificate_Certificate},
        {243,   &get_Device_ManagementServer_X_TWC_COM_RootCertificate_Issuer},
        {244,   &get_Device_ManagementServer_X_TWC_COM_RootCertificate_Subject},

		{811, 	&getETH0EnabledForInternet},
		{812, 	&getETH0WANAccessType},
		{813, 	&getETH0Layer1UpstreamMaxBitRate},
		{817, 	&getETH0SentBytes},
		{818, 	&getETH0ReceivedBytes},
		{819, 	&getETH0SentPackets},
		{820, 	&getETH0ReceivedPackets},
		{2000,	&getLayer3ForwardingEnable},
		{2001, 	&getLayer3ForwardingStatus},
		{2002, 	&getLayer3ForwardingType},
#ifdef VOIP_DEVICE
		{10100, &getVdLineEnable},
		{10101, &getVdLineDirectoryNumber},
		{10200, &getVdLinePacketsSent},
#endif /* VOIP_DEVICE */
		// San. 07 may 2011: IPPing profile getters:
		{16000, &get_InternetGatewayDevice_IPPingDiagnostics_DiagnosticsState },
		{16001, &get_InternetGatewayDevice_IPPingDiagnostics_Interface },
		{16002, &get_InternetGatewayDevice_IPPingDiagnostics_Host },
		{16003, &get_InternetGatewayDevice_IPPingDiagnostics_NumberOfRepetitions },
		{16004, &get_InternetGatewayDevice_IPPingDiagnostics_Timeout },
		{16005, &get_InternetGatewayDevice_IPPingDiagnostics_DataBlockSize },
		{16006, &get_InternetGatewayDevice_IPPingDiagnostics_DSCP },
		{16007, &get_InternetGatewayDevice_IPPingDiagnostics_SuccessCount },
		{16008, &get_InternetGatewayDevice_IPPingDiagnostics_FailureCount },
		{16009, &get_InternetGatewayDevice_IPPingDiagnostics_AverageResponseTime },
		{16010, &get_InternetGatewayDevice_IPPingDiagnostics_MinimumResponseTime },
		{16011, &get_InternetGatewayDevice_IPPingDiagnostics_MaximumResponseTime },
		// gonchar. 12 may 2011: TraceRoute profile getters:
		{16500, &get_InternetGatewayDevice_TraceRouteDiagnostics_DiagnosticsState },
		{16501, &get_InternetGatewayDevice_TraceRouteDiagnostics_Interface },
		{16502, &get_InternetGatewayDevice_TraceRouteDiagnostics_Host },
		{16503, &get_InternetGatewayDevice_TraceRouteDiagnostics_NumberOfTries },
		{16504, &get_InternetGatewayDevice_TraceRouteDiagnostics_Timeout },
		{16505, &get_InternetGatewayDevice_TraceRouteDiagnostics_DataBlockSize },
		{16506, &get_InternetGatewayDevice_TraceRouteDiagnostics_DSCP },
		{16507, &get_InternetGatewayDevice_TraceRouteDiagnostics_MaxHopCount },
		{16508, &get_InternetGatewayDevice_TraceRouteDiagnostics_ResponseTime },
		{16509, &get_InternetGatewayDevice_TraceRouteDiagnostics_RouteHopsNumberOfEntries },
		{16510, &get_InternetGatewayDevice_TraceRouteDiagnostics_RouteHops_i_HopHost },
		{16511, &get_InternetGatewayDevice_TraceRouteDiagnostics_RouteHops_i_HopHostAddress },
		{16512, &get_InternetGatewayDevice_TraceRouteDiagnostics_RouteHops_i_HopErrorCode },
		{16513, &get_InternetGatewayDevice_TraceRouteDiagnostics_RouteHops_i_HopRTTimes },
		// gonchar. 18 may 2011: Time profile getters:
		{16600, &get_InternetGatewayDevice_Time_Enable },
		{16601, &get_InternetGatewayDevice_Time_Status },
		{16602, &get_InternetGatewayDevice_Time_NTPServer1 },
		{16603, &get_InternetGatewayDevice_Time_NTPServer2 },
		{16604, &get_InternetGatewayDevice_Time_NTPServer3 },
		{16605, &get_InternetGatewayDevice_Time_NTPServer4 },
		{16606, &get_InternetGatewayDevice_Time_NTPServer5 },
		{16607, &get_InternetGatewayDevice_Time_CurrentLocalTime },
		{16608, &get_InternetGatewayDevice_Time_LocalTimeZone },
		{16609, &get_InternetGatewayDevice_Time_LocalTimeZoneName },
		{16610, &get_InternetGatewayDevice_Time_DaylightSavingsUsed },
		{16611, &get_InternetGatewayDevice_Time_DaylightSavingsStart },
		{16612, &get_InternetGatewayDevice_Time_DaylightSavingsEnd },
		{18000, &get_ParamValues_tr69hostIf },
		// Device.DeviceInfo.SupportedDataModel Profiles Getters:
		{316,   &get_Device_DeviceInfo_SupportedDataModelNumberOfEntries},
    	{3901, &get_Device_DeviceInfo_SupportedDataModel_URL},
		{3902, &get_Device_DeviceInfo_SupportedDataModel_URN},
		{3903, &get_Device_DeviceInfo_SupportedDataModel_Features},

        {4000,   &get_Device_X_TWC_COM_TEST_UpgradeStatus}
};

static int getArraySize = sizeof (getArray) / sizeof (Func);

/******************************************************************************************/
Func setArray[] = {
		{1, 	&updateParamValue},
		{2, 	&setInt},
		{3,		&setString},
		{MANAGEMENT_SERVER_URL_SETIDX, 	&setManagementServerURL},
		{125,   &setPeriodicInformInterval},
		{INSTANCEMODE_SETIDX, 			&setInstanceModeParam},
		{AUTOCREATEINSTANCES_SETIDX, 	&setAutoCreateInstancesParam},
		{140,	&setManagementServerUsername},
		{141, 	&setManagementServerPassword},
        //Device.ManagementServer Getters
        {235,   &set_Device_ManagementServer_ValidateManagementServerCertificate},
        //Device.ManagementServer.X_TWC_COM_RootCertificate Setters
        {241,   &set_Device_ManagementServer_X_TWC_COM_RootCertificate_Enabled},
        {242,   &set_Device_ManagementServer_X_TWC_COM_RootCertificate_Certificate},

		{811, 	&setETH0EnabledForInternet},
#ifdef HAVE_DIAGNOSTICS
		{1010, 	&setWANDSLDiagnostics},
#endif /* HAVE_DIAGNOSTICS */
		{2000, 	&setLayer3ForwardingEnable},
		{2002, 	&setLayer3ForwardingType},
#ifdef VOIP_DEVICE
		{10100,	&setVdLineEnable},
		{10101,	&setVdLineDirectoryNumber},
#endif /* VOIP_DEVICE */
		// San. 07 may 2011: IPPing profile setters:
		{16000, &set_InternetGatewayDevice_IPPingDiagnostics_DiagnosticsState },
		{16001, &set_InternetGatewayDevice_IPPingDiagnostics_Interface },
		{16002, &set_InternetGatewayDevice_IPPingDiagnostics_Host },
		{16003, &set_InternetGatewayDevice_IPPingDiagnostics_NumberOfRepetitions },
		{16004, &set_InternetGatewayDevice_IPPingDiagnostics_Timeout },
		{16005, &set_InternetGatewayDevice_IPPingDiagnostics_DataBlockSize },
		{16006, &set_InternetGatewayDevice_IPPingDiagnostics_DSCP },
		// San. 31 Oct 2011: DownloadDiagnostic profile setters:
		{16100, &set_InternetGatewayDevice_DownloadDiagnostics_DiagnosticsState },
		{16101, &set_InternetGatewayDevice_DownloadDiagnostics_Interface },
		{16102, &set_InternetGatewayDevice_DownloadDiagnostics_DownloadURL },
		{16103, &set_InternetGatewayDevice_DownloadDiagnostics_DSCP },
		{16104, &set_InternetGatewayDevice_DownloadDiagnostics_EthernetPriority },
		// San. 31 Oct 2011: UploadDiagnostic profile setters:
		{16200, &set_InternetGatewayDevice_UploadDiagnostics_DiagnosticsState },
		{16201, &set_InternetGatewayDevice_UploadDiagnostics_Interface },
		{16202, &set_InternetGatewayDevice_UploadDiagnostics_UploadURL },
		{16203, &set_InternetGatewayDevice_UploadDiagnostics_DSCP },
		{16204, &set_InternetGatewayDevice_UploadDiagnostics_EthernetPriority },
		{16205, &set_InternetGatewayDevice_UploadDiagnostics_TestFileLength },
#ifdef HAVE_DIAGNOSTICS
		// San. 31 Oct 2011: ATMF5Diagnostic profile setters:
		{16300, &setATMF5Diagnostics },
		{16301, &set_WANATMF5LoopbackDiagnostics_NumberOfRepetitions },
		{16302, &set_WANATMF5LoopbackDiagnostics_Timeout },
#endif /* HAVE_DIAGNOSTICS */
		// San. 18 Jun 2012: UDPEchoConfig profile setters:
		{16400, &set_UDPEchoConfig_Enable },
		{16401, &set_UDPEchoConfig_Interface },
		{16402, &set_UDPEchoConfig_UDPPort },
		// gonchar. 12 may 2011: TraceRoute profile setters:
		{16500, &set_InternetGatewayDevice_TraceRouteDiagnostics_DiagnosticsState },
		{16501, &set_InternetGatewayDevice_TraceRouteDiagnostics_Interface },
		{16502, &set_InternetGatewayDevice_TraceRouteDiagnostics_Host },
		{16503, &set_InternetGatewayDevice_TraceRouteDiagnostics_NumberOfTries },
		{16504, &set_InternetGatewayDevice_TraceRouteDiagnostics_Timeout },
		{16505, &set_InternetGatewayDevice_TraceRouteDiagnostics_DataBlockSize },
		{16506, &set_InternetGatewayDevice_TraceRouteDiagnostics_DSCP },
		{16507, &set_InternetGatewayDevice_TraceRouteDiagnostics_MaxHopCount },
		// gonchar. 18 may 2011: Time profile setters:
		{16600, &set_InternetGatewayDevice_Time_Enable },
		{16602, &set_InternetGatewayDevice_Time_NTPServer1 },
		{16603, &set_InternetGatewayDevice_Time_NTPServer2 },
		{16604, &set_InternetGatewayDevice_Time_NTPServer3 },
		{16605, &set_InternetGatewayDevice_Time_NTPServer4 },
		{16606, &set_InternetGatewayDevice_Time_NTPServer5 },
		{16608, &set_InternetGatewayDevice_Time_LocalTimeZone },
		{16609, &set_InternetGatewayDevice_Time_LocalTimeZoneName },
		{16610, &set_InternetGatewayDevice_Time_DaylightSavingsUsed },
		{16611, &set_InternetGatewayDevice_Time_DaylightSavingsStart },
		{16612, &set_InternetGatewayDevice_Time_DaylightSavingsEnd },
		{18001, &set_ParamValues_tr69hostIf},

        {4101,   &set_Device_X_TWC_COM_TEST_DownloadImage},
        {4201,   &set_Device_X_TWC_COM_TEST_InstallImage}

//		/* Functions are examples of using post SPV call back functionality */
//		{9999, &testSetter9999 },
};

static int setArraySize = sizeof (setArray) / sizeof (Func);

/******************************************************************************************/
ObjectFunc addObjectArray[] = {
		{1, &addObjectCallbackDefault},
};

static int addObjectArraySize = sizeof (addObjectArray) / sizeof (Func);

/******************************************************************************************/
ObjectFunc delObjectArray[] = {
		{1, &delObjectCallbackDefault},
};

static int delObjectArraySize = sizeof (delObjectArray) / sizeof (Func);

/******************************************************************************************/
Func rebootArray[] = {
		//{Reboot1, 	&reboottest111}, //for example
		{RebootWithoutFactoryReset, 	&rebootWithoutFactoryReset},
		{FactoryReset, 	&factoryReset},
		/*{FlashImageReboot, 	&flashImageReboot}, */
};

static int rebootArraySize = sizeof (rebootArray) / sizeof (Func);


struct Layer3Forward
{
	bool enable;
	char *string;
	char *type;
};
struct Layer3Forward layer3Forward[LAYER_3_FORWARD_SIZE];

/** Calls the indexed function with the given parameters 
   the call is made through the initArray[] 
 */
int
initAccess (int idx, const char *name, ParameterType type, ParameterValue *value)
{
	register int i = 0;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "InitAccess: %d %s\n", idx, name);
	)
	for (i = 0; i != initArraySize; i++)
	{
		if (initArray[i].idx == idx)
			return initArray[i].func (name, type, value);
	}
	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_ACCESS, "InitAccess: %d %s\n", idx, name);
	)

	return ERR_INTERNAL_ERROR;
}

/** Calls the indexed function with the given parameters 
   the call is made through the deleteArray[] 
 */
int
deleteAccess (int idx, const char *name, ParameterType type, ParameterValue *value)
{
	register int i = 0;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "DeleteAccess: %d %s\n", idx, name);
	)

	for (i = 0; i != deleteArraySize; i++)
	{
		if (deleteArray[i].idx == idx)
			return deleteArray[i].func (name, type, value);
	}

	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_ACCESS, "DeleteAccess: %d %s\n", idx, name);
	)

	return ERR_INTERNAL_ERROR;
}

/** Calls the indexed function with the given parameters 
   the call is made through the getArray[] 
 */
int
getAccess (int idx, const char *name, ParameterType type, ParameterValue *value)
{
	register int i = 0;
	int ret = 0;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "GetAccess(): idx = %d, name = %s, value pointer = %p\n", idx, name, value);
	)

	for (i = 0; i != getArraySize; i++)
	{
		if (getArray[i].idx == idx)
		{
			ret = getArray[i].func (name, type, value);
			return ret;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_ACCESS, "GetAccess() error: idx = %d, name = %s, value pointer = %p\n", idx, name, value);
	)

	return ERR_INTERNAL_ERROR;
}

/** Calls the indexed function with the given parameters 
   the call is made through the setArray[] 
 */
int
setAccess (int idx, const char *name, ParameterType type, ParameterValue *value)
{
	register int i = 0;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "SetAccess: %d %s %p\n", idx, name, value);
	)

	for (i = 0; i != setArraySize; i++)
	{
		if (setArray[i].idx == idx)
			return setArray[i].func (name, type, value);
	}

	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_ACCESS, "SetAccess: %d %s %p\n", idx, name, value);
	)

	return ERR_INTERNAL_ERROR;
}

/** Calls the indexed function with the given parameters
   the call is made through the rebootArray[]
 */
int
rebootAccess ()
{
	int ret = OK;
	register int i = 0;
	unsigned int rebootIdx = getRebootIdx();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "RebootAccess: getRebootIdx() = 0x%.4X\n", rebootIdx);
	)

	for (i = 0; i != rebootArraySize; i++)
	{
		if ( (rebootIdx & (unsigned int)(rebootArray[i].idx < 0 ? 0 : rebootArray[i].idx)) != 0)
		{
			ret = rebootArray[i].func (NULL, rebootIdx, NULL);
			if (ret != OK)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_WARN, DBG_ACCESS, "RebootAccess: rebootArray[%d].idx = 0x%.4X, "
								"reboot function has returned ret = %d.\n", i, rebootArray[i].idx, ret);
				)
			}
		}
	}

	cleanRebootIdx();
	return OK;
}

/** Calls the indexed function with the given parameters
   the call is made through the addObjectArray[]
   objectName - Number based pathName of new added instance
   objectNameAliasBased - Alias based pathName of new added instance - if Alias mechanism is defined.
   	   	   	   	   	   	  Else: (char *)objectNameAliasBased == (char *)objectName)
 */
int
addObjectAccess (int idx, const char *objectName, const char *objectNameAliasBased)
{
	register int i = 0;
	int ret = 0;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "addObjectAccess(): idx = %d, objectName = %s\n", idx, objectName);
	)
	for (i = 0; i != addObjectArraySize; i++)
	{
		if (addObjectArray[i].idx == idx)
		{
			ret = addObjectArray[i].objCallback (objectName, objectNameAliasBased);
			return ret;
		}
	}
	DEBUG_OUTPUT (
			dbglog (SVR_WARN, DBG_ACCESS, "addObjectAccess() error: idx=%d isn't found, objectName = %s\n", idx, objectName);
	)
	return ERR_INTERNAL_ERROR;
}

/** Calls the indexed function with the given parameters
   the call is made through the delObjectArray[]
   objectName - Number based pathName of instance to delete
   objectNameAliasBased - Alias based pathName of instance to delete - if Alias mechanism is defined.
   	   	   	   	   	   	  Else: (char *)objectNameAliasBased == (char *)objectName)
 */
int
delObjectAccess (int idx, const char *objectName, const char *objectNameAliasBased)
{
	register int i = 0;
	int ret = 0;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "delObjectAccess(): idx = %d, objectName = %s\n", idx, objectName);
	)
	for (i = 0; i != delObjectArraySize; i++)
	{
		if (delObjectArray[i].idx == idx)
		{
			ret = delObjectArray[i].objCallback (objectName, objectNameAliasBased);
			return ret;
		}
	}
	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_ACCESS, "delObjectAccess() error: idx=%d isn't found, objectName = %s\n", idx, objectName);
	)
	return ERR_INTERNAL_ERROR;
}


/** Debug function, do nothing then to print the parameter name and type
 */
int
DeleteDebug (const char *name, ParameterType type, ParameterValue *value)
{
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "DeleteDebug: %s %d\n", name, type);
	)

	setParameterReturnStatusSafe(PARAMETER_CHANGES_NOT_APPLIED);

	return OK;
}

/** Sample Functions
 */
static int
getString (const char *name, ParameterType type, ParameterValue *value)
{
	value->out_cval = "Test";

	return OK;
}

static int
getInt (const char *name, ParameterType type, ParameterValue *value)
{
	int x = 1234;
	value->out_int = x;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "GetInt %s %d\n", name, value->out_int);
	)

	return OK;
}

static int
initString(const char *name, ParameterType type, ParameterValue *value)
{
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "Init string: %s %d\n", name, type);
	)

	return OK;
}

static int
initInt(const char *name, ParameterType type, ParameterValue *value)
{
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "Init int: %s %d\n", name, type);
	)

	return OK;
}

static int
setString (const char *name, ParameterType type, ParameterValue *value)
{
#ifdef _DEBUG
	char *in = value->in_cval;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "SetString %s %s\n", name, in);
	)
#endif /* _DEBUG */
	return OK;
}

static int
setInt (const char *name, ParameterType type, ParameterValue *value)
{
#ifdef _DEBUG
	int in = value->in_int;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACCESS, "SetInt %s %d\n", name, in);
	)
#endif /* _DEBUG */
	return OK;
}

static int
getUptime (const char *name, ParameterType type, ParameterValue *value)
{
	struct sysinfo info;

	sysinfo (&info);
	value->out_uint = (unsigned int) info.uptime;

	return OK;
}


static int
getLocalNetworkData (const char *interfaceName)
{
	int sfd;
	unsigned char *u;
	struct ifreq ifr;
	struct sockaddr_in *sin = (struct sockaddr_in *) &ifr.ifr_addr;

	if (!interfaceName)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "getLocalNetworkData(): function parameter 'interfaceName' can't be NULL.\n");
		)
		return ERR_INTERNAL_ERROR;
	}

	memset (&ifr, 0, sizeof ifr);
	if (0 > (sfd = socket (PF_INET, SOCK_STREAM, 0)))
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "getLocalNetworkData socket failure\n");
		)

		return ERR_INTERNAL_ERROR;
	}

	strcpy (ifr.ifr_name, interfaceName);
	sin->sin_family = AF_INET;

	if (0 > ioctl (sfd, SIOCGIFHWADDR, &ifr))
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "getLocalNetworkData: getMacAddr\n");
		)

		return ERR_INTERNAL_ERROR;
	}
	else
	{
		u = (unsigned char *) &ifr.ifr_addr.sa_data;
		if (u[0] + u[1] + u[2] + u[3] + u[4] + u[5])
		{
			sprintf (macAddr, "%2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X", u[0], u[1], u[2], u[3], u[4], u[5]);
		}
	}

	return OK;
}

static int
getMacAddr (const char *name, ParameterType type, ParameterValue *value)
{
	int ret;

	// If the MacAddress is already there ( macAddr[0] != '\0' ) we get it from
	// the buffer
	if (macAddr[0] == '\0') {
		ret = getLocalNetworkData ("eth0");
		if (ret != OK) {
			ret = getLocalNetworkData ("eth2");
			if ( ret != OK ) 
				return ret;
		}
	}
	value->out_cval = macAddr;

	return OK;
}


/* This function checks the ConnectionReguestURLPath.
 * If ConnectionReguestURLPath contains only symbols from [A..Z, a..z, 0..9]
 * then it is valid, and will be returned OK.
 * Else return !OK;
 * */
static int chechCR_URL_path(char tmpConnectionReguestURLPath[CR_URL_PATH_LEN + 1])
{
	unsigned int i;

	for (i=0; i < CR_URL_PATH_LEN; i++)
		if ( tmpConnectionReguestURLPath[i]==0 || !isalnum( tmpConnectionReguestURLPath[i] ) )
			return !OK;

	return OK;
}

static int createAndStoreCR_URL_path()
{
	char * CR_URL_path;
	FILE * f = NULL;
	unsigned int storageExists = 0;

	memset(ConnectionReguestURLPath, 0, sizeof(ConnectionReguestURLPath) );

	CR_URL_path = customerCreationOfCR_URL_path();
	if (CR_URL_path && (chechCR_URL_path(CR_URL_path) == OK))
	{
		strncpy(ConnectionReguestURLPath, CR_URL_path, CR_URL_PATH_LEN );
	}
	else
	{
		f = fopen(CR_URL_PATH_STORAGE_FILE_NAME, "r");
		if (!f || !fgets( ConnectionReguestURLPath, CR_URL_PATH_LEN + 1, f) || (chechCR_URL_path(ConnectionReguestURLPath) != OK) )
		{
			srand(time(NULL));
			int i;
			for (i = 0; i < CR_URL_PATH_LEN; i++)
			{
				unsigned int r = ((unsigned int)rand()) % 3; // r == 0 or 1 or 2. 0 -> [0..9], 1->[A..Z], 2->[a..z]
				ConnectionReguestURLPath[i] = r ? ((r-1 ? 'a' : 'A') + (rand()%26)) : ('0' + (rand()%9)) ;
			}
		}
		else
		{
			storageExists = 1;
		}
		if (f)
			fclose(f);
	}
	ConnectionReguestURLPath[CR_URL_PATH_LEN] = '\0';

	if (!storageExists)
	{
		f = fopen(CR_URL_PATH_STORAGE_FILE_NAME, "w");
		if (f)
		{
			fprintf(f, "%s",  ConnectionReguestURLPath);
			fclose(f);
		}
	}

	return OK;
}

/** Get the ConnectionRequestURL, which is the URL the ACS is using to trigger the CPE
 * Form:  http://<cpe ip addr>/acscall
 */
static int
getCRU (const char *name, ParameterType type, ParameterValue *value)
{
	int ret;
	int i;
	char * newCRUrl;
	static unsigned int ConnectionReguestURLpathIsCreated = false;

	/* generate ranom connection reguest path */
	if(ConnectionReguestURLpathIsCreated == false)
	{
		createAndStoreCR_URL_path();

		ConnectionReguestURLpathIsCreated = true;
	}

	char tmpHost[257];
	int isHostInIPv6Format;

	if (OK != getHostStrFromURL(host, &tmpHost[0], &isHostInIPv6Format))
		return ERR_INTERNAL_ERROR;

	strcpy(host, tmpHost);

#ifdef WITH_IPV6
	int isZeroAdress = 1;
	int j, n = strlen(host);

	if (n == 0)
		isZeroAdress = 0;
	else
	{
		// San: Check IPv6 adress. If it contains only '0' and ':' then it is a zero adress (for example "::"). See standard for IPv6
		for (j=0; j<n; j++)
		{
			if (host[j] != '0' && host[j] != ':')
			{
				isZeroAdress = 0;
				break;
			}
		}
	}

	isZeroAdress = isZeroAdress || !strcmp(host, "::ffff:0.0.0.0") || !strcmp(host, "0.0.0.0");

	if(*host && !isZeroAdress)
	{
		/* no host != "" */
		if (isHostInIPv6Format)
		{
			// if IPv6:
			sprintf(cru, "http://[%s]:%d/%s", host , ACS_NOTIFICATION_PORT + (procId), ConnectionReguestURLPath);
		}
		else
		{
			sprintf(cru, "http://%s:%d/%s", host , ACS_NOTIFICATION_PORT + (procId), ConnectionReguestURLPath);
		}
		strcpy(ip, host);
		value->out_cval = cru;
	}
#else
	if(*host && strcmp(host, "0.0.0.0"))
	{
		/* no host != 0.0.0.0 */
		sprintf(cru, "http://%s:%d/%s", host , ACS_NOTIFICATION_PORT + (procId), ConnectionReguestURLPath);
		strcpy(ip, host);
		value->out_cval = cru;
	}
#endif
	else
	{
		/* yes  host == "" or host == 0.0.0.0 or IPv6 zero adress*/ /*get CRU from data model */
		int ret = retrieveParamValue(name, type, value);
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_ACCESS, "getCRU()->retrieveParamValue() returns error = %i\n",ret);
			)
			if (host[0] == 0)
			{
#ifdef WITH_IPV6
				strcpy(host, "[::]");
#else
				strcpy(host, "0.0.0.0");
#endif
			}
			return ret;
		}

		if (!value)
			return ERR_INTERNAL_ERROR;

		if (!value->out_cval)
			return ERR_INTERNAL_ERROR;

		if (strlen(value->out_cval) == 0)
		{
			if (host[0] == 0)
			{
#ifdef WITH_IPV6
				strcpy(host, "[::]");
#else
				strcpy(host, "0.0.0.0");
#endif
			}
			cru[0] = '\0';
			ip[0] = '\0';
			return OK;
		}

		char *p = strchr(value->out_cval, '/');
		if (!p)
			return ERR_INTERNAL_ERROR;
		p = strchr(p+1, '/');
		if (!p)
			return ERR_INTERNAL_ERROR;
		p = strchr(p+1, '/');
		if (p)
		{
			newCRUrl = (char *)emallocTemp( (p - value->out_cval +1) + CR_URL_PATH_LEN + 1,  38);
			strncpy( newCRUrl, value->out_cval,  p - value->out_cval +1 );
		}
		else
		{
			newCRUrl = (char *)emallocTemp( strlen(value->out_cval) + CR_URL_PATH_LEN + 2,  38); // +1 is for '/' before CR_URL_PATH_LEN and +1 is for end of string.
			strcpy( newCRUrl, value->out_cval);
			strcat( newCRUrl, "/");
		}

		strncat( newCRUrl, ConnectionReguestURLPath, CR_URL_PATH_LEN);

		strcpy(cru, newCRUrl);
		value->out_cval = newCRUrl;

		updateParamValue(name, type, value);

		if (OK != getHostStrFromURL(cru, &ip[0], NULL))
		{
			ip[0] = '\0';
		}

		if(ip[0] == '\0')
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_ACCESS, "getCRU: Not found ip address\n");
			)
			if (host[0] == 0)
			{
#ifdef WITH_IPV6
				strcpy(host, "[::]");
#else
				strcpy(host, "0.0.0.0");
#endif
			}
		}
		else
		{
			if (host[0] == '\0')
				strcpy(host, ip);
		}
	}

	return OK;
}

/*
 * Get the UDPConnectionRequestAddress, which is the URL the ACS is using to trigger the CPE
 * Form:  <cpe ip addr>:port 
 * IPv4 and IPv6 are supported
 * Edited by San, Sep 2012.
 */
int
getUdpCRU (const char *name, ParameterType type, ParameterValue *value)
{
	int ret;
	int isHostInIPv6Format = 0;
	char tmpHost[257];

	//1 - try to read the value from DB
	ret = retrieveParamValue(UDPCONNECTIONREQUESTADDRESS, StringType, value);
	if ( ret == OK && value->out_cval && *(value->out_cval) )
	{
		// if ret == OK and value of parameter is not Empty then return this value
		return OK;
	}
	// else get value from udp_host + ACS_UDP_NOTIFICATION_PORT:

	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_ACCESS, "getUdpCRU()->retrieveParamValue(%s) ret = %d\n", UDPCONNECTIONREQUESTADDRESS, ret);
		)
	}

	// get Host Str From udp_host
	getHostStrFromURL(udp_host, tmpHost, &isHostInIPv6Format);

	strcpy(udp_host, tmpHost);

	if (isHostInIPv6Format)
	{
		// if IPv6:
		sprintf(tmpHost, "[%s]:%d", udp_host , ACS_UDP_NOTIFICATION_PORT);
	}
	else
	{
		sprintf(tmpHost, "%s:%d", udp_host , ACS_UDP_NOTIFICATION_PORT);
	}

	value->out_cval = strnDupByMemType(value->out_cval, tmpHost, strlen(tmpHost), MEM_TYPE_TEMP);
	return OK;
}

/* Initialization of udp_host string.
 * 1). Read value from -d command-line parameter
 *     If empty:
 *     2). try to read from DB (UDPCONNECTIONREQUESTADDRESS parameter), if error then set 0.0.0.0
 * Created by San, Sep 2012.
 */
int udpHostInitialization()
{
	int ret = OK;
	char tmpHost[257] = {0};
	ParameterValue value;
	int isHostInIPv6Format = 0;

	value.in_cval = NULL;

	if (*udp_host)
	{
		// -d command-line parameter isn't empty
		strcpy(tmpHost, udp_host);
	}
	else
	{
		//try to read the value from DB
		ret = retrieveParamValue(UDPCONNECTIONREQUESTADDRESS, StringType, &value);
		if ( ret != OK || !value.out_cval || !(*(value.out_cval)) )
		{
			// if value of parameter is Empty then use 0.0.0.0
			strcpy(tmpHost, "0.0.0.0");
			value.in_cval = NULL;
		}
		else
			strcpy(tmpHost, value.out_cval);
	}

	getHostStrFromURL(tmpHost, udp_host, &isHostInIPv6Format);

	// Save UDPConnectionRequestAddress parameter to DB.
	if (isHostInIPv6Format)
	{
		// if IPv6:
		sprintf(tmpHost, "[%s]:%d", udp_host , ACS_UDP_NOTIFICATION_PORT);
	}
	else
	{
		sprintf(tmpHost, "%s:%d", udp_host , ACS_UDP_NOTIFICATION_PORT);
	}

	if (!value.out_cval || strcmp(tmpHost, value.out_cval))
	{
		value.in_cval = tmpHost;
		updateParamValue(UDPCONNECTIONREQUESTADDRESS, StringType, &value);
	}

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_ACCESS, "udpHostInitialization(): 'udp_host' = \"%s\".\n", udp_host);
	)
	return OK;
}

/* 
 * Test IP address to see if matches udp_host address previously found
 * by call to getUdpCRU
 *
 */
int 
is_IP_local( char *ipAddress, int ipAddressLen )
{
	if (!ipAddress)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "is_IP_local(): function parameter 'ip_addr' can't be NULL.\n");
		)
		return (-1);
	}

	if ( strncmp( udp_host, ipAddress, ipAddressLen ) == 0 )
		return(0);

	return(-1);
}

/** The OUI are the first 24 bits ( 6 chars ) of the MAC Address
 */
static int
getOUI (const char *name, ParameterType type, ParameterValue *value)
{
	char *dummy;
	int ret;

	if (oui[0] == '\0')
	{
		ret = getMacAddr (name, type, (ParameterValue *)&dummy);
		if (ret == OK)
			strncpy (oui, dummy, 6);
	}
	value->out_cval = oui;

	return OK;
}

static int
getManageableDeviceCount( const char *name, ParameterType type, ParameterValue *value )
{
	int ret = OK;
#ifdef ManagementServer_ManageableDevice_Object
	int count;

	ret = countInstances( ManagementServer_ManageableDevice_Object, &count );
	if ( ret == OK )
		value->out_int = count;
#else
	value->out_int = 0;
#endif
	return ret;
}

static int
setLayer3ForwardingEnable (const char *name, ParameterType type, ParameterValue *value)
{
	register int idx;

	idx = getRevIdx (name, "Enable");
	if (idx > 0 && idx <= LAYER_3_FORWARD_SIZE)
	{
		// align index for faster access
		idx--;
		layer3Forward[idx].enable = value->in_int;
		return OK;
	}
	else
	{
		return ERR_INVALID_PARAMETER_NAME;
	}
}

static int
getLayer3ForwardingEnable (const char *name, ParameterType type, ParameterValue *value)
{
	register int idx;

	idx = getRevIdx (name, "Enable");
	if (idx > 0 && idx <= LAYER_3_FORWARD_SIZE)
	{
		idx--;
		BOOL_GET value = layer3Forward[idx].enable;
		return OK;
	}
	else
	{
		return ERR_INVALID_PARAMETER_NAME;
	}
}

int
getLayer3ForwardingStatus (const char *name, ParameterType type, ParameterValue *value)
{
	return OK;
}

static int
setLayer3ForwardingType (const char *name, ParameterType type, ParameterValue *value)
{
	register int idx = 0;
	register char **l3type;

	idx = getRevIdx (name, "Type");
	if (idx > 0 && idx <= LAYER_3_FORWARD_SIZE)
	{
		idx--;
		l3type = &layer3Forward[idx].type;
		if (*l3type != NULL)
			efreeTypedMemByPointer((void**)&(*l3type), 0);
		*l3type = strnDupByMemType(*l3type, value->in_cval, strlen (value->in_cval), MEM_TYPE_PARAM_ENTRY);
		return OK;
	}
	else
	{
		return ERR_INVALID_PARAMETER_NAME;
	}
}

static int
getLayer3ForwardingType (const char *name, ParameterType type, ParameterValue *value)
{
	register int idx = 0;
	// register char **l3type;

	idx = getRevIdx (name, "Type");
	if (idx > 0 && idx <= LAYER_3_FORWARD_SIZE)
	{
		idx--;
		STRING_GET value = layer3Forward[idx].type;
		return OK;
	}
	else
	{
		return ERR_INVALID_PARAMETER_NAME;
	}
}

/**
 * Setter: the ACS connection URL
 */
static int
setManagementServerURL(const char *name, ParameterType type, ParameterValue *value)
{
	int   ret = OK;

	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS,"setManagementServerURL(): updateParameterValue() has returned error = %d\n", ret);
		)
		return ret;
	}

	if (strcmp(value->out_cval, getServerURL()))
	{
		setAsyncInform(true);
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_ACCESS,"setManagementServerURL(): Parameter \"%s\" is changed. Inform will be initiated.\n", MANAGEMENT_SERVER_URL);
		)
	}
	return OK;
}

/**
 * Setter: the PeriodicInformInterval parameter value
 */
static int
setPeriodicInformInterval(const char *name, ParameterType type, ParameterValue *value)
{
	int   ret = OK;

	if (value->in_uint < 1)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS,"setPeriodicInformInterval(): value of this parameter can not be < 1. Error %d.\n", ERR_INVALID_PARAMETER_VALUE);
		)
		return ERR_INVALID_PARAMETER_VALUE;

	}

	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS,"setPeriodicInformInterval(): updateParameterValue() has returned error = %d\n", ret);
		)
		return ret;
	}
	return OK;
}

/**
 * Setter: the ManagementServer.Username
 */
static int
setManagementServerUsername(const char *name, ParameterType type, ParameterValue *value)
{
	int   ret = OK;

	setParameterReturnStatusSafe(PARAMETER_CHANGES_NOT_APPLIED);

	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS,"setManagementServerUsername()->updateParameterValue() has returned error = %d\n", ret);
		)
		return ret;
	}

	return OK;
}


/**
 * Setter: the ManagementServer.Password
 */
static int
setManagementServerPassword(const char *name, ParameterType type, ParameterValue *value)
{
	int   ret = OK;

	setParameterReturnStatusSafe(PARAMETER_CHANGES_NOT_APPLIED);

	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS,"setManagementServerPassword()->updateParameterValue() has returned error = %d\n", ret);
		)
		return ret;
	}

	return OK;
}


/** Created by San. Sep 2012.
 *  return ParameterValue copy in the dstValue, it is the copy instance of srcValue.
 *  if dstValue is NULL, then memory will be allocated with memory type = memTypeIdx.
 *  if dstValue is not NULL then will be used this pointer.
 *  if no errors then function returns OK.
 */
int getParameterValueCopy(ParameterValue *srcValue, ParameterValue *dstValue, ParameterType type, unsigned int memTypeIdx)
{
	int ret = OK;

	if (!srcValue)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "getParameterValueCopy(): function parameter 'srcValue' cannot be NULL.\n");
		)
		return -1;
	}

	if (!dstValue)
	{
		dstValue = emallocTypedMem(sizeof(ParameterValue), memTypeIdx, 0);
		if (!dstValue)
			return -2;
	}

	switch (type)
	{
		case IntegerType:
		case DefIntegerType:
			dstValue->out_int = srcValue->out_int;
			break;
		case UnsignedIntType:
		case DefUnsignedIntType:
			dstValue->out_uint = srcValue->out_uint;
			break;
		case StringType:
		case Base64Type:
		case DefStringType:
		case DefBase64Type:
			if (srcValue->out_cval != NULL)
			{
				dstValue->out_cval = strnDupByMemType (dstValue->out_cval, srcValue->out_cval,
														strlen(srcValue->out_cval), memTypeIdx);
			}
			else
				dstValue->out_cval = NULL;
			break;
		case BooleanType:
		case DefBooleanType:
			dstValue->out_int = srcValue->out_int;
			break;
		case DateTimeType:
		case DefDateTimeType:
			dstValue->out_timet.tv_sec = srcValue->out_timet.tv_sec;
			dstValue->out_timet.tv_usec = srcValue->out_timet.tv_usec;
			break;
		case UnsignedLongType:
		case DefUnsignedLongType:
			dstValue->out_ulong = srcValue->out_ulong;
			break;
		case DefHexBinaryType:
		case hexBinaryType:
			if (srcValue->out_hexBin != NULL)
			{
				dstValue->out_hexBin = strnDupByMemType (dstValue->out_hexBin, srcValue->out_hexBin,
														strlen(srcValue->out_hexBin), memTypeIdx);
			}
			else
				dstValue->out_hexBin = NULL;
			break;
		default:
			ret = ERR_INVALID_PARAMETER_TYPE;
			break;
	}

	return ret;
}

///* Functions are examples of using post SPV call back functionality
// *
// * */
//static int testSetter9999(const char *name, ParameterType type, ParameterValue *value)
//{
//  addSPVCallback(&testCallBackFunctionFor9999, name, type, value);
//	printf("\n\n testSetter9999(): SETTER 9999: CALLBACK AFTER SPV ADDED, name=[%s]\n\n", name);
//	fflush(stdout);
//	return OK;
//}
//
//static int testCallBackFunctionFor9999(const char * name, ParameterValue *value)
//{
//	int ret;
//	printf("\n\n testCallBackFunctionFor9999(): CALLBACK AFTER SPV!!!!!!, name=[%s]\n\n", name);
//	fflush(stdout);
//	//Some actions (after SPV is successful) and save parameter value to DB.
//	ret = updateParamValue(name, <InputParamType>, value);
//	return ret;
//}

/* This function must be called in the FactoryReset callback
 * if integrator implements his own callback function instead Dimark callback.
 * */
int dimarkPreparationToFactoryReset()
{
	int ret;
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_MAIN, "The beginning of preparation for FactoryReset\n");
	)
#ifdef CUSTOMER_FINISHING_FUNCTION
	customerFunctionAtFinish();
#endif
	cleanCallbackLists();
#ifdef HAVE_VOUCHERS_OPTIONS
	resetAllOptions ();
#endif /* HAVE_VOUCHERS_OPTIONS */

#ifdef HAVE_FILE
#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
	resetAllFiletransfers();
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */
#ifdef HAVE_DEPLOYMENT_UNIT
	resetAllDeploymentUnitEntry();
#endif /* HAVE_DEPLOYMENT_UNIT */
#endif /* HAVE_FILE */

#ifdef HAVE_SCHEDULE_INFORM
	clearScheduleInformList();
#endif

#ifdef VOIP_DEVICE
	// freeing memory after using VoIP Parameters and devices
	freeVoIPAllocatedMemory();
#endif /* VOIP_DEVICE */
	clearFault();
#ifdef WITH_USE_SQLITE_DB
	ret = closeDBConnection(0);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_MAIN, "main()->closeDBConnection() 1 has returned ret = %d\n", ret );
	)
#endif /* WITH_USE_SQLITE_DB */
	killAllThreads();
	resetAllParameters ();
	deleteBootstrapMarker();
	freeEventList(true);
	doneAllSoapInstances();
	efreeAllTypedMem(0);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_MAIN, "Do FactoryReset\n");
	)
#ifdef HAVE_COLOR_SCREEN_LOG
	printf ("\033[%um", C_Reset);
#endif /* HAVE_COLOR_SCREEN_LOG */
	cleanRebootIdx();
	return OK;
}

static int factoryReset(const char *name, ParameterType type, ParameterValue *value)
{
#ifdef HAVE_FACTORY_RESET
	ParameterValue frValue;

	name = "Device.DeviceInfo.X_COMCAST-COM_Reset";
	frValue.in_cval = (char *)emallocTemp(strlen("Factory")+1, 1);
	strncpy(frValue.in_cval, "Factory", strlen("Factory")+1);
	DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "The beginning of preparation for Factory Reset RPC.\n");
				dbglog (SVR_ERROR, DBG_MAIN, "Sending request to tr69hostif for Parameter name \'%s\' and value as \'%s\'.\n",
						name, frValue.in_cval);
		)
	set_ParamValues_tr69hostIf(name, StringType, &frValue);
	dimarkPreparationToFactoryReset();
	//exit(1);
#endif /* HAVE_FACTORY_RESET */
	return OK;
}


/* This function must be called in the Reboot callback
 * if integrator implements his own callback function instead Dimark callback.
 * */
int dimarkPreparationToReboot()
{
	int ret;
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_MAIN, "The beginning of preparation for Reboot\n");
	)
#ifdef CUSTOMER_FINISHING_FUNCTION
	customerFunctionAtFinish();
#endif
	cleanCallbackLists();
#ifdef HAVE_SCHEDULE_INFORM
	clearScheduleInformList();
#endif
#ifdef HAVE_FILE
#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
	freeTransferEntryMemory();
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */
#ifdef HAVE_DEPLOYMENT_UNIT
	freeDUEntryMemory();
#endif /* HAVE_DEPLOYMENT_UNIT */
#endif /* HAVE_FILE */
#ifdef VOIP_DEVICE
	// freeing memory after using VoIP Parameters and devices
	freeVoIPAllocatedMemory();
#endif /* VOIP_DEVICE */
	freeEventList(false);
	clearFault();
#ifdef WITH_USE_SQLITE_DB
	ret = closeDBConnection(0);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_MAIN, "main()->closeDBConnection() 2 has returned ret = %d\n", ret );
	)
#endif /* WITH_USE_SQLITE_DB */
	killAllThreads();
	doneAllSoapInstances();
	efreeAllTypedMem(0);
	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_MAIN, "Do Reboot\n");
	)
#ifdef HAVE_COLOR_SCREEN_LOG
	printf ("\033[%um", C_Reset);
#endif /* HAVE_COLOR_SCREEN_LOG */
	cleanRebootIdx();
	return OK;
}


static int rebootWithoutFactoryReset(const char *name, ParameterType type, ParameterValue *value)
{
	/*Commented, since reboot call was hanged in killAllThreads. 
	In case of reboot, system reboots, it doesn't require to kill explicitly, so commented. */
	//dimarkPreparationToReboot();
	callRebootScript();
	exit(1);
	return OK;
}

/**
 * Setter for <ROOT>.ManagementServer.AliasBasedAddressing
 */
static int
initAliasBasedAddressingParam(const char *name, ParameterType type, ParameterValue *value)
{
	int   ret = OK;
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	ret = storeParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS,"initAliasBasedAddressingParam(): storeParamValue() 1 has returned error = %d\n", ret);
		)
		return ret;
	}
	setAliasBasedAddressing(value->in_int);
#else
	value->in_int = 0;
	ret = storeParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS,"initAliasBasedAddressingParam(): storeParamValue() 2 has returned error = %d\n", ret);
		)
		return ret;
	}
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */
	return OK;
}


static int setInstanceModeParam(const char *name, ParameterType type, ParameterValue *value)
{
	int   ret = OK;

	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS,"setInstanceModeParam(): updateParameterValue() has returned error = %d\n", ret);
		)
		return ret;
	}
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	if (value->in_cval && !strcmp(value->in_cval, "InstanceAlias"))
		setInstanceMode(InstanceAlias);
	else
		setInstanceMode(InstanceNumber);
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */
	return OK;
}

static int setAutoCreateInstancesParam(const char *name, ParameterType type, ParameterValue *value)
{
	int   ret = OK;

	ret = updateParamValue(name, type, value);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS,"setAutoCreateInstancesParam(): updateParameterValue() has returned error = %d\n", ret);
		)
		return ret;
	}
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	setAutoCreateInstances(value->in_int);
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */
	return OK;
}
