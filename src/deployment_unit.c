/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

/*
 * deployment_unit.c
 *
 *  Created on: 17.01.2012
 *      Author: apnt
 */

#include "dimark_globals.h"
#include "deployment_unit.h"

#ifdef HAVE_DEPLOYMENT_UNIT
int processDeploymentUnit(DeploymentUnitEntry * due);
int processInstallDeploymentUnitEntry(DeploymentUnitEntry * due);
int processUploadDeploymentUnitEntry(DeploymentUnitEntry * due);
int processUninstallDeploymentUnitEntry(DeploymentUnitEntry * due);

/* work with data-model */
int InstallInstanseDeploymentUnitInDataBase(DeploymentUnitEntry * due);
int UpdateInstanseDeploymentUnitInDataBase(DeploymentUnitEntry * due);
int UninstallInstanseDeploymentUnitInDataBase(DeploymentUnitEntry * due);

extern pthread_mutex_t paramLock;

/**************************************************************************************/
int processDeploymentUnit(DeploymentUnitEntry * due)
{
	int ret = OK;

	if(due->du_status == INSTALLING)
	{
		ret = processInstallDeploymentUnitEntry(due);
		return ret;
	}

	if(due->du_status == UPDATING)
	{
		ret = processUploadDeploymentUnitEntry(due);
		return ret;
	}

	if(due->du_status == UNINSTALLING)
	{
		ret = processUninstallDeploymentUnitEntry(due);
		return ret;
	}

	/* generate error */
	due->faultCode = ERR_INVALID_DU_STATE;
	/* set fault */
	setDeploymentUnitEntryFault(due);
	due->du_status = FAILED;

	return ERR_INVALID_DU_STATE;
}


/**************************************************************************************/
int processInstallDeploymentUnitEntry(DeploymentUnitEntry * due)
{
	int ret = OK;

	/* TODO make install */
	//execInstall()
	due->du_status = INSTALLED;
	strnCopy(due->Version, "1.0.0", (sizeof(due->Version) -1)); /* TODO Correct this */

	/* add instanse to data-model */
	if(due->du_status == INSTALLED)
	{
		ret = InstallInstanseDeploymentUnitInDataBase(due);

		struct timeval currTime;
		gettimeofday(&currTime, NULL);
		due->completeTime.tv_sec = currTime.tv_sec;
		due->completeTime.tv_usec = currTime.tv_usec;
	}

	if(due->initiator == ACS)
	{
		/* create answer for DUStateChangeComplete method */
		due->result.UUID = due->UUID;
		due->result.DeploymentUnitRef = due->DeploymentUnitRef;
		due->result.Version = due->Version;
		due->result.CurrentState = getDeploymentUnitStatus(due->du_status);
		due->result.ExecutionUnitRefList = due->ExecutionEnvRef;
		due->result.Resolved = due->Resolved;
		due->result.StartTime.tv_sec = due->startTime.tv_sec;
		due->result.StartTime.tv_usec = due->startTime.tv_usec;
		due->result.CompleteTime.tv_sec = due->completeTime.tv_sec;
		due->result.CompleteTime.tv_usec = due->completeTime.tv_usec;
		due->result.fault.FaultString = due->faultString;
		due->result.fault.FaultCode = due->faultCode;
	}

	if(due->initiator == NOTACS)
	{
#ifdef	 HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE
		/* create answer for AutonomusDUStateChangeComplete */
		due->autonomous_result.UUID = due->UUID;
		due->autonomous_result.DeploymentUnitRef = due->DeploymentUnitRef;
		due->autonomous_result.Version = due->Version;
		due->autonomous_result.CurrentState = getDeploymentUnitStatus(due->du_status);
		due->autonomous_result.Resolved = due->Resolved;
		due->autonomous_result.ExecutionUnitRefList = due->ExecutionEnvRef;
		due->autonomous_result.StartTime.tv_sec = due->startTime.tv_sec;
		due->autonomous_result.StartTime.tv_usec = due->startTime.tv_usec;
		due->autonomous_result.CompleteTime.tv_sec = due->completeTime.tv_sec;
		due->autonomous_result.CompleteTime.tv_usec = due->completeTime.tv_usec;
		due->autonomous_result.fault.FaultString = due->faultString;
		due->autonomous_result.fault.FaultCode = due->faultCode;
		due->autonomous_result.OperationPerformed = getDeploymentUnitType(due->type);
#endif /* HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE */
	}
	return ret;
}

/**************************************************************************************/
int processUploadDeploymentUnitEntry(DeploymentUnitEntry * due)
{
	int ret = OK;

	/* TODO make update */
	//execUpdate()
	due->du_status = UPDATED;

	/* add instanse to data-model */
	if(due->du_status == UPDATED)
	{
		ret = UpdateInstanseDeploymentUnitInDataBase(due);

		struct timeval currTime;
		gettimeofday(&currTime, NULL);
		due->completeTime.tv_sec = currTime.tv_sec;
		due->completeTime.tv_usec = currTime.tv_usec;
	}

	if(due->initiator == ACS)
	{
		/* create answer for DU State Change Complete method */
		due->result.UUID = due->UUID;
		due->result.DeploymentUnitRef = due->DeploymentUnitRef;
		due->result.Version = due->Version;
		if(due->du_status == UPDATED)
			due->result.CurrentState = getDeploymentUnitStatus(1);
		else
			due->result.CurrentState = getDeploymentUnitStatus(due->du_status);
		due->result.ExecutionUnitRefList = due->ExecutionEnvRef;
		due->result.Resolved = due->Resolved;
		due->result.StartTime.tv_sec = due->startTime.tv_sec;
		due->result.StartTime.tv_usec = due->startTime.tv_usec;
		due->result.CompleteTime.tv_sec = due->completeTime.tv_sec;
		due->result.CompleteTime.tv_usec = due->completeTime.tv_usec;
		due->result.fault.FaultString = due->faultString;
		due->result.fault.FaultCode = due->faultCode;
	}

	if(due->initiator == NOTACS)
	{
#ifdef	 HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE
		/* create answer for AutonomusDUStateChangeComplete */
		due->autonomous_result.UUID = due->UUID;
		due->autonomous_result.DeploymentUnitRef = due->DeploymentUnitRef;
		due->autonomous_result.Version = due->Version;
		if(due->du_status == UPDATED)
			due->autonomous_result.CurrentState = getDeploymentUnitStatus(1);
		else
			due->autonomous_result.CurrentState = getDeploymentUnitStatus(due->du_status);
		due->autonomous_result.Resolved = due->Resolved;
		due->autonomous_result.ExecutionUnitRefList = due->ExecutionEnvRef;
		due->autonomous_result.StartTime.tv_sec = due->startTime.tv_sec;
		due->autonomous_result.StartTime.tv_usec = due->startTime.tv_usec;
		due->autonomous_result.CompleteTime.tv_sec = due->completeTime.tv_sec;
		due->autonomous_result.CompleteTime.tv_usec = due->completeTime.tv_usec;
		due->autonomous_result.fault.FaultString = due->faultString;
		due->autonomous_result.fault.FaultCode = due->faultCode;
		due->autonomous_result.OperationPerformed = getDeploymentUnitType(due->type);
#endif /* HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE */
	}


	return ret;
}

/**************************************************************************************/
int processUninstallDeploymentUnitEntry(DeploymentUnitEntry * due)
{
	int ret = OK;

	/* make uninstall */
	/*TODO*/
	//execUninstall()
	due->du_status = UNINSTALLED;

	/* add instanse to data-model */
	if(due->du_status == UNINSTALLED)
	{
		ret = UninstallInstanseDeploymentUnitInDataBase(due);

		struct timeval currTime;
		gettimeofday(&currTime, NULL);
		due->completeTime.tv_sec = currTime.tv_sec;
		due->completeTime.tv_usec = currTime.tv_usec;
	}

	if(due->initiator == ACS)
	{
		/* create answer for DU State Change Complete method */
		due->result.UUID = due->UUID;
		due->result.DeploymentUnitRef = due->DeploymentUnitRef;
		due->result.Version = due->Version;
		due->result.CurrentState = getDeploymentUnitStatus(due->du_status);
		due->result.ExecutionUnitRefList = due->ExecutionEnvRef;
		due->result.Resolved = due->Resolved;
		due->result.StartTime.tv_sec = due->startTime.tv_sec;
		due->result.StartTime.tv_usec = due->startTime.tv_usec;
		due->result.CompleteTime.tv_sec = due->completeTime.tv_sec;
		due->result.CompleteTime.tv_usec = due->completeTime.tv_usec;
		due->result.fault.FaultString = due->faultString;
		due->result.fault.FaultCode = due->faultCode;
	}

	if(due->initiator == NOTACS)
	{
#ifdef	 HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE
		/* create answer for AutonomusDUStateChangeComplete */
		due->autonomous_result.UUID = due->UUID;
		due->autonomous_result.DeploymentUnitRef = due->DeploymentUnitRef;
		due->autonomous_result.Version = due->Version;
		due->autonomous_result.CurrentState = getDeploymentUnitStatus(due->du_status);
		due->autonomous_result.Resolved = due->Resolved;
		due->autonomous_result.ExecutionUnitRefList = due->ExecutionEnvRef;
		due->autonomous_result.StartTime.tv_sec = due->startTime.tv_sec;
		due->autonomous_result.StartTime.tv_usec = due->startTime.tv_usec;
		due->autonomous_result.CompleteTime.tv_sec = due->completeTime.tv_sec;
		due->autonomous_result.CompleteTime.tv_usec = due->completeTime.tv_usec;
		due->autonomous_result.fault.FaultString = due->faultString;
		due->autonomous_result.fault.FaultCode = due->faultCode;
		due->autonomous_result.OperationPerformed = getDeploymentUnitType(due->type);
#endif /* HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE */
	}
	return 0;
};

/**************************************************************************************/
int InstallInstanseDeploymentUnitInDataBase(DeploymentUnitEntry * due)
{
	int ret = OK;
	unsigned int instanceNumber = 0;
	char paramName[MAX_PARAM_PATH_LENGTH];
	char tempParamName[MAX_PARAM_PATH_LENGTH];

	/* Add new object */
	pthread_mutex_lock(&paramLock);
#ifdef WITH_USE_SQLITE_DB
	executeDBCommand(BEGIN);
#endif

	ret = addObjectIntern(DEPLOYMENT_UNIT_PATH, &instanceNumber, RPC_COMMAND_IS_CALLED_INTERNAL, CALL_USER_DEFINED_CALLBACK);
	if(ret != OK)
	{
#ifdef WITH_USE_SQLITE_DB
		executeDBCommand(END);
#endif
		pthread_mutex_unlock(&paramLock);
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "InstallInstanseDeploymentUnitInDataBase: addObjectIntern() has returned error: ret = %d\n", ret);
		)
		return ret;
	}

	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.UUID
	sprintf(paramName, "%s%u.UUID\0",				DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->UUID);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.DUID
	sprintf(paramName, "%s%u.DUID\0",				DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->DUID);
//	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Alias
//	sprintf(paramName, "%s%u.Alias\0",				DEPLOYMENT_UNIT_PATH, instanceNumber);
//	setUniversalParamValueInternal(NULL, paramName, 1, due->Alias);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Name
	sprintf(paramName, "%s%u.Name\0",				DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->Name);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Status
	sprintf(paramName, "%s%u.Status\0",				DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, getDeploymentUnitStatus(due->du_status));
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Resolved
	sprintf(paramName, "%s%u.Resolved\0",			DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->Resolved);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.URL
	sprintf(paramName, "%s%u.URL\0",				DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->URL);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Description
	sprintf(paramName, "%s%u.Description\0",		DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->Description);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Vendor
	sprintf(paramName, "%s%u.Vendor\0",				DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->Vendor);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Version
	sprintf(paramName, "%s%u.Version\0",			DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->Version);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.VendorLogList
	sprintf(paramName, "%s%u.VendorLogList\0",		DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->VendorLogList);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.VendorConfigList
	sprintf(paramName, "%s%u.VendorConfigList\0",	DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->VendorConfigList);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.ExecutionUnitList
	sprintf(paramName, "%s%u.ExecutionUnitList\0",	DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->ExecutionUnitList);
	//InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.ExecutionEnvRef
	sprintf(paramName, "%s%u.ExecutionEnvRef\0",	DEPLOYMENT_UNIT_PATH, instanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->ExecutionEnvRef);

#ifdef WITH_USE_SQLITE_DB
	executeDBCommand(END);
#endif
	pthread_mutex_unlock(&paramLock);

	/* Save deployment unit ref */
	sprintf(tempParamName, "%s%u.\0",				DEPLOYMENT_UNIT_PATH, instanceNumber);
	strnCopy(due->DeploymentUnitRef, tempParamName, (sizeof(due->DeploymentUnitRef) -1));
	due->InstanceNumber = instanceNumber;

	return ret;

}

/**************************************************************************************/
int UpdateInstanseDeploymentUnitInDataBase(DeploymentUnitEntry * due)
{
	int ret = OK;
	char paramName[MAX_PARAM_PATH_LENGTH];

	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.UUID
	sprintf(paramName, "%s%u.UUID\0",				DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->UUID);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.DUID
	sprintf(paramName, "%s%u.DUID\0",				DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->DUID);
//	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Alias
//	sprintf(paramName, "%s%u.Alias\0",				DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
//	setUniversalParamValueInternal(NULL, paramName, 1, due->Alias);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Name
	sprintf(paramName, "%s%u.Name\0",				DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->Name);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Status
	sprintf(paramName, "%s%u.Status\0",				DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, getDeploymentUnitStatus(due->du_status));
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Resolved
	sprintf(paramName, "%s%u.Resolved\0",			DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->Resolved);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.URL
	sprintf(paramName, "%s%u.URL\0",				DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->URL);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Description
	sprintf(paramName, "%s%u.Description\0",		DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->Description);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Vendor
	sprintf(paramName, "%s%u.Vendor\0",				DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->Vendor);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.Version
	sprintf(paramName, "%s%u.Version\0",			DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->Version);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.VendorLogList
	sprintf(paramName, "%s%u.VendorLogList\0",		DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->VendorLogList);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.VendorConfigList
	sprintf(paramName, "%s%u.VendorConfigList\0",	DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->VendorConfigList);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.ExecutionUnitList
	sprintf(paramName, "%s%u.ExecutionUnitList\0",	DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->ExecutionUnitList);
	// InternetGatewayDevice.SoftwareModules.DeploymentUnit.*.ExecutionEnvRef
	sprintf(paramName, "%s%u.ExecutionEnvRef\0",	DEPLOYMENT_UNIT_PATH, due->InstanceNumber);
	setUniversalParamValueInternal(NULL, paramName, 1, due->ExecutionEnvRef);

	return ret;
}

/**************************************************************************************/
int UninstallInstanseDeploymentUnitInDataBase(DeploymentUnitEntry * due)
{
	int ret = OK;
	char paramName[MAX_PARAM_PATH_LENGTH];
	char buf[MAX_PARAM_PATH_LENGTH];

	strcpy(buf, DEPLOYMENT_UNIT_PATH);
	strcat(buf, "%d.");

	/* Delete exist object */
	sprintf (paramName, buf, due->InstanceNumber);

	ret = deleteObjectIntern(paramName, RPC_COMMAND_IS_CALLED_INTERNAL, CALL_USER_DEFINED_CALLBACK);
	if(ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "UninstallInstanseDeploymentUnitInDataBase: deleteObject() has returned error: ret = %d \n", ret);
		)
		return ret;
	}

	/* Save deployment unit ref */
	memset(due->DeploymentUnitRef, 0, sizeof(due->DeploymentUnitRef));
	due->InstanceNumber = 0;

	return ret;
}

#endif /* HAVE_DEPLOYMENT_UNIT */
