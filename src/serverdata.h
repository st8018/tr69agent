/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

/*
 * Server definitions
 */
#ifndef server_H
#define server_H

#include "utils.h"

#define NEXT_NO_ACTION 		0
#define NEXT_BOOT_ACTION 	1

#define CURRENT_ID_SIZE 	128
#define COMMANDKEY_SIZE 	32

/* Initialize the ServerData with reasonable Values
 */
void initServerData( void );

/** Servers MaxEnvelopes we always use one */
void setSdMaxEnvelopes( int );

/** Our RetryCounter */
void setSdRetryCount( unsigned int );
void incSdRetryCount( void );
void clearSdRetryCount( void );
unsigned int getSdRetryCount( void );

/** Stores the Next Action after we have handled all Requests */
void setSdNextAction( int na );
int getSdNextAction( void );

/** A Command which was given by the ACS */
void setSdLastCommandKey( char * );
const char *getSdLastCommandKey( void );

/** Current Header ID  Used by ACS */
void setSdCurrentId( const char * );
char *getSdCurrentId( void );

/** Holdrequest from ACS */
void setSdHoldRequests( int );
int getSdHoldRequests( void );

/** NoMoreRequests from ACS */
void setSdNoMoreRequests( int );
int getSdNoMoreRequests( void );

#endif /* server_H */
