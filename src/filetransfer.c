/***************************************************************************
 *    Original code © Copyright (C) 2004-2012 by Dimark Software Inc.      *
 *    support@dimark.com                                                   *
 *    Modifications © 2013 Comcast Cable Communications, LLC               *
 ***************************************************************************/

#include "dimark_globals.h"

#ifdef HAVE_FILE

#include <signal.h>
#include <arpa/ftp.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "filetransfer.h"
#include "parameter.h"
#include "utils.h"
#include "httpda.h"
#include "list.h"
#include "serverdata.h"
#include "host/filetransferStore.h"
#include "host/diagParameter.h"
#include "ftp_ft.h"
#include "ftp_var.h"
#include "eventcode.h"
#include "paramconvenient.h"

#include "libIBus.h"
#include "swupMgr.h"


#define SAFE_STR(x) x==NULL? "NULL" : x

#define USE_SWUPMGR 1

/* The following structures defines the path and names
 * of the defined upload files for
 * 
 * 1 Vendor Configuration File
 * 2 Vendor Log File
 * 
 * The mapping from the file type sent by the ACS to the real filename in the file system
 * is made in type2file(). If a new file type has to be supported, add an entry in the
 * UploadFiles array and implement the mapping in type2file function. */
static UploadFile UploadFiles[] =
{
		{ "config.txt", "config.txt", "text/plain" },
		{ "logfile.txt", "logfile.txt", "text/plain" }
};

pthread_mutex_t transferList_usage_mutex = PTHREAD_MUTEX_INITIALIZER;

/* Index of files, which contents an information about transfered files. */
#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
static int indexOfFile = 0;

extern DownloadCB downloadCBB;
extern ScheduleDownloadCB scheduleDownloadCBB;
extern UploadCB uploadCBB;
extern DownloadCB downloadCBA;
extern ScheduleDownloadCB scheduleDownloadCBA;
extern UploadCB uploadCBA;

static int readTransferList(void);
static void printTransferList(void);
static int readTransferListEntry(char *, char *);
static void addTransfer(TransferEntry *);
static int writeTransferEntry(TransferEntry *);
static char *getTransferStateName(TransferState );
static int setTransferStatusAndSaveTransfEntry(TransferEntry *, TransferState );
int flashImageReboot();
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */

extern char proxy_host[];
extern int  proxy_port;
extern char proxy_userid[];
extern char proxy_passwd[];
extern char proxy_version[];

extern int  prevSoapErrorValue;

extern SUPORTEDRPC suportedRPC;
/*Start: For XI3, Firmware download profile  */
char* firmwareDownloadStatus[] = {"Idle","Initiated","InProgress","Completed","Successful","Error"};
/*End: For XI3, Firmware download profile  */
#ifdef HAVE_FILE_UPLOAD
extern char info_for_soap_header[100];
#endif /* HAVE_FILE_UPLOAD */

List transferList;
#ifdef	 HAVE_AUTONOMOUS_TRANSFER_COMPLETE
bool isAutonomousTransferComplete = true;
#endif /* HAVE_AUTONOMOUS_TRANSFER_COMPLETE */

#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD)
static int checkFreeHardMemoryOnDevice(unsigned int );
#endif /* defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) */

#ifdef HAVE_FILE_UPLOAD
static int doUpload(const char *, char *, char *, char *);
static int doFtpUpload(const char *, char *, char *, char *);
static int doHttpUpload(const char *, char *, char *, char *);
static int uploadFile(struct soap *, const UploadFile *, long, bool);
static int sendFile(struct soap *, const UploadFile *);
#endif /* HAVE_FILE_UPLOAD */

static int deleteTransferEntry(TransferEntry *);

#ifdef HAVE_CANCEL_TRANSFER
static int tryCancelTransfer(TransferEntry *);
#endif /* HAVE_CANCEL_TRANSFER */

/********* Function for work with TransferList *******/
static void initTransferList();
static ListEntry * iterateTransferList(ListEntry * entry);
static void addEntryToTransferList(TransferEntry * te);
static ListEntry * getFirstEntryFromTransferList();
static ListEntry * iterateRemoveFromTransferList(ListEntry * entry);
static int getTransferListSize();

#if USE_SWUPMGR
int swupmgr_doHttpDownload(char *URL, char *username, char *password, int bufferSize, char *targetFileName)
{
   bool enableValidation;
   bool* result;
   int ret = -1;

   DEBUG_OUTPUT (
           dbglog (SVR_ERROR, DBG_TRANSFER, "doHttpDownload: %s\n", SAFE_STR(URL));
   )
   DEBUG_OUTPUT (
           dbglog (SVR_ERROR, DBG_TRANSFER, "doHttpDownload: u:%s,p:%s,bs:%d,fn:%s\n",
               SAFE_STR(username), SAFE_STR(password), bufferSize, SAFE_STR(targetFileName));
   )

   if ( getParameter("Device.ManagementServer.X_TWC_COM_ValidateDownloadServerCertificate",
               &result) != OK)
   {
       DEBUG_OUTPUT (
               dbglog (SVR_WARN, DBG_TRANSFER, "Can't retrieve"
                   "X_TWC_COM_ValidateDownloadServerCertificate \n");
       )
       enableValidation = false;
   }
   else
   {
       enableValidation = *result;
   }

   DEBUG_OUTPUT (
       dbglog (SVR_INFO, DBG_MAIN, "X_TWC_COM_ValidateDownloadServerCertificate is %d\n", enableValidation);
   )

    IARM_Bus_SWUPMgr_InvokeSWUpgradeService_Param_t param;

    strcpy(param.commandKey, "testCommandKey");
    strcpy(param.url, URL);
    strcpy(param.userName, username != NULL ? username:"");
    strcpy(param.password, password != NULL ? password:"");
    param.fileSize = bufferSize;
    strcpy(param.targetFileName, targetFileName != NULL ? targetFileName:"");
    param.delaySeconds = 60;
    strcpy(param.successURL, "");
    strcpy(param.failureURL, "");

    DEBUG_OUTPUT (
       dbglog (SVR_INFO, DBG_MAIN, "Calling IARM_BUS_SWUPMGR_API_InvokeSWUpgradeService\n");
   )

    /* populate param here */
    IARM_Bus_Call(IARM_BUS_SWUPMGR_NAME,
            IARM_BUS_SWUPMGR_API_InvokeSWUpgradeService,
            (void *)&param,
            sizeof(param));

    while (1) {
        IARM_Bus_SWUPMgr_GetDownloadState_Param_t param;

        /* populate param here */
        IARM_Bus_Call(IARM_BUS_SWUPMGR_NAME,
                IARM_BUS_SWUPMGR_API_GetDownloadState,
                (void *)&param,
                sizeof(param));
        DEBUG_OUTPUT (
                dbglog (SVR_INFO, DBG_MAIN, "upgradeState:%d\n", 
                    param.upgradeState);
        )
        if (param.upgradeState == IARM_BUS_SWUPMGR_SWUPGRADE_STATE_FAILED) {
            break;
        } else if (param.upgradeState == 
                IARM_BUS_SWUPMGR_SWUPGRADE_STATE_DOWNLOADED) {
            ret = 0;
            break;
        }
        sleep(1);
    }

   if (ret != 0)
   {
       DEBUG_OUTPUT (
           dbglog (SVR_ERROR, DBG_TRANSFER, "Download Failed (%d)\n", ret);
       )
       return ERR_DOWNLOAD_FAILURE;
   }

   return NO_ERROR;
}

int swupmgr_installSWImage() 
{
    int ret=-1;
    IARM_Bus_SWUPMgr_InstallFirmwareUpdate_Param_t param;

    param.afterReboot = false;

    /* populate param here */
    IARM_Bus_Call(IARM_BUS_SWUPMGR_NAME,
            IARM_BUS_SWUPMGR_API_InstallFirmwareUpdate,
            (void *)&param,
            sizeof(param));

    DEBUG_OUTPUT (
        dbglog(SVR_INFO, DBG_TRANSFER,"result:%d\n", param.result);
    )

    if (param.result == 0) {
       DEBUG_OUTPUT (
            dbglog(SVR_ERROR, DBG_TRANSFER, "Installation of a SW upgrade image is failed due to error=%s\n", param.errorMessage);
        )
        return -1;
    }

    while (1) {
        IARM_Bus_SWUPMgr_GetDownloadState_Param_t param;

        /* populate param here */
        IARM_Bus_Call(IARM_BUS_SWUPMGR_NAME,
                IARM_BUS_SWUPMGR_API_GetDownloadState,
                (void *)&param,
                sizeof(param));
        DEBUG_OUTPUT (
                dbglog (SVR_INFO, DBG_MAIN, "upgradeState:%d\n", 
                    param.upgradeState);
        )
        if (param.upgradeState == IARM_BUS_SWUPMGR_SWUPGRADE_STATE_FAILED) {
            break;
        } else if (param.upgradeState == 
                IARM_BUS_SWUPMGR_SWUPGRADE_STATE_COMPLETED) {
            ret = 0;
            break;
        }
        sleep(1);
    }

    return ret;
}
#endif


#ifdef HAVE_GET_QUEUED_TRANSFERS
int execGetQueuedTransfers(struct ArrayOfQueuedTransfers *transferArray)
{
	int ret = OK;
	ListEntry *entry = NULL;
	struct QueuedTransferStruct *soapQueuedTransfer;
	TransferEntry *te = NULL;
	int transferCount;

	transferCount = getTransferListSize();

	transferArray->__ptrQueuedTransferStruct = (cwmp__QueuedTransferStruct **) emallocTemp(sizeof(cwmp__QueuedTransferStruct *) * transferCount, 11);

	if (transferArray->__ptrQueuedTransferStruct == NULL)
	{
		return ERR_RESOURCE_EXCEEDED;
	}
	transferArray->__size = 0;

	while((entry = iterateTransferList(entry)))
	{
		soapQueuedTransfer = (struct QueuedTransferStruct *) emallocTemp(sizeof(struct QueuedTransferStruct), 12);
		if (soapQueuedTransfer == NULL)
		{
			return ERR_RESOURCE_EXCEEDED;
		}
		te = (TransferEntry *) entry->data;
		if (te && !te->initiator)
		{
			soapQueuedTransfer->CommandKey = strnDupByMemType(soapQueuedTransfer->CommandKey, te->commandKey, strlen(te->commandKey), MEM_TYPE_TEMP);
			soapQueuedTransfer->State = ( (te->status >= Transfer_Completed) ? Transfer_Completed : te->status );
			transferArray->__ptrQueuedTransferStruct[transferArray->__size] = soapQueuedTransfer;
			transferArray->__size++;
		}
	}
	return ret;
}
#endif /* HAVE_GET_QUEUED_TRANSFERS */

#ifdef HAVE_GET_ALL_QUEUED_TRANSFERS
int execGetAllQueuedTransfers(struct ArrayOfAllQueuedTransfers *transferArray)
{
	int ret = OK;
	ListEntry *entry = NULL;
	struct AllQueuedTransferStruct *soapAllQueuedTransfer;
	TransferEntry *te = NULL;
	int transferCount;

	transferCount = getTransferListSize();

	transferArray->__ptrAllQueuedTransferStruct = (struct AllQueuedTransferStruct **) emallocTemp(sizeof(struct AllQueuedTransferStruct *) * transferCount, 13);
	if (transferArray->__ptrAllQueuedTransferStruct == NULL)
	{
		return ERR_RESOURCE_EXCEEDED;
	}
	transferArray->__size = 0;

	while ((entry = iterateTransferList(entry)))
	{
		soapAllQueuedTransfer = (struct AllQueuedTransferStruct *) emallocTemp(sizeof(struct AllQueuedTransferStruct), 14);
		if (soapAllQueuedTransfer == NULL)
		{
			return ERR_RESOURCE_EXCEEDED;
		}
		te = (TransferEntry *) entry->data;
		if (!te)
			continue;
		soapAllQueuedTransfer->CommandKey = strnDupByMemType(soapAllQueuedTransfer->CommandKey, te->commandKey, strlen(te->commandKey), MEM_TYPE_TEMP);

		soapAllQueuedTransfer->State = ( (te->status >= Transfer_Completed) ? Transfer_Completed : te->status );
		soapAllQueuedTransfer->IsDownload = te->type != UPLOAD;
		soapAllQueuedTransfer->FileType = strnDupByMemType(soapAllQueuedTransfer->FileType, te->fileType, strlen(te->fileType), MEM_TYPE_TEMP);
		soapAllQueuedTransfer->FileSize = te->fileSize;
		soapAllQueuedTransfer->TargetFileName = strnDupByMemType(soapAllQueuedTransfer->TargetFileName, te->targetFileName, strlen(te->targetFileName), MEM_TYPE_TEMP);
		transferArray->__ptrAllQueuedTransferStruct[transferArray->__size] = soapAllQueuedTransfer;
		transferArray->__size++;
	}
	return ret;
}
#endif /* HAVE_GET_QUEUED_TRANSFERS */

#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
/* resets all file transfers and deletes the bootstrap file */
int resetAllFiletransfers(void)
{
	freeTransferEntryMemory();
	return clearAllFtInfo();
}
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */

#ifdef HAVE_FILE_DOWNLOAD
int execDownload(struct soap *soap,
		char *CommandKey,
		char *FileType,
		char *URL,
		char *Username,
		char *Password,
		unsigned int FileSize,
		char *TargetFileName,
		unsigned int DelaySeconds,
		char *SuccessURL,
		char *FailureURL,
		Transfers_type initiator,
		char *announceURL,
		char *transferURL,
		struct cwmp__DownloadResponse *response)
{
	int returnCode = OK;
	TransferEntry *te;
	char *targetFileName;

	/*Comcast patch: Start: Reject download if the imagename is same */
	char *pchCurFirmwareImageName = NULL;
	char *pchTargetFileName = NULL;
	ParameterValue value;
	/*Comcast patch: End: Reject download if the imagename is same */

	if (!URL || strlen(URL) > URL_STR_LEN)
	{
		value.in_cval = firmwareDownloadStatus[eFWDN_Status_Error];
		updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);
		/* Update status to fwdnldstatus.txt */
		set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);
		
		value.in_cval = TargetFileName;
		updateParamValue(FIRMWARE_DOWNLOAD_PATH_NAME,StringType,&value);
		/* Update download image to fwdnldstatus.txt */
		set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_PATH_NAME,StringType,&value, StringType, &value);

		DEBUG_OUTPUT (
				dbglog (SVR_DEBUG, DBG_PARAMETER, "[%s:%s:%d]X_COMCAST_COM_FirmwareToDownload: %s\n",__FILE__,__FUNCTION__,__LINE__,value.in_cval);
			     )
			return ERR_INVALID_ARGUMENT;
	}

	if (!TargetFileName || strlen(TargetFileName) > TARGET_FILE_NAME_LEN)
	{
           	 //Firmware image failed to download and status is recorded to know the download status.
	    value.in_cval = firmwareDownloadStatus[eFWDN_Status_Error];
            updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);

	    /* Update status to fwdnldstatus.txt */
	    set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);
            
	    value.in_cval = TargetFileName;
            
	    updateParamValue(FIRMWARE_DOWNLOAD_PATH_NAME,StringType,&value);
	    /* Update download image to fwdnldstatus.txt */
	    set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_PATH_NAME,StringType,&value, StringType, &value);
	    
            DEBUG_OUTPUT (
                dbglog (SVR_DEBUG, DBG_PARAMETER, "[%s:%s:%d]X_COMCAST_COM_FirmwareToDownload: %s\n",__FILE__,__FUNCTION__,__LINE__,value.in_cval);)

  	    return ERR_INVALID_ARGUMENT;
	}

	if (!Username || strlen(Username) > USER_STR_LEN)
		return ERR_INVALID_ARGUMENT;

	if (!Password || strlen(Password) > PASS_STR_LEN)
		return ERR_INVALID_ARGUMENT;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "CommandKey  : %s\n", CommandKey);
	dbglog (SVR_INFO, DBG_TRANSFER, "FileType    : %s\n", FileType);
	dbglog (SVR_INFO, DBG_TRANSFER, "URL         : %s\n", URL);
	dbglog (SVR_INFO, DBG_TRANSFER, "Target      : %s\n", TargetFileName);
	dbglog (SVR_INFO, DBG_TRANSFER, "FileSize    : %d\n", FileSize);
	dbglog (SVR_INFO, DBG_TRANSFER, "Username    : %s\n", Username);
	dbglog (SVR_INFO, DBG_TRANSFER, "Password    : %s\n", Password);
	dbglog (SVR_INFO, DBG_TRANSFER, "Delay       : %d\n", DelaySeconds);
	dbglog (SVR_INFO, DBG_TRANSFER, "SuccessURL  : %s\n", SuccessURL);
	dbglog (SVR_INFO, DBG_TRANSFER, "FailureURL  : %s\n", FailureURL);
	dbglog (SVR_INFO, DBG_TRANSFER, "Initiator   : %d\n", initiator);
	dbglog (SVR_INFO, DBG_TRANSFER, "AnnounceURL : %s\n", announceURL);
	dbglog (SVR_INFO, DBG_TRANSFER, "TransferURL : %s\n", transferURL);
	)

	if (!checkFreeHardMemoryOnDevice(FileSize))
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "The FileSize argument exceeds the available space on the device.\n");
		)
		return ERR_DOWNLOAD_FAILURE;
	}

	/* If ACS delivers no TargetFileName therefore we use a default one */
	if(TargetFileName && strlen(TargetFileName))
	{
		targetFileName = TargetFileName;

		/*Comcast Patch: Start: Rejection of firmware if tried for same name*/
		if(0 == strncmp(TargetFileName,"/",1))
		{
			pchTargetFileName = strrchr(TargetFileName,'/') + 1;
		}
		else
		{
			pchTargetFileName = TargetFileName;
		}
	}
	else
	{
		targetFileName = getDefaultDownloadFilename(URL);
		pchTargetFileName = targetFileName;
	}
	/*For XI-3 Box, Comparing the Image Name from version.txt with Target File Name coming from ACS.
	 Get the image Name from the version.txt file.*/
	if(OK != getParameter(FIRMWARE_IMAGENAME, &pchCurFirmwareImageName))
	{
		DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_TRANSFER, "getParameter(): [%s] Failed to get imagename.\n", FIRMWARE_IMAGENAME);
		)
		return ERR_DOWNLOAD_FAILURE;
	}


	// Comparing image Name and target file name delivered by ACS.
	if((pchCurFirmwareImageName[0] != '\0') && (0 == strncasecmp(pchTargetFileName,pchCurFirmwareImageName, strlen(pchCurFirmwareImageName))))
	{
		DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_TRANSFER, "Download Failed \"%s\" Image Version Already Installed \n",pchTargetFileName);
	)

		//Firmware image failed to download and status is recorded to know the download status.
		value.in_cval = firmwareDownloadStatus[eFWDN_Status_Error];
		updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);
		
		/* Update status to fwdnldstatus.txt */
		set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);

		return ERR_DOWNLOAD_FAILURE;
	}

	
	/* Check for image version as per device version */
	char *delimiter = "_";
	int delPos = strcspn (pchTargetFileName, delimiter);
	if (strncasecmp(pchTargetFileName, pchCurFirmwareImageName, delPos) != 0)
	{
		DEBUG_OUTPUT (dbglog (SVR_ERROR, DBG_TRANSFER, "Failed: Downloaded \"%s\" Image version doesn't match with running Image \"%s\" \n", pchTargetFileName, pchCurFirmwareImageName );	)
			value.in_cval = firmwareDownloadStatus[eFWDN_Status_Error];
		updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);
		
		/* Update status to fwdnldstatus.txt */
		set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);

		return ERR_DOWNLOAD_FAILURE;
	}
	/*Comcast Patch: End: Rejection of firmware if tried for same name*/


	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "Delayed Download\n");
	)

	response->Status = Transfer_NotStarted;
	response->StartTime = UNKNOWN_TIME;
	response->CompleteTime = UNKNOWN_TIME;

	/* Can't use emallocTemp() because of unfinished transfers with status = 0 */
	te = (TransferEntry *) emallocTypedMem(sizeof(TransferEntry), MEM_TYPE_FILETRANSFER_LIST, 0);
	if (te == NULL)
	{
		//Firmware image failed to download and status is recorded to know the download status.
		value.in_cval = firmwareDownloadStatus[eFWDN_Status_Error];
		updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);
		
		/* Update status to fwdnldstatus.txt */
		set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);

		return ERR_RESOURCE_EXCEEDED;
	}

	te->type = DOWNLOAD;
	strnCopy(te->commandKey, CommandKey, (sizeof(te->commandKey) - 1));
	strnCopy(te->fileType, FileType, (sizeof(te->fileType) - 1));
	strnCopy(te->URL, URL, (sizeof(te->URL) - 1));
	strnCopy(te->username, Username, (sizeof(te->username) - 1));
	strnCopy(te->password, Password, (sizeof(te->password) - 1));
	strnCopy(te->targetFileName, targetFileName, (sizeof(te->targetFileName) - 1));
	strnCopy(te->successURL, SuccessURL, (sizeof(te->successURL) - 1));
	strnCopy(te->failureURL, FailureURL, (sizeof(te->failureURL) - 1));
	te->fileSize = FileSize;
	gettimeofday(&te->createTime, NULL);
	te->startTime.tv_sec = te->createTime.tv_sec + DelaySeconds;
	te->startTime.tv_usec = te->createTime.tv_usec;
	te->completeTime.tv_sec = 0;
	te->completeTime.tv_usec = 0;
	te->status = Transfer_NotStarted;
	te->faultCode = NO_ERROR;
	te->initiator = initiator;
	indexOfFile = indexOfFile % 10000;
	te->indexOfFile = indexOfFile++;
	strnCopy(te->announceURL, announceURL, (sizeof(te->announceURL) - 1));
	strnCopy(te->transferURL, transferURL, (sizeof(te->transferURL) - 1));
	addTransfer(te);

	 //Firmware image to download is initiated and status is recorded.
	value.in_cval = firmwareDownloadStatus[eFWDN_Status_Initiated];
	updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);
	/* Update status to fwdnldstatus.txt */
	set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);

	value.in_cval = pchTargetFileName;
	updateParamValue(FIRMWARE_DOWNLOAD_PATH_NAME,StringType,&value);
	
	/* Update download image to fwdnldstatus.txt */
	set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_PATH_NAME,StringType,&value, StringType, &value);


	return returnCode;
}
#endif /* HAVE_FILE_DOWNLOAD */

#ifdef HAVE_FILE_SCHEDULE_DOWNLOAD
int execScheduleDownload(struct soap *soap,
		char *CommandKey,
		char *FileType,
		char *URL,
		char *Username,
		char *Password,
		unsigned int FileSize,
		char *TargetFileName,
		struct ArrayOfTimeWindowsStruct *timeWindowList,
		Transfers_type initiator,
		char *announceURL,
		char *transferURL,
		struct cwmp__ScheduleDownloadResponse *response)
{
	int returnCode = OK;
	TransferEntry *te;
	char *targetFileName;
	struct TimeWindowStruct *tmpTimeWindowStruct = NULL;

	if (!URL || strlen(URL) > URL_STR_LEN)
		return ERR_INVALID_ARGUMENT;

	if (!TargetFileName || strlen(TargetFileName) > TARGET_FILE_NAME_LEN)
		return ERR_INVALID_ARGUMENT;

	if (!Username || strlen(Username) > USER_STR_LEN)
		return ERR_INVALID_ARGUMENT;

	if (!Password || strlen(Password) > PASS_STR_LEN)
		return ERR_INVALID_ARGUMENT;

	DEBUG_OUTPUT (
	dbglog (SVR_INFO, DBG_TRANSFER, "execScheduleDownload started with parameters:\n");
	dbglog (SVR_INFO, DBG_TRANSFER, "CommandKey  : %s\n", CommandKey);
	dbglog (SVR_INFO, DBG_TRANSFER, "FileType    : %s\n", FileType);
	dbglog (SVR_INFO, DBG_TRANSFER, "URL         : %s\n", URL);
	dbglog (SVR_INFO, DBG_TRANSFER, "Target      : %s\n", TargetFileName);
	dbglog (SVR_INFO, DBG_TRANSFER, "FileSize    : %d\n", FileSize);
	dbglog (SVR_INFO, DBG_TRANSFER, "Username    : %s\n", Username);
	dbglog (SVR_INFO, DBG_TRANSFER, "Password    : %s\n", Password);
	dbglog (SVR_INFO, DBG_TRANSFER, "Initiator   : %d\n", initiator);
	dbglog (SVR_INFO, DBG_TRANSFER, "AnnounceURL : %s\n", announceURL);
	dbglog (SVR_INFO, DBG_TRANSFER, "TransferURL : %s\n", transferURL);
	)

	if (!checkFreeHardMemoryOnDevice(FileSize))
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "The FileSize argument exceeds the available space on the device.\n");
		)
		return ERR_DOWNLOAD_FAILURE;
	}

	/* If ACS delivers no TargetFileName therefore we use a default one */
	if(TargetFileName && strlen(TargetFileName))
	{
		targetFileName = TargetFileName;
	}
	else
	{
		targetFileName = getDefaultDownloadFilename(URL);
	}

	/* Can't use emallocTemp() because of unfinished transfers with status = 0 */
	te = (TransferEntry *) emallocTypedMem(sizeof(TransferEntry), MEM_TYPE_FILETRANSFER_LIST, 0);
	if (te == NULL)
	{
		return ERR_RESOURCE_EXCEEDED;
	}

	gettimeofday(&te->createTime, NULL);
	if ( !timeWindowList || !(timeWindowList->__ptrTimeWindowStruct) || (timeWindowList->__size != 1 && timeWindowList->__size != 2)  )
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "ScheduleDownload request is wrong. Error in TimeWindowList structure.\n");
		)
		efreeTypedMemByPointer((void**)&te, 0);
		return ERR_INVALID_ARGUMENT;
	}
	tmpTimeWindowStruct = (struct TimeWindowStruct *)timeWindowList->__ptrTimeWindowStruct[0];
	if (tmpTimeWindowStruct->WindowStart >= tmpTimeWindowStruct->WindowEnd)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "Error in TimeWindowList structure 1: WindowStart >= WindowEnd\n");
		)
		efreeTypedMemByPointer((void**)&te, 0);
		return ERR_INVALID_ARGUMENT;
	}
	te->TimeWindow1 = (TimeWindowStructType *) emallocTypedMem(sizeof(TimeWindowStructType), MEM_TYPE_FILETRANSFER_LIST, 0);
	if (te->TimeWindow1 == NULL)
	{
		efreeTypedMemByPointer((void**)&te, 0);
		return ERR_RESOURCE_EXCEEDED;
	}
	te->TimeWindow1->WindowStartTime = (time_t) (te->createTime.tv_sec + tmpTimeWindowStruct->WindowStart);
	te->TimeWindow1->WindowEndTime = (time_t) (te->createTime.tv_sec + tmpTimeWindowStruct->WindowEnd);
	strnCopy(te->TimeWindow1->WindowMode, tmpTimeWindowStruct->WindowMode, WINDOW_MODE_STR_LEN);
	strnCopy(te->TimeWindow1->UserMessage, tmpTimeWindowStruct->UserMessage, USER_MESS_STR_LEN);
	//A value of -1 means “the CPE determines the number of retries”,
	//i.e. that the CPE can use its own retry policy, not that it has to retry forever.
	if (tmpTimeWindowStruct->MaxRetries == -1)
		te->TimeWindow1->MaxRetries = MAX_RETRIES_FOR_MINUS_1;
	else if (tmpTimeWindowStruct->MaxRetries < -1)
		te->TimeWindow1->MaxRetries = 0;
	else
		te->TimeWindow1->MaxRetries = tmpTimeWindowStruct->MaxRetries;
	te->TimeWindow1->MaxRetries++;  // +1 - it is first attempt.

	if (timeWindowList->__size == 2)
	{
		tmpTimeWindowStruct = (struct TimeWindowStruct *)timeWindowList->__ptrTimeWindowStruct[1];
		if (tmpTimeWindowStruct->WindowStart >= tmpTimeWindowStruct->WindowEnd)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_TRANSFER, "Error in TimeWindowList structure 2: WindowStart >= WindowEnd\n");
			)
			efreeTypedMemByPointer((void**)&te->TimeWindow1, 0);
			efreeTypedMemByPointer((void**)&te, 0);
			return ERR_INVALID_ARGUMENT;
		}
		te->TimeWindow2 = (TimeWindowStructType *) emallocTypedMem(sizeof(TimeWindowStructType), MEM_TYPE_FILETRANSFER_LIST, 0);
		if (te->TimeWindow2 == NULL)
		{
			efreeTypedMemByPointer((void**)&te->TimeWindow1, 0);
			efreeTypedMemByPointer((void**)&te, 0);
			return ERR_RESOURCE_EXCEEDED;
		}
		te->TimeWindow2->WindowStartTime = (time_t) (te->createTime.tv_sec + tmpTimeWindowStruct->WindowStart);
		te->TimeWindow2->WindowEndTime = (time_t) (te->createTime.tv_sec + tmpTimeWindowStruct->WindowEnd);
		strnCopy(te->TimeWindow2->WindowMode, tmpTimeWindowStruct->WindowMode, WINDOW_MODE_STR_LEN);
		strnCopy(te->TimeWindow2->UserMessage, tmpTimeWindowStruct->UserMessage, USER_MESS_STR_LEN);
		//A value of -1 means “the CPE determines the number of retries”,
		//i.e. that the CPE can use its own retry policy, not that it has to retry forever.
		if (tmpTimeWindowStruct->MaxRetries == -1)
			te->TimeWindow2->MaxRetries = MAX_RETRIES_FOR_MINUS_1;
		else if (tmpTimeWindowStruct->MaxRetries < -1)
			te->TimeWindow2->MaxRetries = 0;
		else
			te->TimeWindow2->MaxRetries = tmpTimeWindowStruct->MaxRetries;
		te->TimeWindow2->MaxRetries++;  // +1 - it is first attempt.

		if (te->TimeWindow1->WindowStartTime > te->TimeWindow2->WindowStartTime)
		{
			struct TimeWindowStructType * tmp = te->TimeWindow1;
			te->TimeWindow1 = te->TimeWindow2;
			te->TimeWindow2 = tmp;
		}
		// Check whether timewindiws are correct
		if ((te->TimeWindow1->WindowStartTime == te->TimeWindow2->WindowStartTime) || (te->TimeWindow1->WindowEndTime > te->TimeWindow2->WindowStartTime))
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_TRANSFER, "Error in TimeWindowList structure: time windows overlap.\n");
			)
			efreeTypedMemByPointer((void**)&te->TimeWindow1, 0);
			efreeTypedMemByPointer((void**)&te->TimeWindow2, 0);
			efreeTypedMemByPointer((void**)&te, 0);
			return ERR_INVALID_ARGUMENT;
		}
	}
	te->startTime.tv_sec = te->TimeWindow1->WindowStartTime;
	te->startTime.tv_usec = te->createTime.tv_usec;

	te->type = SCHEDULEDOWNLOAD;
	strnCopy(te->commandKey, CommandKey, CMD_KEY_STR_LEN);
	strnCopy(te->fileType, FileType, FILE_TYPE_STR_LEN);
	strnCopy(te->URL, URL, URL_STR_LEN);
	strnCopy(te->username, Username, USER_STR_LEN);
	strnCopy(te->password, Password, PASS_STR_LEN);
	strnCopy(te->targetFileName, targetFileName, TARGET_FILE_NAME_LEN);
	te->successURL[0] = '\0';
	te->failureURL[0] = '\0';
	te->fileSize = FileSize;
	te->completeTime.tv_sec = 0;
	te->completeTime.tv_usec = 0;
	te->status = Transfer_NotStarted;
	te->faultCode = NO_ERROR;
	te->initiator = initiator;
	indexOfFile = indexOfFile % 10000;
	te->indexOfFile = indexOfFile++;
	strnCopy(te->announceURL, announceURL, URL_STR_LEN);
	strnCopy(te->transferURL, transferURL, URL_STR_LEN);

	addTransfer(te);

	return returnCode;
}
#endif /* HAVE_FILE_SCHEDULE_DOWNLOAD */

#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_DEPLOYMENT_UNIT)
int doDownload(const char *URL, char *username, char *password, int bufferSize, char *targetFileName)
{
	int ret = 0;
	if (strnStartsWith(URL, "http", 4) == 1 || strnStartsWith(URL, "https", 5) == 1)
	{
#if USE_SWUPMGR
       ret = swupmgr_doHttpDownload(URL, username, password, bufferSize, targetFileName);
#else 
       ret = doHttpDownload(URL, username, password, bufferSize, targetFileName);
#endif
       return ret;
	}

	if (strnStartsWith(URL, "ftp", 3) == 1)
	{
		return doFtpDownload(URL, username, password, bufferSize, targetFileName);
	}

	return ERR_DOWNLOAD_FAILURE;
}

int doFtpDownload(const char *URL, char *username, char *password, int bufferSize, char *targetFileName)
{
	/* extract remote host and remote filename from URL
	 * Only ftp://hostname[:port]/dir/dir/remotename  is allowed
	 * the remote name must be relative to the ftp directory */

	char *tmp;
	char *host;
	char *remoteFileName;
	long size;
	int ret = OK;

	if (!URL)
		return ERR_DOWNLOAD_FAILURE;

	char tmpURL[strlen(URL) + 1]; // size of URL may be uo to 256 for Download RPC or up to 1024 for DU.
	strcpy(tmpURL, URL);
	tmpURL[sizeof(tmpURL)] = '\0';

	/* skip scheme "ftp://" */
	host = tmpURL + 6;
	tmp = host;

	while (*tmp && *tmp != ':' && *tmp != '/')
	{
		tmp++;
	}

	*tmp = '\0';
	tmp++;
	remoteFileName = tmp;

	if ((ret = ftp_login(host, username, password)) != OK)
	{
		return ret;
	}

	ftp_get(remoteFileName, targetFileName, &size);

	if (size < 0 || (bufferSize != 0 && size != bufferSize))
	{
		return ERR_DOWNLOAD_FAILURE;
	}

	ftp_disconnect();

	return ret;
}

int doHttpDownload(const char *URL, char *username, char *password, int bufferSize, char *targetFileName)
{
	int ret = 0;
	char tmpBuf[500] = {'\0'};
	ParameterValue value;

	sprintf(tmpBuf, "%s %s %s %s", "wget", "-O", targetFileName, URL);

//	printf("[%s:%s] tmpBuf: %s\n", __FILE__, __FUNCTION__, tmpBuf);
	DEBUG_OUTPUT (dbglog (SVR_INFO, DBG_TRANSFER, "wget HttpDownloadGet buffer: %s\n", tmpBuf);)

	 //Firmware image to download is initiated and status is recorded.
	value.in_cval = firmwareDownloadStatus[eFWDN_Status_InProgress];
	updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);
	
	/* Update status to fwdnldstatus.txt */
	set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);

	ret = system(tmpBuf);

	if (WEXITSTATUS(ret) != 0 )
	{
		DEBUG_OUTPUT(dbglog (SVR_INFO, DBG_TRANSFER, "[%s] Failed to download due to wget error code %d. \n",
				__FUNCTION__, WEXITSTATUS(ret)); )
		 //Firmware image to download is initiated and status is recorded.
		value.in_cval = firmwareDownloadStatus[eFWDN_Status_Error];
		updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);
		
		/* Update status to fwdnldstatus.txt */
		set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);
		return ERR_DOWNLOAD_FAILURE;
	}

	 //Firmware image to download is initiated and status is recorded.
	value.in_cval = firmwareDownloadStatus[eFWDN_Status_Completed];
	updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);

	/* Update status to fwdnldstatus.txt */
	set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);
	return NO_ERROR;
}

#if 0
/*Comcast update: Since soap based download takes longer time, so change to wget  */
int doHttpDownload(const char *URL, char *username, char *password, int bufferSize, char *targetFileName)
{
	int ret = 0;
	int readSize = 0;
	char *inpPtr;
	int cnt = 0;
	int fd = 0;
	bool isError = false;
	bool digest_init_done = 0;
	/* get a soap structure and use it's buf for input */
	struct soap soap;
	struct soap *tmpSoap;
	/* Data for digest authorization */
	struct http_da_info da_info = {NULL, NULL, NULL, NULL, NULL, NULL, NULL};

	soap_init(&soap);
	tmpSoap = &soap;
	tmpSoap->proxy_host = proxy_host[0] ? proxy_host : NULL;
	tmpSoap->proxy_port = proxy_port;
	tmpSoap->proxy_userid = proxy_userid[0] ? proxy_userid : NULL;
	tmpSoap->proxy_passwd = proxy_passwd[0] ? proxy_passwd : NULL;
	tmpSoap->proxy_http_version = proxy_version[0] ? proxy_version : NULL;
	/* Register Digest Authentication Plugin */
	soap_register_plugin (tmpSoap, http_da);

	if (username != NULL && strlen(username) > 0)
	{
		tmpSoap->userid = username;
		tmpSoap->passwd = password;
	}

	if (password != NULL && strlen(password) > 0)
	{
		tmpSoap->passwd = password;
	}

	soap_begin(tmpSoap);
	tmpSoap->keep_alive = 1;
	tmpSoap->status = SOAP_GET;
	//tmpSoap->authrealm = "";
	soap_set_endpoint(tmpSoap, URL);

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "HttpDownloadGet: %s Host: %s Port: %d\n", URL, tmpSoap->host, tmpSoap->port);
	)

#if defined(WITH_OPENSSL) || defined(WITH_GNUTLS)
#if defined(WITH_CLIENT_SSLAUTH) && !defined(WITH_SERVER_SSLAUTH)
	if(soap_ssl_client_context (tmpSoap,
								SOAP_SSL_REQUIRE_CLIENT_AUTHENTICATION | SOAP_SSLv3_TLSv1,
								"client.pem",			 	/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
								"password", 				/* password to read the key file (not used with GNUTLS) */
								NULL, 						/* cacert file to store trusted certificates (needed to verify server) */
								NULL, 						/* capath to directory with trusted certificates */
								NULL 						/* if randfile!=NULL: use a file with random data to seed randomness */
								))
	{
		soap_print_fault(tmpSoap, stderr);
		soap_destroy (tmpSoap);
		soap_end(tmpSoap);
		soap_done(tmpSoap);
		return ERR_DOWNLOAD_FAILURE;
	}
#endif /* defined(WITH_CLIENT_SSLAUTH) && !defined(WITH_SERVER_SSLAUTH) */

#if defined(WITH_SERVER_SSLAUTH) && !defined(WITH_CLIENT_SSLAUTH)
	/* Comcast update: In case CDL doesn't have authentication*/
	if((strnStartsWith(URL, "https", 5) == 1) && 
	   (soap_ssl_client_context (tmpSoap,
								SOAP_SSL_REQUIRE_SERVER_AUTHENTICATION | SOAP_SSLv3_TLSv1,
								NULL,					 	/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
								NULL, 						/* password to read the key file (not used with GNUTLS) */
								"ca.crt",					/* cacert file to store trusted certificates (needed to verify server) */
								NULL, 						/* capath to directory with trusted certificates */
								NULL 						/* if randfile!=NULL: use a file with random data to seed randomness */
								)))
	{
		soap_print_fault(tmpSoap, stderr);
		soap_destroy (tmpSoap);
		soap_end(tmpSoap);
		soap_done(tmpSoap);
		return ERR_DOWNLOAD_FAILURE;
	}
	// Do not check for cacert file for untrusted sites  
        else if ((strnStartsWith(URL, "http", 4) == 1) && 
		 (soap_ssl_client_context (tmpSoap,
                                                                SOAP_SSL_NO_AUTHENTICATION,
                                                                NULL,                                           /* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
                                                                NULL,                                           /* password to read the key file (not used with GNUTLS) */
                                                                NULL,                                           /* cacert file to store trusted certificates (needed to verify server) */
                                                                NULL,                                           /* capath to directory with trusted certificates */
                                                                NULL                                            /* if randfile!=NULL: use a file with random data to seed randomness */
                                                                )))
        {
                soap_print_fault(tmpSoap, stderr);
                soap_destroy (tmpSoap);
                soap_end(tmpSoap);
                soap_done(tmpSoap);
                return ERR_DOWNLOAD_FAILURE;
        }
#endif /* defined(WITH_SERVER_SSLAUTH) && !defined(WITH_CLIENT_SSLAUTH) */

#if !defined(WITH_CLIENT_SSLAUTH) && !defined(WITH_SERVER_SSLAUTH)
	if(soap_ssl_client_context (tmpSoap,
								SOAP_SSL_NO_AUTHENTICATION,
								NULL,						/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
								NULL, 						/* password to read the key file (not used with GNUTLS) */
								NULL, 						/* cacert file to store trusted certificates (needed to verify server) */
								NULL, 						/* capath to directory with trusted certificates */
								NULL 						/* if randfile!=NULL: use a file with random data to seed randomness */
								))
	{
		soap_print_fault(tmpSoap, stderr);
		soap_destroy (tmpSoap);
		soap_end(tmpSoap);
		soap_done(tmpSoap);
		return ERR_DOWNLOAD_FAILURE;
	}
#endif /* !defined(WITH_CLIENT_SSLAUTH) && !defined(WITH_SERVER_SSLAUTH) */

#if defined(WITH_CLIENT_SSLAUTH) && defined(WITH_SERVER_SSLAUTH)
	if(soap_ssl_client_context (tmpSoap,
								SOAP_SSL_REQUIRE_CLIENT_AUTHENTICATION | SOAP_SSL_REQUIRE_SERVER_AUTHENTICATION | SOAP_SSLv3_TLSv1,
								"client.pem",			 	/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
								"password", 				/* password to read the key file (not used with GNUTLS) */
								"ca.crt", 					/* cacert file to store trusted certificates (needed to verify server) */
								NULL, 						/* capath to directory with trusted certificates */
								NULL 						/* if randfile!=NULL: use a file with random data to seed randomness */
								))
	{
		soap_print_fault(tmpSoap, stderr);
		soap_destroy (tmpSoap);
		soap_end(tmpSoap);
		soap_done(tmpSoap);
		return ERR_DOWNLOAD_FAILURE;
	}
#endif /* defined(WITH_CLIENT_SSLAUTH) && defined(WITH_SERVER_SSLAUTH) */
#endif /* defined(WITH_OPENSSL) || defined(WITH_GNUTLS) */


	if (make_connect(tmpSoap, URL) || tmpSoap->fpost(tmpSoap, URL, tmpSoap->host, tmpSoap->port, tmpSoap->path, NULL, 0))
	{
		ret = tmpSoap->error;
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_TRANSFER, "Request send %d %d\n", ret, tmpSoap->errmode);
		)
		soap_destroy (tmpSoap);
		soap_end(tmpSoap);
		soap_done(tmpSoap);
		return ERR_DOWNLOAD_FAILURE;
	}

	/* Parse the HTML response header and remove it from the work data */
	ret = tmpSoap->fparse(tmpSoap);

	switch(ret)
	{
		case SOAP_OK:
			break;
		case 401: /* Authorization failure */
			http_da_save(tmpSoap, &da_info, (tmpSoap->authrealm != NULL ? tmpSoap->authrealm : CONNECTION_REALM), username, password);
			digest_init_done = 1;
			make_connect(tmpSoap, URL);
			tmpSoap->fprepareinitsend(tmpSoap);
			tmpSoap->fpost(tmpSoap, URL, tmpSoap->host, tmpSoap->port, tmpSoap->path, NULL, 0);
			ret = tmpSoap->fparse(tmpSoap);
			if (ret == SOAP_OK)
			{
				break;
			}
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_TRANSFER, "401 Request %d\n", ret);
			)
			//soap_closesock(tmpSoap);
			http_da_release(tmpSoap, &da_info);
			soap_destroy (tmpSoap);
			soap_end(tmpSoap);
			soap_done(tmpSoap);
			return ERR_TRANS_AUTH_FAILURE;
		default:
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_TRANSFER, "Request %d\n", ret);
			)
			//soap_closesock(tmpSoap);
			http_da_release(tmpSoap, &da_info);
			soap_destroy (tmpSoap);
			soap_end(tmpSoap);
			soap_done(tmpSoap);
			return ERR_DOWNLOAD_FAILURE;
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "Request 2 %d endpoint: %s\n", ret, tmpSoap->endpoint);
	)

	inpPtr = tmpSoap->buf;

	/* Timeout after 30 seconds stall on recv */
	tmpSoap->recv_timeout = 30;
	/* Timeout after 1 minute stall on send */
	tmpSoap->send_timeout = 60;

	/* open the output file, must be given with the complete path */
	if((fd = open(targetFileName, O_RDWR | O_CREAT | O_TRUNC, 0777)) < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_TRANSFER, "Can't open File: %s", targetFileName);
		)
		//soap_closesock(tmpSoap);
		http_da_release(tmpSoap, &da_info);
		soap_destroy (tmpSoap);
		soap_end(tmpSoap);
		soap_done(tmpSoap);
		return ERR_DOWNLOAD_FAILURE;
	}

	/* the first part of the data is already in the buffer,
	 * Therefore calculate the offset and write it to the output file
	 * bufidx is the index to the first data after the HTML header */

	cnt = soap.buflen - soap.bufidx;
	inpPtr = soap.buf;
	inpPtr += soap.bufidx;

	if(write(fd, inpPtr, cnt) < 0)
	{
		isError = true;
	}

	/* Get the rest of the data */
	while (isError == false && (readSize = tmpSoap->frecv(tmpSoap, inpPtr, SOAP_BUFLEN)) > 0)
	{
		if (write(fd, inpPtr, readSize) < 0)
		{
			isError = true;
			break;
		}
		cnt += readSize;
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "BuffSize: %d  read: %d  soap->err: %d\n", bufferSize, cnt, tmpSoap->errnum);
	)

	if (close(fd) < 0)
	{
		//soap_closesock(tmpSoap);
		http_da_release(tmpSoap, &da_info);
		soap_destroy (tmpSoap);
		soap_end(tmpSoap);
		soap_done(tmpSoap);
		return ERR_DOWNLOAD_FAILURE;
	}

	//soap_closesock(tmpSoap);
	http_da_release(tmpSoap, &da_info);
	soap_destroy (tmpSoap);
	soap_end(tmpSoap);
	soap_done(tmpSoap);

	/* Check if we got all the data we expected */
    /* Conmcast update: Not to check the buffer size, so commented below. */
	//if (cnt == 0 || (bufferSize != 0 && cnt != bufferSize))
	/* TR-069_Amendment-3: Do not check the bufferSize of file */
	if (cnt == 0)
	{
		return ERR_DOWNLOAD_FAILURE;
	}
	else
	{
		return NO_ERROR;
	}
}
#endif /* #if 0 */
#endif /* #if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_DEPLOYMENT_UNIT)*/

#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD)
/* Returns 1 if free memory was found
 * Returns 0 if free memory was not found
 * */
static int checkFreeHardMemoryOnDevice(unsigned int FileSize)
{
	// TODO: implementation for specific device.
	return 1;
}
#endif /* #if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD)*/


#ifdef HAVE_FILE_UPLOAD
int execUpload(struct soap *soap,
		char *cwmp__CommandKey,
		char *cwmp__FileType,
		char *cwmp__URL,
		char *cwmp__Username,
		char *cwmp__Password,
		unsigned int cwmp__DelaySeconds,
		Transfers_type initiator,
		char *announceURL,
		char *transferURL,
		struct cwmp__UploadResponse * response)
{
	int returnCode = OK;
	const UploadFile *srcFile;
	TransferEntry *te;

	if (!cwmp__URL || strlen(cwmp__URL) > URL_STR_LEN)
		return ERR_INVALID_ARGUMENT;

	if (!cwmp__Username || strlen(cwmp__Username) > USER_STR_LEN)
		return ERR_INVALID_ARGUMENT;

	if (!cwmp__Password || strlen(cwmp__Password) > PASS_STR_LEN)
		return ERR_INVALID_ARGUMENT;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "CommandKey  : %s\n", cwmp__CommandKey);
	dbglog (SVR_INFO, DBG_TRANSFER, "FileType    : %s\n", cwmp__FileType);
	dbglog (SVR_INFO, DBG_TRANSFER, "URL         : %s\n", cwmp__URL);
	dbglog (SVR_INFO, DBG_TRANSFER, "Name        : %s\n", cwmp__Username);
	dbglog (SVR_INFO, DBG_TRANSFER, "Password    : %s\n", cwmp__Password);
	dbglog (SVR_INFO, DBG_TRANSFER, "Delay       : %d\n", cwmp__DelaySeconds);
	dbglog (SVR_INFO, DBG_TRANSFER, "Initiator   : %d\n", initiator);
	dbglog (SVR_INFO, DBG_TRANSFER, "AnnounceURL : %s\n", announceURL);
	dbglog (SVR_INFO, DBG_TRANSFER, "TransferURL : %s\n", transferURL);
	)

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "Delayed Upload\n");
	)

	response->Status = Transfer_NotStarted;
	response->StartTime = UNKNOWN_TIME;
	response->CompleteTime = UNKNOWN_TIME;

	/* Can't use emallocTemp() because of unfinished transfers with status = 0 */
	te = (TransferEntry *) emallocTypedMem(sizeof(TransferEntry), MEM_TYPE_FILETRANSFER_LIST, 0);
	if (te == NULL)
	{
		return ERR_RESOURCE_EXCEEDED;
	}

	srcFile = type2file(cwmp__FileType, &te->fileSize);
	te->type = UPLOAD;
	strnCopy(te->commandKey, cwmp__CommandKey, (sizeof(te->commandKey) - 1));
	strnCopy(te->fileType, cwmp__FileType, (sizeof(te->fileType) - 1));
	strnCopy(te->URL, cwmp__URL, (sizeof(te->URL) - 1));
	strnCopy(te->username, cwmp__Username, (sizeof(te->username) - 1));
	strnCopy(te->password, cwmp__Password, (sizeof(te->password) - 1));

	if (srcFile)
	{
		strnCopy(te->targetFileName, srcFile->filepath, (sizeof(te->targetFileName) - 1));
	}

	gettimeofday(&te->createTime, NULL);
	te->startTime.tv_sec = te->createTime.tv_sec + cwmp__DelaySeconds;
	te->startTime.tv_usec = te->createTime.tv_usec;
	te->completeTime.tv_sec = 0;
	te->completeTime.tv_usec = 0;
	te->status = Transfer_NotStarted;
	te->faultCode = NO_ERROR;
	te->initiator = initiator;
	indexOfFile = indexOfFile % 10000;
	te->indexOfFile = indexOfFile++;
	strnCopy(te->announceURL, announceURL, (sizeof(te->announceURL) - 1));
	strnCopy(te->transferURL, transferURL, (sizeof(te->transferURL) - 1));
	addTransfer(te);

	return returnCode;
}

static int doUpload(const char *URL, char *username, char *password, char *fileType)
{
	if (strnStartsWith(URL, "http", 4) == 1 || strnStartsWith(URL, "https", 5) == 1)
	{
		return doHttpUpload(URL, username, password, fileType);
	}

	if (strnStartsWith(URL, "ftp", 3) == 1)
	{
		return doFtpUpload(URL, username, password, fileType);
	}

	return ERR_UPLOAD_FAILURE;
}

static int doFtpUpload(const char *URL, char *username, char *password, char *fileType)
{
	/* extract remote host and remote filename from URL
	 * Only ftp://hostname[:port]/dir/dir/remotename  is allowed
	 * the remote name must be relative to the ftp directory
	 * If no remote name is found in the URL the srcFilename is used.
	 * !!!Attention!!! the complete srcFilename including path is used. */

	char *tmp;
	char *host;
	char *remoteFileName;
	unsigned int fileSize;
	long size;
	const UploadFile *srcFile;
	int ret = OK;
	char tmpURL[URL_STR_LEN + 1];

	srcFile = type2file(fileType, &fileSize);
	size = fileSize;

	if (srcFile == NULL || fileSize == 0)
	{
		return ERR_UPLOAD_FAILURE;
	}

	strncpy(tmpURL, URL, URL_STR_LEN);
	/* skip scheme "ftp://" */
	host = tmpURL + 6;
	tmp = host;

	while (*tmp && *tmp != ':' && *tmp != '/')
	{
		tmp++;
	}

	*tmp = '\0';
	tmp++;

	if (strlen(tmp) == 0)
	{
		remoteFileName = srcFile->filename;
	}
	else
	{
		remoteFileName = tmp;
	}

	if ((ret = ftp_login(host, username, password)) != OK)
	{
		return ret;
	}

	ftp_put(remoteFileName, srcFile->filepath, &size);

	if (size < 0)
	{
		ftp_disconnect();
		return ERR_UPLOAD_FAILURE;
	}

	ftp_disconnect();

	return OK;
}

static int doHttpUpload(const char *URL, char *username, char *password, char *fileType)
{
	int ret = 0;
	const UploadFile *srcFile;
	unsigned int fileSize;
	long size;
	/* Data for digest authorization */
	struct http_da_info da_info;
	/* get a soap structure and use it's buf for input */
	struct soap soap;
	struct soap *tmpSoap;

	srcFile = type2file(fileType, &fileSize);
	size = fileSize;

	if (srcFile == NULL || fileSize == 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "doHttpUpload(): FileType \"%s\" is wrong.\n", fileType);
		)
		return ERR_UPLOAD_FAILURE;
	}

	soap_init(&soap);
	tmpSoap = &soap;
	tmpSoap->proxy_host = proxy_host[0] ? proxy_host : NULL;
	tmpSoap->proxy_port = proxy_port;
	tmpSoap->proxy_userid = proxy_userid[0] ? proxy_userid : NULL;
	tmpSoap->proxy_passwd = proxy_passwd[0] ? proxy_passwd : NULL;
	tmpSoap->proxy_http_version = proxy_version[0] ? proxy_version : NULL;
	/* Register Digest Authentication Plugin */
	soap_register_plugin (tmpSoap, http_da);

	soap_begin(tmpSoap);
	tmpSoap->keep_alive = 1;
	tmpSoap->status = SOAP_PUT;
	soap_set_endpoint(tmpSoap, URL);

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "doHttpUpload(): %s Host: %s Port: %d\n", URL, tmpSoap->host, tmpSoap->port);
	)

	tmpSoap->socket = tmpSoap->fopen(tmpSoap, URL, tmpSoap->host, tmpSoap->port);
	if (tmpSoap->error)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "doHttpUpload(): soap->fopen() has returned error %d.\n", tmpSoap->error);
		)
		soap_destroy (tmpSoap);
		soap_end(tmpSoap);
		soap_done(tmpSoap);
		return ERR_UPLOAD_FAILURE;
	}

	if ( (ret = uploadFile(tmpSoap, srcFile, size, false)) != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "doHttpUpload()->uploadFile(1) has returned error %d.\n", ret);
		)
		soap_destroy (tmpSoap);
		soap_end(tmpSoap);
		soap_done(tmpSoap);
		return ERR_UPLOAD_FAILURE;
	}

	/* Parse the HTML response header and remove it from the work data */
	ret = tmpSoap->fparse(tmpSoap);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "doHttpUpload(): PUT request to endpoint: %s. Response: ret = %d, soap->error = %d, soap->status = %d\n", tmpSoap->endpoint, ret, tmpSoap->error, tmpSoap->status);
	)

	switch (tmpSoap->status)
	{
		case 201:
		case 202:
			break;
		case 401: /* Authorization failure */
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_TRANSFER, "doHttpUpload(): 401 Response\n");
			)
			soap_begin(tmpSoap);
			tmpSoap->keep_alive = 1;
			tmpSoap->status = SOAP_PUT;
			soap_set_endpoint(tmpSoap, URL);

			http_da_save(tmpSoap, &da_info, (tmpSoap->authrealm != NULL ? tmpSoap->authrealm : CONNECTION_REALM), username, password);
			tmpSoap->fprepareinitrecv(tmpSoap);

			soap_begin(tmpSoap);
			if ( ( ret = uploadFile(tmpSoap, srcFile, size, false)) != OK)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_TRANSFER, "doHttpUpload()->uploadFile(2) has returned error %d.\n", ret);
				)
				soap_destroy (tmpSoap);
				soap_end(tmpSoap);
				soap_done(tmpSoap);
				return ERR_UPLOAD_FAILURE;
			}
			ret = tmpSoap->fparse(tmpSoap);
			if (tmpSoap->status == 201 || tmpSoap->status == 202)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_INFO, DBG_TRANSFER, "doHttpUpload(): PUT request to endpoint: %s. Response: ret = %d, soap->error = %d, soap->status = %d\n", tmpSoap->endpoint, ret, tmpSoap->error, tmpSoap->status);
				)
				http_da_release(tmpSoap, &da_info);
				soap_destroy (tmpSoap);
				soap_end(tmpSoap);
				soap_done(tmpSoap);
				return OK;
			}

			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_TRANSFER, "doHttpUpload(): soap->fparse(2) has returned unexpected HTTP status %d.\n", tmpSoap->status);
			)
			http_da_release(tmpSoap, &da_info);
			soap_destroy (tmpSoap);
			soap_end(tmpSoap);
			soap_done(tmpSoap);
			return ERR_TRANS_AUTH_FAILURE;

		default:
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_TRANSFER, "doHttpUpload(): soap->fparse(1) has returned unexpected HTTP status %d.\n", tmpSoap->status);
			)
			soap_destroy (tmpSoap);
			soap_end(tmpSoap);
			soap_done(tmpSoap);
			return ERR_UPLOAD_FAILURE;
	}

	soap_destroy (tmpSoap);
	soap_end(tmpSoap);
	soap_done(tmpSoap);
	return OK;
}
#endif /* HAVE_FILE_UPLOAD */


/* Converts the file type from an upload request into a physical filename
 * it returns the physical filename and the file size */
const UploadFile *type2file(const char *fileType, unsigned int *fileSize)
{
	struct stat fstat;
	UploadFile *uploadFile;

	if (*fileType == '1')
	{
		uploadFile = &UploadFiles[0];
	}
	else if (*fileType == '2')
	{
		uploadFile = &UploadFiles[1];
	}
	else
	{
		return NULL;
	}
#ifdef STAT_SUPPORT
	if (stat (uploadFile->filepath, &fstat) == 0)
	{
		*fileSize = (unsigned int)fstat.st_size;
	}
	else
	{
		*fileSize = 0;
	}
#else
	long size = getFileSize(uploadFile->filepath);
	if((size < 0) || (size > UINT_MAX))
	{
		*fileSize = 0;
	}
	else
	{
		*fileSize = (unsigned int) size;
	}
#endif /* STAT_SUPPORT */

	return uploadFile;
}

/* Build a connection to the endpoint */
int make_connect(struct soap *server, const char *endpoint)
{
	soap_set_endpoint(server, endpoint);
	return soap_connect_command(server, SOAP_GET, endpoint, "");
}

#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
/* Initialize the transfer list and load the transfer list from file
 * if there is one available */
int initFiletransfer(DownloadCB dCBB, ScheduleDownloadCB sdCBB, UploadCB uCBB, DownloadCB dCBA, ScheduleDownloadCB sdCBA, UploadCB uCBA)
{
	downloadCBB = dCBB;
	scheduleDownloadCBB = sdCBB;
	uploadCBB = uCBB;
	downloadCBA = dCBA;
	scheduleDownloadCBA = sdCBA;
	uploadCBA = uCBA;
	initTransferList();
	return readTransferList();
}

/* Checks the existence of a delayed filet ransfer.
 * if there is an entry in the transfer list, then return true
 * else false. */
int isDelayedFiletransfer(void)
{
	return (getFirstEntryFromTransferList() != NULL);
}

void checkTransfersAfterReboot()
{
	ListEntry *entry = NULL;
	TransferEntry *te = NULL;

	while ((entry = iterateTransferList(entry)))
	{
		te = (TransferEntry *) entry->data;

		/*After reboot: Set the status Transfer_Completed for tasks which were aborted because of reboot. */
		if (te && (te->status == Transfer_InProgress))
		{
			gettimeofday(&te->completeTime, NULL);
			te->faultCode = (te->type == UPLOAD) ? ERR_UPLOAD_FAILURE : ERR_DOWNLOAD_FAILURE;
			strcpy(te->faultString, "Aborted because of reboot. URL: ");
			strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
			setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
		}

#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD)
	    /* After reboot: Set the status Transfer_Completed for tasks which expected to reboot before reboot. */
		if (te && (te->status == Apply_After_Boot) )
		{
			gettimeofday(&te->completeTime, NULL);
			setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
		}

#endif /* defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) */

	}
}

/* Iterates through the list of delayed transfers.
 * When find one, it executes the up/download and informs the ACS about the result.
 * For informing the ACS, TransferComplete() WebService is called immediately after the transfer.
 *
 * Attention!!! The list of delayed transfers is not cleared
 * call clearDelayedFiletransfers() after this function */

/* param server	Pointer to the soap data
 * param periodOfHandling - period in microseconds of calling this function.
 * return int	Number of downloaded files */
int handleDelayedFiletransfers(unsigned int periodOfHandling)
{
	int cnt = 0;
	int ret = OK;
	ListEntry *entry = NULL;
	TransferEntry *te = NULL;
	ParameterValue value;
	printTransferList(); // San: print information about current states of transfers.

	while ((entry = iterateTransferList(entry)))
	{
		te = (TransferEntry *) entry->data;
		if (!te)
			continue;

		// if completed transfer is found, then increment count of completed transfers
		if (te->status == Transfer_Completed)
		{
			cnt++;
			continue;
		}

		struct timeval actTime;
		gettimeofday(&actTime, NULL);


/*******************************************  D O W N L O A D **************************************************/
#ifdef HAVE_FILE_DOWNLOAD
		/* all conditions full filled ? */
		if (te->type == DOWNLOAD && te->status == Transfer_NotStarted && te->startTime.tv_sec <= actTime.tv_sec)
		{
			te->startTime = actTime;
			setTransferStatusAndSaveTransfEntry(te, Transfer_InProgress);

			te->faultString[0] = '\0';
			if (downloadCBB != NULL)
			{
				/* in targetFileName is the path of the upload file */
				ret = downloadCBB(te);
				if (ret != OK)
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_TRANSFER, "handleDelayedFiletransfers(): Download Callback Before has returned error = %d\n", ret);
					)
					cnt++;
					te->faultCode = ERR_DOWNLOAD_FAILURE;
					strcpy(te->faultString, "Download Callback Before function returned with error. URL: ");
					strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
					setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
					continue;
				}
			}

			ret = doDownload (te->URL, te->username, te->password, te->fileSize, te->targetFileName);
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_TRANSFER, "handleDelayedDownload URL: %s  ret: %d\n", te->URL, ret);
			)
			te->faultCode = ret;
			if (ret != SOAP_OK)
			{
				cnt++;
				strcpy(te->faultString, "Download error. URL: ");
				strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
				setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
			}
			else
			{
				te->faultString[0] = '\0';

				if (downloadCBA != NULL)
				{
					ret = downloadCBA (te);
					if (ret != OK)
					{
						DEBUG_OUTPUT (
								dbglog (SVR_ERROR, DBG_TRANSFER, "handleDelayedFiletransfers(): Download Callback After has returned error = %d\n", ret);
						)
						cnt++;
						te->faultCode = ERR_DOWNLOAD_FAILURE;
						strcpy(te->faultString, "Download Callback After function returned with error. URL: ");
						strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
						setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
						continue;
					}
				}

				if(*(te->fileType) == '1')
				{
					gettimeofday(&te->completeTime, NULL);

					//setTransferStatusAndSaveTransfEntry(te, Apply_After_Boot);

					//For XI3: Firmware image to download is completed and status is recorded.
					value.in_cval = firmwareDownloadStatus[eFWDN_Status_Completed];
					updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);
					
					/* Update status to fwdnldstatus.txt */
					set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);

					DEBUG_OUTPUT (
							dbglog (SVR_DEBUG, DBG_PARAMETER, "[%s:%s:%d]X_COMCAST_COM_FirmwareDownloadStatus: %s\n",__FILE__,__FUNCTION__,__LINE__,value.in_cval);
					)

					if (OK == flashImageReboot())
					{
						setTransferStatusAndSaveTransfEntry(te, Apply_After_Boot);
						callRebootScript();
					}
					else
					{
						cnt++;
						te->faultCode = ERR_COMPLETE_DOWNLOAD;
						strcpy(te->faultString, "Image Flash error. URL: ");
	 				   	strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
	  				   	setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
						
						//For XI3: Firmware image to download is failed and status is recorded.
						value.in_cval = firmwareDownloadStatus[eFWDN_Status_Error];
						updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);
						/* Update status to fwdnldstatus.txt */
						set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);

					}
					//addRebootIdx(REBOOT_CALLBACKS_INDEX_SUM);
					//addRebootIdx(FlashImageReboot);
					//DEBUG_OUTPUT (
					//		dbglog (SVR_INFO, DBG_TRANSFER, "handleDelayedFiletransfers() -> addRebootIdx(0x%.4X)\n", REBOOT_CALLBACKS_INDEX_SUM);
					//)
				}
				else
				{
					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_TRANSFER, "URL: %s Status: \"Completed\"(%d)\n", te->URL, te->status);
					)
					cnt++;
					gettimeofday(&te->completeTime, NULL);
					setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
				}
			}
			continue;
		}
#endif /* HAVE_FILE_DOWNLOAD */


/**********************************************  U P L O A D ******************************************************/
#ifdef HAVE_FILE_UPLOAD
		if (te->type == UPLOAD && te->status == Transfer_NotStarted && te->startTime.tv_sec <= actTime.tv_sec)
		{
			te->startTime = actTime;
			setTransferStatusAndSaveTransfEntry(te, Transfer_InProgress);

			te->faultString[0] = '\0';
			if ( uploadCBB != NULL )
			{
				/* in targetFileName is the path of the upload file */
				ret = uploadCBB( te );
				if (ret != OK)
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_TRANSFER, "handleDelayedFiletransfers(): Upload Callback Before has returned error = %d\n", ret);
					)
					cnt++;
					te->faultCode = ERR_UPLOAD_FAILURE;
					strcpy(te->faultString, "Upload Callback Before function returned with error. URL: ");
					strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
					setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
					continue;

				}
			}

			ret = doUpload (te->URL, te->username, te->password, te->fileType);

			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_TRANSFER, "handleDelayedUpload URL: %s  ret: %d \n", te->URL, ret);
			)

			te->faultCode = ret;

			if (ret != SOAP_OK)
			{
				strcpy(te->faultString, "Upload error. URL: ");
				strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
			}
			else
			{
				te->faultString[0] = '\0';
				if (uploadCBA != NULL)
				{
					ret = uploadCBA( te );
					if (ret != OK)
					{
						DEBUG_OUTPUT (
								dbglog (SVR_ERROR, DBG_TRANSFER, "handleDelayedFiletransfers(): Upload Callback After has returned error = %d\n", ret);
						)
						cnt++;
						te->faultCode = ERR_UPLOAD_FAILURE;
						strcpy(te->faultString, "Upload Callback After function returned with error. URL: ");
						strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
						setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
						continue;
					}
				}
			}

			cnt++;
			gettimeofday(&te->completeTime, NULL);
			setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
			continue;
		}
#endif /* HAVE_FILE_UPLOAD */


/*****************************************  S C H E D U L E D O W N L O A D ******************************************/

#ifdef HAVE_FILE_SCHEDULE_DOWNLOAD
		bool isFirstTimeWindow = false;
		struct TimeWindowStructType *tmpTimeWindow = NULL;
		if (te->type == SCHEDULEDOWNLOAD)
		{
			if ( te->status == Transfer_NotStarted
					&& (
							(isFirstTimeWindow = (te->TimeWindow1
									&& actTime.tv_sec >= te->TimeWindow1->WindowStartTime
									&&  actTime.tv_sec <= te->TimeWindow1->WindowEndTime
									&& te->TimeWindow1->MaxRetries > 0))
									|| (te->TimeWindow2
											&& actTime.tv_sec >= te->TimeWindow2->WindowStartTime
											&&  actTime.tv_sec <= te->TimeWindow2->WindowEndTime
											&& te->TimeWindow2->MaxRetries > 0)
					)
			)
			{
				te->startTime = actTime;
				setTransferStatusAndSaveTransfEntry(te, Transfer_InProgress);

				te->faultString[0] = '\0';
				if (scheduleDownloadCBB != NULL)
				{
					/* in targetFileName is the path of the upload file */
					ret = scheduleDownloadCBB(te, isFirstTimeWindow);
					if (ret != OK)
					{
						DEBUG_OUTPUT (
								dbglog (SVR_ERROR, DBG_TRANSFER, "handleDelayedFiletransfers(): Schedule Download Callback Before has returned error = %d\n", ret);
						)

						if (ret == ERR_SCHEDULE_DOWNLOAD_ISNOT_CONFIRMED)
						{
							if (isFirstTimeWindow && te->TimeWindow2)
							{
								te->TimeWindow1->MaxRetries = 0;
								te->startTime.tv_sec = te->TimeWindow2->WindowStartTime;
								setTransferStatusAndSaveTransfEntry(te, Transfer_NotStarted);
							}
							else
							{
								DEBUG_OUTPUT (
										dbglog (SVR_ERROR, DBG_TRANSFER, "handleDelayedFiletransfers(): ScheduleDownload is not confirmed.\n");
								)
								cnt++;
								te->faultCode = ERR_COMPLETE_DOWNLOAD_TIME_WINDOWS;
								strcpy(te->faultString, "ScheduleDownload is not confirmed. URL: ");
								strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
								setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
							}
							continue;
						}
						cnt++;
						te->faultCode = ERR_DOWNLOAD_FAILURE;
						strcpy(te->faultString, "ScheduleDownload Callback Before function returned with error. URL: ");
						strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
						setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
						continue;
					}
				}

				ret = doDownload (te->URL, te->username, te->password, te->fileSize, te->targetFileName);
				DEBUG_OUTPUT (
						dbglog (SVR_INFO, DBG_TRANSFER, "handleDelayedScheduleDownload URL: %s  ret: %d\n", te->URL, ret);
				)
				if (isFirstTimeWindow)
					tmpTimeWindow = te->TimeWindow1;
				else
					tmpTimeWindow = te->TimeWindow2;

				tmpTimeWindow->MaxRetries--;

				if (ret != SOAP_OK)
				{
					if (tmpTimeWindow->MaxRetries > 0  || (isFirstTimeWindow && te->TimeWindow2))
					{
						gettimeofday(&te->startTime, NULL);
						timeval_add_time(&(te->startTime), periodOfHandling);
						if ( isFirstTimeWindow  &&  te->startTime.tv_sec > te->TimeWindow1->WindowEndTime && te->TimeWindow2)
							te->startTime.tv_sec = te->TimeWindow2->WindowStartTime;
						setTransferStatusAndSaveTransfEntry(te, Transfer_NotStarted);
					}
					else
					{
						cnt++;
						te->faultCode = ret;
						strcpy(te->faultString, "ScheduleDownload error. URL: ");
						strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
						setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
					}
				}
				else
				{
					te->faultCode = ret;
					te->faultString[0] = '\0';

					if (scheduleDownloadCBA != NULL)
					{
						ret = scheduleDownloadCBA (te, isFirstTimeWindow);
						if (ret != OK)
						{
							DEBUG_OUTPUT (
									dbglog (SVR_ERROR, DBG_TRANSFER, "handleDelayedFiletransfers(): ScheduleDownload Callback After has returned error = %d\n", ret);
							)
							cnt++;
							te->faultCode = ERR_DOWNLOAD_FAILURE;
							strcpy(te->faultString, "ScheduleDownload Callback After function returned with error. URL: ");
							strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
							setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
							continue;
						}
					}

					// Will be applied if timewindow is true
					if ( (te->TimeWindow1 && actTime.tv_sec >= te->TimeWindow1->WindowStartTime
							&&  actTime.tv_sec <= te->TimeWindow1->WindowEndTime)
							|| (te->TimeWindow2 && actTime.tv_sec >= te->TimeWindow2->WindowStartTime
									&&  actTime.tv_sec <= te->TimeWindow2->WindowEndTime)
					)
					{
						if(*(te->fileType) == '1')
						{
							gettimeofday(&te->completeTime, NULL);
							setTransferStatusAndSaveTransfEntry(te, Apply_After_Boot);
							addRebootIdx(REBOOT_CALLBACKS_INDEX_SUM);
							DEBUG_OUTPUT (
									dbglog (SVR_INFO, DBG_TRANSFER, "handleDelayedFiletransfers() -> addRebootIdx(0x%.4X)\n", REBOOT_CALLBACKS_INDEX_SUM);
							)
						}
						else
						{
							DEBUG_OUTPUT (
									dbglog (SVR_INFO, DBG_TRANSFER, "URL: %s Status: \"Completed\"(%d)\n", te->URL, te->status);
							)
							cnt++;
							gettimeofday(&te->completeTime, NULL);
							setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
						}
					}
					else
					{
						gettimeofday(&actTime, NULL);
						// if now() > WindowEndTime of last time window.
						if ((te->TimeWindow2  &&  actTime.tv_sec > te->TimeWindow2->WindowEndTime) ||
								(te->TimeWindow2==NULL  &&  te->TimeWindow1  &&  actTime.tv_sec > te->TimeWindow1->WindowEndTime)
						)
						{
							cnt++;
							te->faultCode = ERR_COMPLETE_DOWNLOAD_TIME_WINDOWS;
							strcpy(te->faultString, "Unable to complete ScheduleDownload within specified time windows. URL: ");
							strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
							setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
						}
						else
						{
							setTransferStatusAndSaveTransfEntry(te, Schedule_DL_Wait_for_Apply);
							DEBUG_OUTPUT (
									dbglog (SVR_INFO, DBG_TRANSFER, "URL: %s Status: \"Schedule_DL_Wait_for_Apply\"(%d)\n", te->URL, te->status);
							)
						}
					}
				}
			} /*end of  if ( te->status == Transfer_NotStarted && ... */
			else
			{	// Check, if Schedule_DL_Wait_for_Apply & one from timewindows is true then apply download.
				if (te->status == Schedule_DL_Wait_for_Apply &&
						((te->TimeWindow1 && actTime.tv_sec >= te->TimeWindow1->WindowStartTime &&  actTime.tv_sec <= te->TimeWindow1->WindowEndTime)
								|| (te->TimeWindow2 && actTime.tv_sec >= te->TimeWindow2->WindowStartTime &&  actTime.tv_sec <= te->TimeWindow2->WindowEndTime))
					)
				{
					if(*(te->fileType) == '1')
					{
						gettimeofday(&te->completeTime, NULL);
						//addRebootIdx(FlashImageReboot);
						setTransferStatusAndSaveTransfEntry(te, Apply_After_Boot);
						flashImageReboot();
						//addRebootIdx(REBOOT_CALLBACKS_INDEX_SUM);
						//addRebootIdx(FlashImageReboot);
						DEBUG_OUTPUT (
								dbglog (SVR_INFO, DBG_TRANSFER, "handleDelayedFiletransfers() -> addRebootIdx(0x%.4X)\n", REBOOT_CALLBACKS_INDEX_SUM); 
								/*dbglog (SVR_INFO, DBG_TRANSFER, "handleDelayedFiletransfers() -> addRebootIdx(0x%.4X)\n", FlashImageReboot); */
						)
					}
					else
					{
						DEBUG_OUTPUT (
								dbglog (SVR_INFO, DBG_TRANSFER, "URL: %s Status: \"Completed\"(%d)\n", te->URL, te->status);
						)
						cnt++;
						gettimeofday(&te->completeTime, NULL);
						setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
					}
				}
				else if ((te->TimeWindow2  &&  actTime.tv_sec > te->TimeWindow2->WindowEndTime) ||
						(te->TimeWindow2==NULL  &&  te->TimeWindow1  &&  actTime.tv_sec > te->TimeWindow1->WindowEndTime)
					)
				{
					// If timewindows are closed:
					cnt++;
					te->faultCode = ERR_COMPLETE_DOWNLOAD_TIME_WINDOWS;
					strcpy(te->faultString, "Unable to complete ScheduleDownload within specified time windows. URL: ");
					strnCopy(&(te->faultString[0]) + strlen(te->faultString), te->URL, (sizeof (te->faultString) - 1 - strlen(te->faultString)));
					setTransferStatusAndSaveTransfEntry(te, Transfer_Completed);
				}
			}
		}
#endif /* HAVE_FILE_SCHEDULE_DOWNLOAD */

	}

	return cnt;
}


int handleDelayedFiletransfersEvents(int * isNeedInform)
{
	int ret = SOAP_OK;
	ListEntry *entry = NULL;
	TransferEntry *te = NULL;
	char *event = NULL;

	if (isNeedInform)
	{
		*isNeedInform = 0;
	}

	entry = iterateTransferList(entry);
	while(entry)
	{
		te = (TransferEntry *) entry->data;
		if(!te)
		{
			entry = iterateTransferList(entry);
			continue;
		}
		if (te->status == Transfer_Completed)
		{
			/* It's used in the InformMessage with the TRANSFER COMPLETE event */
			if(te->initiator == ACS)
			{
				switch (te->type)
				{
					case DOWNLOAD:
						event = EV_M_DOWNLOAD;
						break;
					case SCHEDULEDOWNLOAD:
						event = EV_M_SCHEDULEDOWNLOAD;
						break;
					case UPLOAD:
						event = EV_M_UPLOAD;
						break;
					default:
						event = "";
						break;
				}
				addEventCodeMultiple(event, te->commandKey);
				addEventCodeSingle(EV_TRANSFER_COMPLETE);
			}
#ifdef HAVE_AUTONOMOUS_TRANSFER_COMPLETE
			else
			{
				if(isAutonomousTransferComplete)
				{
					addEventCodeSingle(EV_AUTONOMOUS_TRANSFER_COMPLETE);
				}
			}
#endif /* HAVE_AUTONOMOUS_TRANSFER_COMPLETE */
			setTransferStatusAndSaveTransfEntry(te, Informed_to_ACS);
			if (isNeedInform)
			{
				*isNeedInform = 1;
			}
		}
		entry = iterateTransferList(entry);
	}

	return ret;
}

static int setTransferStatusAndSaveTransfEntry(TransferEntry *te, TransferState status)
{
	int ret = OK;
	te->status = status;
#ifdef FILETRANSFER_STORE_STATUS
	ret = writeTransferEntry(te);
#endif /* FILETRANSFER_STORE_STATUS */
	return ret;
}

/* For all transfers with status == 1 a TranferComplete Message is sent to the ACS
 * and the entry is cleared from the file transfer list.
 * In case of an error the status is not changed. */
int clearDelayedFiletransfers(struct soap *server)
{
	int ret = SOAP_OK;
	ListEntry *entry = NULL;
	TransferEntry *te = NULL;

	entry = iterateTransferList(entry);
	while (entry)
	{
		te = (TransferEntry *) entry->data;

		if (!te)
		{
			entry = iterateTransferList(entry);
			continue;
		}

		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_TRANSFER, "ClearDelay Type: %d URL: %s  Status: %s(%d)\n", te->type, te->URL, getTransferStateName(te->status), te->status);
		)

		if (te->status == Informed_to_ACS)
		{
			/* Inform Server */
			Fault fs;
			fs.FaultCode = te->faultCode;
			fs.FaultString = te->faultString;
			fs.__sizeParameterValuesFault = 0;
			fs.SetParameterValuesFault = NULL;

			if (te->initiator == ACS)
			{
				struct cwmp__TransferCompleteResponse transferCompleteResponse;
				unsigned int redirectCount = MAX_REDIRECT_COUNT + 1; //+1 to sent first packet
				setAttrToHTTPandSOAPHeaders(server);
				while(redirectCount)
				{
					ret = soap_call_cwmp__TransferComplete(server, getServerURL(), "", te->commandKey, &fs, te->startTime, te->completeTime, &transferCompleteResponse);
					prevSoapErrorValue = server->error;
					if (ret != SOAP_OK)
					{
						switch (server->error)
						{
							case 301:
							case 302:
							case 307:
								set_server_url_when_redirect(server->endpoint);
								redirectCount--;
								continue;
								break;
							case 401:
							case 407:
								DEBUG_OUTPUT (
										dbglog (SVR_DEBUG, DBG_DU_TRANSFER, "clearDelayedFiletransfers()->soap_call_cwmp__TransferComplete(): HTTP Authentication required. "
												"soap->error=%d\n", server->error);
								)
								setAttrToHTTPandSOAPHeaders(server);
								ret = soap_call_cwmp__TransferComplete(server, getServerURL(), "", te->commandKey, &fs, te->startTime, te->completeTime, &transferCompleteResponse);
								break;
							default:
								break;
						}
						break;
					}
					else
						break;
				}
				DEBUG_OUTPUT (
						dbglog((ret?SVR_WARN:SVR_DEBUG), DBG_MAIN, "soap_call_cwmp__TransferComplete() has returned %d, soap->error=%d\n", ret, server->error);
				)
				if (redirectCount==0)
					return ret;
			}
			else
			{
#ifdef HAVE_AUTONOMOUS_TRANSFER_COMPLETE
				if (suportedRPC.rpc.isAutonomousTransferComplete && isAutonomousTransferComplete)
				{
					struct cwmp__AutonomousTransferCompleteResponse autonomousTransferCompleteResponse;
					unsigned int redirectCount = MAX_REDIRECT_COUNT + 1; //+1 to sent first packet
					setAttrToHTTPandSOAPHeaders(server);
					while(redirectCount)
					{
						ret = soap_call_cwmp__AutonomousTransferComplete(server, getServerURL(), "", te->announceURL, te->transferURL,
								te->type, te->fileType,	te->fileSize, te->targetFileName, &fs, te->startTime, te->completeTime,	&autonomousTransferCompleteResponse);
						prevSoapErrorValue = server->error;
						if (ret != SOAP_OK)
						{
							switch (server->error)
							{
								case 301:
								case 302:
								case 307:
									set_server_url_when_redirect(server->endpoint);
									redirectCount--;
									continue;
									break;
								case 401:
								case 407:
									DEBUG_OUTPUT (
											dbglog (SVR_DEBUG, DBG_DU_TRANSFER, "clearDelayedFiletransfers()->soap_call_cwmp__AutonomousTransferComplete(): HTTP Authentication required. "
													"soap->error=%d\n", server->error);
									)
									setAttrToHTTPandSOAPHeaders(server);
									ret = soap_call_cwmp__AutonomousTransferComplete(server, getServerURL(), "", te->announceURL, te->transferURL,
											te->type, te->fileType,	te->fileSize, te->targetFileName, &fs, te->startTime, te->completeTime,	&autonomousTransferCompleteResponse);
									break;
								default:
									break;
							}
							break;
						}
						else
							break;
					}
					DEBUG_OUTPUT (
							dbglog((ret?SVR_WARN:SVR_DEBUG), DBG_MAIN, "soap_call_cwmp__AutonomousTransferComplete() has returned %d, soap->error=%d\n", ret, server->error);
					)
					if (redirectCount==0)
						return ret;
				}
#endif /* HAVE_AUTONOMOUS_TRANSFER_COMPLETE */
			}

			/* remove this when REGMEN bug fixed */
		   	/* Comcast ACS Fix: Checking for ACS server fault 8803. 
			Remove this when 8803 (soap return as 'ret == 12' ) is handled from ACS */
			if (ret == SOAP_OK || ret == 32  || ret == 12)
			{
				deleteTransferEntry (te);
				DEBUG_OUTPUT (
						dbglog (SVR_INFO, DBG_TRANSFER, "Remove Type: %d URL: %s Status: %s(%d)\n", te->type, te->URL, getTransferStateName(te->status), te->status);
				)
				if (te->TimeWindow1)
					efreeTypedMemByPointer((void**)&te->TimeWindow1, 0);
				if (te->TimeWindow2)
					efreeTypedMemByPointer((void**)&te->TimeWindow2, 0);
				efreeTypedMemByPointer((void**)&te, 0);
				entry = iterateRemoveFromTransferList(entry);
			}
			else
			{
				if(server->error == SOAP_SVR_FAULT)
					getFaultFromACS(server);
				entry = iterateTransferList(entry);
			}
		}
		else
		{
			entry = iterateTransferList(entry);
		}
	}
	return ret;
}

int freeTransferEntryMemory()
{
	transferList.firstEntry = transferList.lastEntry = NULL;
	efreeTypedMemByType(MEM_TYPE_FILETRANSFER_LIST, 0);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "freeTransferEntryMemory() is completed.\n");
	)
	return OK;
}

static int readTransferList(void)
{
	int ret = OK;
	ret = readFtInfos((newFtInfo *) &readTransferListEntry);

	return ret;
}

static int readTransferListEntry(char *name, char *data)
{
	TransferEntry *te = NULL;
	time_t WindowStart,	WindowEnd;
	char WindowMode[WINDOW_MODE_STR_LEN+1] = {"\0"};
	char UserMessage[USER_MESS_STR_LEN+1] = {"\0"};
	int MaxRetriess;


	char *bufptr;
	bufptr = data;
	te = (TransferEntry *) emallocTypedMem(sizeof(TransferEntry), MEM_TYPE_FILETRANSFER_LIST, 0);

	if (te == NULL)
	{
		return ERR_DIM_TRANSFERLIST_READ;
	}

	te->type = a2i(strsep(&bufptr, "|"), NULL);
	strnCopy(te->commandKey, strsep(&bufptr, "|"), CMD_KEY_STR_LEN);
	strnCopy(te->fileType, strsep(&bufptr, "|"), FILE_TYPE_STR_LEN);
	strnCopy(te->URL, strsep(&bufptr, "|"), URL_STR_LEN);
	strnCopy(te->username, strsep(&bufptr, "|"), USER_STR_LEN);
	strnCopy(te->password, strsep(&bufptr, "|"), PASS_STR_LEN);
	te->fileSize = a2ui(strsep(&bufptr, "|"), NULL);
	strnCopy(te->targetFileName, strsep(&bufptr, "|"), TARGET_FILE_NAME_LEN);
	strnCopy(te->successURL, strsep(&bufptr, "|"), URL_STR_LEN);
	strnCopy(te->failureURL, strsep(&bufptr, "|"), URL_STR_LEN);
	te->createTime.tv_sec = a2l(strsep(&bufptr, "|"), NULL);
	te->createTime.tv_usec = a2l(strsep(&bufptr, "|"), NULL);
	te->startTime.tv_sec = a2l(strsep(&bufptr, "|"), NULL);
	te->startTime.tv_usec = a2l(strsep(&bufptr, "|"), NULL);
	te->completeTime.tv_sec = a2l(strsep(&bufptr, "|"), NULL);
	te->completeTime.tv_usec = a2l(strsep(&bufptr, "|"), NULL);
	te->status = a2i(strsep(&bufptr, "|"), NULL);
	te->faultCode = a2ui(strsep(&bufptr, "|"), NULL);
	strnCopy(te->faultString, strsep(&bufptr, "|"), FAULT_STR_LEN);
	strnCopy(te->announceURL, strsep(&bufptr, "|"), URL_STR_LEN);
	strnCopy(te->transferURL, strsep(&bufptr, "|"), URL_STR_LEN);
	te->initiator = a2i(strsep(&bufptr, "|"), NULL);
	te->indexOfFile = a2i(strsep(&bufptr, "|"), NULL);

	if (te->type == SCHEDULEDOWNLOAD)
	{
		//read 1st timewindow
		WindowStart = a2l(strsep(&bufptr, "|"), NULL);
		WindowEnd = a2l(strsep(&bufptr, "|"), NULL);
		strnCopy(WindowMode, strsep(&bufptr, "|"), WINDOW_MODE_STR_LEN);
		strnCopy(UserMessage, strsep(&bufptr, "|"), USER_MESS_STR_LEN);
		MaxRetriess = a2i(strsep(&bufptr, "|"), NULL);
		if ( WindowStart > 0  &&  WindowEnd > WindowStart )
		{
			te->TimeWindow1 = (TimeWindowStructType *) emallocTypedMem(sizeof(TimeWindowStructType), MEM_TYPE_FILETRANSFER_LIST, 0);
			if (te->TimeWindow1 == NULL)
			{
				efreeTypedMemByPointer((void**)&te, 0);
				return ERR_DIM_TRANSFERLIST_READ;
			}
			te->TimeWindow1->WindowStartTime = WindowStart;
			te->TimeWindow1->WindowEndTime = WindowEnd;
			strnCopy(te->TimeWindow1->WindowMode, WindowMode, WINDOW_MODE_STR_LEN);
			strnCopy(te->TimeWindow1->UserMessage, UserMessage, USER_MESS_STR_LEN);
			te->TimeWindow1->MaxRetries = MaxRetriess;
		}
		else
		{
			return ERR_DIM_TRANSFERLIST_READ; // if did not found the first timeWindow
		}

		//read 2nd timewindow
		WindowStart = a2l(strsep(&bufptr, "|"), NULL);
		WindowEnd = a2l(strsep(&bufptr, "|"), NULL);
		strnCopy(WindowMode, strsep(&bufptr, "|"), WINDOW_MODE_STR_LEN);
		strnCopy(UserMessage, strsep(&bufptr, "|"), USER_MESS_STR_LEN);
		MaxRetriess = a2i(strsep(&bufptr, "|"), NULL);
		if ( WindowStart >= te->TimeWindow1->WindowEndTime  &&  WindowEnd > WindowStart )
		{
			te->TimeWindow2 = (TimeWindowStructType *) emallocTypedMem(sizeof(TimeWindowStructType), MEM_TYPE_FILETRANSFER_LIST, 0);
			if (te->TimeWindow2 == NULL)
			{
				efreeTypedMemByPointer((void**)&te->TimeWindow1, 0);
				efreeTypedMemByPointer((void**)&te, 0);
				return ERR_DIM_TRANSFERLIST_READ;
			}
			te->TimeWindow2->WindowStartTime = WindowStart;
			te->TimeWindow2->WindowEndTime = WindowEnd;
			strnCopy(te->TimeWindow2->WindowMode, WindowMode, WINDOW_MODE_STR_LEN);
			strnCopy(te->TimeWindow2->UserMessage, UserMessage, USER_MESS_STR_LEN);
			te->TimeWindow2->MaxRetries = MaxRetriess;
		}
		else
			te->TimeWindow2 = NULL;
	}
	addTransfer(te);

	return OK;
}

/* Prints the transfer list */
static void printTransferList(void)
{
	ListEntry *entry = NULL;
	TransferEntry *te = NULL;
	struct timeval now;
	char * type;
	char * uploadType = "Upload";
	char * downloadType = "Download";
	char * scheduleDownloadType = "ScheduleDownload";
	int n = 0;

	gettimeofday(&now, NULL);

	while((entry = iterateTransferList(entry)))
	{
		n++;
		te = (TransferEntry *) entry->data;
		if (!te)
			continue;

		switch (te->type)
		{
			case UPLOAD:
				type = uploadType;
				break;
			case DOWNLOAD:
				type = downloadType;
				break;
			case SCHEDULEDOWNLOAD:
				type = scheduleDownloadType;
				break;
			default:
				type = NULL;
				break;
		}

		if((te->status == Transfer_NotStarted) && (te->startTime.tv_sec >= now.tv_sec))
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_TRANSFER, "TransferType: %s URL: %s Status: \"NotStarted\"(%d) Start in %d sec\n", type, te->URL, te->status, te->startTime.tv_sec - now.tv_sec);
			)
			continue;
		}
		if((te->status == Transfer_InProgress) && (te->startTime.tv_sec <= now.tv_sec))
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_TRANSFER, "TransferType: %s URL: %s Status: \"InProgress\"(%d) Started %d sec ago\n", type, te->URL, te->status, now.tv_sec - te->startTime.tv_sec);
			)
			continue;
		}
		if((te->status == Transfer_Completed) && (te->completeTime.tv_sec <= now.tv_sec))
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_TRANSFER, "TransferType: %s URL: %s Status: \"Completed\"(%d) Completed %d sec ago\n", type, te->URL, te->status, now.tv_sec - te->completeTime.tv_sec);
			)
			continue;
		}
		if((te->status == Apply_After_Boot) && (te->completeTime.tv_sec <= now.tv_sec) && (getRebootIdx() & REBOOT_CALLBACKS_INDEX_SUM))
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_TRANSFER, "TransferType: %s URL: %s Status: \"Apply_After_Boot\"(%d) Awaiting reboot %d sec\n", type, te->URL, te->status, now.tv_sec - te->completeTime.tv_sec);
			)
			continue;
		}
		if((te->status == Schedule_DL_Wait_for_Apply) && (te->completeTime.tv_sec <= now.tv_sec))
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_TRANSFER, "TransferType: %s URL: %s Status: \"Schedule_DL_Wait_for_Apply\"(%d) Awaiting apply %d sec\n", type, te->URL, te->status, now.tv_sec - te->completeTime.tv_sec);
			)
		}
	}
}

/* Helper function to add an TransferEntry to the TransferList */
static void addTransfer(TransferEntry * te)
{
	addEntryToTransferList(te);
	writeTransferEntry(te);
}

static int writeTransferEntry(TransferEntry * te)
{
	int ret = OK;
	char name[MAX_NAME_SIZE];
	char data[FILETRANSFER_MAX_INFO_SIZE];

	if (!te)
	{
		DEBUG_OUTPUT(
				dbglog (SVR_ERROR, DBG_TRANSFER, "writeTransferEntry(): pointer 'te' must not be NULL\n");
		)
		return ERR_DIM_TRANSFERLIST_WRITE;
	}
	sprintf(name, "%ld_%d", te->createTime.tv_sec, te->indexOfFile);

	DEBUG_OUTPUT(
			dbglog (SVR_INFO, DBG_TRANSFER, "Save: %s   Status: \"%s\"(%d)\n", te->URL, getTransferStateName(te->status), te->status);
	)

	ret = sprintf(data,
			"%d|%s|%s|%s|%s|%s|%d|%s|%s|%s|%ld|%ld|%ld|%ld|%ld|%ld|%d|%d|%s|%s|%s|%d|%d|%ld|%ld|%s|%s|%d|%ld|%ld|%s|%s|%d\n",
			te->type,
			te->commandKey,
			te->fileType,
			te->URL,
			te->username,
			te->password,
			te->fileSize,
			te->targetFileName,
			te->successURL,
			te->failureURL,
			te->createTime.tv_sec,
			te->createTime.tv_usec,
			te->startTime.tv_sec,
			te->startTime.tv_usec,
			te->completeTime.tv_sec,
			te->completeTime.tv_usec,
			te->status,
			te->faultCode,
			te->faultString,
			te->announceURL,
			te->transferURL,
			te->initiator,
			te->indexOfFile,
			(te->TimeWindow1 ? te->TimeWindow1->WindowStartTime : 0 ),
			(te->TimeWindow1 ? te->TimeWindow1->WindowEndTime : 0 ),
			(te->TimeWindow1 ? te->TimeWindow1->WindowMode : NULL ),
			(te->TimeWindow1 ? te->TimeWindow1->UserMessage : NULL ),
			(te->TimeWindow1 ? te->TimeWindow1->MaxRetries : 0 ),
			(te->TimeWindow2 ? te->TimeWindow2->WindowStartTime : 0 ),
			(te->TimeWindow2 ? te->TimeWindow2->WindowEndTime : 0 ),
			(te->TimeWindow2 ? te->TimeWindow2->WindowMode : NULL ),
			(te->TimeWindow2 ? te->TimeWindow2->UserMessage : NULL ),
			(te->TimeWindow2 ? te->TimeWindow2->MaxRetries : 0 )
	);

	if (ret < 0)
	{
		return ERR_DIM_TRANSFERLIST_WRITE;
	}

	ret = storeFtInfo(name, data);

	return ret;
}


/* Returns the string with name of TransferState
 * */
static char *getTransferStateName(TransferState state)
{
	switch (state)
	{
		case Transfer_NotStarted:
			return "Transfer_NotStarted";
		case Transfer_InProgress:
			return "Transfer_InProgress";
		case Transfer_Completed:
			return "Transfer_Completed";
		case Apply_After_Boot:
			return "Apply_After_Boot";
		case Informed_to_ACS:
			return "Informed_to_ACS";
		case Transfer_Aborted:
			return "Transfer_Aborted";
		case Schedule_DL_Wait_for_Apply:
			return "Schedule_DL_Wait_for_Apply";
		default:
			return "";
	}
	return "";
}

#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */


static int deleteTransferEntry(TransferEntry * te)
{
	int ret = OK;
	char buf[MAX_PATH_NAME_SIZE];

	sprintf(buf, "%ld_%d", te->createTime.tv_sec, te->indexOfFile);
	ret = deleteFtInfo(buf);

	return ret;
}



#ifdef HAVE_CANCEL_TRANSFER
int cancelTransferHandler( struct soap *soap, char *CommandKey )
{
	int returnCode = OK;
	int ret = OK;
	ListEntry *entry = NULL;
	TransferEntry *te = NULL;

	if (!soap)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "cancelTransferHandler(): function parameter 'soap' cannot be NULL.\n");
		)
		return ERR_RESOURCE_EXCEEDED;
	}

	if (!CommandKey)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_TRANSFER, "cancelTransferHandler(): function parameter 'CommandKey' cannot be NULL.\n");
		)
		return ERR_RESOURCE_EXCEEDED;
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "cancelTransferHandler() has started for CommandKey = \"%s\"\n", CommandKey);
	)

	entry = iterateTransferList(entry); // get first item in List
	while (entry)
	{
		te = (TransferEntry *) entry->data;

		if ( te && (strcmp(te->commandKey, CommandKey) == 0) )
		{
			if ( (tryCancelTransfer(te) == OK) && ( deleteTransferEntry(te) == OK ) )
			{
				char tmpURL[URL_STR_LEN + 1];
				strcpy(tmpURL, te->URL);
				if (te->TimeWindow1)
					efreeTypedMemByPointer((void**)&te->TimeWindow1, 0);
				if (te->TimeWindow2)
					efreeTypedMemByPointer((void**)&te->TimeWindow2, 0);
				efreeTypedMemByPointer((void**)&te, 0);

				entry = iterateRemoveFromTransferList (entry);

				DEBUG_OUTPUT (
						dbglog (SVR_INFO, DBG_TRANSFER, "cancelTransferHandler(): Transfer is aborted. URL= \"%s\" CommandKey = \"%s\"\n", tmpURL, CommandKey);
				)

				continue;
			}
			else
			{
				returnCode = ERR_CANCELATION_NOT_PERMITTED;
			}
		}
		entry = iterateTransferList(entry);
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "cancelTransferHandler() was finished with returnCode = %d\n", returnCode);
	)

	return returnCode;
}

static int tryCancelTransfer( TransferEntry * te )
{
	int ret;
	// TODO: need add mutex?

	if (te && (te->status == Transfer_NotStarted) )
	{
		te->status = Transfer_Aborted;
		ret = OK;
	}
	else
		ret = -1;

	return ret;
}

#endif /* HAVE_CANCEL_TRANSFER */


#ifdef HAVE_FILE_UPLOAD
/* Read the file into memory and send it via soap_send returns OK or ERR_UPLOAD_FAILURE
  * Implemented with HTTP PUT method.
  * */
static int uploadFile(struct soap *tmpSoap, const UploadFile *srcFile, long fileSize, bool useAuth)
{
	char fileSizeStr[20];

	if (tmpSoap->path[strlen(tmpSoap->path)-1] == '/')
		sprintf(tmpSoap->tmpbuf, "\r\nPUT %s HTTP/1.1\r\n", tmpSoap->path);
	else
		sprintf(tmpSoap->tmpbuf, "\r\nPUT %s/ HTTP/1.1\r\n", tmpSoap->path);
	soap_send(tmpSoap, tmpSoap->tmpbuf);
	sprintf(tmpSoap->tmpbuf, "%s:%d", tmpSoap->host, tmpSoap->port);
	tmpSoap->fposthdr(tmpSoap, "Host", tmpSoap->tmpbuf);
	if (tmpSoap->userid && tmpSoap->passwd && strlen(tmpSoap->userid) + strlen(tmpSoap->passwd) < 761)
	{
		sprintf(tmpSoap->tmpbuf + 262, "%s:%s", tmpSoap->userid, tmpSoap->passwd);
		strcpy(tmpSoap->tmpbuf, "Basic ");
		soap_s2base64(tmpSoap, (const unsigned char*) (tmpSoap->tmpbuf + 262), tmpSoap->tmpbuf + 6, strlen(tmpSoap->tmpbuf + 262));
		tmpSoap->fposthdr(tmpSoap, "Authorization", tmpSoap->tmpbuf);
	}
	tmpSoap->fposthdr(tmpSoap, "User-Agent", info_for_soap_header);
	tmpSoap->fposthdr(tmpSoap, "Connection", tmpSoap->keep_alive ? "keep_alive" : "close");
	tmpSoap->fposthdr(tmpSoap, "Content-Type", srcFile->filetype);

	// Calc contentlength
	sprintf(fileSizeStr, "%ld", fileSize);
	tmpSoap->fposthdr(tmpSoap, "Content-Length", fileSizeStr);
	soap_send(tmpSoap, "\r\n");

	if (sendFile(tmpSoap, srcFile) != OK)
	{
		soap_end(tmpSoap);
		soap_done(tmpSoap);
		return ERR_UPLOAD_FAILURE;
	}

	soap_send(tmpSoap, "\r\n");

	return OK;
}

static int sendFile(struct soap *tmpSoap, const UploadFile *srcFile)
{
	int ret = OK;
	int fd;
	int bufsize = 1023;
	int rSize;

	if((fd = open(srcFile->filepath, O_RDONLY)) > 0)
	{
		while((rSize = read(fd, tmpSoap->tmpbuf, bufsize)) > 0)
		{
			tmpSoap->tmpbuf[rSize] = '\0';
			if (soap_send_raw(tmpSoap, tmpSoap->tmpbuf, rSize) != SOAP_OK)
			{
				ret = ERR_UPLOAD_FAILURE;
				break;
			}
		}
		close(fd);
		return ret;
	}
	else
	{
		return ERR_UPLOAD_FAILURE;
	}
}
#endif /* HAVE_FILE_UPLOAD */

/********* Function for work with TransferList *******/

/* Initialization Transfer List */
static void initTransferList()
{
	pthread_mutex_lock(&transferList_usage_mutex);
	initList(&transferList);
	pthread_mutex_unlock(&transferList_usage_mutex);
}

/* Iterate  Transfer List */
static ListEntry * iterateTransferList(ListEntry * entry)
{
	pthread_mutex_lock(&transferList_usage_mutex);
	entry = iterateList(&transferList, entry);
	pthread_mutex_unlock(&transferList_usage_mutex);
	return entry;
}

/* Add new Entry to Transfer List */
static void addEntryToTransferList(TransferEntry * te)
{
	pthread_mutex_lock(&transferList_usage_mutex);
	addEntry(&transferList, te, MEM_TYPE_FILETRANSFER_LIST);
	pthread_mutex_unlock(&transferList_usage_mutex);
}

/* Get first entry from Transfer List */
static ListEntry * getFirstEntryFromTransferList()
{
	ListEntry * entry = NULL;
	pthread_mutex_lock(&transferList_usage_mutex);
	entry = getFirstEntry(&transferList);
	pthread_mutex_unlock(&transferList_usage_mutex);
	return entry;
}

/* Remove entry from Transfer List */
static ListEntry * iterateRemoveFromTransferList(ListEntry * entry)
{
	pthread_mutex_lock(&transferList_usage_mutex);
	entry = iterateRemove (&transferList, entry);
	pthread_mutex_unlock(&transferList_usage_mutex);
	return entry;
}

/* get size Transfer List */
static int getTransferListSize()
{
	int ret = 0;
	pthread_mutex_lock(&transferList_usage_mutex);
	ret = getListSize(&transferList);
	pthread_mutex_unlock(&transferList_usage_mutex);
	return ret;
}

int flashImageReboot()
{
	int ret = -1;
	printf("[%s:%s:%d]\n", __FILE__, __FUNCTION__, __LINE__);

#if USE_SWUPMGR
    ret = swupmgr_installSWImage();
#else
	ParameterValue value;
	if (true == callImageFlashScript())
	{
		 //Firmware image to download is initiated and status is recorded.
		value.in_cval = firmwareDownloadStatus[eFWDN_Status_Successful];
		updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);
		/* Update status to fwdnldstatus.txt */
		set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);
	    return OK;
	}
	else
	{
		 //Firmware image to download is initiated and status is recorded.
		value.in_cval = firmwareDownloadStatus[eFWDN_Status_Error];
		updateParamValue(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value);
		/* Update status to fwdnldstatus.txt */
		set_ParamValues_tr69hostIf(FIRMWARE_DOWNLOAD_STATUS_PATH_NAME,StringType,&value, StringType, &value);
		DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_TRANSFER, "%s() Failed to flash image.\n", __FUNCTION__ );
			)
		return ret;

	}
#endif
	printf("[%s:%s:%d]\n", __FILE__, __FUNCTION__, __LINE__);

    return ret;
}


#endif /* HAVE_FILE */
