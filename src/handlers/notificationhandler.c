/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"

#ifdef HAVE_NOTIFICATION

#include <pthread.h>
#include "parameter.h"
#include "notificationhandler.h"


time_t lastInformTime;

extern pthread_mutex_t paramLock;
extern pthread_mutex_t informLock;

void *passiveNotificationHandler(void *localVariables)
{
	ParameterValue value;
	int ret = OK;
	unsigned int DefaultActiveNotificationThrottle = 0;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	sleep(5);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_MAIN, "Passive Notification Enabled\n");
	)

	while (true)
	{
		ret = retrieveParamValue(ACTIVE_NOTIFICATION_TIME, UnsignedIntType, &value);
		DefaultActiveNotificationThrottle = value.out_uint;
		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);

		if(ret != OK)
		{
			DEBUG_OUTPUT(
					dbglog (SVR_ERROR, DBG_MAIN, "passiveNotificationHandler(): Can`t get notification time. An attempt will be made ​​after 1 second.\n");
			)
			sleep(1);
			continue; // try to read ACTIVE_NOTIFICATION_TIME after 1 sec.
		}
		if (DefaultActiveNotificationThrottle == 0)
		{
			DEBUG_OUTPUT(
					dbglog (SVR_INFO, DBG_MAIN, "passiveNotificationHandler(): DefaultActiveNotificationThrottle == 0. Passive notification will not be started.\n");
			)
			sleep(10);
			continue; // try to read ACTIVE_NOTIFICATION_TIME after 10 sec.
		}

		sleep(DefaultActiveNotificationThrottle);

		pthread_mutex_lock(&paramLock);
		DEBUG_OUTPUT (
				dbglog (SVR_DEBUG, DBG_MAIN, "Start passive notification handler\n");
		)

#ifdef WITH_USE_SQLITE_DB
		executeDBCommand(BEGIN);
#endif
		checkPassiveNotification();

#ifdef WITH_USE_SQLITE_DB
		executeDBCommand(END);
#endif

		DEBUG_OUTPUT	(
				dbglog (SVR_DEBUG, DBG_MAIN, "Finish passive notification handler\n");
		)
		pthread_mutex_unlock(&paramLock);
	}
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	pthread_exit(NULL);
	return NULL;
}

void *activeNotificationHandler(void *localVariables)
{
	int sendInformFlag = false;
	unsigned int DefaultActiveNotificationThrottle = 0;
	ParameterValue value;
	int ret = OK;
	time_t currentTime;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_MAIN, "Active Notification Enabled\n");
	)

	/* wait last inform */
	while(true)
	{
		if(getLastInformTime() == 0)
		{
			sleep(1);
			continue;
		}
		break;
	}

	/* start active notification */
	while (true)
	{
		/* get current Time */
		currentTime = time(NULL);

		if( (currentTime % 10) == 0)
		{
			/* get default active notofication parameter */
			ret = retrieveParamValue(ACTIVE_NOTIFICATION_TIME, UnsignedIntType, &value);
			DefaultActiveNotificationThrottle = value.out_uint;
			efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);

			if(ret != OK)
			{
				DEBUG_OUTPUT(
						dbglog (SVR_ERROR, DBG_MAIN, "activeNotificationHandler(): Can`t get notification time. An attempt will be made ​​after 1 second.\n");
				)
				sleep(1);
				continue; // try to read ACTIVE_NOTIFICATION_TIME after 1 sec.
			}

			if (DefaultActiveNotificationThrottle == 0)
			{
				DEBUG_OUTPUT(
						dbglog (SVR_INFO, DBG_MAIN, "activeNotificationHandler(): DefaultActiveNotificationThrottle == 0. Active notification will not be started.\n");
				)
				sleep(1);
				continue; // try to read ACTIVE_NOTIFICATION_TIME at next time period.
			}

			if (pthread_mutex_trylock (&informLock)==OK)
			{
				/* check time for notification */
				if (currentTime >= (getLastInformTime() + DefaultActiveNotificationThrottle))
				{
					/* run notification */
					pthread_mutex_lock(&paramLock);
					DEBUG_OUTPUT (
							dbglog (SVR_DEBUG, DBG_MAIN, "Start active notification handler\n");
					)

#ifdef WITH_USE_SQLITE_DB
					executeDBCommand(BEGIN);
#endif

					sendInformFlag = checkActiveNotification();

#ifdef WITH_USE_SQLITE_DB
					executeDBCommand(END);
#endif
					pthread_mutex_unlock(&paramLock);
					efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);

					if (sendInformFlag)
					{
						/* send inform by active notification */
						DEBUG_OUTPUT (
								dbglog (SVR_DEBUG, DBG_MAIN, "Finish active notification handler. Inform with \"4 VALUE CHANGE\" will be sent.\n");
						)
						pthread_mutex_unlock (&informLock);
						setAsyncInform(true);
					}
					else
					{
						DEBUG_OUTPUT (
								dbglog (SVR_DEBUG, DBG_MAIN, "Finish active notification handler. Inform with \"4 VALUE CHANGE\" will not be sent.\n");
						)
						pthread_mutex_unlock (&informLock);
					}

					if (currentTime == time(NULL)) // If notification has had time to be fulfilled less 1 second
						sleep(1);
				}
				else
				{
					pthread_mutex_unlock (&informLock);
					sleep(1);
				}
			}
			else
				sleep(1);
		}
		else
			sleep(1);
	} // while (true)
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	pthread_exit(NULL);
	return NULL;
}

time_t getLastInformTime()
{
	return lastInformTime;
}

void setLastInformTime(time_t t)
{
	lastInformTime = t;
}

#endif /* HAVE_NOTIFICATION */

