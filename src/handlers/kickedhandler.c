/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"

#ifdef HAVE_KICKED

#include <pthread.h>

#include "eventcode.h"
#include "paramconvenient.h"

#define		BUF_SIZE	1024
#define		NEXT_URL	"NextURL"
#define		FAULT_URL	"FaultURL"

extern int procId;
extern pthread_mutex_t informLock;
extern char host[];

extern int  prevSoapErrorValue;

/* used to inform the mainloop that the kickedHandler could bind on the port. */
pthread_cond_t kickedHandlerStarted = PTHREAD_COND_INITIALIZER;
pthread_mutex_t kickedHandlerMutexLock = PTHREAD_MUTEX_INITIALIZER;
bool isKicked = true;

static char buf[BUF_SIZE + 1];
static char Command[SOAP_TAGLEN];
static char Referer[SOAP_TAGLEN];
static char Arg[SOAP_TAGLEN];
static char Next[SOAP_TAGLEN];
static char NextURL[SOAP_TAGLEN] = "";
static bool KickedFlag = false;

/* For Kicked a Kicked Message is sent to the ACS */
int clearKicked(struct soap *server)
{
	int ret = OK;

	if(KickedFlag && isKicked)
	{
		struct cwmp__KickedResponse kickedResp;
		unsigned int redirectCount = MAX_REDIRECT_COUNT + 1; //+1 to sent first packet
		setAttrToHTTPandSOAPHeaders(server);
		while(redirectCount)
		{
			ret = soap_call_cwmp__Kicked(server, getServerURL(), "", Command, Referer, Arg, Next, &kickedResp);
			prevSoapErrorValue = server->error;
			if (ret != SOAP_OK)
			{
				switch (server->error)
				{
					case 301:
					case 302:
					case 307:
						set_server_url_when_redirect(server->endpoint);
						redirectCount--;
						continue;
						break;
					case 401:
					case 407:
						DEBUG_OUTPUT (
								dbglog (SVR_DEBUG, DBG_KICK, "clearKicked()->soap_call_cwmp__Kicked(): HTTP Authentication required. soap->error=%d\n", server->error);
						)
						setAttrToHTTPandSOAPHeaders(server);
						ret = soap_call_cwmp__Kicked(server, getServerURL(), "", Command, Referer, Arg, Next, &kickedResp);
						break;
					default:
						break;
				}
				break;
			}
			else
				break;
		}
		if (redirectCount==0 || ret != SOAP_OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_KICK, "clearKicked()->soap_call_cwmp__Kicked(): has returned soap->error=%d\n", server->error);
			)
			strcpy(NextURL, FAULT_URL);
			return ret;
		}

		strncpy(NextURL, (!kickedResp.NextURL || strcmp(kickedResp.NextURL, Next) ? FAULT_URL : kickedResp.NextURL), sizeof(NextURL) -1 );

		KickedFlag = false;

		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_KICK, "Call Kicked ret: %d\n", ret);
		)
	}
	return ret;
}

/* This function handles a call from the Kicked */
void *kickedHandler(void *localSoap)
{
	int ret = OK;
	int mutexStat = OK;
	struct soap *soap = (struct soap *) localSoap;
	int timeout = 0;
	struct timespec delay;
	struct timespec rem;
	delay.tv_sec = 1;
	delay.tv_nsec = 0;
	int requestCnt = 0;
	time_t actualTime = 0;
	time_t requestSlot = 0;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_KICK, "Host: %s , Kicked Port: %d\n", host, (KICKED_NOTIFICATION_PORT + (procId)));
	)

	soap->accept_timeout = timeout;
	soap->recv_timeout = 30;

	/* wait on port */
	soap->bind_flags = SO_REUSEADDR;
	ret = soap_bind(soap, host, KICKED_NOTIFICATION_PORT + (procId), 100);

	while (ret < SOAP_OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_KICK, "Kicked port doesn't bind. The next attempt will be fulfilled. "
						"Bind(): ret = %d, errno = %d (%s), soap->error = %d (see stdsoap2.h)\n", ret, soap->errnum, strerror(soap->errnum), soap->error);
		)
		//soap_closesock (soap);
		soap_destroy(soap);  // delete deserialized objects
		soap_end(soap); // Remove temporary data and deserialized data
		soap->bind_flags = SO_REUSEADDR;
		ret = soap_bind(soap, host, KICKED_NOTIFICATION_PORT + (procId), 100);
		nanosleep(&delay, &rem);
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_KICK, "Kicked port binds success\n");
	)

#ifdef USE_DELAY_AT_THREAD_START
	/* tell the mainloop we are ready to serve the Kicked */
	nanosleep(&delay, &rem);
#endif
	pthread_mutex_lock(&kickedHandlerMutexLock);
	pthread_cond_signal(&kickedHandlerStarted);
	pthread_mutex_unlock(&kickedHandlerMutexLock);

	ret = soap_accept(soap);

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_KICK, "Kicked Accept ret: %d\n", ret);
	)

	/* init DoS attack detection */
	requestSlot = time(NULL);
	requestSlot += MAX_REQUEST_TIME;

	while (true)
	{
		/* count the number of requests in a defined period of time
		 * and reject any request if the number of request is above a maximum number
		 *
		 * 	requestSlot ( lower and upper time) is set
		 *  MAX_REQUEST_TIME defined
		 *  MAX_REQUESTS_PER_TIME defined
		 *  reqCounter set to zero
		 *
		 *  			requestSlot			AnalysisRequests
		 *	-------------------------------------------------------
		 *  OK  	 	actTime in          <= MaxRequests
		 *  Fail		actTime in			> MaxRequests
		 *  OK  		actTime out			Reset reqCounter, recalc requestSlot */
		if (ret > 0)
		{
			/* check for DoS attack */
			actualTime = time(NULL);
			if (actualTime <= requestSlot)
			{
				requestCnt++;
				if (requestCnt > MAX_REQUESTS_PER_TIME)
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_KICK, "kickedHandler(): DoS attack was detected. Request count is %d per %d seconds. The response 503 error message.\n", requestCnt, MAX_REQUEST_TIME);
					)
					/* Write a HTTP 503 Error message if we are not ready to serve the request */

					soap->fresponse(soap, 503, 0);
					//soap_closesock (soap);
					soap_destroy(soap);  // delete deserialized objects
					soap_end(soap); // Remove temporary data and deserialized data
					efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
					ret = soap_accept(soap);
					continue;
				}
			}
			else
			{
				/* recalc requestSlot value */
				requestSlot = actualTime + MAX_REQUEST_TIME;
				requestCnt = 0;
			}

			mutexStat = pthread_mutex_trylock(&informLock);
			if(mutexStat == OK)
			{
				soap_getline(soap, buf, BUF_SIZE);

				/* Check FaultURL site */
				if(strstr(buf, FAULT_URL))
				{
					pthread_mutex_unlock(&informLock);

					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_KICK, "FaultURL site\n");
					)

					/* Go to end of HTTP/MIME header */
					/* empty line: end of HTTP/MIME header */
					while(*buf)
					{
						soap_getline(soap, buf, BUF_SIZE);
					}

					/* Go to FaultURL site */
					soap_send(soap, "FaultURL site.");
					//soap_closesock (soap);
					soap_destroy(soap);  // delete deserialized objects
					soap_end(soap); // Remove temporary data and deserialized data
					efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
					ret = soap_accept(soap);
					continue;
				}

				/*Check NextURL site */
				if(strstr(buf, NEXT_URL) && !strstr(buf, "command"))
				{
					pthread_mutex_unlock(&informLock);

					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_KICK, "NextURL site\n");
					)

					/* Go to end of HTTP/MIME header*/
					/* empty line: end of HTTP/MIME header */
					while(*buf)
					{
						soap_getline(soap, buf, BUF_SIZE);
					}

					/* Go to NextURL site */
					soap_send(soap, "NextURL site.");
					//soap_closesock (soap);
					soap_destroy(soap);  // delete deserialized objects
					soap_end(soap); // Remove temporary data and deserialized data
					efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
					ret = soap_accept(soap);
					continue;
				}

				char *s;
				Command[0] = 0;
				strncpy(Referer, soap->href, sizeof(Referer));
				Arg[0] = 0;
				Next[0] = 0;

				s = strtok(buf, "=");
				if (s)
				{
					s = strtok(NULL, "&");
					if (s)
					{
						strncpy(Command, s, sizeof(Command)-1 );
						s = strtok(NULL, "=");
						if (s)
						{
							s = strtok(NULL, "&");
							if (s)
							{
								strncpy(Arg, s, sizeof(Arg)-1 );
								s = strtok(NULL, "=");
								if (s)
								{
									s = strtok(NULL, " ");
									if (s)
									{
										strncpy(Next, s, sizeof(Next)-1 );
									}
								}
							}
						}
					}
				}

				char *KickURL;
				char *SiteURL;
				bool flag_out = false;

				/* find X-Forwarded-For */
				do
				{
					soap_getline(soap, buf, BUF_SIZE);
					if(strstr(buf, "X-Forwarded-For"))
					{
						SiteURL = strchr(buf, ' ');
						while (SiteURL && *SiteURL != 0 && *SiteURL == ' ')
						{
							SiteURL++;
						}

						if (SiteURL)
						{
							ret = getParameter(KICKURL, &KickURL);
							if (ret != OK || inet_addr(KickURL) != inet_addr(SiteURL))
							{
								break;
							}
							else
							{
								flag_out = true;
								break;
							}
						}
						else
						{
							break;
						}
					}
				}while(*buf);

				/* Go to end of HTTP/MIME header*/
				/* empty line: end of HTTP/MIME header */
				while(*buf)
				{
					soap_getline(soap, buf, BUF_SIZE);
				}

				if(!flag_out)
				{
					pthread_mutex_unlock(&informLock);

					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_KICK, "Kicked. Not accepted. The response 503 error message.\n");
					)

					/* Write a HTTP 503 Error message */
					soap->fresponse(soap, 503, 0);
					//soap_closesock (soap);
					soap_destroy(soap);  // delete deserialized objects
					soap_end(soap); // Remove temporary data and deserialized data
					efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
					ret = soap_accept(soap);
					continue;
				}

				DEBUG_OUTPUT (
						dbglog (SVR_INFO, DBG_KICK, "Kicked. Accepted\n");
				)

				addEventCodeSingle(EV_KICKED);
				KickedFlag = true;

				pthread_mutex_unlock(&informLock);
				setAsyncInform(true);

				strncpy(soap->endpoint, NextURL, sizeof(soap->endpoint));

				/*302 Found */
				soap->fresponse(soap, 302, 0);
				//soap_closesock (soap);
				soap_destroy(soap);  // delete deserialized objects
				soap_end(soap); // Remove temporary data and deserialized data
				efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
				ret = soap_accept(soap);
			}
			else
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_KICK, "kickedHandler()->pthread_mutex_trylock() returns mutexStat = %d. The response 503 error message..\n", mutexStat);
				)
				/* Write a HTTP 503 Error message if we are not ready to serve the request */
				soap->fresponse(soap, 503, 0);
				//soap_closesock (soap);
				soap_destroy(soap);  // delete deserialized objects
				soap_end(soap); // Remove temporary data and deserialized data
				efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
				ret = soap_accept(soap);
			}
		}
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_KICK, "Kicked Accept error: %d\n", errno);
			)

			nanosleep(&delay, &rem);
		}
	}
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	pthread_exit(NULL);
	return NULL;
}

#endif /* HAVE_KICKED */
