/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef stunhandler_H
#define stunhandler_H

#include "dimark_globals.h"

#ifdef WITH_STUN_CLIENT

int runStunHandler();
int terminateStunHandler();

#endif /* WITH_STUN_CLIENT */

#endif /* stunhandler_H */
