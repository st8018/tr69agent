/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"

#ifdef WITH_STUN_CLIENT

#include <pthread.h>

#include "STUN_client.h"
#include "STUN_dimark.h"
#include "paramconvenient.h"

#define URL_STR_LEN 256

extern pthread_mutex_t informLock;
extern pthread_t stunTid;

static void *stunHandler (void *);
static int getSTUNServerAddressPort(char *, unsigned int, unsigned int *);

int runStunHandler()
{
	setCancelStunThreadActivated(0);
	return pthread_create (&stunTid, NULL, stunHandler, NULL);
}

int terminateStunHandler()
{
	int ret;
	if (stunTid > 0)
	{
		ret = pthread_cancel(stunTid);
		if (ret) // if ret == ESRCH
			return OK;
		setCancelStunThreadActivated(1);
		shutdownUdpCRSocket();
		closeUDPSocket();
		pthread_join(stunTid, NULL);
		stunTid = 0;
	}
	return OK;
}

void stunThreadExitCallback(void * arg)
{
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_STUN, "stunThreadExitCallback() has been done. The allocated memory is freed.\n");
	)
}

/*
 * This thread handles the STUN client process, including interaction
 * with the STUN server as well as receiving UDP Connection Requests
 */
void *
stunHandler (void *param)
{
	unsigned int periodicIntervalDelay, minPeriodicIntervalDelay;
	int maxPeriodicIntervalDelay;
	char stun_host[URL_STR_LEN] = {"\0"};
	int isStunEnabled = 0;
	unsigned int  stun_port;
	int nat_stat;
	static int prev_nat_stat = 0;
	char udp_conn[300];
	char *pUdp_conn;
	char new_conn[300];
	char mapped_ip[32];
	int  mapped_port;
	int result;
	bool b;
	int mutexStat = OK;
	char name[32] = { '\0' };
	ParameterValue value;
	char *tmp_username,   *tmp_password;
	char  cr_username[100], cr_password[100];
	time_t startTime, tmpTime;
	unsigned int isInformNeeded = 0;

	sleep(10); // sleep 10 seconds

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_STUN, "stun Handler started\n");
	)

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	pthread_cleanup_push(stunThreadExitCallback, NULL);

	value.in_cval = NULL;

	/* Set cancel state as Enable, so pthread_cancel can cancel asynchronously.*/
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_testcancel();


	for (;;)
	{
		startTime = time(NULL);

		minPeriodicIntervalDelay = getMinStunPeriodicInterval();
		maxPeriodicIntervalDelay = getMaxStunPeriodicInterval();

		if ( (maxPeriodicIntervalDelay > 0) && ( (unsigned int)maxPeriodicIntervalDelay > (minPeriodicIntervalDelay+1)) )
			periodicIntervalDelay = maxPeriodicIntervalDelay - 1;
		else
			// if maxPeriodicIntervalDelay = -1 then use minPeriodicIntervalDelay, because no maximum period is specified.
			periodicIntervalDelay = (minPeriodicIntervalDelay != 0) ? minPeriodicIntervalDelay+1 : 1;


		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_STUN, "stunHandler(): stun_process periodic interval %d\n",periodicIntervalDelay);
		)

		/* is_Stun_enabled() returns value of parameter (e.g */
		/* 0=disabled, 1=enabled, or -1 if param fetch fails */
		isStunEnabled = is_Stun_enabled(STUNENABLE);
		if (isStunEnabled != 0 )
		{
			DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_STUN, "stunHandler(): STUN is enabled\n");
			)

			result = getSTUNServerAddressPort(stun_host, sizeof(stun_host), &stun_port);
			if (result != OK)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_STUN, "stunHandler->getSTUNServerAddressPort() has returned error = %d\n", result);
				)
				if ( periodicIntervalDelay >= ( tmpTime = (time(NULL)-startTime) ) )
					periodicIntervalDelay -= tmpTime;
				else
					periodicIntervalDelay = 0;
				sleep(periodicIntervalDelay);
				continue;
			}

			DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_STUN, "stunHandler->bindingRequest() pre: stun_host = %s, stun_port = %d\n", stun_host, stun_port);
			)

			setCancelStunThreadActivated(0);
			/*Commented above line execpt bindingRequest.
			 * Reason: If exection in this point, then due to disable state, thread is not getting killed or joined. 
                         * as a result, it fails to kill during rebootWithoutFactoryReset()*/
//			pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
			result = bindingRequest(stun_host,stun_port,NULL,NULL, mapped_ip,&mapped_port);
//			pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
//			pthread_testcancel(); // If PTHREAD_CANCEL_ASYNCHRONOUS is used this line is not mandatory.
			/* if OK, then got a valid response from STUN server */
			/* use mapped_ip:mapped_port as new UDP connection info */
			/* Otherwise, use default */
			if (result == OK)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_STUN,"STUN server at %s:%d has returned mapped ip %s,  port %d,  with status %d\n",
								stun_host, stun_port,mapped_ip,mapped_port, result);
				)

				//TODO: add support of IPv6

				sprintf(new_conn,"%s:%d",mapped_ip,mapped_port);
				if ( (is_IP_local( mapped_ip, sizeof(mapped_ip)) == 0) && ( mapped_port == ACS_UDP_NOTIFICATION_PORT))
				{
					nat_stat = 0;		// no addr or port translation detected
				}
				else
				{
					nat_stat = 1;		// translation detected
				}
			}
			else
			{
				DEBUG_OUTPUT (
						dbglog (SVR_WARN, DBG_STUN,"STUN server at %s:%d has not returned mapped ip & port\n",
								stun_host, stun_port);
						dbglog (SVR_ERROR, DBG_STUN, "stunHandler->bindingRequest() FAIL stat = %d\n", result);
				)
				getUdpCRU(NULL, StringType, &value);
				sprintf(new_conn,"%s",value.out_cval);
				nat_stat = 0;
			}


			DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_STUN, "STUN enabled new_conn = %s  nat_stat = %d\n", new_conn, nat_stat);
			)
		} // if (is_Stun_enabled(STUNENABLE) == 1 )
		else
		{
			/* STUN Disabled, use local info */
			getUdpCRU(NULL, StringType, &value);
			sprintf(new_conn,"%s",value.out_cval);
			nat_stat = 0;

			DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_STUN, "STUN disabled new_conn = %s\n", new_conn);
			)
		} // end  "if (is_Stun_enabled(STUNENABLE) == 1 )"




		// Us result we have value in  nat_stat
		memset(udp_conn, 0, sizeof(udp_conn));
		result = getParameter(UDPCONNECTIONREQUESTADDRESS,&pUdp_conn);
		if (result != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_STUN, "stunHandler->getParameter(%s) has returned error = %d\n", UDPCONNECTIONREQUESTADDRESS, result);
			)
		}
		else
		{
			if (pUdp_conn)
				strncpy(udp_conn, pUdp_conn, sizeof(udp_conn));
			DEBUG_OUTPUT (
				dbglog (SVR_DEBUG, DBG_STUN, "STUN current conn = %s, new_con = %s\n", udp_conn, new_conn);
			)
		}

		DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_STUN, "STUN NAT Detected nat_stat = %d\n", nat_stat);
		)
		if (nat_stat != prev_nat_stat) //if nat_stat is changed then store new parameter value.
		{
			result = setUniversalParamValueInternal(NULL, NATDETECTED, 0, nat_stat);
			if (result != OK)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_WARN, DBG_STUN, "stunHandler->setUniversalParamValueInternal(%s) has returned error = %d\n",NATDETECTED, result);
				)
			}
			else
			{
				prev_nat_stat = nat_stat;
				isInformNeeded = 1;
			}
		}


		/* If report info has changed, send update to ACS */
		if ((udp_conn[0] == '\0') || (strcmp(udp_conn,new_conn) != 0) )
		{
			DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_STUN, "STUN udp_conn %s != %s new_conn -> update parameters\n", udp_conn, new_conn);
			)

			if (isStunEnabled && nat_stat)
			{
				// San, Sep 2011:
				setCancelStunThreadActivated(0);
				pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
				result = bindingChange(stun_host,stun_port,NULL,NULL);
				pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
				pthread_testcancel(); // If PTHREAD_CANCEL_ASYNCHRONOUS is used this line is not mandatory.
				if (result == OK)
				{
					DEBUG_OUTPUT (
							dbglog (SVR_DEBUG, DBG_STUN,
									"stunHandler()->bindingChange(): has returned with status %d. STUN server at %s:%d.\n",
									result, stun_host, stun_port);
					)
				}
				else
				{
					DEBUG_OUTPUT (
							dbglog (SVR_WARN, DBG_STUN,
									"stunHandler()->bindingChange(): has returned with status %d.\n", result);
					)
				}
			}
			result = setUniversalParamValueInternal(NULL, UDPCONNECTIONREQUESTADDRESS, 0, new_conn);
			if (result != OK)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_WARN, DBG_STUN, "stunHandler->setUniversalParamValueInternal(%s) has returned error = %d\n",
								UDPCONNECTIONREQUESTADDRESS, result);
				)
			}
			else
				isInformNeeded = 1;
		} // end of    if ((udp_conn[0] == '\0') || (strcmp(udp_conn,new_conn) != 0) )
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_STUN, "STUN udp_conn %s == %s new_conn -> no update parameters\n", udp_conn, new_conn);
			)
		}

		usleep(1000); // 1 ms
		if (isInformNeeded)
		{
			pthread_mutex_lock(&informLock); // Waiting, until inform completed
			addEventCodeSingle ( EV_VALUE_CHANGE );
			setAsyncInform(true);
			pthread_mutex_unlock(&informLock);
			isInformNeeded = 0;
		}

		if ( periodicIntervalDelay >= ( tmpTime = (time(NULL)-startTime) ) )
			periodicIntervalDelay -= tmpTime;
		else
			periodicIntervalDelay = 0;
		DEBUG_OUTPUT (
				dbglog (SVR_DEBUG, DBG_STUN, "stunHandler() thread will sleep %d sec\n", periodicIntervalDelay);
		)
		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
		sleep(periodicIntervalDelay);
	} // for (;;)
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	pthread_exit(NULL);
	pthread_cleanup_pop(1);
	return NULL;
}


/* Created by San, 5 September 2011.
 * Function returns STUNServerAddress and STUNServerPort.
 * addr - pointer to STUNServerAddress char array.
 * addrSize - size of STUNServerAddress char array.
 * port - pointer to STUNServerPort
 * */
static int getSTUNServerAddressPort(char *addr, unsigned int addrSize, unsigned int *port)
{
	char * pstun_host = NULL;
	unsigned int  *stun_port = NULL;
	int ret;

	if (!addr)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_STUN, "getSTUNServerAddressPort(): function parameter 'addr' can't be NULL.\n");
		)
		return -1;
	}
	if (!port)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_STUN, "getSTUNServerAddressPort(): function parameter 'port' can't be NULL.\n");
		)
		return -1;
	}

	memset(addr, 0, addrSize);

	ret = getParameter(STUNSERVERADDRESS, &pstun_host);
	if ( ret != OK || !pstun_host || strlen(pstun_host) == 0)
		strncpy(addr, getServerAddr(), addrSize-1);
	else
		strncpy(addr, pstun_host, addrSize-1);

	if (strlen(addr) == 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_STUN, "Determination of the STUNServerAddress is failed. Return -1.\n");
		)
		return -1;
	}

	ret = getParameter(STUNSERVERPORT, &stun_port);
	if ( ret != OK || !stun_port)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_STUN, "STUN parameter fetch of %s failed. The default value %d will be returned.\n", STUNSERVERPORT, DEFAULT_STUN_SERVER_UDP_PORT);
		)
		*port = DEFAULT_STUN_SERVER_UDP_PORT;
	}
	else
	{
		*port = *stun_port;
	}

	return OK;
}


#endif /* WITH_STUN_CLIENT */
