/***************************************************************************
 *    Original code © Copyright (C) 2004-2012 by Dimark Software Inc.      *
 *    support@dimark.com                                                   *
 *    Modifications © 2013 Comcast Cable Communications, LLC               *
 ***************************************************************************/

#include "dimark_globals.h"

#include <sys/sysinfo.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <net/ethernet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <openssl/hmac.h>

#include "paramaccess.h"
#include "parameterStore.h"
#include "utils.h"
#include "parameter.h"
#include "STUN_dimark.h"
#include "eventcode.h"
#include "udpCRhandler.h"

#define		QVALUE_SIZE		64

extern char udp_host[257];

static volatile int udpSocket = 0;
volatile int udpSocketIsShutdown = 0;
pthread_mutex_t udpSockMutex = PTHREAD_MUTEX_INITIALIZER;

#ifdef HAVE_CONNECTION_REQUEST
extern pthread_mutex_t informLock;

static char previous_ts_str[32] = "";
static char previous_id_str[32] = "";
static volatile int  udpSockMutexIsLockedByUdpCRThread = 0;

static unsigned int validate_UDPConnectionRequest( char *, int );

void udpCRThreadExitCallback(void * arg)
{
	if (udpSockMutexIsLockedByUdpCRThread)
	{
		pthread_mutex_trylock(&udpSockMutex);	//if EBUSY then mutex is locked, else this function locks mutex.
		//It isn't necessary to use pthread_mutex_trylock(), but it saves us from the undefined behavior of pthread_mutex_unlock()

		shutdownUdpCRSocket();
		closeUDPSocket();

		pthread_mutex_unlock(&udpSockMutex); //unlock the mutex.
		udpSockMutexIsLockedByUdpCRThread = 0;
	}
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "udpCRThreadExitCallback() has been done. The allocated memory is freed.\n");
	)
}

/*
 * This thread handles the UDP Connection Request listener
 */
void *
udpCRhandler (void *param)
{
	int sock, bytesAmount, ret;
	char buf[2048];
	int mutexStat = OK;
	struct timeval timeout;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	udpHostInitialization();
	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
	sleep(10); // sleep 10 seconds

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_REQUEST, "UDP Connection Request listener started\n");
	)

	pthread_cleanup_push(udpCRThreadExitCallback, NULL);

	for (;;)
	{
		memset(buf, 0, sizeof(buf));
		pthread_mutex_lock(&udpSockMutex);
		udpSockMutexIsLockedByUdpCRThread = 1;
		sock = getUDPSocket();
		if (sock < 0)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_REQUEST, "udpCRhandler->getUDPSocket() has returned %d\n", sock);
			)
			pthread_mutex_unlock(&udpSockMutex);
			udpSockMutexIsLockedByUdpCRThread = 0;
			sleep(1); // delay before next trying.
			continue;
		}

		bytesAmount = udpSocketIsShutdown ? 0 : recvfrom(sock, buf, sizeof(buf), 0, NULL, NULL);

		if (bytesAmount < 0)
		{
			/*DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_REQUEST,"udpCRhandler->recvfrom() has returned %d, errno = %d (%s)\n", bytesAmount, errno, strerror(errno));
			)*/
			pthread_mutex_unlock(&udpSockMutex);
			udpSockMutexIsLockedByUdpCRThread = 0;
			usleep(300); // sleep 0.3 ms
			continue;
		}

		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST,"udpCRhandler->recvfrom() has returned %d bytes\n", bytesAmount);
		)

		if (bytesAmount==0)
		{
			if (udpSocketIsShutdown)
			{
				closeUDPSocket();
				pthread_mutex_unlock(&udpSockMutex);
				udpSockMutexIsLockedByUdpCRThread = 0;
				sleep(1); // sleep 1 sec
				continue;
			}
			pthread_mutex_unlock(&udpSockMutex);
			udpSockMutexIsLockedByUdpCRThread = 0;
			usleep(1000); // sleep 0.1 sec
			continue;
		}

		pthread_mutex_unlock(&udpSockMutex);
		udpSockMutexIsLockedByUdpCRThread = 0;

		ret = validate_UDPConnectionRequest(buf, bytesAmount);
		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
		if ( ret == 0 )
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_REQUEST, "UDP Connection Request validated (%d bytes)-> ACS Notification\n", bytesAmount);
			)
			mutexStat = pthread_mutex_trylock (&informLock);
			// reset nonce value to force a new Digest challenge next time
			if (mutexStat == OK)
			{
				addEventCodeSingle (EV_CONNECT_REQ);

				pthread_mutex_unlock (&informLock);
				setAsyncInform(true);
			}
			sleep(1); //Simply protection from DoS attacks (if was received and processed bytes, and they were validated Ok)
		}
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_REQUEST, "UDP Connection Request not validated (%d bytes), ret = %d\n", bytesAmount, ret);
			)
		}

	} // for (;;)
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	pthread_exit (NULL);
	pthread_cleanup_pop(1);
	return NULL;
}

/*
 * validate_UDPConnectionRequest() examines the packet data
 * contents for presence of the information required by TR-111
 * Specifically:
 *    HTTP GET Request for version HTTP 1.1
 *    Valid URI
 *    Empty PATH
 *    Query String with fields: ts, id, un, cn, and sig
 */
static unsigned int
validate_UDPConnectionRequest( char *data, int len)
{
	char *end_data, *to,*from;
	//int  test_len;
	int  count;
	char URI[128];
	char URI_host[64], URI_port[32];
	char ts[QVALUE_SIZE], id[QVALUE_SIZE], un[QVALUE_SIZE], cn[QVALUE_SIZE], sig[QVALUE_SIZE];
	int  ts_f, id_f, un_f, cn_f, sig_f;
	char sha_key[QVALUE_SIZE], sha_text[128], sha_md[64];
	int  sha_key_len, sha_text_len,  sha_md_len;
	HMAC_CTX ctx;
	int i;
	char cmp[64];
	unsigned long previous_ts, this_ts;
	char *tmp_username,   *tmp_password;
	char  connection_username[QVALUE_SIZE], connection_password[QVALUE_SIZE];

	end_data = data + len - 1;

	memset(connection_username, 0, QVALUE_SIZE);
	memset(connection_password, 0, QVALUE_SIZE);

	/* fetch connection request credentials, default to "dps" "dps" */
	if ( getConnectionUsername( &tmp_username ) != OK)
		strcpy(  connection_username, "dps" );
	else
		strcpy(  connection_username, tmp_username );

	if ( getConnectionPassword( &tmp_password ) != OK)
		strcpy(  connection_password, "dps" );
	else
		strcpy(  connection_password, tmp_password );

	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_REQUEST,
					"validate_UDPConnectionRequest() data_point = %p, len = %d, user = %s, pass= %s\n",
					data, len, connection_username, connection_password );
	)

	/* Check for "GET " at start */
	if ((strncmp(data, "GET ", 4) != 0))
		return(-1);
	data += 4;

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "Found \"GET \"\n");
	)

	/* Check for "http://" next  */
	if (strncasecmp( data, "http://", 7 ) != 0)
		return (-2);

	/* Copy URI up to '?' char, or end of URI or data buffer */
	to = URI;
	count = 1;
	while ( (*data != '?' ) && (count<sizeof(URI)) && (data < end_data)) {
		*to = *data;
		to++;
		data++;
		count++;
		if ( count >= sizeof(URI) )
			return(-3);
		if ( data >= end_data )
			return(-4);
	}

	*to = '\0';

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "URI = %s\n",URI);
	)

	/* Extract host portion from URI */
	strcpy(URI_host,"");
	to       = URI_host;
	from     = URI + 7;
	count    = 1;
	while ( (*from != ':') && (*from != '/') && (*from != '\0')) {
		*to = *from;
		to++;
		from++;
		count++;
		if ( count >= sizeof(URI_host) )
			return(-5);
	}
	*to = '\0';

	/* Extract port portion if present */
	strcpy(URI_port,"");
	if ( *from == ':' ) {
		to     = URI_port;
		from  += 1;
		count  = 1;
		while ( (*from != '/') && (*from != '\0')) {
			*to = *from;
			to++;
			from++;
			count++;
			if ( count >= sizeof(URI_port) )
				return(-6);
		}
		*to = '\0';
	}

	/* If URI contains a path component, reject as per TR111 */
	if (*from == '/')
		return(-7);

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "URI_host = %s\n", URI_host);
			dbglog (SVR_DEBUG, DBG_REQUEST, "URI_port = %s\n", URI_port);
	)

	/* Now need to parse out query fields */
	ts[0] = id[0] = un[0] = cn[0] = sig[0] = '\0';
	ts_f  = id_f  = un_f  = cn_f  = sig_f  = 1;

	while ( data < end_data ) {
		data++;
		if ( data >= end_data )
			return(-8);

		/* recognize next query name<>value pair? */
		/* If yes, set ptrs */
		/* Recognize with any order of pairs */
		if ( strncmp(data,"ts=",3)  == 0 ) {
			to    = ts;
			ts_f  = 0;
			data += 3;
		}
		if ( strncmp(data,"id=",3)  == 0 ) {
			to    = id;
			id_f  = 0;
			data += 3;
		}
		if ( strncmp(data,"un=",3)  == 0 ) {
			to    = un;
			un_f  = 0;
			data += 3;
		}
		if ( strncmp(data,"cn=",3)  == 0 ) {
			to    = cn;
			cn_f  = 0;
			data += 3;
		}
		if ( strncmp(data,"sig=",4) == 0 ) {
			to    = sig;
			sig_f = 0;
			data += 4;
		}

		/* Copy value */
		count = 1;
		while ((*data != '&' ) && (*data != ' ') && ( count<sizeof(ts) ) && ( data < end_data )) {
			*to = *data;
			to++;
			data++;
			count++;
			if ( count >= sizeof(ts) )
				return(-6);
			if ( data >= end_data )
				return(-9);
		}
		*to = '\0';

		/* got all values, even if null string? */
		if ( (ts_f == 0) && (id_f == 0) && (un_f == 0) && (cn_f == 0) && (sig_f == 0) )
			break;

	} // while ( data < end_data )

	if ( (ts_f != 0)  || (strlen(ts) == 0 ))  // missing or null ts name<>value
		return(-10);
	if ( (id_f != 0)  || (strlen(id) == 0 ))  // missing or null id name<>value
		return(-11);
	if ( (un_f != 0)  || (strlen(un) == 0 ))  // missing or null un name<>value
		return(-12);
	if ( (cn_f != 0)  || (strlen(cn) == 0 ))  // missing or null cn name<>value
		return(-13);
	if ( (sig_f != 0) || (strlen(sig) == 0 ))  // missing or null sig name<>value
		return(-14);

	/* Should have all required query name<>value pairs now */
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "ts = %s\n",ts);
			dbglog (SVR_DEBUG, DBG_REQUEST, "id = %s\n",id);
			dbglog (SVR_DEBUG, DBG_REQUEST, "un = %s\n",un);
			dbglog (SVR_DEBUG, DBG_REQUEST, "cn = %s\n",cn);
			dbglog (SVR_DEBUG, DBG_REQUEST, "sig = %s\n",sig);
	)

	/* Skip space terminating query string */
	if ( *data == ' ' )
		data++;

	/* Test if any chars left in receive buffer */
	if ( data >= end_data )
		return(-15);

	/* Check for HTTP Version */
	if (strncmp(data,"HTTP/1.1",8) != 0)
		return(-16);

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "Found \"HTTP/1.1\"\n");
	)

	// Check that un value  matches connection_username
	if ( strcmp( un, connection_username) != 0 )
		return(-17);

	strcpy( sha_key, connection_password);
	sha_key_len = strlen(sha_key);
	sprintf( sha_text, "%s%s%s%s",ts,id,un,cn);
	sha_text_len = strlen(sha_text);
	sha_md_len   = 0;

	HMAC_CTX_init( &ctx);
	HMAC_Init(   &ctx, sha_key,  sha_key_len, EVP_sha1());
	HMAC_Update( &ctx, (unsigned char *)sha_text, sha_text_len );
	HMAC_Final(  &ctx, (unsigned char *)sha_md, (unsigned int *)&sha_md_len );
	HMAC_CTX_cleanup( &ctx );

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "sha_key [%3d] = %s\n", sha_key_len, sha_key);
			dbglog (SVR_DEBUG, DBG_REQUEST, "sha_text[%3d] = %s\n", sha_text_len, sha_text);
	)

	to = cmp;

	for (i = 0; i < sha_md_len; i++) {

		sprintf(to,"%02x", sha_md[i] & 0xff);
		to += 2;
	}

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "sig [%3d] = %s\n", strlen(sig), sig);
			dbglog (SVR_DEBUG, DBG_REQUEST, "cmp [%3d] = %s\n", strlen(cmp), cmp);
	)

	i = strncasecmp( sig, cmp, strlen(sig));

	if ( i !=  0 ) {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST, "STUN UDP Connection Request failed authentication\n");
		)

		return(-18);
	}

	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_REQUEST, "STUN UDP Connection Request is Authentic\n");
	)

	/************************************************/
	/* UDP Connection Request Message Is AUTHENTIC! */
	/************************************************/

	/* Check that ts value is larger than previous_ts */
	/* If previous string is null, this is first time */
	if (strlen(previous_ts_str) != 0 ) {
		previous_ts = atol(previous_ts_str);
		this_ts     = atol(ts);
		if ( this_ts <= previous_ts ) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_REQUEST, "STUN Check timestamp failed\n");
			)

			return(-19);
		}
	}
	strcpy( previous_ts_str, ts );
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "STUN Check timestamp OK\n");
	)

	/* Check that id value different from previous */
	/* If previous string is null, this is first time */
	if ( strlen(previous_id_str) != 0 ) {
		if ( strcmp( id, previous_id_str) == 0 ) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_REQUEST, "STUN Check ID failed\n");
			)

			return(-20);
		}
	}

	strcpy( previous_id_str, id );
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "STUN Check ID OK\n");
	)

	return(0);
}
#endif

/*
 *	The function returns opened socket.
 *	If socket not be opened, it will be open.
 */
int getUDPSocket()
{
	struct sockaddr_in client_addr;
	int sock, ret;
	struct timeval timeout;
	int one;

	if (udpSocket > 0) // if already socket is opened, then return current socket
		return udpSocket;

	sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST,"getUDPSocket()->socket() has returned %d, errno = %d (%s)\n", sock, errno, strerror(errno));
		)
		return sock;
	}

	udpSocketIsShutdown = 0;
	timeout.tv_sec  = UDP_CR_RECV_TIMEOUT;
	timeout.tv_usec = 0;
	ret = setsockopt( sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
	if (ret < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST,"getUDPSocket()->setsockopt() has returned %d, errno = %d (%s)\n", ret, errno, strerror(errno));
		)
		shutdown(sock, SHUT_WR);
		close(sock);
		return ret;
	}

	one = 1;
	ret = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
	if (ret < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST,"getUDPSocket()->setsockopt(SO_REUSEADD) has returned %d, errno = %d (%s)\n", ret, errno, strerror(errno));
		)
		shutdown(sock, SHUT_WR);
		close(sock);
		return ret;
	}

	client_addr.sin_family = AF_INET;
	client_addr.sin_port = htons(ACS_UDP_NOTIFICATION_PORT);
	if (*udp_host)
	{
		client_addr.sin_addr.s_addr = inet_addr(udp_host);
		if (client_addr.sin_addr.s_addr == INADDR_NONE)
			client_addr.sin_addr.s_addr = INADDR_ANY;
	}
	else
		client_addr.sin_addr.s_addr = INADDR_ANY;

	memset(&(client_addr.sin_zero), 0, 8);
	ret = bind(sock, (struct sockaddr *) &client_addr, sizeof(client_addr));
	if (ret < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST,"getUDPSocket()->bind() has returned %d, errno = %d (%s)\n", ret, errno, strerror(errno));
		)
		shutdown(sock, SHUT_WR);
		close(sock);
		return ret;
	}

	udpSocket = sock;
	return udpSocket;
}

/*
 * Close opened socket.
 */
int closeUDPSocket()
{
	int ret = OK;
	if (udpSocket <= 0)
		return ret;
	ret = close(udpSocket);
	if (ret < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST,"closeUDPSocket()->close() has returned %d, errno = %d (%s)\n", ret, errno, strerror(errno));
		)
		return ret;
	}
	udpSocket = 0;
	return ret;
}

void shutdownUdpCRSocket()
{
	if (udpSocket>0)
	{
		errno = 0;
		shutdown(udpSocket,SHUT_RDWR);
		DEBUG_OUTPUT (
				dbglog ( SVR_DEBUG, DBG_REQUEST, "shutdownUdpCRSocket(): shutdown of 'udpSocket' is done.\n");
		)
		udpSocketIsShutdown = 1;
	}
}

int rebindUdpCRSocket(char *new_udp_host)
{
#ifdef WITH_STUN_CLIENT
	terminateStunHandler();
#endif
	if (new_udp_host && *new_udp_host)
	{
		strncpy(udp_host, new_udp_host, sizeof(udp_host)-1);
	}
	else
	{
		strcpy(udp_host, "0.0.0.0");
	}

	udpHostInitialization();
	shutdownUdpCRSocket();
	closeUDPSocket();
#ifdef WITH_STUN_CLIENT
	runStunHandler();
#endif
	return OK;
}
