/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef notification_handler_H
#define notification_handler_H

#include "dimark_globals.h"

#ifdef HAVE_NOTIFICATION

void *passiveNotificationHandler(void *);
void *activeNotificationHandler(void *);

time_t getLastInformTime();
void setLastInformTime(time_t);

#endif /* HAVE_NOTIFICATION */

#endif /* notification__H */
