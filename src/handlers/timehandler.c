/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"
#include "paramconvenient.h"
#include "timehandler.h"

#include <pthread.h>

extern pthread_mutex_t informLock;
extern pthread_cond_t condGo;
extern pthread_mutex_t mutexGo;

#ifdef HAVE_FILE
#include "filetransfer.h"
#include "du_transfer.h"
#include "si_transfer.h"

extern int  prevSoapErrorValue;

#ifdef HAVE_REQUEST_DOWNLOAD
#define	FIRST_TIME_REQUEST_DOWNLOAD		2592000 	//2592000 sec = 30 days
#define	NEXT_TIME_REQUEST_DOWNLOAD		2592000 	//2592000 sec = 30 days

bool isRequestDownload = true;
static bool RequestDownloadFlag = false;

static int setRequestDownload(void);

static int setRequestDownload(void)
{
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_REQUEST, "ACS Notification\n");
	)

	addEventCodeSingle(EV_REQUEST_DOWNLOAD);
	RequestDownloadFlag = true;

	return SOAP_OK;
}

int clearRequestDownload(struct soap *server)
{
	int ret = SOAP_OK;

	cwmp__ArgStruct cwmp__Arg_value[1] = { { ""/*"NameOne"*/, "" /*"ValueOne"*/} };
	cwmp__ArgStruct *cwmp__Arg_value_ptr = cwmp__Arg_value;
	struct ArrayOfArgs cwmp__FileTypeArg = { &cwmp__Arg_value_ptr, 0 };//{ &cwmp__Arg_value_ptr, 1 };
	struct cwmp__RequestDownloadResponse empty = { 0 };

	if (isRequestDownload && RequestDownloadFlag)
	{
		unsigned int redirectCount = MAX_REDIRECT_COUNT + 1; //+1 to sent first packet
		setAttrToHTTPandSOAPHeaders(server);
		while(redirectCount)
		{
			ret = soap_call_cwmp__RequestDownload(server, getServerURL(), "", FIRMWARE_UPGRADE_IMAGE, &cwmp__FileTypeArg, &empty);
			prevSoapErrorValue = server->error;
			if (ret != SOAP_OK)
			{
				switch (server->error)
				{
					case 301:
					case 302:
					case 307:
						set_server_url_when_redirect(server->endpoint);
						redirectCount--;
						continue;
						break;
					case 401:
					case 407:
						DEBUG_OUTPUT (
								dbglog (SVR_DEBUG, DBG_REQUEST, "clearRequestDownload()->soap_call_cwmp__RequestDownload(): HTTP Authentication required. soap->error=%d\n", server->error);
						)
						setAttrToHTTPandSOAPHeaders(server);
						ret = soap_call_cwmp__RequestDownload(server, getServerURL(), "", FIRMWARE_UPGRADE_IMAGE, &cwmp__FileTypeArg, &empty);
						break;
					default:
						break;
				}
				break;
			}
			else
				break;
		}
		if (redirectCount==0 || ret != SOAP_OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_REQUEST, "clearRequestDownload()->soap_call_cwmp__RequestDownload(): has returned soap->error=%d\n", server->error);
			)
			return ret;
		}

		RequestDownloadFlag = false;
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_REQUEST, "Call RequestDownload ret: %d\n", ret);
		)
	}
	return ret;
}
#endif /* HAVE_REQUEST_DOWNLOAD */


#if defined(HAVE_DEPLOYMENT_UNIT) && defined(HAVE_FILE_DOWNLOAD)
/* This function creates a timer with a one second delay.
 * After every Delay the handleDeploymentUnitTransfers() is called.
 * If the function returns a value > 0
 * the "11 DU STATE CHANGE COMPLETE" or "12 AUTONOMOUS DU STATE CHANGE COMPLETE" event is set,
 * communication with the ACS is started.
 */
void * deploymentUnitTimeHandler(void *param)
{
	int mutexStat;
	unsigned int period = 1000000; //1000000 microseconds = 1 sec.

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	while (true)
	{
		usleep(period);
		/* if informLock is BUSY then the CPE has already a connection to the ACS and we don't want to disturb it. */
		if(isDeploymentUnitTransfer() && pthread_mutex_trylock(&informLock) != EBUSY)
		{
			pthread_mutex_unlock(&informLock);
			if (handleDeploymentUnitTransfers() > 0)
			{
				/* check if the communication with ACS is already online */
				mutexStat = pthread_mutex_trylock(&informLock);
				if (mutexStat == OK)
				{
					int isNeedInform = 0;
					handleDeploymentUnitTransfersEvents(&isNeedInform);

					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_DU_TRANSFER, "Deployment unit done. Inform ACS\n");
					)

					pthread_mutex_unlock(&informLock);
					if (isNeedInform == 1)
					{
						setAsyncInform(true);
					}
				}
			}
		}
	}
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	pthread_exit(NULL);
	return NULL;
}
#endif /* HAVE_DEPLOYMENT_UNIT && HAVE_FILE_DOWNLOAD */


#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
/* This function creates a timer with a one second delay.
 * After every Delay the handleDelayedFiletransfers() is called.
 * If the function returns a value > 0
 * the "7 TRANSFER COMPLETE"
 * communication with the ACS is started.
 */
void * transferTimerHandler(void *param)
{
	int mutexStat;
	unsigned int period = 1000000; //1000000 microseconds = 1 sec.

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	checkTransfersAfterReboot();

	while (true)
	{
		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
		usleep(period);

		/* if informLock is BUSY then the CPE has already a connection to the ACS and we don't want to disturb it. */
		if(isDelayedFiletransfer() && pthread_mutex_trylock(&informLock) != EBUSY)
		{
			pthread_mutex_unlock(&informLock);

			if (handleDelayedFiletransfers(period) > 0)
			{
				/* check if the communication with ACS is already online */
				mutexStat = pthread_mutex_trylock(&informLock);
				if (mutexStat == OK)
				{
					int isNeedInform = 0;
					handleDelayedFiletransfersEvents(&isNeedInform);

					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_TRANSFER, "Transfer done. Inform ACS\n");
					)

					pthread_mutex_unlock(&informLock);
					if (isNeedInform == 1)
					{
						setAsyncInform(true);
					}
				}
			}
		}
	}
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	pthread_exit(NULL);
	return NULL;
}
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */

#endif /* HAVE_FILE */


/* This function creates a timer with a one second delay.
 * After every Delay the handleDeploymentUnitTransfers() is called.
 * If the function returns a value > 0
 * or  "11 DU STATE CHANGE COMPLETE"
 * or  "12 AUTONOMOUS DU STATE CHANGE COMPLETE" event is set,
 * communication with the ACS is started.
 * This function handlers "3 SCHEDULED" and "9 REQUEST DOWNLOAD" events too. */
void * timeHandler(void *param)
{
	int mutexStat;
	time_t ActRequestDownloadTime;
	time_t StartRequestDownloadTime;
	unsigned int period = 50000; //50000 microseconds = 0,05 sec.
	unsigned int requestDownloadCoefficient = 10;



	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

#ifdef HAVE_REQUEST_DOWNLOAD
	StartRequestDownloadTime = time(NULL) + FIRST_TIME_REQUEST_DOWNLOAD;
#endif /* HAVE_REQUEST_DOWNLOAD */

	while (true)
	{

		usleep(period);

		if(getAsyncInform() && pthread_mutex_trylock(&informLock) == OK)
		{
			pthread_mutex_unlock(&informLock);
			pthread_mutex_lock(&mutexGo);
			//setAsyncInform(false); //flag will be cleared at the Inform sending start.
			pthread_cond_signal(&condGo);
			pthread_mutex_unlock(&mutexGo);
		}

#ifdef HAVE_REQUEST_DOWNLOAD
		if ( !(--requestDownloadCoefficient) )
		{
			// period interval = 1 sec.
			requestDownloadCoefficient = 20;
			/* if informLock is BUSY then the CPE has already a connection to the ACS and we don't want to disturb it. */
			if (isRequestDownload && (pthread_mutex_trylock(&informLock) == OK) )
			{
				ActRequestDownloadTime = time(NULL);
				if (StartRequestDownloadTime <= ActRequestDownloadTime)
				{
					StartRequestDownloadTime = ActRequestDownloadTime + NEXT_TIME_REQUEST_DOWNLOAD;
					/* check if the communication with ACS is already online */
					setRequestDownload();
					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_REQUEST, "RequestDownload done. Inform ACS\n");
					)
					pthread_mutex_unlock(&informLock);
					setAsyncInform(true);
				}
				else
					pthread_mutex_unlock(&informLock);
			}
			efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
		}
#endif /* HAVE_REQUEST_DOWNLOAD */
	}
	efreeAllTypedMemForCurrentThread(0);
	return NULL;
}


#if defined(HAVE_SCHEDULE_INFORM)
/* This function creates a timer with a one second delay.
 * After every Delay the handleDeploymentUnitTransfers() is called.
 * If the function returns a value > 0
 * the "3 SCHEDULED" event is set,
 * communication with the ACS is started.
 */
void * scheduleInformTimeHandler(void *param)
{
	int mutexStat;
	unsigned int period = 1000000; //1000000 microseconds = 1 sec.

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	sleep(2); //some pause to not start SI analysis before 1st Inform is not sent.

	while (true)
	{
		usleep(period);
		/* if informLock is BUSY then the CPE has already a connection to the ACS and we don't want to disturb it. */
		if (isDelayedScheduleInform() && pthread_mutex_trylock(&informLock) != EBUSY)
		{
			pthread_mutex_unlock(&informLock);
			if (handleDelayedScheduleInform() > 0)
			{
				/* check if the communication with ACS is already online */
				mutexStat = pthread_mutex_trylock(&informLock);
				if (mutexStat == OK)
				{
					addEventCodeSingle(EV_SCHEDULED);
					handleDelayedScheduleInformEvents();

					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_SCHEDULE, "Schedule Inform done. Inform ACS\n");
					)

					pthread_mutex_unlock(&informLock);
					setAsyncInform(true);
				}
			}
		}
	}
	return NULL;
}
#endif /* HAVE_SCHEDULE_INFORM */
