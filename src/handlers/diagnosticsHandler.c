/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * diagnosticsHandler.c
 *
 *  Created on: Oct 24, 2011
 *      Author: san
 */

#include "dimark_globals.h"

#ifdef HAVE_DIAGNOSTICS

#include <pthread.h>

#include "list.h"
#include "diagnosticsHandler.h"
#include "serverdata.h"
#include "diagParameter.h"

pthread_mutex_t diagnosticsListLock = PTHREAD_MUTEX_INITIALIZER;
extern pthread_mutex_t informLock;

List DiagnosticsList;
DiagnosticTypes currentDiagType; // Type of current executed diagnostic
pthread_t currentDiagnisticThreadId;

char cwmpID_of_lastAddedDiagnostic[CURRENT_ID_SIZE+1] = {"\0"};

static void setCurrentDiagType( DiagnosticTypes );
static char * getDiagnosticName( DiagnosticTypes );
static void diagnosticsThreadExitCallback(void * );
static void setCurrentDiagType(DiagnosticTypes );
static int currentDiagnisticThreadCancel();
static int cmpPoint(void * , void * );

static void diagnosticsThreadExitCallback(void * arg)
{
	/*freeing of memory of this thread is not needed in this place because diagnosticsThread will be terminated only when Dimclient process
	 * is terminated. In this case the memory freeing will be executed for all memory.
	 */
	//efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);

	currentDiagnisticThreadCancel();
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "diagnosticsThreadExitCallback() has been done.\n");
	)
}

void * diagnosticsThread(void * param)
{
	DiagnosticTypes diagType = zero;
	GetDiagnosticInfo getDiagnosticInfo = NULL;
	DiagnosticStartFunk diagnosticStartFunk = NULL;
	ListEntry *entry = NULL;
	int status = 0;
	int * pstatus = NULL;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	initList(&DiagnosticsList);

	sleep(10); // sleep 10 seconds
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DIAGNOSTIC, "diagnosticsThread(): Diagnostics Thread is started\n");
	)

	setCurrentDiagType(zero);
	currentDiagnisticThreadId = 0;

	while (1)
	{
		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
		while (1)
		{
			pthread_mutex_lock(&informLock);
			//Waiting, until inform completed.
			//Also we can use the findEventCode() function under 'informLock' locked mutex.
			int isEventFound = findEventCode(EV_DIAG_COMPLETE);
			pthread_mutex_unlock(&informLock);

			// If EV_DIAG_COMPLETE isn't found then DiagnosticComplete has been sent. And we can run next diagnostic.
			if ( !isEventFound )
				break;

			usleep(500000);
		}

		pthread_mutex_lock(&diagnosticsListLock);
		if ( getListSize(&DiagnosticsList) > 0)
		{
			entry = getFirstEntry( &DiagnosticsList );
			// remove 'entry' from list because it will be handled at the next moment. Will be returned entry->data:
			getDiagnosticInfo = (GetDiagnosticInfo) removeEntryWithoutCmp( &DiagnosticsList, entry ); // Point to GetDiagnosticInfo function
		}
		pthread_mutex_unlock(&diagnosticsListLock);

		if (getDiagnosticInfo)
		{
			// get diagnosticStartFunk for begin of new thread, and DiagnosticTypes
			diagType = getDiagnosticInfo(&diagnosticStartFunk);
			setCurrentDiagType(diagType);

			pthread_cleanup_push(diagnosticsThreadExitCallback, NULL);

			// Start the new threads with diagnostic
			pthread_create (&currentDiagnisticThreadId, NULL, diagnosticStartFunk, (void *) &status);
			// wait the finish of thread. Finish may be if:
			// 1). If the thread is forced to shut down (for example: CancelTransfer or some parameter was changed);
			// 2). If the thread is stopped itself (successful finish).
			pthread_join(currentDiagnisticThreadId, (void**) &pstatus);
			currentDiagnisticThreadId = 0;
			setCurrentDiagType(zero);
			entry = NULL;
			getDiagnosticInfo = NULL;
			diagnosticStartFunk = NULL;

			pthread_cleanup_pop(0);


			if ((void *)pstatus == (void*)-1)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_INFO, DBG_DIAGNOSTIC, "diagnosticsThread(): \"%s\" diagnostic was aborted. Returned status = -1\n", getDiagnosticName(diagType) );
				)
			}
			else
			{
				if (pstatus)
				{
					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_DIAGNOSTIC, "diagnosticsThread(): \"%s\" diagnostic was finished with status = %d\n", getDiagnosticName(diagType), *pstatus);
					)
				}
				else
				{
					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_DIAGNOSTIC, "diagnosticsThread(): \"%s\" diagnostic was finished with status = NULL\n", getDiagnosticName(diagType));
					)
				}
				pthread_mutex_lock(&informLock); // Waiting, until inform completed
				addEventCodeSingle ( EV_DIAG_COMPLETE );
				pthread_mutex_unlock(&informLock);
				setAsyncInform(true);
			}
			diagType = zero;
			status = 0;
			pstatus = NULL;
		}
		else
		{
			usleep(500000); // sleep 0.5 seconds
		}
	}
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	pthread_exit(NULL);
	return NULL;
}
/*
static DiagnosticTypes getCurrentDiagType()
{
	return currentDiagType;
}*/

static void setCurrentDiagType(DiagnosticTypes type)
{
	currentDiagType = type;
}

static int currentDiagnisticThreadCancel()
{
	setCurrentDiagType(zero);
	if (currentDiagnisticThreadId>0)
		return pthread_cancel(currentDiagnisticThreadId);
	else
		return 0;
}

static int cmpPoint(void * p1, void * p2)
{
	return (p1 == p2) ? 0 : 1;
}

/* Add diagnostic task to diagnosticsList.
 * If this diagnostic task already started, that it will be aborted before adding to diagnosticsList.
 */
int addDiagnosticToDiagnosticsList(DiagnosticTypes diagType, GetDiagnosticInfo getDiagInfoFunc)
{
	int ret = OK;
	if ( currentDiagType == diagType)
	{
		ret = currentDiagnisticThreadCancel();
		if (ret == 0)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DIAGNOSTIC, "addDiagnosticToDiagnosticsList(): \"%s\" diagnostic thread was aborted successful\n", getDiagnosticName(diagType));
			)
		}
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DIAGNOSTIC, "addDiagnosticToDiagnosticsList(): \"%s\" diagnostic thread was aborted with error, ret = %d\n", getDiagnosticName(diagType), ret);
			)
		}
	}

	pthread_mutex_lock(&diagnosticsListLock);
	//if not found current diagnostic task in DiagnosticsList then add diagnostic task to DiagnosticsList
	if (!findEntry(&DiagnosticsList, (void *) getDiagInfoFunc, cmpPoint))
	{
		ret = addEntry(&DiagnosticsList, (void *) getDiagInfoFunc, MEM_TYPE_DIAGNOSTIC_LIST);
		if (ret == LIST_OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DIAGNOSTIC, "addDiagnosticToDiagnosticsList(): addEntry() was finished successful\n");
			)
			memset(cwmpID_of_lastAddedDiagnostic, 0, sizeof(cwmpID_of_lastAddedDiagnostic));
			strcpy(cwmpID_of_lastAddedDiagnostic, getSdCurrentId());
		}
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "addDiagnosticToDiagnosticsList(): addEntry() has returned error = %d\n", ret);
			)
		}
	}
	pthread_mutex_unlock(&diagnosticsListLock);
	return ret;
}

/* If diagnostic task already started, that it will be aborted,
 * else it will be deleted from DiagnosticsList.
 */
int diagnosticTaskCancel(DiagnosticTypes diagType, char * nameOfDiagnosticStateParam)
{
	int ret = OK;
	DiagnosticTypes diagTypetmp = zero;
	GetDiagnosticInfo getDiagnosticInfo = NULL;
	ListEntry *entry = NULL;
	ParameterValue value_None;

	// If setter of parameter be on the same cwmp__SetParameterValues() and have same cwmp_ID as setter of DiagnosticState parameter
	// then return
	if (strcmp(cwmpID_of_lastAddedDiagnostic, getSdCurrentId()) == 0)
		return ret;

	value_None.in_cval = DiagnosticsState_None;
	ret = updateParamValue(nameOfDiagnosticStateParam, StringType, &value_None);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "diagnosticTaskCancel(): updateParamValue(%s) error\n", nameOfDiagnosticStateParam);
		)
		return ret;
	}

	if ( currentDiagType == diagType)
	{
		ret = currentDiagnisticThreadCancel();
		if (ret == 0)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DIAGNOSTIC, "diagnosticTaskCancel(): \"%s\" diagnostic thread was aborted successful\n", getDiagnosticName(diagType));
			)
		}
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DIAGNOSTIC, "diagnosticTaskCancel(): \"%s\" diagnostic thread was aborted with error, ret = %d\n", getDiagnosticName(diagType), ret);
			)
		}
	}
	else
	{
		pthread_mutex_lock(&diagnosticsListLock);
		if ( getListSize(&DiagnosticsList) > 0)
		{
			entry = getFirstEntry( &DiagnosticsList );
			while(entry)
			{
				getDiagnosticInfo = (GetDiagnosticInfo) entry->data; // Point to GetDiagnosticInfo function
				if (getDiagnosticInfo)
				{
					diagTypetmp = getDiagnosticInfo(NULL);
					if (diagTypetmp == diagType)
					{
						removeEntryWithoutCmp( &DiagnosticsList, entry );
						DEBUG_OUTPUT (
								dbglog (SVR_INFO, DBG_DIAGNOSTIC, "diagnosticTaskCancel(): \"%s\" diagnostic task was deleted from DiagnosticsList\n", getDiagnosticName(diagType));
						)
						break;
					}
				}
				entry = iterateList(&DiagnosticsList, entry);
			} // while(entry)
		}
		pthread_mutex_unlock(&diagnosticsListLock);
	}
	return ret;
}

static char * getDiagnosticName(DiagnosticTypes diagType)
{
	char * diagName;
	switch ( diagType )
	{
#ifdef HAVE_IP_PING_DIAGNOSTICS
		case ipPingDiagnostic:
			diagName = "IP Ping";
			break;
#endif
#ifdef HAVE_TRACE_ROUTE_DIAGNOSTICS
		case traceRouteDiagnostic:
			diagName = "Trace Route";
			break;
#endif
#ifdef HAVE_WANDSL_DIAGNOSTICS
		case WANDSLDiagnostic:
			diagName = "WAN_DSL";
			break;
#endif
#ifdef HAVE_ATMF5_DIAGNOSTICS
		case ATMF5Diagnostic:
			diagName = "ATMF5";
			break;
#endif
#ifdef HAVE_DOWNLOAD_DIAGNOSTICS
		case downloadDiagnostic:
			diagName = "Download";
			break;
#endif
#ifdef HAVE_UPLOAD_DIAGNOSTICS
		case uploadDiagnostic:
			diagName = "Upload";
			break;
#endif
		default:
			diagName = "Unknown";
			break;

	}
	return diagName;
}

#endif /* HAVE_DIAGNOSTICS */
