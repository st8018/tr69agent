/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef udpCRhandler_H
#define udpCRhandler_H

#include "dimark_globals.h"

#define UDP_CR_RECV_TIMEOUT 3 // timeout for recvfrom() function in the UDP Connection Request handler.
							  // It isn't recommended to use value > 5 sec or value < 1 sec.

#ifdef HAVE_CONNECTION_REQUEST
void * udpCRhandler (void * );
#endif

int getUDPSocket();
int closeUDPSocket();
void shutdownUdpCRSocket();
int rebindUdpCRSocket(char *);

#endif /* udpCRhandler_H */
