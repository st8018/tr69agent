/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * diagnosticsHandler.h
 *
 *  Created on: Oct 24, 2011
 *      Author: san
 */

#ifndef _diagnosticsHandler_h
#define _diagnosticsHandler_h 1

#include "dimark_globals.h"

#ifdef HAVE_DIAGNOSTICS

/* Returns DiagnosticType.
 * If point != NULL, then returns point to diagFunc too (in parameter void **pFunc)
 */
typedef enum diagTypes
{
	zero = 0,
#ifdef HAVE_IP_PING_DIAGNOSTICS
	ipPingDiagnostic,
#endif
#ifdef HAVE_TRACE_ROUTE_DIAGNOSTICS
	traceRouteDiagnostic,
#endif
#ifdef HAVE_WANDSL_DIAGNOSTICS
	WANDSLDiagnostic,
#endif
#ifdef HAVE_ATMF5_DIAGNOSTICS
	ATMF5Diagnostic,
#endif
#ifdef HAVE_DOWNLOAD_DIAGNOSTICS
	downloadDiagnostic,
#endif
#ifdef HAVE_UPLOAD_DIAGNOSTICS
	uploadDiagnostic,
#endif
} DiagnosticTypes;

typedef void * (*DiagnosticStartFunk) (void * );
typedef DiagnosticTypes (*GetDiagnosticInfo) (DiagnosticStartFunk* pDiagMainFunc);

void * diagnosticsThread(void * );
int addDiagnosticToDiagnosticsList(DiagnosticTypes , GetDiagnosticInfo );
int diagnosticTaskCancel(DiagnosticTypes, char * );

#endif /* HAVE_DIAGNOSTICS */

#endif /* _diagnosticsHandler_h */
