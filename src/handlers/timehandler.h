/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef timehandler_H
#define timehandler_H

#include "dimark_globals.h"

#ifdef HAVE_FILE

#ifdef HAVE_REQUEST_DOWNLOAD
extern bool isRequestDownload;
int clearRequestDownload(struct soap *);
#endif /* HAVE_REQUEST_DOWNLOAD */

#if defined(HAVE_DEPLOYMENT_UNIT) && defined(HAVE_FILE_DOWNLOAD)
void * deploymentUnitTimeHandler(void *);
#endif /* HAVE_DEPLOYMENT_UNIT && HAVE_FILE_DOWNLOAD */

#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
void *transferTimerHandler(void *);
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */

#endif /* HAVE_FILE */

void *timeHandler(void *);

#if defined(HAVE_SCHEDULE_INFORM)
void * scheduleInformTimeHandler(void *);
#endif /* HAVE_SCHEDULE_INFORM */

#endif /* timehandler_H */
