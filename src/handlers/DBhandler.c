/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * DBhandler.c
 *
 *  Created on: Sep 12, 2011
 *      Author: san
 */
#ifdef WITH_USE_SQLITE_DB

#include <sqlite3.h>
#include <pthread.h>

#include "DBhandler.h"

extern char DB_file_path[];

struct tSQLiteDBState SQLiteDBState = {NULL, DBClose};
pthread_mutex_t SQLiteDBLock = PTHREAD_MUTEX_INITIALIZER;

/*
 * Write to screen log information about error.
 * */
static void
databaseError(sqlite3* db)
{
	int errcode = sqlite3_errcode(db);
	const char *errmsg = sqlite3_errmsg(db);
	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_DB, "Database error %d: %s\n", errcode, errmsg);
	)

	fprintf(stderr, "Database error %d: %s\n", errcode, errmsg);
}

sqlite3* getDBConnection()
{
	pthread_mutex_lock(&SQLiteDBLock);
	if (SQLiteDBState.DBstate == DBOpen)
	{
		SQLiteDBState.DBstate = DBBusy;
		pthread_mutex_unlock(&SQLiteDBLock);
		return SQLiteDBState.db;
	}
	pthread_mutex_unlock(&SQLiteDBLock);

	while (SQLiteDBState.DBstate == DBBusy)
	{
		usleep(1000);
	}

	pthread_mutex_lock(&SQLiteDBLock);
	if ( SQLiteDBState.DBstate == DBClose )
	{
		// Open the database
		if( sqlite3_open(DB_file_path, &(SQLiteDBState.db)) != SQLITE_OK )
		{
			databaseError(SQLiteDBState.db);
			sqlite3_close(SQLiteDBState.db);
			pthread_mutex_unlock(&SQLiteDBLock);
			return NULL;
		}
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DB, "getDBConnection() has returned SQLITE_OK\n");
		)
		SQLiteDBState.DBstate = DBBusy;
	}
	pthread_mutex_unlock(&SQLiteDBLock);
	return SQLiteDBState.db;
}

int releaseDBConnection()
{
	if (SQLiteDBState.DBstate == DBBusy)
		SQLiteDBState.DBstate = DBOpen;
	return OK;
}

int closeDBConnection(int exitIfDBBusy)
{
	int ret;
	if (exitIfDBBusy && SQLiteDBState.DBstate == DBBusy)
		return OK;
	while (SQLiteDBState.DBstate == DBBusy)
	{
		usleep(1000);
	}
	pthread_mutex_lock(&SQLiteDBLock);
	// Close the database
	ret = sqlite3_close(SQLiteDBState.db);
	if( ret != SQLITE_OK )
	{
		databaseError(SQLiteDBState.db);
		pthread_mutex_unlock(&SQLiteDBLock);
		return ret;
	}
	SQLiteDBState.db = NULL;
	SQLiteDBState.DBstate = DBClose;
	pthread_mutex_unlock(&SQLiteDBLock);
	return OK;
}

/*
 * Create the Params table in database db.
 * Return an SQLite error code.
 */
int
createDBAndTables()
{
	int ret = OK;
	sqlite3 *db;
	const char *zSql = "CREATE TABLE params(param_id INTEGER PRIMARY KEY, name TEXT, value BLOB, type_id INTEGER, notification INTEGER, accessList TEXT);\n"
			"CREATE TABLE metadatas(meta_id INTEGER PRIMARY KEY, name TEXT, data BLOB);";

	db = getDBConnection();
	if( !db )
	{
		return -1;
	}

	// Create the table
	ret = sqlite3_exec(db, zSql, 0, 0, 0);
	if( ret != SQLITE_OK )
	{
		databaseError(db);
		releaseDBConnection();
		return ret;
	}

	releaseDBConnection();

	return OK;
}

// This function insert parameter to the DB.
// It must be called when parameter is creating, only one time for each parameter.
// Actually, this function is already called in the all needed places.
// When you need to update param value use updateParamValue() function.
int storeParamValue(const char *name, ParameterType type , ParameterValue *value)
{
	int ret = OK;
	sqlite3 *db;
	char *zSql;
	unsigned int param_id = 0;
	unsigned int param_notification = 0;
	char ** accessList;
	unsigned int accessListSize;
	char * accessListString;
	char *pzSql, *cVal;
	unsigned int bufSize;

	if(getParamIdNotificationAccessList(name, &param_id, &param_notification, &accessList, &accessListSize) !=  OK)
		return ERR_INVALID_PARAMETER_NAME;

	bufSize = 220; // 220 for SQL command size

	if (value)
	{
		switch (type)
		{
			case DefStringType:
			case StringType:
			case DefBase64Type:
			case Base64Type:
				bufSize += strlen(value->out_cval) + 1;
				break;
			case DefHexBinaryType:
			case hexBinaryType:
				bufSize += strlen(value->out_hexBin) + 1;
				break;
			case DefDateTimeType:
			case DateTimeType:
				bufSize += 50;
				break;
			default:
				bufSize += 25;
				break;
		}
	}

	bufSize += name ? strlen(name) : 10; // 10 - for "(null)"

	accessListString = accessListToString(accessList, accessListSize);
	bufSize += accessListString ? strlen(accessListString) : 10;

	// Memory allocation:
	zSql = (char *) emallocTypedMem(bufSize, MEM_TYPE_SHORT, 0);
	if (!zSql)
		return ERR_WRITE_PARAMFILE;

	sprintf(zSql, "INSERT INTO params(param_id, name, value, type_id, notification, accessList) VALUES(%d, '%s'", param_id, name);

	if(!value)
		sprintf(zSql + strlen(zSql), ", %s", NULL);
	else
	{
		switch (type)
		{
			case DefIntegerType:
			case IntegerType:
			case DefBooleanType:
			case BooleanType:
				sprintf(zSql + strlen(zSql), ", %d", value->out_int);
				break;
			case DefUnsignedIntType:
			case UnsignedIntType:
				sprintf(zSql + strlen(zSql), ", %u", value->out_uint);
				break;
			case DefStringType:
			case StringType:
			case DefBase64Type:
			case Base64Type:
				pzSql = zSql + strlen(zSql);
				*pzSql++ = ',';  // comma
				*pzSql++ = ' ';  // space
				*pzSql++ = '\''; // open Single-quota
				cVal = value->out_cval;
				while(pzSql && cVal && *cVal != '\0')
				{
					if (*cVal == '\'')
						*pzSql++ = '\'';
					*pzSql++ = *cVal++;
				}
				*pzSql++ = '\''; // close Single-quota
				*pzSql = '\0';
				break;
			case DefDateTimeType:
			case DateTimeType:
				sprintf(zSql + strlen(zSql), ", '%ld|%ld'", value->out_timet.tv_sec, value->out_timet.tv_usec);
				break;
			case DefUnsignedLongType:
			case UnsignedLongType:
				sprintf(zSql + strlen(zSql), ", '%lu'", value->out_ulong);
				break;
			case DefHexBinaryType:
			case hexBinaryType:
				sprintf(zSql + strlen(zSql), ", '%s'", value->out_hexBin);
				break;
			default:
				break;
		}
	}

	if (accessListString)
	{
		sprintf(zSql + strlen(zSql), ", %d, %d, '%s')", type, param_notification, accessListString);
	}
	else
		sprintf(zSql + strlen(zSql), ", %d, %d, (null))", type, param_notification);

	db = getDBConnection();
	if (!db)
	{
		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_SHORT, 0); // free mem which was allocated in the accessListToString() & with MEM_TYPE_SHORT
		return ERR_WRITE_PARAMFILE;
	}

	// Write to table
	ret = sqlite3_exec(db, zSql, 0, 0, 0);

	if( ret != SQLITE_OK )
	{
		databaseError(db);
		releaseDBConnection();
		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_SHORT, 0); // free mem which was allocated in the accessListToString() & with MEM_TYPE_SHORT
		return ERR_WRITE_PARAMFILE;
	}

	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_SHORT, 0); // free mem which was allocated in the accessListToString() & with MEM_TYPE_SHORT
	releaseDBConnection();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DB, "storeParamValue(): parameter %s was inserted to DB successfully\n", name);
	)

	return ret;
}

/* executeDBCommand(BEGIN)...executeDBCommand(END)  construction use under locked 'paramLock' mutex only.
  * */
int executeDBCommand(DBSimpleCommand command)
{
	int ret = OK;
	sqlite3 *db;
	db = getDBConnection();

	if (!db)
		return -1;

	switch (command)
	{
		case BEGIN_TRANSACTION:
			ret = sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, NULL);
			if (ret != SQLITE_OK )
			{
				databaseError(db);
			}
			break;
		case BEGIN:
			ret = sqlite3_exec(db, "BEGIN;", NULL, NULL, NULL);
			if (ret != SQLITE_OK )
			{
				databaseError(db);
			}
			break;
		case ROLLBACK:
			if (!sqlite3_get_autocommit(db)) //check, if transaction is activated
			{
				ret = sqlite3_exec(db, "ROLLBACK;", NULL, NULL, NULL);
				if (ret != SQLITE_OK )
				{
					databaseError(db);
				}
			}
			break;
		case COMMIT:
			if (!sqlite3_get_autocommit(db)) //check, if transaction is activated
			{
				ret = sqlite3_exec(db, "COMMIT;", NULL, NULL, NULL);
				if (ret != SQLITE_OK )
				{
					databaseError(db);
				}
			}
			break;
		case END:
			if (!sqlite3_get_autocommit(db)) //check, if transaction is activated
			{
				ret = sqlite3_exec(db, "END;", NULL, NULL, NULL);
				if (ret != SQLITE_OK )
				{
					databaseError(db);
				}
			}
			break;
		case SAVEPOINT:
			ret = sqlite3_exec(db, "SAVEPOINT savepoint1;", NULL, NULL, NULL);
			if (ret != SQLITE_OK )
			{
				databaseError(db);
			}
			break;
		case RELEASE_SAVEPOINT:
			ret = sqlite3_exec(db, "RELEASE SAVEPOINT savepoint1;", NULL, NULL, NULL);
			if (ret != SQLITE_OK )
			{
				databaseError(db);
			}
			break;
		case ROLLBACK_TO_SAVEPOINT:
			ret = sqlite3_exec(db, "ROLLBACK TRANSACTION TO SAVEPOINT savepoint1;", NULL, NULL, NULL);
			if (ret != SQLITE_OK )
			{
				databaseError(db);
			}
			break;
		default:
			break;
	}
	releaseDBConnection();
	return ret;
}

/* Store Parameter Attributes to SQLite BD.
 * if accessListSize < 0 then accessList is ignored. If (accessList is NULL or accessListSize==0) then set to table empty accessList value.
 * */
int storeParameterAttributes(int param_id, const char * paramName, int type, int param_notification, char ** accessList, int accessListSize)
{
	int ret = OK;
	sqlite3 *db;
	sqlite3_stmt *res;
	const char * tail;
	char zSql[1024] = {"\0"};
	char * accessListString = NULL;
	bool isParamExistsInParams = false; /* 0 - insert, 1 - update */
	int param_id_tmp;

	if ( (param_id <= 0) && (!paramName) )
		return ERR_WRITE_PARAMFILE;

	/* find param data in DB */
	if (param_id <= 0)
		sprintf(zSql, "select param_id from params where name='%s'",paramName);
	else
		sprintf(zSql, "select param_id from params where param_id=%d",param_id);

	db = getDBConnection();
	if (!db)
		return ERR_WRITE_PARAMFILE;

	ret = sqlite3_prepare_v2(db, zSql, 1024, &res, &tail);
	if (ret != SQLITE_OK)
	{
		databaseError(db);
		releaseDBConnection();
		return ERR_READ_PARAMFILE;
	}
	if(sqlite3_step(res) == SQLITE_ROW)
	{
		/* make update */
		if (param_id <= 0)
			param_id_tmp = sqlite3_column_int(res, 0); // read ID of first found parameter with name=paramName
		else
			param_id_tmp = param_id;
		isParamExistsInParams = true;
	}

	sqlite3_finalize(res);

	if(isParamExistsInParams == true)
	{
		releaseDBConnection();
		return updateParameterAttributes(param_id_tmp, paramName, type, param_notification, accessList, accessListSize);
	}
	else
	{
		if ( (param_notification < NotificationNone) || (param_notification > NotificationAllways) )
			param_notification = 0;

		if (param_id <= 0)
			sprintf(zSql, "INSERT INTO params(name, type_id, notification, accessList) VALUES('%s', %d, %d",
					paramName, type, param_notification);
		else
			sprintf(zSql, "INSERT INTO params(param_id, name, type_id, notification, accessList) VALUES(%d, '%s', %d, %d",
					param_id, paramName, type, param_notification);

		accessListString = accessListToString(accessList, accessListSize);
		if (accessListString)
		{
			sprintf(&zSql[strlen(zSql)], ", '%s')", accessListString);
			efreeTypedMemByTypeForCurrentThread(MEM_TYPE_SHORT, 0); // free mem which was allocated in the accessListToString()
		}
		else
			strcat(zSql, ", (null))");

		// Write to table
		ret = sqlite3_exec(db, zSql, 0, 0, 0);
		if( ret != SQLITE_OK )
		{
			databaseError(db);
			releaseDBConnection();
			return ERR_WRITE_PARAMFILE;
		}

		releaseDBConnection();
		if (paramName)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DB, "StoreParameterAttributes(): Attributes of parameter %s was stored to DB successfully\n", paramName);
				)
		}
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_DB, "StoreParameterAttributes(): Attributes of parameter id=%d was stored to DB successfully\n", param_id);
				)
		}
	}

	return OK;
}

int updateParamValue(const char *name, ParameterType type , ParameterValue *value)
{
	if (!value || !name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DB, "updateParamValue(): 'value' and 'name' can't be NULL.\n");
		)
		return ERR_INVALID_ARGUMENT;
	}
	int ret = OK;
	sqlite3 *db;
	char *zSql;
	unsigned int param_id = 0;
	char *pzSql, *cVal;
	unsigned int bufSize;

	if(getParamIdNotificationAccessList(name, &param_id, NULL, NULL, NULL) !=  OK)
		return ERR_INVALID_PARAMETER_NAME;

	bufSize = 80; // 80 for SQL command size

	if (value)
	{
		switch (type)
		{
			case DefStringType:
			case StringType:
			case DefBase64Type:
			case Base64Type:
				bufSize += strlen(value->out_cval) + 1;
				break;
			case DefHexBinaryType:
			case hexBinaryType:
				bufSize += strlen(value->out_hexBin) + 1;
				break;
			case DefDateTimeType:
			case DateTimeType:
				bufSize += 50;
				break;
			default:
				bufSize += 25;
				break;
		}
	}

	// Memory allocation:
	zSql = (char *) emallocTypedMem(bufSize, MEM_TYPE_SHORT, 0);
	if (!zSql)
		return ERR_WRITE_PARAMFILE;

	strcpy(zSql, "UPDATE params SET value=");

	switch (type)
	{
		case DefIntegerType:
		case IntegerType:
		case DefBooleanType:
		case BooleanType:
			sprintf(zSql+strlen(zSql), "%d", value->out_int);
			break;
		case DefUnsignedIntType:
		case UnsignedIntType:
			sprintf(zSql+strlen(zSql), "%u", value->out_uint);
			break;
		case DefStringType:
		case StringType:
		case DefBase64Type:
		case Base64Type:
			pzSql = zSql+strlen(zSql);
			*pzSql++ = '\''; // open Single-quota
			cVal = value->out_cval;
			while(pzSql && cVal && *cVal != '\0')
			{
				if (*cVal == '\'')
					*pzSql++ = '\'';
				*pzSql++ = *cVal++;
			}
			*pzSql++ = '\''; // close Single-quota
			*pzSql = '\0';
			break;
		case DefDateTimeType:
		case DateTimeType:
			sprintf(zSql+strlen(zSql), "'%ld|%ld'", value->out_timet.tv_sec, value->out_timet.tv_usec);
			break;
		case DefUnsignedLongType:
		case UnsignedLongType:
			sprintf(zSql+strlen(zSql), "'%lu'", value->out_ulong);
			break;
		case DefHexBinaryType:
		case hexBinaryType:
			sprintf(zSql+strlen(zSql), "'%s'", value->out_hexBin);
			break;
		default:
			break;
	}

	sprintf(zSql+strlen(zSql), " where param_id=%d", param_id);

	db = getDBConnection();
	if (!db)
	{
		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_SHORT, 0); // free mem which was allocated in the accessListToString() & with MEM_TYPE_SHORT
		return ERR_WRITE_PARAMFILE;
	}

	// Write to table
	ret = sqlite3_exec(db, zSql, 0, 0, 0);
	if( ret != SQLITE_OK )
	{
		databaseError(db);
		releaseDBConnection();
		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_SHORT, 0); // free mem which was allocated in the accessListToString() & with MEM_TYPE_SHORT
		return ERR_WRITE_PARAMFILE;
	}

	releaseDBConnection();
	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_SHORT, 0); // free mem which was allocated in the accessListToString() & with MEM_TYPE_SHORT

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DB, "updateParamValue(): parameter %s was updated in DB successfully\n", name);
	)

	return ret;

}

/* Update Parameter Attributes to SQLite BD.
 * To identify the parameter used param_id. If param_id <= 0, will be used the paramName. If param_id > 0  paramName is ignored.
 * if type < 0  type is ignored.
 * if param_notification < 0  param_notification is ignored.
 * if accessListSize < 0 then accessList is ignored. If (accessList is NULL or accessListSize==0) then set to table empty accessList value.
 * */
int updateParameterAttributes(int param_id, const char * paramName, int type, int param_notification, char ** accessList, int accessListSize)
{
	int ret = OK;
	sqlite3 *db;
	char zSql[1024] = {"\0"};
	char * accessListString;
	if ( (param_id <= 0) && (!paramName) )
		return OK;
	if ( ((param_notification < NotificationNone) || (param_notification > NotificationAllways)) && (accessListSize < 0) )
		return OK;

	strcpy(zSql, "UPDATE params SET ");

	if ( type >= 0 )
		sprintf(&zSql[strlen(zSql)], " type_id=%d", type);

	if ( (param_notification >= NotificationNone) && (param_notification <= NotificationAllways) )
		sprintf(&zSql[strlen(zSql)], "%s notification=%d%s ", (type >= 0)?",":"", param_notification, (accessListSize < 0)?"":",");

	if ( accessListSize >= 0 )
	{
		accessListString = accessListToString(accessList, accessListSize);

		if (accessListString)
		{
			sprintf(&zSql[strlen(zSql)], " accessList='%s' ", accessListString);
			efreeTypedMemByTypeForCurrentThread(MEM_TYPE_SHORT, 0); // free mem which was allocated in the accessListToString()
		}
		else
			strcat(zSql, " accessList=(null) ");

	}

	if (param_id <= 0)
		sprintf(&zSql[strlen(zSql)], " WHERE name='%s'", paramName);
	else
		sprintf(&zSql[strlen(zSql)], " WHERE param_id=%d", param_id);

	db = getDBConnection();
	if (!db)
		return ERR_WRITE_PARAMFILE;

	// Write to table
	ret = sqlite3_exec(db, zSql, 0, 0, 0);
	if( ret != SQLITE_OK )
	{
		databaseError(db);
		releaseDBConnection();
		return ERR_WRITE_PARAMFILE;
	}

	releaseDBConnection();

	if (paramName)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DB, "updateParameterAttributes(): Attributes of parameter %s was updated to DB successfully\n", paramName);
		)
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DB, "updateParameterAttributes(): Attributes of parameter id=%d was updated to DB successfully\n", param_id);
		)
	}

	return ret;
}

int retrieveParamValue(const char *name, ParameterType type, ParameterValue * value)
{
	int ret = OK;
	sqlite3 *db;
	char zSql[1024] = {"\0"};
	sqlite3_stmt *res;
	const char * tail;
	unsigned int param_id = 0;
	int rowCount = 0;

	if(getParamIdNotificationAccessList(name, &param_id, NULL, NULL, NULL) !=  OK)
		return ERR_INVALID_PARAMETER_NAME;

	sprintf(zSql, "select value from params where param_id=%d",param_id);
	//sprintf(zSql, "select value from params where name='%s'", name);

	db = getDBConnection();
	if (!db)
		return ERR_READ_PARAMFILE;

	ret = sqlite3_prepare_v2(db, zSql, 1000, &res, &tail);

	if (ret != SQLITE_OK)
	{
		databaseError(db);
		releaseDBConnection();
		return ERR_READ_PARAMFILE;
	}

	while (sqlite3_step(res) == SQLITE_ROW)
	{
		rowCount++;
		switch (type)
		{
			case DefIntegerType:
			case IntegerType:
			case DefBooleanType:
			case BooleanType:
				value->in_int = sqlite3_column_int(res, 0);
				break;
			case DefUnsignedIntType:
			case UnsignedIntType:
				value->in_uint = sqlite3_column_int(res, 0);
				break;
			case DefStringType:
			case StringType:
			case DefBase64Type:
			case Base64Type:
			{
				char *s = (char *)sqlite3_column_text(res, 0);
				int len = 0;
				if (s)
					len = strlen(s);
				if(len == 0)
				{
					value->in_cval = (char*)emallocTemp(1, 33);
					*(value->in_cval) = '\0';
				}
				else
				{
					value->in_cval = (char*)emallocTemp(len+1, 34);
					strncpy(value->in_cval, s, len);
					*(value->in_cval + len) = '\0';
				}
				break;
			}
			case DefDateTimeType:
			case DateTimeType:
			{
				char *s = (char *)sqlite3_column_text(res, 0);
				int len = 0;
				if (s)
					len = strlen(s);
				if(len == 0)
				{
					value->in_timet.tv_sec = 0;
					value->in_timet.tv_usec = 0;
				}
				else
				{
					sscanf(s, "%ld|%ld" , &value->in_timet.tv_sec, &value->in_timet.tv_usec);
				}
				break;
			}
			case DefUnsignedLongType:
			case UnsignedLongType:
			{
				char *s = (char *)sqlite3_column_text(res, 0);
				if(!s || strlen(s) == 0)
					value->in_ulong = 0;
				else
					if (sscanf(s, "%lu" , &value->in_ulong) != 1)
						value->in_ulong = 0;
				break;
			}
			case DefHexBinaryType:
			case hexBinaryType:
				{
					char *s = (char *)sqlite3_column_text(res, 0);
					int len = 0;
					if (s)
						len = strlen(s);
					if(len == 0)
					{
						value->in_hexBin = (char*)emallocTemp(1, 33);
						*(value->in_hexBin) = '\0';
					}
					else
					{
						value->in_hexBin = (char*)emallocTemp(len+1, 34);
						strncpy(value->in_hexBin, s, len);
						*(value->in_hexBin + len) = '\0';
					}
					break;
				}
			default:
				break;
		}
	}

	sqlite3_finalize(res);
	releaseDBConnection();

	if (rowCount == 0)
	{
		DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_DB, "retrieveParamValue(): row count in select result is equal 0. Parameter isn't found in DB. Return ERR_READ_PARAMFILE (9830).\n");
		)
		return ERR_READ_PARAMFILE;
	}
	return OK;
}

/* Retrieve Parameter Attributes from SQLite BD.
 * To identify the parameter used param_id. If param_id <= 0, will be used the paramName. If param_id > 0  paramName is ignored.
 * if type is NULL type not returns.
 * if param_notification is NULL param_notification not returns.
 * if accessListString is NULL then accessList not returns.
 * */
int retrieveParameterAttributes(int * param_id, const char * paramName, int * type, NotificationType * param_notification, char * accessListString)
{
	int ret = OK;
	sqlite3 *db;
	char zSql[1024] = {"\0"};
	sqlite3_stmt *res;
	const char * tail;

	if (param_id && *param_id > 0)
		sprintf(zSql, "select param_id, type_id, notification, accessList from params where param_id=%d", *param_id);
	else
		sprintf(zSql, "select param_id, type_id, notification, accessList from params where name='%s'", paramName);

	db = getDBConnection();
	if (!db)
		return ERR_READ_PARAMFILE;

	ret = sqlite3_prepare_v2(db, zSql, 1000, &res, &tail);
	if (ret != SQLITE_OK)
	{
		databaseError(db);
		releaseDBConnection();
		return ERR_READ_PARAMFILE;
	}
	releaseDBConnection();

	if (sqlite3_step(res) == SQLITE_ROW)
	{
		if (param_id && *param_id <= 0)
			*param_id = sqlite3_column_int(res, 0);
		if (type)
			*type = sqlite3_column_int(res, 1);
		if (param_notification)
			*param_notification = sqlite3_column_int(res, 2);
		if (accessListString)
			sprintf(accessListString, "%s", sqlite3_column_text(res, 3));
	}

	sqlite3_finalize(res);

	if (paramName)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DB, "retrieveParameterAttributes(): Attributes of parameter %s was retrieved from DB successfully\n", paramName);
		)
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_DB, "retrieveParameterAttributes(): Attributes of parameter id=%d was retrieved from DB successfully\n", *param_id);
		)
	}
	return ret;
}

int removeParameter(const char * name)
{
	int ret = OK;
	sqlite3 *db;
	char zSql[1024] = {"\0"};

	sprintf(zSql, "DELETE FROM params WHERE name='%s';\n"                 /* Delete from table params */
				  "DELETE FROM metadatas WHERE name='%s';",name, name); /* Delete from table metadatas */

	db = getDBConnection();
	if (!db)
		return ERR_WRITE_PARAMFILE;

	ret = sqlite3_exec(db, zSql, 0, 0, 0);
	if( ret != SQLITE_OK )
	{
		databaseError(db);
		releaseDBConnection();
		return ERR_WRITE_PARAMFILE;
	}

	releaseDBConnection();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DB, "removeParameter(): parameter %s was deleted from DB successfully\n", name);
	)

	return ret;
}

int removeObjectFromDB(const char * objectPath)
{
	int ret = OK;
	sqlite3 *db;
	char zSql[1024] = {"\0"};
	//char tmpObjectPath[512] = {"\0"};
	int len = 0;

	if (!objectPath)
		return ret;

	len = strlen(objectPath);
	if (objectPath[len-1] != '.')
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DB, "removeObjectFromDB(): '%s' is a wrong objectPath\n", objectPath);
		)
		return ERR_INVALID_PARAMETER_NAME;
	}

	sprintf(zSql, "DELETE FROM params WHERE name like '%s%%';\n"                 /* Delete from table params */
				  "DELETE FROM metadatas WHERE name like '%s%%';",objectPath, objectPath); /* Delete from table metadatas */

	db = getDBConnection();
	if (!db)
		return ERR_WRITE_PARAMFILE;

	ret = sqlite3_exec(db, zSql, 0, 0, 0);
	if( ret != SQLITE_OK )
	{
		databaseError(db);
		releaseDBConnection();
		return ERR_WRITE_PARAMFILE;
	}

	releaseDBConnection();

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DB, "removeObjectFromDB(): Object %s was deleted from DB successfully\n", objectPath);
	)

	return ret;
}

int removeAllParameters(void)
{
	int ret = OK;
	sqlite3 *db;
	const char *zSql = "DELETE FROM params;\n"
			"DELETE FROM metadatas;";

	db = getDBConnection();
	if (!db)
		return ERR_WRITE_PARAMFILE;

	/* Write to table*/
	ret = sqlite3_exec(db, zSql, 0, 0, 0);
	if( ret != SQLITE_OK )
	{
		databaseError(db);
		releaseDBConnection();
		return ERR_WRITE_PARAMFILE;
	}

	releaseDBConnection();
	return ret;
}

static
int callbackForPrintDB(void *notused, int coln, char **rows, char **colnm)
{
	int i;
	static int b = 1;
	/* print column names */
	if (b) /* print only once */
	{
		for(i=0; i<coln; i++)
			printf("%s\t", colnm[i]);
		printf("\n");
		b = 0;
	}
	/* print line separator */
	for(i=0; i<coln; i++)
		printf("-------\t");
	printf("\n");
	/* print values */
	for(i=0; i<coln; i++)
		printf("%s\t|", rows[i]);
	printf("\n");
	return OK;
}

int printRunTimeDB()
{
	int ret = OK;
	sqlite3 *db;
	const char *zSql = "select * from params";
	const char *zSql2 = "select * from metadatas";

	db = getDBConnection();
	if (!db)
		return ERR_WRITE_PARAMFILE;

	/* Execution request */
	ret = sqlite3_exec(db, zSql, callbackForPrintDB, 0, 0);
	if( ret != SQLITE_OK )
	{
		databaseError(db);
		releaseDBConnection();
		return ret;
	}

	ret = sqlite3_exec(db, zSql2, callbackForPrintDB, 0, 0);
	if( ret != SQLITE_OK )
	{
		databaseError(db);
		releaseDBConnection();
		return ret;
	}

	releaseDBConnection();

	return ret;
}


/** Store the metadata of parameter in DB
 * The metadata is given in a character string
 */
int storeParameter(const char * name, const char * data)
{
	int ret = OK;
	sqlite3 *db;
	char zSql[MAX_PARAM_VALUE_SIZE + 100] = {"\0"};
	sqlite3_stmt *res;
	const char * tail;
	bool isParamExistsInParams = false; /* 0 - insert, 1 - update */
	char tmpResult[MAX_PARAM_VALUE_SIZE + 1] = {0};
	char *p1 = zSql, *p2 = zSql;
	int n = 0, n1 = 1, n2 = 1;

	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "storeParameter(): function parameter 'name' cannot be NULL.\n");
		)
		return ERR_INTERNAL_ERROR;
	}

	/* find meta data in DB */
	sprintf(zSql, "select data from metadatas where name='%s'",name);

	db = getDBConnection();
	if (!db)
		return ERR_WRITE_PARAMFILE;

	ret = sqlite3_prepare_v2(db, zSql, sizeof(zSql), &res, &tail);
	if (ret != SQLITE_OK)
	{
		databaseError(db);
		releaseDBConnection();
		return ERR_READ_PARAMFILE;
	}
	if(sqlite3_step(res) == SQLITE_ROW)
	{
		char * tmp = (char *)sqlite3_column_text(res, 0);
		if (tmp)
		{
			//We use zSql as temporary buffer
			strncpy(zSql, tmp, sizeof(zSql)-1);
			n = strlen(zSql);
		}
		/* make update */
		isParamExistsInParams = true;
	}

	sqlite3_finalize(res);

	if( n == 0 )
	{
		strncpy(tmpResult, data, sizeof(tmpResult)-1);
	}
	else
	{
		while( *p2++ != '\n' )
			n1++;
		if( p2 == &zSql[n] )  *p2 = 0;
		if( *p1 == *data )
		{
			strcpy(tmpResult, data);
			if( *p2 )
			{
				p1 = p2;
				while( *p1++ != '\n' )
					n2++;
				strncat(tmpResult, p2, n2);
			}
		}
		else
		{
			strncpy(tmpResult, p1, n1 );
			strncat(tmpResult, data, strlen(data));
		}
	}

	if(isParamExistsInParams == true)
	{
		/* make update */
		sprintf(zSql, "UPDATE metadatas SET data='%s' where name='%s'",tmpResult, name);
	}
	else
	{
		/* make insert*/
		sprintf(zSql, "INSERT INTO metadatas(name, data) VALUES('%s','%s')", name, tmpResult);
	}

	ret = sqlite3_exec(db, zSql, 0, 0, 0);
	if( ret != SQLITE_OK )
	{
		databaseError(db);
		releaseDBConnection();
		return ERR_WRITE_PARAMFILE;
	}
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_DB, "storeParameter(): metadata of parameter %s was %s to DB successfully\n", name, (isParamExistsInParams ? "updated" : "inserted"));
	)

	releaseDBConnection();
	return OK;
}

int reloadParameters(newParam *callbackF)
{
	int ret = OK;
	sqlite3 *db;
	char zSql[MAX_PARAM_VALUE_SIZE+100] = {"\0"};
	sqlite3_stmt *res;
	const char * tail;
    int len = 0, offset=0;

	/* find meta data in DB */
	sprintf(zSql, "select name, data from metadatas");

	db = getDBConnection();
	if (!db)
		return ERR_WRITE_PARAMFILE;

	ret = sqlite3_prepare_v2(db, zSql, 1000, &res, &tail);
	if (ret != SQLITE_OK)
	{
		databaseError(db);
		releaseDBConnection();
		return ERR_READ_PARAMFILE;
	}

	while (sqlite3_step(res) == SQLITE_ROW)
	{
		char *name = (char *)sqlite3_column_text(res, 0);
		char *data = (char *)sqlite3_column_text(res, 1);

		releaseDBConnection();

		offset = len = 0;

	    memset(zSql, 0, sizeof zSql);
	    while ( (ret=sscanf( data + offset,"%[^\n]%n", zSql , &len)) != EOF)
	    {
	    	offset += len * ret; //if no line was returned, then ret == 0, but len is undefined, so I set len to 0.
	    	offset += ( *(data+offset) == '\n' ) ? 1 : 0; //skip '\n' symbol

	    	if ( zSql[0] == '#' || zSql[0] == ' ' || zSql[0] == '\n' || zSql[0] == '\0' )
	    		continue;
	    	ret = callbackF (name, zSql);

	    	*zSql = 0;
	    }
		db = getDBConnection();
	}

	sqlite3_finalize(res);

	releaseDBConnection();
	return OK;
}

#endif /* WITH_USE_SQLITE_DB */
