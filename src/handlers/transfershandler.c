/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"

#ifdef HAVE_GET_ALL_QUEUED_TRANSFERS

#include <pthread.h>

#include "filetransfer.h"
#include "stdsoap2.h"

#define 	BUF_SIZE_TRANSFERS		1024
#define 	BUF_SIZE_COMMAND		257
#define		TRANSFER_ACCEPTED		"Transfer accepted"
#define		TRANSFER_NOT_ACCEPTED	"Transfer not Accepted"

extern int procId;
extern pthread_mutex_t informLock;
extern char host[];

/* used to inform the mainloop that the transfersHandler could bind on the port. */
pthread_cond_t transfersHandlerStarted = PTHREAD_COND_INITIALIZER;
pthread_mutex_t transfersHandlerMutexLock = PTHREAD_MUTEX_INITIALIZER;

static char buf_transfers[BUF_SIZE_TRANSFERS + 1];

static char Tcommand[BUF_SIZE_COMMAND];
static char Ttype[BUF_SIZE_COMMAND];
static char Turl[BUF_SIZE_COMMAND];
static char Tusername[BUF_SIZE_COMMAND];
static char Tpassword[BUF_SIZE_COMMAND];
static char Tdelay[BUF_SIZE_COMMAND];
static char Tsize[BUF_SIZE_COMMAND];
static char Ttarget[BUF_SIZE_COMMAND];
static char Tsuccess[BUF_SIZE_COMMAND];
static char Tfailure[BUF_SIZE_COMMAND];

static struct TimeWindowStruct __ptrTimeWindowStruct1, __ptrTimeWindowStruct2;
static struct TimeWindowStruct * arrayPtrTimeWindowStruct[] = {&__ptrTimeWindowStruct1, &__ptrTimeWindowStruct2};
static struct ArrayOfTimeWindowsStruct timeWindowList;
static char tmpstr[20];
static char WindowMode1[65];
static char UserMessage1[257];
static char WindowMode2[65];
static char UserMessage2[257];

static char from_hex( char );
static char *url_decode( char * );
static void StrFind( const char *, const char *, char *, int );

/* Converts a hex character to its integer value */
static char from_hex( char ch )
{
	return isdigit(ch) ? ch - '0' : tolower(ch) - 'a' + 10;
}

/* Returns a url-decoded version of str */
static char * url_decode(char *str)
{
	char s[BUF_SIZE_COMMAND];
	char *pstr = str;
	char *pbuf = s;

	while (*pstr)
	{
		if (*pstr == '%')
		{
			if (pstr[1] && pstr[2])
			{
				*pbuf++ = from_hex(pstr[1]) << 4 | from_hex(pstr[2]);
				pstr += 2;
			}
		}
		else if (*pstr == '+')
		{
			*pbuf++ = ' ';
		}
		else
		{
			*pbuf++ = *pstr;
		}
		pstr++;
	}
	*pbuf = '\0';
	strcpy(str, s);

	return str;
}

static void StrFind (const char *bstr, const char *cstr, char *tstr, int tstr_len)
{
	char localbuf[BUF_SIZE_TRANSFERS + 1];
	char *s = localbuf;

	memset( tstr, 0, tstr_len);
	strncpy (s, bstr, BUF_SIZE_TRANSFERS);
	s = strstr (s, cstr);
	if (s)
	{
		s = strtok (s, "=");
		if(s)
		{
			s = strtok (NULL,"& ");
			if(s)
			{
				strncpy (tstr, s, tstr_len-1);
			}
		}
	}
}

/* This function handles AllQueuedTransfers */
void * transfersHandler (void *localSoap)
{
	int ret = OK;
	int mutexStat = OK;
	struct soap *soap = (struct soap *) localSoap;
	int timeout = 0;
	struct timespec delay;
	struct timespec rem;
	delay.tv_sec = 1;
	delay.tv_nsec = 0;
	int requestCnt = 0;
	time_t actualTime = 0;
	time_t requestSlot = 0;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "Host: %s , Transfer handler port: %d\n", host, (TRANSFERS_NOTIFICATION_PORT + (procId)));
	)

	soap->accept_timeout = timeout;
	soap->recv_timeout = 30;

	/* wait on port */
	soap->bind_flags = SO_REUSEADDR;
	ret = soap_bind (soap, host, TRANSFERS_NOTIFICATION_PORT + (procId), 100);
	while (ret < SOAP_OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_HOST, "Transfer handler port doesn't bind. The next attempt will be fulfilled. "
						"Bind(): ret = %d, errno = %d (%s), soap->error = %d (see stdsoap2.h)\n", ret, soap->errnum, strerror(soap->errnum), soap->error);
		)
		//soap_closesock (soap);
		soap_destroy(soap);
		soap_end(soap);
		soap->bind_flags = SO_REUSEADDR;
		ret = soap_bind (soap, host, TRANSFERS_NOTIFICATION_PORT + (procId), 100);
		nanosleep( &delay, &rem );
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "Transfer handler port binds success\n");
	)

#ifdef USE_DELAY_AT_THREAD_START
	/* tell the mainloop we are ready to serve the Kicked */
	nanosleep( &delay, &rem );
#endif
	pthread_mutex_lock (&transfersHandlerMutexLock);
	pthread_cond_signal (&transfersHandlerStarted);
	pthread_mutex_unlock (&transfersHandlerMutexLock);

	ret = soap_accept (soap);

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_TRANSFER, "Transfers Accept ret: %d\n", ret);
	)

	/* init DoS attack detection */
	requestSlot = time(NULL);
	requestSlot += MAX_REQUEST_TIME;

	while (true)
	{
		/* count the number of requests in a defined period of time
		 * and reject any request if the number of request is above a maximum number
		 *
		 * 	requestSlot ( lower and upper time) is set
		 *  MAX_REQUEST_TIME defined
		 *  MAX_REQUESTS_PER_TIME defined
		 *  reqCounter set to zero
		 *
		 *  			requestSlot			AnalysisRequests
		 *	-------------------------------------------------------
		 *  OK  	 	actTime in          <= MaxRequests
		 *  Fail		actTime in			> MaxRequests
		 *  OK  		actTime out			Reset reqCounter, recalc requestSlot */
		if (ret > 0)
		{
			/* check for DoS attack */
			actualTime = time(NULL);
			if ( actualTime <= requestSlot )
			{
				requestCnt++;
				if ( requestCnt > MAX_REQUESTS_PER_TIME )
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_TRANSFER, "transfersHandler(): DoS attack was detected. Request count is %d per %d seconds. The response 503 error message.\n", requestCnt, MAX_REQUEST_TIME);
					)
					/* Write a HTTP 503 Error message if we are not ready to serve the request */
					soap->fresponse( soap, 503, 0 );
					//soap_closesock (soap);
					soap_destroy(soap);
					soap_end(soap);
					efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
					ret = soap_accept (soap);
					continue;
				}
			}
			else
			{
				/* recalc requestSlot value */
				requestSlot = actualTime + MAX_REQUEST_TIME;
				requestCnt = 0;
			}

			mutexStat = pthread_mutex_trylock (&informLock);
			if (mutexStat == OK)
			{
				soap_getline(soap, buf_transfers, BUF_SIZE_TRANSFERS);

				StrFind (buf_transfers, "command", Tcommand, sizeof(Tcommand) );
				StrFind (buf_transfers, "type", Ttype, sizeof(Ttype));
				StrFind (buf_transfers, "url", Turl, sizeof(Turl));
				StrFind (buf_transfers, "username", Tusername, sizeof(Tusername));
				StrFind (buf_transfers, "password", Tpassword, sizeof(Tpassword));
				StrFind (buf_transfers, "size", Tsize, sizeof(Tsize));
				StrFind (buf_transfers, "target", Ttarget, sizeof(Ttarget));
				StrFind (buf_transfers, "success", Tsuccess, sizeof(Tsuccess));
				StrFind (buf_transfers, "failure", Tfailure, sizeof(Tfailure));
				if (strcmp(Tcommand, "ScheduleDownLoad"))
				{
					StrFind (buf_transfers, "delay", Tdelay, sizeof(Tdelay));
				}
				else
				{
					// First time window:
					StrFind (buf_transfers, "WindowStart1", tmpstr, sizeof(tmpstr));
					if (*tmpstr)
						__ptrTimeWindowStruct1.WindowStart = atoi(tmpstr);
					else
						__ptrTimeWindowStruct1.WindowStart = 0;

					StrFind (buf_transfers, "WindowEnd1", tmpstr, sizeof(tmpstr));
					if (*tmpstr)
						__ptrTimeWindowStruct1.WindowEnd = atoi(tmpstr);
					else
						__ptrTimeWindowStruct1.WindowEnd = 0;

					StrFind (buf_transfers, "MaxRetriess1", tmpstr, sizeof(tmpstr));
					if (*tmpstr)
						__ptrTimeWindowStruct1.MaxRetries = atoi(tmpstr);
					else
						__ptrTimeWindowStruct1.MaxRetries = 0;

					StrFind (buf_transfers, "WindowMode1", WindowMode1, sizeof(WindowMode1));
					__ptrTimeWindowStruct1.WindowMode = WindowMode1;

					StrFind (buf_transfers, "UserMessage1", UserMessage1, sizeof(UserMessage1));
					__ptrTimeWindowStruct1.UserMessage = UserMessage1;

					// Second time window:
					StrFind (buf_transfers, "WindowStart2", tmpstr, sizeof(tmpstr));
					if (*tmpstr)
						__ptrTimeWindowStruct2.WindowStart = atoi(tmpstr);
					else
						__ptrTimeWindowStruct2.WindowStart = 0;

					StrFind (buf_transfers, "WindowEnd2", tmpstr, sizeof(tmpstr));
					if (*tmpstr)
						__ptrTimeWindowStruct2.WindowEnd = atoi(tmpstr);
					else
						__ptrTimeWindowStruct2.WindowEnd = 0;

					StrFind (buf_transfers, "MaxRetriess2", tmpstr, sizeof(tmpstr));
					if (*tmpstr)
						__ptrTimeWindowStruct2.MaxRetries = atoi(tmpstr);
					else
						__ptrTimeWindowStruct2.MaxRetries = 0;

					StrFind (buf_transfers, "WindowMode2", WindowMode2, sizeof(WindowMode2));
					__ptrTimeWindowStruct2.WindowMode = WindowMode2;

					StrFind (buf_transfers, "UserMessage2", UserMessage2, sizeof(UserMessage2));
					__ptrTimeWindowStruct2.UserMessage = UserMessage2;

					timeWindowList.__ptrTimeWindowStruct = (struct TimeWindowStruct **) &arrayPtrTimeWindowStruct;
					timeWindowList.__size = 0;
					if (__ptrTimeWindowStruct1.WindowStart > 0)
					{
						timeWindowList.__size++;
						if (__ptrTimeWindowStruct2.WindowStart > 0)
							timeWindowList.__size++;
					}
				}

				char *TransfersURL;
				char *TransfersSiteURL;
				bool flag_out_transfer = false;

				/* find X-Forwarded-For */
				do
				{
					soap_getline(soap, buf_transfers, BUF_SIZE_TRANSFERS);
					if(strstr(buf_transfers,"X-Forwarded-For"))
					{
						TransfersSiteURL = strchr(buf_transfers,' ');
						while( *TransfersSiteURL != 0 && *TransfersSiteURL == ' ')
						{
							TransfersSiteURL++;
						}
						if(TransfersSiteURL)
						{
							ret = getParameter (TRANSFERURL, &TransfersURL);
							if (ret != OK || inet_addr (TransfersURL) != inet_addr (TransfersSiteURL))
							{
								break;
							}
							else
							{
								flag_out_transfer = true;
								break;
							}
						}
						else
						{
							break;
						}
					}
				}while(*buf_transfers);

				/* Go to end of HTTP/MIME header*/
				/* empty line: end of HTTP/MIME header */
				while(*buf_transfers)
				{
					soap_getline(soap, buf_transfers, BUF_SIZE_TRANSFERS);
				}

				if(!flag_out_transfer || !Turl[0] ||
						(1
#ifdef HAVE_FILE_DOWNLOAD
								&& strcmp(Tcommand, "Download")
#endif
#ifdef HAVE_FILE_SCHEDULE_DOWNLOAD
								&& strcmp(Tcommand, "ScheduleDownLoad")
#endif
#ifdef HAVE_FILE_UPLOAD
								&& strcmp(Tcommand, "Upload")
#endif
						)
				  )
				{
					pthread_mutex_unlock (&informLock);

					DEBUG_OUTPUT (
							dbglog (SVR_INFO, DBG_TRANSFER, "Transfer. Not accepted. The response 503 error message.\n");
					)

					/* Write a HTTP 503 Error message */
					soap->fresponse( soap, 503, 0 );
					//soap_closesock (soap);
					soap_destroy(soap);
					soap_end(soap);
					efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
					ret = soap_accept (soap);
					continue;
				}
				DEBUG_OUTPUT (
						dbglog (SVR_INFO, DBG_TRANSFER, "Transfer. Accepted\n");
				)

				if (!strcmp(Tcommand, "Download"))
				{
#ifdef HAVE_FILE_DOWNLOAD
					struct cwmp__DownloadResponse response;
					ret = execDownload ( soap, "",
							url_decode (Ttype),
							url_decode (Turl),
							url_decode (Tusername),
							url_decode (Tpassword),
							atoi (Tsize),
							url_decode (Ttarget),
							atoi (Tdelay),
							url_decode (Tsuccess),
							url_decode (Tfailure),
							NOTACS,
							TransfersURL,
							Turl,
							&response);
#endif
				}
				else
					if (!strcmp(Tcommand, "ScheduleDownLoad"))
					{
#ifdef HAVE_FILE_SCHEDULE_DOWNLOAD
						struct cwmp__ScheduleDownloadResponse response;
						ret = execScheduleDownload ( soap,
								"",
								url_decode (Ttype),
								url_decode (Turl),
								url_decode (Tusername),
								url_decode (Tpassword),
								atoi (Tsize),
								url_decode (Ttarget),
								&timeWindowList,
								NOTACS,
								TransfersURL,
								Turl,
								&response);
#endif
					}
					else
					{
#ifdef HAVE_FILE_UPLOAD
						struct cwmp__UploadResponse response;
						ret = execUpload ( soap, "",
								url_decode (Ttype),
								url_decode (Turl),
								url_decode (Tusername),
								url_decode (Tpassword),
								atoi (Tdelay),
								NOTACS,
								TransfersURL,
								Turl,
								&response);
#endif
					}

				pthread_mutex_unlock (&informLock);

				soap_send (soap, ret ? TRANSFER_NOT_ACCEPTED : TRANSFER_ACCEPTED);
				//soap_closesock (soap);
				soap_destroy(soap);
				soap_end(soap);
				efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
				ret = soap_accept (soap);
			}
			else
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_TRANSFER, "transfersHandler()->pthread_mutex_trylock() returns mutexStat = %d. The response 503 error message..\n", mutexStat);
				)
				/* Write a HTTP 503 Error message if we are not ready to serve the request */
				soap->fresponse( soap, 503, 0 );
				//soap_closesock (soap);
				soap_destroy(soap);
				soap_end(soap);
				efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
				ret = soap_accept (soap);
			}
		}
		else
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_TRANSFER, "Transfers Accept error: %d\n", errno);
			)
			efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
			nanosleep( &delay, &rem );
		}
	}
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	pthread_exit(NULL);
	return NULL;
}

#endif /* HAVE_GET_ALL_QUEUED_TRANSFERS */
