/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/
/*
 * DBhandler.h
 *
 *  Created on: Sep 12, 2011
 *      Author: san
 */

#ifndef DBhandler_h
#define DBhandler_h

#ifdef WITH_USE_SQLITE_DB

#include <sqlite3.h>

#include "dimark_globals.h"

typedef int (newParam) (char *, char *);

typedef enum tDBstateEnum
{
	DBClose,
	DBOpen,
	DBBusy
} DBstateEnum;

typedef struct tSQLiteDBState
{
	sqlite3* db;
	DBstateEnum DBstate;
} tSQLiteDBState;

typedef enum tDBSimpleCommand
{
	END,
	BEGIN,
	BEGIN_TRANSACTION,
	COMMIT,
	ROLLBACK,
	SAVEPOINT,
	RELEASE_SAVEPOINT,
	ROLLBACK_TO_SAVEPOINT
} DBSimpleCommand;

int createDBAndTables();
int storeParamValue(const char * , ParameterType , ParameterValue * );
int storeParameterAttributes(int , const char * , int , int , char ** , int );
int updateParamValue(const char * , ParameterType , ParameterValue * );
int updateParameterAttributes(int , const char * , int , int , char ** , int );
int retrieveParamValue(const char *, ParameterType, ParameterValue * );
int retrieveParameterAttributes(int * param_id, const char * paramName, int * type, NotificationType * param_notification, char * accessListString);
int removeParameter(const char *);
int removeObjectFromDB(const char * objectPath);
int removeAllParameters(void);
int printRunTimeDB();

int storeParameter( const char *, const char * );
int reloadParameters(newParam *callbackF);

sqlite3* getDBConnection();
int releaseDBConnection();
int closeDBConnection(int );

int executeDBCommand(DBSimpleCommand  );

#endif /* WITH_USE_SQLITE_DB */

#endif /* DBhandler_h */
