/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef CRhandler_H
#define CRhandler_H

#include "dimark_globals.h"

#ifdef HAVE_CONNECTION_REQUEST
/* Time we wait until the CRhandler must bind otherwise there is a problem */
#define ACS_HANDLER_WAIT_SECS  120

extern pthread_cond_t CRhandlerStarted;
extern pthread_mutex_t CRhandlerMutexLock;

void *CRhandler (void *);


int runCRhandler(char *, int , char *, char *, char *);

int terminateCRhandler();

#endif /* HAVE_CONNECTION_REQUEST */
#endif /* CRhandler_H */
