/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include <stdio.h>
#include <time.h>
#include <sys/time.h>

//#include "dimark_globals.h"
#include <integrationSettings.h>
#include "DoSAttackChecking.h"
#include "utils.h"

typedef unsigned int uint;
static uint getRequestCountPerInterval(uint [], uint , uint *, struct timeval *, uint );
static uint getEverageIntervalBetweenRequests(uint [], uint , uint *, struct timeval *);


/* The CPE SHOULD restrict the number of Connection Requests for a particular CWMP Endpoint
 * that it accepts during a given period of time in order to further reduce the possibility
 * of a denial of service attack. If the CPE chooses to reject a Connection Request for this reason,
 * the CPE MUST respond to that Connection Request with an HTTP 503 status code (Service Unavailable).
 * In this case, the CPE SHOULD NOT include the HTTP Retry-After header in the response.
 * */

/* Defines for DoS attack recognition length of the time slot/interval in seconds */
#ifndef CR_TIME_INTERVAL
#define CR_TIME_INTERVAL				30 //sec
#endif

/* the number of allowed requests in one time slot/interval */
#ifndef CR_MAX_REQUESTS_PER_TIME
#define CR_MAX_REQUESTS_PER_TIME 		6
#endif

/* Size of CR_time_deltas[] to save the statistics of last requests times. MUST be >= CR_MAX_REQUESTS_PER_TIME !!! */
#ifndef CR_REQUEST_STATISTICS_COUNT
#define CR_REQUEST_STATISTICS_COUNT		CR_MAX_REQUESTS_PER_TIME
#endif

/* Optional setting (may be not defined):
 * the min time between requests, msec
 * If defined the any response will not be sent if time between current
 * and previous HTTP requests < CR_MIN_TIME_BETWEEN_REQUESTS */
#ifndef CR_MIN_TIME_BETWEEN_REQUESTS
#define CR_MIN_TIME_BETWEEN_REQUESTS	200 //in milliseconds
#endif

/* Optional setting (may be not defined):
 * If defined this value means following:
 * if the time between last sent Inform with event "6 CONNECTION REQUEST" and
 * current time > MAX_INTERVAL_WITHOUT_CR_INFORM_DURING_DOS_ATTACK then
 * Inform with event "6 CONNECTION REQUEST" will be send
 * regardless of whether the attack is detected.*/
#ifndef MAX_INTERVAL_WITHOUT_CR_INFORM_DURING_DOS_ATTACK
#define MAX_INTERVAL_WITHOUT_CR_INFORM_DURING_DOS_ATTACK  30000 //in milliseconds
#endif

int isCR_DoS_AttackDetected(struct timeval *last_CR_InformTime)
{
	static uint CR_currentIdx = 0;
	static uint CR_time_deltas[CR_REQUEST_STATISTICS_COUNT] = { 0 };
	static struct timeval CR_lastRequestTime = {0, 0};
	uint requestCountPerInterval;
	uint average;

	int i;

	requestCountPerInterval = getRequestCountPerInterval(CR_time_deltas, CR_REQUEST_STATISTICS_COUNT, &CR_currentIdx, &CR_lastRequestTime, CR_TIME_INTERVAL);
	//now (after getRequestCountPerInterval call) the CR_lastRequestTime contains a current time.

#ifdef CR_MIN_TIME_BETWEEN_REQUESTS
	if ( CR_time_deltas[(CR_REQUEST_STATISTICS_COUNT + CR_currentIdx-1)%CR_REQUEST_STATISTICS_COUNT] < CR_MIN_TIME_BETWEEN_REQUESTS)
	{
		return DOS_ATTACK_IS_DETECTED_DO_NOT_RESPOND;
	}
#endif

#ifdef MAX_INTERVAL_WITHOUT_CR_INFORM_DURING_DOS_ATTACK
	if ( get_timeval_delta(last_CR_InformTime, &CR_lastRequestTime) / 1000 >= MAX_INTERVAL_WITHOUT_CR_INFORM_DURING_DOS_ATTACK)
	{
		// store last_CR_InformTime
		last_CR_InformTime->tv_sec = CR_lastRequestTime.tv_sec;
		last_CR_InformTime->tv_usec = CR_lastRequestTime.tv_usec;
		return NO_DOS_ATTACK;
	}
#endif

	if (requestCountPerInterval > CR_MAX_REQUESTS_PER_TIME)
			return DOS_ATTACK_IS_DETECTED_SEND_503;

	return NO_DOS_ATTACK;
}

static uint getRequestCountPerInterval(uint time_deltas[], uint timesSize, uint *currentIdx, struct timeval *lastRequestTime, uint interval)
{
	int i;
	long delta;
	struct timeval current_tv;
	uint realCount;

	gettimeofday(&current_tv, NULL);
	usleep(1000 - current_tv.tv_usec%1000); //to avoid handling several requests for 1 millisecond

	delta = get_timeval_delta(lastRequestTime, &current_tv);

	delta = (delta+500)/1000; // convert to milliseconds

	lastRequestTime->tv_sec = current_tv.tv_sec;
	lastRequestTime->tv_usec = current_tv.tv_usec;

	time_deltas[*currentIdx] = ( delta > UINT_MAX ? UINT_MAX : (uint)delta);
	*currentIdx = (*currentIdx + 1)%timesSize;

	interval *= 1000; // convert to milliseconds

	realCount = 1;
	delta = 0L;
	i = *currentIdx;
	do
	{
		i = (timesSize + i-1)%timesSize;
		if (time_deltas[i])
		{
			delta += time_deltas[i];
			if (delta > interval)
				break;
			realCount++;
		}
	} while (i != *currentIdx);

	return realCount;
}


static uint getEverageIntervalBetweenRequests(uint time_deltas[], uint timesSize, uint *currentIdx, struct timeval *lastRequestTime)
{
	int i;
	long delta;
	struct timeval current_tv;
	uint realCount = 0;

	gettimeofday(&current_tv, NULL);
	usleep(1000 - current_tv.tv_usec%1000); //to avoid handling several requests for 1 millisecond

	delta = get_timeval_delta(lastRequestTime, &current_tv);
	delta = (delta+500)/1000; // convert to milliseconds

	lastRequestTime->tv_sec = current_tv.tv_sec;
	lastRequestTime->tv_usec = current_tv.tv_usec;

	time_deltas[*currentIdx] = ( delta > UINT_MAX ? UINT_MAX : (uint)delta);
	*currentIdx = (*currentIdx + 1)%timesSize;

	delta = 0L;
	// calculate the average interval between requests:
	for (i=0; i<timesSize; i++)
	{
		if (time_deltas[i])
		{
			delta += time_deltas[i];
			realCount++;
		}
	}

	return (uint)(delta / realCount);
}



//#define CR_MIN_EVERAGE_INTERVAL_BETWEEN_REQUESTS  1000 //in milliseconds
// CR_MIN_EVERAGE_INTERVAL_BETWEEN_REQUESTS_WITH_SENDING_503 must be <= CR_MIN_EVERAGE_INTERVAL_BETWEEN_REQUESTS
//#define CR_MIN_EVERAGE_INTERVAL_BETWEEN_REQUESTS_WITH_SENDING_503  400 //in milliseconds
















