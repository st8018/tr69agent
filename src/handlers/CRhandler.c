/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "dimark_globals.h"

#ifdef HAVE_CONNECTION_REQUEST

#include <pthread.h>
#include <errno.h>

#include "eventcode.h"
#include "paramconvenient.h"
#include "DoSAttackChecking.h"
#include "CRhandler.h"

struct DigestData
{
	char username[256+1];
	char realm[32+1];
	char uri[32+1];
	char nonce[32+1];
	char nc[10+1];
	char cnonce[32+1];
	char qop[32+1];
	char response[256+1];
};

extern int procId;
extern pthread_mutex_t informLock;
extern char host[];
#ifdef BINDING_TO_INTERFACE_NAME_IS_ENABLED
// Intel Bind to device, TCP socket option
extern char bind_interface_name[];
#endif /* BINDING_TO_INTERFACE_NAME_IS_ENABLED */
extern char ConnectionReguestURLPath[CR_URL_PATH_LEN+1];
extern char info_for_soap_header[100];

/* used to inform the mainloop that the CRhandler could bind on the port. */
pthread_cond_t CRhandlerStarted = PTHREAD_COND_INITIALIZER;
pthread_mutex_t CRhandlerMutexLock = PTHREAD_MUTEX_INITIALIZER;

static const char *myNonce;
static struct DigestData digestData;

static int acsHttpGet (struct soap *);
static int acsHttpParseHeader (struct soap *, const char *, const char *);
static int checkDigestResponse (struct soap *, const struct DigestData *);
static void storeDigestData (char *);
static void hex2Ascii (const unsigned char *, char *, int);
static void tcpCRThreadExitCallback(void * arg);

pthread_t CRhandlerTid;
extern struct soap CRSoap;

int runCRhandler(char *proxy_host, int proxy_port, char *proxy_userid, char *proxy_passwd, char *proxy_version)
{
	struct timespec handlerWait;
	pthread_attr_t  threadAttr;

	soap_init(&CRSoap);
	CRSoap.proxy_host = (proxy_host && *proxy_host) ? proxy_host : NULL;
	if (CRSoap.proxy_host)
	{
		CRSoap.proxy_port = proxy_port;
		CRSoap.proxy_userid = (proxy_userid && *proxy_userid) ? proxy_userid : NULL;
		CRSoap.proxy_passwd = (proxy_passwd && *proxy_passwd) ? proxy_passwd : NULL;
		CRSoap.proxy_http_version = (proxy_version && *proxy_version) ? proxy_version : NULL;
	}

	pthread_attr_init(&threadAttr);
	pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_DETACHED);
	pthread_create (&CRhandlerTid, &threadAttr, CRhandler, (void *) &CRSoap);
	/* Wait for x seconds the CRhandler could bind the port */
	handlerWait.tv_sec = time(NULL) + ACS_HANDLER_WAIT_SECS;
	handlerWait.tv_nsec = 0;
	pthread_mutex_lock(&CRhandlerMutexLock);
	if(pthread_cond_timedwait( &CRhandlerStarted, &CRhandlerMutexLock, &handlerWait ) == ETIMEDOUT)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "TCP Connection Request handler binds failed\n" );
		)
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_MAIN, "TCP Connection Request handler binds successfully\n" );
		)
	}
	pthread_mutex_unlock(&CRhandlerMutexLock);

	return OK;
}

int terminateCRhandler()
{
	if (CRhandlerTid > 0)
		pthread_cancel(CRhandlerTid);

	CRhandlerTid = 0;

	return OK;
}

static void tcpCRThreadExitCallback(void * arg)
{
	soap_destroy(&CRSoap);  // delete deserialized objects
	soap_end(&CRSoap); // Remove temporary data and deserialized data
	soap_done(&CRSoap);

	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_REQUEST, "tcpCRThreadExitCallback() has been done. The allocated memory is freed.\n");
	)
}


/** This function handles a call from the ACS and triggers through the accept timeout the InformIntervall */
void *CRhandler (void *localSoap)
{
	int ret = OK;
	int mutexStat = OK;
	struct soap *soap = (struct soap *) localSoap;
	int timeout = 0;
	unsigned int delay_usec = 1000000;
	struct timeval last_CR_InformTime = {0, 0};

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACS, "Host: %s , ACS Port: %d\n", host, (ACS_NOTIFICATION_PORT + (procId)));
	)

	pthread_cleanup_push(tcpCRThreadExitCallback, NULL);

	soap->accept_timeout = 36000;//timeout;
	soap->recv_timeout = 30;

#ifdef BINDING_TO_INTERFACE_NAME_IS_ENABLED
	// Intel Bind to device, TCP socket option
	if (bind_interface_name[0])
	{
		strncpy(soap->bind_interface, bind_interface_name, sizeof(soap->bind_interface));
		soap->bind_interface[sizeof(soap->bind_interface)-1] = '\0';
	}
	else
		memset(soap->bind_interface, 0, sizeof(soap->bind_interface));
#endif

	/* set my one HttpGet and HttpParseHeader handler */
	soap->fget = &acsHttpGet;
	soap->fparsehdr = &acsHttpParseHeader;

	/* wait on port */
	soap->bind_flags = SO_REUSEADDR;
	ret = soap_bind (soap, host, ACS_NOTIFICATION_PORT + (procId), 100);
	while (ret < SOAP_OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACS, "ACS port doesn't bind. The next attempt will be fulfilled. "
						"Bind(): ret = %d, errno = %d (%s), soap->error = %d (see stdsoap2.h)\n", ret, soap->errnum, strerror(soap->errnum), soap->error);
		)
		//soap_closesock (soap);
		soap_destroy(soap);  // delete deserialized objects
		soap_end(soap); // Remove temporary data and deserialized data
		soap->bind_flags = SO_REUSEADDR;
		ret = soap_bind (soap, host, ACS_NOTIFICATION_PORT + (procId), 100);
		usleep(delay_usec);
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_ACS, "ACS port binds success\n");
	)

#ifdef USE_DELAY_AT_THREAD_START
	/* tell the mainloop we are ready to serve the ACS ConnectionRequests */
	usleep(delay_usec);
#endif
	pthread_mutex_lock (&CRhandlerMutexLock);
	pthread_cond_signal (&CRhandlerStarted);
	pthread_mutex_unlock (&CRhandlerMutexLock);

	myNonce = NULL;
	ret = soap_accept (soap);

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_ACS, "CRhandler()->soap_accept() 1 ret: %d\n", ret);
	)

	gettimeofday(&last_CR_InformTime, NULL);

	while(true)
	{
		/* count the number of requests in a defined period of time
		 * and reject any request if the number of request is above a maximum number
		 *
		 * 	requestSlot ( lower and upper time) is set
		 *  MAX_REQUEST_TIME defined
		 *  MAX_REQUESTS_PER_TIME defined
		 *  reqCounter set to zero
		 *
		 *  			requestSlot			AnzahlRequests
		 *	-------------------------------------------------------
		 *  OK 	        actTime in          <= MaxRequests
		 *  Fail		actTime in			> MaxRequests
		 *  OK  		actTime out			Reset reqCounter, recalc requestSlot
		 */

		memset (&digestData, '\0', sizeof (digestData));
		if (ret > 0)
		{
			/* HTTP Request from Server */
			ret = soap->fparse (soap);
			// ret may be == DOS_ATTACK_IS_DETECTED_SEND_503 or DOS_ATTACK_IS_DETECTED_DO_NOT_RESPOND too

			if (soap->error == 1401) // If wrong password was obtained
				soap->error = 401;
			// San:
			// Were found changes in gSoap > stdsoap2.c > soap_getline()
			// Next line added for bug fixing in CR, and in order not to make changes to a gSoap
			//soap->error = ((soap->error == 401) || (soap->error == 404)) ? -1 : soap->error;

			/* Timeout: close socket and restart */
			if ( ret == -1 || ret == 404 || ret == 1401)
			{
				if (ret == 1401)
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_ACS, "ACS Notification failed: 401. The username or password is invalid.\n");
					)
				}
				else
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_ACS, "ACS Notification failed: %d\n", ret);
					)
				}
				soap_destroy(soap);  // delete deserialized objects
				soap_end(soap); // Remove temporary data and deserialized data
				efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
				ret = soap_accept (soap);
				DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_ACS, "CRhandler()->soap_accept() 2 ret: %d\n", ret);
				)
				/* reset nonce value to force a new Digest challenge next time */
				myNonce = NULL;
				continue;
			}

			// Handling of situation when DoS attack is detected:
			if (ret == DOS_ATTACK_IS_DETECTED_DO_NOT_RESPOND || ret == DOS_ATTACK_IS_DETECTED_SEND_503)
			{
				if (ret == DOS_ATTACK_IS_DETECTED_DO_NOT_RESPOND)
				{
					DEBUG_OUTPUT (
							dbglog (SVR_WARN, DBG_ACS, "CRhandler(): frequent DoS attack was detected. This request will be ignored.\n");
					)
					// Don't send any response, because it is very frequent DoS attack. The Dimclient will not lose a resource for this.
				}
				else
				{
					DEBUG_OUTPUT (
							dbglog (SVR_WARN, DBG_ACS, "CRhandler(): DoS attack was detected. The 503 response will be sent.\n");
					)
					/* Write a HTTP 503 (Service Unavailable) Error message if we are not ready to serve the request */
					soap->fresponse( soap, 503, 0 );
					//next line for debug:
					//soap_send (soap, "warn-text: DoS attack was detected");
				}
				soap_destroy(soap);  // delete deserialized objects
				soap_end(soap); // Remove temporary data and deserialized data
				efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
				ret = soap_accept (soap);
				DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_ACS, "CRhandler()->soap_accept() 3 ret: %d\n", ret);
				)
				continue;
			}

			/* if no problems then SOAP_STOP is returned */
			if (ret != SOAP_STOP)
			{
				// In this case the ret == 401 (after 1sr request from ACS and before 2nd request with digest info.)
				DEBUG_OUTPUT (
						dbglog ( (ret == 401 ? SVR_WARN : SVR_ERROR), DBG_ACS, "ACS Notification failed: %d.%s\n", ret,
								(ret == 401 ? " The Digest Authorization information is required from the ACS." : "") );
				)
				efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
				continue;
			}

			mutexStat = pthread_mutex_trylock (&informLock);
			/* reset nonce value to force a new Digest challenge next time */
			myNonce = NULL;
			if (mutexStat == OK)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_INFO, DBG_ACS, "ACS Notification\n");
				)
				addEventCodeSingle (EV_CONNECT_REQ);

				/* Write a HTTP 200 OK message if OK*/
				soap->fresponse( soap, SOAP_OK, 0 );
				//soap_closesock (soap);
				soap_destroy(soap);  // delete deserialized objects
				soap_end(soap); // Remove temporary data and deserialized data

				pthread_mutex_unlock (&informLock);
				gettimeofday(&last_CR_InformTime, NULL);
				setAsyncInform(true);
				efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
				ret = soap_accept (soap);
				DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_ACS, "CRhandler()->soap_accept() 4 ret: %d\n", ret);
				)
			}
			else
			{
				DEBUG_OUTPUT (
						dbglog (SVR_WARN, DBG_ACS, "CRhandler()->pthread_mutex_trylock() returns mutexStat = %d. The response 503 error message..\n", mutexStat);
				)
				/* Write a HTTP 503 Error message if we are not ready to serve the request */
				soap->fresponse( soap, 503, 0 );
				//soap_closesock (soap);
				soap_destroy(soap);  // delete deserialized objects
				soap_end(soap); // Remove temporary data and deserialized data
				efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
				ret = soap_accept (soap);
				DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_ACS, "CRhandler()->soap_accept() 5 ret: %d\n", ret);
				)
			}
		}
		else
		{
			DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_ACS, "ACS Accept: %d\n", errno);
				)
				soap_destroy(soap);  // delete deserialized objects
				soap_end(soap); // Remove temporary data and deserialized data
				efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
				usleep(delay_usec);
				ret = soap_accept (soap);
				DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_ACS, "CRhandler()->soap_accept() 6 ret: %d\n", ret);
				)
		}
	}
	efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY(0);
	pthread_exit (NULL);
	pthread_cleanup_pop(1);
	return NULL;
}

/* This function is called after the header is parsed.
 * If the digest information is not found in digestData
 * an 401 error is returned to the server, to initiate an authorization challenge.
 * If the digest information is found but it is not valid then
 * an 401 error is returned to the server too, but function returns 1401.
 * Also this function may return error code of DoS attack detection.
 */
static int acsHttpGet (struct soap *soap)
{
	int ret = 0;
	char *p = NULL;

	static struct timeval last_CR_InformTime;
	//gettimeofday(&last_CR_InformTime, NULL);

	if(	myNonce == NULL || strlen(digestData.username) == 0 || strlen(digestData.response) == 0 ) //If GET request without HTTP Authorization
	{
		ret = isCR_DoS_AttackDetected(&last_CR_InformTime);
		//if frequent DoS attack is detected the return DOS_ATTACK_IS_DETECTED_DO_NOT_RESPOND.
		if (ret == DOS_ATTACK_IS_DETECTED_DO_NOT_RESPOND)
			return ret;
	}

#ifndef ACS_REGMAN
	p = strchr(soap->endpoint, '/');
	if (!p)
	{
		soap_send (soap, "HTTP/1.1 404 Not Found\r\n");
		return 404;
	}
	p = strchr(p+1, '/');
	if (!p)
	{
		soap_send (soap, "HTTP/1.1 404 Not Found\r\n");
		return 404;
	}
	p = strchr(p+1, '/');
	if (!p)
	{
		soap_send (soap, "HTTP/1.1 404 Not Found\r\n");
		return 404;
	}
	if (strcmp(p+1, ConnectionReguestURLPath) )
	{
		soap_send (soap, "HTTP/1.1 404 Not Found\r\n");
		return 404;
	}
#endif /* #ifndef ACS_REGMAN */

	// When we check URL path, if simply DoS attack is detected the return DOS_ATTACK_IS_DETECTED_SEND_503.
	if (ret == DOS_ATTACK_IS_DETECTED_SEND_503)
		return ret;

	ret = SOAP_OK;

	if(	myNonce == NULL || strlen(digestData.username) == 0 || strlen(digestData.response) == 0 ) //If GET request without HTTP Authorization
	{
		ret = 401;
	}
	else
	{
		// Else, if GET request with HTTP Authorization, then check authorization parameters.
		if ( strCmp(myNonce, digestData.nonce) != 1 || checkDigestResponse (soap, &digestData) != 0	)
		{
			ret = 1401;
		}
	}

	if (ret != SOAP_OK)
	{
		myNonce = createNonceValue();
		sprintf (soap->tmpbuf,
				"WWW-Authenticate: Digest realm=\"Dimark\", qop=\"auth\", nonce=\"%s\"\r\nServer: %s\r\nContent-Type: text/xml; charset=utf-8\r\nContent-Length: 0\r\nConnection: keep-alive\r\n\r\n",
				myNonce, info_for_soap_header);
		soap_send (soap, "HTTP/1.1 401 Unauthorized\r\n");
		soap_send (soap, soap->tmpbuf);
		return ret;
	}

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_ACS, "Right Connection Request was received from ACS.\n");
	)
	return SOAP_OK;
}

/* Only parse the Host and the Authorization Digest */
static int acsHttpParseHeader (struct soap *soap, const char *key, const char *val)
{
	char *valPtr;
	char *headerValue;

	if (!soap_tag_cmp (key, "Host"))
	{
		strcpy (soap->endpoint, "http://");
		strncat (soap->endpoint, val, (sizeof (soap->endpoint) - 8));
		soap->endpoint[sizeof (soap->endpoint) - 1] = '\0';
	}
	else if (!soap_tag_cmp (key, "Authorization"))
	{
		if (!soap_tag_cmp (val, "digest *"))
		{
			valPtr = (char *) &val[7];
			memset(soap->tmpbuf, 0 , sizeof (soap->tmpbuf));
			strncpy (soap->tmpbuf, valPtr, sizeof (soap->tmpbuf)-1);
			valPtr = soap->tmpbuf;
			/* Split the authorization string into the single parts username, realm, nonce, uri, response */
			while (valPtr != NULL)
			{
				headerValue = strsep (&valPtr, ",");
				DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_ACS, "Digest:  %s\n", headerValue);
				)
				storeDigestData (headerValue);
			}
			return SOAP_OK;
		}
	}
	return SOAP_OK;
}

static int checkDigestResponse (struct soap *soap, const struct DigestData *digest)
{
	int ret;
	EVP_MD_CTX ctx;
	static unsigned char a1Digest[17];
	static unsigned char a2Digest[17];
	static unsigned char a3Digest[17];
	unsigned char a1[600];
	unsigned char a2[200];
	unsigned char a3[255];
	unsigned int size;

	char *pConnectionUsername;

	char *pConnectionPassword;

	if (getConnectionUsername(&pConnectionUsername) != OK)
	{
		return 1;
	}
	if (!pConnectionUsername)
		pConnectionUsername = "";

	if((ret = getConnectionPassword(&pConnectionPassword)) != OK)
	{
		return 1;
	}
	if (!pConnectionPassword)
		pConnectionPassword = "";

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_ACS, "user: %s pass: %s nonce= %s\n", pConnectionUsername, pConnectionPassword,
					myNonce ? myNonce:"NULL");
	)

	memset(a1, 0, sizeof(a1));
	memset(a2, 0, sizeof(a2));

	strncpy((char*)a1, pConnectionUsername, 256);
	strcat((char*)a1, ":");
	strcat((char*)a1, CONNECTION_REALM);
	strcat((char*)a1, ":");
	strncat((char*)a1, pConnectionPassword, 256);

	strcpy((char*)a2, "GET");
	strcat((char*)a2, ":");
	strncat((char*)a2, digest->uri, ( sizeof(a2)-strlen((char*)a2)-1));

	EVP_MD_CTX_init((EVP_MD_CTX*)&ctx);
	EVP_DigestInit(&ctx, EVP_md5());
	EVP_DigestUpdate (&ctx, a1, strlen ((char*)a1));
	EVP_DigestFinal(&ctx, a1Digest, &size);

	hex2Ascii(a1Digest, (char*)a1, size);
	EVP_DigestInit(&ctx, EVP_md5());
	EVP_DigestUpdate(&ctx, a2, strlen ((char*)a2));
	EVP_DigestFinal(&ctx, a2Digest, &size);

	hex2Ascii(a2Digest, (char*)a2, size);
	strcpy((char*)a3, (char*)a1);
	strcat((char*)a3, ":");
	strncat((char*)a3, myNonce, sizeof(a3) - strlen(a3) - 1 );
	if(digest->qop[0])
	{
		strcat((char*)a3, ":");
		strcat((char*)a3, digest->nc);
		strcat((char*)a3, ":");
		strcat((char*)a3, digest->cnonce);
		strcat((char*)a3, ":");
		strcat((char*)a3, digest->qop);
	}
	strcat((char*)a3, ":");
	strcat((char*)a3, (char*)a2);
	EVP_DigestInit(&ctx, EVP_md5());
	EVP_DigestUpdate(&ctx, (unsigned char *) a3, strlen ((char*)a3));
	EVP_DigestFinal(&ctx, a3Digest, &size);
	hex2Ascii(a3Digest, (char*)a3, size);
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_ACS, "Digest-Auth: response acs: %s client: %s\n", digest->response, a3 );
	)

	return strcmp ((char*)a3, digest->response);
}

/* Split the dataSet and store the value in digestData */
static void storeDigestData (char *dataSet)
{
	char *keyPtr, *key;
	char *value;
	char *value_first_symbol;

	keyPtr = strsep (&dataSet, "=");
	/* skip trailing spaces */
	while (*keyPtr == ' ')
	{
		keyPtr++;
	}
	key = keyPtr;
	value = strsep(&dataSet, "=");

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_ACS, "Key: %s Value: %s\n", key, value);
	)

	if ( key == NULL || value == NULL )
	{
		return;
	}

	value_first_symbol = value;
	if (strlen( value ) != 0 )
	{
		/* check for quotes */
		if ( value[0] == '"' )
		{
			if(value[strlen (value) - 1] == '"')
			{
				value[strlen (value) - 1] = '\0';
			}
			value_first_symbol++;
		}
	}

	if(strcmp(key, "username") == 0)
	{
		strncpy(digestData.username, value_first_symbol, (sizeof(digestData.username) - 1));
	}
	else if(strcmp(key, "realm") == 0)
	{
		strncpy(digestData.realm, value_first_symbol, (sizeof(digestData.realm) - 1));
	}
	else if(strcmp(key, "uri") == 0)
	{
		strncpy(digestData.uri, value_first_symbol, (sizeof (digestData.uri) - 1));
	}
	else if(strcmp (key, "nonce") == 0)
	{
		strncpy(digestData.nonce, value_first_symbol, (sizeof(digestData.nonce) - 1));
	}
	else if(strcmp (key, "nc") == 0)
	{
		strncpy(digestData.nc, value_first_symbol, (sizeof(digestData.nc) - 1));
	}
	else if(strcmp (key, "response") == 0)
	{
		strncpy(digestData.response, value_first_symbol, (sizeof(digestData.response) - 1));
	}
	else if(strcmp (key, "cnonce") == 0)
	{
		strncpy(digestData.cnonce, value_first_symbol, (sizeof(digestData.cnonce) - 1));
	}
	else if(strcmp (key, "qop") == 0)
	{
		strncpy(digestData.qop, value_first_symbol, (sizeof (digestData.qop) - 1));
	}
}

static void hex2Ascii (const unsigned char *hex, char *ascii, int len)
{
	char *aptr = ascii;
	register int i;
	for (i = 0; i != len; i++)
	{
		sprintf (aptr, "%2.2x", hex[i]);
		aptr++;
		aptr++;
	}
}

#endif /* HAVE_CONNECTION_REQUEST */
