/***************************************************************************
 *    Copyright (C) 2004-2013 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef DoSAttackChecking_H
#define DoSAttackChecking_H

//#include "dimark_globals.h"

#define NO_DOS_ATTACK					 		0
#define DOS_ATTACK_IS_DETECTED_DO_NOT_RESPOND	-1503
#define DOS_ATTACK_IS_DETECTED_SEND_503	 		1503


int isCR_DoS_AttackDetected(struct timeval *);

#endif /* DoSAttackChecking_H */
