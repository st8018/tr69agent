/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef utils_H
#define utils_H

#include "soapH.h"
#include "./integration_interfaces/integrationSettings.h"

#if defined(true) && defined(false)
typedef unsigned int bool;
#else
//if one of them is defined the undefine it.
#ifdef false
#undef false
#endif

#ifdef true
#undef true
#endif
typedef enum Bool
{
	false = 0,
	true = 1
} bool;
#endif

unsigned int getRebootIdx( void );
void addRebootIdx( unsigned int );

/** Returns true if the system made a bootstrap boot 
 * and false all the other time.
 * The flag is set by the event handling
 */
int isBootstrap( void );
void setBootstrap( bool );

void setAsyncInform( const bool );
int getAsyncInform( void );
long getTime( void );
void clearFault( void );
int getFaultFromACS(struct soap *soap);
int  addFault ( const char *, const char *, int, const char * );
void createFault( struct soap *, int );
SetParameterFaultStruct **getParameterFaults( void );
char *getFaultString( int );

/** String functions */
char *strnDupByMemType( char *, const char *, int , unsigned int );
//char *strnDupSession( char *, const char *, int );
//char *strnDupTemp( char *, const char *, int );
//char *strnDup( char *, const char *, int );
void strnCopy( char *, const char *, int );
int strCmp( const char *, const char * );
int strnStartsWith( const char *, const char *, int );

struct SOAP_ENV__Header *setHeader( struct SOAP_ENV__Header * );
struct SOAP_ENV__Header *analyseHeader( struct SOAP_ENV__Header * );

int expWait( unsigned long );

/** Access to the instance number in an object pathname */
int getIdx( const char *, const char * );
int getIdxByName( const char *, const char * );
int getRevIdx( const char *, const char * );

/** Handles the Status return code for SetParameterValue, AddObj, DelObj. */
#define PARAMETER_CHANGES_APPLIED		0
#define PARAMETER_CHANGES_NOT_APPLIED	1

void setParameterReturnStatus( unsigned int );
void setParameterReturnStatusSafe( unsigned int );
int getParameterReturnStatus( void );

/** ACS-CPE Session monitoring */
/** call at start of a ACS session, after the inform message is successfully returned */
void sessionStart( void );
/** get the start time as a session id, this can be used to identify a new session */
time_t getSessionId( void );
/** returns true if the sessionId differs from the given parameter */
int isNewSession( time_t );
/** call at end of an ACS session, after the empty message is received */
void sessionEnd( void );
/** build a session info string */
const char *sessionInfo( void );
/** Increments the call counter every time the ACS calls a CPE function */
void sessionCall( void );

/** creates a random nonce value for Digest */
const char *createNonceValue( void );

/** creates a random integer value */
const int createRandomValue( void );

int a2i( const char *, int * );
unsigned int a2ui( const char * , int * );
long a2l( const char *, int * );
unsigned long a2ul( const char * , int * );
char *dateTime2s( struct timeval *);
int s2dateTime( const char *, struct timeval * );

/** San. 26 July 2011. round functions **/
int iRoundf(float);
int uiRoundf(float);
long int lRoundd(double);

int check_log_files_size(const char * , const unsigned long , const int );
long getFileSize(const char * );
int isFileExists(const char * );

char * accessListToString(char ** , int );

/*San: check format of the adress
 * if format is IPv6 then return 1
 * else return 0
 * */
int isIPv6Adress(const char * );

/* San: extract host string from URL.
 * Format of URL can be IPv4 or IPv6.
 * return -1 if error else return OK
 * return host adress in parameter host
 * return (if isHostInIPv6Format != NULL) isHostInIPv6Format == 1 if WITH_IPV6 && host have IPv6 format
 *  or isHostInIPv6Format == 0
 */
int getHostStrFromURL(const char *URL, char *host, int * isHostInIPv6Format);

void timeval_add_time(struct timeval *, long );
void timeval_sub_time(struct timeval *, long );
long get_timeval_delta(struct timeval *, struct timeval *);

char * formatStringToXml(const char *, char *, unsigned int, char *);

void printTimeMarker( char * );

void setAttrToHTTPandSOAPHeaders(struct soap *);

void checkDirectories(const char *);

int isHexBinaryValueTrue(xsd__hexBinary );

char* getIPAddrByInterfaceName(const char * , struct sockaddr_in *);

void printProcMemStatus(int );

char *myStrCaseStr(char *, const char *);
#endif /* utils_H */
