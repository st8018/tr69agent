/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "eventcode.h"
#include "dimark_globals.h"
#include "utils.h"
#include "debug.h"
#include "host/eventStore.h"
#include "dimclient.h"

#define EVENT_BUFSIZE	200

typedef struct EventStructList
{
	struct EventStruct *data;
	struct EventStructList *next;
} EventStructList;

static EventStructList *evtListFirst = NULL;
static int evtListSize = 0;

/* Inserts an eventCode into the List of EventCodes
 * does no persistent storage */
static int insertEventCode(const char *, const char *);
static int addNewEvent(char *);

/* Write the EventCode into the persistent Database */
static int writeEventCodes(void);

/* Returns an ArrayOfEventStruct with all event codes found in the eventCodelist
 * If "0 BOOTSTRAP" is in the eventCodeList then all other event codes (except on "1 BOOT") will be deleted.
 */
int getEventCodeList(struct ArrayOfEventStruct *evList)
{
	int i = 0;
	struct EventStructList *el;

	cwmp__EventStruct **ev = (cwmp__EventStruct **) emallocTemp(sizeof(cwmp__EventStruct[evtListSize]), 10);
	if (ev == NULL)
		return ERR_RESOURCE_EXCEEDED;

	if (findEventCode(EV_BOOTSTRAP))
	{
		if ( findEventCode(EV_BOOT) )
		{
			freeEventList(false); //it is needed to clear Event Storage, but it will be done in the addEventCodeSingle(). So, use "false" param.
			addEventCodeSingle(EV_BOOT);
		}
		else
			freeEventList(false); //it is needed to clear Event Storage, but it will be done in the addEventCodeSingle(). So, use "false" param.
		addEventCodeSingle(EV_BOOTSTRAP);
		setCWMPVersionToDefault();
	}

	/* Copy list evtListLast entries into array ev[] */
	el = evtListFirst;

	for (i = 0; i != evtListSize; i++)
	{
		ev[i] = el->data;
		el = el->next;
	}
	evList->__ptrEventStruct = ev;
	evList->__size = evtListSize;

	return OK;
}

/* Frees the entries in the EventList
 * call it after successful transfer of Data (after Inform, no after all session) */
void freeEventList(bool freeEventStorage)
{
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_EVENTCODE, "freeEventList(): All event codes will be freed. freeEventStorage = %d\n", freeEventStorage);
	)
	evtListFirst = NULL;
	evtListSize = 0;
	efreeTypedMemByType(MEM_TYPE_EVENT_LIST, 0);
	if (freeEventStorage)
		clearEventStorage();
}

/* Adds an EventCode into the EventCode list
 *
 * param ec 	EventCode
 * param ck	Commandkey
 *
 * returns ErrorCode */
int addEventCodeMultiple(const char *ec, const char *ck)
{
	int ret = OK;

	ret = insertEventCode(ec, ck);
	if (ret == OK)
	{
		ret = writeEventCodes();
	}
	return ret;
}

/* Adds an EventCode into the EventCode list
 * Only eventCodes which are not in the list are inserted
 *
 * param ec 	EventCode
 * param ck	Commandkey
 *
 * returns ErrorCode */
int addEventCodeSingle(const char *ec)
{
	int ret = OK;

	if (findEventCode(ec))
	{
		return OK;
	}

	ret = insertEventCode(ec, "");

	if (ret == OK)
	{
		ret = writeEventCodes();
	}

	return ret;
}

/* Insert an eventCode into the list
 *
 * param ec 	EventCode
 * param ck	Commandkey
 *
 * returns ErrorCode */
static int insertEventCode(const char *ec, const char *ck)
{
	struct EventStructList *el;
	struct EventStruct *es;

	es = (struct EventStruct*) emallocTypedMem(sizeof(struct EventStruct), MEM_TYPE_EVENT_LIST, 0);
	if (es == NULL)
	{
		return ERR_RESOURCE_EXCEEDED;
	}

	es->EventCode = strnDupByMemType(es->EventCode, ec ? ec : "", ec ? strlen(ec) : 0, MEM_TYPE_EVENT_LIST);
	if (es->EventCode == NULL)
	{
		efreeTypedMemByPointer((void **) &es , 0);
		return ERR_RESOURCE_EXCEEDED;
	}

	es->CommandKey = strnDupByMemType(es->CommandKey, ck ? ck : "", ck ? strlen(ck) : 0, MEM_TYPE_EVENT_LIST);
	if (es->CommandKey == NULL)
	{
		efreeTypedMemByPointer((void **) &(es->EventCode) , 0);
		efreeTypedMemByPointer((void **) &es , 0);
		return ERR_RESOURCE_EXCEEDED;
	}

	el = (struct EventStructList*) emallocTypedMem(sizeof(struct EventStructList), MEM_TYPE_EVENT_LIST, 0);

	if (el == NULL)
	{
		efreeTypedMemByPointer((void **) &(es->CommandKey) , 0);
		efreeTypedMemByPointer((void **) &(es->EventCode) , 0);
		efreeTypedMemByPointer((void **) &es , 0);
		return ERR_RESOURCE_EXCEEDED;
	}

	if(evtListFirst  &&  evtListSize == MAX_EVENT_LIST_NUM)
	{
		DEBUG_OUTPUT (
				dbglog( SVR_DEBUG, DBG_EVENTCODE, "Max event list number (%d) is over by event |%s|%s|\n", MAX_EVENT_LIST_NUM, es->EventCode, es->CommandKey );
		)

		struct EventStructList *em = evtListFirst;
		struct EventStructList *before_kill = NULL;
		struct EventStructList *kill = NULL;
		struct EventStructList *tmp = NULL;

		// find last "M ..." eventCode in the List for deleting
		while (em != NULL)
		{
			if (em->data->EventCode[0] == 'M')
			{
				before_kill = tmp;
				kill = em;
			}
			tmp = em;
			em = em->next;
		}

		//kill contains NULL or pointer to last "M ..." event
		//before_kill contains: NULL when kill==evtListFirst  or  kill==NULL
        //						pointer to previous event in the List before kill

		if (kill) // if event for killing was found
		{
			DEBUG_OUTPUT (
					dbglog( SVR_DEBUG, DBG_EVENTCODE, "Event |%s|%s| is taken out from event list\n", kill->data->EventCode, kill->data->CommandKey );
			)

			if (before_kill)
				before_kill->next = kill->next;  // if kill isn't evtListFirst
			else
				evtListFirst = kill->next;  // if kill is evtListFirst

			efreeTypedMemByPointer((void**)&kill->data->EventCode , 0);
			efreeTypedMemByPointer((void**)&kill->data->CommandKey , 0);
			efreeTypedMemByPointer((void**)&kill->data , 0);
			efreeTypedMemByPointer((void**)&kill , 0);
			evtListSize--;
		}
	}

	if (evtListSize < MAX_EVENT_LIST_NUM)
	{
		// add new eventCode to begin of list
		el->data = es;
		el->next = evtListFirst;
		evtListFirst = el;
		evtListSize++;
		return OK;
	}

	//In theory: following code should never execute
	DEBUG_OUTPUT (
			dbglog( SVR_ERROR, DBG_EVENTCODE, "insertEventCode(): event isn't added because eventList is full and none event can be deleted from it.\n");
	)
	return ERR_INTERNAL_ERROR;
}

/* Creates an EventCode depending on
 *
 * PersistentFile		Created EventCode
 * Absent				0 BOOTSTRAP
 * Present				1 BOOT */
int loadEventCode(void)
{
	int ret = OK;
	/* A boot is identified */
	ret = insertEventCode(EV_BOOT, "");
	if (ret != OK)
	{
		return ret;
	}

	if (!isBootstrapMarker())
	{
		/* A bootstrap is identified */
		setBootstrap(true);
		ret = insertEventCode(EV_BOOTSTRAP, "");
		if (ret != OK)
		{
			return ret;
		}
		ret = createBootstrapMarker();
		return ret;
	}

	ret = readEvents((newEvent *) &addNewEvent);
	return ret;
}

int addNewEvent(char *data)
{
	char *ptrBuf;
	char *eventCode;
	char *commandKey;
	int  ret;

	ptrBuf = data;
	eventCode = strsep(&ptrBuf, "|");
	commandKey = strsep(&ptrBuf, "|");
	ret = insertEventCode(eventCode, commandKey);

	return ret;
}

int findEventCode(const char *ec)
{
	struct EventStructList *el = evtListFirst;
	while (el != NULL)
	{
		if (el->data && (strncmp(ec, el->data->EventCode, strlen(el->data->EventCode)) == 0) )
		{
			return 1;
		}
		el = el->next;
	}
	return 0;
}

/* Write all event code from the list into the persistent file */
static int writeEventCodes(void)
{
	int ret = OK;

	struct EventStructList *el;
	char buf[EVENT_BUFSIZE + 1];

	/* remove all pending events before we write the complete list again */
	clearEventStorage();

	el = evtListFirst;
	while (el != NULL)
	{
		DEBUG_OUTPUT (
				dbglog( SVR_DEBUG, DBG_EVENTCODE, "Save Event: %s CommandKey: %s\n", el->data->EventCode, el->data->CommandKey );
		)

		sprintf(buf, "%s|%s|", el->data->EventCode, el->data->CommandKey);
		insertEvent(buf);
		el = el->next;
	}
	return ret;
}

