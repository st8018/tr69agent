/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <stdarg.h>

#include "dimark_globals.h"
#include "utils.h"
#include "parameter.h"
#include "paramaccess.h"
#include "eventcode.h"
#include "parameterStore.h"
#include "methods.h"
#include "list.h"
#include "aliasBasedAddressingUtils.h"
#include "memory_manager.h"

#define NO_DEFAULT_PARAMETER(e)     (e->type < minDefType )
#define IS_DEFAULT_PARAMETER(e) 	(e->type >= minDefType )
#define IS_OBJECT(e)                (e->type == DefaultType || e->type == ObjectType || e->type == MultiObjectType )
#define IS_CHAR_PTR_TYPE(e)        (e->type == StringType || e->type == Base64Type || e->type == hexBinaryType \
||e->type == DefStringType || e->type == DefBase64Type || e->type == DefHexBinaryType)

// store the value bootstrap flag
// for usage in initial parameter load.
//
static volatile bool paramBootstrap = false;

// flag to handle new parameters during an add object call
// used in copy
static volatile bool inAddObject = false;

typedef union
{
	char *cval;	/* Pointer  Characters, Bytes and Base64 */
	unsigned int uival;	/* Value    unsigned Integers */
	int ival;				/* Value    Integers and Boolean */
	struct timeval tval;			/* Value    Date times */
	unsigned long ulval;		/*! Value Unsigned long */
	xsd__hexBinary hexBinval; 	/*! Value hexBinary */
} ParamValue;

/** \brief       Structure for storing a Parameter
 */
typedef struct parameterEntry
{
	ParameterType type;               /*  kind of Parameter see enum ParameterType */
	unsigned int instance;      /* max instance number of instances which were created since dimclient started, default = 0 */
	unsigned int noChilds;           /*  number of children in this object */
	char name[MAX_PARAM_PATH_LENGTH];
	AccessType writeable;             /*  AccessTyp readOnly,writeOnly, readWrite  */
	NotificationType notification;    /*  0 = off ( Default ) 1 = Passive 2 = Active 3 = Allays */
	NotificationMax notificationMax; /*  0 = off ( Default ) 1 = Passive 2 = Active 3 = Allays 4 = NotChangeable by ACS */
	RebootType rebootIdx;        /*  Reboot the System on change of value */
	StatusType status;
	/* Access functions to get / set data value */
	int initDataIdx;
	union
	{
		int getDataIdx; //When ParameterEntry describes the Parameter
		int addObjIdx;  //When ParameterEntry describes the Object. If addObjIdx>0 the callback will be called,
						// if addObjIdx==0 - no any callbacks, if addObjIdx<0 and #ifdef ADD_OBJ_CAN_BE_UNALLOWED the adding in unallowed.
	};
	union
	{
		int setDataIdx; //When ParameterEntry describes the Parameter
		int delObjIdx;	//When ParameterEntry describes the Object. If delObjIdx>0 the callback will be called,
						// if delObjIdx==0 - no any callbacks, if delObjIdx<0 and #ifdef DEL_OBJ_CAN_BE_UNALLOWED the adding in unallowed.
	};

	/* Because we have to store different kind of values
	 * use a union */
	ParamValue value;

	/* index minEntries maxEntries only for MultiObject */
	unsigned int NumberOfMultiObject;
	char minEntries[MAX_ENTRY_PARAM_SIZE];
	char maxEntries[MAX_ENTRY_PARAM_SIZE];
	char *numEntriesParameter; //defines path or name of xxxNumberOfEntries parameter for current MultiObject. If NULL the path will be calculated automatically.

	/* Index in sqllite database */
	unsigned int paramEntryDB_id;

	char **accessList;                   /*! Access list Empty = ACS only */
	int accessListSize;
	/* Build a double linked list */
	struct parameterEntry *prev;
	struct parameterEntry *next;
	struct parameterEntry *parent;
	struct parameterEntry *child;                /*! Only used if type is Object */
	struct parameterEntry *defaultChild;   /*! Link to the default Object for the MultiObject */
	bool isLeaf;                      /*  true = leaf      false = node */
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	unsigned int hasAliasParam;             /*  1 = has   0 = doesn't have */
	char * aliasValue; //For MultiObject it is poiner to Alias value
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */
} ParameterEntry;

/**     Local defined functions        */
static ParameterEntry *findParamInObjectByName (const ParameterEntry *, const char *);
static ParameterEntry *findParameterByPath (const char *);
static ParameterEntry *findDefaultParameterForParamByPath(const char * );
static int traverseParameter (ParameterEntry * , bool , int (*func) (ParameterEntry *, void *), void *);
static ParameterEntry * traverseParameterReverse (ParameterEntry * entry, bool nextLevel, ParameterEntry * (*func) (ParameterEntry *, void *), void *);
static int setParametersAttributesHelper (ParameterEntry *, void *);
static int newParamFromLineDescription (char *, char *);

#if 0
static int newParam2 (char *, char *);
#endif

static int 	addInformParameterValueStructHelper(struct ArrayOfParameterValueStruct* , ParameterEntry*,int *, bool *);
static int  addParameterValueStructHelper(struct ArrayOfParameterValueStruct *, ParameterEntry * , char * , int * );
static int checkArrayOfParameterValueStructOnRepetitions(struct ArrayOfParameterValueStruct * );
static int  addParameterAttributesStructHelper(struct ArrayOfParameterAttributeStruct *, ParameterEntry *, char * , int *);
static void *getVoidPointerToEmptyValue(ParameterType );
static char *getPathName (const ParameterEntry *);
static void getParentPath (ParameterEntry *, char **);
static int 	insertParameter (const char *, ParameterEntry *);
static int 	insertParameterInObject (ParameterEntry *, ParameterEntry *);
static int 	isObject (const char *);
static int 	newParameter( const char *name, ParameterType , int , RebootType, NotificationType, NotificationMax, int, int, int,
						 char *, ParamValue *, const bool, const char *, const char *, const char *);
static ParameterEntry *deleteParameter (ParameterEntry *, void *);
static void clearResourcesInAddObjectInternIfError(ParameterEntry * );
static int 	printParameter (ParameterEntry *, void *);
static int 	printParameterName (ParameterEntry *, void *);
static int 	saveParameter (ParameterEntry *, void *);
static int 	saveMetaParameter (ParameterEntry *, void *);
#if 0//#ifndef WITH_USE_SQLITE_DB
static int 	callInitFunction( ParameterEntry *, void *);
#endif
static int 	callDeleteAccessCallbackFunction( ParameterEntry *, void *);
#if 0
static bool 	compareValue ( ParameterEntry *, void * );
#endif
static void 	freeAccessList (ParameterEntry *);
static int 	copyAccessList (ParameterEntry *, int, char **);
static int 	countPieces (char *);
static int 	copyObject (ParameterEntry *, ParameterEntry *, const char *);
static int 	copyParameters (ParameterEntry *, ParameterEntry *);
static char 	*getName (const char *);
static char 	*getParentName (const char *);
static int 	setAccess2ParamValue (ParameterEntry *, ParameterValue *, unsigned int);
static int 	setValuePtr (ParameterEntry *, void *);
static void 	*getValue (ParameterEntry *);
static char 	*getValueAsString (ParameterEntry * );
static void 	setVoidPtr2Access( const ParameterType, void *, ParameterValue *);
static void   	setParamValue2Access( const ParameterType, ParamValue *, ParameterValue *);
static void 	setAccess2VoidPtr( const ParameterType, void *, ParameterValue *);
static int 	setParameterStatus (ParameterEntry *, void *);
static int 	countChilds (ParameterEntry *);
static int traverseTreeAndCheckPassiveNotification(ParameterEntry *);
static int traverseTreeAndCheckActiveNotification(ParameterEntry *, int *);
static int traverseTreeAndPrint(ParameterEntry *);
static int traverseTreeAndReadMarkedParameters(ParameterEntry * );
static void checkOnAliasParameterAfterSet(ParameterEntry *, char * );
static int checkOnAliasParameterBeforeSet(ParameterEntry *, char * );
static char *getAliasBasedPathName(const ParameterEntry *, const char * );
static ParameterEntry *getNumOfEntriesParamEntry(ParameterEntry *);

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
static void getAliasBasedParentPath(ParameterEntry *, char *, InstanceMode);
static ParameterEntry *autoCreateInstanceForSPV(const char * , unsigned int, unsigned int );
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */

/*Variables to hold the pointer of the parameter list
 */
static ParameterEntry rootEntry;
static ParameterEntry *firstParam = &rootEntry;

// Total number of parameters
static int paramCounter = 0;   
static int paramDBCounter = 0;

// EmptyString as a return value of an empty access array
static char *EMPTY_STRING = "";

extern List postSPVCbList; //extern from callback.c file
extern pthread_mutex_t paramLock;

extern char udp_host[257];

/** Creates a copy of the object named in paramName.
 *  The object must be an multi-instance object ( type = MulitObjectType )
 * 
 * \param paramName name of the multi-instance object, with trailing '.'
 * \param response  pointer to the response
 * 
 * \return  OK or ErrorCode
 * 
 */
int 
addObject (const char *paramName, struct cwmp__AddObjectResponse *response)
{
	int ret = OK;
	unsigned int newInstance = 0;

	if (response == NULL)
		return ERR_INVALID_ARGUMENT;

	// set default return status to '0'
	// this return status can be changed inside the function addObjectIntern() and its subfunctions.
	setParameterReturnStatus(PARAMETER_CHANGES_APPLIED);
	ret = addObjectIntern( paramName, &newInstance, RPC_COMMAND_IS_CALLED_FROM_ACS, CALL_USER_DEFINED_CALLBACK);

	if (ret == OK)
	{
		/* update response
		 */
		response->InstanceNumber = newInstance;
		response->Status = getParameterReturnStatus();
	}

	return ret;
}

/** Add a new object ( a new instance of a existing parameter ) to the parameter list.
 There must be an object exist with the same name it must be of type MultiObjectType
 a deep copy is to be done, means copy all Child parameter of the original Object
 The new value is taken from the default value of the copy source, the access list and the attributes
 are copied one by one

               Find the source parameter. Must exist and be a MultiObjectType
               Get the actual instance number
               Call the initFunction to check the instance number is not out of bounds
               Increment the instance number
               Create a deep copy of all parameters below the old instance number node
               into the new instance number node

	\param paramName       		Name of the object
	\param newInstanceNumber	returns number of added Instance
	\param isCalledFromACS  	can accept values RPC_COMMAND_IS_CALLED_INTERNAL (​​0) or RPC_COMMAND_IS_CALLED_FROM_ACS (1)
	\param callUserDefinedCallback  can accept values 0 or 1. If 1 - user callback with index addObjIdx will be called,
				 else user callback will not be called, but default callback with index=1 will be called if it is defined.
	\return       	OK or ErrorCode
 */
int
addObjectIntern (const char *paramName, unsigned int *newInstanceNumber, unsigned int isCalledFromACS, unsigned int callUserDefinedCallback)
{
	int ret = OK;
	ParameterEntry *parent, *copyEntry, *newEntry;
	char * pathNameOfNewInstance = NULL;
	char buf[80]; //Temp buffer

	if (!paramName || !isObject(paramName))
		return ERR_INVALID_ARGUMENT;

	parent = findParameterByPath (paramName);

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	// Analyze the paramName for format obj1...objN.[ACS Assigned Instance Alias].
	char tmpBuf[MAX_PATH_NAME_SIZE] = {0};
	char tmpAlias[MAX_ALIAS_VALUE_SIZE] = {0};
	if (getAliasBasedAddressing())
	{
		strncpy(tmpBuf, (char *)paramName, MAX_PATH_NAME_SIZE-1);
		int paramNameLen = strlen(tmpBuf);
		if (tmpBuf[paramNameLen-1]=='.'  &&  tmpBuf[paramNameLen-2]==']')
		{
			tmpBuf[paramNameLen-2] ='\0';
			paramNameLen -= 2;
			char * tmp = strrchr(tmpBuf,'.');
			if (!tmp || (paramNameLen-(tmp-tmpBuf) < 3) || (*(tmp+1) != '[') )
				return ERR_INVALID_PARAMETER_NAME; // if path format isn't valid then return error.

			/*If an AddObject request uses an Instance Alias and requests a new Object instance
			 * and the Instance Alias already exists, the CPE MUST return a fault response
			 * with “Invalid Parameter Name” (9005) fault code.
			 * */
			if (parent)
				return ERR_INVALID_PARAMETER_NAME;

			strncpy(tmpAlias, tmp+2, MAX_ALIAS_VALUE_SIZE-1);

			// check is Alias value correct in the pathName.
			if (!isAliasValueValid(tmpAlias))
				return ERR_INVALID_PARAMETER_NAME;

			*(tmp+1) = '\0';
			parent = findParameterByPath (tmpBuf);
			if (!parent || !parent->child || !parent->child->hasAliasParam)
				return ERR_INVALID_PARAMETER_NAME;
		}
	}
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */

	if (!parent || parent->type != MultiObjectType)
		return ERR_INVALID_PARAMETER_NAME;

#ifdef ADD_OBJ_CAN_BE_UNALLOWED
	if (isCalledFromACS == RPC_COMMAND_IS_CALLED_FROM_ACS  &&  parent->addObjIdx < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_PARAMETER, "addObjectIntern(%s): adding of object is unallowed, addObjIdx = %d\n", paramName, parent->addObjIdx);
		)
		return ERR_REQUEST_DENIED;
	}
#endif

	if(strcmp(parent->maxEntries, "unbounded"))
	{
		int isError = 0;
		int maxEntries = a2i(parent->maxEntries, &isError);
		if (isError)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_PARAMETER, "addObjectIntern(%s)->a2i(%s) has returned error.\n", paramName, parent->maxEntries);
			)
			return ERR_REQUEST_DENIED;
		}
		if(parent->NumberOfMultiObject >= maxEntries)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_WARN, DBG_PARAMETER, "addObjectIntern(%s): Limit exceeded: maxEntries = %s\n", paramName, parent->maxEntries);
			)
			return ERR_RESOURCE_EXCEEDED;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "addObjectIntern(): CopyEntry found. parent->name = %s, parent->instance = %d\n", parent->name, parent->instance);
	)

	// Take the defaultChild as the copy instance
	copyEntry = parent->defaultChild; 
	// If no default for copy available search for the exists instance
	ParameterEntry *tmp = parent->child;
	while (!copyEntry && tmp)
	{
		if (tmp->type != ObjectType)
		{
			tmp = tmp->next;
			continue;
		}
		a2ui(tmp->name, &ret);
		if (!ret)
			copyEntry = tmp; // and exit in the while instruction
		else
			tmp = tmp->next;
	}
	if (!copyEntry)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "addObjectIntern(): source instance of the object to be copied is not found.\n", ret );
		)
		return ERR_INTERNAL_ERROR;
	}

	/* print source parameters */
	traverseParameter (copyEntry, true, &printParameterName, NULL);

	// Create the new object name, it's the instance number
	*newInstanceNumber = parent->instance + 1;
	sprintf (buf, "%u", *newInstanceNumber);

	//Create a copy with the new instance number 
	ret = copyObject (copyEntry, parent, buf);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "addObjectIntern()->copyObject() has returned error: %d\n", ret );
		)
		return ret;
	}

	// check, we have create the new object by searching for it
	newEntry = findParamInObjectByName (parent, buf);
	if (newEntry == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "addObjectIntern()->findParamInObjectByName(\"%s\"): "
						"parameter not found.\n",  buf);
		)
		clearResourcesInAddObjectInternIfError(newEntry);
		return ERR_INTERNAL_ERROR;
	}

	// now create a deep copy of the copyEntry into newEntry 
	inAddObject = true;
	ret = copyParameters (copyEntry, newEntry);
	inAddObject = false;
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "addObjectIntern()->copyParameters() has returned error: %d\n", ret );
		)
		clearResourcesInAddObjectInternIfError(newEntry);
		return ret;
	}

	pathNameOfNewInstance = getPathName(newEntry);
	if (!pathNameOfNewInstance)
	{
		clearResourcesInAddObjectInternIfError(newEntry);
		return ERR_RESOURCE_EXCEEDED;
	}

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	if ( newEntry->hasAliasParam )
	{
		sprintf(tmpBuf, "%sAlias\0", pathNameOfNewInstance);
		// if tmpAlias is empty then create new Alias value and set.
		// if tmpAlias isn't empty the try to set Alias value. If setting is fault then create new Alias value and reset.
		if ( (tmpAlias[0]==0) || (ERR_INVALID_PARAMETER_VALUE == setUniversalParamValueInternal( NULL, tmpBuf, 0, tmpAlias)) )
		{
			// if Alias wasn't sent in the addObject command and tmpAlias is empty:
			// Read default Alias Value:
			char * aliasValueFromDB = NULL;
			ret = getParameter(tmpBuf, (void *) &aliasValueFromDB);
			if (ret != OK)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_PARAMETER, "addObjectIntern(%s)->getParameter(%s) has returned error: %d.\n", paramName, tmpBuf, ret);
				)
				clearResourcesInAddObjectInternIfError(newEntry);
				return ret;
			}
			sprintf(tmpAlias, "%s%.*s_%s\0", (isCalledFromACS==RPC_COMMAND_IS_CALLED_FROM_ACS ? "" : "cpe-"),
					MAX_ALIAS_VALUE_SIZE - 2 - strlen(newEntry->name) - (isCalledFromACS==RPC_COMMAND_IS_CALLED_FROM_ACS ? 0 : 4/*len of "cpe-"*/),
					(aliasValueFromDB && *aliasValueFromDB ? aliasValueFromDB : parent->name), newEntry->name);
			// check is Alias value correct in the pathName.
			if (!isAliasValueValid(tmpAlias))
				sprintf(tmpAlias, "cpe-UNDEFINED_%s\0", newEntry->name);
			// Save Alias parameter value for new created instance
			setUniversalParamValueInternal( NULL, tmpBuf, 0, tmpAlias);
		}
	}
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */


	// Call the addObjectAccess function corresponding to the parent->addObjIdx.
	// objectName - name of added instance (number addressing)
	// objectNameAliasBased = name of added instance (alias addressing)
	if (parent->addObjIdx > 0)
	{
		char * pathNameOfNewInstanceAliasBased = NULL;
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
		InstanceMode tmpInstanceMode = getInstanceMode();
		setInstanceMode(InstanceAlias);
		pathNameOfNewInstanceAliasBased = getAliasBasedPathName(newEntry, pathNameOfNewInstance);
		setInstanceMode(tmpInstanceMode);
#else
		pathNameOfNewInstanceAliasBased = pathNameOfNewInstance;
#endif

		ret = addObjectAccess ( (callUserDefinedCallback ? parent->addObjIdx : 1), pathNameOfNewInstance, pathNameOfNewInstanceAliasBased);
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_PARAMETER, "addObjectIntern(%s)->addObjectAccess() has returned error: %d.\n", paramName, ret);
			)
			clearResourcesInAddObjectInternIfError(newEntry);
			return ret;
		}
	}

#ifdef HANDLE_NUMBERS_OF_ENTRIES
	newEntry = getNumOfEntriesParamEntry(parent);
	// Ignore this feature if no NUMBER_OF_ENTRIES Parameter available
	// should not happen but can happen 
	if ( newEntry )
	{
		// because parent is adding 1 for default child we have to decrement
		// the number of children by 1 if parent has a default child entry != NULL.
		// to store the value we need a pointer to the value, therefore use the
		// noChilds from the counter parameter as a temporary buffer
		ret = setUniversalParamValueInternal(newEntry, NULL, 1, (parent->defaultChild ? parent->noChilds - 1 : parent->noChilds));
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_WARN, DBG_PARAMETER, "addObjectIntern(%s)->setUniversalParamValueInternal(%s) has returned error: %d.\n", paramName, newEntry->name, ret);
			)
		}
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_PARAMETER, "addObjectIntern(%s)->getNumOfEntriesParamEntry(): parameter isn't found.\n", paramName);
		)
	}
#endif

	parent->instance = *newInstanceNumber;

	// write parent with the updated instance counter
	ret = saveMetaParameter (parent, NULL);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "addObjectIntern(%s)->saveMetaParameter() has returned error: %d.\n", paramName, ret);
		)
		clearResourcesInAddObjectInternIfError(newEntry);
		return ret;
	}

	/* print the newly created parameters
	 */
	traverseParameter (parent, true, &printParameterName, NULL);

	//number of multi object was incremented in the insertParameterInObject() function.
	//parent->NumberOfMultiObject++;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "addObjectIntern(%s): After instance is added the NumberOfMultiObject = %d.\n", paramName, parent->NumberOfMultiObject);
	)

	return OK;
}

// Delete the object and children in the memory,
// then delete them from SQLite DB
static void clearResourcesInAddObjectInternIfError(ParameterEntry * entryToDelete)
{
#ifdef WITH_USE_SQLITE_DB
	char *pathNameOfInstance = getPathName(entryToDelete); //get pathName before 'entryToDelete' will be freed
#endif
	// delete the object and the children
	traverseParameterReverse (entryToDelete, true, &deleteParameter, NULL);
	entryToDelete = NULL;
#ifdef WITH_USE_SQLITE_DB
	int ret = removeObjectFromDB(pathNameOfInstance);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "clearResourcesInAddObjectInternIfError()->removeObjectFromDB(%s) has returned error %d.\n",
						pathNameOfInstance, ret);
		)
	}
#endif
}

/** Delete object
 *  The object must be an multi-instance object ( type = MulitObjectType )
 *
 * \param paramName name of the multi-instance object, with trailing '.'
 * \param response  pointer to the response
 *
 * \return  OK or ErrorCode
 *
 */
int
deleteObject (const char *paramName, struct cwmp__DeleteObjectResponse *response)
{
	int ret = OK;

	if (response == NULL)
		return ERR_INVALID_ARGUMENT;

	// set default return status to '0'
	// this return status can be changed inside the function deleteObjectIntern() and its subfunctions.
	setParameterReturnStatus(PARAMETER_CHANGES_APPLIED);

	ret = deleteObjectIntern(paramName, RPC_COMMAND_IS_CALLED_FROM_ACS, CALL_USER_DEFINED_CALLBACK);

	response->Status = getParameterReturnStatus();

	return ret;
}


/** Deletes an Object an all it's children. The name must end with a '.'.
 * Before the delete is executed, for the parent and for the deleted object
 * with it children, the delObjectAccess function is called if an delObjIdx is > 0.
 * 
 * \param name	 name of the object to delete
 * \param isCalledFromACS  - can accept values RPC_COMMAND_IS_CALLED_INTERNAL (​​0) or RPC_COMMAND_IS_CALLED_FROM_ACS (1)
 * \param callUserDefinedCallback  can accept values 0 or 1. If 1 - user callback with index delObjIdx will be called,
 *				 else user callback will not be called, but default callback with index=1 will be called if it is defined.
 * \return  OK or ErrorCode
 */
int
deleteObjectIntern(const char *objectName, unsigned int isCalledFromACS, unsigned int callUserDefinedCallback)
{
	int ret = OK;
	ParameterEntry *parent, *entry;
	char * pathNameOfInstanceWithNumberAddressing = NULL;

	if (objectName == NULL)
		return ERR_INVALID_ARGUMENT;
	if ( !isObject(objectName) || (objectName[0]=='.' && objectName[1]=='\0') )
		return ERR_INVALID_PARAMETER_NAME;
	entry = findParameterByPath (objectName);
	if (entry == NULL)
		return ERR_INVALID_PARAMETER_NAME;

	parent = entry->parent;
	if (!parent)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "deleteObjectIntern( %s ): Parent of object isn't found.\n", objectName);
		)
		return ERR_INVALID_PARAMETER_NAME;

	}

	if (entry == firstParam || parent == firstParam)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "deleteObjectIntern( %s ): Root object cannot be deleted.\n", objectName);
		)
		return ERR_INVALID_PARAMETER_NAME;
	}

	if (entry->type != ObjectType || parent->type != MultiObjectType)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "deleteObjectIntern( %s ): Invalid parameter name of object to remove.\n", objectName);
		)
		return ERR_INVALID_PARAMETER_NAME;
	}

#ifdef DEL_OBJ_CAN_BE_UNALLOWED
	if (isCalledFromACS == RPC_COMMAND_IS_CALLED_FROM_ACS  &&  parent->delObjIdx < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_PARAMETER, "deleteObjectIntern( %s ): deleting of object is unallowed, delObjIdx = %d\n", objectName, parent->delObjIdx);
		)
		return ERR_REQUEST_DENIED;
	}
#endif

	if(parent->minEntries && *parent->minEntries)
	{
		int isError = 0;
		unsigned int minEntries = a2ui(parent->minEntries, &isError);
		if (isError)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_PARAMETER, "deleteObjectIntern( %s )->a2ui(%s) has returned error.\n", objectName, parent->minEntries);
			)
			return ERR_REQUEST_DENIED;
		}
		if(parent->NumberOfMultiObject <= minEntries)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_WARN, DBG_PARAMETER, "deleteObjectIntern( %s ): Minimum limit exceeded: minEntries = %s\n", objectName, parent->minEntries);
			)
			return ERR_INVALID_ARGUMENT;
		}
	}

	pathNameOfInstanceWithNumberAddressing = getPathName(entry);
	if (!pathNameOfInstanceWithNumberAddressing)
		return ERR_RESOURCE_EXCEEDED;

	// Call the DeleteAccessFunctions of all Parameters 
	ret = traverseParameter(entry, true, &callDeleteAccessCallbackFunction, &callUserDefinedCallback);
	if (ret != OK)
		return ret;

	// Now delete the object and the children
	traverseParameterReverse (entry, true, &deleteParameter, NULL);
	entry = NULL;

#ifdef WITH_USE_SQLITE_DB
	ret = removeObjectFromDB(pathNameOfInstanceWithNumberAddressing);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "deleteObjectIntern( %s )->removeObjectFromDB(%s) has returned error %d.\n",
						objectName, pathNameOfInstanceWithNumberAddressing, ret);
		)
		return ret;
	}
#endif

	// update the parents child counter
	parent->noChilds --;
#ifdef HANDLE_NUMBERS_OF_ENTRIES
	entry = getNumOfEntriesParamEntry(parent);
	// Ignore this feature if no NUMBER_OF_ENTRIES Parameter available
	// should not happen but can happen 
	if ( entry )
	{
		// because parent is adding 1 for default child we have to decrement
		// the number of children by 1 if parent has a default child entry != NULL.
		// to store the value we need a pointer to the value, therefore use the
		// noChilds from the counter parameter as a temporary buffer
		int ret1 = setUniversalParamValueInternal(entry, NULL, 1, (parent->defaultChild ? parent->noChilds - 1 : parent->noChilds));
		if (ret1 != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_WARN, DBG_PARAMETER, "deleteObjectIntern( %s )->setUniversalParamValueInternal(%s) has returned error: %d.\n", objectName, entry->name, ret1);
			)
		}
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_PARAMETER, "deleteObjectIntern( %s )->getNumOfEntriesParamEntry(): parameter isn't found.\n", objectName);
		)
	}
#endif
	parent->NumberOfMultiObject--;
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "deleteObjectIntern( %s ): After instance is deleted the NumberOfMultiObject = %d.\n", objectName, parent->NumberOfMultiObject);
	)

	return ret;
}


/* This function deletes all subobjects which are the instances of Multi-Object.
 * \param objEntry - pointer to Multi-Object ParameterEntry.
 * \param objectName - full name of Multi-Object.
 * 			You can use or objEntry or objectName argument of this function.
 * \param callUserDefinedCallback  can accept values 0 or 1. If 1 - user callback with index delObjIdx will be called,
 *				 else user callback will not be called, but default callback with index=1 will be called if it is defined.
 * \param mutexLockIsNeeded - is it needed that the 'paramLock' mutex will be locked insade the function.
 * NOTE: when you call this function you MUST decide whether a lock of 'paramLock' mutex is needed?
 * If deleteAllInstancesOfMultiObject() is called from main thread (getters, setters, etc.) during Inform-session
 * then it is not needed to lock the 'paramLock' mutex, because it is already locked in the session.
 * Else, when this function is called from not main threads it is match possible that the locking is needed.
 *
 * Returns error or OK.
 * */

int deleteAllInstancesOfMultiObject(const void * objEntry, const char *objectName, unsigned int callUserDefinedCallback, unsigned int mutexLockIsNeeded)
{

	int ret = OK, ret2 = OK;
	char *tmpname;
	ParameterEntry *tmp = NULL;
	char tmpPathName[MAX_PATH_NAME_SIZE] = {0};
	unsigned int objNameLen = 0;

	if ( !objEntry && (!objectName || !*objectName || !isObject(objectName)) )
		return ERR_INVALID_ARGUMENT;

	if ( objEntry )
		tmp = (ParameterEntry *)objEntry;
	else
	{
		tmp = findParameterByPath(objectName);
		strncpy(tmpPathName, objectName, MAX_PATH_NAME_SIZE-1);
		objNameLen = strlen(tmpPathName);
	}

	if (!tmp || tmp->type != MultiObjectType)
		return ERR_INVALID_ARGUMENT;

	if (!tmpPathName[0])
	{
		strncpy(tmpPathName, getPathName(tmp), MAX_PATH_NAME_SIZE-1);
		objNameLen = strlen(tmpPathName);
	}

	int oldCancelState;
	pthread_testcancel();
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldCancelState);
	if (mutexLockIsNeeded)
		pthread_mutex_lock(&paramLock);


	tmp = tmp->child;
	while (tmp != NULL)
	{
		if (tmp->type == ObjectType) //and != DefaultType or some parameter
		{
			strncpy(&tmpPathName[objNameLen], tmp->name, MAX_PATH_NAME_SIZE - 2 - objNameLen);
			strcat(tmpPathName, ".\0");
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_PARAMETER, "deleteAllInstancesOfMultiObject(): Object instance \"%s\" will be deleted.\n", tmpPathName);
			)
			tmp = tmp->next;
			ret = deleteObjectIntern(tmpPathName, RPC_COMMAND_IS_CALLED_INTERNAL, callUserDefinedCallback);
			DEBUG_OUTPUT (
					dbglog ((ret!=OK ? SVR_ERROR: SVR_INFO), DBG_PARAMETER,
							"deleteAllInstancesOfMultiObject()->deleteObjectIntern(%s): has returned %d\n", tmpPathName, ret);
			)
			ret2 += ret;
		}
		else
			tmp = tmp->next;
	}

	if (mutexLockIsNeeded)
		pthread_mutex_unlock(&paramLock);
	pthread_setcancelstate(oldCancelState, NULL);
	pthread_testcancel();
	return ret2 ? ERR_INTERNAL_ERROR : OK;
}


/* This function deletes all subobjects which are the instances of Multi-Object.
 * Also this function reset counter of Instanses and reset next instance number to 0.
 * \param objEntry - pointer to Multi-Object ParameterEntry.
 * \param objectName - full name of Multi-Object.
 * 			You can use or objEntry or objectName argument of this function.
 * \param callUserDefinedCallback  can accept values 0 or 1. If 1 - user callback with index delObjIdx will be called,
 *				 else user callback will not be called, but default callback with index=1 will be called if it is defined.
 * \param mutexLockIsNeeded - is it needed that the 'paramLock' mutex will be locked insade the function.
 * NOTE: when you call this function you MUST decide whether a lock of 'paramLock' mutex is needed?
 * If deleteAllInstancesOfMultiObject() is called from main thread (getters, setters, etc.) during Inform-session
 * then it is not needed to lock the 'paramLock' mutex, because it is already locked in the session.
 * Else, when this function is called from not main threads it is match possible that the locking is needed.
 *
 * Returns error or OK.
 * */
int deleteAllInstancesOfMultiObjectAndResetNextInstNumber(const void * objEntry, const char *objectName, unsigned int callUserDefinedCallback, unsigned int mutexLockIsNeeded)
{
	int ret = OK, ret2 = OK;
	char *tmpname;
	ParameterEntry *tmp = NULL;
	ParameterEntry *tmpObjEntry = NULL;
	char tmpPathName[MAX_PATH_NAME_SIZE] = {0};
	unsigned int objNameLen = 0;

	if ( !objEntry && (!objectName || !*objectName || !isObject(objectName)) )
		return ERR_INVALID_ARGUMENT;

	if ( objEntry )
		tmp = (ParameterEntry *)objEntry;
	else
	{
		tmp = findParameterByPath(objectName);
		strncpy(tmpPathName, objectName, MAX_PATH_NAME_SIZE-1);
		objNameLen = strlen(tmpPathName);
	}

	if (!tmp || tmp->type != MultiObjectType)
		return ERR_INVALID_ARGUMENT;

	tmpObjEntry = tmp;

	if (!tmpPathName[0])
	{
		strncpy(tmpPathName, getPathName(tmp), MAX_PATH_NAME_SIZE-1);
		objNameLen = strlen(tmpPathName);
	}

	int oldCancelState;
	pthread_testcancel();
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldCancelState);
	if (mutexLockIsNeeded)
		pthread_mutex_lock(&paramLock);


	tmp = tmp->child;
	while (tmp != NULL)
	{
		if (tmp->type == ObjectType) //and != DefaultType or some parameter
		{			
			strncpy(&tmpPathName[objNameLen], tmp->name, MAX_PATH_NAME_SIZE - 2 - objNameLen);
			strcat(tmpPathName, ".\0");
			DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_PARAMETER, "deleteAllInstancesOfMultiObjectAndResetNextInstNumber(): Object instance \"%s\" will be deleted (%d).\n", tmpPathName, tmp->instance);
			)
			tmp = tmp->next;
			ret = deleteObjectIntern(tmpPathName, RPC_COMMAND_IS_CALLED_INTERNAL, callUserDefinedCallback);
			DEBUG_OUTPUT (
					dbglog ((ret!=OK ? SVR_ERROR: SVR_INFO), DBG_PARAMETER,
							"deleteAllInstancesOfMultiObjectAndResetNextInstNumber()->deleteObjectIntern(%s): has returned %d\n", tmpPathName, ret);)

			ret2 += ret;
		}
		else
			tmp = tmp->next;
	}

	if (ret2 == OK)
	{
		tmpObjEntry->instance = 0;
		// write parent with the updated instance counter
		ret = saveMetaParameter (tmpObjEntry, NULL);
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_PARAMETER, "deleteAllInstancesOfMultiObjectAndResetNextInstNumber()->"
							"saveMetaParameter() has returned error: %d.\n", ret);
			)
			ret2 = ret;
		}
	}

	if (mutexLockIsNeeded)
		pthread_mutex_unlock(&paramLock);
	pthread_setcancelstate(oldCancelState, NULL);
	pthread_testcancel();
	return ret2 ? ERR_INTERNAL_ERROR : OK;
}

/** Creates a copy of a parameter and insert it in to the child list of parent.
 The name of the new parameter is taken from dest.
 A '.' is appended to the name if it is not a leaf type.
 The accessList is copied from the src parameter.
 The newly created parameter is stored.

        \param src            	Sourceparameter
        \param parent			Parent of the new created parameter
        \param dest           	Name of the new parameter

        \returns int   			Error code
 */
static int
copyObject (ParameterEntry * src, ParameterEntry * parent, const char *dest)
{
	int ret = OK;
	ParameterType type;
	ParameterEntry *newEntry;
	char buf[MAX_PATH_NAME_SIZE];
	strncpy (buf, getPathName (parent), sizeof(buf) - 1 );
	strncat (buf, dest, sizeof(buf) -strlen(buf) - 1);
	if (src->isLeaf == false)
		strcat (buf, ".");

	if ( dest[0] == '0' )
	{
		// don't change the type of the new object if the new object is a DefaultObject ".0."( dest[0] == '0' )
		type = src->type;
	}
	else
	{
		// change the type of the new parameter if the src is a defaultObject
		// and the new Parameter is not a DefaultObject
		if ( src->type == DefaultType && parent->name[0] != '0' )
		{
			type = ObjectType;
		}
		else if ( IS_DEFAULT_PARAMETER(src) && parent->type != DefaultType )
		{
			type = (src->type - DefaultType);
		}
		else
		{
			type = src->type;
		}
	}

	/* create a new parameter as a copy of the DefaultParameter */
	ret = newParameter (buf, type, src->instance,
			src->rebootIdx, src->notification, src->notificationMax,
			src->initDataIdx, src->getDataIdx,
			src->setDataIdx, NULL, &src->value, true, src->minEntries, src->maxEntries, src->numEntriesParameter);
	if (ret != OK)
		return ret;
	// Search the new parameter 
	newEntry = findParameterByPath (buf);
	if (newEntry == NULL)
	{
		deleteParameter(newEntry, NULL);
		return ERR_INTERNAL_ERROR;
	}
	// copy the access list
	if ((ret = copyAccessList (newEntry, src->accessListSize, src->accessList)) != OK)
	{
		deleteParameter(newEntry, NULL);
		return ret;
	}

	// store the parameter 
	ret = saveParameter (newEntry, NULL);
	if (ret != OK)
	{
		deleteParameter(newEntry, NULL);
		return ret;
	}

	return OK;
}

/**	Copies all Parameters from the src ParameterEntry to the dest ParameterEntry
 * 	It makes a deep copy. src and dest must already exists.
 */
static int
copyParameters (ParameterEntry * src, ParameterEntry * dest)
{
	int ret = OK;
	ParameterEntry *newDest;
	ParameterEntry *tmp = src->child;

	while (tmp != NULL)
	{
		if (tmp->isLeaf)
		{
			// make a copy of tmp into dest
			ret = copyObject (tmp, dest, tmp->name);
			if (ret != OK)
				return ret;
		}
		else
		{
			ret = copyObject (tmp, dest, tmp->name);
			if (ret != OK)
				return ret;
			newDest = findParamInObjectByName (dest, tmp->name);
			ret = copyParameters (tmp, newDest);
			if (ret != OK)
				return ret;
		}
		tmp = tmp->next;
	}

	return ret;
}

/** Count the number of instances of a MultiObject parameter.
 * Don't mix the number of instances with the instance counter up.
 * The instance counter will never decrement.
 * 
 * \param path  MultiObject ParmeterPath
 * \param count pointer to an integer to store the result
 *
 * \return  OK or Error Code
 */
int
countInstances( const char *path, int *count )
{
	int ret = OK;
	int tmpCnt = 0;
	ParameterEntry *ctmp;

	ParameterEntry *pe = findParameterByPath( path );
	if ( pe == NULL )
		return ERR_INVALID_PARAMETER_NAME;
	if ( pe->type != MultiObjectType )
		return ERR_INVALID_PARAMETER_TYPE;

	ctmp = pe->child;
	tmpCnt = 0;
	while ( ctmp != NULL ) {
		// Only instances greater zero counts
		if ( ctmp->name[0] != '0' )
			tmpCnt++;
		ctmp = ctmp->next;
	}
	*count = tmpCnt;
	return ret;
}

/** Update the parameter with metadata gotten from the database
 *  
 *  The following attributes are updated:
 *             notification, instance, access list
 * 
 */
static int
updParameter (const char *path, int instance, NotificationType notification, char *accessList )
{
	int ret = OK;
	int cnt, idx;
	char **array;
	ParameterEntry *updEntry = NULL;

	if ((updEntry = findParameterByPath (path)) != NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_PARAMETER, "UpdEntry: %s %d %d %s\n", path, instance, notification, accessList);
		)
		updEntry->notification &= NotificationAllways;
		updEntry->notification |= notification;
		if (updEntry->instance < instance)
			updEntry->instance = instance;
		updEntry->status = ValueNotChanged;
		if (accessList != NULL && strlen (accessList) > 0)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_PARAMETER, "path: %s access: %s\n", path, accessList);
			)
			cnt = countPieces (accessList);
			//MEM_TYPE_PARAM_ENTRY
			//array = (char **) emalloc ((sizeof (char *)) * cnt);
			array = (char **) emallocTypedMem( (sizeof(char *)) * cnt, MEM_TYPE_SHORT, 0);
			char * accessListTemp = accessList; //strdup(accessList);
			for (idx = 0; idx < cnt; idx++)
			{
				array[idx] = strsep (&accessListTemp, "|");
			}
			copyAccessList (updEntry, cnt, array);
			efreeTypedMemByTypeForCurrentThread(MEM_TYPE_SHORT, 0); // free mem which was allocated in the emallocTypedMem()
			//efree((void**)&array);
			accessListTemp = NULL;
		}
		ret = OK;
	} else
		ret = ERR_INVALID_PARAMETER_NAME;

	return ret;
}

/** (NEW) This function is primarily used to setup the Parameters, normally this Parameters have to be stored in
  a RAM environment, so they don't get lost in the case of a power down or reboot.
 In case there is already a parameter there with the same name and it's a MultiObjectTyp and writable
 a new instance of this parameter is created. 

 In our case the Parameterdata is stored in a file in the flash ROM file system.
        \param name                         Parameter name limited to 256 chars
        \param type                         Kind of parameter 
        \param writeable              access restriction false = read only true = read/write
        \param reboot                if Reboot reboot Hostsystem after all Parameters are set
        \param notification           notification 
        \param notificationMax         maximal notification value settable by ACS
        \param accessList             Access list values separated by '|' or NULL
        \param value                 value of this parameter depends on the type
        \param writeData			 if true store data in data file, otherwise only create parameter
        							 used when loading tmp.param at bootstrap or boot
        							 for bootstrap set writeData to true
        							 for boot set writeData to false
        							 for addObject set writeData to true 

        \return        int                  ErrorCode
 */
static int
newParameter (	const char *path,
				ParameterType type,
				int instance,
				RebootType reboot,
				NotificationType notification,
				NotificationMax notificationMax,
				int initParam,
				int getParam,
				int setParam,
				char *accessList,
				ParamValue *paramValue,
				const bool writeData,
				const char *minEn,
				const char *maxEn,
				const char *numEntriesParameter)
{
	int ret = OK;
	bool isNew = true;
	ParameterEntry *newEntry = NULL;
	ParameterEntry *parentEntry = NULL;
	char *name;
	char *parent;
	char **array;
	register int idx;
	register int cnt;
	ParameterValue accessValue = {0};

	// If parameter already exists return OK
	if ( findParameterByPath(path) != NULL )
		return OK;

	parent = getParentName (path);
	name = getName (path);

	/* check first if parent already exists in our parameter tree
	 * if not try to reload it
	 */
	if (parent != NULL && findParameterByPath (parent) == NULL) {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "newParameter(): Parent not found\n");
		)
		return INFO_PARENT_NOT_FOUND;
	}

	if ((newEntry = findParameterByPath (path)) == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_PARAMETER, "newParameter(): NewEntry:  %s %s %d\n", parent, name, type);
		)
		newEntry = (ParameterEntry *) emallocTypedMem(sizeof (ParameterEntry), MEM_TYPE_PARAM_ENTRY, 0);
		newEntry->type = type;
		strnCopy (newEntry->name, name, strlen (name));

		/* save max and min entry*/
		if(minEn)
			strnCopy (newEntry->minEntries, minEn, strlen(minEn));
		else
			strcpy(newEntry->minEntries, "");

		if(maxEn)
			strnCopy (newEntry->maxEntries, maxEn, strlen(maxEn));
		else
			strcpy(newEntry->maxEntries, "");

		if(numEntriesParameter && *numEntriesParameter)
			newEntry->numEntriesParameter = strnDupByMemType(newEntry->numEntriesParameter, numEntriesParameter, strlen(numEntriesParameter), MEM_TYPE_PARAM_ENTRY );
		else
			newEntry->numEntriesParameter = NULL;

		if (setParam <= -1 && getParam > -1)
			newEntry->writeable = ReadOnly;
		else if (setParam > -1 && getParam <= -1)
			newEntry->writeable = WriteOnly;
		else  if (setParam > -1 && getParam > -1)
			newEntry->writeable = ReadWrite;
		else
			newEntry->writeable = NotReadNotWrite;

		newEntry->next = NULL;
		newEntry->child = NULL;
		newEntry->defaultChild = NULL;
		newEntry->value.cval = NULL;
		if (newEntry->type == MultiObjectType)
			newEntry->NumberOfMultiObject = 0;
		if ( newEntry->type == DefaultType ) {
			parentEntry = findParameterByPath(parent);
			parentEntry->defaultChild = newEntry;
		}
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
		newEntry->hasAliasParam = 0; // this field can be set to 1 in the function insertParameterInObject().
		newEntry->aliasValue = NULL;
#endif
		isNew = true;
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_PARAMETER, "newParameter(): OldEntry %s\n", path);
		)
		isNew = false;
	}

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "newParameter(): Parent %s, Name %s\n", parent, name);
	)
	/* The instance counter is set after this ParameterEntry is inserted in it's parent list
	 */
	if (newEntry->instance < instance)
		newEntry->instance = instance;
	newEntry->accessList = NULL;
	newEntry->accessListSize = 0;
	// Patch old NotificationAllways values to the new one
	if ( notification == 3 )
		notification = NotificationAllways;
	newEntry->notification = notification;
	newEntry->notificationMax = notificationMax;
	newEntry->rebootIdx = reboot;
	newEntry->status = ValueNotChanged;
	newEntry->initDataIdx = initParam;
	newEntry->getDataIdx = getParam;
	newEntry->setDataIdx = setParam;
	newEntry->noChilds = 0;
	if (accessList != NULL && strlen (accessList) > 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_PARAMETER, "newParameter(): path %s, access %s\n", path, accessList);
		)
		cnt = countPieces (accessList);
		array = (char **) emallocTypedMem((sizeof (char *)) * cnt, MEM_TYPE_SHORT, 0);
		char * accessListTemp = accessList;
		for (idx = 0; idx < cnt; idx++)
		{
			array[idx] = strsep (&accessListTemp, "|");
		}
		copyAccessList (newEntry, cnt, array);
		//efree((void**)&array);
		efreeTypedMemByTypeForCurrentThread(MEM_TYPE_SHORT, 0); // free mem which was allocated in the emallocTypedMem()
		accessListTemp = NULL;
	}
	if ( IS_OBJECT(newEntry) )
		newEntry->isLeaf = false;
	else
	{
		newEntry->isLeaf = true;
		if (NO_DEFAULT_PARAMETER(newEntry))
			newEntry->paramEntryDB_id = ++paramDBCounter; /* Save parameterDB_id */
	}

	if (isNew)
	{
		ret = insertParameter (parent, newEntry);
		if (ret)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_PARAMETER, "newParameter()->insertParameter(): has returned error %d. path %s\n", ret, path);
			)

			freeAccessList (newEntry);
			if (newEntry->numEntriesParameter)
				efreeTypedMemByPointer((void**)&newEntry->numEntriesParameter, 0);
			efreeTypedMemByPointer((void**)&newEntry, 0);
		}
	}

	// If the parameter has a setDataIdx > 0 or it is a read only parameter, which has to be
	// stored in the database, and is not a part of a DefaultObject
	// and we are during a bootstrap, make an initial load into the parameter value database.
	//
	// If the parameter has no setDataIdx or is part of a DefaultObject then store
	// the data in dimclient and not in the database.

	if ( NO_DEFAULT_PARAMETER(newEntry)
			&& isNew
			&& newEntry->isLeaf)
	{
		if ( paramBootstrap || inAddObject )
		{
			// If paramBootstrap || inAddObject then it is needed to initialize the parameter. (for example: create parameter in DB)
			// 'value' contains the Default Value that is defined in the data-model:
			setParamValue2Access(newEntry->type, paramValue, &accessValue);
			// check the unique of Alias value only if paramBootstrap==true,
			// because in when parameter added in the object the Alias value will check in the addObjectIntern().
			if ( paramBootstrap && checkOnAliasParameterBeforeSet(newEntry, accessValue.out_cval)!=OK)
			{
				// if Alias isn't unique then create new unique value
				if (accessValue.out_cval)
					efreeTypedMemByPointer((void **)&accessValue.out_cval, 0);
				accessValue.out_cval = emallocTypedMem(6+strlen(newEntry->parent->name), MEM_TYPE_PARAM_ENTRY, 0);
				if (!accessValue.out_cval)
				{
					deleteParameter(newEntry, NULL);
					return ERR_RESOURCE_EXCEEDED;
				}
				sprintf(accessValue.out_cval, "cpe-_%s\0", newEntry->parent->name);
			}
			ret = OK;
			if (initParam > 0)
				ret = initAccess(initParam, path, type, &accessValue);
			if (ret != OK)
			{
				deleteParameter(newEntry, NULL);
				return ERR_RESOURCE_EXCEEDED;
			}
			setAccess2ParamValue(newEntry, &accessValue, MEM_TYPE_PARAM_ENTRY);
			//check on Alias & set Alias value to ParamEntry of parent object:
			checkOnAliasParameterAfterSet(newEntry, accessValue.out_cval);
		}
		else
		{	//If not Bootstrap & not in addObject then after Boot.
#ifdef WITH_USE_SQLITE_DB
			if (initParam > 0)
			{
				//restore parameter Attributes from DB.
				int param_id = 0;
				NotificationType param_notification = NotificationNone;
				ret = retrieveParameterAttributes(&param_id, path, NULL, &param_notification, NULL);
				if (ret == OK)
				{
					newEntry->paramEntryDB_id = param_id;
					if (param_id > paramDBCounter)
						paramDBCounter = param_id;
					newEntry->notification = param_notification;
				}
				DEBUG_OUTPUT (
						dbglog (SVR_INFO, DBG_PARAMETER, "newParameter()->retrieveParameterAttributes() has returned: param_id = %d, paramName = %s, notification = %d, ret = %d\n",
								param_id, path, newEntry->notification, ret);
				)
			}
#endif
			//read value from store-DB after boot to write this value to ParamEntry in the memory
			ret = getAccess( (newEntry->getDataIdx > 1 && newEntry->initDataIdx != MARKED_NODE_FOR_ADDITIONAL_READ? newEntry->getDataIdx : 1),
								path, newEntry->type, &accessValue);
			if (ret != OK)
			{
				// if reading from store-DB is fail then take parameter value from 'value'
				setParamValue2Access(newEntry->type, paramValue, &accessValue);
			}
			setAccess2ParamValue(newEntry, &accessValue, MEM_TYPE_PARAM_ENTRY);
			//check on Alias & set Alias value to ParamEntry of parent object:
			checkOnAliasParameterAfterSet(newEntry, accessValue.out_cval);
		}
	}
	else
	{
		if (IS_DEFAULT_PARAMETER(newEntry))
		{
			setParamValue2Access(newEntry->type, paramValue, &accessValue);
			ret = setAccess2ParamValue( newEntry, &accessValue, MEM_TYPE_PARAM_ENTRY );
		}
		else if (!isNew)
		{//not default parameter and not new Parameter then rewtite value
			setParamValue2Access(newEntry->type, paramValue, &accessValue);
			if ( OK == checkOnAliasParameterBeforeSet(newEntry, accessValue.out_cval) )
			{
				if ( setParam > 1 || initParam > 0 )
					setAccess(setParam, path, type, &accessValue);
				setAccess2ParamValue(newEntry, &accessValue, MEM_TYPE_PARAM_ENTRY);
				//check on Alias & set Alias value to ParamEntry of parent object:
				checkOnAliasParameterAfterSet(newEntry, accessValue.out_cval);
			}
		}
	}

	return OK;
}

/** (NEW) Helper function for getParameterNames
  Recursive calls for child handling if necessary
  Use of traverse() is not possible because we have to count the parameterInfos
        \param parameters      Array of ParameterInfos, defines the input parameter
        \param entry          A parameter
        \param nextLevel       true or false
        \param idx            Ptr to the counter
        \returns int          ErrorCode
 */
int
addParameterInfoStructHelper (struct ArrayOfParameterInfoStruct *parameters, ParameterEntry * entry, bool nextLevel, int *idx)
{
	int ret = OK;
	cwmp__ParameterInfoStruct *pvs;
	ParameterEntry *ctmp;
	/** Allways skip the DefaultObjects & default parameters.
	 */
	if ( entry->type == DefaultType || IS_DEFAULT_PARAMETER(entry))
	{
		return OK;
	}

	pvs = (cwmp__ParameterInfoStruct *) emallocTemp (sizeof (cwmp__ParameterInfoStruct), 15);
	if (pvs == NULL)
		return ERR_RESOURCE_EXCEEDED;
	pvs->Name = getAliasBasedPathName(entry, NULL);
	if (pvs->Name == NULL)
		return ERR_RESOURCE_EXCEEDED;
	pvs->Writable =	((entry->writeable == WriteOnly) || (entry->writeable == ReadWrite)) ? true_ : false_;
	parameters->__ptrParameterInfoStruct[(*idx)++] = pvs;
	/* Go to the next Level if nextLevel == False
	 * or the actual ParemeterEntry is a Object
	 */
	if (nextLevel == false && entry->child != NULL)
	{
		ctmp = entry->child;
		while (ctmp != NULL)
		{
			ret = addParameterInfoStructHelper (parameters, ctmp, nextLevel, idx);
			if (ret != OK)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_PARAMETER, "addParameterInfoStructHelper(%s): ret = %d\n", getPathName(ctmp), ret);
				)
				return ret;
			}
			ctmp = ctmp->next;
		}
	}
	return ret;
}

/** (NEW) Get all parameters, name starting with given name,
 If nextLevel == false, return all Levels
 If true only return the parameters of one Level below
 Example:
           ParameterList:  Top.FirstLevel.2ndLevel.3rdLevel.Leaf
               name = Top.FirstLevel  nextLevel = false  returns Top.FirstLevel.2ndLevel.3rdLevel.Leaf
               name = Top.FirstLevel  nextLevel = true  returns Top.FirstLevel.2ndLevel.
        \param name           pathname to root of the requested parameter names,
                                                  can be a Leaf type or an Object type but no MultiObjectType
        \param nextLevel       if true get only the children, if false get the whole hierarchy below
        \param parameters      Return Structure for the found parameter names, incl. name
        \returns int          Status Code
 */

int
getParameterNames (const xsd__string name, xsd__boolean nextLevel, struct ArrayOfParameterInfoStruct *parameters)
{
	int idx = 0;
	int ret = OK;
	char *tmpname;
	ParameterEntry *tmp, *ctmp = NULL;
	cwmp__ParameterInfoStruct *pvs;

	if ( parameters == NULL)
		return ERR_INVALID_ARGUMENT;
	if ( name != NULL )
		tmpname = (char*)name;
	else
		return ERR_INVALID_ARGUMENT;

	tmp = findParameterByPath (tmpname);
	if (tmp == NULL || tmp->type == DefaultType || IS_DEFAULT_PARAMETER(tmp))
		return ERR_INVALID_PARAMETER_NAME;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "getParameterNames: %s %s\n", tmpname, tmp->name);
	)
	if (tmp->isLeaf == true)
	{
		/* If NextLevel is true and ParameterPath is a Parameter name rather than a Partial Path Name,
		 *  the CPE MUST return a fault response with the Invalid Arguments	fault code (9003).
		 */
		if (nextLevel)
			return ERR_INVALID_ARGUMENT;

		/* Alloc Memory for only 1 Parameter
		 */
		parameters->__ptrParameterInfoStruct =
				(cwmp__ParameterInfoStruct **)
				emallocTemp (sizeof (cwmp__ParameterInfoStruct *), 16);

		if (parameters->__ptrParameterInfoStruct == NULL)
			return ERR_RESOURCE_EXCEEDED;
		pvs = (cwmp__ParameterInfoStruct *)
                    		  emallocTemp (sizeof (cwmp__ParameterInfoStruct), 17);
		if (pvs == NULL)
			return ERR_RESOURCE_EXCEEDED;
		pvs->Name = tmpname;
		pvs->Writable =	( ((tmp->writeable == WriteOnly) || (tmp->writeable == ReadWrite)) ? true_ : false_);
		parameters->__ptrParameterInfoStruct[0] = pvs;
		parameters->__size = 1;
		//isAddedToRespTheRequestedParam = 1;
		return OK;
	}
	else
	{
		// Alloc Memory for all parameter pointers in the parameter list
		parameters->__ptrParameterInfoStruct =
				(cwmp__ParameterInfoStruct **)
				emallocTemp (sizeof (cwmp__ParameterInfoStruct *) * paramCounter, 18);

		if (parameters->__ptrParameterInfoStruct == NULL)
			return ERR_RESOURCE_EXCEEDED;

		//add parameter/object + all children depend on the NextLevel Flag
		ctmp = (nextLevel || tmp==firstParam) ? tmp->child : tmp;
		while (ctmp != NULL)
		{
			ret = addParameterInfoStructHelper (parameters, ctmp, nextLevel, &idx);
			if (ret != OK)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_PARAMETER, "getParameterNames()->addParameterInfoStructHelper(%s): ret = %d\n", getPathName(ctmp), ret);
				)
				return ret;
			}
			if (!nextLevel)
				break; //break in this case after object with his children were added (not go to ctmp->next object)
			ctmp = ctmp->next;
		}
		parameters->__size = idx;

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
		for(idx=0; idx < parameters->__size; idx++)
			parameters->__ptrParameterInfoStruct[idx]->Name = correctParamPathNameInResponse(parameters->__ptrParameterInfoStruct[idx]->Name, tmpname);
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */

		return OK;
	}
}

/**  Helper function for getParameterNames for Inform message
 * We only return Parameters with:
 *  1. Notification equal NotificationAllways
 *  2. Notification is 
 *              ( NotificationActive or NotificationPassive ) 
 *             and the value has modified by the CPE
 *  3. Notification is 
 *              NotificationPassive and the Parameter is ReadOnly and the value has 
 *              changed until the last Inform message call
 *      4. Even if Notification is NotificationAllways DefaultObjects are excepted
 *  
 */
static int
addInformParameterValueStructHelper (	struct ArrayOfParameterValueStruct
										*parameters,
										ParameterEntry * entry,
										int *idx,
										bool * setEventCode)
{
	int ret = OK;
	cwmp__ParameterValueStruct *pvs;
	ParameterEntry *ctmp;
	ParameterValue *accessValue;
	/* Collect all Parameters which has
	 *    ( NotificationPassive + ValueModifiedNotFromACS  or
	 *      NotificationActive  + ValueModifiedNotFromACS  or
	 *      NotificationAllways ) and isLeaf
	 *     and not part of a DefaultObject
	 */
	/* Don't add MultiObjectTypes cause these are only storage for the InstanceNumber
	 */

	if ( entry->isLeaf == true  &&  NO_DEFAULT_PARAMETER(entry)
			&&
			( (entry->notification & NotificationAllways)
					|| (entry->status == ValueModifiedNotFromACS && ((entry->notification & NotificationPassive) || (entry->notification & NotificationActive))
						)
			)
	   )
	{
		pvs = (cwmp__ParameterValueStruct *) emallocTemp (sizeof(cwmp__ParameterValueStruct), 19);
		if (pvs == NULL)
			return ERR_RESOURCE_EXCEEDED;
		pvs->Name = getPathName (entry);
		if (pvs->Name == NULL)
			return ERR_RESOURCE_EXCEEDED;

#ifndef ACS_REGMAN
		pvs->__typeOfValue = entry->type;
#endif
		if ( (entry->writeable == WriteOnly) || (entry->writeable == NotReadNotWrite) || ((entry->initDataIdx <= -1) && (entry->getDataIdx == 1)) )
		{
			// For WriteOnly or NotReadNotWrite parameters send Empty value.
			pvs->Value = getVoidPointerToEmptyValue(entry->type);
		}
		else
		{
			/* No Access function defined, so we take value from the parameter
			 */
			if (entry->getDataIdx == 0)
			{
				pvs->Value = getValue (entry);
				if (!pvs->Value)
					pvs->Value = getVoidPointerToEmptyValue(entry->type);
			}
			else
			{
				accessValue = (ParameterValue*)emallocTemp( sizeof( ParameterValue ), 20);
				setParamValue2Access(entry->type, &entry->value, accessValue);
				/* we have to return a pointer to the parameter value,
				 * therefore we store the value in parameter a
				 * return a pointer to this value.
				 */
				ret = getAccess (entry->getDataIdx, pvs->Name, entry->type, accessValue);
				if (ret != OK)
					return ret;

				setAccess2VoidPtr(entry->type, &pvs->Value, accessValue);

				// Additional check if the notification is passive and
				// the access restriction is read only:
				// has the value changed since the last inform message?
				if ( entry->status == ValueModifiedNotFromACS )
					setAccess2ParamValue(entry, accessValue, MEM_TYPE_PARAM_ENTRY);

			}
		}
		pvs->Name = getAliasBasedPathName(entry, pvs->Name);
		parameters->__ptrParameterValueStruct[(*idx)++] = pvs;
		if (entry->status == ValueModifiedNotFromACS)
			*setEventCode = true;
	}
	/* Go to the next Level if there a more child parameter
	 */
	if (entry->child != NULL)
	{
		ctmp = entry->child;
		while (ctmp != NULL)
		{
			ret = addInformParameterValueStructHelper (parameters, ctmp, idx, setEventCode);
			if (ret != OK)
				return ret;
			ctmp = ctmp->next;
		}
	}

	return ret;
}
/** Returns an array of parameters where the following conditions are true
 Collect all Parameters which has 
               ( NotificationPassive + ValueModifiedNotFromACS  or
                 NotificationActive  + ValueModifiedNotFromACS  or
                 NotificationAllways ) and isLeaf

        The memory for the array is allocated via emallocTemp() 

        \param parameters      Array of Value structs for returning the data
        \returns              OK | ERR_RESOURCE_EXCEEDED
 */
int
getInformParameters (struct ArrayOfParameterValueStruct *parameters)
{
	int ret = OK;
	int idx = 0;
	bool setEventCode = false;
	ParameterEntry *tmp = NULL;
	/* Alloc Memory for all parameter pointers in the parameter list
	 */
	parameters->__ptrParameterValueStruct =
			(cwmp__ParameterValueStruct **)
			emallocTemp (sizeof (cwmp__ParameterValueStruct *) * (paramCounter+2), 21);

	if (parameters->__ptrParameterValueStruct == NULL)
		return ERR_RESOURCE_EXCEEDED;
	/* loop over all parameters and return all parameters which are changed
	 * and has a passive or active notification
	 */
	tmp = firstParam;
	ret = addInformParameterValueStructHelper (parameters, tmp, &idx, &setEventCode);
	if (ret != OK)
		return ret;

	parameters->__size = idx;

	if (setEventCode)
		addEventCodeSingle (EV_VALUE_CHANGE);

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "Number of Informparameters: %d setEventCode: %d\n", idx, setEventCode);
	)

	return OK;
}

/** Reset the Status of the Parameters to ValueNotChanged
 */
static int
setParameterStatus (ParameterEntry * entry, void *arg)
{
	int ret = OK;
	if (entry->status == ValueModifiedNotFromACS)
	{
		entry->status = (StatusType) arg;
#ifndef WITH_USE_SQLITE_DB
		ret = saveParameter (entry, NULL);
#endif
	}

	return ret;
}

void
resetParameterStatus (void)
{
	traverseParameter (firstParam, true, &setParameterStatus, ValueNotChanged);
}

/** Helper function for getParameterNames
 Recursive calls for child handling if necessary
 Use of traverse() is not possible because we have to count the parameterInfos
 */
static int
addParameterValueStructHelper (struct ArrayOfParameterValueStruct *parameters, ParameterEntry * entry, char * nameInRequest, int *idx)
{
	int ret = OK;
	int tmpIdx;
	char *tmpParamPath;
	cwmp__ParameterValueStruct *pvs;
	ParameterEntry *ctmp;
	ParameterValue *accessValue;
	/* Don't add MultiObjectTypes cause these are only storage for the InstanceNumber
	 */
	if (entry->type != MultiObjectType
			&& entry->type != ObjectType
			&& entry->type != DefaultType
			&& entry != firstParam
			&& NO_DEFAULT_PARAMETER(entry))
	{
		// ignore the parameter entry if it is already in the paramaterValueStruct
		// for this, search the list of all the parameters we already found for the new
		// parameter.
		tmpIdx = 0;
		tmpParamPath = getAliasBasedPathName(entry, NULL);
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
		tmpParamPath = correctParamPathNameInResponse( tmpParamPath, nameInRequest);
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */
		if (tmpParamPath == NULL)
			return ERR_RESOURCE_EXCEEDED;
		while( tmpIdx != *idx ) {
			if ( strCmp( parameters->__ptrParameterValueStruct[tmpIdx++]->Name, tmpParamPath) == 1)
			{
				// in this place we can call efreeTypedMemByPointer((void **)&paramPath, 0);
				// if this function isn't called at this place the memory will be freed after current SOAP packet sending.
				return OK;
			}
		}

		pvs = (cwmp__ParameterValueStruct *)
                		   emallocTemp (sizeof (cwmp__ParameterValueStruct), 22);
		if (pvs == NULL)
			return ERR_RESOURCE_EXCEEDED;
		pvs->Name = tmpParamPath;

#ifndef ACS_REGMAN
		if( IS_DEFAULT_PARAMETER(entry) )  // ->type >= DefStringType )
			pvs->__typeOfValue = (entry->type - DefaultType);
		else
			pvs->__typeOfValue = entry->type;
#endif
		// If parameter is WriteOnly then return an empty string
		if ((entry->writeable == WriteOnly) || (entry->writeable == NotReadNotWrite) || ((entry->initDataIdx <= -1) && (entry->getDataIdx == 1)) )
		{
			pvs->Value = getVoidPointerToEmptyValue(entry->type);
		}
		else
		{
			// Don't use access functions for DefaultParameters
			if (entry->getDataIdx == 0 || IS_DEFAULT_PARAMETER(entry))
			{
				pvs->Value = getValue (entry);
				if (!pvs->Value)
					pvs->Value = getVoidPointerToEmptyValue(entry->type);
			}
			else
			{
				accessValue = (ParameterValue*)emallocTemp( sizeof( ParameterValue ), 23);
				setParamValue2Access(entry->type, &entry->value, accessValue);
				// we have to return a pointer therefore we use the entry->value
				// as a buffer for the retrieved parameter value
				//                           )
				//                           value = &entry->value;
				// this may cause an error if the parameter is deleted
				// the memory is freed twice, therefore the pointer has to be cleared after free()

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
				tmpParamPath = getPathName(entry); //get path name without Alias-addressing, for getAccess()
				if (tmpParamPath == NULL)
					return ERR_RESOURCE_EXCEEDED;
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */
				ret = getAccess (entry->getDataIdx, tmpParamPath, entry->type, accessValue);
				if (ret != OK)
					return ret;
				setAccess2VoidPtr(entry->type, &pvs->Value, accessValue);
			}
		}
		parameters->__ptrParameterValueStruct[(*idx)++] = pvs;
		return OK;
	}

	if ((entry->type == MultiObjectType) || (entry->child != NULL))
	{
		ctmp = entry->child;
		while (ctmp != NULL)
		{
			// don't return values with instance is 0 because this are only for creating a new instance
			if ( ctmp->name[0] !=  '0' ) {
				ret = addParameterValueStructHelper (parameters, ctmp, nameInRequest, idx);
				if (ret != OK)
					return ret;
			}
			ctmp = ctmp->next;
		}
	}

	return ret;
}
/** (NEW) Returns an array of parameters identified by name in the array parameters.
 If name is an ObjectType, then all parameters starting with this name are returned.
 The memory for the array is allocated via emallocTemp() 
        \param name            	ParameterNameList
        \param parameters      	Returnlist
        \returns              	OK or ERR_INVALID_PARAMETER_NAME
 */
int
getParameters (const struct ArrayOfString *nameList, struct ArrayOfParameterValueStruct *parameters)
{
	int i = 0;
	int idx = 0;
	int ret = OK;
	int nameArraySize = 0;
	char *name;
	char **nameArray;
	ParameterEntry *tmpEntry = NULL;
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "getParameters(): paramCounter: %d\n", paramCounter);
	)
	if (nameList == NULL || parameters == NULL)
		return ERR_INVALID_ARGUMENT;
	if (nameList->__ptrstring == NULL) // HH 18.2. Sphairon
		return ERR_INVALID_ARGUMENT;
	nameArraySize = nameList->__size;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "getParameters(): nameListe size: %d\n", nameArraySize);
	)
	nameArray = nameList->__ptrstring; // HH 18.2. Sphairon
	/* Alloc Memory for all parameter pointers in the parameter list
	 */
	parameters->__ptrParameterValueStruct =
			(cwmp__ParameterValueStruct **)
			emallocTemp (sizeof (cwmp__ParameterValueStruct *) *paramCounter, 24);
	if (parameters->__ptrParameterValueStruct == NULL)
		return ERR_RESOURCE_EXCEEDED;

	/* loop over all searched names
	 */
	for (i = 0; i != nameArraySize; i++)
	{
		name = (char *) nameArray[i];
		if (!name)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_PARAMETER, "getParameters(): parameterName #%d is NULL in the 'ParameterNames' list.\n", i);
			)
			return ERR_INVALID_PARAMETER_NAME;
		}

		if (strstr(name, ".0.") || (tmpEntry=findParameterByPath(name))==NULL || IS_DEFAULT_PARAMETER(tmpEntry))
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_PARAMETER, "getParameters(): parameter '%s' is not found in the runtime DB.\n", name);
			)
			return ERR_INVALID_PARAMETER_NAME;
		}
		// HH 18.2. Sphairon MultiObjectType is a legal parameter
		//             if (tmpEntry->type == MultiObjectType)
			//                    return ERR_INVALID_PARAMETER_TYPE;
		//              if ( tmpEntry->writeable     == WriteOnly )
		//                      return ERR_WRITEONLY_PARAMETER;
		ret = addParameterValueStructHelper (parameters, tmpEntry, name, &idx);
		if (ret != OK)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_PARAMETER, "getParameters()->addParameterValueStructHelper(%s) has returned error %d.\n", name, ret);
			)
			return ret;
		}
	}
	parameters->__size = idx;

	return OK;
}


/* check array of parameter value structs on repetitions
 * If no repetitions - return OK (0), else return ERR_INVALID_ARGUMENT (9003).
 * */
static int checkArrayOfParameterValueStructOnRepetitions(struct ArrayOfParameterValueStruct *arrayOfPVS)
{
	int i, j, ret = OK;
	if (!arrayOfPVS || !arrayOfPVS->__ptrParameterValueStruct || !arrayOfPVS->__size)
		return OK;

	for (i = 0; i < arrayOfPVS->__size-1; i++)
		for (j = i+1; j < arrayOfPVS->__size; j++)
			if ( strCmp(arrayOfPVS->__ptrParameterValueStruct[i]->Name, arrayOfPVS->__ptrParameterValueStruct[j]->Name) != 0)
			{
				addFault ("SetParameterValuesFault", arrayOfPVS->__ptrParameterValueStruct[i]->Name, ERR_INVALID_ARGUMENT, "Duplication of parameter name is detected");
				ret = ERR_INVALID_ARGUMENT;
				break;
			}

	return ret;
}


/** (NEW) Set all parameters, name starting with given name,
 the host function parameterValueChanged() is called to give the host a change to do the appropriate actions
 There is <b>no</b> need to inform the server of changed Parameter, which are changed by the server.
 \param parameters      Parameter name and value
 \param status         Status of the setting, 
                                           0 = changes have been validated and applied
                                           1 = changes have been validated and committed, but not yet applied.

 \returns int ErrorCode or OK
 */
int
setParameters (struct ArrayOfParameterValueStruct *arrayOfPVS, int *status)
{
	register int i = 0;
	int er, ret = OK;
	int masterRet = OK;
	int paramArraySize = 0;
	char *name, boolchar[2];
	cwmp__ParameterValueStruct **paramArray;
	cwmp__ParameterValueStruct *pvs;
	ParameterEntry *tmp = NULL;
	void *value = NULL;
	int intValue;
	unsigned int uintValue;
	unsigned long ulongValue;
	struct timeval timeValue;
	ParameterValue accessValue = {0};

	// delete formerly created FaultMessage
	clearFault ();

	if ( !arrayOfPVS )
		return ERR_INVALID_ARGUMENT;

	if ( arrayOfPVS->__size <= 0 )
	{
		*status = 0;
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_PARAMETER, "setParameters(): No parameters are in the SPV. Return %d\n", OK);
		)
		return OK;
	}

	if ( !arrayOfPVS->__ptrParameterValueStruct )
		return ERR_INVALID_ARGUMENT;

	paramArraySize = arrayOfPVS->__size;
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "setParameters(): paramlist  size: %d\n", paramArraySize);
	)
	/* Alloc Memory for all parameter pointers in the parameter list
	 */
	paramArray = arrayOfPVS->__ptrParameterValueStruct;
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "setParameters(): parameters->__ptrParameterValueStruct: %p\n",
					arrayOfPVS->__ptrParameterValueStruct);
	)

	// check array of parameter value structs on repetitions:
	if ( (ret = checkArrayOfParameterValueStructOnRepetitions(arrayOfPVS)) != OK )
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "setParameters()->checkArrayOfParameterValueStructOnRepetitions() has returned error %d\n", ret);
		)
		return ret; // ERR_INVALID_ARGUMENT will be returned
	}

	// set default return status to '0'
	// this return status can be changed inside the current function and its subfunctions (including setters).
	// for changing use setParameterReturnStatusSafe()
	setParameterReturnStatus(PARAMETER_CHANGES_APPLIED);

	cleanCallbackList(&postSPVCbList);


	// Loop over the paramArray to check the parameters from parameter list.
	// This cycle is used for checking only, and it does not call setter function.
	for (i = 0; i != paramArraySize; i++)
	{
		pvs = paramArray[i];
		if (!pvs)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_WARN, DBG_PARAMETER, "setParameters(): paramArray[%d] is NULL\n", i);
			)
			continue;
		}
		name = (char *) pvs->Name;

		// If xsi:type attribute isn't defined in the ParameterValueStruct (SPV), then pvs->__typeOfValue == SOAP_TYPE_Value.
		// In this case the value must be handled as String.
		if (pvs->__typeOfValue == SOAP_TYPE_Value)
			pvs->__typeOfValue =  StringType;

		if (isObject(name)) {
			addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_TYPE, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}
		if ( pvs->Value == NULL ) {
			addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_VALUE, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}

		tmp = findParameterByPath (name);
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
		if (tmp == NULL && getAliasBasedAddressing() && getAutoCreateInstances())
		{
			tmp = findDefaultParameterForParamByPath(name);
			if (tmp == NULL) {
				addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_NAME, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
			if ( (pvs->__typeOfValue != tmp->type % DefaultType) && (pvs->__typeOfValue != StringType) ){
				addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_TYPE, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
			if ((tmp->writeable == ReadOnly) || (tmp->writeable == NotReadNotWrite)) {
				addFault ("SetParameterValuesFault", name, ERR_READONLY_PARAMETER, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
		}
		else
		{
			if (tmp == NULL) {
				addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_NAME, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
			if ( (pvs->__typeOfValue != tmp->type) && (pvs->__typeOfValue != StringType) ){
				addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_TYPE, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
			if ((tmp->writeable == ReadOnly) || (tmp->writeable == NotReadNotWrite)) {
				addFault ("SetParameterValuesFault", name, ERR_READONLY_PARAMETER, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
		}
#else
		if (tmp == NULL) {
			addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_NAME, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}
		if ( (pvs->__typeOfValue != tmp->type) && (pvs->__typeOfValue != StringType) ){
			addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_TYPE, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}
		if ((tmp->writeable == ReadOnly) || (tmp->writeable == NotReadNotWrite)) {
			addFault ("SetParameterValuesFault", name, ERR_READONLY_PARAMETER, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */
		/* Strict type checking only with STRICT_TYPE_CHECKING set */
#ifdef	STRICT_TYPE_CHECKING
		// Special handling for boolean type, because of xdr__boolean_ ( Code 20 )
		// which may be set instead of xdr__boolean ( Code 18 )
		if ( tmp->type != SOAP_TYPE_xsd__boolean ) {
#ifndef ACS_REGMAN
			if (pvs->__typeOfValue != tmp->type ) {
				addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_TYPE, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
#endif
		} else {
#ifndef ACS_REGMAN  // Remove the TypeChecking
			if ( pvs->__typeOfValue != tmp->type && pvs->__typeOfValue != SOAP_TYPE_xsd__boolean_ ) {
				addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_TYPE, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
#endif	ACS_REGMAN
		}
#endif	/* STRICT_TYPE_CHECKING */

		// check: is hexBinary value correct
		if ( (tmp->type == hexBinaryType || tmp->type == DefHexBinaryType) && !isHexBinaryValueTrue( (xsd__hexBinary) pvs->Value ) )
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = '%s' is not valid hexBinary value.\n", name, (xsd__hexBinary)pvs->Value );
			)
			addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_VALUE, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}

		ret = checkOnAliasParameterBeforeSet(tmp, (char*)pvs->Value);
		if (ret != OK)
		{
			addFault ("SetParameterValuesFault", name, ret, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}
	} /* for (i = 0; i != paramArraySize; i++) */


	if (masterRet != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "setParameters(): error(s) detected in the ParameterList. Return %d\n", masterRet);
		)
		return masterRet;
	}
	// end of the parameter list checking.
	/***************************************************************************************************************************/

#ifdef WITH_USE_SQLITE_DB
	executeDBCommand(SAVEPOINT);
#endif

	// Loop over the paramArray
	for (i = 0; i != paramArraySize; i++) {
		pvs = paramArray[i];
		name = (char *) pvs->Name;
		/* Can't change a node Object -> ERROR
		 */

		// If xsi:type attribute isn't defined in the ParameterValueStruct (SPV), then pvs->__typeOfValue == SOAP_TYPE_Value.
		// In this case the value must be handled as String.
		if (pvs->__typeOfValue == SOAP_TYPE_Value)
			pvs->__typeOfValue =  StringType;

		if (isObject (name)) {
			addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_TYPE, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}
		if ( pvs->Value == NULL ) {
			addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_VALUE, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}

		tmp = findParameterByPath (name);
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
		if (tmp == NULL && getAliasBasedAddressing() && getAutoCreateInstances())
		{
			tmp = findDefaultParameterForParamByPath(name);
			if (tmp == NULL) {
				addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_NAME, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
			if ( (pvs->__typeOfValue != tmp->type % DefaultType) && (pvs->__typeOfValue != StringType) ){
				addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_TYPE, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
			if ((tmp->writeable == ReadOnly) || (tmp->writeable == NotReadNotWrite)) {
				addFault ("SetParameterValuesFault", name, ERR_READONLY_PARAMETER, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
			tmp = autoCreateInstanceForSPV(name, RPC_COMMAND_IS_CALLED_FROM_ACS, CALL_USER_DEFINED_CALLBACK);
		}
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */

		if (tmp == NULL) {
			addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_NAME, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}
		if ( (pvs->__typeOfValue != tmp->type) && (pvs->__typeOfValue != StringType) ){
			addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_TYPE, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}
		if ((tmp->writeable == ReadOnly) || (tmp->writeable == NotReadNotWrite)) {
			addFault ("SetParameterValuesFault", name, ERR_READONLY_PARAMETER, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}
		/* Strict type checking only with STRICT_TYPE_CHECKING set */
#ifdef	STRICT_TYPE_CHECKING
		// Special handling for boolean type, because of xdr__boolean_ ( Code 20 )
		// which may be set instead of xdr__boolean ( Code 18 )
		if ( tmp->type != SOAP_TYPE_xsd__boolean ) {
#ifndef ACS_REGMAN
			if (pvs->__typeOfValue != tmp->type ) {
				addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_TYPE, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
#endif
		} else {
#ifndef ACS_REGMAN  // Remove the TypeChecking
			if ( pvs->__typeOfValue != tmp->type && pvs->__typeOfValue != SOAP_TYPE_xsd__boolean_ ) {
				addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_TYPE, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
#endif	ACS_REGMAN
		}
#endif	/* STRICT_TYPE_CHECKING */

		/* If input (from ACS) is of type String, check destination type  */
		/* and do conversion here to destination type value               */
		if ( pvs->__typeOfValue == StringType ) {
			int len;
			switch (tmp->type) {
				/* Accept '0|1|f|t|T|F */
				case BooleanType:
				case DefBooleanType:
					/* compiler didn't like (char *) pvs->Value[0]			*/
					/* or (char) pvs->Value[0], so changed to copy a byte		*/
					(void) strncpy(boolchar, pvs->Value, 1);
					if     ( boolchar[0] == 't' || boolchar[0] == 'T' )
						intValue = true;
					else if( boolchar[0] == 'f' || boolchar[0] == 'F' )
						intValue = false;
					else
						intValue = a2i (pvs->Value, NULL) ? true : false;

					DEBUG_OUTPUT (
							dbglog (SVR_DEBUG, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = %d, type = bool\n", name, intValue);
					)

					value = &intValue;
					break;
				case DefIntegerType:
				case IntegerType:
					intValue = a2i (pvs->Value, &er);
					if (er)
					{
						DEBUG_OUTPUT (
								dbglog (SVR_ERROR, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = '%s' is not valid Integer value.\n", name, pvs->Value);
						)
						addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_VALUE, NULL);
						masterRet = ERR_INVALID_ARGUMENT;
						continue;
					}
					DEBUG_OUTPUT (
							dbglog (SVR_DEBUG, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = %d, type = int\n", name, intValue);
					)

					value = &intValue;
					break;
				case DefUnsignedIntType:
				case UnsignedIntType:
					uintValue = a2ui (pvs->Value, &er);
					if (er)
					{
						DEBUG_OUTPUT (
								dbglog (SVR_ERROR, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = '%s' is not valid Unsigned int value.\n", name, pvs->Value);
						)
						addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_VALUE, NULL);
						masterRet = ERR_INVALID_ARGUMENT;
						continue;
					}
					DEBUG_OUTPUT (
							dbglog (SVR_DEBUG, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = %d, type = unsigned int\n", name, uintValue);
					)

					value = &uintValue;
					break;
				case DefStringType:
				case DefBase64Type:
				case StringType:
				case Base64Type:
					value = (void *) pvs->Value;
					len = strlen(value);
					DEBUG_OUTPUT (
							dbglog (SVR_DEBUG, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = %.*s%s, type = str\n", name, (len>40?40:len), value, (len>40?"...":""));
					)
					break;
				case DefDateTimeType:
				case DateTimeType:
					ret = s2dateTime((const char *)pvs->Value, &timeValue);
					if (ret != OK)
					{
						DEBUG_OUTPUT (
								dbglog (SVR_ERROR, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = '%s' is not valid DateTime value.\n", name, pvs->Value);
						)
						addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_VALUE, NULL);
						masterRet = ERR_INVALID_ARGUMENT;
						continue;
					}
					value = &timeValue;
					DEBUG_OUTPUT (
							dbglog (SVR_DEBUG, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = %s, type = dateTime\n", name, pvs->Value);
					)
					break;
				case DefHexBinaryType:
				case hexBinaryType:
					value = (void *) pvs->Value;
					len = strlen(value);
					DEBUG_OUTPUT (
							dbglog (SVR_DEBUG, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = %.*s%s, type = hexBinary\n", name, (len>40?40:len), value, (len>40?"...":""));
					)
					break;
				case DefUnsignedLongType:
				case UnsignedLongType:
					ulongValue = a2ul (pvs->Value, &er);
					if (er)
					{
						DEBUG_OUTPUT (
								dbglog (SVR_ERROR, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = '%s' is not valid Unsigned Long value.\n", name, pvs->Value);
						)
						addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_VALUE, NULL);
						masterRet = ERR_INVALID_ARGUMENT;
						continue;
					}
					DEBUG_OUTPUT (
							dbglog (SVR_DEBUG, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = %d, type = unsigned long\n", name, ulongValue);
					)

					value = &ulongValue;
					break;
				default:
					break;
			} /* switch */

		} else {
			/* Input type != string, therefore convert to	*/
			/* target type later in setValuePtr()			*/
			value = pvs->Value;
		}

		// In this place the 'value' == pointer to the typed variable.
		// For example: if type == string, then value is char *
		// if type == int, then value is int *
		// if type == unsignedInt, then value is unsigned int *

		// check: is hexBinary value correct
		if ( (tmp->type == hexBinaryType || tmp->type == DefHexBinaryType) && !isHexBinaryValueTrue( (xsd__hexBinary)value ) )
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_PARAMETER, "setParameters(): parameter \"%s\", value = '%s' is not valid hexBinary value.\n", name, value);
			)
			addFault ("SetParameterValuesFault", name, ERR_INVALID_PARAMETER_VALUE, NULL);
			masterRet = ERR_INVALID_ARGUMENT;
			continue;
		}

		/* if the setDataIdx == 0 we only store the data an do not call
		 * the host system
		 */
		if (tmp->setDataIdx == 0)
		{
			ret = checkOnAliasParameterBeforeSet(tmp, value);
			if (ret != OK)
			{
				addFault ("SetParameterValuesFault", name, ret, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
			ret = setValuePtr (tmp, value );
			if (ret != OK)
			{
				addFault ("SetParameterValuesFault", name, ret, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
			checkOnAliasParameterAfterSet(tmp, value);
			/* store the parameter in the persistence system */
			ret = saveParameter (tmp, NULL);
			if (ret != OK)
			{
				addFault ("SetParameterValuesFault", name, ret, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
		}
		else
		{
			ret = checkOnAliasParameterBeforeSet(tmp, value);
			if (ret != OK)
			{
				addFault ("SetParameterValuesFault", name, ret, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
			setVoidPtr2Access(tmp->type, value, &accessValue);

			/* inform the host system that a parameter has  changed */
			ret = setAccess (tmp->setDataIdx, name, tmp->type, &accessValue );  // &pvs->Value);
			if (ret != OK)
			{
				addFault ("SetParameterValuesFault", name, ret, NULL);
				masterRet = ERR_INVALID_ARGUMENT;
				continue;
			}
			checkOnAliasParameterAfterSet(tmp, accessValue.out_cval);
		}

		tmp->status = ValueModifiedFromACS;

#ifdef NO_LOCAL_REBOOT_ON_CHANGE
		if (tmp->rebootIdx != NoReboot)
			setParameterReturnStatusSafe(PARAMETER_CHANGES_NOT_APPLIED);
#else
		if (tmp->rebootIdx != NoReboot)
		{
			addRebootIdx (tmp->rebootIdx);
			DEBUG_OUTPUT (
					dbglog (SVR_INFO, DBG_PARAMETER, "setParameters(): parameter \"%s\" -> addRebootIdx(0x%.4X)\n",name, tmp->rebootIdx);
			)
			setParameterReturnStatusSafe(PARAMETER_CHANGES_NOT_APPLIED);
		}
#endif
	} /* for (i = 0; i != paramArraySize; i++) */

	*status = getParameterReturnStatus();

	// Execute the SPV call back functions
	if (masterRet == OK)
	{
		executeSPVCallback(&postSPVCbList);
#ifdef WITH_USE_SQLITE_DB
		executeDBCommand(RELEASE_SAVEPOINT);
#endif
	}
	else
	{
#ifdef WITH_USE_SQLITE_DB
		executeDBCommand(ROLLBACK_TO_SAVEPOINT);
#endif
	}

	cleanCallbackList(&postSPVCbList);

	return masterRet;
}

/** (NEW) The internal version of setParameter.
 No notification is generated if the Parameter has changed.

        \param parameterPath   the complete Parameter name with all parents
        \param value                 pointer to the new value, a copy is made

        \returns ErrorCode     
 */
int
setParameter (const char *parameterPath, void *value)
{
	int ret = OK;
	ParameterValue accessParam = {0};
	ParameterEntry *tmp = NULL;

	if (parameterPath == NULL)
		return ERR_INVALID_ARGUMENT;
	if (isObject (parameterPath))
		return ERR_INVALID_PARAMETER_TYPE;
	tmp = findParameterByPath (parameterPath);
	if (tmp == NULL)
		return ERR_INVALID_ARGUMENT;
	/* if the parameter is read only for the ACS we use the
	 * getDataIdx for accessing the data storage
	 * If the ..DataIdx value == 1 the value is store permanently
	 * If the ..DataIdx value == 0 the value is stored temporarily
	 */

	ret = checkOnAliasParameterBeforeSet(tmp, value);
	if (ret != OK)
		return ret;
	setVoidPtr2Access(tmp->type, value, &accessParam);
	if (tmp->writeable == ReadOnly || tmp->writeable == NotReadNotWrite)
	{
		ret = setAccess (1, parameterPath, tmp->type, &accessParam); // 1 - default setter
		if (ret != OK)
			return ret;
		checkOnAliasParameterAfterSet(tmp, accessParam.out_cval);
	}
	else if ( tmp->setDataIdx > 0 )
	{
		ret = setAccess (tmp->setDataIdx, parameterPath, tmp->type, &accessParam);
		if (ret != OK)
			return ret;
		checkOnAliasParameterAfterSet(tmp, accessParam.out_cval);
	}

	if (tmp->setDataIdx == 0 || tmp->getDataIdx == 0 )
	{
		ret = setAccess2ParamValue (tmp, &accessParam, MEM_TYPE_PARAM_ENTRY);
		if (ret != OK)
			return ret;
		checkOnAliasParameterAfterSet(tmp, accessParam.out_cval);
	}

	if (tmp->rebootIdx != NoReboot)
	{
		addRebootIdx(tmp->rebootIdx);
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_PARAMETER, "setParameter(): \"%s\" -> addRebootIdx(0x%.4X)\n",parameterPath, tmp->rebootIdx);
		)
	}

#ifndef WITH_USE_SQLITE_DB
	/* store the parameter in the file system
	 */
	ret = saveParameter (tmp, NULL);
	if (ret != OK)
		return ret;
#endif

	return OK;
}

/** Get the value of a single parameter into value.
 * There is no way to get the value of an object.
 * The write only status of a parameter is ignored, because this function
 * is only used inside the CPE and the write only status is only for the ACS
 *
        \param parameterPath   Path incl. Name of the Parameter
        \param value                 Pointer to a value object which can hold the value
        \returns int                 ErrorCode
                                                  ERR_INVALID_PARAMETER_TYPE   The parameterPath points to    a Object
                                                  ERR_WRITEONLY_PARAMETER      The Parameter is not readable ( not used here )
                                                  ERR_INVALID_ARGUMENT         the Parameter is unknown
 */
int
getParameter (const char *parameterPath, void *value)
{
	int ret = OK;
	ParameterValue *accessValue;
	ParameterEntry *tmp = NULL;

	if (parameterPath == NULL) {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "getParameter(): \"%s\" err=%d\n",parameterPath, ERR_INVALID_ARGUMENT);
		)
		return ERR_INVALID_ARGUMENT;
	}

	if (isObject (parameterPath)) {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "getParameter->isObject(): \"%s\" err=%d\n",parameterPath, ERR_INVALID_PARAMETER_TYPE);
		)

		return ERR_INVALID_PARAMETER_TYPE;
	}

	tmp = findParameterByPath (parameterPath);

	if (tmp == NULL) {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "getParameter->findParameterByPath(): \"%s\" err=%d\n",parameterPath, ERR_INVALID_ARGUMENT);
		)

		return ERR_INVALID_ARGUMENT;
	}

	/*This causes the memory leak on stun thread. no issue on mainloop, so commented */
//	DEBUG_OUTPUT (
//			dbglog (SVR_INFO, DBG_PARAMETER, "getParameter() Pathname:  %s\n", getPathName (tmp));
//	)

	/* ignore the readOnly flag. It is only for the ACS
	 */
	if (tmp->getDataIdx == 0 || IS_DEFAULT_PARAMETER(tmp) || ( tmp->getDataIdx < 0 && tmp->setDataIdx == 0) )
	{
		switch (tmp->type)
		{
			case DefStringType:
			case DefBase64Type:
			case StringType:
			case Base64Type:
			{
				*(char **) value = tmp->value.cval;
				break;
			}
			case DefUnsignedIntType:
			case UnsignedIntType:
			{
				*(unsigned int **) value = &tmp->value.uival;
				break;
			}
			case DefDateTimeType:
			case DateTimeType:
			{
				*(struct timeval **) value = &tmp->value.tval;
				break;
			}
			case DefUnsignedLongType:
			case UnsignedLongType:
			{
				*(unsigned long **) value = &tmp->value.ulval;
				break;
			}
			case DefHexBinaryType:
			case hexBinaryType:
			{
				*(xsd__hexBinary *) value = tmp->value.hexBinval;
				break;
			}
			default: // HH
				*(int **) value = &tmp->value.ival;
			break;
		}
	}
	else
	{
		accessValue = (ParameterValue*)emallocTemp( sizeof( ParameterValue ), 25);

		if ((tmp->writeable == WriteOnly || tmp->writeable == NotReadNotWrite) && (tmp->getDataIdx == -1))
			ret = getAccess (1, parameterPath, tmp->type, accessValue); // 1 - default getter
		else
			ret = getAccess (tmp->getDataIdx, parameterPath, tmp->type, accessValue);

		if (ret != OK)
			return ret;
		setAccess2VoidPtr(tmp->type, value, accessValue);
	}

	return ret;
}

/** The external version of setParameter.
 * notification is generated if the Parameter has changed.
 *      
 *      \param parameterPath	the complete Parametername with all parents
 *      \param notification		pointer to bool variable which is set to true if notification has to be done
 *      \param value			pointer to the new value, a copy is made
 *       						If value type is DateTime then value MUST be a pointer to string in format "%ld|%ld" ("sec|usec")
 *      \returns ErrorCode     
 */
int
setParameter2Host (const char *parameterPath, bool *notification, char *value)
{
	int ret = OK;
	int er;
	ParameterEntry *param = NULL;
	/* In case of error we send no notification to the ACS */
	if (notification)
		*notification = false;
	if (parameterPath == NULL)
		return ERR_INVALID_ARGUMENT;
	param = findParameterByPath (parameterPath);
	if (param == NULL)
		return ERR_INVALID_ARGUMENT;
	if (IS_OBJECT (param) )
		return ERR_INVALID_PARAMETER_TYPE;

#if 0
	if ((param->writeable == ReadOnly) || (param->writeable == NotReadNotWrite))
		return ERR_READONLY_PARAMETER;
#endif


	// The new value is delivered as a ASCII string
	// Therefore we first convert the string depending the parameter type
	// TODO check access method before storing data in parameter
	switch (param->type)
	{
		case StringType:
		case Base64Type:
			ret = setParameter( parameterPath, value );
			break;
		case BooleanType:
		case IntegerType:
			param->value.ival = a2i (value, &er);
			if (er)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_PARAMETER, "setParameter2Host(): value = '%s' is not valid Integer value.\n",value);
				)
				return ERR_INVALID_PARAMETER_VALUE;
			}
			ret = setParameter( parameterPath, &param->value.ival );
			break;
		case UnsignedIntType:
			param->value.uival = a2ui (value, &er);
			if (er)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_PARAMETER, "setParameter2Host(): value = '%s' is not valid Unsigned int value.\n",value);
				)
				return ERR_INVALID_PARAMETER_VALUE;
			}
			ret = setParameter( parameterPath, &param->value.uival );
			break;
		case DateTimeType:
			sscanf((const char *)value, "%ld|%ld", &param->value.tval.tv_sec, &param->value.tval.tv_usec);
			ret = setParameter( parameterPath, &param->value.tval );
			break;
		case hexBinaryType:
			if (!isHexBinaryValueTrue( (xsd__hexBinary)value) )
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_PARAMETER, "setParameter2Host(): value = '%s' is not valid hexBinary value.\n",value);
				)
				return ERR_INVALID_PARAMETER_VALUE;
			}
			ret = setParameter( parameterPath, value );
			break;
		case UnsignedLongType:
			param->value.ulval = a2ul (value, &er);
			if (er)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_PARAMETER, "setParameter2Host(): value = '%s' is not valid Unsigned Long value.\n",value);
				)
				return ERR_INVALID_PARAMETER_VALUE;
			}
			ret = setParameter( parameterPath, &param->value.ulval );
			break;
		default:
			break;
	}

	if (ret != OK)
		return ret;
	param->status = ValueModifiedNotFromACS;
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "Notify ACS: name = %s notification = %d\n", param->name, param->notification);
	)

	if (notification && (param->notification & NotificationActive) == NotificationActive )
	{
		*notification = true;
	}

	return ret;
}

/** Returns a named parameter which is send to the client host.
 * returns the parameter type in the type variable
 * and the write able value
 * 
 *      \param parameterPath	the complete Parameter name
 * 		\param *type			pointer to returning the parameter type
 *      \param *access			pointer to return the access rights
 *      \param *value			pointer to return the value, do not free this mem pointer
 *      
 *      \returns ErrorCode     
 */
int
getParameter2Host (const char *parameterPath, ParameterType *type, AccessType *access, void *value)
{
	int ret = OK;
	ParameterEntry *param = NULL;

	if (parameterPath == NULL)
		return ERR_INVALID_ARGUMENT;
	param = findParameterByPath (parameterPath);
	if (param == NULL)
		return ERR_INVALID_ARGUMENT;
	if (IS_OBJECT (param))
		return ERR_INVALID_PARAMETER_TYPE;
	if (IS_DEFAULT_PARAMETER(param))
		return ERR_INVALID_PARAMETER_TYPE;
	*access = param->writeable;
	*type = param->type;
	ret = getParameter( parameterPath, value );

	return ret;
}

int
setParametersAttributesHelperGroup (ParameterEntry *param, void *arg)
{
	cwmp__SetParameterAttributesStruct *pas =
			(cwmp__SetParameterAttributesStruct *) arg;

	// skip objects and default parameters
	if ( IS_OBJECT(param) || IS_DEFAULT_PARAMETER(param) )
		return OK;

	if (pas->NotificationChange == true_)
	{
		if ( param->notificationMax == NotificationNotChangeable || pas->Notification > param->notificationMax)
			return ERR_NOTIFICATION_REQ_REJECT;
	}
	if (pas->AccessListChange == true_)
	{
		if (!pas->AccessList || (pas->AccessList->__size > 0 && pas->AccessList->__ptrstring==NULL) )
		{
			return ERR_INVALID_ARGUMENT;
		}
	}

	return OK;
}

/** HelperFunction for setting parameter attributes using the traverse()
        \param entry  Ptr to the first Parameter in the hieratic,
                                    using an Object type means all below the entry
        \param arg           Pointer to the data structure
        \return        int           Error code
 */
static int
setParametersAttributesHelper (ParameterEntry *param, void *arg)
{
	bool isChanged = false;
	int ret = OK;
	char **accessList;
	int cnt;
	cwmp__SetParameterAttributesStruct *pas =
			(cwmp__SetParameterAttributesStruct *) arg;

	if (!pas)
		return ERR_INVALID_ARGUMENT;

	// skip objects and default parameters
	if ( IS_OBJECT(param) || IS_DEFAULT_PARAMETER(param) )
		return OK;

	if (pas->NotificationChange == true_)
	{
		if ( param->notificationMax == NotificationNotChangeable || pas->Notification > param->notificationMax)
		{
			return ERR_NOTIFICATION_REQ_REJECT;
		}
		else
		{
			// clear old notification and preserve NotificationAllways if set
			param->notification &= NotificationAllways;
			param->notification |= pas->Notification;
			isChanged = true;
		}
	}

	if (pas->AccessListChange == true_)
	{
		// pas->AccessList->__ptrstring != NULL, it was checked in the setParametersAttributesHelperGroup()
		accessList = pas->AccessList->__ptrstring; // HH 18.2. Sphairon
		cnt = pas->AccessList->__size;
		ret = OK;
		if ((ret = copyAccessList (param, cnt, accessList)) != OK)
			return ret;
		isChanged = true;
	}
	if (isChanged == true)
	{
		ret = saveMetaParameter(param, arg);
		if (ret != OK)
			return ret;
#ifdef WITH_USE_SQLITE_DB
		if ((pas->NotificationChange == true_) && (pas->AccessListChange == true_))
			ret = updateParameterAttributes(param->paramEntryDB_id, NULL, -1, param->notification, param->accessList, param->accessListSize);
		else
		{
			if (pas->NotificationChange == true_)
				ret = updateParameterAttributes(param->paramEntryDB_id, NULL, -1, param->notification, NULL, -1);
			else
				ret = updateParameterAttributes(param->paramEntryDB_id, NULL, -1, -1, param->accessList, param->accessListSize);
		}
		if (ret != OK)
			return ret;
#endif
	}
	return OK;
}

/** (NEW) Set parameter attributes, name starting with given name,
 the host function parameterValueChanged() is called to give the host a change to do the appropriate actions

 \param nameList       Parameternames
 \param attributes     Returns the attribute values ( Name, Notification and AccessList ) of the named parameter

 \returns int ErrorCode or OK
 */
int
setParametersAttributes (struct ArrayOfSetParameterAttributesStruct *parameters)
{
	register int i = 0;
	int ret = OK;
	int paramArraySize = 0;
	char *name;
	cwmp__SetParameterAttributesStruct **paramArray, *pas;
	ParameterEntry *tmp = NULL;

	if (parameters == NULL
			|| parameters->__ptrSetParameterAttributesStruct == NULL)
		return ERR_INVALID_ARGUMENT;

	paramArraySize = parameters->__size;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "paramlist size: %d\n", paramArraySize);
	)

	paramArray = parameters->__ptrSetParameterAttributesStruct;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "parameters->__ptrParameterValueStruct: %p\n",
					parameters->__ptrSetParameterAttributesStruct);
	)

	for (i = 0; i != paramArraySize; i++)
	{
		pas = paramArray[i];
		if (!pas)
			return ERR_INVALID_ARGUMENT;
		name = (char *) pas->Name;
		tmp = findParameterByPath (name);
		if (tmp == NULL)
			return ERR_INVALID_PARAMETER_NAME;
		ret = traverseParameter (tmp, true, &setParametersAttributesHelperGroup, pas);
		if (ret != OK)
			return ret;
	}

	for (i = 0; i != paramArraySize; i++)
	{
		pas = paramArray[i];
		if (!pas)
			return ERR_INVALID_ARGUMENT;
		name = (char *) pas->Name;
		tmp = findParameterByPath (name);
		if (tmp == NULL)
			return ERR_INVALID_PARAMETER_NAME;
		ret = traverseParameter (tmp, true, &setParametersAttributesHelper, pas);
		if (ret != OK)
			return ret;
	}

	return OK;
}

/** (NEW) Helper function for getParameterAttributes
 Recursive calls for child handling if necessary
 Use of traverse() is not possible because we have to count the parameterInfos
 */
static int
addParameterAttributesStructHelper (struct ArrayOfParameterAttributeStruct *attributes, ParameterEntry *entry, char * nameInRequest, int *idx)
{
	int ret = OK;
	int tmpIdx;
	char *paramPath;
	ParameterEntry *ctmp;

	/* Don't add MultiObjectTypes cause these are only storage for the InstanceNumber
	 */
	if (entry->isLeaf)
	{
		// ignore the parameter entry if it is already in the paramaterValueStruct
		// for this, search the list of all the parameters we already found for the new
		// parameter.
		tmpIdx = 0;
		paramPath = getAliasBasedPathName(entry, NULL);
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
		paramPath = correctParamPathNameInResponse(paramPath, nameInRequest);
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */
		while( tmpIdx != *idx ) {
			if ( strCmp( attributes->__ptrParameterAttributeStruct[tmpIdx++]->Name, paramPath) == 1 )
				return ret;
		}

		cwmp__ParameterAttributeStruct *pas =
				(cwmp__ParameterAttributeStruct *)
				emallocTemp (sizeof (cwmp__ParameterAttributeStruct), 26);
		if (pas == NULL)
			return ERR_RESOURCE_EXCEEDED;
		pas->Name = paramPath;
		if (pas->Name == NULL)
			return ERR_RESOURCE_EXCEEDED;
		// Special handling for notification type: NotificationAllways
		// NotificationAllways is equal to NotificationNone, except it is allays included in
		// an InformMessage
		pas->Notification = entry->notification & ~NotificationAllways;
		pas->AccessList =
				(struct ArrayOfString *)
				emallocTemp (sizeof (struct ArrayOfString), 27);
		if (pas->AccessList == NULL)
			return ERR_RESOURCE_EXCEEDED;
		if (entry->accessList == NULL)
		{
			pas->AccessList->__ptrstring = &EMPTY_STRING; // HH 18.2. Sphairon
			pas->AccessList->__size = entry->accessListSize;
		}
		else
		{
			pas->AccessList->__ptrstring =         // HH 18.2. Sphairon
					entry->accessList;
			pas->AccessList->__size = entry->accessListSize;
		}
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_PARAMETER, "OrigAccessList: %p size %d\n",
						entry->accessList, entry->accessListSize);
		)

		attributes->__ptrParameterAttributeStruct[(*idx)++] = pas;

		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_PARAMETER, "Found: %s\n", pas->Name);
		)
	}

	/* Go to the actual ParemeterEntry if it is a Object with children
	 */
	if (entry->child != NULL && entry->type != DefaultType)
	{
		ctmp = entry->child;
		while (ctmp != NULL)
		{
			ret = addParameterAttributesStructHelper (attributes, ctmp, nameInRequest, idx);
			if (ret != OK)
				return ret;
			ctmp = ctmp->next;
		}
	}

	return ret;
}

/** (NEW) Returns the ParameterAttributes for a list of given parameter names
 Objects are expanded to their leaf children
        \param nameList       Structure with the search Parameter pathnames
        \param attributes     Structure to return the found parameters data
        \return        int                  ErrorCode
 */
int
getParametersAttributes (const struct ArrayOfString *nameList,
		struct ArrayOfParameterAttributeStruct
		*attributes)
{
	int i = 0;
	int idx = 0;
	int ret = OK;
	int nameArraySize = 0;
	char **nameArray;
	char *name;
	ParameterEntry *tmp = NULL;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "getParameterAttributes: paramCounter: %d\n", paramCounter);
	)

	if (nameList == NULL || nameList->__ptrstring == NULL)
		return ERR_INVALID_ARGUMENT;
	nameArraySize = nameList->__size;

	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "nameListe size: %d\n", nameArraySize);
	)
	nameArray = nameList->__ptrstring;
	/* Alloc Memory for all parameters in the name list,
	 * // handles the output parameters
	 */
	attributes->__ptrParameterAttributeStruct =
			(cwmp__ParameterAttributeStruct **)
			emallocTemp (sizeof (cwmp__ParameterAttributeStruct *) *paramCounter, 28);
	if (attributes->__ptrParameterAttributeStruct == NULL)
		return ERR_RESOURCE_EXCEEDED;
	/* loop over all searched names
	 */
	for (i = 0; i != nameArraySize; i++)
	{
		name = (char *) nameArray[i];
		tmp = findParameterByPath (name);
		if (tmp == NULL || tmp->type == DefaultType)
			return ERR_INVALID_PARAMETER_NAME;

		// A Partial PathName MUST end with a “.” (dot) after the last node name in the hierarchy.
		if ( (tmp->type == ObjectType || tmp->type == MultiObjectType) && !isObject(name) )
			return ERR_INVALID_PARAMETER_NAME;

		ret = addParameterAttributesStructHelper (attributes, tmp, name, &idx);
		if (ret != OK)
			return ret;
	}
	attributes->__size = idx;
	return OK;
}

/** (NEW) Helper function to append a Parameter to the parameter list
        \param *path          char * to the complete path of the entry
        \param *newEntry       Ptr to the new Entry.
 */
static int
insertParameter (const char *parent, ParameterEntry *newEntry)
{
	ParameterEntry *parentEntry;
	/* Different handling if parent == 0 because of strtok()
	 */
	if (parent != NULL)
	{
		/* create a copy of parentName before we can use it with strtok()
		 */
		parentEntry = findParameterByPath (parent);
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_PARAMETER, "Insert: %s into: %s\n", newEntry->name, parentEntry->name);
		)
		return insertParameterInObject (parentEntry, newEntry);
	}
	else
	{      /* Insert a Parameter w/o a Parent which means it must be a root Parameter */
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_PARAMETER, "Parent is Null, newEntry: %s\n", newEntry->name);
		)
		parentEntry = firstParam;

		return insertParameterInObject (parentEntry, newEntry);
	}
}

static int
insertParameterInObject (ParameterEntry *object, ParameterEntry *newEntry)
{
	ParameterEntry *tmp;
	unsigned int n;
	int isError = 0;
	tmp = object->child;

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	if (!strcmp(newEntry->name, "Alias"))
		object->hasAliasParam = 1;
#endif

	if (tmp == NULL)
	{
		object->child = newEntry;
		object->child->next = NULL;
		newEntry->parent = object;
		paramCounter++;
		object->noChilds ++;
		if (newEntry->type == ObjectType && newEntry->parent->type == MultiObjectType)
		{
			newEntry->parent->NumberOfMultiObject++;
			n = a2ui(newEntry->name, &isError);
			if (!isError && newEntry->parent->instance < n)
					newEntry->parent->instance = n;
		}
		return OK;
	}
	while (tmp->next != NULL)
	{
		tmp = tmp->next;
	}

	tmp->next = newEntry;
	newEntry->prev = tmp;
	newEntry->parent = object;
	paramCounter++;
	object->noChilds ++;
	if (newEntry->type == ObjectType && newEntry->parent->type == MultiObjectType)
	{
		newEntry->parent->NumberOfMultiObject++;
		n = a2ui(newEntry->name, &isError);
		if (!isError && newEntry->parent->instance < n)
				newEntry->parent->instance = n;
	}
	return OK;
}

/** Get the whole pathname of a ParameterEntry.
 * Objects ends with a '.'
 The allocated Memory is automatic freed 
        \param entry  ParameterEntry
        \return        char * the pathname of entry
 */
static char *
getPathName (const ParameterEntry *entry)
{
	char *pathName = (char *) emallocTemp (MAX_PATH_NAME_SIZE, 29);
	if (!entry || !pathName)
		return NULL;
	memset(pathName, 0, MAX_PATH_NAME_SIZE);

	if (entry->parent != NULL)
		getParentPath (entry->parent, &pathName);
	strcat (pathName, entry->name);
	/* Add an '.' if it is an Object or an MultiObject ( TR: Page 33 ex. NextLevel )
	 */
	if (entry->isLeaf == false)
		strcat (pathName, ".\0");
		
	return pathName;
}

/** Returns the parent path of parameter given in entry.
 The path is returned through path.

        \param entry   Parameter
        \param path    Pointer to a char *
 */
static void
getParentPath (ParameterEntry *entry, char **path)
{
	/* Loop until we back in the first entry, which we ignore 
	 */
	if (entry->parent != NULL)
	{
		getParentPath (entry->parent, path);
		strcat (*path, entry->name);
		strcat (*path, ".");
	}
}

/** Deletes a parameter and its parameter file if existing
        \param entry   Parameter entry to delete
        \param arg     Not used parameter
        \returns ParameterEntry * point to next parameter or NULL (if there is no next parameter )
 */
static ParameterEntry *
deleteParameter (ParameterEntry *entry, void *arg)
{
	int ret = OK;
	ParameterEntry *next = entry->next;
	ParameterEntry *prev = entry->prev;
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "Delete Parameter: %s\n", entry->name);
	)
	if (next != NULL)
		next->prev = prev;
	if (prev != NULL)
		prev->next = next;
	/* If next == prev then all Children are deleted, though we can clear the child list in our parent
	 */
	if (next == prev)
		entry->parent->child = NULL;

	if (IS_CHAR_PTR_TYPE(entry))
	{
		efreeTypedMemByPointer((void**)&entry->value.cval, 0);
	}
	freeAccessList (entry);

	if (entry->numEntriesParameter)
		efreeTypedMemByPointer((void**)&entry->numEntriesParameter, 0);

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	if (entry->aliasValue)
		efreeTypedMemByPointer((void**)&entry->aliasValue, 0);
#endif

	/* delete the parameter file, ignore errors maybe the file may not exist
	 */
#ifndef WITH_USE_SQLITE_DB
	ret = removeParameter (getPathName (entry));
#endif
	efreeTypedMemByPointer((void**)&entry, 0);
	paramCounter--;
	return next;
}

/** Helper function to find a parameterEntry by its pathname
        \param *name                        Name of the searched parameter
        \return        *ParameterEntry      found parameter or NULL
 */
static ParameterEntry *
findParameterByPath (const char *paramPath)
{
	char *ptr = NULL;
	char *parentName;
	char *name;
	char *pathPart;
	ParameterEntry *tmp;
	if (!paramPath)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "findParameterByPath(): function parameter 'paramPath' cannot be NULL.\n");
		)
		return NULL;
	}
	/* check first for special parameter '.' which defines the root of all Parameter
	 */
	if (strlen(paramPath) == 0 || (*paramPath == '.' && strlen(paramPath) == 1))
		return firstParam;
	ptr = NULL;
	tmp = firstParam;
	if (tmp == NULL)
		return NULL;
	parentName = getParentName (paramPath);
	name = getName (paramPath);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "findParameterByPath: %s / %s\n", parentName, name);
	)


	if (parentName != NULL)
	{
		pathPart = strtok_r (parentName, ".", &ptr);
		while (pathPart != NULL)
		{
			tmp = findParamInObjectByName (tmp, pathPart);
			if (tmp == NULL)
				return NULL;
			pathPart = strtok_r (ptr, ".", &ptr);
		}
	}
	tmp = findParamInObjectByName (tmp, name);
	return tmp;
}

/** Helper function to find a parameterEntry which represents an object by its name
        \param *name                        Name of the searched object typed parameter
        \return        *ParameterEntry               found parameter or NULL
 */
static ParameterEntry *
findParamInObjectByName (const ParameterEntry *object, const char *name)
{
	ParameterEntry *tmp;
	if (object == NULL || name == NULL)
		return NULL;

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	char * tmpStr = NULL;
	int isAliasName = 0;
	char tmpName[MAX_PARAM_PATH_LENGTH] = {'\0'};
	isAliasName = (name[0]=='[' && name[strlen(name)-1]==']');
	if (isAliasName && !getAliasBasedAddressing())
		return NULL;
	if (isAliasName)
	{
		strncpy(tmpName, name+1, MAX_PARAM_PATH_LENGTH-1);
		tmpName[strlen(tmpName)-1] = '\0';
	}
	else
		strncpy(tmpName, name, MAX_PARAM_PATH_LENGTH-1);
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */

	tmp = object->child;
	while (tmp != NULL)
	{

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
		tmpStr = (tmp->hasAliasParam && isAliasName) ? tmp->aliasValue : tmp->name;
		if (tmpStr && (strCmp (tmpStr, tmpName) == 1) )
				return tmp;
#else
		if (strCmp (tmp->name, name) == 1)
			return tmp;
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */

		tmp = tmp->next;
	}

	return NULL;
}

/** Helper function for traversing the tree
 * Starting at entry, all objects are visited and a given function is called 
 */
static int
traverseParameter (ParameterEntry * entry, bool nextLevel, int (*func) (ParameterEntry *, void *), void *arg)
{
	int ret = OK;
	ParameterEntry *tmp = entry->child;

	ret = func (entry, arg);
	// if (ret != OK) then don't start the circle WHILE and return ret

	while (ret == OK  &&  tmp != NULL)
	{
		if (tmp->isLeaf)
		{
			ret = func (tmp, arg);
			// if (ret != OK) then exit from WHILE and return ret
		}
		else
		{
			ret = traverseParameter (tmp, nextLevel, func, arg);
			// if (ret != OK) then exit from WHILE and return ret
		}
		tmp = tmp->next;
	}
	return ret;
}

/** Helper function for traversing the tree from bottom up.
 * Starting at entry, all objects are visited and a given function is called 
 * Returns point to the next parameter(or object) in parent entry  or NULL
 */
static ParameterEntry *
traverseParameterReverse (ParameterEntry * entry, bool nextLevel, ParameterEntry * (*func) (ParameterEntry *, void *), void *arg)
{
	int ret = OK;
	ParameterEntry *tmp = entry->child;
	ParameterEntry *next;
	while (tmp != NULL)
	{
		if (tmp->isLeaf)
		{
			tmp = func (tmp, arg); // func is deleteParameter(), returns next parameter in parent object or NULL
		}
		else
		{
			tmp = traverseParameterReverse (tmp, nextLevel, func, arg);
			//Returns point to the next parameter(or object) in parent entry  or NULL
		}
	}
	tmp = func (entry, arg); // func is deleteParameter(), returns next parameter in object or NULL
	return tmp; // return next parameter(or object) in parent object
}

/** Returns the actual number of parameters 
 */
int
getParameterCount (void)
{
	return paramCounter;
}

void
printModifiedParameterList (void)
{
	ParameterEntry *tmp = firstParam;
	while (tmp != NULL)
	{
		if (tmp->status != ValueNotChanged)
			printParameter (tmp, NULL);
		tmp = tmp->next;
	}
}

static int
printParameterName (ParameterEntry * entry, void *arg)
{
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "\n+++ Parameter: %s +++\n", entry->name);
	)
	return OK;
}

static int
printParameter (ParameterEntry * entry, void *arg)
{
	return ERR_INTERNAL_ERROR;
}

/** Copies the values of the accessList into the accessList of the ParameterEntry
 The size of the created accessList is stored in accessListSize

 \param entry  identifies the ParameterEntry
 \param size   the number of entries in the following accessList
 \param accessList     an char * Array of the new accessList

 \return        OK or ERR_RESOURCE_EXCEEDED ( emalloc failed )
 */
static int
copyAccessList (ParameterEntry * entry, int size, char **accessList)
{
	int idx = 0;
	/* check that the accessList of entry is empty, if not free     it,
	 * the accessList is set to NULL
	 */
	if (entry->accessList != NULL)
		freeAccessList (entry);

	if ( size <= 0 || !accessList )
		return OK; // Don't add any new access list entity.

	// only allocate memory if we have to set a new access list
	entry->accessList =	(char **) emallocTypedMem (sizeof(char *)*size, MEM_TYPE_PARAM_ENTRY, 0);
	if (entry->accessList == NULL)
		return ERR_RESOURCE_EXCEEDED;

	// loop over the new accesslist values and create a copy of it
	for (idx = 0; idx < size; idx++)
	{
		entry->accessList[idx] =
			strnDupByMemType(entry->accessList[idx], accessList[idx], strlen(accessList[idx]), MEM_TYPE_PARAM_ENTRY);

		if (entry->accessList[idx] == NULL)
			return ERR_RESOURCE_EXCEEDED;
		(entry->accessListSize)++;
	}
	return OK;
}

/** Frees the accessList entries of a ParameterEntry
 The number of entries are taken from accessListSize

        \param entry   identifies the ParameterEntry
 */
static void
freeAccessList (ParameterEntry * entry)
{
	int idx = 0;
	if (entry->accessList != NULL)
	{
		for (idx = 0; idx != entry->accessListSize; idx++)
		{
			//efree((void**)&entry->accessList[idx]);
			efreeTypedMemByPointer((void**)&entry->accessList[idx], 0);
		}
		//efree((void**)&entry->accessList);
		efreeTypedMemByPointer((void**)&entry->accessList, 0);
		entry->accessList = NULL;
		entry->accessListSize = 0;
	}
}

/** Checks if the given name is the name of an object type.
 * Every object must end with a dot (.)

        \param  name  name of the object
        \returns 1  is an object name
                 0  is not an object name
 */
static int
isObject (const char *name)
{
	if (!name)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "isObject(): function parameter 'name' cannot be NULL.\n");
		)
		return 0;
	}
	if (!(*name))
		return 0;
	return (*(name + strlen (name) - 1) == '.');
}

/** Returns the last part of the path
 path is a list of  '.' separated names

        \param *path  char * to a pathname
        \returns      char* of the last part of the path
 */
static char *
getName (const char *path)
{
	char *tmp = NULL;
	char *name = NULL;

	if (!path)
		return NULL;
	name = strnDupByMemType(name, path, strlen (path), MEM_TYPE_TEMP);  // TODO: it can be optimized
	if (!name)
			return NULL;
	/* If path references an Object then remove the last PathDelimiter
	 */
	if (isObject (name))
	{
		name[strlen(name)-1] = '\0';
	}
	tmp = strrchr (name, '.');
	if (tmp != NULL)
		/* skip Path Delimiter
		 */
		return (tmp + 1); // TODO: it can be optimized
	else
		return name;
}

/** Returns a copy the pathname to the parent, which is path before the last part of the path
 path is a list of '.' separated names

        \param *path  char * to a pathname
        \returns      char* up to the last part of the path or NULL 
 */
static char *
getParentName (const char *path)
{
	//TODO: can be optimized and can return name too. (through the additional function parameter).
	char *tmp, *parentName = NULL;
	if (!path)
		return NULL;
	parentName = strnDupByMemType(parentName, path, strlen (path), MEM_TYPE_TEMP);
	if (!parentName)
		return NULL;
	/* If path references an Object then remove the last PathDelimiter
	 */
	if (isObject (parentName))
	{
		parentName[strlen(parentName)-1] = '\0';
	}
	tmp = strrchr (parentName, '.');
	if (tmp != NULL)
	{
		*tmp = '\0';
		return parentName;
	}
	else
	{
		return NULL;
	}
}
/** Count the number of children in the ParameterEntry
 *  Number of children including Default object (ObjectName.0.) for Multi-Instance Object.
 */
static int
countChilds (ParameterEntry * entry)
{
	register int cnt = 0;
	register ParameterEntry *tmp = entry->child;
	while (tmp != NULL)
	{
		cnt++;
		tmp = tmp->next;
	}
	return cnt;
}

/**     Stores the content of the pointer value in ParameterEntry entry
 */
static int
setAccess2ParamValue (ParameterEntry * entry, ParameterValue *accessValue, unsigned int memTypeIdx)
{
	int ret = OK;

	switch (entry->type)
	{
		case IntegerType:
		case DefIntegerType:
			entry->value.ival = accessValue->out_int;
			break;
		case UnsignedIntType:
		case DefUnsignedIntType:
			entry->value.uival = accessValue->out_uint;
			break;
		case StringType:
		case Base64Type:
		case DefStringType:
		case DefBase64Type:
			if (accessValue->out_cval != NULL)
			{
				if (memTypeIdx == MEM_TYPE_PARAM_ENTRY  &&  entry->value.cval != NULL)
					efreeTypedMemByPointer((void**)&entry->value.cval, 0);
				entry->value.cval = strnDupByMemType (entry->value.cval, accessValue->out_cval,
														strlen(accessValue->out_cval), memTypeIdx);

			}
			break;
		case BooleanType:
		case DefBooleanType:
			entry->value.ival = accessValue->out_int;
			break;
		case DateTimeType:
		case DefDateTimeType:
			entry->value.tval.tv_sec = accessValue->out_timet.tv_sec;
			entry->value.tval.tv_usec = accessValue->out_timet.tv_usec;
			break;
		case UnsignedLongType:
		case DefUnsignedLongType:
			entry->value.ulval = accessValue->out_ulong;
			break;
		case DefHexBinaryType:
		case hexBinaryType:
			if (accessValue->out_hexBin != NULL)
			{
				if (memTypeIdx == MEM_TYPE_PARAM_ENTRY  &&  entry->value.hexBinval != NULL)
					efreeTypedMemByPointer((void**)&entry->value.hexBinval, 0);
				entry->value.hexBinval = strnDupByMemType (entry->value.hexBinval, accessValue->out_hexBin,
														strlen(accessValue->out_hexBin), memTypeIdx);
			}
			break;
		case DefaultType:
		case ObjectType:
		case MultiObjectType:
			ret = OK;
			break;
		default:
			ret = ERR_INVALID_PARAMETER_TYPE;
			break;
	}

	return ret;
}
/**     Stores the content of the pointer value in ParameterEntry entry.
 It cares about the read-write ability of entry.
 If entry is write able the new value is stored in cval which is the value returned by getParameter()
 if entry is read only the new value is stored in defaultValue.
 */
static int
setValuePtr (ParameterEntry * entry, void *value)
{
	if (entry == NULL || value == NULL)
		return ERR_INTERNAL_ERROR;

	if (entry->writeable == ReadWrite || entry->writeable == WriteOnly)
	{
		switch (entry->type)
		{
			case StringType:
			case DefStringType:
			case Base64Type:
			case DefBase64Type:
				if (entry->value.cval)
					efreeTypedMemByPointer((void**)&entry->value.cval, 0);
				entry->value.cval =
						strnDupByMemType(entry->value.cval, (char *) value,
								strlen ((char *) value), MEM_TYPE_PARAM_ENTRY); //MEM_TYPE_TEMP
				if (entry->value.cval == NULL)
					return ERR_INTERNAL_ERROR;
				break;
			case DefBooleanType:
			case BooleanType:
			case DefIntegerType:
			case IntegerType:
				entry->value.ival = *(int *) value;
				break;
			case DefUnsignedIntType:
			case UnsignedIntType:
				entry->value.uival = *(unsigned int *) value;
				break;
			case DefDateTimeType:
			case DateTimeType:
				entry->value.tval.tv_sec = ((struct timeval *)value)->tv_sec;
				entry->value.tval.tv_usec = ((struct timeval *)value)->tv_usec;
				break;
			case DefUnsignedLongType:
			case UnsignedLongType:
				entry->value.ulval = *(unsigned long *) value;
				break;
			case DefHexBinaryType:
			case hexBinaryType:
				if (entry->value.hexBinval)
					efreeTypedMemByPointer((void**)&entry->value.hexBinval, 0);
				entry->value.hexBinval =
						strnDupByMemType(entry->value.hexBinval, (char *) value,
								strlen ((char *) value), MEM_TYPE_PARAM_ENTRY); //MEM_TYPE_TEMP
				if (entry->value.hexBinval == NULL)
					return ERR_INTERNAL_ERROR;
				break;
			default:
				break;
		}
	}
	return OK;
}

/**     Returns        a pointer to the value of entry, 
        depending on the access type, the default value or the stored    value is returned.
 */
static void *
getValue (ParameterEntry * entry)
{
#ifdef ACS_REGMAN
	char *charValue = NULL;
#endif  

	switch (entry->type)
	{
		case DefStringType:
		case StringType:
		case DefBase64Type:
		case Base64Type:
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly )
				return entry->value.cval;
			break;
		case DefBooleanType:
		case BooleanType:
		case DefIntegerType:
		case IntegerType:
#ifdef ACS_REGMAN
			charValue = (char*)emallocTemp(12);
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly)
				sprintf( charValue, "%d", entry->value.ival);
			return charValue;
#else
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly)
				return &entry->value.ival;
#endif
			break;
		case DefUnsignedIntType:
		case UnsignedIntType:
#ifdef ACS_REGMAN
			charValue = (char*)emallocTemp(12);
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly)
				sprintf( charValue, "%u", entry->value.uival);
			return charValue;
#else
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly)
				return &entry->value.uival;
#endif
			break;
		case DefDateTimeType:
		case DateTimeType:
#ifdef ACS_REGMAN
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly)
				charValue = strnDupByMemType( charValue, dateTime2s( entry->value.tval), DATE_TIME_LENGHT, MEM_TYPE_TEMP);
			return charValue;
#else
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly)
				return &entry->value.tval;
#endif
			break;
		case DefUnsignedLongType:
		case UnsignedLongType:
#ifdef ACS_REGMAN
			charValue = (char*)emallocTemp(21);
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly)
				sprintf( charValue, "%lu", entry->value.ulval);
			return charValue;
#else
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly)
				return &entry->value.ulval;
#endif
			break;
		case DefHexBinaryType:
		case hexBinaryType:
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly )
				return entry->value.hexBinval;
			break;
		default:
			break;
	}

	return NULL;
}

/**     Returns the value of a parameter as a char* for storage in a file.
 */
static char *
getValueAsString (ParameterEntry * entry)
{
	char *charValue;

	switch (entry->type)
	{
		case DefStringType:
		case StringType:
		case DefBase64Type:
		case Base64Type:
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly )
				return entry->value.cval;
			break;
		case DefBooleanType:
		case BooleanType:
		case DefIntegerType:
		case IntegerType:
			charValue = (char*)emallocTemp(12, 30);
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly)
				sprintf( charValue, "%d", entry->value.ival);
			return charValue;
			break;
		case DefUnsignedIntType:
		case UnsignedIntType:
			charValue = (char*)emallocTemp(12, 31);
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly)
				sprintf( charValue, "%u", entry->value.uival);
			return charValue;
			break;
		case DefDateTimeType:
		case DateTimeType:
			charValue = (char*)emallocTemp(DATE_TIME_LENGHT, 31);
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly)
				strcpy(charValue, dateTime2s(&(entry->value.tval)) );
			return charValue;
			break;
		case DefUnsignedLongType:
		case UnsignedLongType:
			charValue = (char*)emallocTemp(21, 31);
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly)
				sprintf( charValue, "%lu", entry->value.ulval);
			return charValue;
			break;
		case DefHexBinaryType:
		case hexBinaryType:
			if (entry->writeable == ReadWrite || entry->writeable == ReadOnly )
				return entry->value.hexBinval;
			break;
		default:
			return NULL;
			break;
	}
	return NULL;
}

static void
setVoidPtr2Access (const ParameterType type, void *value, ParameterValue *accessValue)
{
	switch (type)
	{
		case StringType:
		case Base64Type:
		case DefStringType:
		case DefBase64Type:
			accessValue->in_cval = (char*)value;
			break;
		case BooleanType:
		case IntegerType:
		case DefBooleanType:
		case DefIntegerType:
			accessValue->in_int = *(int*)value;
			break;
		case UnsignedIntType:
		case DefUnsignedIntType:
			accessValue->in_uint = *(unsigned int*)value;
			break;
		case DateTimeType:
		case DefDateTimeType:
			accessValue->in_timet.tv_sec = ((struct timeval*)value)->tv_sec;
			accessValue->in_timet.tv_usec = ((struct timeval*)value)->tv_usec;
			break;
		case UnsignedLongType:
		case DefUnsignedLongType:
			accessValue->in_ulong = *(unsigned long*)value;
			break;
		case hexBinaryType:
		case DefHexBinaryType:
			accessValue->in_hexBin = (xsd__hexBinary)value;
			break;
		default:
			break;
	}
}
static void
setParamValue2Access (const ParameterType type, ParamValue *value, ParameterValue *accessValue)
{
	switch (type)
	{
		case StringType:
		case Base64Type:
		case DefStringType:
		case DefBase64Type:
			accessValue->in_cval = value->cval;
			break;
		case BooleanType:
		case IntegerType:
		case DefBooleanType:
		case DefIntegerType:
			accessValue->in_int = value->ival;
			break;
		case UnsignedIntType:
		case DefUnsignedIntType:
			accessValue->in_uint = value->uival;
			break;
		case DateTimeType:
		case DefDateTimeType:
			accessValue->in_timet.tv_sec = value->tval.tv_sec;
			accessValue->in_timet.tv_usec = value->tval.tv_usec;
			break;
		case UnsignedLongType:
		case DefUnsignedLongType:
			accessValue->in_ulong = value->ulval;
			break;
		case hexBinaryType:
		case DefHexBinaryType:
			accessValue->in_hexBin = value->hexBinval;
			break;
		default:
			break;
	}
}

static void
setAccess2VoidPtr (const ParameterType type, void *value, ParameterValue *accessValue)
{
	switch (type)
	{
		case StringType:
		case Base64Type:
		case DefStringType:
		case DefBase64Type:
			*(char**)value = (accessValue->in_cval) ? accessValue->in_cval : EMPTY_STRING;
			break;
		case BooleanType:
		case IntegerType:
		case DefBooleanType:
		case DefIntegerType:
#ifdef ACS_REGMAN
			*(char**)value = (char*)emallocTemp(12);
			sprintf( *(char**)value, "%d", accessValue->out_int);
			break;
#else
			*(int**)value = &accessValue->out_int;
			break;
#endif                 
		case UnsignedIntType:
		case DefUnsignedIntType:
#ifdef ACS_REGMAN
			*(char**)value = (char*)emallocTemp(12);
			sprintf( *(char**)value, "%u", accessValue->out_uint);
			break;
#else
			*(unsigned int **)value = &accessValue->out_uint;
			break;
#endif                      
		case DateTimeType:
		case DefDateTimeType:
#ifdef ACS_REGMAN
			*(char**)value = strnDupTemp( *(char**)value, dateTime2s( accessValue->out_timet), DATE_TIME_LENGHT);
			break;
#else
			*(struct timeval **)value = &accessValue->out_timet;
#endif
			break;
		case UnsignedLongType:
		case DefUnsignedLongType:
#ifdef ACS_REGMAN
			*(char**)value = (char*)emallocTemp(21);
			sprintf( *(char**)value, "%lu", accessValue->out_ulong);
			break;
#else
			*(unsigned long **)value = &accessValue->out_ulong;
			break;
#endif
		case hexBinaryType:
		case DefHexBinaryType:
			*(xsd__hexBinary*)value = (accessValue->in_hexBin) ? accessValue->in_hexBin : EMPTY_STRING;
			break;
		default:
			break;
	}
}

/** Stores all parameter data, without the value data into the database
 *  This function is used if a new parameter is created by an addObject().
 *  The data is also stored if the getIdx/writeIdx = 0 and inAddObject == true
 *  o/w there is no chance to store a default value because there is no data file
 *  
 */
static int
saveParameter (ParameterEntry * entry, void *arg)
{
	int ret = OK;
	int idx, aclSize;
	unsigned int bufSize;
	char * pathName;
	char *strTmp = NULL;
	char * buf;

	if (!entry)
		return ERR_INTERNAL_ERROR;


	// Calculate the size of buffer:
	pathName = getPathName(entry);

	bufSize = strlen(pathName)+105;

	aclSize = entry->accessListSize;
	if ( aclSize > 0)
	{
		for (idx = 0; idx != aclSize; idx++)
			bufSize += strlen(entry->accessList[idx]) + 1;
	}

	// if in addObject and memory data storage is on
	// store any default value in the parameter file
	if ( inAddObject == true  && entry->getDataIdx == 0 )
	{
		strTmp = getValueAsString(entry);
		bufSize += strTmp ? strlen(strTmp) : 0;
	}

	// Memory allocation:
	buf = (char *) emallocTypedMem(bufSize, MEM_TYPE_SHORT, 0);
	if (!buf)
		return ERR_INTERNAL_ERROR;

	// Set the values to buffer:
	ret = sprintf (buf, "%s;%d;%d;%d;%d;%d;%d;%d;%d;",
			pathName, entry->type, entry->instance,
			entry->notification, entry->notificationMax, entry->rebootIdx,
			entry->initDataIdx, entry->getDataIdx,
			entry->setDataIdx);
	aclSize = entry->accessListSize;
	if ( aclSize > 0)
	{
		for (idx = 0; idx != aclSize; idx++)
		{
			strncat( buf, entry->accessList[idx], sizeof(buf) - strlen(buf) - 1 );
			strcat( buf, "|" );
		}
	}
	strcat(buf, ";" );
	// if in addObject and memory data storage is on
	// store any default value in the parameter file
	if ( inAddObject && entry->getDataIdx == 0 )
	{
		char *strTmp = getValueAsString(entry);
		strncat(buf, strTmp ? strTmp : "", sizeof(buf) - strlen(buf) - 1 );
	}

	if (entry->type == MultiObjectType)
	{
		sprintf (buf, "%s;%s;%s;%s", buf, entry->minEntries, entry->maxEntries, (entry->numEntriesParameter?entry->numEntriesParameter:""));
	}
	strcat(buf, "\n" );

	ret = storeParameter( getPathName(entry), buf );

	efreeTypedMemByPointer((void**)&buf, 0);
	return ret;
}

/** Stores only the volatile metadata of a parameter to the database
 */
static int
saveMetaParameter (ParameterEntry * entry, void *arg)
{
	int ret = OK;
	int idx, aclSize;

	char buf[MAX_PATH_NAME_SIZE];
	ret = sprintf (buf, "$;%d;%d;", entry->instance, entry->notification );
	aclSize = entry->accessListSize;
	if ( aclSize > 0)
	{
		for (idx = 0; idx != aclSize; idx++)
		{
			strncat( buf, entry->accessList[idx], MAX_PATH_NAME_SIZE - strlen(buf) - 1 );
			strcat( buf, "|" );
		}
	}
	strcat(buf, ";\n" );

	ret = storeParameter( getPathName(entry), buf );
	return ret;
}

static int
newParamFromLineDescription (char *name, char *data)
{
	int ret = OK;
	char *bufPtr;
	char *path;
	char *accessList;
	ParameterType type;
	int instance;
	NotificationType notification;
	NotificationMax  notificationMax;
	//RebootType reboot;
	unsigned int reboot;
	int initIdx;
	int getIdx;
	int setIdx;
	ParamValue value;
	char * minEntries = NULL;
	char * maxEntries = NULL;
	char * numEntriesParameter = NULL;
	int err = 0;

	// Skip comments or empty lines
	if ( data[0] == '#' || data[0] == ' ' || data[0] == '\n' || data[0] == '\0' )
		return OK;
	if ( data[0] == '$' )
	{
		// read a parameter update set
		bufPtr = data;
		// skip the update set sign character in the first column
		strsep (&bufPtr, ";");
		instance = a2i (strsep (&bufPtr, ";"), NULL);
		notification = a2i (strsep (&bufPtr, ";"), NULL);
		if (!(accessList = strsep (&bufPtr, ";")))
			return ERR_INVALID_PARAM_DESCRIPTION_LINE;

		ret = updParameter( name, instance, notification, accessList );

	}
	else
	{
		// read a complete parameter set
		bufPtr = data;
		if (!(path = strsep (&bufPtr, ";")))
			return ERR_INVALID_PARAM_DESCRIPTION_LINE;
		type = a2i (strsep (&bufPtr, ";"), NULL);
		instance = a2i (strsep (&bufPtr, ";"), NULL);
		notification = a2i (strsep (&bufPtr, ";"), NULL);
		notificationMax = a2i (strsep (&bufPtr, ";"), NULL);
		char * rebootStr;
		if (!(rebootStr = strsep (&bufPtr, ";")))
			return ERR_INVALID_PARAM_DESCRIPTION_LINE;

		if (!strcasecmp(rebootStr, "Reboot"))
			reboot = REBOOT_CALLBACKS_INDEX_SUM;
		else
		{
			reboot = a2ui(rebootStr, &err) & maxRebootIdxValue;
			if (err)
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_PARAMETER, "newParamFromLineDescription(): name=%s, rebootIdx \"%s\" is invalid value. "
								"Will be used value 0.\n", name, rebootStr);
				)
			}
		}
		initIdx = a2i (strsep (&bufPtr, ";"), NULL);
		getIdx = a2i (strsep (&bufPtr, ";"), NULL);
		setIdx = a2i (strsep (&bufPtr, ";"), NULL);
		// for MANAGEMENT_SERVER_URL we set the setIdx to MANAGEMENT_SERVER_URL_SETIDX forcibly (default 120, setManagementServerURL()) - for some functionality work.
		if (!strcmp(path, MANAGEMENT_SERVER_URL))
			setIdx = MANAGEMENT_SERVER_URL_SETIDX;

		if (!(accessList = strsep (&bufPtr, ";")))
			return ERR_INVALID_PARAM_DESCRIPTION_LINE;

		/* if Dimclient was rebooted
		 * and udp_host is not defined in the Dimclient command line
		 * then use udp_host from data-model.xml(tmp.param), or if it is empty use "0.0.0.0"
		 */
		if (!isBootstrap() && !*udp_host && !strcmp(path, UDPCONNECTIONREQUESTADDRESS))
		{
			if (strlen(bufPtr)>0)
				strncpy(udp_host, bufPtr, sizeof(udp_host) - 1);
			else
				strcpy(udp_host, "0.0.0.0");
		}

		switch (type)
		{
			case DefIntegerType:
			case DefBooleanType:
			case IntegerType:
			case BooleanType:
				value.ival = a2i (bufPtr, NULL);
				break;
			case DefUnsignedIntType:
			case UnsignedIntType:
				value.uival = a2ui (bufPtr, NULL);
				break;
			case DefStringType:
			case DefBase64Type:
			case StringType:
			case Base64Type:
				value.cval = bufPtr;
				break;
			case DefDateTimeType:
			case DateTimeType:
				if (strlen(bufPtr) > 0)
					s2dateTime(bufPtr,&(value.tval));
				else
					value.tval.tv_sec = value.tval.tv_usec = 0;
				break;
			case DefUnsignedLongType:
			case UnsignedLongType:
				value.ulval = a2ul(bufPtr, NULL);
				break;
			case DefHexBinaryType:
			case hexBinaryType:
				if ( isHexBinaryValueTrue((xsd__hexBinary)bufPtr) )
					value.hexBinval = bufPtr;
				else
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_PARAMETER, "newParamFromLineDescription(): value = '%s' is not valid hexBinary value. "
									"Will be used Empty String\n",bufPtr);
					)
					value.hexBinval = "";
				}
				break;
			case ObjectType:
			case MultiObjectType:
			case DefaultType:
			default:
				value.cval = "";
				break;
		}
		if(type == MultiObjectType)
		{
			strsep (&bufPtr, ";");
			/* save minEntry and maxEntry */
			if (!(minEntries = strsep (&bufPtr, ";")))
				return ERR_INVALID_PARAM_DESCRIPTION_LINE;
			if (!(maxEntries = strsep (&bufPtr, ";")))
				return ERR_INVALID_PARAM_DESCRIPTION_LINE;
			if (!(numEntriesParameter = strsep (&bufPtr, ";")))
				return ERR_INVALID_PARAM_DESCRIPTION_LINE;
		}

		ret = newParameter (path, type, instance, reboot, notification,notificationMax, initIdx, getIdx, setIdx, accessList, &value, paramBootstrap, minEntries, maxEntries, numEntriesParameter);
	}

	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 2);

	return ret;
}

#if 0
static int
newParam2 (char *name, char *data)
{
	int ret = OK;
	char *bufPtr;
	char *path;
	char *accessList;
	ParameterType type;
	int instance;
	NotificationType notification;
	NotificationMax  notificationMax;
	RebootType reboot;
	int initIdx;
	int getIdx;
	int setIdx;
	ParamValue value;
	char * minEntries = NULL;
	char * maxEntries = NULL;

	// Skip comments or empty lines
	if ( data[0] == '#' || data[0] == ' ' || data[0] == '\n' || data[0] == '\0' )
		return OK;
	if ( data[0] == '$' ){
		// read a parameter update set
		bufPtr = data;
		// skip the update set sign character in the first column
		strsep (&bufPtr, ";");
		instance = a2i (strsep (&bufPtr, ";"));
		notification = a2i (strsep (&bufPtr, ";"));
		accessList = strsep (&bufPtr, ";");
		updParameter( name, instance, notification, accessList );
	} else {
		// read a complete parameter set
		bufPtr = data;
		path = strsep (&bufPtr, ";");
		type = a2i (strsep (&bufPtr, ";"));
		instance = a2i (strsep (&bufPtr, ";"));
		notification = a2i (strsep (&bufPtr, ";"));
		notificationMax = a2i (strsep (&bufPtr, ";"));
		reboot = a2i (strsep (&bufPtr, ";"));
		initIdx = a2i (strsep (&bufPtr, ";"));
		getIdx = a2i (strsep (&bufPtr, ";"));
		setIdx = a2i (strsep (&bufPtr, ";"));
		accessList = strsep (&bufPtr, ";");

		switch (type)
		{
			case DefIntegerType:
			case DefBooleanType:
			case IntegerType:
			case BooleanType:
				value.ival = a2i (bufPtr, NULL);
				break;
			case DefUnsignedIntType:
			case UnsignedIntType:
				value.uival = a2ui (bufPtr, NULL);
				break;
			case DefStringType:
			case DefBase64Type:
			case StringType:
			case Base64Type:
				value.cval = bufPtr;
				break;
			case DefDateTimeType:
			case DateTimeType:
				if (strlen(bufPtr) > 0)
					s2dateTime(bufPtr,&(value.tval));
				else
					value.tval.tv_sec = value.tval.tv_usec = 0;
				break;
			case DefUnsignedLongType:
			case UnsignedLongType:
				value.ulval = a2ul(bufPtr, NULL);
				break;
			case DefHexBinaryType:
			case hexBinaryType:
				if ( isHexBinaryValueTrue((xsd__hexBinary)bufPtr) )
					value.hexBinval = bufPtr;
				else
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_PARAMETER, "newParam2(): value = '%s' is not valid hexBinary value. "
									"Will be used Empty String\n",bufPtr);
					)
					value.hexBinval = "";
				}
				break;
			case ObjectType:
			case MultiObjectType:
			case DefaultType:
			default:
				value.cval = "";
				break;
		}

		if(type == MultiObjectType)
		{
			strsep (&bufPtr, ";");
			/* save minEntry and maxEntry */
			minEntries = strsep (&bufPtr, ";");
			maxEntries = strsep (&bufPtr, ";");
		}

		ret = newParameter (path, type, instance, reboot, notification,notificationMax,
				initIdx, getIdx, setIdx, accessList, &value, paramBootstrap, minEntries, maxEntries);
	}
	//efreeTemp(16);
	efreeTypedMemByTypeForCurrentThread(MEM_TYPE_TEMP, 0);
	return ret;
}
#endif

int
loadParameters (bool bootstrap)
{
	int ret = OK;
	paramBootstrap = bootstrap;

	// Load the initial Parameters with the whole Structure
	ret = loadInitialParameterFile( (newParam *)&newParamFromLineDescription );
	paramBootstrap = false;

	return ret;
}

int
resetAllParameters (void)
{
	int ret = OK;

	ret = removeAllParameters();  // deleteParameters (PERSISTENT_PARAMETER_DIR);

	return ret;
}

/** Counts the pieces in the given string.
 * The pieces are separated by a '|' character. 
 *
 *      \param data           String to split, delimiter is '|'
 *
 *      \returns int   Number of pieces
 */
static int
countPieces (char *data)
{
	register char *ptr;
	register int cnt = 0;
	ptr = data;
	/* count the number of access list entries
	 * separated by the '|'character
	 */
	while (ptr && *ptr != '\0')
	{
		if (*ptr == '|')
			cnt++;
		ptr++;
	}
	cnt++;
	return cnt;
}

#if 0//#ifndef WITH_USE_SQLITE_DB
/** Calls the initAccess function of the parameter in entry.
 * The function is only called if the parameter initDataIdx value is > 0
 * 
 *      \param entry  pointer to parameter
 *      \param param  pointer to parameter value
 * 
 *      \return        int           Return code of the function call
 */
static int
callInitFunction( ParameterEntry *entry, void *param )
{
	int ret = OK;
	if ( entry->initDataIdx > 0)
	{
		ret = initAccess (entry->initDataIdx, getPathName (entry), entry->type, param);
	}
	return ret;
}
#endif

/** Calls the deleteAccess function of the parameter in entry.
 * The function is only called if the parameter initDataIdx value is > 0
 * 
 *      \param entry  pointer to parameter
 *      \param param  pointer to parameter value
 * 
 *      \return        int           Return code of the function call
 */
static int
callDeleteAccessCallbackFunction( ParameterEntry *entry, void *param )
{
	int ret = OK;

	if (!entry)
		return ERR_INTERNAL_ERROR;

	if (entry->isLeaf)
	{
		if ( entry->initDataIdx > 0)
		{
			ret = deleteAccess (entry->initDataIdx, getPathName (entry), entry->type, param);
		}
	}
	else
	{
		// Call the delObjectAccess function corresponding to the parent->delObjIdx.
		// the object is not deleted already
		if (entry->parent->delObjIdx > 0)
		{
			char * pathNameOfInstance = getPathName(entry);
			char * pathNameOfInstanceAliasBased = NULL;
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
			InstanceMode tmpInstanceMode = getInstanceMode();
			setInstanceMode(InstanceAlias);
			pathNameOfInstanceAliasBased = getAliasBasedPathName(entry, pathNameOfInstance);
			setInstanceMode(tmpInstanceMode);
#else
			pathNameOfInstanceAliasBased = pathNameOfInstance;
#endif
			// param in this place is a pointer to unsigned int callUserDefinedCallback value from deleteObjectIntern():
			ret = delObjectAccess ((*((unsigned int*)param) ? entry->parent->delObjIdx : 1), pathNameOfInstance, pathNameOfInstanceAliasBased);
		}
	}


	return ret;
}

#if 0
/** Compare the value of the parameter entry with value
 * San 8 june 2011: Returns TRUE if two parameters are equal, else returns FALSE.
 */
static bool
compareValue ( ParameterEntry *entry, void *value )
{
	if ((entry == NULL)||(value == NULL)) return false;
	switch( entry->type ) {
		case DefIntegerType:
		case IntegerType:
		case BooleanType:
		case DefBooleanType:
			return entry->value.ival == INT_SET value;
			break;
		case UnsignedIntType:
		case DefUnsignedIntType:
			return entry->value.uival == UINT_SET value;
			break;
		case StringType:
		case DefStringType:
		case Base64Type:
		case DefBase64Type:
		{
			if((entry->value.cval == NULL)&&((STRING_GET value) == NULL)) // If two values are NULL, that return TRUE
					return true;
			if((entry->value.cval == NULL)||((STRING_GET value) == NULL)) // If one from two values are NULL, that return FALSE
					return false;
			return (strCmp( entry->value.cval,  STRING_GET value) == 1);
			break;
		}
		case DateTimeType:
		case DefDateTimeType:
			return entry->value.tval == TIME_SET value;
			break;
		default:
			break;
	}

	return false;
}
#endif

static int
traverseTreeAndCheckPassiveNotification(ParameterEntry * entry)
{
	int ret = OK;
	ParameterEntry *tmp = entry->child;
	while (tmp != NULL)
	{
		if (tmp->isLeaf)
		{
			char *strFromGetter = NULL;
			int * intValueFromGetter = NULL;
			unsigned int * uintValueFromGetter = NULL;
			unsigned long * ulongValueFromGetter = NULL;
			struct timeval * timeValueFromGetter = NULL;
			xsd__hexBinary hexBinFromGetter = NULL;

			/* find parameter with passive notification */
			if (((tmp->notification & NotificationPassive) == NotificationPassive) && (tmp->getDataIdx != 1))
			{
				ParameterValue tmpValue;
				if ((ret = retrieveParamValue(getPathName(tmp), tmp->type, &tmpValue)) != OK)
				{
					return ret;
				}

				/* Call getter function */
				if( (tmp->type == UnsignedIntType) ||
						(tmp->type == DefUnsignedIntType)
				)
				{
					/* use uintValueFromGetter */
					if(getParameter(getPathName(tmp), &uintValueFromGetter) == OK)
					{
						if(uintValueFromGetter != NULL)
						{
							if (tmpValue.out_uint != *uintValueFromGetter)
							{
								/* yes, value is different */
								setUniversalParamValueInternal(tmp, NULL, 1, *uintValueFromGetter);
							}
						}
					}
				}
				if( (tmp->type == DefIntegerType) ||
						(tmp->type == IntegerType ) ||
						(tmp->type == BooleanType) ||
						(tmp->type == DefBooleanType))
				{
					/* use intValueFromGetter */
					if(getParameter(getPathName(tmp), &intValueFromGetter) == OK)
					{
						if(intValueFromGetter != NULL)
						{
							if (tmpValue.out_int != *intValueFromGetter)
							{
								/* yes, value is different */
								setUniversalParamValueInternal(tmp, NULL, 1, *intValueFromGetter);
							}
						}
					}
				}
				if( (tmp->type == DateTimeType) ||
						(tmp->type == DefDateTimeType) )
				{
					/* use timeValueFromGetter */
					if(getParameter(getPathName(tmp), &timeValueFromGetter) == OK)
					{
						if(timeValueFromGetter != NULL)
						{
							if( (tmpValue.out_timet.tv_sec != timeValueFromGetter->tv_sec) ||
								(tmpValue.out_timet.tv_usec != timeValueFromGetter->tv_usec) )
							{
								/* yes, value is different */
								setUniversalParamValueInternal(tmp, NULL, 1, timeValueFromGetter);
							}
						}
					}
				}

				if( (tmp->type == StringType) ||
						(tmp->type == DefStringType) ||
						(tmp->type == Base64Type) ||
						(tmp->type == DefBase64Type) )
				{
					/* use strFromGetter */
					if(getParameter (getPathName(tmp), &strFromGetter) == OK)
					{
						if(strFromGetter != NULL)
						{
							if (strCmp( tmpValue.out_cval, strFromGetter) != 1) // not equal.
							{
								/* yes, value is different */
								setUniversalParamValueInternal(tmp, NULL, 1, strFromGetter);
							}
						}
					}
				}
				if( (tmp->type == UnsignedLongType) ||
						(tmp->type == DefUnsignedLongType)
				)
				{
					/* use ulongValueFromGetter */
					if(getParameter(getPathName(tmp), &ulongValueFromGetter) == OK)
					{
						if(ulongValueFromGetter != NULL)
						{
							if (tmpValue.out_ulong != *ulongValueFromGetter)
							{
								/* yes, value is different */
								setUniversalParamValueInternal(tmp, NULL, 1, *ulongValueFromGetter);
							}
						}
					}
				}
				if( (tmp->type == hexBinaryType) ||
						(tmp->type == DefHexBinaryType))
				{
					/* use hexBinFromGetter */
					if(getParameter (getPathName(tmp), &hexBinFromGetter) == OK)
					{
						if(hexBinFromGetter != NULL)
						{
							if (strCmp( tmpValue.out_hexBin, hexBinFromGetter) != 1) // not equal.
							{
								/* yes, value is different */
								setUniversalParamValueInternal(tmp, NULL, 1, hexBinFromGetter);
							}
						}
					}
				}
			}
		}  // end  if (tmp->isLeaf)
		else
			traverseTreeAndCheckPassiveNotification(tmp);
		tmp = tmp->next;
	} // end while (tmp != NULL)
	return ret;
}

void
checkPassiveNotification()
{
	ParameterEntry *pEntry = &rootEntry;
	if(!pEntry)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "checkPassiveNotification() error: rootEntry == NULL.\n");
		)
		return;
	}
	traverseTreeAndCheckPassiveNotification(pEntry);
}

static int
traverseTreeAndCheckActiveNotification(ParameterEntry * entry, int *sendInformFlag)
{
	int ret = OK;
	ParameterEntry *tmp = entry->child;
	while (tmp != NULL)
	{
		if (tmp->isLeaf)
		{
			char *strFromGetter = NULL;
			int * intValueFromGetter = NULL;
			unsigned int * uintValueFromGetter = NULL;
			unsigned long * ulongValueFromGetter = NULL;
			struct timeval * timeValueFromGetter = NULL;
			xsd__hexBinary hexBinFromGetter = NULL;

			/* find parameter with active notification */
			if (((tmp->notification & NotificationActive) == NotificationActive) && (tmp->getDataIdx != 1))
			{
				ParameterValue tmpValue;
				if ((ret = retrieveParamValue(getPathName(tmp), tmp->type, &tmpValue)) != OK)
				{
					return ret;
				}

				/* Call getter function */
				if( (tmp->type == UnsignedIntType) ||
						(tmp->type == DefUnsignedIntType)
				)
				{
					/* use uintValueFromGetter */
					if(getParameter(getPathName(tmp), &uintValueFromGetter) == OK)
					{
						if(uintValueFromGetter != NULL)
						{
							if (tmpValue.out_uint != *uintValueFromGetter)
							{
								/* yes, value is different */
								if(setUniversalParamValueInternal(tmp, NULL, 1, *uintValueFromGetter) == OK)
									*sendInformFlag = 1;
							}
						}
					}
				}
				if( (tmp->type == DefIntegerType) ||
						(tmp->type == IntegerType ) ||
						(tmp->type == BooleanType) ||
						(tmp->type == DefBooleanType))
				{
					/* use intValueFromGetter */
					if(getParameter(getPathName(tmp), &intValueFromGetter) == OK)
					{
						if(intValueFromGetter != NULL)
						{
							if (tmpValue.out_int != *intValueFromGetter)
							{
								/* yes, value is different */
								if(setUniversalParamValueInternal(tmp, NULL, 1, *intValueFromGetter) == OK)
									*sendInformFlag = 1;
							}
						}
					}
				}
				if( (tmp->type == DateTimeType) ||
						(tmp->type == DefDateTimeType) )
				{
					/* use timeValueFromGetter */
					if(getParameter(getPathName(tmp), &timeValueFromGetter) == OK)
					{
						if(timeValueFromGetter != NULL)
						{
							if( (tmpValue.out_timet.tv_sec != timeValueFromGetter->tv_sec) ||
								(tmpValue.out_timet.tv_usec != timeValueFromGetter->tv_usec) )
							{
								/* yes, value is different */
								if(setUniversalParamValueInternal(tmp, NULL, 1, timeValueFromGetter) == OK)
									*sendInformFlag = 1;
							}
						}
					}
				}

				if( (tmp->type == StringType) ||
						(tmp->type == DefStringType) ||
						(tmp->type == Base64Type) ||
						(tmp->type == DefBase64Type) )
				{
					/* use strFromGetter */
					if(getParameter (getPathName(tmp), &strFromGetter) == OK)
					{
						if(strFromGetter != NULL)
						{
							if (strCmp( tmpValue.out_cval, strFromGetter) != 1)
							{
								/* yes, value is different */
								if(setUniversalParamValueInternal(tmp, NULL, 1, strFromGetter) == OK)
									*sendInformFlag = 1;
							}
						}
					}
				}
				if( (tmp->type == UnsignedLongType) ||
						(tmp->type == DefUnsignedLongType)
				)
				{
					/* use ulongValueFromGetter */
					if(getParameter(getPathName(tmp), &ulongValueFromGetter) == OK)
					{
						if(ulongValueFromGetter != NULL)
						{
							if (tmpValue.out_ulong != *ulongValueFromGetter)
							{
								/* yes, value is different */
								if(setUniversalParamValueInternal(tmp, NULL, 1, *ulongValueFromGetter) == OK)
									*sendInformFlag = 1;
							}
						}
					}
				}
				if( (tmp->type == hexBinaryType) ||
						(tmp->type == DefHexBinaryType))
				{
					/* use hexBinFromGetter */
					if(getParameter (getPathName(tmp), &hexBinFromGetter) == OK)
					{
						if(hexBinFromGetter != NULL)
						{
							if (strCmp( tmpValue.out_hexBin, hexBinFromGetter) != 1) // not equal.
							{
								/* yes, value is different */
								if(setUniversalParamValueInternal(tmp, NULL, 1, hexBinFromGetter) == OK)
									*sendInformFlag = 1;
							}
						}
					}
				}
			}
		} // end  if (tmp->isLeaf)
		else
			traverseTreeAndCheckActiveNotification(tmp, sendInformFlag);
		tmp = tmp->next;
	}  // end while (tmp != NULL)
	return ret;
}

int
checkActiveNotification()
{
	int sendInformFlag = 0;

	ParameterEntry *pEntry = &rootEntry;
	if(!pEntry)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "checkActiveNotification() error: rootEntry == NULL.\n");
		)
		return 0;
	}
	traverseTreeAndCheckActiveNotification(pEntry, &sendInformFlag);

	return sendInformFlag;
}

/* Helper function - print all tree */
void
printAllTree()
{
	ParameterEntry *pEntry = &rootEntry;
	if(!pEntry)
	{
		printf("Function printAllTree: rootEntry = NULL\n");
		return;
	}
	traverseTreeAndPrint(pEntry);
}

/* Helper function for traverse tree and print*/
static int
traverseTreeAndPrint(ParameterEntry * entry)
{
	int ret = OK;
	ParameterEntry *tmp = entry->child;
	while (tmp != NULL)
	{
		if (tmp->isLeaf)
		{
			printf("Parameter path     Leaf= %s, ID=%d\n", getPathName(tmp), tmp->paramEntryDB_id);
		} // end  if (tmp->isLeaf)
		else
		{
			printf("Parameter path Not Leaf= %s, ID=%d\n", getPathName(tmp), tmp->paramEntryDB_id);
			traverseTreeAndPrint(tmp);
		}
		tmp = tmp->next;
	}  // end while (tmp != NULL)
	return ret;
}



/* Used to create the instances of Multi-Instance Objects on Start-up
 */
void
readAllMarkedParamsToCreateInstancesOnStartUp()
{
	ParameterEntry *pEntry = &rootEntry;
	if(!pEntry)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "readAllMarkedParamsToCreateInstancesOnStartUp() error: rootEntry == NULL.\n");
		)
		return;
	}
	traverseTreeAndReadMarkedParameters(pEntry);
}

/*
 Function traverseTreeAndReadMarkedParameters:
 Traverse tree according to metadata file(tree definition derived from data-model.xml)
 but reads only Nodes defined with initDataIdx == MARKED_NODE_FOR_ADDITIONAL_READ.
 It responsible for initialization of  sub-tree(s) of multiple objects.
 call for "getter" function for such Node causes initialization process
*/
static int
traverseTreeAndReadMarkedParameters(ParameterEntry * entry)
{
	int ret = OK;
	ParameterEntry *tmp = entry->child;
	char *pathName;
	static ParameterValue value;

	if (entry->type == DefaultType)
		return OK;


	// traverse all 'leaf' parameters
	while (tmp != NULL)
	{
		if (tmp->isLeaf)
		{
			/* find parameter that initializes Subtree of multiple objects */
			if ( NO_DEFAULT_PARAMETER(tmp) && (tmp->getDataIdx > 1) && (tmp->initDataIdx == MARKED_NODE_FOR_ADDITIONAL_READ) )
			{
				pathName = getPathName(tmp);
		        ret = getAccess (tmp->getDataIdx, pathName, tmp->type, &value);
		        if (ret == OK)
		        {
		        	DEBUG_OUTPUT (
		        			dbglog (SVR_DEBUG, DBG_PARAMETER, "traverseTreeAndReadMarkedParameters(): read of '%s' is successful\n", pathName);
		        	)
					setAccess2ParamValue(tmp, &value, MEM_TYPE_PARAM_ENTRY);
					//check on Alias & set Alias value to ParamEntry of parent object:
					checkOnAliasParameterAfterSet(tmp, value.out_cval);
		        }
		        else
		        {
		        	DEBUG_OUTPUT (
		        			dbglog (SVR_WARN, DBG_PARAMETER, "traverseTreeAndReadMarkedParameters(): read of '%s' isn't successful\n", pathName);
		        	)
		        }
			}
		} // end  if (tmp->isLeaf)
		tmp = tmp->next;
	}  // end while (tmp != NULL)

	// traverse all objects after all parameters on this layer are traversed
	tmp = entry->child;
	while (tmp != NULL)
	{
		if (!tmp->isLeaf)
		{
		   traverseTreeAndReadMarkedParameters(tmp);
        }
		tmp = tmp->next;
	}	// end while (tmp != NULL)

	return OK;
}


int
createNewParameter( const char *path, ParameterType type, int instance,
		RebootType reboot, NotificationType notification,
		NotificationMax notificationMax, int initParam, int getParam, int setParam, char *accessList, char * newValue, const bool writeData,
		const char *minEn, const char *maxEn, const char *numEntriesParameter)
{
	ParamValue value;
	ParameterEntry *param = NULL;
	ParameterValue accessValue;
	int ret = 0;

	switch (type)
	{
		case DefIntegerType:
		case DefBooleanType:
		case IntegerType:
		case BooleanType:
			value.ival = a2i(newValue, NULL);
			break;
		case DefUnsignedIntType:
		case UnsignedIntType:
			value.uival = a2ui(newValue, NULL);
			break;
		case DefStringType:
		case DefBase64Type:
		case StringType:
		case Base64Type:
			value.cval = newValue;
			break;
		case DefDateTimeType:
		case DateTimeType:
			sscanf(newValue, "%ld|%ld" ,&value.tval.tv_sec, &value.tval.tv_usec);
			break;
		case DefHexBinaryType:
		case hexBinaryType:
			value.hexBinval = (xsd__hexBinary)newValue;
			break;
		case DefUnsignedLongType:
		case UnsignedLongType:
			value.ulval = a2ul(newValue, NULL);
			break;
		case ObjectType:
		case MultiObjectType:
		case DefaultType:
		default:
			value.cval = "";
			break;
	}
	ret = newParameter(path, type, instance, reboot, notification, notificationMax, initParam, getParam, setParam, accessList, &value, writeData, minEn, maxEn, numEntriesParameter);
	if(ret != OK)
		return ret;

	param = findParameterByPath(path);
	if (param == NULL)
		return ERR_INVALID_ARGUMENT;
	/*param->isNew = true;*/
	saveParameter(param, NULL);
	setParamValue2Access (param->type,  &param->value, &accessValue);
	ret = storeParamValue( path, param->type, &accessValue);

	return ret;
}

int
deleteNewParameter(const char * path)
{
	ParameterEntry * param;

	param = findParameterByPath(path);
	if (param == NULL)
		return ERR_INVALID_ARGUMENT;
	deleteParameter(param, NULL);
	return OK;
}

/* Returns 1 if parameter exists in the ParamEntry tree,
 * else returns 0.
 * */
int
isParameterExists(const char * path)
{
	if (findParameterByPath(path) != NULL)
		return 1;
	return 0;
}

int
getParamIdNotificationAccessList(const char * path, unsigned int * id, unsigned int * notification, char *** accessList, int * accessListSize)
{
	ParameterEntry * entry;
	entry = findParameterByPath(path);

	if(entry == NULL)
		return ERR_INVALID_ARGUMENT;

	if (id)
		*id = entry->paramEntryDB_id;
	if (notification)
		*notification = entry->notification;
	if (accessList)
		*accessList = (char**) entry->accessList;
	if (accessListSize)
		*accessListSize = entry->accessListSize;
	return OK;
}


/* Check before param value is set: if parameter name is "Alias",
 * then check is it unique.
 * If "Alias" value is unique the returns OK;
 * If parameter name != "Alias" returns OK too.
 * */
static int checkOnAliasParameterBeforeSet(ParameterEntry * entry, char *newStringValue)
{
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	if ( !entry || entry->type != StringType)
		return OK;

	if (strcmp(entry->name, "Alias") == 0) // if name of parameter is "Alias"
	{
		if (!newStringValue || !*newStringValue)
			return ERR_INVALID_PARAMETER_VALUE;

		if (!isAliasValueValid(newStringValue))
			return ERR_INVALID_PARAMETER_VALUE;

		ParameterEntry * parentObj, *tmp;
		parentObj = entry->parent;
		if (!parentObj || !(parentObj->parent) )
			return OK;

		tmp = parentObj->parent->child;
		while(tmp)
		{
			if ( tmp != parentObj && tmp->aliasValue && (strcmp(tmp->aliasValue, newStringValue) == 0) )
				return ERR_INVALID_PARAMETER_VALUE;
			tmp = tmp->next;
		}
	}
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */
	return OK;
}

/* Check after param value is set: if parameter name is "Alias",
 * then for parent object instance will be set aliasValue = newStringValue.
 * */
static void checkOnAliasParameterAfterSet(ParameterEntry * entry, char *newStringValue)
{
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	if ( !entry || !newStringValue || !entry->parent || (entry->type != StringType /*&& entry->type != DefStringType*/) )
		return;

	if (strcmp(entry->name, "Alias") == 0) // if name of parameter is "Alias"
	{
		if ( entry->parent->aliasValue  &&  (strcmp(entry->parent->aliasValue, newStringValue) == 0) )
			return;

		// if Alias value is new value:
		if (entry->parent->aliasValue)
			efreeTypedMemByPointer( (void **) &(entry->parent->aliasValue), 0 );

		entry->parent->aliasValue = strnDupByMemType(entry->parent->aliasValue, newStringValue, strlen(newStringValue), MEM_TYPE_PARAM_ENTRY );
	}
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */
	return;
}


/** Get the whole pathname of a ParameterEntry with Alias-Based Addressing if InstanceMode="InstanceAlias".
 * Objects end with a '.'
 * The allocated Memory will be automatic freed
 *      \param ParameterEntry entry
 *      \param char * numberPathName - must contain path with Number Addressing or NULL. If NULL - will not be used,
 *      else:
 *         \If entry==NULL or aliasPathName will be equal to numberPathName then numberPathName pointer will be returned.
 *         \If HAVE_ALIAS_BASED_ADDR_MECHANISM isn't defined  or InstanceMode parameter != InstanceAlias then numberPathName pointer will be returned too.
 *      \return   char * the pathname of entry with/without Alias-Based Addressing.
 */
static char *
getAliasBasedPathName(const ParameterEntry *entry, const char * numberPathName)
{
	if (!entry)
		return numberPathName ? (char *)numberPathName : NULL;

#ifndef HAVE_ALIAS_BASED_ADDR_MECHANISM
	if (numberPathName)
		return (char*)numberPathName;
	else
		return getPathName(entry);
#else
	InstanceMode instanceMode = getInstanceMode();

	if (instanceMode != InstanceAlias)
	{
		if (numberPathName)
			return (char *)numberPathName;
		else
			return getPathName(entry);
	}

	char resultPathNameTmp[MAX_PATH_NAME_SIZE];

	// if instanceMode == InstanceAlias:
	memset(&resultPathNameTmp, 0, MAX_PATH_NAME_SIZE);

	if (entry->parent != NULL)
		getAliasBasedParentPath (entry->parent, resultPathNameTmp, instanceMode);
	if (entry->isLeaf == false && entry->hasAliasParam && entry->aliasValue && *entry->aliasValue)
	{
		strcat(resultPathNameTmp, "[");
		strcat(resultPathNameTmp, entry->aliasValue);
		strcat(resultPathNameTmp, "]."); 	// Add an '.' if it is an Object or an MultiObject ( TR: Page 33 ex. NextLevel )
	}
	else
	{
		strcat (resultPathNameTmp, entry->name);
		// Add an '.' if it is an Object or an MultiObject ( TR: Page 33 ex. NextLevel )
		if (entry->isLeaf == false)
			strcat (resultPathNameTmp, ".\0");
	}

	if (numberPathName)
	{
		if (!strcmp(resultPathNameTmp, numberPathName))
			return (char *)numberPathName;
	}

	return strnDupByMemType( NULL, resultPathNameTmp, strlen(resultPathNameTmp),  MEM_TYPE_TEMP);
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */
}

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
/** Returns the Alias-Based (if InstanceMode == InstanceAlias) parent path of parameter given in entry.
 * or returns Number-Based parent path
 The path is returned through path.
        \param entry   Parameter
        \param path    Pointer to a char *
 */
static void
getAliasBasedParentPath(ParameterEntry *entry, char *path, InstanceMode instanceMode)
{
	/* Loop until we back in the first entry, which we ignore
	 */
	if (entry->parent != NULL)
	{
		getAliasBasedParentPath (entry->parent, path, instanceMode);
		if (instanceMode == InstanceAlias && entry->isLeaf == false && entry->hasAliasParam && entry->aliasValue && *entry->aliasValue)
		{
			strcat(path, "[");
			strcat(path, entry->aliasValue);
			strcat(path, "]");
		}
		else
		{
			strcat(path, entry->name);
		}
		strcat(path, ".");
	}
}

/** function executes the Auto Creation of Instances and used in the SPV
 * 'paramPath' - PathName of the parameter
 * 'isCalledFromACS'  - can accept values RPC_COMMAND_IS_CALLED_INTERNAL (​​0) or RPC_COMMAND_IS_CALLED_FROM_ACS (1)
 * 'callUserDefinedCallback' - can accept values 0 or 1. If 1 - user callback with index addObjIdx will be called,
				 else user callback will not be called, but default callback with index=1 will be called if it is defined.
 *  return ParameterEntry* - pointer to created parameter in the new created object or NULL.
 *  Before this function you MUST call the findDefaultParameterForParamByPath() function
 *  and MUST be sure that 'paramPath' is valid path for used data-model.
 */
static ParameterEntry *
autoCreateInstanceForSPV(const char * paramPath, unsigned int isCalledFromACS, unsigned int callUserDefinedCallback)
{
	char *ptr = NULL;
	char *parentName;
	char *name;
	char *pathPart;
	char tmpPath[strlen(paramPath)];
	ParameterEntry *tmp, *prevTmp;
	if (!paramPath)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "autoCreateInstanceForSPV(): function parameter 'paramPath' cannot be NULL.\n");
		)
		return NULL;
	}
	/* check first for special parameter '.' which defines the root of all Parameter
	 */
	if (strlen(paramPath) == 0 || (*paramPath == '.' && *(paramPath+1) == '\0'))
		return firstParam;

	tmp = firstParam;
	if (!tmp)
		return NULL;
	parentName = getParentName(paramPath);
	name = getName(paramPath);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "autoCreateInstanceForSPV(\"%s\")\n", paramPath);
	)

	if (parentName != NULL)
	{
		memset(tmpPath, 0, sizeof(tmpPath));
		pathPart = strtok_r(parentName, ".", &ptr);
		while (pathPart != NULL)
		{
			strcat(tmpPath, pathPart);
			strcat(tmpPath, ".");
			prevTmp = tmp;
			tmp = findParamInObjectByName(prevTmp, pathPart);
			if (!tmp)
			{
				if (prevTmp->type != MultiObjectType)
					return NULL;
				int len = strlen(pathPart);
				if ( len >= 3  &&  pathPart[0]=='['  &&  pathPart[len-1]==']')
				{
					unsigned int instanceNumber, ret;
					ret = addObjectIntern(tmpPath, &instanceNumber, isCalledFromACS, callUserDefinedCallback);
					if (ret != OK)
					{
						DEBUG_OUTPUT (
								dbglog (SVR_ERROR, DBG_PARAMETER, "autoCreateInstanceForSPV()->addObjectIntern(%s): ret=%d.\n", tmpPath, ret);
						)
						return NULL;
					}
					tmp = findParamInObjectByName(prevTmp, pathPart);
				}
				if (!tmp)
					return NULL;
			}
			pathPart = strtok_r(ptr, ".", &ptr);
		}
	}
	prevTmp = tmp;
	tmp = findParamInObjectByName(prevTmp, name);

	return tmp;
}
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */

/** Helper function to find a parameterEntry of default Parameter by parameter with name 'paramPath'
 * 'paramPath' - Name of the searched parameter
 *  return ParameterEntry* - found default parameter or NULL.
 *  Function can change nonexistent object to default object ONLY if this object is addressed by Alias identifier.
 *  For example:
 *  if paramPath == "InternetGatewayDevice.Layer2Bridging.Bridge.3.Port.[AliasValue1].PortEnable"
 *  then returns (when "InternetGatewayDevice.Layer2Bridging.Bridge.3.Port.[AliasValue1]." does not exist):
 *  1). if "InternetGatewayDevice.Layer2Bridging.Bridge.3." exists:
 *      ParameterEntry* which points to "InternetGatewayDevice.Layer2Bridging.Bridge.3.Port.0.PortEnable"
 *  2). if "InternetGatewayDevice.Layer2Bridging.Bridge.3." doesn't exist:
 *      ParameterEntry* which points to "InternetGatewayDevice.Layer2Bridging.Bridge.0.Port.0.PortEnable"
 *  3). or NULL if default parameter isn't found.
 */
static ParameterEntry *
findDefaultParameterForParamByPath(const char * paramPath)
{
	char *ptr = NULL;
	char *parentName;
	char *name;
	char *pathPart;
	ParameterEntry *tmp, *prevTmp;
	if (!paramPath)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "findDefaultParameterForParamByPath(): function parameter 'paramPath' cannot be NULL.\n");
		)
		return NULL;
	}
	/* check first for special parameter '.' which defines the root of all Parameter
	 */
	if (strlen(paramPath) == 0 || (*paramPath == '.' && *(paramPath+1) == '\0'))
		return firstParam;

	tmp = firstParam;
	if (!tmp)
		return NULL;
	parentName = getParentName(paramPath);
	name = getName(paramPath);
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "findDefaultParameterForParamByPath(): %s / %s\n", parentName, name);
	)

	if (parentName != NULL)
	{
		pathPart = strtok_r(parentName, ".", &ptr);
		while (pathPart != NULL)
		{
			prevTmp = tmp;
			tmp = findParamInObjectByName(prevTmp, pathPart);
			if (!tmp)
			{
				if (prevTmp->type != MultiObjectType)
					return NULL;
				int len = strlen(pathPart);
				if ( len >= 3  &&  pathPart[0]=='['  &&  pathPart[len-1]==']')
					tmp = findParamInObjectByName(prevTmp, "0");
				if (!tmp)
					return NULL;
			}
			pathPart = strtok_r(ptr, ".", &ptr);
		}
	}
	prevTmp = tmp;
	tmp = findParamInObjectByName(prevTmp, name);
	if (!tmp && prevTmp->type == MultiObjectType)
		tmp = findParamInObjectByName(prevTmp, "0");

	return tmp;
}


/* Created by San, Dec 2012.
 * The function can be used for internal setting of parameter value.
 *
 * voidParamEntryPtr - void Ptr, will be converted to the pointer to ParameterEntry struct with parameter.
 * 		(void * is used because the ParameterEntry struct is defined in the parameter.c file only)
 *
 * paramName - full path name to parameter (may be NULL, if voidParamEntryPtr != NULL)
 * 		If voidParamEntryPtr is NULL then paramName will be used for paramEntry searching.
 * 		Else (voidParamEntryPtr != NULL) paramName can be calculated from voidParamEntryPtr.
 *
 * setDirectlyToDB - flag that defines:
 * 		1 - update the parameter value in the DB directly (setAccess() will call with setDataIdx=1)
 * 		0 - setAccess() will call with setDataIdx defined in the data-model if setDataIdx >= 1.
 * 			(In this case when setDataIdx==1 the setDirectlyToDB is not matter.)
 *		If parameter's initDataIdx<=0 the setDirectlyToDB will be ignored.
 *
 * 4th parameter is a new value.
 * 		It can be one of following type:
 * 			int, unsigned int, unsigned long, char *, xsd__hexBinary, struct timeval*.
 * 		The function handles the 4th parameter according to paramEntry->type.
 *
 * Simultaneously the function sets the new value to the paramEntry->value.
 * returns OK or ERROR code.
 * */
int setUniversalParamValueInternal(void *voidParamEntryPtr, const char * paramName, unsigned int setDirectlyToDB, ...)
{
	int ret = OK;
	va_list argptr;
	void* voidValue = NULL;
	int intVal;
	unsigned int uIntVal;
	unsigned long uLongVal;
	struct timeval tVal;
	ParameterValue accessParam = {0};
	ParameterEntry * paramEntry = (ParameterEntry*) voidParamEntryPtr;

	if (setDirectlyToDB != 1 && setDirectlyToDB != 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "setUniversalParamValueInternal(): setDirectlyToDB has invalid value. Maybe you don't use this function properly!\n");
		)
		return ERR_INVALID_ARGUMENT;
	}

	if (!paramEntry)
	{
		if (!paramName)
			return ERR_INVALID_ARGUMENT;
		paramEntry = findParameterByPath (paramName);
		if (!paramEntry)
			return ERR_INVALID_PARAMETER_NAME;
	}

	if (IS_OBJECT (paramEntry) )
		return ERR_INVALID_PARAMETER_TYPE;

	// if parameter isn't initialized in DB and doesn't have specific (>1) setter (which can write to hardware, not to DB)
	// 		then return error.
	if (paramEntry->initDataIdx < 1  &&  paramEntry->setDataIdx <= 1)
		return ERR_INTERNAL_ERROR;

	/* Initialization of argptr */
	va_start(argptr, setDirectlyToDB);

	switch (paramEntry->type)
	{
		case StringType:
		case DefStringType:
		case Base64Type:
		case DefBase64Type:
			 voidValue = va_arg(argptr, char *);
			break;
		case BooleanType:
		case DefBooleanType:
		case IntegerType:
		case DefIntegerType:
			intVal = va_arg(argptr, int);
			voidValue = &intVal;
			break;
		case UnsignedIntType:
		case DefUnsignedIntType:
			uIntVal = va_arg(argptr, unsigned int);
			voidValue = &uIntVal;
			break;
		case DateTimeType:
		case DefDateTimeType:
			memcpy(&tVal, va_arg(argptr, struct timeval*), sizeof(struct timeval));
			voidValue = &tVal;
			break;
		case hexBinaryType:
		case DefHexBinaryType:
			voidValue = va_arg(argptr, xsd__hexBinary);
			if (!isHexBinaryValueTrue( (xsd__hexBinary)voidValue) )
			{
				DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_PARAMETER, "setUniversalParamValueInternal(): voidValue = '%s' is not valid hexBinary voidValue.\n",voidValue);
				)
				va_end(argptr);
				return ERR_INVALID_PARAMETER_VALUE;
			}
			break;
		case UnsignedLongType:
		case DefUnsignedLongType:
			uLongVal = va_arg(argptr, unsigned long);
			voidValue = &uLongVal;
			break;
		default:
			break;
	}

	va_end(argptr);

	ret = setValuePtr (paramEntry, voidValue);
	if (ret != OK)
		return ret;

	if (!paramName)
	{
		paramName = getPathName(paramEntry);
		if (!paramName)
			return ERR_INTERNAL_ERROR;
	}

	ret = checkOnAliasParameterBeforeSet(paramEntry, voidValue);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_PARAMETER, "setUniversalParamValueInternal(%s)->checkOnAliasParameterBeforeSet(): has returned error %d\n",paramName, ret);
		)
		return ret;
	}

	setVoidPtr2Access(paramEntry->type, voidValue, &accessParam);
	if ( setDirectlyToDB || paramEntry->setDataIdx <= 1 )
	{
		ret = setAccess (1, paramName, paramEntry->type, &accessParam); // 1 - default setter
	}
	else
	{
		ret = setAccess (paramEntry->setDataIdx, paramName, paramEntry->type, &accessParam);
	}

	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "setUniversalParamValueInternal(%s)->setAccess(): has returned error %d\n",paramName, ret);
		)
		return ret;
	}

	checkOnAliasParameterAfterSet(paramEntry, accessParam.out_cval);


	if (paramEntry->rebootIdx != NoReboot)
	{
		addRebootIdx(paramEntry->rebootIdx);
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_PARAMETER, "setUniversalParamValueInternal(): \"%s\" -> addRebootIdx(0x%.4X)\n",paramName, paramEntry->rebootIdx);
		)
	}

	paramEntry->status = ValueModifiedNotFromACS;
	DEBUG_OUTPUT (
			dbglog (SVR_INFO, DBG_PARAMETER, "setUniversalParamValueInternal(): Notify ACS: name = %s notification = %d\n", paramEntry->name, paramEntry->notification);
	)

	return ret;
}

static void *getVoidPointerToEmptyValue(ParameterType type)
{
#ifdef ACS_REGMAN
	char *tmp = (char*)emallocTemp(3, 0);
#else
	void *tmp = NULL;
#endif

	switch (type)
	{
		case IntegerType:
		case DefIntegerType:
#ifdef ACS_REGMAN
			sprintf( tmp, "%d", -1);
#else
			tmp = emallocTemp(sizeof(int), 36);
			*((int *)tmp) = -1;
#endif
			break;
		case UnsignedIntType:
		case DefUnsignedIntType:
		case BooleanType:
		case DefBooleanType:
#ifdef ACS_REGMAN
			sprintf( tmp, "%u", 0);
#else
			tmp = emallocTemp(sizeof(unsigned int), 36);
			*((int *)tmp) = 0;
#endif
			break;
		case UnsignedLongType:
		case DefUnsignedLongType:
#ifdef ACS_REGMAN
			sprintf( tmp, "%u", 0);
#else
			tmp = emallocTemp(sizeof(unsigned long), 36);
			*((unsigned long *)tmp) = 0L;
#endif
			break;
		case DateTimeType:
		case DefDateTimeType:
#ifdef ACS_REGMAN
			tmp = UNKNOWN_TIME;
#else
			tmp = emallocTemp(sizeof(struct timeval), 36);
			((struct timeval *)tmp)->tv_sec = ((struct timeval *)tmp)->tv_usec = 0L;
#endif
			break;
		case StringType:
		case DefStringType:
		case hexBinaryType:
		case DefHexBinaryType:
		case Base64Type:
		case DefBase64Type:
			tmp = emallocTemp(1, 0);
			*((char *)tmp) = '\0';
			break;
		default:
			tmp = emallocTemp(1, 0);
			*((char *)tmp) = '\0';
			break;
	}
	return (void *)tmp;
}

/* Returns pointer to ParameterEntry of XXXNumberOfEntries parameter corresponding to 'objectEntry' multi object.
 * If error - returns NULL.
 * For example: if object is "InternetGatewayDevice.DeviceInfo.VendorConfigFile."
 *              then returns pointer to "InternetGatewayDevice.DeviceInfo.VendorConfigFileNumberOfEntries" ParameterEntry
 * */
static ParameterEntry *getNumOfEntriesParamEntry(ParameterEntry *objectEntry)
{
	char buf[80]; //Temp buffer for XXXNumberOfEntries parameter name
	if (!objectEntry)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "getNumOfEntriesParamPath(): 'objectEntry' function argument can not be NULL\n");
		)
		return NULL;
	}

	if (objectEntry->numEntriesParameter && *objectEntry->numEntriesParameter)
	{
		if (strchr(objectEntry->numEntriesParameter, '.'))
		{
			//If '.' is found then numEntriesParameter contains full PathName to NoE parameter
			return findParameterByPath(objectEntry->numEntriesParameter);
		}
		else
		{
			//If '.' is NOT found then numEntriesParameter contains just Name of NoE parameter
			//In this case we will search parameter in the parent object.
			return findParamInObjectByName(objectEntry->parent, objectEntry->numEntriesParameter);
		}
	}
	else
	{
		strcpy(buf, objectEntry->name);
		strcat(buf, NUMBER_OF_ENTRIES_STR);
		return findParamInObjectByName(objectEntry->parent, buf);
	}
}


/* Returns full PathName of XXXNumberOfEntries parameter corresponding to 'objPathName' multi object.
 * If error - returns NULL.
 * For example: if object is "InternetGatewayDevice.DeviceInfo.VendorConfigFile."
 *              then returns pathName "InternetGatewayDevice.DeviceInfo.VendorConfigFileNumberOfEntries"
 * TODO: P.S. If you change name of this function make sure that the name will be changed in the docs too.
 * */
char *getNumOfEntriesParamPathByObjectPathName(const char *objPathName)
{
	ParameterEntry *objectEntry = NULL;

	if (!objPathName)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "getNumOfEntriesParamPathByObjectPathName(): 'objPathName' function argument can not be NULL\n");
		)
		return NULL;
	}

	objectEntry = findParameterByPath(objPathName);
	if (!objectEntry)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "getNumOfEntriesParamPathByObjectPathName()->findParameterByPath(\"%s\"): object is not found.\n", objPathName);
		)
		return NULL;
	}
	return getPathName(getNumOfEntriesParamEntry(objectEntry));
}


/** Description: Delete last the instances of nested object under a given parent object
 *               xxx.{i}.yyy{i}.
 *	     This is patch fix by Comcast for XITHREE-3554
 *  Input:   objEntriesPath - Object path of Number of entries.
 *     	     objEntriesPath - Parent Object path
 *           paramPath - object path of delete instance
 *           mutexLockIsNeeded - This is mutex lock required if we runs from different thread.
             for nested object for a given parent object instance
 *
 * \Return:  OK
 *
 */
int checkAndDeleteTblObjectLastInstances(const char *objEntriesPath, const char *tableObjectPath, const char *paramPath, unsigned int mutexLockIsNeeded)
{
    int ret = OK;
    int subObjDBCnt = 0, subObjCnt = 0;
    unsigned int instance;
    ParameterValue pValue;
    char tmpPathName[MAX_PATH_NAME_SIZE] = {'\0'};
    int count = 0;
    ParameterEntry *tmp = NULL;
    ParameterEntry *tmpObjEntry = NULL;
    unsigned int objNameLen = 0;

    /*Get the number of entries for Table object from hostif.*/
    ret = get_ParamValues_tr69hostIf (objEntriesPath, IntegerType, &pValue);

    if (OK == ret)
    {
        subObjCnt = pValue.out_int;
    }

//    printf ("[%s:%s:%d] subObjCnt  = %d for paramPath = %s\n", __FILE__, __FUNCTION__, __LINE__,  subObjCnt, paramPath);
    /* get the all instances for Associated MoCA device */
    ret = countInstances( tableObjectPath, &count );
    if ( ret == OK ) {
        subObjDBCnt = count;
    }
    else
    {
        subObjDBCnt = 0;
    }
//    printf ("[%s:%s:%d] subObjDBCnt  = %d for paramPath = %s\n", __FILE__, __FUNCTION__, __LINE__, subObjDBCnt, paramPath);


    int oldCancelState;
    pthread_testcancel();
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldCancelState);

    if (subObjCnt < subObjDBCnt)
    {

//        printf ("[%s:%s:%d] \n", __FILE__, __FUNCTION__, __LINE__);
        tmp = findParameterByPath(tableObjectPath);
        strncpy(tmpPathName, tableObjectPath, MAX_PATH_NAME_SIZE-1);
        objNameLen = strlen(tmpPathName);


        if (!tmp || tmp->type != MultiObjectType)
            return ERR_INVALID_ARGUMENT;

        tmpObjEntry = tmp;

        if (!tmpPathName[0])
        {
            strncpy(tmpPathName, getPathName(tmp), MAX_PATH_NAME_SIZE-1);
            objNameLen = strlen(tmpPathName);
        }


        if (mutexLockIsNeeded)
            pthread_mutex_lock(&paramLock);

        tmp = tmp->child;
        int instCount = 0;
        while (tmp != NULL)
        {
            instCount++;
            if (tmp->type == ObjectType) //and != DefaultType or some parameter
            {
//                printf ("[%s:%s:%d] tmp->name :%s tmp->instance : %d\n", __FILE__, __FUNCTION__, __LINE__, tmp->name, tmp->instance);
                if (instCount >= subObjDBCnt)
                {
                    //printf ("[%s:%s:%d] Matched tmp->instance : %d with subObjDBCnt : %d\n", __FILE__, __FUNCTION__, __LINE__, tmp->parent->instance, subObjDBCnt);
                    memset(tmpPathName, '\0',MAX_PATH_NAME_SIZE);
                    sprintf(tmpPathName, "%s%d.", tableObjectPath, instCount);
                    DEBUG_OUTPUT (dbglog (SVR_DEBUG, DBG_PARAMETER, "[%s:%s():%d]: Object instance \"%s\" will be deleted (%d).\n",
                                          __FILE__, __FUNCTION__, __LINE__, tmpPathName, tmp->parent->instance);)

                    ret = deleteObjectIntern(tmpPathName, RPC_COMMAND_IS_CALLED_INTERNAL, CALL_USER_DEFINED_CALLBACK);

                    DEBUG_OUTPUT ( dbglog ((ret!=OK ? SVR_ERROR: SVR_INFO), DBG_PARAMETER,
                                           "[%s:%s()->deleteObjectIntern(%s)] has returned %d\n", __FILE__, __FUNCTION__, tmpPathName, ret);)
                    tmp->parent->instance--;
                }
                tmp = tmp->next;
                ret += ret;
            }
            else
                tmp = tmp->next;
        }
    }

    if (mutexLockIsNeeded)
        pthread_mutex_unlock(&paramLock);
    pthread_setcancelstate(oldCancelState, NULL);
    pthread_testcancel();

    return ret;
}



