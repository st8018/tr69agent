/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef ftcallback_H
#define ftcallback_H

#include "dimark_globals.h"

#ifdef HAVE_FILE

#include "utils.h"
#include "filetransfer.h"

typedef struct vendorConfigFile
{
#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM
	char Alias[65];
#endif
	char Name[65];
	char Version[17];
	xsd__dateTime Date;
	char Description[257];
} VendorConfigFile;

/* Callback functions is called before/after a file is dowload/uploaded */
int fileDownloadCallbackBefore(TransferEntry * te);
int fileDownloadCallbackAfter(TransferEntry * te);
int fileUploadCallbackBefore(TransferEntry *);
int fileUploadCallbackAfter(TransferEntry *);
int fileScheduleDownloadCallbackBefore(TransferEntry *, int );
int fileScheduleDownloadCallbackAfter(TransferEntry *, int );
int isConfirmationTrue(TransferEntry *);

#endif /* HAVE_FILE */
#endif /* ftcallback_H */
