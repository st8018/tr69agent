/***************************************************************************
 *    Original code ©Copyright (C) 2004-2012 by Dimark Software Inc.       *
 *    support@dimark.com                                                   *
 *    Modifications © 2013 Comcast Cable Communications, LLC               *
 ***************************************************************************/

/** Implement the access to some often needed parameters
 */

#include "paramconvenient.h"
#include "parameter.h"
#include "dimark_globals.h"
#include "eventcode.h"

char *server_url = NULL;

struct ConfigManagement dimclientConfStruct;

static char *server_username = NULL;
static char *server_passwd = NULL;
static struct DeviceId devId;
static char server_addr[32];
static time_t sessionId = (time_t)0;
static char url[256] = "";

static int isSessionNew( void );
static void updateServerAddr( void );
static char sha_key[65] = {'\0'};

/**	Returns	the	PeriodicInformInterval Value
 *  If PeriodicInformEnable == false return 0
 */
unsigned int
getPeriodicInterval( void )
{
	bool *periodicEnabled;
	unsigned int *periodicInterval = NULL;

	if ( getParameter(PERIODIC_INFORM_ENABLE, &periodicEnabled) != OK)
		return 0;
	if ( *periodicEnabled == false )
		return 0;
	if (getParameter (PERIODIC_INFORM_INTERVAL, &periodicInterval) == OK)
		return *periodicInterval;

	return 0;
}

/**	Returns	the	PeriodicInformTime Value in parameter p
 *  Returns -1 - if error, and OK if no errors.
 *  If PeriodicInformEnable == false return OK and 0.0 sec in p
 */
int
getPeriodicTime( struct timeval* p )
{
	bool *periodicEnabled;
	struct timeval * tv_tmp;
	p->tv_sec = 0;
	p->tv_usec = 0;

	if ( getParameter(PERIODIC_INFORM_ENABLE, &periodicEnabled) != OK)
		return -1;
	if ( *periodicEnabled == false )
		return OK;
	if ( *periodicEnabled == true
			&& getParameter (PERIODIC_INFORM_TIME, &tv_tmp) == OK)
	{
		p->tv_sec = tv_tmp->tv_sec;
		p->tv_usec = tv_tmp->tv_usec;
		return OK;
	}

	return -1;
}

/**
 * Get the ACS connection URL
 */
const char *
getServerURL( void )
{
	char *tmp = NULL;
	int   ret = 0;

	// check if the url is not initialized yet or the session is new
	if ( !server_url || isSessionNew() || ( strlen(server_url)==0) )
	{
		ret = getParameter (MANAGEMENT_SERVER_URL, &tmp);
		if ( ret != OK) {
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_PARAMETER, "getServerURL: getParameter(%s) failed ret = %d\n", MANAGEMENT_SERVER_URL, ret);
			)
			if (!server_url)
				server_url = "";
		}
		else
		{
			//server_url = strnDupByMemType(server_url, tmp, tmp ? strlen(tmp) : 0, MEM_TYPE_SESSION);
			server_url = strnDupByMemType(server_url, "https://acs-cpe-devel.eng.rr.com:7547/edge/tr69", strlen("https://acs-cpe-devel.eng.rr.com:7547/edge/tr69"), MEM_TYPE_SESSION);
			if (server_url)
			{
				updateServerAddr();
				if (*url && strcmp(url, server_url))
				{
					//If "0 BOOTSTRAP" is in the eventCodeList then all other event codes (except on "1 BOOT") will be deleted.
					if ( findEventCode(EV_BOOT) )
					{
						freeEventList(false); //it is needed to clear Event Storage, but it will be done in the addEventCodeSingle(). So, use "false" param.
						addEventCodeSingle(EV_BOOT);
					}
					else
						freeEventList(false); //it is needed to clear Event Storage, but it will be done in the addEventCodeSingle(). So, use "false" param.
					addEventCodeSingle(EV_BOOTSTRAP);
				}
				strncpy(url, server_url, sizeof(url)-1);
				DEBUG_OUTPUT (
						dbglog (SVR_DEBUG, DBG_PARAMETER,"getServerURL(): getParameter(%s) returns ret = %d, server_url = \"%s\"\n", MANAGEMENT_SERVER_URL, ret, server_url);
				)
			}
			else
			{
				server_url = "";
			}
		}
		sessionId = getSessionId();
	}
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_DEBUG, DBG_PARAMETER,"getServerURL: server_url = \"%s\"\n", server_url, ret);
		)
	}
	return server_url;
}

void set_server_url_when_redirect(const char * endpoint)
{
	if (endpoint && strlen(endpoint)>0)
		server_url = strnDupByMemType(server_url, endpoint, strlen(endpoint), MEM_TYPE_SESSION);
}

#ifdef WITH_STUN_CLIENT
/*
 * Return IPv4 address found for the ServerURL
 */
const char *
getServerAddr( void )
{
	return server_addr;
}
#endif /* WITH_STUN_CLIENT */

/* 
 * When server_url is updated, also update server_addr
 */
static void
updateServerAddr (void)
{
	/* Parse URL into extra host and port parts */
	char tmp_buf[128] = {0};
	int  ret;
	struct hostent     *he;
	struct sockaddr_in server;

	strcpy( server_addr, "");

	ret = getHostStrFromURL(server_url, tmp_buf, NULL);

	if ( ret == OK && strlen(tmp_buf) > 0 )
	{
		if ((he = gethostbyname( tmp_buf ))) {
			bzero((void *) &server, sizeof(server));
			server.sin_family = AF_INET;
			memcpy((char *)&server.sin_addr, (char *) he->h_addr_list[0], he->h_length);
			sprintf (server_addr, "%s",inet_ntoa( server.sin_addr));
		}

		DEBUG_OUTPUT (
				dbglog (SVR_DEBUG, DBG_PARAMETER, "updateServerAddr(): server_addr = %s\n",server_addr);
		)
	}
}

#if 0
/*
 *	Returns	the	Username defined in	
 *      *.ManagementServer.Username
 *	if the parameter doesn't exist or is empty return NULL
 */
const char *
getUsername( void )
{
	int ret = OK;
	char *oui = NULL;
	char *productClass = NULL;
	char *serial = NULL;
	char *tmp = NULL;
	char tmpUserName[140] = {'\0'};

	if (  isSessionNew() || !server_username)
	{
		if (getParameter(MANAGEMENT_SERVER_USERNAME, &tmp) == OK)
		{
			sessionId = getSessionId();
			if (tmp && *tmp)
				server_username = strnDupByMemType(server_username, tmp, strlen(tmp), MEM_TYPE_SESSION);
			else
				server_username = NULL;

			if (!server_username || !*server_username)
			{
				ret = getParameter(DEVICE_INFO_MANUFACTURER_OUI, &oui);
				ret += getParameter(DEVICE_INFO_PRODUCT_CLASS, &productClass);
				ret += getParameter(DEVICE_INFO_SERIAL_NUMBER, &serial);

				if (ret == OK && oui && serial)
				{
					strncpy (tmpUserName, oui, 6);
					strcat (tmpUserName, "-");
					if (productClass && *productClass)
					{
						strncat (tmpUserName, productClass, 64);
						strcat (tmpUserName, "-");
					}
					strncat (tmpUserName, serial, 64);

					int numSymbolToEncode = 0;
					tmp = tmpUserName;
					while(*tmp)
					{
						if (!isalnum(*tmp) && *tmp != '_' && (tmp-tmpUserName != strlen(oui)) && (tmp-tmpUserName != strlen(tmpUserName)-strlen(serial) - 1))
							numSymbolToEncode++; //Inc the number of symbols to encode
						tmp++;
					}

					server_username = (char *) emallocSession (strlen(tmpUserName) + numSymbolToEncode*2 + 1, 1);
					if (!server_username)
						return NULL;

					char *tmpPointer = server_username;
					tmp = tmpUserName;
					while(*tmp)
					{
						if (!isalnum(*tmp) && *tmp != '_' && (tmp-tmpUserName != strlen(oui)) && (tmp-tmpUserName != strlen(tmpUserName)-strlen(serial) - 1))
						{
							*tmpPointer = '%';
							tmpPointer++;
							sprintf(tmpPointer, "%0.2X", *tmp);
							tmpPointer += 2;
						}
						else
						{
							*tmpPointer = *tmp;
							tmpPointer++;
						}
						tmp++;
					}

					/* set the parameter for later usage  */
					setParameter (MANAGEMENT_SERVER_USERNAME, server_username);
				}
				else
					server_username = NULL;
			}
		}
	}
	return server_username;
}
#endif

/*
 *	Returns	the	Username defined in	
 *      *.ManagementServer.Username
 *	if the parameter doesn't exist or is empty return NULL
 */
const char *
getUsername( void )
{
	int ret = OK;
	char *oui;
	char *serial;
        char *productClass;
	char *tmp;

	//  if ( server_username == NULL || isSessionNew() ) {
	isSessionNew();

	if (getParameter (MANAGEMENT_SERVER_USERNAME, &tmp) == OK)	{
		sessionId = getSessionId();
		if ( tmp != NULL )
			server_username = strnDupByMemType(server_username, tmp, strlen(tmp), MEM_TYPE_SESSION);
		else
			server_username = tmp;

		if (server_username == NULL || strlen (server_username) == 0) {
			ret = getParameter (DEVICE_INFO_MANUFACTURER_OUI, &oui);
			ret += getParameter (DEVICE_INFO_SERIAL_NUMBER, &serial);
			ret += getParameter (DEVICE_INFO_PRODUCT_CLASS, &productClass); 

			if (ret == OK) 
			{
				/*As per DIG, CPE username format is "<OUI>-<ProductClass>-<SerialNumber>". */
				server_username = (char *) emallocSession (strlen (oui) + strlen (serial) + strlen(productClass) + 3, 1);
				/*Commented: Because above format is supported */
				//server_username = (char *) emallocTypedMem(strlen (oui) + strlen (serial) + 3, MEM_TYPE_SESSION, 1);
				strcpy (server_username, oui);
				strcat (server_username, "-");
				if(productClass && *productClass)
				{
				   strcat (server_username, productClass);
				   strcat (server_username, "-");
				}
				strcat (server_username, serial);

				/* set the parameter for later usage  */
				setParameter (MANAGEMENT_SERVER_USERNAME, server_username);

				/* reread the parameter to get the pointer to the new parameter,
				 * the old username  pointer is freed after the mainloop exits
				 */
				getParameter (MANAGEMENT_SERVER_USERNAME, &tmp);
				server_username = strnDupByMemType(server_username, tmp, strlen(tmp), MEM_TYPE_SESSION);
			} else
				server_username = NULL;
		}
	}
	//  }

	return server_username;
}

/**  Returns the password defined in Device.ManagementServer.Password
 *  As per DIG, if the CPE has not received any value for ManagementServer.Password
 *  parameter via any mechanism defined in this document or respective product
 *  specifications, it MUST calculate the default password using HMAC-SHA256(key, text)
 *  (HMAC using SHA-256 hash function of SHA-2 group) where:
 *  > HMAC method MUST adhere to the RFC2104.
 *  > text is of the form: <OUI>-<ProductClass> e.g. AABBCC-STB
 *  > key, Vendor MUST contact Comcast to receive the value of this parameter.
 *  > OUI and ProductClass used in “text” MUST be in UPPERCASE.
 * @TODO: Hence, key is set default to 'PreSharedKey', once the vendor gets
 * it, then it would take from specific file.
 *  if the parameter doesn't exist or is empty return NULL
 */
const char *
getPassword( void )
{
	int ret = OK;
	char *oui = NULL;
	char *productClass = NULL;
	char *tmp = NULL, *to = NULL;
	char cmp[64] = {'\0'};
	char sha_text[128] = {'\0'}, sha_md[64] = {'\0'};
	int  i = 0, sha_key_len = 0, sha_text_len = 0,  sha_md_len = 0;
	HMAC_CTX ctx;

	isSessionNew();

	if (getParameter (MANAGEMENT_SERVER_PASSWORD, &tmp) == OK)
	{
		if ( tmp != NULL )
			server_passwd = strnDupByMemType(server_passwd, tmp, strlen(tmp), MEM_TYPE_SESSION);
		else
			server_passwd = tmp;

		sessionId = getSessionId();
		if (server_passwd == NULL || strlen (server_passwd) == 0)
		{
			ret = getParameter (DEVICE_INFO_MANUFACTURER_OUI, &oui);
			ret += getParameter (DEVICE_INFO_PRODUCT_CLASS, &productClass);

			if (ret == OK)
			{
				strcpy (sha_text, oui);
				DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_AUTH, "getOUI(); DEVICE_INFO_MANUFACTURER_OUI [%s], productClass: [%s]\n", oui, productClass);
				)
				if(productClass && *productClass)
				{
				   strcat (sha_text, "-");
				   strcat (sha_text, productClass);
				}

				sha_text_len = strlen(sha_text);
				
				if(NULL != getSharedKey())
                                {
					sha_key_len = strlen(sha_key);
				}
				else
				{
					/* Deafult sha_key */
				//	strcpy(sha_key, "PreSharedKey");
				//	sha_key_len = strlen(sha_key);

					DEBUG_OUTPUT (
						dbglog (SVR_ERROR, DBG_AUTH, "%s; Failed to read the sharedkey from path [%s]. The default path should be \'/etc/tr69/sharedKey.txt\'.\n", __FUNCTION__, sha_key,dimclientConfStruct.SharedKeyPathName );
						dbglog (SVR_ERROR, DBG_AUTH, "%s; Using the default value of sharedkey as \'%s\'\'.\n", __FUNCTION__, sha_key );
					)
				}
				
				HMAC_CTX_init( &ctx);
				HMAC_Init(   &ctx, sha_key,  sha_key_len, EVP_sha256());
				HMAC_Update( &ctx, (unsigned char *)sha_text, sha_text_len);
				HMAC_Final(  &ctx, (unsigned char *)sha_md, (unsigned int *)&sha_md_len );

				to = cmp;

				for (i = 0; i < sha_md_len; i++) 
				{
					sprintf(to,"%02x", sha_md[i] & 0xff);
					to += 2;
				}

				server_passwd = (char *) emallocSession(strlen (cmp) + 3, 2);
				server_passwd = cmp;

				HMAC_CTX_cleanup( &ctx );

				DEBUG_OUTPUT (
					dbglog (SVR_DEBUG, DBG_AUTH, "getPassword(); HMAC-SHA256 (server_passwd) [%3d] = %s\n", strlen(server_passwd), server_passwd);
				)

				/* set the parameter for later usage */
				setParameter (MANAGEMENT_SERVER_PASSWORD, server_passwd);

				/* reread the parameter to get the pointer to the new parameter,
				 * the old passwd pointer is freed after the mainloop exits
				 */
				getParameter (MANAGEMENT_SERVER_PASSWORD, &tmp);
				server_passwd = strnDupByMemType(server_passwd, tmp, strlen(tmp), MEM_TYPE_SESSION);
			}else
				server_passwd = NULL;
		}
	}
	return server_passwd;
}

#if 0
/**	Returns	the	password defined in	
  *	InternetGatewayDevice.ManagementServer.Password
  * if the parameter doesn't exist or is empty return NULL
 */
const char *
getPassword( void )
{
	int ret = OK;
	char *oui;
	char *serial;
	char *tmp;

	if ( isSessionNew() || !server_passwd )
	{
		if (getParameter (MANAGEMENT_SERVER_PASSWORD, &tmp) == OK)
		{
			if ( tmp != NULL )
				server_passwd = strnDupByMemType(server_passwd, tmp, strlen(tmp), MEM_TYPE_SESSION);
			else
				server_passwd = NULL;

			sessionId = getSessionId();
			if (!server_passwd)
				server_passwd = "";
		}
	}
	return server_passwd;
}
#endif

/**
 * Last modification: San, 15 July 2011
 * Return the connection URL in parameter connURL, with which the ACS connects to the CPE,
 * when the ACS sends a ConnectionRequest
 *  Return OK - if result of function is OK
 */
int
getConnectionURL (void * connURL)
{
	int ret;
	if ((ret = getParameter (CONNECTION_REQUEST_URL, connURL)) != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_PARAMETER, "getConnectionURL()->getParameter(%s) has returned error = %i\n",CONNECTION_REQUEST_URL,ret);
		)
		return ret;
	}
	return OK;
}

/**
 * Last modification: San, 15 July 2011
 * Return the Username in parameter connUser, with which the ACS connects to the CPE,
 * when the ACS sends a ConnectionRequest
 *  Return OK - if result of function is OK
 */
int
getConnectionUsername (void * connUser)
{
	int ret;
	if ((ret = getParameter (CONNECTION_REQUEST_USERNAME, connUser)) != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST, "getConnectionUsername()->getParameter(%s) returns error = %i\n",CONNECTION_REQUEST_USERNAME,ret);
		)
		return ret;
	}
	return OK;
}

/**
 * Last modification: San, 15 July 2011
 * Return the Password in parameter connPass, with which the ACS connects to the CPE,
 * when the ACS sends a ConnectionRequest
 *  Return OK - if result of function is OK
 */
int
getConnectionPassword (void * connPass)
{
	int ret;
	if ((ret = getParameter (CONNECTION_REQUEST_PASSWORD, connPass)) != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST, "getConnectionPassword()->getParameter(%s) returns error = %i\n",CONNECTION_REQUEST_PASSWORD,ret);
		)
		return ret;
	}
	return OK;
}

/** Return the DeviceId structure, which is used in the inform message
 */
struct DeviceId *
getDeviceId (void)
{
	int ret;
	ret = getParameter (DEVICE_INFO_MANUFACTURER, &devId.Manufacturer);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST, "getDeviceId()->getParameter(%s) returns error = %i\n",DEVICE_INFO_MANUFACTURER,ret);
		)
		return NULL;
	}
	ret = getParameter (DEVICE_INFO_MANUFACTURER_OUI, &devId.OUI);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST, "getDeviceId()->getParameter(%s) returns error = %i\n",DEVICE_INFO_MANUFACTURER_OUI,ret);
		)
		return NULL;
	}
	ret = getParameter (DEVICE_INFO_PRODUCT_CLASS, &devId.ProductClass);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST, "getDeviceId()->getParameter(%s) returns error = %i\n",DEVICE_INFO_PRODUCT_CLASS,ret);
		)
		return NULL;
	}
	ret = getParameter (DEVICE_INFO_SERIAL_NUMBER, &devId.SerialNumber);
	if (ret != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_REQUEST, "getDeviceId()->getParameter(%s) returns error = %i\n",DEVICE_INFO_SERIAL_NUMBER,ret);
		)
		return NULL;
	}
	return &devId;
}

static int
isSessionNew(void) 
{
	if ( isNewSession(sessionId) )
	{
		server_passwd = NULL;
		server_username = NULL;
		sessionId = getSessionId();
		return 1;
	}
	else
		return 0;
}

/**
 * Init configuration struct with a data from the file, which a path is a function parameter
 */
int
initConfStruct(const char *confPath )
{
	char line[MAX_CONF_FILE_SRT_LEN];
	int ret;
	FILE* fp;
	char * point1;
	char paramName[50] = {'\0'};
	int len = 0;
	int res;

	memset(&dimclientConfStruct.url, 0, sizeof(dimclientConfStruct.url));
	memset(&dimclientConfStruct.username, 0, sizeof(dimclientConfStruct.username));
	memset(&dimclientConfStruct.password, 0, sizeof(dimclientConfStruct.password));
#ifdef BINDING_TO_INTERFACE_NAME_IS_ENABLED
	memset(&dimclientConfStruct.netDeviceName, 0, sizeof(dimclientConfStruct.netDeviceName));
#endif
	memset(&dimclientConfStruct.HTTPLogFilePathName, 0, sizeof(dimclientConfStruct.HTTPLogFilePathName));
	memset(&dimclientConfStruct.TESTLogFilePathName, 0, sizeof(dimclientConfStruct.TESTLogFilePathName));
	memset(&dimclientConfStruct.ScriptPath, 0, sizeof(dimclientConfStruct.ScriptPath));
	memset(&dimclientConfStruct.SharedKeyPathName, 0, sizeof(dimclientConfStruct.SharedKeyPathName));
	memset(&dimclientConfStruct.BootstrapFilePathName, 0, sizeof(dimclientConfStruct.BootstrapFilePathName));
	memset(&dimclientConfStruct.SslCertPath_Acs, 0, sizeof(dimclientConfStruct.SslCertPath_Acs));
	memset(&dimclientConfStruct.devicePropertiesFile, 0, sizeof(dimclientConfStruct.devicePropertiesFile));

	dimclientConfStruct.maxHTTPLogSize = 0;
	dimclientConfStruct.maxHTTPLogBackupCount = 0;
	dimclientConfStruct.maxTestLogSize = 0;
	dimclientConfStruct.maxTestLogBackupCount = 0;

	fp = fopen(confPath,"r");
	if (!fp)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "initConfStruct: fopen() error. Check if path set correctly by the -f attribute of dimclient command line.\n");
		)
		return ERR_INTERNAL_ERROR;
	}
	while (fgets(line, MAX_CONF_FILE_SRT_LEN, fp))
	{
		memset(paramName, 0, sizeof(paramName) );
		point1 = strchr(line,0x3D); // symbol '='
		if (!point1) { continue; }
		len = strlen(line) - strlen(point1);
		if (len < 1) { continue; }
		strncpy(paramName,line,len);

		if (0 == strcmp(paramName,"ManagementServerURL")){
			res = sscanf(line, "ManagementServerURL=%s", &dimclientConfStruct.url);
			if (res > 0  &&  strlen(dimclientConfStruct.url) > 0 && isBootstrap()) {
				ret = setUniversalParamValueInternal(NULL, MANAGEMENT_SERVER_URL, 1, dimclientConfStruct.url);
				if (ret != OK)
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "initConfStruct()->setUniversalParamValueInternal(%s) has returned error = %i\n",MANAGEMENT_SERVER_URL,ret);
					)
					fclose(fp);
					return ret;
				}
			}
			continue;
		}

		if (0 == strcmp(paramName,"Username")){
			res = sscanf(line, "Username=%s", &dimclientConfStruct.username);
			if (res > 0  &&  strlen(dimclientConfStruct.username) > 0 && isBootstrap()) {
				ret = setUniversalParamValueInternal(NULL, MANAGEMENT_SERVER_USERNAME, 1, dimclientConfStruct.username);
				if (ret != OK)
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "initConfStruct()->setUniversalParamValueInternal(%s) has returned error = %i\n",MANAGEMENT_SERVER_USERNAME,ret);
					)
					fclose(fp);
					return ret;
				}
			}
			continue;
		}


		if (0 == strcmp(paramName,"Password")){
			res = sscanf(line, "Password=%s", &dimclientConfStruct.password);
			if (res > 0  &&  strlen(dimclientConfStruct.password) > 0 && isBootstrap()) {
				ret = setUniversalParamValueInternal(NULL, MANAGEMENT_SERVER_PASSWORD, 1, dimclientConfStruct.username);
				if (ret != OK)
				{
					DEBUG_OUTPUT (
							dbglog (SVR_ERROR, DBG_DIAGNOSTIC, "initConfStruct()->setUniversalParamValueInternal(%s) has returned error = %i\n",MANAGEMENT_SERVER_PASSWORD,ret);
					)
					fclose(fp);
					return ret;
				}
			}
			continue;
		}
#ifdef BINDING_TO_INTERFACE_NAME_IS_ENABLED
// Intel change for Bind to Device TCP Socket option - read eRouter name from dimark.conf file
// Perform Bind to eRouter Interface only
		if (0 == strcmp(paramName,"NetDeviceName")){
			res = sscanf(line, "NetDeviceName=%s", &dimclientConfStruct.netDeviceName);
			if (res <= 0 )
			{
				memset(&dimclientConfStruct.netDeviceName, 0, sizeof(dimclientConfStruct.netDeviceName)); // Default
			}
			continue;
		}
#endif
		if (0 == strcmp(paramName,"HTTPLogFilePathName")){
			res = sscanf(line, "HTTPLogFilePathName=%s", &dimclientConfStruct.HTTPLogFilePathName);
			if (res <= 0  ||  strlen(dimclientConfStruct.HTTPLogFilePathName) == 0)
			{
				strcpy(dimclientConfStruct.HTTPLogFilePathName, "/dev/null");
			}
			continue;
		}

		if (0 == strcmp(paramName,"TESTLogFilePathName")){
			res = sscanf(line, "TESTLogFilePathName=%s", &dimclientConfStruct.TESTLogFilePathName);
			if (res <= 0  ||  strlen(dimclientConfStruct.TESTLogFilePathName) == 0)
			{
				strcpy(dimclientConfStruct.TESTLogFilePathName, "/dev/null");
			}
			continue;
		}

		if (0 == strcmp(paramName,"maxHTTPLogSize")){
			long longtmp;
			res = sscanf(line,"maxHTTPLogSize=%ld", &longtmp);
			if (res <= 0 ){
				dimclientConfStruct.maxHTTPLogSize = 0; // Default, Without limit
			}
			else {
				dimclientConfStruct.maxHTTPLogSize = (longtmp < 0) ? 0 : (unsigned long)longtmp;
			}
			continue;
		}

		if (0 == strcmp(paramName,"maxHTTPLogBackupCount")){
			int inttmp;
			res = sscanf(line,"maxHTTPLogBackupCount=%d", &inttmp);
			if (res <= 0 ){
				dimclientConfStruct.maxHTTPLogBackupCount = BACKUP_FILES_DEFAULT_COUNT; // Default
			}
			else {
				dimclientConfStruct.maxHTTPLogBackupCount = (inttmp < 0) ? 0 : inttmp;
			}
			continue;
		}

		if (0 == strcmp(paramName,"MaxTestLogSize")){
			long longtmp;
			res = sscanf(line,"MaxTestLogSize=%ld", &longtmp);
			if (res <= 0 ){
				dimclientConfStruct.maxTestLogSize = 0; // Default, Without limit
			}
			else {
				dimclientConfStruct.maxTestLogSize = (longtmp < 0) ? 0 : (unsigned long)longtmp;
			}
			continue;
		}

		if (0 == strcmp(paramName,"MaxTestLogBackupCount")){
			int inttmp;
			res = sscanf(line,"MaxTestLogBackupCount=%d", &inttmp);
			if (res <= 0 ){
				dimclientConfStruct.maxTestLogBackupCount = BACKUP_FILES_DEFAULT_COUNT; // Default
			}
			else {
				dimclientConfStruct.maxTestLogBackupCount = (inttmp < 0) ? 0 : inttmp;
			}
			continue;
		}
	
		if (0 == strcmp(paramName,"ScriptPath")){
			res = sscanf(line, "ScriptPath=%s", &dimclientConfStruct.ScriptPath);
			if (res <= 0  ||  strlen(dimclientConfStruct.ScriptPath) == 0)
			{
				strcpy(dimclientConfStruct.ScriptPath, "/lib/rdk/");
			}
			continue;
		}

		if (0 == strcmp(paramName,"SharedKeyPathName")){
			res = sscanf(line, "SharedKeyPathName=%s", &dimclientConfStruct.SharedKeyPathName);
			if (res <= 0  ||  strlen(dimclientConfStruct.SharedKeyPathName) == 0)
			{
				strcpy(dimclientConfStruct.SharedKeyPathName, "/etc/tr69/sharedKey.txt");
			}
		}
		if (0 == strcmp(paramName,"BootStrapFilePathName")){
			res = sscanf(line, "BootStrapFilePathName=%s", &dimclientConfStruct.BootstrapFilePathName);
			if (res <= 0  ||  strlen(dimclientConfStruct.BootstrapFilePathName) == 0)
			{
			    strcpy(dimclientConfStruct.BootstrapFilePathName, "/opt/persistent/tr69bootstrap.dat");
			}
		}
		if (0 == strcmp(paramName,"CA_CERT_ACS")){
			res = sscanf(line, "CA_CERT_ACS=%s", &dimclientConfStruct.SslCertPath_Acs);
			if (res <= 0  ||  strlen(dimclientConfStruct.SslCertPath_Acs) == 0)
			{
			    strcpy(dimclientConfStruct.SslCertPath_Acs, "/usr/share/tr69/certs/comcast_acs_ca.crt");
			}
		}

		#ifdef AUTH_SERVICE
		if (0 == strcmp(paramName,"DEV_PROP_FILE")){
			res = sscanf(line, "DEV_PROP_FILE=%s", &dimclientConfStruct.devicePropertiesFile);
			if (res <= 0  ||  strlen(dimclientConfStruct.devicePropertiesFile) == 0)
			{
				strcpy(dimclientConfStruct.devicePropertiesFile, "/etc/device.properties");
			}
		}
		#endif
	}

	fclose(fp);
	return OK;
}

/*
 *	Returns	the	URL defined in	configuration file
 *	if the parameter doesn't exist or is empty return NULL
 */
char *
getServerURLFromConf( void )
{
	return dimclientConfStruct.url;
}

/*
 *	Returns	the	Username defined in	configuration file
 *	if the parameter doesn't exist or is empty return NULL
 */
char *
getUsernameFromConf( void )
{
	return dimclientConfStruct.username;
}


/*
 *	Returns	the	Password defined in	configuration file
 *	if the parameter doesn't exist or is empty return NULL
 */
char *
getPasswordFromConf( void )
{
	return dimclientConfStruct.password;
}

/*
 *	Returns	the sharedkey from the defined path from configuration file
 *	if the parameter doesn't exist or is empty return NULL
 */
char *
getSharedKey( void )
{
	FILE* fp = NULL;
	if(dimclientConfStruct.SharedKeyPathName[0] != '\0')
	{
	   fp = fopen(dimclientConfStruct.SharedKeyPathName,"r");
	   if (!fp)
	   {
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ACCESS, "%s: fopen() error. Check if path set correctly by the -f attribute of dimclient command line.\n", __FUNCTION__);
		)
		return NULL;
	  }
	  if(fscanf(fp, "%s" , sha_key) == 1)
	  { 
	       	DEBUG_OUTPUT (
				dbglog (SVR_DEBUG, DBG_ACCESS, "getSharedKey: sha_key: %s.\n", sha_key);
		)

          }
	  fclose(fp);
	}
	return sha_key;
}



