/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef filetransfer_H
#define filetransfer_H

#ifdef HAVE_FILE

#include "utils.h"

#define FILE_TYPE_STR_LEN		64
#define URL_STR_LEN				256
#define USER_STR_LEN			256
#define PASS_STR_LEN			256
#define TARGET_FILE_NAME_LEN	256
#define FAULT_STR_LEN			256
#define MAX_NAME_SIZE			32
#define WINDOW_MODE_STR_LEN		64
#define USER_MESS_STR_LEN		256

/* Type of file transfers */
enum Type
{
	UPLOAD,
	DOWNLOAD,
	SCHEDULEDOWNLOAD
};

typedef enum transferState
{
	Transfer_NotStarted = 1,
	Transfer_InProgress = 2,
	Transfer_Completed = 3,
	Apply_After_Boot = 4,
	Informed_to_ACS = 5,
	Transfer_Aborted = 6,
	Schedule_DL_Wait_for_Apply = 7
} TransferState;

typedef struct TimeWindowStructType
{
	time_t WindowStartTime;
	time_t WindowEndTime;
	char WindowMode[WINDOW_MODE_STR_LEN+1];
	char UserMessage[USER_MESS_STR_LEN+1];
	int MaxRetries;
} TimeWindowStructType;


/*Start: For XI3, Firmware download profile  */
#define FIRMWARE_DOWNLOAD_STATUS_LENGTH 64
#define FIRMWARE_DOWNLOAD_PATH_NAME "Device.DeviceInfo.X_COMCAST-COM_FirmwareToDownload"
#define FIRMWARE_DOWNLOAD_STATUS_PATH_NAME "Device.DeviceInfo.X_COMCAST-COM_FirmwareDownloadStatus"
extern char* firmwareDownloadStatus[];
typedef enum firmware_Download_Status
{
        eFWDN_Status_Idle=0,
        eFWDN_Status_Initiated,
        eFWDN_Status_InProgress,
        eFWDN_Status_Completed,
        eFWDN_Status_Successful,
        eFWDN_Status_Error
}EFirmwareDownloadStatus;
/*End: For XI3, Firmware download profile  */

/*
 * MaxRetries:
The maximum number of retries for downloading and/or applying the file before regarding the transfer as having failed. Refers only
to this time window (each time window can specify its own value). A value of 0 means “No retries are permitted”.
A value of -1 means “the CPE determines the number of retries”, i.e. that the CPE can use its own retry policy, not that it has to retry forever.
 */
#define MAX_RETRIES_FOR_MINUS_1		3

/* A TransferEntry holds all the data we need to up- or download
 * The data is supported by the ACS */
typedef struct TransferEntry
{
	/* Type of transfer */
	enum Type type;
	/* All Data we got from the ACS */
	char commandKey[CMD_KEY_STR_LEN + 1];
	char fileType[FILE_TYPE_STR_LEN + 1];
	char URL[URL_STR_LEN + 1];
	char username[USER_STR_LEN + 1];
	char password[PASS_STR_LEN + 1];
	unsigned int fileSize;
	char targetFileName[TARGET_FILE_NAME_LEN + 1];
	char successURL[URL_STR_LEN + 1];
	char failureURL[URL_STR_LEN + 1];
	/* time this object was created, also used for persistence */
	struct timeval createTime;
	/* StartTime is calculated from now() + delayedSeconds */
	struct timeval startTime;
	/* CompleteTime */
	struct timeval completeTime;
	/* Transfer State */
	TransferState status;
	/* Fault Handling */
	unsigned int faultCode;
	char faultString[FAULT_STR_LEN + 1];
	char announceURL[URL_STR_LEN + 1];
	char transferURL[URL_STR_LEN + 1];
	Transfers_type initiator;
	/*Index of files, which contents an information about transfered files. */
	int indexOfFile;
	//timewindows for scheduleDownload. If pointer == NULL, then timewindow is not active.
	struct TimeWindowStructType *TimeWindow1;
	struct TimeWindowStructType *TimeWindow2;
} TransferEntry;

/* Callback for download */
typedef int (*DownloadCB)(TransferEntry * te);

/* Callback for scheduleDownload */
typedef int (*ScheduleDownloadCB)(TransferEntry *, int);

/* Callback for upload, called before the upload starts,
 * the callback has to prepare the files for upload, depending on the fileType */
typedef int (*UploadCB)(TransferEntry *);

typedef struct UploadFile
{
	char *filepath;
	char *filename;
	char *filetype;
} UploadFile;

#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_DEPLOYMENT_UNIT)
int doDownload(const char *, char *, char *, int, char *);
int doFtpDownload(const char *, char *, char *, int, char *);
int doHttpDownload(const char *, char *, char *, int, char *);
int initFiletransfer(DownloadCB, ScheduleDownloadCB, UploadCB, DownloadCB, ScheduleDownloadCB, UploadCB);
int isDelayedFiletransfer(void);
int handleDelayedFiletransfers(unsigned int);
int handleDelayedFiletransfersEvents(int *);
int clearDelayedFiletransfers(struct soap *);
void checkTransfersAfterReboot();
#endif /* defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD)  || defined(HAVE_DEPLOYMENT_UNIT)*/


#ifdef HAVE_GET_QUEUED_TRANSFERS
int execGetQueuedTransfers (struct ArrayOfQueuedTransfers *);
#endif /* HAVE_GET_QUEUED_TRANSFERS */

#ifdef HAVE_GET_ALL_QUEUED_TRANSFERS
int execGetAllQueuedTransfers (struct ArrayOfAllQueuedTransfers *);
#endif /* HAVE_GET_ALL_QUEUED_TRANSFERS */

#ifdef	 HAVE_AUTONOMOUS_TRANSFER_COMPLETE
extern bool isAutonomousTransferComplete;
#endif /* HAVE_AUTONOMOUS_TRANSFER_COMPLETE */

#if defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
int freeTransferEntryMemory();
int resetAllFiletransfers(void);
#endif /* HAVE_FILE_DOWNLOAD || HAVE_FILE_SCHEDULE_DOWNLOAD || HAVE_FILE_UPLOAD */

#ifdef HAVE_FILE_DOWNLOAD
int execDownload(struct soap *,
				 char *,
				 char *,
				 char *,
				 char *,
				 char *,
				 unsigned int,
				 char *,
				 unsigned int,
				 char *,
				 char *,
				 Transfers_type,
				 char *,
				 char *,
				 struct cwmp__DownloadResponse * );
#endif /* HAVE_FILE_DOWNLOAD */

#ifdef HAVE_FILE_SCHEDULE_DOWNLOAD
int execScheduleDownload(struct soap *,
		char *,
		char *,
		char *,
		char *,
		char *,
		unsigned int ,
		char *,
		struct ArrayOfTimeWindowsStruct *,
		Transfers_type ,
		char *,
		char *,
		struct cwmp__ScheduleDownloadResponse *);
#endif /* HAVE_FILE_SCHEDULE_DOWNLOAD */

#ifdef HAVE_FILE_UPLOAD
int execUpload(struct soap *,
			   char *,
			   char *,
			   char *,
			   char *,
			   char *,
			   unsigned int,
			   Transfers_type,
			   char *,
			   char *,
			   struct cwmp__UploadResponse * );
#endif /* HAVE_FILE_UPLOAD */

#ifdef HAVE_CANCEL_TRANSFER
int cancelTransferHandler( struct soap *, char * );
#endif /* HAVE_CANCEL_TRANSFER */


const UploadFile *type2file(const char *, unsigned int *);
int make_connect(struct soap *, const char *);

#endif /* HAVE_FILE */

#endif /* filetransfer_H */
