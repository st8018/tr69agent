/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef		parameter_H
#define		parameter_H

#include "utils.h"
#include "soapStub.h"

#define INFO_PARENT_NOT_FOUND (1)
#define ERR_INVALID_PARAM_DESCRIPTION_LINE (2)

/* Definition of some important Parameter names */
#if defined(WITH_DEVICE_ROOT) || defined(TR_181_DEVICE)
//
//	We don't provide Gateway functionality. This device is just a
//	plain device connected to a network. Treat it as such by
//	naming parameters Device and not InternetGatewayDevice
//
#define 	PARAMETER_KEY  					     			"Device.ManagementServer.ParameterKey"
#define 	DEVICE_INFO				    		 			"Device.DeviceInfo."
#define		DEVICE_INFO_MANUFACTURER			 			"Device.DeviceInfo.Manufacturer"
#define		DEVICE_INFO_MANUFACTURER_OUI 		 			"Device.DeviceInfo.ManufacturerOUI"
#define 	DEVICE_INFO_PRODUCT_CLASS		     			"Device.DeviceInfo.ProductClass"
#define		DEVICE_INFO_SERIAL_NUMBER		     			"Device.DeviceInfo.SerialNumber"
#define 	VENDOR_CONFIG_FILE	            		 		"Device.DeviceInfo.VendorConfigFile."
#define 	PERIODIC_INFORM_ENABLE			     			"Device.ManagementServer.PeriodicInformEnable"
#define		PERIODIC_INFORM_INTERVAL		     			"Device.ManagementServer.PeriodicInformInterval"
#define		PERIODIC_INFORM_TIME			     			"Device.ManagementServer.PeriodicInformTime"
#define		MANAGEMENT_SERVER_URL			     			"Device.ManagementServer.URL"
#define 	MANAGEMENT_SERVER_USERNAME		     			"Device.ManagementServer.Username"
#define		MANAGEMENT_SERVER_PASSWORD		     			"Device.ManagementServer.Password"
#define 	CONNECTION_REQUEST_URL			     			"Device.ManagementServer.ConnectionRequestURL"
#define		CONNECTION_REQUEST_USERNAME		     			"Device.ManagementServer.ConnectionRequestUsername"
#define		CONNECTION_REQUEST_PASSWORD		     			"Device.ManagementServer.ConnectionRequestPassword"
#define		ACTIVE_NOTIFICATION_TIME						"Device.ManagementServer.DefaultActiveNotificationThrottle"
#define 	UDPCONNECTIONREQUESTADDRESS                  	"Device.ManagementServer.UDPConnectionRequestAddress"
#define 	NATDETECTED                                  	"Device.ManagementServer.NATDetected"
#define 	UDPCONNECTIONREQUESTADDRESSNOTIFICATIONLIMIT 	"Device.ManagementServer.UDPConnectionRequestAddressNotificationLimit"
#define 	STUNENABLE                                   	"Device.ManagementServer.STUNEnable"
#define 	STUNSERVERADDRESS                            	"Device.ManagementServer.STUNServerAddress"
#define 	STUNSERVERPORT                               	"Device.ManagementServer.STUNServerPort"
#define 	STUNUSERNAME                                 	"Device.ManagementServer.STUNUsername"
#define 	STUNPASSWORD                                 	"Device.ManagementServer.STUNPassword"
#define 	STUNMAXIMUMKEEPALIVEPERIOD                   	"Device.ManagementServer.STUNMaximumKeepAlivePeriod"
#define 	STUNMINIMUMKEEPALIVEPERIOD                   	"Device.ManagementServer.STUNMinimumKeepAlivePeriod"
#define		KICKURL											"Device.ManagementServer.KickURL"
#define 	ALIASBASEDADDRESSING                           	"Device.ManagementServer.AliasBasedAddressing"
#define 	INSTANCEMODE                                   	"Device.ManagementServer.InstanceMode"
#define 	AUTOCREATEINSTANCES                           	"Device.ManagementServer.AutoCreateInstances"
#define		TRANSFERURL										"Device.Dimark.TransferURL"
#define		DEPLOYMENT_UNIT_PATH							"Device.SoftwareModules.DeploymentUnit."
#ifndef TR_181_DEVICE
#define		DEFAULT_GATEWAY									"Device.LAN.DefaultGateway"
#endif
#else
//
// We are actually providing a Gateway, so we need to 
// name our parameters InternetGatewayDevice
//
#define 	PARAMETER_KEY  			  		      	 		"InternetGatewayDevice.ManagementServer.ParameterKey"
#define 	DEVICE_INFO			            		 		"InternetGatewayDevice.DeviceInfo."
#define		DEVICE_INFO_MANUFACTURER	             		"InternetGatewayDevice.DeviceInfo.Manufacturer"
#define		DEVICE_INFO_MANUFACTURER_OUI 	         		"InternetGatewayDevice.DeviceInfo.ManufacturerOUI"
#define 	DEVICE_INFO_PRODUCT_CLASS	             		"InternetGatewayDevice.DeviceInfo.ProductClass"
#define		DEVICE_INFO_SERIAL_NUMBER	             		"InternetGatewayDevice.DeviceInfo.SerialNumber"
#define 	VENDOR_CONFIG_FILE	            		 		"InternetGatewayDevice.DeviceInfo.VendorConfigFile."
#define 	PERIODIC_INFORM_ENABLE		             		"InternetGatewayDevice.ManagementServer.PeriodicInformEnable"
#define		PERIODIC_INFORM_INTERVAL	             		"InternetGatewayDevice.ManagementServer.PeriodicInformInterval"
#define		PERIODIC_INFORM_TIME		             		"InternetGatewayDevice.ManagementServer.PeriodicInformTime"
#define		MANAGEMENT_SERVER_URL		             		"InternetGatewayDevice.ManagementServer.URL"
#define 	MANAGEMENT_SERVER_USERNAME	             		"InternetGatewayDevice.ManagementServer.Username"
#define		MANAGEMENT_SERVER_PASSWORD	 	   		 		"InternetGatewayDevice.ManagementServer.Password"
#define 	CONNECTION_REQUEST_URL			  				"InternetGatewayDevice.ManagementServer.ConnectionRequestURL"
#define		CONNECTION_REQUEST_USERNAME		     			"InternetGatewayDevice.ManagementServer.ConnectionRequestUsername"
#define		CONNECTION_REQUEST_PASSWORD		     			"InternetGatewayDevice.ManagementServer.ConnectionRequestPassword"
#define		ACTIVE_NOTIFICATION_TIME						"InternetGatewayDevice.ManagementServer.DefaultActiveNotificationThrottle"
#define 	UDPCONNECTIONREQUESTADDRESS                  	"InternetGatewayDevice.ManagementServer.UDPConnectionRequestAddress"
#define 	NATDETECTED                                  	"InternetGatewayDevice.ManagementServer.NATDetected"
#define 	UDPCONNECTIONREQUESTADDRESSNOTIFICATIONLIMIT 	"InternetGatewayDevice.ManagementServer.UDPConnectionRequestAddressNotificationLimit"
#define 	STUNENABLE                                   	"InternetGatewayDevice.ManagementServer.STUNEnable"
#define 	STUNSERVERADDRESS                            	"InternetGatewayDevice.ManagementServer.STUNServerAddress"
#define 	STUNSERVERPORT                               	"InternetGatewayDevice.ManagementServer.STUNServerPort"
#define 	STUNUSERNAME                                 	"InternetGatewayDevice.ManagementServer.STUNUsername"
#define 	STUNPASSWORD                                 	"InternetGatewayDevice.ManagementServer.STUNPassword"
#define 	STUNMAXIMUMKEEPALIVEPERIOD                   	"InternetGatewayDevice.ManagementServer.STUNMaximumKeepAlivePeriod"
#define 	STUNMINIMUMKEEPALIVEPERIOD                   	"InternetGatewayDevice.ManagementServer.STUNMinimumKeepAlivePeriod"
#define		KICKURL											"InternetGatewayDevice.ManagementServer.KickURL"
#define 	ALIASBASEDADDRESSING                           	"InternetGatewayDevice.ManagementServer.AliasBasedAddressing"
#define 	INSTANCEMODE                                   	"InternetGatewayDevice.ManagementServer.InstanceMode"
#define 	AUTOCREATEINSTANCES                           	"InternetGatewayDevice.ManagementServer.AutoCreateInstances"
#define		TRANSFERURL										"InternetGatewayDevice.Dimark.TransferURL"
#define		DEPLOYMENT_UNIT_PATH							"InternetGatewayDevice.SoftwareModules.DeploymentUnit."
#endif


#define		RPC_COMMAND_IS_CALLED_FROM_ACS		1
#define		RPC_COMMAND_IS_CALLED_INTERNAL		0

#define		CALL_USER_DEFINED_CALLBACK			1
//use CALL_USER_DEFINED_CALLBACK or !CALL_USER_DEFINED_CALLBACK in the addObjectIntern()/deleteObjectIntern() calls

/* Kind	of Parameter
 */
typedef	enum parameterType
{
	DefaultType			= /*97*//*98*/99,
	ObjectType			= /*98*//*99*/100,								/* old */ /*new+1*/
	MultiObjectType		= /*99*//*100*/101,
	StringType			= SOAP_TYPE_xsd__string,						/*  6  */ /*  7  */ /* 11  */
	DefStringType		= (DefaultType + SOAP_TYPE_xsd__string),		/* 103 */ /* 104 */ /* 110 */
	IntegerType			= SOAP_TYPE_xsd__int,							/*  7  */ /*  8  */ /* 12  */
	DefIntegerType		= (DefaultType + SOAP_TYPE_xsd__int),			/* 104 */ /* 105 */ /* 111 */
	UnsignedIntType		= SOAP_TYPE_xsd__unsignedInt,					/*  9  */ /*  10 */ /* 14  */
	DefUnsignedIntType	= (DefaultType + SOAP_TYPE_xsd__unsignedInt),	/* 106 */ /* 107 */ /* 113 */
	BooleanType			= SOAP_TYPE_xsd__boolean,						/*  18 */ /*  19 */ /* 21  */ /* 16 */
	DefBooleanType		= (DefaultType + SOAP_TYPE_xsd__boolean),		/* 115 */ /* 116 */ /* 120 */ /* 115*/
	DateTimeType		= SOAP_TYPE_xsd__dateTime,						/*  11 */ /*  12 */ /* 10  */
	DefDateTimeType		= (DefaultType + SOAP_TYPE_xsd__dateTime),		/* 108 */ /* 109 */ /* 109 */
	Base64Type			= SOAP_TYPE_xsd__base64,						/*  12 */ /*  13 */ /* 15  */
	DefBase64Type		= (DefaultType + SOAP_TYPE_xsd__base64),		/* 109 */ /* 110 */ /* 114 */
	UnsignedLongType	= SOAP_TYPE_xsd__unsignedLong,					/*  -- */ /*  -- */ /* 25  */ /* 20 */
	DefUnsignedLongType	= (DefaultType + SOAP_TYPE_xsd__unsignedLong),	/*  -- */ /*  -- */ /* 124 */ /* 119*/
	hexBinaryType		= SOAP_TYPE_xsd__hexBinary,						/*  -- */ /*  -- */ /* 26  */ /* 21 */
	DefHexBinaryType	= (DefaultType + SOAP_TYPE_xsd__hexBinary),		/*  -- */ /*  -- */ /* 125 */ /* 120*/
	// MIN value from DefStringType, DefIntegerType, DefUnsignedIntType, DefBooleanType, DefDateTimeType, DefBase64Type:
	// !!!!! Manual filling is necessary !!!!!
	minDefType			= DefDateTimeType // Is DefDateTimeType now.
} ParameterType;

/* Available access types
 */
typedef	enum accessType
{
	ReadOnly = 0,
	ReadWrite = 1,
	WriteOnly = 2,
	NotReadNotWrite = 3
} AccessType;

/* Available Reboot types
 * If rebootIdx of Parameter is set to some index > 0,
 * a reboot has to initiated after all parameters are set during a SetParameterValues() call.
 * An index must be a power of 2 (0, 1, 2, 4, 8, 16, 32....) or a sum of the power of 2 (ex. 1+4+32)
 * For example: if rebootIdx = "2" then reboot-function #2 from the rebootArray[] will be called.
 * If rebootIdx = "10" then reboot-function #2 will be called first then reboot-function #8 will be called second.
 */
typedef	enum rebootType
{
	NoReboot = 0,
	//for example Reboot1:
//	Reboot1 = 1, // least idx - will be executed first

	RebootWithoutFactoryReset = 0x2000, //dec: 8192
	FactoryReset = 0x4000,  // dec: 16384. Greatest idx - will be executed last
//	FlashImageReboot = 0x8000,  //Comcast patch for flashing image
	maxRebootIdxValue = /*Reboot1 |*/ RebootWithoutFactoryReset | FactoryReset // MUST be edited when RebootType enum is edited. Use all reboot types.
} RebootType;

/* Here you can define a sum of callback function indexes for Reboot/Factory Reset behavior of Dimclient */
#ifndef REBOOT_CALLBACKS_INDEX_SUM
#define REBOOT_CALLBACKS_INDEX_SUM  (RebootWithoutFactoryReset + FactoryReset/*+ <some your index> +...*/ )
#endif

#ifndef FACTORYRESET_CALLBACKS_INDEX_SUM
#define FACTORYRESET_CALLBACKS_INDEX_SUM  (FactoryReset /*+ <some your index> +...*/ )
#endif

/* Available notification types as bit coded values
 */
typedef	enum notificationType
{
	NotificationNone = 0,
	NotificationPassive = 1,
	NotificationActive = 2,
	NotificationAllways = 4
} NotificationType;

/* Available notification change types
 */ 
typedef	enum notificationMax
{
	NotificationMaxNone = 0 ,
	NotificationMaxPassive = 1,
	NotificationMaxActive = 2,
	NotificationMaxAllways = 3,
	NotificationNotChangeable = 4
} NotificationMax;

/* Available status
 */
typedef	enum statusType
{
	ValueNotChanged =	0,
	ValueModifiedNotFromACS = 1,
	ValueModifiedFromACS = 2
} StatusType;

/* Available persistence types
 */
typedef	enum persitenceType
{
	ValuePersistent,
	ValueTransient
}	PersitenceType;

/**	Returns	a array	of parameters in parameters	*/
int	getParameters( const struct ArrayOfString *, struct ArrayOfParameterValueStruct * );
int	setParameters( struct ArrayOfParameterValueStruct *, int * );

int	setParameter( const	char *,	void * );
int	getParameter( const	char *,	void * );

/**	Returns	a array	of parameters in parameters	*/
int	getParameterNames( const xsd__string, xsd__boolean, struct ArrayOfParameterInfoStruct *);

/**	Returns	a array	of parameters in parameters	*/
int	getInformParameters( struct	ArrayOfParameterValueStruct *);
void resetParameterStatus( void );

int	setParametersAttributes( struct	ArrayOfSetParameterAttributesStruct * );
int	getParametersAttributes( const struct ArrayOfString *, struct ArrayOfParameterAttributeStruct * );
int getParameter2Host( const char *, ParameterType *, AccessType *, void * );
int setParameter2Host( const char *, bool *, char * );
int addObjectIntern( const char *, unsigned int *, unsigned int, unsigned int );
int	addObject( const char * , struct cwmp__AddObjectResponse * );
int	deleteObject( const	char *,	struct cwmp__DeleteObjectResponse * );
int deleteObjectIntern( const char *, unsigned int, unsigned int );
int deleteAllInstancesOfMultiObject(const void * , const char *, unsigned int, unsigned int );
int countInstances( const char *, int * );

int setUniversalParamValueInternal(void *, const char * , unsigned int , ...);

/**	Returns	the	number of parameters in	the	parameter list */
int	getParameterCount( void );
void printModifiedParameterList( void );

/** Load the Parameters from the init file and the written parameter files */
int	loadParameters( bool );
/** Delete all Parameters from disk, used by factory reset function */
int resetAllParameters( void );
/* Check Passive Notification */
void checkPassiveNotification();
/* Check Active Notification */
int checkActiveNotification();
/* helper function print all tree */
void printAllTree();

void readAllMarkedParamsToCreateInstancesOnStartUp();

/* Create new parameter or object */
int createNewParameter( const char *, ParameterType , int , RebootType , NotificationType , NotificationMax , int , int , int ,
						char *, char * , const bool,const char *, const char *, const char *);
/* Delete parameter or object */
int deleteNewParameter(const char *);

int isParameterExists(const char *);

int getParamIdNotificationAccessList(const char * , unsigned int * , unsigned int * , char *** , int * );
char *getNumOfEntriesParamPathByObjectPathName(const char *);

#endif /* parameter_H */
