/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef methods_H
#define methods_H

#import "gsoap/struct_timeval.h"

//gsoap cwmp service name:	dim
//gsoap cwmp service style:	rpc
//gsoap cwmp service encoding:	encoded

/* Define schema types */
typedef char* xsd__string;
typedef int xsd__int;
typedef unsigned int xsd__unsignedInt;
/*typedef time_t xsd__dateTime;*/

/*typedef char* xsd__base64Binary;*/
/*typedef struct xsd__base64
{
	unsigned char *__ptr;
	int __size;
} xsd__base64;
*/
typedef char *xsd__base64;

/* allows to handle 0|1|true|false as boolean values coming from the ACS the client is still sending 0|1 */
typedef enum xsd__boolean
{
	_0,
	_1,
	false_ = 0,
	true_ = 1
} xsd__boolean;

/* define a special boolean type for the SOAP Header
 * because HoldRequests are only allowed in the message from ACS to CPE
 * but not from CPE to ACS. Therefore we need a custom implementation
 * to ignore this value in the outgoing messages */
extern typedef xsd__boolean xsd__HoldRequestBoolean;

typedef unsigned long xsd__unsignedLong;
typedef char *xsd__hexBinary;

typedef struct cwmp__ArgStruct
{
	xsd__string Name;
	xsd__string Value;
} cwmp__ArgStruct;

typedef struct SetParameterFaultStruct
{
	xsd__string ParameterName;
	xsd__unsignedInt FaultCode;
	xsd__string FaultString;
} SetParameterFaultStruct;

/* EventStruct Definition */
typedef struct EventStruct
{
	xsd__string EventCode;
	xsd__string CommandKey;
} cwmp__EventStruct;

typedef struct Option
{
	xsd__string OptionName;
	xsd__string VoucherSN;
	xsd__unsignedInt State;
	xsd__int Mode;
	xsd__dateTime StartDate;
	xsd__dateTime ExpirationDate;
	xsd__boolean IsTransferable;
} cwmp__Option;

typedef struct AllQueuedTransferStruct
{
	xsd__string CommandKey;
	xsd__int State;
	xsd__boolean IsDownload;
	xsd__string FileType;
	xsd__unsignedInt FileSize;
	xsd__string TargetFileName;
} cwmp__AllQueuedTransferStruct;

typedef struct QueuedTransferStruct
{
	xsd__string CommandKey;
	xsd__int State;
} cwmp__QueuedTransferStruct;

typedef struct ParameterAttributeStruct
{
	xsd__string Name;
	xsd__int Notification;
	struct ArrayOfString *AccessList;
} cwmp__ParameterAttributeStruct;

typedef struct SetParameterAttributesStruct
{
	xsd__string Name;
	xsd__boolean NotificationChange;
	xsd__int Notification;
	xsd__boolean AccessListChange;
	struct ArrayOfString *AccessList;
} cwmp__SetParameterAttributesStruct;

typedef struct ParameterInfoStruct
{
	xsd__string Name;
	xsd__boolean Writable;
} cwmp__ParameterInfoStruct;

typedef struct ParameterValueStruct
{
	xsd__string Name;
	xsd__int __typeOfValue;
	void *Value;
} cwmp__ParameterValueStruct;

struct ArrayOfString
{
	xsd__string *__ptrstring;
	xsd__int __size;
};

struct ArrayOfParameterValueStruct
{
	cwmp__ParameterValueStruct **__ptrParameterValueStruct;
	xsd__int __size;
};

struct ArrayOfParameterInfoStruct
{
	cwmp__ParameterInfoStruct **__ptrParameterInfoStruct;
	xsd__int __size;
};

struct ArrayOfSetParameterAttributesStruct
{
	cwmp__SetParameterAttributesStruct **__ptrSetParameterAttributesStruct;
	xsd__int __size;
};

struct cwmp__SetParameterAttributesResponse
{
	void *empty;
};

struct ArrayOfParameterAttributeStruct
{
	cwmp__ParameterAttributeStruct **__ptrParameterAttributeStruct;
	xsd__int __size;
};

struct cwmp__AddObjectResponse
{
	xsd__unsignedInt InstanceNumber;
	xsd__int Status;
};

struct cwmp__DeleteObjectResponse
{
	 xsd__int Status;
};

struct cwmp__RebootResponse
{
	void *empty;
};

typedef struct cwmp__DownloadResponse
{
	xsd__int Status;
	/* use xsd__string o/w we can not return an "Unknown Time" 0001-01-01T00:00:00Z */
	xsd__string StartTime;
	xsd__string CompleteTime;
};

typedef struct cwmp__UploadResponse
{
	xsd__int Status;
	/* use xsd__string o/w we can not return an "Unknown Time" 0001-01-01T00:00:00Z */
	xsd__string StartTime;
	xsd__string CompleteTime;
};

struct cwmp__FactoryResetResponse
{
	void *empty;
};

struct ArrayOfQueuedTransfers
{
	cwmp__QueuedTransferStruct **__ptrQueuedTransferStruct;
	xsd__int __size;
};

struct ArrayOfAllQueuedTransfers
{
	cwmp__AllQueuedTransferStruct **__ptrAllQueuedTransferStruct;
	xsd__int __size;
};

struct cwmp__ScheduleInformResponse
{
	void *empty;
};

struct ArrayOfVouchers
{
	xsd__base64 __ptrVoucher;
	xsd__int __size;
};

struct cwmp__SetVouchersResponse
{
	void *empty;
};

struct ArrayOfOptions
{
	cwmp__Option **__ptrOptionStruct;
	xsd__int __size;
};

/* DeviceId Definition used by Inform() */
typedef struct DeviceId
{
	xsd__string Manufacturer;
	xsd__string OUI;
	xsd__string ProductClass;
	xsd__string SerialNumber;
} cwmp__DeviceId;

/* Array of EventStructs used by Inform() */
struct ArrayOfEventStruct
{
	cwmp__EventStruct **__ptrEventStruct;
	xsd__int __size;
};

/* Fault Structure Definitions*/
typedef struct cwmp__Fault
{
	xsd__unsignedInt FaultCode;
	xsd__string FaultString;
	int __sizeParameterValuesFault;
	SetParameterFaultStruct **SetParameterValuesFault;
} Fault;

struct cwmp__TransferCompleteResponse
{
	void *empty;
};

struct cwmp__AutonomousTransferCompleteResponse
{
	void *empty;
};

struct ArrayOfArgs
{
	cwmp__ArgStruct **__ptrArgStruct;
	xsd__int __size;
};

struct cwmp__RequestDownloadResponse
{
	void *empty;
};

/* Soap Header structure */
struct SOAP_ENV__Header
{
	mustUnderstand xsd__string cwmp__ID;
	mustUnderstand xsd__HoldRequestBoolean cwmp__HoldRequests;
	xsd__unsignedInt *cwmp__SessionTimeout;
/*  NoMoreRequests is deprecated. see TR_121
    xsd__boolean cwmp__NoMoreRequests; */
};

/* Fault detail structure */
typedef struct SOAP_ENV__Detail
{
	Fault *cwmp__Fault;
} cwmp__Detail;

struct SOAP_ENV__Code
{
	xsd__string SOAP_ENV__Value;
	struct SOAP_ENV__Code *SOAP_ENV__Subcode;
	xsd__string SOAP_ENV__Role;
};

struct OptionStruct
{
	xsd__string VSerialNum;
	struct DeviceId *DeviceId;
	xsd__string OptionIdent;
	xsd__string OptionDesc;
	xsd__dateTime StartDate;
	xsd__int Duration;
	xsd__string DurationUnits;
	xsd__string Mode;
	xsd__boolean Transferable;
};

typedef struct Object
{
	struct OptionStruct *Option;
} Object;

struct Signature
{
	int __size;
	Object *__ptrObject;
};

typedef struct TimeWindowStruct
{
	xsd__unsignedInt WindowStart;
	xsd__unsignedInt WindowEnd;
	xsd__string WindowMode;
	xsd__string UserMessage;
	xsd__int MaxRetries;
} TimeWindowStruct;

struct ArrayOfTimeWindowsStruct
{
	TimeWindowStruct **__ptrTimeWindowStruct;
	xsd__int __size;
};

typedef struct cwmp__ScheduleDownloadResponse
{
	void *empty;
};

struct cwmp__CancelTransferResponse
{
	void *empty;
};

typedef struct InstallOpStruct
{
	xsd__string URL;
	xsd__string UUID;
	xsd__string Username;
	xsd__string Password;
	xsd__string ExecutionEnvRef;
} InstallOpStruct;
typedef InstallOpStruct OperationStruct;

typedef struct UpdateOpStruct
{
	xsd__string UUID;
	xsd__string Version;
	xsd__string URL;
	xsd__string Username;
	xsd__string Password;
} UpdateOpStruct;
//typedef UpdateOpStruct OperationStruct;

typedef struct UninstallOpStruct
{
	xsd__string UUID;
	xsd__string Version;
	xsd__string ExecutionEnvRef;
} UninstallOpStruct;
//typedef UninstallOpStruct OperationStruct;


struct ArrayOfOperationStruct
{
	OperationStruct *__ptrOperationStruct;
	xsd__int __size;
};

struct cwmp__ChangeDUStateResponse
{
	void *empty;
};

typedef struct Fault_Struct
{
	xsd__unsignedInt FaultCode;
	xsd__string FaultString;
} Fault_Struct;

typedef struct OpResultStruct
{
	xsd__string UUID;
	xsd__string DeploymentUnitRef;
	xsd__string Version;
	xsd__string CurrentState;
	xsd__boolean Resolved;
	xsd__string ExecutionUnitRefList;
	xsd__dateTime StartTime;
	xsd__dateTime CompleteTime;
	Fault fault;
} OpResultStruct;

struct ArrayOfOpResultStruct
{
	OpResultStruct **__ptrOpResultStruct;
	xsd__int __size;
};

struct cwmp__DUStateChangeCompleteResponse
{
	void *empty;
};

typedef struct AutonOpResultStruct
{
	xsd__string UUID;
	xsd__string DeploymentUnitRef;
	xsd__string Version;
	xsd__string CurrentState;
	xsd__boolean Resolved;
	xsd__string ExecutionUnitRefList;
	xsd__dateTime StartTime;
	xsd__dateTime CompleteTime;
	Fault fault;
	xsd__string OperationPerformed;
} AutonOpResultStruct;

struct ArrayOfAutonOpResultStruct
{
	AutonOpResultStruct ** __ptrAutonOpResultStruct;
	xsd__int __size;
};

struct cwmp__AutonomousDUStateChangeCompleteResponse
{
	void *empty;
};

struct cwmp__InformResponse
{
	xsd__unsignedInt MaxEnvelopes;
};

struct cwmp__SetParameterValuesResponse
{
	xsd__int Status;
};

struct cwmp__GetRPCMethodsResponse
{
	struct ArrayOfString MethodList;
};

struct cwmp__KickedResponse
{
	xsd__string NextURL;
};

struct cwmp__GetParameterValuesResponse
{
	struct ArrayOfParameterValueStruct ParameterList;
};

struct cwmp__GetParameterNamesResponse
{
	struct ArrayOfParameterInfoStruct ParameterList;
};

struct cwmp__GetParameterAttributesResponse
{
	struct ArrayOfParameterAttributeStruct ParameterList;
};

struct cwmp__GetQueuedTransfersResponse
{
	struct ArrayOfQueuedTransfers TransferList;
};

struct cwmp__GetAllQueuedTransfersResponse
{
	struct ArrayOfAllQueuedTransfers TransferList;
};

struct cwmp__GetOptionsResponse
{
	struct ArrayOfOptions OptionList;
};


/* San 10.12.2012:
  Type attribute is defined:
    <ParameterValueStruct>
        <Name>InternetGatewayDevice.ManagementServer.PeriodicInformInterval</Name>
        <Value xsi:type="xsd:unsignedInt">31</Value>
    </ParameterValueStruct>

    Type attribute isn't defined:
    <ParameterValueStruct>
        <Name>InternetGatewayDevice.ManagementServer.PeriodicInformInterval</Name>
        <Value>31</Value>
    </ParameterValueStruct>

 If xsi:type attribute isn't defined in the SPV, then gSoap handles tag "Value" as simple type.
 So, I have added the type 'Value' as string (char *).
 In this case the value will be handled as String.
*/
typedef char* Value;

/* CPE methods */
int cwmp__GetRPCMethods(struct cwmp__GetRPCMethodsResponse *response);
int cwmp__SetParameterValues(struct ArrayOfParameterValueStruct *ParameterList, xsd__string ParameterKey, struct cwmp__SetParameterValuesResponse *response);
int cwmp__GetParameterValues(struct ArrayOfString *ParameterNames, struct cwmp__GetParameterValuesResponse *response);
int cwmp__GetParameterNames(xsd__string ParameterPath, xsd__boolean NextLevel, struct cwmp__GetParameterNamesResponse *response);
int cwmp__SetParameterAttributes(struct ArrayOfSetParameterAttributesStruct *ParameterList, struct cwmp__SetParameterAttributesResponse *emptyRes);
int cwmp__GetParameterAttributes(struct ArrayOfString *ParameterNames, struct cwmp__GetParameterAttributesResponse *response);
int cwmp__AddObject(xsd__string ObjectName, xsd__string ParameterKey, struct cwmp__AddObjectResponse *ReturnValue);
int cwmp__DeleteObject(xsd__string ObjectName, xsd__string ParameterKey, struct cwmp__DeleteObjectResponse *ReturnValue);
int cwmp__Reboot(xsd__string CommandKey, struct cwmp__RebootResponse *emptyRes);
int cwmp__Download(xsd__string CommandKey,
				   xsd__string FileType,
				   xsd__string URL,
				   xsd__string Username,
				   xsd__string Password,
				   unsigned int FileSize,
				   xsd__string TargetFileName,
				   unsigned int DelaySeconds,
				   xsd__string SuccessURL,
				   xsd__string FailureURL,
				   struct cwmp__DownloadResponse *response);
int cwmp__Upload(xsd__string CommandKey,
				 xsd__string FileType,
				 xsd__string URL,
				 xsd__string Username,
				 xsd__string Password,
				 xsd__unsignedInt DelaySeconds,
				 struct cwmp__UploadResponse *response);
int cwmp__ScheduleDownload(xsd__string CommandKey,
						   xsd__string FileType,
						   xsd__string URL,
						   xsd__string Username,
						   xsd__string Password,
						   unsigned int FileSize,
						   xsd__string TargetFileName,
						   struct ArrayOfTimeWindowsStruct *TimeWindowList,
						   struct cwmp__ScheduleDownloadResponse *emptyRes);
int cwmp__CancelTransfer(xsd__string CommandKey, struct cwmp__CancelTransferResponse *emptyRes);
int cwmp__ChangeDUState(xsd__string CommandKey, struct ArrayOfOperationStruct * Operations, struct cwmp__ChangeDUStateResponse *emptyRes);
int cwmp__FactoryReset(void *emptyReq, struct cwmp__FactoryResetResponse *emptyRes);
int cwmp__GetQueuedTransfers(void *empty, struct cwmp__GetQueuedTransfersResponse *response);
int cwmp__GetAllQueuedTransfers(void *empty, struct cwmp__GetAllQueuedTransfersResponse *response);
int cwmp__ScheduleInform(xsd__unsignedInt DelaySeconds, xsd__string CommandKey, struct cwmp__ScheduleInformResponse *emptyRes);
int cwmp__SetVouchers(struct ArrayOfVouchers *VoucherList, struct cwmp__SetVouchersResponse *emptyRes);
int cwmp__GetOptions(xsd__string OptionName, struct cwmp__GetOptionsResponse *response);


/* ACS methods */

int cwmp__Inform(struct DeviceId *DeviceId,
				 struct ArrayOfEventStruct *Event,
				 int MaxEnvelopes,
				 xsd__dateTime CurrentTime,
				 int RetryCount,
				 struct ArrayOfParameterValueStruct *ParameterList,
				 struct cwmp__InformResponse *response);

int cwmp__TransferComplete(xsd__string CommandKey,
						   struct cwmp__Fault *FaultStruct,
						   xsd__dateTime StartTime,
						   xsd__dateTime CompleteTime,
						   struct cwmp__TransferCompleteResponse *emptyRes);

int cwmp__AutonomousTransferComplete(xsd__string AnnounceURL,
									 xsd__string TransferURL,
									 xsd__boolean IsDownload,
									 xsd__string FileType,
									 xsd__unsignedInt FileSize,
									 xsd__string TargetFileName,
									 struct cwmp__Fault *FaultStruct,
									 xsd__dateTime StartTime,
									 xsd__dateTime CompleteTime,
									 struct cwmp__AutonomousTransferCompleteResponse *emptyRes);

int cwmp__RequestDownload(xsd__string FileType, struct ArrayOfArgs *FileTypeArg, struct cwmp__RequestDownloadResponse *emptyRes);
int cwmp__Kicked(xsd__string Command, xsd__string Referer, xsd__string Arg, xsd__string Next, struct cwmp__KickedResponse *response);
int cwmp__DUStateChangeComplete(struct ArrayOfOpResultStruct * Results, xsd__string CommandKey, struct cwmp__DUStateChangeCompleteResponse *emptyRes);
int cwmp__AutonomousDUStateChangeComplete(struct ArrayOfAutonOpResultStruct * Results, struct cwmp__AutonomousDUStateChangeCompleteResponse *emptyRes);


#endif /* methods_H */
