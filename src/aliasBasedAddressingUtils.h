/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                      *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef aliasBasedAddressingUtils_H
#define aliasBasedAddressingUtils_H

#include "dimark_globals.h"

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM

typedef enum
{
    InstanceNumber,
    InstanceAlias
} InstanceMode;

void setAliasBasedAddressing(int );
int getAliasBasedAddressing();
void setInstanceMode(InstanceMode );
InstanceMode getInstanceMode();
void setAutoCreateInstances(int );
int getAutoCreateInstances();

int readAliasBasedAddressingInfoFromDB();

char * correctParamPathNameInResponse(char *, char *);
int isAliasValueValid(char *);

#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */

#endif /* aliasBasedAddressingUtils_H */
