/***************************************************************************
 *    Original code ©Copyright (C) 2004-2012 by Dimark Software Inc.       *
 *    support@dimark.com                                                   *
 *    Modifications © 2013 Comcast Cable Communications, LLC               *
 ***************************************************************************/

#ifndef DIMARK_GLOBALS_H
#define DIMARK_GLOBALS_H

#include <pthread.h>
#include <stddef.h>
//#include <assert.h>

#include "integration_interfaces/integrationSettings.h"
#include "debug.h"
#include "parameter.h"
#include "paramaccess.h"
#include "soapStub.h"
#include "DBhandler.h"
#include "memory_manager.h"

// This CWMP version will be used in the first Inform RPC after reboot by default.
// Change CWMP_DEFAULT_VERSION value if you want to compile the client with other default CWMP version.
// Use "urn:dslforum-org:cwmp-1-3" or "urn:dslforum-org:cwmp-1-2" or "urn:dslforum-org:cwmp-1-1" or "urn:dslforum-org:cwmp-1-0"
// !!! NOTE: If you add new cwmp version (cwmp-1-4, etc.) you MUST add the handling of it in the function setSuportedRPCInfoFromCWMPVersion() !!!
#ifndef CWMP_DEFAULT_VERSION
#define CWMP_DEFAULT_VERSION "urn:dslforum-org:cwmp-1-3"
#endif

// San: for tr-181 data-model supporting
#ifdef TR_181_DEVICE
	#ifndef WITH_DEVICE_ROOT
		#define WITH_DEVICE_ROOT
	#endif
	#ifndef DEVICE_WITHOUT_GATEWAY
		#define DEVICE_WITHOUT_GATEWAY
	#endif
	#undef WITH_GATEWAY_ROOT
#endif


/* TR standard requiers:
 *  When there is a SOAP response in an HTTP Request, or when there is a SOAP Fault response in an HTTP Request,
 *  the SOAPAction header in the HTTP Request MUST have no value (with no quotes), indicating that this header
 *  provides no information as to the intent of the message.
 *  That is, it MUST appear as follows:
 *  	SOAPAction:
 *  So, we send follow line as function parameter and gSOAP code will handle SOAPAction: field as it is needed for us.
 * */
#define DO_NOT_SEND_SOAPACTION "DO_NOT_SEND_SOAPACTION"

/* define NO_LOCAL_REBOOT_ON_CHANGE to ignore the reboot flag in the data-model.xml file
 * the reboot must be done by the ACS 
 * therefore a status return code of 1 is returned to signal that the parameter change
 * has be done but not confirmed yet. */
#define NO_LOCAL_REBOOT_ON_CHANGE
#undef NO_LOCAL_REBOOT_ON_CHANGE // San: I undefined this flag for reboot execution after SPV for some parameters

/* Attention! Connection Request is REQUIRED in a CPE
 * but there is an opportunity to switch it off if it is not needed
 * it can save many memory */
#define HAVE_CONNECTION_REQUEST

/* Enable notification */
#define	HAVE_NOTIFICATION

/* if WITH_PRINT_MEM_MSG is defined, print to log-screen messages about allocating and freeing memory.
 */
//#define WITH_PRINT_MEM_MSG


/* if VOIP_DEVICE is defined then VoIP device (tr104) is enabled.
 * But functionality VoIP isn't used at present. So, it isn't recommended to define the VOIP_DEVICE definition.
 * VOIP_DEVICE enabling requires the implementation of the func freeVoIPAllocatedMemory() -
 * - freeing memory after using VoIP Parameters and devices.
 * */
#undef VOIP_DEVICE
//#define VOIP_DEVICE

// if LONG_COMMAND_LINE_OPTION_ENABLED then dimclient code will try to be compiled with a support of getopt_long() usage.
#define LONG_COMMAND_LINE_OPTION_ENABLED

#define MAX_REDIRECT_COUNT	5

/* define or undefine one or more of the following defines to activate the function */
#define HAVE_OPTIONAL

#ifdef HAVE_OPTIONAL
	#define HAVE_HOST
	#define HAVE_RPC_METHODS
	#define HAVE_VOUCHERS_OPTIONS
	#define HAVE_FACTORY_RESET
	//#define HAVE_KICKED
	//#define HAVE_ALIAS_BASED_ADDR_MECHANISM
	#define HAVE_SCHEDULE_INFORM
	#define HAVE_FILE
	#ifdef HAVE_FILE
		#define HAVE_FILE_DOWNLOAD
		#define HAVE_FILE_UPLOAD
		#define HAVE_FILE_SCHEDULE_DOWNLOAD
		#define HAVE_CANCEL_TRANSFER
		#define HAVE_REQUEST_DOWNLOAD
		#define HAVE_GET_QUEUED_TRANSFERS
		#define HAVE_GET_ALL_QUEUED_TRANSFERS
		#ifdef HAVE_GET_ALL_QUEUED_TRANSFERS
			#define HAVE_AUTONOMOUS_TRANSFER_COMPLETE
		#endif /* HAVE_GET_ALL_QUEUED_TRANSFERS */
		#define HAVE_DEPLOYMENT_UNIT
		#ifdef  HAVE_DEPLOYMENT_UNIT
			#define HAVE_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE
		#endif /* HAVE_DEPLOYMENT_UNIT */
	#endif /* HAVE_FILE */
	#define HAVE_DIAGNOSTICS
	#ifdef HAVE_DIAGNOSTICS
		#define HAVE_UDP_ECHO
		#define HAVE_IP_PING_DIAGNOSTICS
		#define HAVE_TRACE_ROUTE_DIAGNOSTICS
		#define HAVE_WANDSL_DIAGNOSTICS
		#define HAVE_ATMF5_DIAGNOSTICS
		#ifdef HAVE_FILE_DOWNLOAD
			#define HAVE_DOWNLOAD_DIAGNOSTICS
		#endif /* HAVE_FILE_DOWNLOAD */
		#ifdef HAVE_FILE_UPLOAD
			#define HAVE_UPLOAD_DIAGNOSTICS
		#endif /* HAVE_FILE_UPLOAD */
	#endif /* HAVE_DIAGNOSTICS */
#endif /* HAVE_OPTIONAL */


/* Define the following definition, if the client should
 * automatically update the NumberOfEntries of an Object
 * The update happens during an AddObject or DeleteObject */
#define HANDLE_NUMBERS_OF_ENTRIES
#define NUMBER_OF_ENTRIES_STR		"NumberOfEntries"

#define CONNECTION_REALM			"Dimark"

/* Host notification port */
#define HOST_NOTIFICATION_PORT			1			/* 8081 */
/* URL ports for host and ACS notification */
#define	ACS_NOTIFICATION_PORT			2			/* 8082 */
#define CR_URL_PATH_LEN 				8
/* URL ports for host and ACS notification */
#define	KICKED_NOTIFICATION_PORT		4			/* 8084 */
/* The default server port */
#define	UDP_ECHO_DEFAULT_PORT			8			/* 8088 */
#define	TRANSFERS_NOTIFICATION_PORT		16			/* 8096 */
#define ACS_UDP_NOTIFICATION_PORT		16000		// UDP port for listening UDP Connection Request
#define MAX_PATH_NAME_SIZE				400
#define MAX_PARAM_PATH_LENGTH 			257
#define	MAX_EVENT_LIST_NUM				64
#define	CMD_KEY_STR_LEN					32
#define	MAX_CONF_FILE_SRT_LEN   		128
#define	MAX_ENTRY_PARAM_SIZE			12
#define	MAX_PARAM_VALUE_SIZE			61430
#define	MAX_ACCESS_LIST_SIZE			50
#define	MAX_ALIAS_VALUE_SIZE			65 			// 64 + '\0'

#ifndef DEFAULT_SESSION_TIMEOUT_IN_HEADER
#define	DEFAULT_SESSION_TIMEOUT_IN_HEADER	30
#endif

/* Max count of backup of log files: */
#ifndef BACKUP_FILES_DEFAULT_COUNT
#define BACKUP_FILES_DEFAULT_COUNT		3
#endif

#ifndef DEBUG_LOG_FILE_NAME
#define	DEBUG_LOG_FILE_NAME				"HTTP.log"
#endif

#ifndef TEST_LOG_FILE_NAME
#define	TEST_LOG_FILE_NAME				"TEST.log"
#endif

#ifndef CR_URL_PATH_STORAGE_FILE_NAME
#define	CR_URL_PATH_STORAGE_FILE_NAME	"CR_URL_path.txt"
#endif

#define CLIENT_AUTHENTICATION_KEYFILE_NAME		"client.pem"
#define CLIENT_AUTHENTICATION_KEYFILE_PASSWORD	"password"
#define SERVER_AUTHENTICATION_CACERT_FILE_NAME	"ca.crt"
//#define SERVER_AUTHENTICATION_CACERT_FILE_NAME	"/usr/share/tr69/certs/comcast_acs_ca.crt"

/* Definition of "Unknown Time" from TR121 */
#define UNKNOWN_TIME					"0001-01-01T00:00:00.000000Z"
#define DATE_TIME_LENGHT				36

/* Definition of all ErrorCodes */
#define NO_ERROR						0
#define OK								0
#define DIAG_ERROR						-1

#define ERR_METHOD_NOT_SUPPORTED 						9000
#define ERR_REQUEST_DENIED								9001
#define ERR_INTERNAL_ERROR								9002
#define ERR_INVALID_ARGUMENT							9003
#define ERR_RESOURCE_EXCEEDED							9004
#define ERR_INVALID_PARAMETER_NAME						9005
#define ERR_INVALID_PARAMETER_TYPE						9006
#define ERR_INVALID_PARAMETER_VALUE						9007
#define ERR_READONLY_PARAMETER    						9008
#define ERR_NOTIFICATION_REQ_REJECT						9009
#define ERR_DOWNLOAD_FAILURE							9010
#define ERR_UPLOAD_FAILURE								9011
#define ERR_TRANS_AUTH_FAILURE							9012
#define ERR_NO_TRANS_PROTOCOL							9013
#define ERR_JOIN_MULTICAST_GROUP						9014	/* Download failure: unable to join multicast group */
#define ERR_CONTACT_FILE_SERVER							9015	/* Download failure: unable to contact file server */
#define ERR_ACCESS_FILE									9016	/* Download failure: unable to access file */
#define ERR_COMPLETE_DOWNLOAD							9017	/* Download failure: unable to complete download */
#define ERR_CORRUPTED									9018	/* Download failure: file corrupted */
#define ERR_FILE_AUTHENTICATION							9019	/* Download failure: file authentication failure */
#define ERR_COMPLETE_DOWNLOAD_TIME_WINDOWS				9020	/* Download failure: unable to complete download within specified time windows */
#define ERR_CANCELATION_NOT_PERMITTED					9021	/* Cancelation of file transfer not permitted  */
#define ERR_INVALID_UUID_FORMAT							9022	/* Invalid UUID format */
#define ERR_UNKNOWN_EXEC_ENVIRONMENT					9023	/* Unknown Execution Environment */
#define ERR_DISABLE_EXEC_ENVIRONMENT					9024	/* Disabled Execution Environment */
#define ERR_DU_EXEC_ENVIRONMENT_MISHMATCH				9025	/* Deployment Unit to Execution Environment Mistmatch */
#define	ERR_DUPLICATE_DU								9026	/* Duplicate Deployment Unit  */
#define ERR_SYSTEM_RESOURCES_EXCEEDED					9027	/* System Resources Exceeded */
#define ERR_UNKNOWN_DU									9028	/* Unknown Deployment Unit */
#define ERR_INVALID_DU_STATE							9029	/* Invalid Deployment Unit State */
#define	ERR_INVALID_DU_UPDATE_DOWNGRADE_NOT_PERMITTED	9030	/* Invalid Deployment Unit Update - Downgrade not permitted */
#define	ERR_INVALID_DU_UPDATE_VERSION_NOT_SPECIFIED		9031	/* Invalid Deployment Unit Update - Version not specified */
#define	ERR_INVALID_DU_UPDATE_VERSION_ALREADY_EXISTS	9032	/* Invalid Deployment Unit Update - Version already exists */

#define ERR_NO_INFORM_DONE				9890
#define ERR_DIM_EVENT_WRITE				9800	/* Error during opening for writing the event storage file */
#define ERR_DIM_EVENT_READ				9801	/* Error during opening for reading or reading the event storage */
#define ERR_DIM_MARKER_OP				9805	/* Error during creating or deleting one of markers for detecting boot or bootstrap */

/* Error during reading or writing the file transfer information from or into the storage */
#define 	ERR_DIM_TRANSFERLIST_WRITE	9810
#define 	ERR_DIM_TRANSFERLIST_READ	9811

/* Error during handling options */
#define 	ERR_INVALID_OPTION_NAME		9820
#define		ERR_CANT_DELETE_OPTION		9821
#define 	ERR_READ_OPTION				9822
#define 	ERR_WRITE_OPTION			9823

/*  Error reading the initial parameter file or data from storage or parameter metadata from storage */
#define 	ERR_READ_PARAMFILE			9830
/* Error during writing data into storage or parameter metadata into storage */
#define 	ERR_WRITE_PARAMFILE			9831

/* Definition of all EventCodes */
/* Cumulative Behavior - Single */
#define EV_BOOTSTRAP							"0 BOOTSTRAP"
#define EV_BOOT									"1 BOOT"
#define EV_PERIODIC								"2 PERIODIC"
#define EV_SCHEDULED							"3 SCHEDULED"
#define EV_VALUE_CHANGE							"4 VALUE CHANGE"
#ifdef HAVE_KICKED
#define EV_KICKED								"5 KICKED"
#endif /* HAVE_KICKED */
#define EV_CONNECT_REQ							"6 CONNECTION REQUEST"
#define EV_TRANSFER_COMPLETE					"7 TRANSFER COMPLETE"
#define EV_DIAG_COMPLETE						"8 DIAGNOSTICS COMPLETE"
#ifdef HAVE_REQUEST_DOWNLOAD
#define EV_REQUEST_DOWNLOAD						"9 REQUEST DOWNLOAD"
#endif /* HAVE_REQUEST_DOWNLOAD */
#define EV_AUTONOMOUS_TRANSFER_COMPLETE			"10 AUTONOMOUS TRANSFER COMPLETE"
#define EV_DU_STATE_CHANGE_COMPLETE				"11 DU STATE CHANGE COMPLETE"
#define EV_AUTONOMOUS_DU_STATE_CHANGE_COMPLETE	"12 AUTONOMOUS DU STATE CHANGE COMPLETE"



/* Cumulative Behavior - Multiple */
#define EV_M_BOOT							"M Reboot"
#define EV_M_SCHEDULED						"M ScheduleInform"
#define EV_M_DOWNLOAD						"M Download"
#define EV_M_SCHEDULEDOWNLOAD				"M ScheduleDownload"
#define EV_M_UPLOAD							"M Upload"
#define EV_M_CHANGE_DU_STATE				"M ChangeDUState"

#if defined(HAVE_REQUEST_DOWNLOAD) || defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD)
#define		FIRMWARE_UPGRADE_IMAGE		"1 Firmware Upgrade Image" 		//download only
#define		WEB_CONTENT					"2 Web Content"					//download only
#define		VENDOR_CONFIGURATION_FILE	"3 Vendor Configuration File"
#define		VENDOR_LOG_FILE				"4 Vendor Log File"				//upload only
#define		TONE_FILE					"4 Tone File"					//download only
#define		RINGER_FILE					"5 Ringer File"					//download only
#endif /* #if defined(HAVE_REQUEST_DOWNLOAD) || defined(HAVE_FILE_DOWNLOAD) || defined(HAVE_FILE_SCHEDULE_DOWNLOAD) || defined(HAVE_FILE_UPLOAD) */

#ifdef HAVE_FILE_SCHEDULE_DOWNLOAD
#define		TIME_WINDOW_MODE_AT_ANY_TIME   "1 At Any Time"
#define		TIME_WINDOW_MODE_IMMEDIATELY   "2 Immediately"
#define		TIME_WINDOW_MODE_WHEN_IDLE     "3 When Idle"
#define		TIME_WINDOW_MODE_CONFIR_NEEDED "4 Confirmation Needed"
#endif /* HAVE_FILE_SCHEDULE_DOWNLOAD */

#define		ERR_SCHEDULE_DOWNLOAD_ISNOT_CONFIRMED 100

// Comment this flag when debugging to accelerate the dimclient start. (when debugging only!!!).
#define		USE_DELAY_AT_THREAD_START

// @@@@@@@ Delete when it will be unneeded:
/* At present these flags are used in the kickedHandler & transferHandler
 * Defines for DoS attack recognition length of the time slot in seconds */
#define MAX_REQUEST_TIME		100
/* the number of allowed requests in one time slot */
#define MAX_REQUESTS_PER_TIME 	10

typedef int (*accessParamCallback)(const char *, ParameterType, ParameterValue *);
typedef int (*addDelObjCallback)(const char *, const char *);

#ifndef IFNAMSIZ
#define IFNAMSIZ 16
#endif /* IFNAMSIZ */

typedef unsigned int uint;

/* ManagementServer parameters from a config */
struct ConfigManagement
{
	char url[257]; 				/* ManagementServerURL */
	char username[257]; 			/*  ManagementServerUsername */
	char password[257]; 			/*  ManagementServerPassword */
#ifdef BINDING_TO_INTERFACE_NAME_IS_ENABLED
// Intel change for Bind to Device TCP Socket option
    char netDeviceName[IFNAMSIZ];   /* network device name */
#endif
	unsigned long maxHTTPLogSize; 	/* max Debug.Log Size. If == 0, without limit */
	int maxHTTPLogBackupCount; 	/* Max count of backup DEBUG.log file*/
	unsigned long maxTestLogSize; 	/*max Test.Log Size. If == 0, without limit */
	int maxTestLogBackupCount; 		/* Max count of backup TEST.log file*/
	char HTTPLogFilePathName[257];  /* Path+Name of HTTP Log File (for ex. tmp/HTTP.log) */
	char TESTLogFilePathName[257]; /* Path+Name of TEST Log File (for ex. tmp/TEST.log) */
	char ScriptPath[257];     		/* Path of scripts in rootfs (for ex. '/lib/rdk/' for trunk and '/sysint/' for stable.) */
	char SharedKeyPathName[257];   /* Path+Name of Shared key File (for ex. /etc/tr69/sharedKey.txt) */
	char BootstrapFilePathName[257];  /* Path+Name of bootstrap file (for ex. /opt/persistent/tr69bootstrap.dat) */
	char SslCertPath_Acs[257];  /* Path+Name of  acs ssl cert path file (for ex. /usr/share/tr69/certs/comcast_acs_ca.crt) */
	char devicePropertiesFile[257];  /* Path+Name of file (for ex. /etc/device.properties) */
};

typedef struct func
{
	int idx;
	accessParamCallback func;
} Func;

typedef struct objectFunc
{
	int idx;
	addDelObjCallback objCallback;
} ObjectFunc;

/* Who initiated transfer */
typedef enum transfers_type
{
	ACS,
	NOTACS
} Transfers_type;

typedef union SUPORTEDRPC
{
    struct
    {
        unsigned int isGetAllQueuedTransfers :1;
        unsigned int isAutonomousTransferComplete :1;
        unsigned int isScheduleDownload :1;
        unsigned int isCancelTransfer :1;
        unsigned int isChangeDUState :1;
        unsigned int isDUStateChangeComplete :1;
        unsigned int isAutonomousDUStateChangeComplete :1;
    } rpc;
    unsigned int cwmpMask;
} SUPORTEDRPC;

#endif /* DIMARK_GLOBALS_H */
