/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef callback_H
#define callback_H

#include "utils.h" 
#include "list.h"

#define CALLBACK_REPEAT		1
#define CALLBACK_STOP		0

extern List initParametersBeforeCbList;
extern List initParametersDoneCbList;
extern List preSessionCbList;
extern List postSessionCbList;
extern List cleanUpCbList;

typedef int (*Callback) (void);
typedef int (*SPVCallback) (const char *,  ParameterValue *value);

typedef struct SPVCallbackData
{
	char *paramName;
	ParameterValue paramValue;
	SPVCallback cbFunction;
} SPVCallbackData;

typedef SPVCallbackData *pSPVCallbackData;


void initCallbackList (void);
void addCallback( Callback, List * );
void addSPVCallback(SPVCallback , const char * , ParameterType , ParameterValue * );
void cleanCallbackList(List* );
void cleanCallbackLists();

/* Call all callback functions in the list
 * the callback is removed after the call
 */
int executeCallback( List * );

#endif /* callback_H */
