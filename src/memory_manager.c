/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

/** Useful functions for access the Modem data */
#include <stdio.h>
#include <stdlib.h>
#include <linux/unistd.h>
//#include <time.h>

#include "dimark_globals.h"
//#include "paramaccess.h"
//#include "serverdata.h"
#include "list.h"
//#include "utils.h"

// Count allocated chunks	
static int allocatedChunksCount = 0;
// Count memory allocations for Typed Memory
static long typedMemAllocCount = 0L;
static long typedMemAllocBytes = 0L;
// Maximum of allocated Bytes in typed memory
static long maxTypedMemAllocBytes = 0L;

// Structure to hold a pointer to the allocated Memory which is freed at the 
// end of the Client Mainloop
typedef struct AllocatedMemory
{
	void *memory; // pointer to the allocated memory.
	unsigned int size; // size of the allocated memory.
	unsigned int memId; // memory id.  0..23 bits - ID of thread in which the memory was allocated. 24-31 bits - memory type.
	struct AllocatedMemory *next; // pointer to the next AllocatedMemory struct in the list.
} AllocatedMemory;

static AllocatedMemory *typedMemListFirst = NULL;
static AllocatedMemory *typedMemListLast = NULL;

static void sysError( const char * );
static void *_emalloc( unsigned int );

pthread_mutex_t typedMemList_mutex = PTHREAD_MUTEX_INITIALIZER;

/** Mallocs a memory for permanent usage after the mainloop
 *  If size n is 0, then alloc 1 byte for the EOS.
 */
static void *
emalloc(unsigned int n)
{ 
	void  *p;
	if ( n != 0 )
	{
		p = _emalloc( n );
	}
	else
	{
		p = _emalloc(1);
	}
	if (p)
		allocatedChunksCount ++;
	return p;
}

static void *
_emalloc(unsigned int n)
{ 
	void  *p;
	if ((p = (void*)calloc(1,n)) == NULL && n != 0 )
		sysError("Out of memory");
	// this is for the gcc 2.95.x compiler
	// changed call to calloc(), otherwise,  need the memset()
	// memset( p, '\0', n );
#ifdef WITH_PRINT_MEM_MSG
	printf("Memory message: function _emalloc(): allocated memory address = [%p], sizeOfMem = %d\n", p, n);
#endif
	return p;
}

static void
efree(void **mem )
{
#ifdef WITH_PRINT_MEM_MSG
	printf("Memory message: function efree(): mem = [%p], memoryAllocCount = %d\n", *mem, allocatedChunksCount-1);
#endif
	free( (void*)*mem );
	*mem = NULL;
	allocatedChunksCount --;
}


/** Mallocs memory which is freed after leaving the mainloop
 * n - size of needed memory
 * callIndex - auxiliary parameter for debug messages, which is used for definition of the place, from function is called.
 */
void *
emallocTemp( unsigned int n, int callIndex )
{
	return emallocTypedMem(n, MEM_TYPE_TEMP, callIndex);
}


/** Mallocs memory which is freed after leaving the mainloop
 *  n - size of needed memory
 *  callIndex - auxiliary parameter for debug messages, which is used for definition of the place, from function is called.
 */
void *
emallocSession( unsigned int n, int callIndex  )
{
	return emallocTypedMem(n, MEM_TYPE_SESSION, callIndex );
}


/* San. 17.04.2012.
 * Mallocs typed memory.
 * size - size of memory. If size is 0, then alloc 1 byte for the EOS.
 * typeIdx - memory type identifier ( [0..255] )
 * callIndex - auxiliary parameter for debug messages, which is used for definition of the place, from function is called.
 */
void *
emallocTypedMem( unsigned int size, unsigned int typeIdx, int callIndex )
{
	AllocatedMemory *am;
	unsigned int memId;
	void *pointer;
	memId = (unsigned int) abs(getTid()) + (typeIdx << 24);

	pthread_testcancel();
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

#ifdef WITH_PRINT_MEM_MSG
	printf("Memory message: function emallocTypedMem(): sizeOfMem = %d, memId = 0x%.8X, callIndex = %d\n", size, memId, callIndex);
#endif

	pointer = emalloc( size );
	if ( pointer == NULL )
	{
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		pthread_testcancel();
		return NULL;
	}

	if (size == 0)
		size = 1;
#ifdef WITH_PRINT_MEM_MSG
	printf("Memory message: function emallocTypedMem(): pointer to memory = [%p], sizeOfMem = %d\n", pointer, size);
#endif

	am = (AllocatedMemory*)_emalloc( sizeof(AllocatedMemory));
	if (!am)
	{
		efree(&pointer);
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		pthread_testcancel();
		return NULL;
	}

	am->size = size;
	am->memId = memId;
	am->memory = pointer;
	am->next = NULL;
	pthread_mutex_lock(&typedMemList_mutex);
	if ( typedMemListFirst == NULL )
	{
		typedMemListFirst = am;
		typedMemListLast = am;
	}
	else
	{
		typedMemListLast->next = am;
		typedMemListLast = am;
	}

	typedMemAllocCount++;
	typedMemAllocBytes += size;

	// save statistics
	if ( typedMemAllocBytes > maxTypedMemAllocBytes)
		maxTypedMemAllocBytes = typedMemAllocBytes;
	pthread_mutex_unlock(&typedMemList_mutex);

	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_testcancel();

	return pointer;
}


/* San. 18.04.2012.
 * frees all typed memory for all threads and types.
 * callIndex - auxiliary parameter for debug messages, which is used for definition of the place, from function is called.
 * */
void
efreeAllTypedMem( int callIndex )
{
	AllocatedMemory *pAM;
	AllocatedMemory *tmpAM;

	pthread_testcancel();
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	pthread_mutex_lock(&typedMemList_mutex);
	pAM = typedMemListFirst;
	tmpAM = typedMemListFirst;

	while( pAM != NULL )
	{
		if (pAM->memory)
		{
#ifdef WITH_PRINT_MEM_MSG
			printf("Memory message: function efreeAllTypedMem(): pAM->memory = [%p], pAM->memId = 0x%.8X\n", pAM->memory, pAM->memId);
#endif
			efree( (void**)&(pAM->memory) );
		}
#ifdef WITH_PRINT_MEM_MSG
		else
			printf("Memory message: function efreeAllTypedMem(): aml->memory is NULL\n");
#endif

		typedMemAllocCount--;
		typedMemAllocBytes -= pAM->size;

#ifdef WITH_PRINT_MEM_MSG
		printf("Memory message: function efreeAllTypedMem(): pAM = [%p]\n", pAM);
#endif
		tmpAM = pAM->next;
		free( pAM );
		pAM = NULL;
		pAM = tmpAM;
	}

	typedMemListFirst = typedMemListLast = NULL;
	typedMemAllocCount = 0;

	pthread_mutex_unlock(&typedMemList_mutex);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_testcancel();
}


/* San. 18.04.2012.
 * frees typed memory with type from parameter typeIdx.
 * typeIdx - memory type identifier ( [0..255] )
 * callIndex - auxiliary parameter for debug messages, which is used for definition of the place, from function is called.
 * */
void
efreeTypedMemByType(unsigned int typeIdx, int callIndex )
{
	AllocatedMemory *pAM;
	AllocatedMemory *tmpAM;

	typeIdx = typeIdx << 24;

	pthread_testcancel();
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	pthread_mutex_lock(&typedMemList_mutex);
	pAM = typedMemListFirst;
	tmpAM = typedMemListFirst;

	while( pAM != NULL )
	{
		if ( (pAM->memId & 0xFF000000) != typeIdx )
		{
			tmpAM = pAM;
			pAM = pAM->next;
			continue;
		}

		if (pAM->memory)
		{
#ifdef WITH_PRINT_MEM_MSG
			printf("Memory message: function efreeTypedMemByType(): aml->memory = [%p]\n", pAM->memory);
#endif
			efree( (void**)&(pAM->memory) );
		}
#ifdef WITH_PRINT_MEM_MSG
		else
			printf("Memory message: function efreeTypedMemByType(): pAM->memory is NULL\n");
#endif

		typedMemAllocCount--;
		typedMemAllocBytes -= pAM->size;

#ifdef WITH_PRINT_MEM_MSG
		printf("Memory message: function efreeTypedMemByType(): pAM = [%p]\n", pAM);
#endif

		if (pAM == typedMemListFirst)
		{
			tmpAM = typedMemListFirst = pAM->next;
			free( pAM );
			pAM = NULL;
			pAM = tmpAM;
		}
		else
		{
			tmpAM->next = pAM->next;
			free( pAM );
			pAM = NULL;
			pAM = tmpAM->next;
		}
	}

	typedMemListLast = tmpAM;

	pthread_mutex_unlock(&typedMemList_mutex);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_testcancel();
}


/* San. 18.04.2012.
 * frees all typed memory for current thread.
 * callIndex - auxiliary parameter for debug messages, which is used for definition of the place, from function is called.
 * Use the function efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY()
 * if it is called when program does not finish and paramEntry list it should not be deleted (damaged).
 * */
void
efreeAllTypedMemForCurrentThread( int callIndex )
{
	AllocatedMemory *pAM;
	AllocatedMemory *tmpAM;

	unsigned int curTid = (unsigned int) abs(getTid());

	pthread_testcancel();
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	pthread_mutex_lock(&typedMemList_mutex);
	pAM = typedMemListFirst;
	tmpAM = typedMemListFirst;

	while( pAM != NULL )
	{
		if ( ((pAM->memId) & 0x00FFFFFF) != curTid )
		{
			tmpAM = pAM;
			pAM = pAM->next;
			continue;
		}

		if (pAM->memory)
		{
#ifdef WITH_PRINT_MEM_MSG
			printf("Memory message: function efreeTypedMemByType(): aml->memory = [%p]\n", pAM->memory);
#endif
			efree( (void**)&(pAM->memory) );
		}
#ifdef WITH_PRINT_MEM_MSG
		else
			printf("Memory message: function efreeTypedMemByType(): pAM->memory is NULL\n");
#endif

		typedMemAllocCount--;
		typedMemAllocBytes -= pAM->size;

#ifdef WITH_PRINT_MEM_MSG
		printf("Memory message: function efreeTypedMemByType(): pAM = [%p]\n", pAM);
#endif

		if (pAM == typedMemListFirst)
		{
			tmpAM = typedMemListFirst = pAM->next;
			free( pAM );
			pAM = NULL;
			pAM = tmpAM;
		}
		else
		{
			tmpAM->next = pAM->next;
			free( pAM );
			pAM = NULL;
			pAM = tmpAM->next;
		}
	}

	typedMemListLast = tmpAM;
	
	pthread_mutex_unlock(&typedMemList_mutex);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_testcancel();
}

/* San. 26.07.2012.
 * frees all typed memory for current thread except of MEM_TYPE_PARAM_ENTRY memory.
 * callIndex - auxiliary parameter for debug messages, which is used for definition of the place, from function is called.
 * */
void
efreeAllTypedMemForCurrentThreadExcept_MEM_TYPE_PARAM_ENTRY( int callIndex )
{
	AllocatedMemory *pAM;
	AllocatedMemory *tmpAM;

	unsigned int curTid = (unsigned int) abs(getTid());

	pthread_testcancel();
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	pthread_mutex_lock(&typedMemList_mutex);
	pAM = typedMemListFirst;
	tmpAM = typedMemListFirst;

	while( pAM != NULL )
	{
		if ( (pAM->memId & 0xFF000000) == (MEM_TYPE_PARAM_ENTRY << 24) || (pAM->memId & 0x00FFFFFF) != curTid)
		{
			tmpAM = pAM;
			pAM = pAM->next;
			continue;
		}

		if (pAM->memory)
		{
#ifdef WITH_PRINT_MEM_MSG
			printf("Memory message: function efreeTypedMemByType(): aml->memory = [%p]\n", pAM->memory);
#endif
			efree( (void**)&(pAM->memory) );
		}
#ifdef WITH_PRINT_MEM_MSG
		else
			printf("Memory message: function efreeTypedMemByType(): pAM->memory is NULL\n");
#endif

		typedMemAllocCount--;
		typedMemAllocBytes -= pAM->size;

#ifdef WITH_PRINT_MEM_MSG
		printf("Memory message: function efreeTypedMemByType(): pAM = [%p]\n", pAM);
#endif

		if (pAM == typedMemListFirst)
		{
			tmpAM = typedMemListFirst = pAM->next;
			free( pAM );
			pAM = NULL;
			pAM = tmpAM;
		}
		else
		{
			tmpAM->next = pAM->next;
			free( pAM );
			pAM = NULL;
			pAM = tmpAM->next;
		}
	}

	typedMemListLast = tmpAM;

	pthread_mutex_unlock(&typedMemList_mutex);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_testcancel();
}


/* San. 18.04.2012.
 * frees typed memory for current thread with type from parameter typeIdx.
 * typeIdx - memory type identifier ( [0..255] )
 * callIndex - auxiliary parameter for debug messages, which is used for definition of the place, from function is called.
 * */
void
efreeTypedMemByTypeForCurrentThread(unsigned int typeIdx, int callIndex )
{
	AllocatedMemory *pAM;
	AllocatedMemory *tmpAM;

	unsigned int memId = (unsigned int) abs(getTid())  +  (typeIdx << 24);

	pthread_testcancel();
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	pthread_mutex_lock(&typedMemList_mutex);
	pAM = typedMemListFirst;
	tmpAM = typedMemListFirst;

	while( pAM != NULL )
	{
		if ( pAM->memId != memId )
		{
			tmpAM = pAM;
			pAM = pAM->next;
			continue;
		}

		if (pAM->memory)
		{
#ifdef WITH_PRINT_MEM_MSG
			printf("Memory message: function efreeTypedMemByType(): aml->memory = [%p]\n", pAM->memory);
#endif
			efree( (void**)&(pAM->memory) );
		}
#ifdef WITH_PRINT_MEM_MSG
		else
			printf("Memory message: function efreeTypedMemByType(): pAM->memory is NULL\n");
#endif

		typedMemAllocCount--;
		typedMemAllocBytes -= pAM->size;

#ifdef WITH_PRINT_MEM_MSG
		printf("Memory message: function efreeTypedMemByType(): pAM = [%p]\n", pAM);
#endif

		if (pAM == typedMemListFirst)
		{
			tmpAM = typedMemListFirst = pAM->next;
			free( pAM );
			pAM = NULL;
			pAM = tmpAM;
		}
		else
		{
			tmpAM->next = pAM->next;
			free( pAM );
			pAM = NULL;
			pAM = tmpAM->next;
		}
	}

	typedMemListLast = tmpAM;

	pthread_mutex_unlock(&typedMemList_mutex);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_testcancel();
}

/* San. 24.04.2012.
 * frees  typed memory by pointer.
 * pointer - pointer to memory which we want to free.
 * callIndex - auxiliary parameter for debug messages, which is used for definition of the place, from function is called.
 * */
void
efreeTypedMemByPointer( void ** pointer, int callIndex )
{
	AllocatedMemory *pAM;
	AllocatedMemory *tmpAM;

	if ((void *) *pointer == NULL)
		return;

	pthread_testcancel();
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	pthread_mutex_lock(&typedMemList_mutex);
	pAM = typedMemListFirst;
	tmpAM = NULL;

	while( (pAM != NULL) && (pAM->memory != (void *) *pointer))
	{
		tmpAM = pAM;
		pAM = pAM->next;
	}

	if (pAM)
	{
#ifdef WITH_PRINT_MEM_MSG
		printf("Memory message: function efreeTypedMemByPointer(): *pointer = [%p], pAM->memId = 0x%.8X, pAM->size = %d\n", *pointer, pAM->memId, pAM->size);
#endif
		efree( pointer );

		typedMemAllocCount--;
		typedMemAllocBytes -= pAM->size;

		if (pAM == typedMemListFirst)
		{
			typedMemListFirst = pAM->next;
			free( pAM );
			pAM = NULL;
			if (typedMemListFirst == NULL)
				typedMemListLast = NULL;
		}
		else
		{
			tmpAM->next = pAM->next;
			free( pAM );
			pAM = NULL;
			if (tmpAM->next == NULL)
				typedMemListLast = tmpAM;
		}
	}

	pthread_mutex_unlock(&typedMemList_mutex);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_testcancel();
}




/* San. 17.04.2012.
 * returns id of current thread, which called this function
 * */
static pid_t getTid()
{
	return (pid_t)syscall(__NR_gettid);
}


static void
sysError( const char *mesg )
{
	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_MEMORY, "%s\n", mesg);
	)
}


/* San. 17.04.2012.
 * */
int getAllocatedChunksCount()
{
	return allocatedChunksCount;
}

/* San. 18.04.2012.
 * */
long getTypedMemAllocCount()
{
	return typedMemAllocCount;
}

/* San. 18.04.2012.
 * */
long getTypedMemAllocBytes()
{
	return typedMemAllocBytes;
}

/* San. 18.04.2012.
 * */
long getMaxTypedMemAllocBytes()
{
	return maxTypedMemAllocBytes;
}

/* San. 19.04.2012.
 * typeIdx - memory type identifier ( [0..255] )
 * bytes - [out] parameter for returning the bytes size of memory with type 'typeIdx'.
 * */
long getTypedMemInfoByType(unsigned int typeIdx, long * bytes)
{
	AllocatedMemory *pAM = typedMemListFirst;
	long count = 0L;
	long resBytes = 0L;

	typeIdx = typeIdx << 24;
	pthread_mutex_lock(&typedMemList_mutex);

	while( pAM != NULL )
	{
		if ( (pAM->memId & 0xFF000000) == typeIdx )
		{
			count++;
			resBytes += pAM->size;
		}
		pAM = pAM->next;
	}

	pthread_mutex_unlock(&typedMemList_mutex);
	if (bytes)
		*bytes = resBytes;
	return count;
}

/* San. 19.04.2012.
 * typeIdx - memory type identifier ( [0..255] )
 * bytes - [out] parameter for returning the bytes size of memory with type 'typeIdx'.
 * */
long getTypedMemInfoByTypeForThread(unsigned int typeIdx, long * bytes)
{
	AllocatedMemory *pAM = typedMemListFirst;
	long count = 0L;
	long resBytes = 0L;
	unsigned int memId = (unsigned int) abs(getTid())  +  (typeIdx << 24);

	pthread_mutex_lock(&typedMemList_mutex);

	while( pAM != NULL )
	{
		if ( pAM->memId == memId )
		{
			count++;
			resBytes += pAM->size;
		}
		pAM = pAM->next;
	}

	pthread_mutex_unlock(&typedMemList_mutex);
	if (bytes)
		*bytes = resBytes;
	return count;
}

/* San. 19.04.2012.
 * bytes - [out] parameter for returning the bytes size of memory with type 'typeIdx'.
 * */
long getTypedMemInfoForThread( long * bytes)
{
	AllocatedMemory *pAM = typedMemListFirst;
	long count = 0L;
	long resBytes = 0L;
	unsigned int curTid = (unsigned int) abs(getTid());

	pthread_mutex_lock(&typedMemList_mutex);

	while( pAM != NULL )
	{
		if ( ((pAM->memId) & 0x00FFFFFF) == curTid )
		{
			count++;
			resBytes += pAM->size;
		}
		pAM = pAM->next;
	}

	pthread_mutex_unlock(&typedMemList_mutex);
	if (bytes)
		*bytes = resBytes;
	return count;
}

void printMemoryInfo()
{
	printf("\n------------------------------------------- printMemoryInfo BEGIN\n");
	long size = 0, count = 0;
	long allSize = 0, allCount = 0;
	printf("getAllocatedChunksCount  = %u\n", getAllocatedChunksCount() );
	printf("getTypedMemAllocCount    = %lu\n", getTypedMemAllocCount() );
	printf("getTypedMemAllocBytes    = %lu\n", getTypedMemAllocBytes() );
	printf("getMaxTypedMemAllocBytes = %lu\n", getMaxTypedMemAllocBytes() );

	count = getTypedMemInfoByType(MEM_TYPE_SESSION, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_SESSION   count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_SESSION   bytes = %lu\n", size );

	count = getTypedMemInfoByType(MEM_TYPE_TEMP, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_TEMP      count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_TEMP      bytes = %lu\n", size );

	count = getTypedMemInfoByType(MEM_TYPE_PARAM_ENTRY, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_PARAM_ENTRY  count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_PARAM_ENTRY  bytes = %lu\n", size );

	count = getTypedMemInfoByType(MEM_TYPE_SHORT, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_SHORT  count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_SHORT  bytes = %lu\n", size );

	count = getTypedMemInfoByType(MEM_TYPE_EVENT_LIST, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_EVENT_LIST  count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_EVENT_LIST  bytes = %lu\n", size );

	count = getTypedMemInfoByType(MEM_TYPE_DIAGNOSTIC_LIST, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_DIAGNOSTIC_LIST  count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_DIAGNOSTIC_LIST  bytes = %lu\n", size );

	count = getTypedMemInfoByType(MEM_TYPE_CALLBACK_LISTS, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_CALLBACK_LISTS  count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_CALLBACK_LISTS  bytes = %lu\n", size );

	count = getTypedMemInfoByType(MEM_TYPE_DU_LIST, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_DU_LIST  count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_DU_LIST  bytes = %lu\n", size );

	count = getTypedMemInfoByType(MEM_TYPE_FILETRANSFER_LIST, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_FILETRANSFER_LIST  count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_FILETRANSFER_LIST  bytes = %lu\n", size );

	count = getTypedMemInfoByType(MEM_TYPE_SI_LIST, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_SI_LIST  count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_SI_LIST  bytes = %lu\n", size );

	count = getTypedMemInfoByType(MEM_TYPE_OPSION_LIST, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_OPSION_LIST  count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_OPSION_LIST  bytes = %lu\n", size );

	count = getTypedMemInfoByType(MEM_TYPE_PARAM_FAULT_LIST, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_PARAM_FAULT_LIST  count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_PARAM_FAULT_LIST  bytes = %lu\n", size );

#if 0
	count = getTypedMemInfoByType(MEM_TYPE_GSOAP_MEM, &size);
	allCount += count;
	allSize += size;
	printf("getTypedMemInfoByType  MEM_TYPE_GSOAP_MEM  count = %lu\n", count);
	printf("getTypedMemInfoByType  MEM_TYPE_GSOAP_MEM  bytes = %lu\n", size );
#endif

	printf("allSize = %ld, allCount = %lu\n------------------------------------------- printMemoryInfo END\n\n", allSize, allCount);
}

void unlock_typedMemList_mutex()
{
	pthread_mutex_unlock(&typedMemList_mutex);
}


/* Dimark malloc/calloc analogous function.
 * It may be used in in the gSOAP files instead of system calloc/malloc functions.
 * */
void* dcalloc(unsigned int size)
{
	return emallocTypedMem( size, MEM_TYPE_GSOAP_MEM, 0 );
}

/* Dimark free analogous function.
 * It may be used in in the gSOAP files instead of system free function.
 * */
void dfree(void * mem)
{
	void * tmp = mem;
	efreeTypedMemByPointer( &tmp , 0 );
}
