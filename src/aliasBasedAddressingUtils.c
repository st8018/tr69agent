/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                      *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#include "aliasBasedAddressingUtils.h"

#ifdef HAVE_ALIAS_BASED_ADDR_MECHANISM

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

//#include "dimclient.h"
#include "paramaccess.h"
//#include "utils.h"

volatile int aliasBasedAddressing = 0;
volatile InstanceMode instanceMode = InstanceNumber; // "InstanceNumber" is default value (from standard).
volatile int autoCreateInstances = 0;

static int getPathLayerCount(char * );

void setAliasBasedAddressing(int value)
{
	aliasBasedAddressing = (value ? 1 : 0);
}

int getAliasBasedAddressing()
{
	return aliasBasedAddressing;
}

void setInstanceMode(InstanceMode value)
{
	instanceMode = value;
}

InstanceMode getInstanceMode()
{
	return instanceMode;
}

void setAutoCreateInstances(int value)
{
	autoCreateInstances = (value ? 1 : 0);
}

int getAutoCreateInstances()
{
	return autoCreateInstances;
}


int readAliasBasedAddressingInfoFromDB()
{
	int ret;
	int * tmpPointer = NULL;
	char * tmpCharPointer = NULL;

	if ((ret = getParameter(ALIASBASEDADDRESSING, &tmpPointer)) != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ABAM, "getParameter(%s) error, ret = %d\n",ALIASBASEDADDRESSING, ret);
		)
		return ret;
	}
	setAliasBasedAddressing( (tmpPointer ? *tmpPointer : 0) );


	if ((ret = getParameter(INSTANCEMODE, &tmpCharPointer)) != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ABAM, "getParameter(%s) error, ret = %d\n",INSTANCEMODE, ret);
		)
		return ret;
	}
	if (tmpCharPointer && !strcmp(tmpCharPointer, "InstanceAlias"))
		setInstanceMode( InstanceAlias );
	else
		setInstanceMode( InstanceNumber );


	tmpPointer = NULL;
	if ((ret = getParameter(AUTOCREATEINSTANCES, &tmpPointer)) != OK)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_ABAM, "getParameter(%s) error, ret = %d\n",AUTOCREATEINSTANCES, ret);
		)
		return ret;
	}
	setAutoCreateInstances( (tmpPointer ? *tmpPointer : 0) );

	return OK;
}

/* return resultParamName after editing by requestedParamName.
 * if any error occurs, or (layer of resultParamName < layer of requestedParamName) returns resultParamName without editing.
 * for example:
 * 1)
 * resultParamName = "Device.DeviceInfo.VendorConfigFile.1.Name"
 * requestedParamName = "Device.DeviceInfo.VendorConfigFile.[AAA]."
 * result = "Device.DeviceInfo.VendorConfigFile.[AAA].Name"
 * 2)
 * resultParamName = "InternetGatewayDevice.LANDevice.[AAA].Hosts.Host.[BBB].MACAddress"
 * requestedParamName = "InternetGatewayDevice.LANDevice.[AAA].Hosts.Host.3."
 * result = "InternetGatewayDevice.LANDevice.[AAA].Hosts.Host.3.MACAddress"
 * */
char * correctParamPathNameInResponse(char * resultParamName, char * requestedParamName)
{
	int layerCount;
	char *tmpPath;
	char *dot;
	int resLen;
	char *result;
	int requestedParamNameIsObject;

	if (!resultParamName)
		return NULL;
	if (!getAliasBasedAddressing() || !requestedParamName || !(*requestedParamName) || !strcmp(resultParamName, requestedParamName))
		return resultParamName;

	layerCount = getPathLayerCount(requestedParamName);
	requestedParamNameIsObject = (*(requestedParamName + strlen (requestedParamName) - 1) == '.');
	if (!layerCount)
		return resultParamName;

	tmpPath = resultParamName;
	while (layerCount > 0)
	{
		dot = strchr(tmpPath, '.');
		if (!dot)
		{
			if (!requestedParamNameIsObject) // requestedParamName is not object
			{
				if (*tmpPath)
					layerCount--;
				while(*tmpPath) tmpPath++;
			}
			break;
		}
		tmpPath = dot + 1;
		layerCount--;
	}

	if (layerCount > 0)
		return resultParamName;

	resLen = strlen(requestedParamName) + strlen(tmpPath) + (requestedParamNameIsObject ? 1 : 2);

	result = emallocTemp(resLen, 0);
	if (!result)
		return resultParamName;
	strcpy(result, requestedParamName);
	if (!requestedParamNameIsObject && *tmpPath)
		strcat(result, ".");
	strcat(result, tmpPath);
	return result;
}

/* Returns the number of layers in the parameter path.
 * For example:
 * If path = "Device.DeviceInfo." then ret = 2
 * If path = "Device.ManagementServer.URL" then ret = 3
 * */
static int getPathLayerCount(char * path)
{
	int ret = 0;
	char *tmpPath;
	char *dot;

	if (!path)
		return 0;

	tmpPath = path;

	while ( (dot = strchr(tmpPath, '.')) )
	{
		++ret;
		tmpPath = dot + 1;
	}
	if (*tmpPath != '\0')
		++ret;

	return ret;
}

/* 3.8.1 Alias Parameter Definition
  The Alias Parameter value MUST be a string as defined in Section 3.2.1/XML Schema Part 2 [14]. Specifically, it
  has a length from 1 to 64 characters (so it cannot be empty); the supported character set is defined as numbers
  (ASCII 0x30-0x39), letters (ASCII 0x41-0x5A and ASCII 0x61-0x7A), underscore (ASCII 0x5F) and dash (ASCII
  0x2D); it is case sensitive; the first character MUST be a letter (ASCII 0x41-0x5A or ASCII 0x61-0x7A).
 */
int isAliasValueValid(char * aliasValue)
{
	char * c;
	if (!aliasValue)
		return 0;
	if (!*aliasValue || strlen(aliasValue)>64)
		return 0;

	if (!isalpha(*aliasValue))
		return 0;

	for (c = aliasValue; *c; c++)
	{
		if (!isalnum(*c) && (*c != '_') && (*c != '-'))
			return 0;
	}

	return 1;
}
#endif /* HAVE_ALIAS_BASED_ADDR_MECHANISM */
