/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

/** Useful functions for access the Modem data */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <errno.h>
#include <net/if.h>
#include <sys/ioctl.h>

#include "dimark_globals.h"
#include "utils.h"
#include "dimclient.h"
#include "paramaccess.h"
#include "serverdata.h"
#include "list.h"

// the digest nonce length must fit the nonce length
#define DIGEST_NONCE_LENGTH		24
#define DIGEST_NONCE_FORMAT		"%8.8x%8.8x%8.8x"

typedef struct faultMessage
{
	int		faultCode;
	char	*faultString;
} FaultMessage;

static FaultMessage faultMessages[] =
{
		{ ERR_METHOD_NOT_SUPPORTED, "Method not Supported" },
		{ ERR_REQUEST_DENIED, "Request Denied" },
		{ ERR_INTERNAL_ERROR, "Internal Error" },
		{ ERR_INVALID_ARGUMENT, "Invalid Arguments" },
		{ ERR_RESOURCE_EXCEEDED, "Resources exceeded" },
		{ ERR_INVALID_PARAMETER_NAME, "Invalid Parameter Name" },
		{ ERR_INVALID_PARAMETER_TYPE, "Invalid Parameter Type" },
		{ ERR_INVALID_PARAMETER_VALUE, "Invalid Parameter Value" },
		{ ERR_READONLY_PARAMETER, "Attempt to set a non-writable parameter" },
		{ ERR_NOTIFICATION_REQ_REJECT, "Notification request rejected" },
		{ ERR_DOWNLOAD_FAILURE, "Download/ScheduleDownload failure" },
		{ ERR_UPLOAD_FAILURE, "Upload failure" },
		{ ERR_TRANS_AUTH_FAILURE, "File transfer server authentication failure" },
		{ ERR_NO_TRANS_PROTOCOL, "Unsupported protocol for file transfer" },
		{ ERR_JOIN_MULTICAST_GROUP,	"File transfer failure: unable to join multicast group"},
		{ ERR_CONTACT_FILE_SERVER, "File transfer failure: unable to contact file server "},
		{ ERR_ACCESS_FILE, "File transfer failure: unable to access file"},
		{ ERR_COMPLETE_DOWNLOAD, "File transfer failure: unable to complete download "},
		{ ERR_CORRUPTED, "File transfer failure: file corrupted or otherwise unusable"},
		{ ERR_FILE_AUTHENTICATION, "File transfer failure: file authentication failure"},
		{ ERR_COMPLETE_DOWNLOAD_TIME_WINDOWS, "File transfer failure: unable to complete download within specified time windows"},
		{ ERR_CANCELATION_NOT_PERMITTED, "Cancelation of file transfer not permitted in current transfer state"},
		{ ERR_INVALID_UUID_FORMAT, "Invalid UUID format"},
		{ ERR_UNKNOWN_EXEC_ENVIRONMENT, "Unknown Execution Environment"},
		{ ERR_DISABLE_EXEC_ENVIRONMENT, "Disabled Execution Environment"},
		{ ERR_DU_EXEC_ENVIRONMENT_MISHMATCH, "Deployment Unit to Execution Environment Mismatch"},
		{ ERR_DUPLICATE_DU, "Duplicate Deployment Unit"},
		{ ERR_SYSTEM_RESOURCES_EXCEEDED, "System Resources Exceeded"},
		{ ERR_UNKNOWN_DU, "Unknown Deployment Unit"},
		{ ERR_INVALID_DU_STATE, "Invalid Deployment Unit State"},
		{ ERR_INVALID_DU_UPDATE_DOWNGRADE_NOT_PERMITTED, "Invalid Deployment Unit Update - Downgrade not permitted"},
		{ ERR_INVALID_DU_UPDATE_VERSION_NOT_SPECIFIED, "Invalid Deployment Unit Update - Version not specified"},
		{ ERR_INVALID_DU_UPDATE_VERSION_ALREADY_EXISTS, "Invalid Deployment Unit Update - Version already exists"}
};

// Calculate the number of entries in faultMessages
static int faultMessagesSize = sizeof( faultMessages ) / sizeof( FaultMessage );	

// Status which is returned by SetParamterValues
static volatile unsigned int parameterReturnStatus = 0;

// Pointer to a SetParameterValuesFault Array
static bool	initListDone = false;
static List	parameterFaultList;

static SetParameterFaultStruct	**parameterFault = NULL;
static int parameterFaultSize = 0;

// Session informations
static time_t		sessionStarttime = 0;
static time_t		sessionId = 0;
static time_t 		sessionEndtime = 0;
static time_t 		sessionRuntime = 0;
static unsigned int	sessionCallCount = 0;
static char			sessionInfoMsg[60];

// Async start of the inform message
static volatile bool asyncInform = false;


static volatile unsigned int globalRebootIdx = 0;
static bool bootstrap = false;
static char nonceValue[DIGEST_NONCE_LENGTH+1];
static char dateTimeBuf[DATE_TIME_LENGHT];

static void setSoapEnvHeader(struct soap *);

extern char *server_url;
extern struct http_da_info info;
extern long local_sendInform_count;

/** Session handling.
  A Session is the communication between a CPE and an ACS.
  The Session starts with the Inform message and ends when the connection is broken
 */
void
sessionStart( void )
{
	sessionCallCount = 0;
	sessionStarttime = time( NULL );
	sessionId = sessionStarttime;
}

time_t
getSessionId( void )
{
	return sessionId;	
}

int
isNewSession( time_t checkSessionId )
{
	if ( sessionId != 0 )
		return (sessionId != checkSessionId);
	else
		return 1;
}

void
sessionEnd( void )
{
	sessionEndtime = time( NULL );
	sessionRuntime = sessionEndtime - sessionStarttime;
	sessionId = -1;
	server_url = NULL;
}

// Builds a string with some session info
const char *
sessionInfo( void )
{
	sprintf( sessionInfoMsg, "Runtime: %ld Calls: %d\n", sessionRuntime, sessionCallCount );
	return sessionInfoMsg;
}

// Increments the call counter every time the ACS calls a CPE function
void
sessionCall( void )
{
	sessionCallCount ++;
}

/** Handles the reboot flag, which can be set by different functions
 ex. setParameterValue(), addObject() etc.
 It sets a flags which are used in the mainloop, after no more requests are received
 the system is doing a reboot
 */
void
addRebootIdx( unsigned int rebootIdx)
{
	globalRebootIdx |= (rebootIdx & maxRebootIdxValue);
}

unsigned int
getRebootIdx( void )
{
	return globalRebootIdx & maxRebootIdxValue;
}

void cleanRebootIdx()
{
	globalRebootIdx = 0;
}

int
isBootstrap( void )
{
	if (bootstrap)
		return 1;
	else
		return 0;
}

void
setBootstrap(bool flag)
{
	bootstrap = flag;
}

void
setAsyncInform( const bool async )
{
	asyncInform = async;
}

int
getAsyncInform( void )
{
	if (asyncInform)
		return 1;
	else
		return 0;
}

/** Get the Header and fill the ServerData Structure
 * The HoldRequests must be clear because the CPE is not allowed to send this flag
 */
struct SOAP_ENV__Header *
analyseHeader( struct SOAP_ENV__Header *header )
{
	if ( header != NULL )
	{
		setSdCurrentId( header->cwmp__ID );

		if ( header->cwmp__HoldRequests != 0 ) {
			setSdHoldRequests( (int)header->cwmp__HoldRequests );
			header->cwmp__HoldRequests = 0;  // NULL
		} else {
			// If the Flag is not found in the header it is reseted to false
			setSdHoldRequests( 0 );
		}
		/* NoMoreRequest is deprecated. see TR121
		if ( header->cwmp__NoMoreRequests != 0 ) {
			setSdNoMoreRequests( (int)header->cwmp__NoMoreRequests );
			header->cwmp__NoMoreRequests = 0;  // NULL;
		} 
		 */
	}
	else
	{
		// If no header is found it is reseted to false
		setSdHoldRequests( 0 );
	}

	return header;
}

/** Set the returnstatus for SetParameterValue/addObj/delObj
 * 
 * \param value new value only 0 or 1 allowed 
 */
void
setParameterReturnStatus( unsigned int value )
{
	parameterReturnStatus = ( value > 0 ) ? 1 : 0;
}

/** Sefe setting of returnstatus for SetParameterValue/addObj/delObj
 *
 * \param value new value only 0 or 1 allowed
 *  check: if parameterReturnStatus == PARAMETER_CHANGES_NOT_APPLIED then value will be ignored.
 *  It is implemented for situation when in the 1 RPC the setting of Status occurs in the several places,
 *  and if previous place sets Status to PARAMETER_CHANGES_NOT_APPLIED then next places can not set status to PARAMETER_CHANGES_APPLIED.
 *  Therefore in the start of RPC handling the setParameterReturnStatus(PARAMETER_CHANGES_APPLIED) must be called
 *  if you want to use setParameterReturnStatusSafe() in this RPC handling.
 */
void
setParameterReturnStatusSafe( unsigned int value )
{
	if (parameterReturnStatus == PARAMETER_CHANGES_APPLIED)
		parameterReturnStatus = ( value > 0 ) ? 1 : 0;
}

int 
getParameterReturnStatus( void )
{
	return parameterReturnStatus;
}

/*
Post reboot session retry count			Wait interval range (min-max seconds)
	#1										5-10
	#2										10-20
	#3										20-40
	#4										40-80
	#5										80-160
	#6										160-320
	#7										320-640
	#8										640-1280
	#9										1280-2560
	#10 and subsequent						2560-5120
 */
int
expWait( unsigned long power )
{
	power = (1 << (power>9 ? 9 : power) ) * 10;
	power = power - rand()%(power/2 - 1) - 1;
	
	return power;
}

void
strnCopy( char *dest, const char *src, int len )
{
	if (!dest)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MEMORY, "strnCopy(): function parameter 'dest' can't be NULL.\n");
		)
		return;
	}
	if ( src != NULL )
		strncpy( dest, src, (len+1) );
	else
		memset( dest, '\0', (len+1) );
}


/* San. 20.04.2012.
 * Create a copy of src, and returns a pointer to the copy.
 * typeIdx - memory type identifier ( [0..255] )
 * */
char *
strnDupByMemType( char *dest, const char *src, int len, unsigned int typeIdx )
{
	if ( src != NULL )
	{
		dest = emallocTypedMem(len + 1, typeIdx, 4);
		if (!dest)
		{
			DEBUG_OUTPUT (
					dbglog (SVR_ERROR, DBG_MEMORY, "strnDupbyMemType()->emallocTypedMem() has returned NULL\n");
			)
			return NULL;
		}
		strncpy( dest, src, len );
		dest[len] = '\0';
		return dest;
	}
	else
	{
		return NULL;
	}
}




///** Create a copy of src, and returns a pointer to the copy
// do not free the allocated memory, this is done in efreeTemp()
// */
//char *
//strnDupTemp( char *dest, const char *src, int len )
//{
//	if ( src != NULL ) {
//		dest = emallocTemp( len + 1 , 32);
//		strncpy( dest, src, (len+1) );
//		return dest;
//	} else
//		return NULL;
//}
//
///** Create a copy of src, and returns a pointer to the copy
// do not free the allocated memory, this is done in efreeSession()
// */
//char *
//strnDupSession( char *dest, const char *src, int len )
//{
//	if ( src != NULL )
//	{
//		dest = emallocSession( len + 1, 3 );
//		//dest = emallocTypedMem(len + 1, MEM_TYPE_SESSION, 3);
//		if (!dest)
//		{
//			DEBUG_OUTPUT (
//					dbglog (SVR_ERROR, DBG_MEMORY, "strnDupSession()->emallocSession() has returned NULL\n");
//			)
//			return NULL;
//		}
//		strncpy( dest, src, len );
//		dest[len] = '\0';
//		return dest;
//	}
//	else
//	{
//		return NULL;
//	}
//}


//char *
//strnDup( char *dest, const char *src, int len )
//{
//	if ( src != NULL )
//	{
//		dest = emalloc( len + 1 );
//		if (!dest)
//		{
//			DEBUG_OUTPUT (
//					dbglog (SVR_ERROR, DBG_MEMORY, "strnDupSession(): dest is NULL\n");
//			)
//			return NULL;
//		}
//		strncpy( dest, src, len );
//		dest[len] = '\0';
//		return dest;
//	}
//	else
//		return NULL;
//}

/** Compares two char * which also have to have the same length
	\return 1 	val1 and val2 have same length and value
			0   val1 differs from val2 in length or value
			-1  if error
 */
int
strCmp( const char *val1, const char *val2 )
{
	if (!val1 || !val2)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DEBUG, "strCmp(): function parameters 'val1' and 'val2' cannot be NULL\n");
		)
		return -1;
	}
	return ( strlen( val1 ) == strlen( val2) && strcmp( val1, val2 ) == 0 );
}

/** Compares two char * 
	\return 1 	val1 equal val2 for len characters
			0   val1 differs from val2
			-1  if error
 */
int
strnStartsWith( const char *val1, const char *val2, int len )
{
	if (!val1 || !val2)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DEBUG, "strnStartsWith(): function parameters 'val1' and 'val2' cannot be NULL\n");
		)
		return -1;
	}
	return ( strncmp( val1, val2, len ) == 0 );
}

void
clearFault( void )
{
	parameterFaultSize = 0;
	parameterFaultList.firstEntry = parameterFaultList.lastEntry = NULL;
	efreeTypedMemByType(MEM_TYPE_PARAM_FAULT_LIST, 0); // free all parameterFault memory
}

SetParameterFaultStruct **
getParameterFaults( void )
{
	SetParameterFaultStruct *paramFault;
	int cnt;

	parameterFaultSize = getListSize( &parameterFaultList );
	parameterFault = (SetParameterFaultStruct **)emallocTypedMem((sizeof(SetParameterFaultStruct*)) * parameterFaultSize, MEM_TYPE_PARAM_FAULT_LIST, 0);
	ListEntry *le = iterateList( &parameterFaultList, NULL );
	cnt = 0;
	while ( le ) {
		paramFault = (SetParameterFaultStruct *)le->data;
		parameterFault[cnt] = paramFault;
		cnt ++;
		le = iterateList( &parameterFaultList, le );
	}
	return parameterFault;
}

/** Create a new SetParameterValuesFault entry
 * If faultString is NULL or Empty String then the getFaultString(code) function will be used to calculate paramFault->FaultString.
 */
int
addFault( const char *fault, const char *name, int code, const char *faultString)
{
	SetParameterFaultStruct *paramFault;

	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_PARAMETER, "SetParameterValuesFault: %s %s %d\n", fault, name, code );
	)

	if ( !initListDone )
	{
		initList( &parameterFaultList );
		initListDone = true;
	}
	paramFault = (SetParameterFaultStruct *)emallocTypedMem( sizeof(SetParameterFaultStruct), MEM_TYPE_PARAM_FAULT_LIST, 0);
	if ( !paramFault )
		return ERR_RESOURCE_EXCEEDED;

	paramFault->ParameterName = strnDupByMemType( paramFault->ParameterName, name, strlen(name), MEM_TYPE_PARAM_FAULT_LIST);
	paramFault->FaultCode = code;
	if (!faultString || !*faultString)
		paramFault->FaultString = getFaultString( code );
	else
		paramFault->FaultString = strnDupByMemType( paramFault->FaultString, faultString, strlen(faultString), MEM_TYPE_PARAM_FAULT_LIST);


	addEntry( &parameterFaultList, paramFault, MEM_TYPE_PARAM_FAULT_LIST );

	return code;
}

void
createFault (struct soap *soap, int errCode)
{
	Fault *cwmpFaultStruct;

	//cwmpFaultStruct = (Fault *) soap_malloc (soap, sizeof (Fault));
	cwmpFaultStruct = (Fault *) emallocTypedMem(sizeof (Fault), MEM_TYPE_PARAM_FAULT_LIST, 0);

	// Set the ErrorCode and ErrorMessage as defined in utils.c
	cwmpFaultStruct->FaultString = (char *) getFaultString (errCode);
	if (!cwmpFaultStruct->FaultString)
	{
		soap_set_fault(soap);
		const char **pfaultstring = soap_faultstring(soap);

		if (pfaultstring && *pfaultstring)
			cwmpFaultStruct->FaultString = (char*)*pfaultstring;
	}
	cwmpFaultStruct->FaultCode = (errCode == SOAP_NO_METHOD) ? ERR_METHOD_NOT_SUPPORTED : errCode;//errCode;
	// Append the SetParameterValuesFault messages
	cwmpFaultStruct->SetParameterValuesFault = getParameterFaults();
	// the size is calculated during getParameterFaults() 
	cwmpFaultStruct->__sizeParameterValuesFault = parameterFaultSize;

	soap_sender_fault (soap, "CWMP fault", NULL);
	//soap_copy_fault(soap, soap->version == 2 ? "Sender" : "Client", NULL, "CWMP fault", NULL);

	//soap->fault->detail = (struct SOAP_ENV__Detail *) soap_malloc (soap, sizeof (struct SOAP_ENV__Detail));
	soap->fault->detail = (struct SOAP_ENV__Detail *) emallocTypedMem (sizeof (struct SOAP_ENV__Detail), MEM_TYPE_PARAM_FAULT_LIST, 0);

	soap->fault->detail->cwmp__Fault = cwmpFaultStruct;
	//	soap->fault->detail->__any = NULL;	// parameterFault; /* no other XML data      Status = 0;*/
	DEBUG_OUTPUT (
			dbglog (SVR_ERROR, DBG_SOAP, "Fault detected %d %s\n", errCode, cwmpFaultStruct->FaultString);
	)
}

int getFaultFromACS(struct soap *soap)
{
	//printf("soap BUF = [%s]\n", soap->buf);
	//printf("soap error = [%d]\n", soap->error);

	char str_Fault[512] = {"\0"};
	char * str_Fault_begin = NULL;
	char * str_Fault_end = NULL;
	int len_Fault = 0;

	char FaultCode[8] = {"\0"};
	char FaultString[256] = {"\0"};

	memset(str_Fault, 0, sizeof(str_Fault));
	str_Fault_begin = strstr(soap->buf, "<cwmp:Fault>");
	str_Fault_end = strstr(soap->buf, "</cwmp:Fault>");

	if( (str_Fault_begin == NULL || str_Fault_end == NULL) )
		return OK;

	/* get len section cwmp:Fault */
	len_Fault = strlen(str_Fault_begin) - strlen(str_Fault_end);

	if(len_Fault > sizeof(str_Fault))
	{
		DEBUG_OUTPUT(
			dbglog (SVR_INFO, DBG_SOAP, "getFaultFromACS: buffer size is insufficient\n");
		)

		return OK;
	}
	strncpy(str_Fault, str_Fault_begin + strlen("<cwmp:Fault>"), (strlen(str_Fault_begin) - strlen(str_Fault_end) - strlen("<cwmp:Fault>")) );

	str_Fault_begin = strstr(str_Fault, "<FaultCode>");
	str_Fault_end = strstr(str_Fault, "</FaultCode>");

	if( (str_Fault_begin == NULL || str_Fault_end == NULL) )
		return OK;

	/* get FAULT CODE */
	strncpy(FaultCode, str_Fault_begin + strlen("<FaultCode>"), (strlen(str_Fault_begin) - strlen(str_Fault_end) - strlen("<FaultCode>")));



	str_Fault_begin = strstr(str_Fault, "<cwmp:FaultString>");
	str_Fault_end = strstr(str_Fault, "</cwmp:FaultString>");

	if( (str_Fault_begin == NULL || str_Fault_end == NULL) )
		return OK;

	/* get FAULT STRING */
	strncpy(FaultString, str_Fault_begin + strlen("<cwmp:FaultString>"), (strlen(str_Fault_begin) - strlen(str_Fault_end) - strlen("<cwmp:FaultString>")));


	DEBUG_OUTPUT (
			dbglog(SVR_ERROR, DBG_SOAP, "Get fault from ACS: FaultCode: %s, FaultString: %s\n", FaultCode, FaultString);
	)

	return OK;
}


char *
getFaultString( int faultCode )
{
	register int i = 0;
	for ( i = 0; i != faultMessagesSize; i ++ )
	{
		if ( faultMessages[i].faultCode == faultCode )
			return faultMessages[i].faultString;
	}

	return NULL;
}

long
getTime( void )
{
	struct timeval now;

	int ret = gettimeofday( &now, NULL );

	if (ret == OK)
	{
		return (long)(now.tv_sec*1000 + now.tv_usec/1000);
	}
	else
	{
		return 0;
	}
}

void
printTimeMarker( char * location )
{
	struct timeval now;

	int ret = gettimeofday( &now, NULL );

	if (ret == OK)
	{
		printf("\n\tLocation: [%s]. Time: %d.%.6d\n\n", location, now.tv_sec, now.tv_usec);
	}
	else
	{
		printf("\n\tLocation: [%s]. Time: unknown\n\n", location );
	}
}

/** Get the instance number of a named object given in paramPath.
	If the name appears more then once, the first hit is taken to get the instance number.

	ParamPath =     Internet.Object.1.Test.2.Enable
	Name =          Test
	Returns 2
 */
int
getIdxByName( const char *paramPath, const char *name )
{
	const	register char *ecp , *scp;
	char buf[10];

	if (!paramPath)
	{
		DEBUG_OUTPUT(
				dbglog (SVR_ERROR, DBG_PARAMETER, "getIdxByName(): function parameter 'paramPath' cannot be NULL.\n");)
				return -1;
	}
	if (!name)
	{
		DEBUG_OUTPUT(
				dbglog (SVR_ERROR, DBG_PARAMETER, "getIdxByName(): function parameter 'name' cannot be NULL.\n");)
				return -1;
	}
	scp = paramPath;
	memset( buf, 0, sizeof( buf ));
	while( true )
	{
		ecp = strchr( scp, '.' );
		if ( strncmp( scp, name, ecp-scp ) == 0 )
		{
			ecp ++;
			scp = ecp;
			ecp = strchr( scp, '.' );
			strncpy( buf, scp, ecp - scp );
			return a2i( buf, NULL );
		}
		else
		{
			// skip dot
			ecp ++;
			scp = ecp;
		}
	}
	return 0;
}

/** Get the instance of an object in the parameterPath.
 the name of the object must be equal up to the instance number.
 ex.
	ParamPath =     Internet.Object.1.Test.2.Enable
	Name =          Internet.Object.1.Test
	Returns 2
 */
int
getIdx( const char *paramPath, const char *name )
{
	register int nameLen, i;
	const	register char *ecp , *scp;
	char buf[10];

	if (!paramPath)
	{
		DEBUG_OUTPUT(
				dbglog (SVR_ERROR, DBG_PARAMETER, "getIdx(): function parameter 'paramPath' cannot be NULL.\n");)
				return -1;
	}
	if (!name)
	{
		DEBUG_OUTPUT(
				dbglog (SVR_ERROR, DBG_PARAMETER, "getIdx(): function parameter 'name' cannot be NULL.\n");)
				return -1;
	}
	scp = paramPath;
	nameLen = strlen( name );
	scp += nameLen;
	// skip dot
	scp ++;
	ecp = strchr( scp, '.' );
	// between scp and ecp is the number we search 
	i = 0;
	while ( scp != ecp )
		buf[i++] = *scp++;
	buf[i] = '\0';

	return a2i( buf, NULL );
}

/** Get the instance of an object in the parameterPath.
	The access method is different to getIdx(). 
	In getRevIdx() the part of the path is given from rear to front, until the instance number.

 ex.
	ParamPath =     Internet.Object.1.Test.2.Enable
	Name =          Test.2.Enable
	Returns 1
 */
int
getRevIdx( const char *paramPath, const char *name )
{
	register int pathLen, nameLen, i;
	const	register char *ecp , *scp;
	char buf[256];
	if (!paramPath)
	{
		DEBUG_OUTPUT(
				dbglog (SVR_ERROR, DBG_PARAMETER, "getRevIdx(): function parameter 'paramPath' cannot be NULL.\n");)
				return -1;
	}
	if (!name)
	{
		DEBUG_OUTPUT(
				dbglog (SVR_ERROR, DBG_PARAMETER, "getRevIdx(): function parameter 'name' cannot be NULL.\n");)
				return -1;
	}
	pathLen = strlen( paramPath );
	nameLen = strlen( name );
	scp = paramPath + pathLen;
	scp -= nameLen;
	// skip dot
	scp --;
	ecp = scp;
	while ( *--scp != '.' && scp != paramPath )
	{
		;
	}
	// between scp and ecp is the number we search 
	i = 0;
	while ( ++scp != ecp )
		buf[i++] = *scp;
	buf[i] = '\0';
	return a2i( buf, NULL );
}

/** Create a random integer value
 * use your own random generator available on your system
 */
const int
createRandomValue( void )
{
	return rand();
}

/** Create a random nonce value for Digest Authentication
 * 
 */
const char *
createNonceValue( void )
{
	sprintf( nonceValue, DIGEST_NONCE_FORMAT, createRandomValue(), getAllocatedChunksCount(), (int)time(NULL));
	return nonceValue;
}

/** Convert a string into a int type value
 */
int
a2i( const char *cp, int * isError )
{
	char * s;
	if (isError)
		*isError = 0;
	if (!cp)
	{
		if (isError)
			*isError = 1;
		return 0;
	}
	errno = 0;
	long l = strtol(cp, &s, 0);
	if (*s == 0 && errno == 0 && l>=INT_MIN && l<=INT_MAX)
		return (int)l;
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DEBUG, "a2i(): error convert the string [%s] to int.\n", cp);
		)
		if (isError)
			*isError = 1;
		return 0;
	}
}

/** Convert a string into a unsigned int type value
 */
unsigned int
a2ui( const char *cp, int * isError )
{
	char * s;
	if (isError)
		*isError = 0;
	if (!cp)
	{
		if (isError)
			*isError = 1;
		return 0;
	}
	errno = 0;
	unsigned long ul = strtoul(cp, &s, 0);
	if (*s == 0 && errno == 0 && ul <= UINT_MAX)
		return (unsigned int)ul;
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DEBUG, "a2ui(): error convert the string [%s] to unsigned int.\n", cp);
		)
		if (isError)
			*isError = 1;
		return 0;
	}

}

/** Convert a string into a long type value
 */
long
a2l( const char *cp, int * isError )
{
	char * s;
	if (isError)
		*isError = 0;
	if (!cp)
	{
		if (isError)
			*isError = 1;
		return 0L;
	}
	errno = 0;
	long l = strtol(cp, &s, 0);
	if (*s == 0 && errno == 0)
		return l;
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DEBUG, "a2l(): error convert the string [%s] to long.\n", cp);
		)
		if (isError)
			*isError = 1;
		return 0L;
	}
}

/** Convert a string into a unsigned long type value
 */
unsigned long
a2ul( const char *cp, int * isError )
{
	char * s;
	if (isError)
		*isError = 0;
	if (!cp)
	{
		if (isError)
			*isError = 1;
		return 0L;
	}
	errno = 0;
	unsigned long ul = strtoul(cp, &s, 0);
	if (*s == 0 && errno == 0)
		return ul;
	else
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_DEBUG, "a2ul(): error convert the string [%s] to unsigned long.\n", cp);
		)
		if (isError)
			*isError = 1;
		return 0L;
	}
}

/** Convert a time into a SOAP String.
 *  Create a copy of the returned string, cause it's overwritten by the next call.
 */
char *
dateTime2s( struct timeval *time )
{
	struct tm *tm1;
	char timeZoneTmp[7];
	memset(timeZoneTmp, 0, sizeof(timeZoneTmp));
	if (!time)
		return UNKNOWN_TIME;

	// when time = absolute 0, the return "0001-01-01T00:00:00.000000Z"
	if (!time->tv_sec && !time->tv_usec)
		return UNKNOWN_TIME;

	tm1 = localtime((const time_t*)&(time->tv_sec));

 	if ((strftime(dateTimeBuf, sizeof(dateTimeBuf),  "%Y-%m-%dT%H:%M:%S", tm1) == 0)
			|| (strftime(timeZoneTmp, sizeof(timeZoneTmp),  "%z", tm1) == 0)
			)
		return UNKNOWN_TIME;
	else
	{
		sprintf(dateTimeBuf + strlen(dateTimeBuf), ".%0.6d%s", time->tv_usec, timeZoneTmp);
		return dateTimeBuf;
	}
}

/** Converts the string in timeStr into a time_t type
 * which is written into *time.
 * Uses gSoap Function
 */
int
s2dateTime( const char *timeStr, struct timeval *tv1 )
{
	char fractional_part[32];
	const char *template;
	struct tm tm1;
	char *c;

	memset((void*)tv1, 0, sizeof(struct timeval));
	if (timeStr)
	{
		memset((void*)&tm1, 0, sizeof(struct tm));
		fractional_part[sizeof(fractional_part)-1] = '\0';
		if (strchr(timeStr, '-'))
			template = "%d-%d-%dT%d:%d:%d%31s";
		else if (strchr(timeStr, ':'))
			template = "%4d%2d%2dT%d:%d:%d%31s";
		else /* parse non-XSD-standard alternative ISO 8601 format */
			template = "%4d%2d%2dT%2d%2d%2d%31s";

		if (sscanf(timeStr, template, &tm1.tm_year, &tm1.tm_mon, &tm1.tm_mday, &tm1.tm_hour, &tm1.tm_min, &tm1.tm_sec, fractional_part) < 6)
			return -1;

		if ( strlen(fractional_part) != strlen(strstr(timeStr, fractional_part)) )
			return -1;

		tm1.tm_wday = -1;
		tm1.tm_yday = -1;
		tm1.tm_mon--;
		if (tm1.tm_year == 1)
			tm1.tm_year = 70;
		else
			tm1.tm_year -= 1900;
		tm1.tm_isdst = 0;

		if (fractional_part[0] != 0)
		{
			if (fractional_part[0] == '.' || fractional_part[0] == ',')
			{
				for (timeStr = fractional_part + 1; *timeStr; timeStr++)
					if (*timeStr < '0' || *timeStr > '9')
						break;
				char fractional_part2[32] = {'\0'};
				strncpy(fractional_part2, fractional_part+1, timeStr - (fractional_part+1) );
				if (strlen(fractional_part2) == 3)
					tv1->tv_usec = (__suseconds_t)(a2l(fractional_part2, NULL) * 1000);
				else if (strlen(fractional_part2) == 6)
					tv1->tv_usec = (__suseconds_t)a2l(fractional_part2, NULL);
				else
					return -1;
			}
			else
			{
				timeStr = fractional_part;
				tv1->tv_usec = 0;
			}
		}

		if (*timeStr == 'Z')
		{
			tv1->tv_sec = soap_timegm(&tm1);
		}
		else if (*timeStr)
		{
			if (*timeStr == '+' || *timeStr == '-') //If != + or - then timezone will be ignored, and time will be calculated as localtime for current zone.
			{
				int h = 0, m = 0;
				if (timeStr[3] == ':')
				{
					/* +hh:mm */
					sscanf(timeStr, "%d:%d", &h, &m);
					if (h < 0)	m = -m;
				}
				else /* +hhmm */
				{
					m = (int)atol(timeStr);
					h = m/100;
					m = m%100;
				}
				tm1.tm_hour -= h;
				tm1.tm_min -= m;
				/* put hour and min in range */
				tm1.tm_hour += tm1.tm_min / 60;
				tm1.tm_min %= 60;
				if (tm1.tm_min < 0)
				{
					tm1.tm_min += 60;
					tm1.tm_hour--;
				}
				tm1.tm_mday += tm1.tm_hour / 24;
				tm1.tm_hour %= 24;
				if (tm1.tm_hour < 0)
				{
					tm1.tm_hour += 24;
					tm1.tm_mday--;
				}
				/* note: day of the month may be out of range, timegm() handles it */

				tv1->tv_sec = soap_timegm(&tm1);
			}
			else
			{
				tv1->tv_sec = mktime(&tm1);
			}
		}
		else
		{
			tv1->tv_sec = mktime(&tm1);
		}
	}

	return OK;
}


/** San. 26 July 2011. round functions **/
int
iRoundf(float value)
{
	int roundValue;
	if (value >= 0.0)
		roundValue = (int) (value + 0.5);
	else
		roundValue = (int) (value - 0.5);
	return roundValue;
}

int
uiRoundf(float value)
{
	unsigned int roundValue = 0;
	if (value > 0.0)
		roundValue = (unsigned int) (value + 0.5);
	return roundValue;
}

long int
lRoundd(double value)
{
	long int roundValue;
	if (value >= 0.0)
		roundValue = (long int) (value + 0.5);
	else
		roundValue = (long int) (value - 0.5);
	return roundValue;
}

/* San. July 2011.
 * Check size of 'logFileName' file and make backup if necessary.
 * Previous backup will be change by next. Last backup will be change by current log file.
 */
int
check_log_files_size(const char * logFileName, const unsigned long maxLogFileSize, const int maxBackupCount)
{
	if (!logFileName || (strlen(logFileName) <= 0))
		return -1;

	int _maxBackupCount = maxBackupCount;
	int ret, i;
	long fsize;
	int logFileNameLen = strlen(logFileName);
	unsigned int commandLineLen = logFileNameLen * 3  +  50;
	char commandLine[commandLineLen]; // "mv -f logFileName.2147483646 logFileName.2147483647\0"
		// or "cp -f logFileName.2147483646 logFileName.2147483647 ; rm -f logFileName.2147483646\0"
		// or "rm -f logFileName\0"

	unsigned int fNameLen = logFileNameLen+12;
	char fname[fNameLen];

	if (!isFileExists(logFileName))
	{
		DEBUG_OUTPUT (
				dbglog (SVR_INFO, DBG_MAIN, "check_log_files_size(): file %s is not found. Backup is canceled.\n", logFileName);
		)
		return OK;
	}

	// if _maxBackupCount < 0, than we make backup files without count limit
	_maxBackupCount = _maxBackupCount < 0 ? __INT_MAX__ : _maxBackupCount;

	if (maxLogFileSize == 0) return OK;
	if (_maxBackupCount == 0)
	{
		memset(commandLine, 0, commandLineLen);
		sprintf(commandLine, "rm -f %s", logFileName);
		ret = system(commandLine);
		if (ret < 0)
			return ret;
		return OK;
	}

	fsize = getFileSize(logFileName);
	if (fsize < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "check_log_files_size(): getFileSize(\"%s\") returns error. ret = %d\n", logFileName, fsize);
		)
		return -1; // If been error in getFileSize(logFileName)
	}

	if ((unsigned long)fsize >= maxLogFileSize)
	{
		while(--_maxBackupCount > 0)
		{
			// Find existing backup file with most index. But starting with the search index _maxBackupCount-1.
			memset(fname, 0, fNameLen);
			sprintf(fname, "%s.%u", logFileName, _maxBackupCount);
			if (isFileExists(fname))
				break;
		}

		for ( i = _maxBackupCount; i > 0; i--)
		{
			memset(commandLine, 0, commandLineLen);
#ifdef MOVE_COMMAND_IS_NOT_INSTALLED
            // If 'Move' is not installed in the Embedded Linus, use 'Copy' and 'Remove' instead
	        sprintf(commandLine, "cp -f %s.%u %s.%u ; rm -f %s.%u", logFileName, i, logFileName, i + 1 ,logFileName, i);
#else
			sprintf(commandLine, "mv -f %s.%u %s.%u", logFileName, i, logFileName, i + 1);
#endif
			ret = system(commandLine);
			if (ret < 0)
				return ret;
		}

		memset(commandLine, 0, commandLineLen);
#ifdef MOVE_COMMAND_IS_NOT_INSTALLED
		// If 'Move' is not installed in the Embedded Linus, use 'Copy' and 'Remove' instead
	    sprintf(commandLine, "cp -f %s %s.1 ; rm -f %s", logFileName, logFileName ,logFileName);
#else
		sprintf(commandLine, "mv -f %s %s.1", logFileName, logFileName);
#endif
		ret = system(commandLine);
		if (ret < 0)
			return ret;
	}

	return OK;
}

/* San. July 2011.
 * Function returns size of file.
 * If error, returns negative value
 * */
long
getFileSize(const char * fileName)
{
	long size;
	FILE *file;

	if (!fileName) return -1;

	file = fopen(fileName,"r");

	if (!file) return -1;

	if ( fseek(file, 0, SEEK_END) != 0 ) {
		fclose(file);
		return -1;
	}
	size = ftell(file);
	fclose(file);

	return size;
}

/* San. July 2011.
 * Check, if file 'fileName' exists return 1, else return 0
 * */
int
isFileExists(const char * fileName)
{
	FILE * file;

	if (!fileName || !*fileName) return 0;

	file = fopen(fileName, "r");
	if (file) {
		fclose(file);
		return 1;
	}
	return 0;
}

char * accessListToString(char ** accessList, int accessListSize)
{
	int i;
	int len = 0;
	char* string;
	char** accessListTempPoint = accessList;

	if (!accessListTempPoint || accessListSize <= 0)
		return NULL;

	for ( i=0; i<accessListSize; i++)
	{
		len += strlen((char*)(*accessListTempPoint));
		accessListTempPoint++;
	}
	len += accessListSize; // +(accessListSize - 1)  -  for symbols '|';  +1 - for last symbol '\0'
	string = (char *) emallocTypedMem(len * sizeof(char), MEM_TYPE_SHORT, 0);
	if (!string)
		return NULL;

	accessListTempPoint = accessList;
	strcpy(string, (char*)(*accessListTempPoint));
	accessListTempPoint++;

	for ( i=1; i<accessListSize; i++)
	{
		strcat(string, "|");
		strcat(string, (char*)(*accessListTempPoint));
		accessListTempPoint++;
	}
	string[len-1] = '\0'; // last symbol
	return string;
}


/*San: check format of the address
 * if format is IPv6 then return 1
 * else return 0
 * */
int isIPv6Adress(const char * URL)
{
#ifndef WITH_IPV6
	return 0;
#else
	char * s = (char *)URL;
	char * tmpStr;
	int isIPv6AdressTmp;

	if ((tmpStr = strstr(s, "http://")))
	{
		s = tmpStr + 7; // strlen of "http://"
	}
	else if ((tmpStr = strstr(s, "https://")))
	{
		s = tmpStr + 8; // strlen of "https://"
	}

	if (s[0] == '[')
	{
		DEBUG_OUTPUT (
				dbglog (SVR_DEBUG, DBG_PARAMETER, "URL \"%s\" is in IPv6 format.\n", URL);
		)
		return 1;
	}

	/* San: check: is adress in IPv6 format */
	char * symbol_double_dot = strchr(s, ':');
	char * symbol_dot = strchr(s, '.');
	char * symbol_slash = strchr(s, '/');

	if (!symbol_dot)
	{
		isIPv6AdressTmp = 1;
	}
	else
	{
		if (symbol_slash)
		{
			if (symbol_dot > symbol_slash)
				isIPv6AdressTmp = 1;
			else
				if (symbol_double_dot  &&  symbol_double_dot < symbol_dot)
					isIPv6AdressTmp = 1;
				else
					isIPv6AdressTmp = 0;
		}
		else
		{
			if (symbol_double_dot  &&  symbol_double_dot < symbol_dot)
				isIPv6AdressTmp = 1;
			else
				isIPv6AdressTmp = 0;
		}
	}

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_PARAMETER, "isIPv6Adress(): URL \"%s\" is in %s format.\n", URL, isIPv6AdressTmp ? "IPv6" : "IPv4");
	)
	return isIPv6AdressTmp;
#endif
}

/* San: extract host string from URL.
 * Format of URL can be IPv4 or IPv6.
 * return -1 if error else return OK
 * return host adress in parameter host
 * return (if isHostInIPv6Format != NULL) isHostInIPv6Format == 1 if WITH_IPV6 defined && host is in IPv6 format
 *  or isHostInIPv6Format == 0
 */
int getHostStrFromURL(const char * URL, char *host, int * isHostInIPv6Format)
{
	const char * s = URL;
	char *tmpStr;
	char tmpHost[257] = {"\0"};
	int  i, n;
	int  isHostInIPv6FormatTmp;

	if (!URL)
		return -1;

	if (!*URL)
	{
		host[0] = '\0';
		return OK;
	}

	if ((tmpStr = strstr(s, "http://")))
	{
		s = tmpStr + 7; // strlen of "http://"
	}
	else if ((tmpStr = strstr(s, "https://")))
	{
		s = tmpStr + 8; // strlen of "https://"
	}

	n = strlen(s);
	if (n >= sizeof(tmpHost))
		n = sizeof(tmpHost) - 1;

#ifdef WITH_IPV6
	if (s[0] == '[') // if adress is in IPv6 format anyway
	{
		isHostInIPv6FormatTmp = 1;
		s++;
		for (i = 0; i < n; i++)
		{
			if (s[i] == ']')
			{
				s++;
				--n;
				break;
			}
			tmpHost[i] = s[i];
		}
	}
	else
	{
		isHostInIPv6FormatTmp = isIPv6Adress(s);
		for (i = 0; i < n; i++)
		{
			tmpHost[i] = s[i];
			/* San: if adress is IPv6 We delete comparation with ':' because IPv6 adress contains a symbol(s) ':' */
			/* If URL is IPv6 and contains the port, then URL must be started from symbol '[' */
			if (s[i] == '/' || (!isHostInIPv6FormatTmp && s[i] == ':') )
				break;
		}
	}
#else
	isHostInIPv6FormatTmp = 0;

	if (s[0] != '[')
	{
		for (i = 0; i < n; i++)
		{ tmpHost[i] = s[i];
		if (s[i] == '/' || s[i] == ':')
			break;
		}
	}
#endif

	tmpHost[i] = '\0';

	if (isHostInIPv6Format)
		*isHostInIPv6Format = isHostInIPv6FormatTmp;
	strcpy(host, tmpHost);

	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_PARAMETER, "getHostStrFromURL(): URL \"%s\" is in %s format. Host = \"%s\"\n", URL, isHostInIPv6FormatTmp ? "IPv6" : "IPv4", host);
	)

	return OK;
}

/* The function summarizes the time 'tv1' with time 'delta'
 * 'delta' - in microseconds
 * Returns the result in the structure 'tv1'.
 * */
void
timeval_add_time(struct timeval *tv1, long delta)
{
	if ((tv1->tv_usec += delta) > 999999)
	{
		tv1->tv_sec++;
		tv1->tv_usec-=1000000;
	}
	tv1->tv_sec += delta / 1000000;
}


/* The function subtracts from the time 'tv1' time 'delta'
 * 'delta' - in microseconds
 * Returns the result in the structure 'tv1'.
 * */
void
timeval_sub_time(struct timeval *tv1, long delta)
{
	if ((tv1->tv_usec -= delta) < 0)
	{
		--tv1->tv_sec;
		tv1->tv_usec += 1000000;
	}
	tv1->tv_sec -= delta / 1000000;
}

/* The function returns the delta between time 'tv1' time 'tv2' (tv2 - tv1)
 * returned 'delta' - in microseconds
 * */
long
get_timeval_delta(struct timeval *tv1, struct timeval *tv2)
{
	//return (long)((tv2->tv_sec * 1000000L + tv2->tv_usec) - (tv1->tv_sec*1000000L + tv1->tv_usec));
	return (long)((tv2->tv_sec - tv1->tv_sec)*1000000L + (tv2->tv_usec - tv1->tv_usec));
}

/* The function formats body of HTTP packet (str) to the readable XML format.
 * result - pointer to the char array for result
 * len - len of str.
 * tab - indent tab.
 * Returns:
 * a). result string - if Ok;
 * b). NULL - if str or result pointer is/are empty;
 * c). source str - if malloc error.
 * */
char * formatStringToXml(const char * str, char *result, unsigned int len, char *tab)
{
	static int leftCount = 0;
	int i;
	char *s1, *s2;
	char *strTmp;
	time_t time_of_day;

	if (!str || !result)
		return NULL;

	*result = '\0';

	strTmp = malloc( sizeof(char) * (len+1) ); // use system malloc() because formatStringToXml() is used in the gSOAP.
	if (!strTmp)
		return (char *)str;
	strncpy(strTmp, str, len);

	strTmp[len] = 0;
	s1 = s2 = strTmp;

	//printf("str =\n+++++++++++++++++++++++\n%s\n+++++++++++++++++++++++\n", s2);
	if ( (!strncmp(strTmp, "HTTP/", 5)) || (!strncmp(strTmp, "POST", 4)) || (!strncmp(strTmp, "GET", 3)) || (!strncmp(strTmp, "PUT", 3)) )
	{
		time_of_day = time( NULL );
		strftime (result, 300, "----------------------------%c----------------------------\n", localtime( &time_of_day ));
		leftCount = 0;
	}

	if (leftCount < 0)
		leftCount = 0;

	// Search and copy to result the symbols before tag <?...?>
	if ( (s2 = strstr(s1, "<?")) != NULL)
	{
		if (s2>s1)
		{
			strncat(result, s1, (int)(s2-s1) );
			if (*(s2-1) != '\n')
				strcat(result, "\n");
		}
		// Search and copy to result the tags <? ... ?>
		while ( s2  &&  (*s2)  &&  (s1 = strstr(s2, "<?")) )
		{
			if ( (s2 = strstr(s1, "?>")) )
			{
				// if "?>" is found then copy string "<? ... ?>" to result
				s2 += 2;
				strncat(result, s1, (int)(s2-s1));
				if (*(s2) != '\n')
					strcat(result, "\n");
			}
			else
			{
				// if "?>" isn't found then copy string "<? ... \0" to result
				strcat(result, s1);
				s2 = NULL;
			}
		}
		s1 = s2;
	}
	else
	{
		s2 = strTmp;
	}
	// s2 points or to start of message (if "<?" did not found) or to first symbol after "?>" or to NULL (if "<?" was found but "?>" didn't found)

	// Search and copy to result the symbols before first tag <...>
	if ( s2  &&  (*s2) && ((s2 = strchr(s2, '<')) != NULL) )
	{
		strncat(result, s1, s2-s1);
		if (*(s2-1) != '\n')
			strcat(result, "\n");
	}
	else
		strcat(result, s1);

	while ( s2  &&  (*s2) && (s1 = strchr(s2, '<')) && *(s1+1)!=0)
	{
		if (*(s1+1) == '/')
			leftCount--;

		for ( i=0; i<leftCount; i++)
		{
			strcat(result, tab);
		}
		// Search and copy to result the tag <...>
		if ( (s2 = strchr(s2, '>')) )
		{
			s2++;
			strncat(result, s1, s2-s1);
			if (*s2 == '\0')
			{
				strcat(result, "\n");
				break;
			}
		}
		else
		{
			strcat(result, s1);
			break;
		}

		// if this tag isn't </...> and isn't <.../>  (is this tag "Open tag" ?)
		if ( (*(s1+1) != '/') && (*(s2-2) != '/') )
		{
			// search thr next close tag </...>
			// if close tag is the first next tag after <...> then copy symbols without "\n". For example: <Open_tag>12345</Close_tag>
			if ( (s1 = strchr(s2, '<')) == strstr(s2, "</"))
			{
				// if '<' isn't found in the next symbols of string then save last part of string and break
				if (!s1)
				{
					strcat(result, s2);
					s2 = NULL;
					break;
				}

				s1 = s2;
				if ( (s2 = strchr(s2, '>')) )
				{
					s2++;
					strncat(result, s1, s2-s1);
				}
				else
				{
					strcat(result, s1);
					break;
				}
			}
			else
				leftCount++;
		}
		strcat(result, "\n");
	}
	if (s2)
		strcat(result, "\n");
	//printf("result = \n*****************\n%s\n*****************\n", result);
	free(strTmp);
	return result;
}

/* set Soap Envelope Header to soap struct.
 * set new cwmp__ID from last_cwmp_ID
 * increment of last_cwmp_ID.
 * !!!!!  This function is called only from dimarkMain(). If you want to call it from other place then:
 *  1). Move definition of soapEnvHeaderStruct from dimarkMain() to this function with storage type "static".
 *      Delete pSoapEnvHeaderStruct parameter of this function.
 *  2). Move definition of cwmp_ID_string from dimarkMain() to this function with storage type "static".
 *      Delete cwmp_ID_string parameter of this function.
 */
static void setSoapEnvHeader(struct soap *soap)
{
	static unsigned long last_cwmp_ID = 1000; //for cwmp:ID in the SOAP Envelope Header.
	static char cwmp_ID_string[32];
	static struct SOAP_ENV__Header soapEnvHeaderStruct = {cwmp_ID_string, 0, NULL};

	if (!soap)
		return;

	sprintf(cwmp_ID_string, "CPE_%lu", last_cwmp_ID++);
	if (soap->header)
	{
		soap->header->cwmp__ID = cwmp_ID_string;
		soap->header->cwmp__SessionTimeout = NULL;
	}
	else
	{
		soapEnvHeaderStruct.cwmp__ID = cwmp_ID_string;
		soapEnvHeaderStruct.cwmp__SessionTimeout = NULL;
		soap->header = &soapEnvHeaderStruct;
	}

	return;
}

void setAttrToHTTPandSOAPHeaders(struct soap *soap)
{
	// Restore Authentication data:
	authorizeClient(soap, &info);
	setSoapEnvHeader(soap);
}

void checkDirectories(const char *dir_path)
{
	DIR *dir = opendir (dir_path);
	if (dir == NULL)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_ERROR, DBG_MAIN, "checkDirectories: directory not found %s\n", dir_path);
		)
		exit(0);
	}
	closedir(dir);
}

/* check HexBinary parameter. Is value corrected?
 * returns 1 - if value is hexBinary
 * returns 0 - if value is not hexBinary
 * */
int isHexBinaryValueTrue(xsd__hexBinary value)
{
	char * s = (char *)value;
	if (!s)
		return 1; // HexBinary value can be empty
	while(*s != 0)
	{
		if ( (*s>='0' && *s<='9') || (*s>='A' && *s<='F') || (*s>='a' && *s<='f') )
			s++;
		else
			return 0;
	}
	return 1;
}


/*
 * get IP address associated to interface with name ifname
 * returns NULL - if error  or IP string. And returns the same IP in the  addr struct.
 * */
char* getIPAddrByInterfaceName(const char * interfaceName, struct sockaddr_in *addr)
{
	int sock, ret;
	struct sockaddr_in *in_addr;
	struct ifreq ifdata;
	char *IP = NULL;

	if (!interfaceName)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_MAIN, "getIPAddrByInterfaceName(): interfaceName can not be NULL. Exit.\n");
		)
		return NULL;
	}

	memset(&addr->sin_addr, 0, sizeof(addr->sin_addr));

	sock = socket(PF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_MAIN, "getIPAddrByInterfaceName()->socket() has returned %d, error: %s\n", sock, strerror(errno));
		)
		return NULL;
	}

	memset(&ifdata, 0, sizeof(ifdata));
	strncpy(ifdata.ifr_name, interfaceName, strlen(interfaceName));  // set interface's name

	//get the IP addr
	if ( (ret = ioctl(sock, SIOCGIFADDR, &ifdata)) < 0)
	{
		DEBUG_OUTPUT (
				dbglog (SVR_WARN, DBG_MAIN, "getIPAddrByInterfaceName()->ioctl() has returned %d, error: %s. interfaceName = \"%s\"\n", ret, strerror(errno), interfaceName);
		)
		close(sock);
		return NULL;
	}

	close(sock);

	in_addr = (struct sockaddr_in *) &ifdata.ifr_addr;
	IP = inet_ntoa(in_addr->sin_addr); //for IPv6 use 	inet_ntop()
	memcpy( (void*)(&addr->sin_addr), (const void*)(&in_addr->sin_addr), sizeof(in_addr->sin_addr) );
	DEBUG_OUTPUT (
			dbglog (SVR_DEBUG, DBG_MAIN, "getIPAddrByInterfaceName(): for interface \"%s\" IP = [%s]\n", interfaceName, IP);
	)
	return IP;
}

void printProcMemStatus(int point)
{
	char memcomm[30]= {0};
	printf("****************************************************** %d\n\n", point);
	sprintf(memcomm, "cat /proc/%d/status", getpid());
	system(memcomm);
	printf("****************************************************** %d, local_sendInform_count = %d\n\n", point, local_sendInform_count);
	fflush(stdout);
}

/* Find substring in the string with case insensitive.
 * Works as strstr() with case insensitive.
 * */
char *myStrCaseStr(char *str1, const char *str2)
{
	const char *c1, *c2;
	if (!str1 || !str2)
		return NULL;

	for(;*str1;*str1++)
	{
		c1 = str1;
		c2 = str2;
		while((*c1++ | 32) == (*c2++ | 32))
			if(!*c2)
				return (str1);
	}
	return(NULL);
}
