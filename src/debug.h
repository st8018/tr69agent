/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef debug_H
#define debug_H

#include "./integration_interfaces/integrationSettings.h"

#ifdef _DEBUG
# define DEBUG_OUTPUT(x) {x}
#define HAVE_COLOR_SCREEN_LOG  // if defined then will be used different colors in screen log.
#else
# define DEBUG_OUTPUT(x)
#endif

#ifdef _DEBUG

typedef enum dbg_sys_enum
{
	DBG_SOAP,
	DBG_MAIN,
	DBG_PARAMETER,
	DBG_TRANSFER,
	DBG_DU_TRANSFER,
	DBG_STUN,
	DBG_ACCESS,
	DBG_MEMORY,
	DBG_EVENTCODE,
	DBG_SCHEDULE,
	DBG_ACS,
	DBG_VOUCHERS,
	DBG_OPTIONS,
	DBG_DIAGNOSTIC,
	DBG_VOIP,
	DBG_KICK,
	DBG_CALLBACK,
	DBG_HOST,
	DBG_REQUEST,
	DBG_DEBUG,
	DBG_AUTH,
	DBG_DB,
	DBG_ABAM
} Dbg_sys_enum;

#ifndef DBG_LEVEL_ENUM_IS_DECLARED
typedef enum dbg_level_enum
{
	SVR_DEBUG,
	SVR_INFO,
	SVR_WARN,
	SVR_ERROR
} Dbg_level_enum;
#endif

struct dbg_stuct
{
	char sys_str[20];
	Dbg_level_enum level_int;
	char level_str[20];
};

#ifdef HAVE_COLOR_SCREEN_LOG
// Colors for screen log:
#define C_Reset		0
#define C_Bold		1
#define C_Under		4
#define C_Blinking	5
#define C_Invers	7
#define C_Black		30
#define C_Red		31
#define C_Green		32
#define C_Brown		33
#define C_Blue		34
#define C_Magenta	35
#define C_Cyan		36
#define C_Light		37
#define BG_Black	40
#define BG_Red		41
#define BG_Green	42
#define BG_Brown	43
#define BG_Blue		44
#define BG_Magenta	45
#define BG_Cyan		46
#define BG_Light	47

#endif /* HAVE_COLOR_SCREEN_LOG */

int loadConfFile (char *);

#ifndef dbglog
void dbglog( Dbg_level_enum, Dbg_sys_enum, const char *, ... );
#endif

#endif /* _DEBUG */

#endif /* debug_H */
