/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

/*
 * deployment_unit.h
 *
 *  Created on: 17.01.2012
 *      Author: apnt
 */

#ifndef DEPLOYMENT_UNIT_H_
#define DEPLOYMENT_UNIT_H_

#ifdef HAVE_DEPLOYMENT_UNIT
#include "du_transfer.h"

int processDeploymentUnit(DeploymentUnitEntry * due);
#endif /* HAVE_DEPLOYMENT_UNIT */

#endif /* DEPLOYMENT_UNIT_H_ */



