/***************************************************************************
 *    Copyright (C) 2004-2012 by Dimark Software Inc.                         *
 *    support@dimark.com                                                   *
 ***************************************************************************/

#ifndef SI_TRANSFER_H_
#define SI_TRANSFER_H_

#ifdef HAVE_SCHEDULE_INFORM

#define SI_MAX_NAME_SIZE			32

int initScheduleInform(void);
int execScheduleInform(struct soap *, xsd__unsignedInt, xsd__string);
int clearDelayedScheduleInform();
void clearScheduleInformList();
int isDelayedScheduleInform(void);
unsigned int handleDelayedScheduleInform();
int handleDelayedScheduleInformEvents();

#endif /* HAVE_SCHEDULE_INFORM */

#endif /* SI_TRANSFER_H_ */
